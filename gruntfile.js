module.exports = function (grunt) {
       grunt.initConfig({
              pkg: grunt.file.readJSON('package.json'),
			
              concat: {
                     options: {
                           separator: '\n\n',
                           stripBanners: true
                     },
                     js: {
                           src: ['src/main/webapp/app/route/rfxcelRoute.js','src/main/webapp/app/controller/*.js','src/main/webapp/app/service/*.js','src/main/webapp/app/directive/*.js','src/main/webapp/app/component/*.js'],
                           //dest: 'src/main/webapp/app/itt-<%= pkg.version %>.js'
						   dest: 'src/main/webapp/app/itt-<%= pkg.version %>-min.js'
						   
                     },
					 css: {
						   src: ['src/main/webapp/app/assets/css/common/nav-tab.css'
						   ,'src/main/webapp/app/assets/css/common/main.css'
						   ,'src/main/webapp/app/assets/css/common/sensor.css'
						   ,'src/main/webapp/app/assets/css/common/common_styles.css'
						   ,'src/main/webapp/app/assets/css/common/angular-material-new.css'
						   ,'src/main/webapp/app/assets/css/common/sensor-dropdown-multiselect.css'
						   ,'src/main/webapp/app/assets/css/common/sensor-new.css'
						   ,'src/main/webapp/app/assets/css/common/profile/main_updates.css'
						   ,'src/main/webapp/app/assets/css/common/profile/profiles_update.css'
						   ,'src/main/webapp/app/assets/css/common/profile/create_profile.css'
						   ,'src/main/webapp/app/assets/css/common/profile/rzslider.css'],
                           //dest: 'src/main/webapp/app/assets/css/itt-<%= pkg.version %>.css'
						   dest: 'src/main/webapp/app/assets/css/itt-<%= pkg.version %>-min.css'
					 }
              },
              uglify: {
                     options: {
                           compress: {
                             drop_console: true
                           },
                           banner: '/*! <%= pkg.name %> <%= grunt.template.today("dd-mm-yyyy") %> */\n',
                           mangle: false
                     },
                     dist: {
                           files: {
                                  'src/main/webapp/app/itt-<%= pkg.version %>-min.js': [ '<%= concat.js.dest %>' ]
                           }
                     }
              },
			  cssmin: {
				options: {
					keepSpecialComments: 0
				},
				my_target: {
					options: {
						keepSpecialComments: 1
					},
					src: [ '<%= concat.css.dest %>' ],
					dest: 'src/main/webapp/app/assets/css/itt-<%= pkg.version %>-min.css'
				}
			},
			replace: {
				  dist: {
				    options: {
				      patterns: [
				        {
				          match: 'timestamp',
				          replacement: '<%= new Date().getTime() %>'
				        }
				      ]
				    },
				    files: [
				      {src: ['grunt-build/index.html'], dest: 'src/main/webapp/index.html'}
				    ]
				  }
				}
			  
       });
 
       grunt.loadNpmTasks('grunt-contrib-concat');
	   grunt.loadNpmTasks('grunt-contrib-uglify');
	   grunt.loadNpmTasks('grunt-css');
	   grunt.loadNpmTasks('grunt-replace');
	   grunt.registerTask('dev', ['concat']);
	    grunt.registerTask('prod', ['concat','uglify','cssmin','replace']);
	   
};