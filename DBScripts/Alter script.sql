
--------------------------------------- ITT 2.1 Migration starts-----------------------------------------------------
delete from system_config where param_name in ('kirsen.config.template');
INSERT INTO `system_config`(`param_name`, `param_value`,`display_name`,`active`) VALUES
('kirsen.config.template','{@temp_min@:-200.0,@temp_max@:200.0,@temp_mode@:@transfer@,@communication_period@:<profile.comm.freq>,@gps_period@:<profile.gps.freq>,@temperature_period@:<profile.attr.freq>}','Kirsen Configuration Template', 1)
;

ALTER TABLE `sensor_package`  
	DROP KEY `package_product_id`,
 	ADD UNIQUE KEY `package_product_id`  (`org_id`,`package_id`,`product_id`) ;
 	
DELETE FROM `system_config` WHERE (param_name in ('create.password.text.body','reset.password.text.body') );
INSERT INTO `system_config`(`param_name`, `param_value`,`display_name`,`active`) VALUES
('create.password.text.body',' With Verizon Intelligent Track & Trace, quickly track products, manage devices, and access alerts.#Please verify your email addresss by clicking the following link:# Please contact support if you have any issues with your account.', 'Set Password text Body', 1),
('reset.password.text.body',' With Verizon Intelligent Track & Trace, quickly track products, manage devices, and access alerts.#Please reset password by clicking the following link:# Please contact support if you have any issues with your account.', 'ReSet Password text body', 1)
;


delete from org_config where param_name in ('sensor.data.copyfields','dashboard.map.center.latitude','dashboard.map.center.longitude','dashboard.map.zoom','dashboard.map.type');
INSERT INTO `org_config` VALUES
('sensor.data.copyfields','','List of Fields to Propagate', 0, 1, 0),
('dashboard.map.center.latitude', '40', 'Dashboard Map Center (latitude)', 0, 1, 0),
('dashboard.map.center.longitude', '-95', 'Dashboard Map Center (longitude)', 0, 1, 0),
('dashboard.map.zoom', '6', 'Dashboard Map Zoom Factor', 0, 1, 0),
('dashboard.map.type', 'google.maps.MapTypeId.HYBRID', 'Dashboard Map Type', 0, 1, 0);
-- added on 7/27 for identifying home geo location
ALTER TABLE `sensor_geopoints` ADD COLUMN IF NOT EXISTS `is_home` INT(2) UNSIGNED NOT NULL DEFAULT 0 AFTER `radius`;
ALTER TABLE `sensor_associate` DROP COLUMN IF EXISTS `geopoint_name` ;

-- Table to capture Excursion at an attribute level
DROP TABLE IF EXISTS `sensor_excursion_log`;
CREATE TABLE `sensor_excursion_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(45) NOT NULL,
  `container_id` varchar(255) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `attribute` varchar(45)NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

delete from system_config where param_name ='build.version';
insert into system_config values ('build.version','v2.1','Buid Version Number',0);

delete from system_config where param_name ='build.version';
insert into system_config values ('build.version','v2.1','Buid Version Number',0);

DELETE FROM system_config WHERE param_name = 'queclink.config.template';
INSERT INTO system_config VALUES
('queclink.config.template', 'AT+GTRTO=gl300vc,4,,,,,,FFFF$AT+GTSRI=gl300vc,2,,1,<queclink.device.url>,<queclink.device.port>,216.218.206.20,6880,,0,0,0,,,,FFFF$AT+GTFKS=gl300vc,1,1,3,1,1,3,3,10,3,3,FFFF$AT+GTCFG=gl300vc,gl300vc,GL300VC,0,0.0,1,10,1F,,,823,0,1,1,<profile.attr.freq>,0,0,20491231235959,1,0,,FFFF$AT+GTTMA=gl300vc,+,0,0,0,,0,,,,FFFF$AT+GTFRI=gl300vc,1,0,,,0000,0000,<profile.gps.freq>,<profile.gps.freq>,,9999,9999,,1000,1000,0,5,50,5,0,0,FFFF$AT+GTTEM=gl300vc,0,-20,60,900,3600,2,,,,,,FFFF$AT+GTTPR=gl300vc,0,<profile.attr.freq>,<profile.comm.count>,1,,,,,FFFF$ AT+GTDOG=gl300vc,0,60,30,0200,,1,0,0,360,360,1,FFFF$AT+GTPDS=gl300vc,1,FB,,,,,,,FFFF$AT+GTRTO=gl300vc,1,,,,,0,FFFF$AT+GTDAT=gl300vc,0,,Device <deviceid> setup 2.1 completed,1,,,,FFFF$', 'Queclink Device Configuration Template', 1)
;

delete from org_config where param_name in ('sensor.data.copyexpiry');
INSERT INTO `org_config` VALUES
('sensor.data.copyexpiry','0','Seconds to Expire Propagation Data', 0, 1, null)
;

-- 18th Aug 2017, Table to manage menus with menu_code
DROP TABLE IF EXISTS `rfx_menus`;
CREATE TABLE `rfx_menus` (
`id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`menu_code` INT(20) NOT NULL,
`state` VARCHAR(200) NOT NULL,
`menu_name` VARCHAR(40) NOT NULL,
`order` INT(10) NOT NULL,
`org_id` BIGINT(20) DEFAULT 0,
`min_user_type` int(2) unsigned DEFAULT '2' COMMENT '0-System, 1-Admin, 2-Normal',
PRIMARY KEY (`id`),
UNIQUE KEY `unique_index` (`menu_code`,`org_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 23rd Aug 2017, JIRA:ITT-374-Changed menu name from "Profile" to "Device"
delete from rfx_menus;
INSERT INTO `rfx_menus` VALUES
(1, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 2),
(2, 2, 'app.profile', 'Devices', 2, 0, 2),
(3, 3, 'app.products', 'Shipments', 3, 0, 2),
(4, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 2),
(5, 5, 'app.organization', 'Organization', 5, 0, 2),
(6, 6, 'app.settings', 'Settings', 6, 0, 1),
(7, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 1);

delete from system_config where param_name in ('thinkspace.integration.url','thinkspace.integration.retry.count',
'thinkspace.integration.get.interval','thinkspace.logFolder','thinkspace.poll.service.start');
INSERT INTO system_config VALUES
('thingspace.integration.url','https://preprodiwk.thingspace.verizon.com/api/v2/devices','ThingSpace Integration Url',1),
('thingspace.integration.retry.count','3','ThingSpace Integration retry count for request',1),
('thingspace.integration.get.interval','1','ThingSpace Integration get request interval (minute))',1),
('thingspace.logFolder','c:\\thingspaceLog','ThingSpace Log Folder',1),
('thingspace.poll.service.start','true','Start ThingSpace poll service (true/false)',1);

delete from org_config where param_name in ('thinkspace.auth.token','date.format');
INSERT INTO `org_config` VALUES 
('thingspace.auth.token','','Think space authorization token', 0, 1, null),
('date.format','yyyy-MM-dd HH:mm:ss','Date Format', 0, 1, null);

-- 22nd Aug 2017, added gps_com_freq_limit, receive_com_freq_limit, send_com_freq_limit in table sensor_device_types*/
ALTER TABLE sensor_device_types 
ADD COLUMN gps_com_freq_limit INT(20) DEFAULT 86400 COMMENT 'frequency limit unit is seconds',
ADD COLUMN receive_com_freq_limit INT(20) DEFAULT 86400 COMMENT 'frequency limit unit is seconds',
ADD COLUMN send_com_freq_limit INT(20) DEFAULT 86400 COMMENT 'frequency limit unit is seconds';

delete from system_config where param_name in ('email.disclaimer');
INSERT INTO system_config VALUES
('email.disclaimer','Recommended browser is Chrome version 58 and above.','Email Disclaimer ',1);

-- 29 Aug for Locate now support for Queclink
INSERT INTO system_config VALUES
('queclink.config.locate.cmd','AT+GTRTO=gl300vc,1,,,,,,FFFF$','Queclink Locate now command',1);


ALTER TABLE `rfx_org` MODIFY COLUMN `email` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `rfx_users` MODIFY COLUMN `user_email` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
MODIFY COLUMN `last_login` DATETIME;

--------------------------------------- ITT 2.1 Migration ends -----------------------------------------------------
--------------------------------------- ITT 2.2 Migration starts -----------------------------------------------------
-- Build#12
-- 04 Sept 2017, ITT-400 : Master table alert_group_mapping
DROP TABLE IF EXISTS `alert_group_mapping`;
CREATE TABLE `alert_group_mapping` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `alert_id` BIGINT(20) UNSIGNED NOT NULL,
  `alert_group_id` BIGINT(20) UNSIGNED NOT NULL,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
ENGINE = INNODB DEFAULT CHARSET=utf8;

-- 04 Sept 2017, ITT-400 : migration script to migrate data from table sensor_alert_types to table alert_group_mapping
INSERT INTO alert_group_mapping(alert_id, alert_group_id, created_time)
SELECT id, alert_group_id, created_time FROM sensor_alert_types WHERE alert_group_id IS NOT NULL AND alert_group_id>0;
ALTER TABLE sensor_alert_types DROP COLUMN alert_group_id;

-- 11 Sept, ITT-390, support for same packageId in different products
ALTER TABLE sensor_associate ADD COLUMN `product_id`  varchar(100) AFTER `child_id`;
ALTER TABLE sensor_notifications ADD COLUMN `product_id` varchar(100) AFTER `container_id`;
ALTER TABLE sensor_data ADD COLUMN `product_id` varchar(100) AFTER `container_id`;

-- This needs to be run only once, to populate product_id values from sensor_package to sensor_associate, sensor_data, sensor_notifications
-- NOTE uncomment this and run it once for migrating the data
/* UPDATE sensor_associate sa
INNER JOIN sensor_package sp
ON sa.parent_id = sp.package_id
SET sa.product_id  = sp.product_id
WHERE sa.parent_id =  sp.package_id;

UPDATE sensor_data sd
INNER JOIN sensor_package sp
ON sd.container_id = sp.package_id
SET sd.product_id  = sp.product_id
WHERE sd.container_id =  sp.package_id;

UPDATE sensor_notifications sn
INNER JOIN sensor_package sp
ON sn.container_id = sp.package_id
SET sn.product_id  = sp.product_id
WHERE sn.container_id =  sp.package_id; */
-- End 
ALTER TABLE sensor_data MODIFY container_id varchar(255) NOT NULL;
ALTER TABLE sensor_notifications MODIFY container_id varchar(255) NOT NULL;

ALTER TABLE sensor_associate MODIFY product_id varchar(100) NOT NULL;
ALTER TABLE sensor_data MODIFY product_id varchar(100) NOT NULL;
ALTER TABLE sensor_notifications MODIFY product_id varchar(100) NOT NULL;

-- ITT 411 : Added column to store shock and vibration value
Alter TABLE sensor_data 
ADD COLUMN vibration varchar(45) DEFAULT NULL,
ADD COLUMN shock varchar(45) DEFAULT '0';

INSERT INTO `sensor_attributes` VALUES
(20,'vibration',1, 3054,'g/hz2',0,6);

UPDATE sensor_attributes SET range_max = 180 WHERE attr_name = "tilt";

INSERT INTO `sensor_default_alerts` VALUES
(18,3054,'Alert Notification - Vibration Threshold Exceeded ','Alert Notification- The Vibration (<<ATTR>>) is not within the acceptable range.\r\n','vibration',0);

-- 11 Sept 2017, ITT-424 : help desk information
DELETE FROM system_config WHERE param_name IN ('helpdesk.contact.email','helpdesk.contact.phone.primary',
'helpdesk.contact.phone.other');
INSERT INTO system_config VALUES
('helpdesk.contact.email','IoTTechSupport@VerizonWireless.com','Helpdesk Contact Email',1),
('helpdesk.contact.phone.primary','800-525-0481','Helpdesk Contact Primary Phone',1),
('helpdesk.contact.phone.other','800-922-0204','Helpdesk Contact Other Phone',1);

-- 11 Sept 2017, ITT-429 : Added support for shock attribute
UPDATE sensor_attributes SET range_max = 8 WHERE attr_name = "shock";

-- 12 Sept 2017, ITT:412 
Alter TABLE sensor_associate 
ADD COLUMN left_home tinyint(1) DEFAULT 0;

-- 13 Sept 2017, ITT-422
ALTER TABLE sensor_devices ADD COLUMN `mdn` VARCHAR(25) DEFAULT NULL;

-- 14 Sept 2017, ITT-431
ALTER TABLE sensor_threshold 
ADD COLUMN `alert` TINYINT(1) DEFAULT 1 COMMENT '0 - Disable , 1 -  Enable' AFTER `max_value`,
ADD COLUMN `notification` TINYINT(1) DEFAULT 1 COMMENT '0 - Disable , 1 - Enable' AFTER `alert`;

DELETE FROM org_config WHERE param_name IN ('system.demo','data.simulator.path');
INSERT INTO `org_config` VALUES 
('system.demo','false','Is demo system', 0, 1, 0),
('data.simulator.path','c:\simulator.bat','Data Simulator path', 0, 1, 0);

-- 22 Sept 2017, added show_graph in sensor_attr_type
ALTER TABLE sensor_attr_type 
ADD COLUMN `show_map` TINYINT(1) DEFAULT 0 COMMENT '0 - Do not show on map , 1 -  Show on map' AFTER `show_graph`;


--------- 26 sept ,2017 , reset stored procedure.

DELIMITER $$
DROP PROCEDURE IF EXISTS `resetDevice` $$
CREATE PROCEDURE `resetDevice`(IN deviceId VARCHAR(70), IN productId VARCHAR(100), IN packageId VARCHAR(200))
BEGIN
	delete from sensor_notifications where device_id=deviceId and container_id=packageId and product_id=productId;
	delete from sensor_data where device_id=deviceId and container_id=packageId and product_id=productId;
	update sensor_associate set excursion_status=0 where child_id=deviceId and parent_id=packageId and product_id=productId;
	update sensor_devices set state=2 where device_id=deviceId and state <>1;
	delete FROM sensor_excursion_log where device_id=deviceId;
	delete FROM sensor_device_log where device_id=deviceId;
	delete FROM sensor_bacterial_growth_log where device_id=deviceId;
END $$
DELIMITER ;


DROP PROCEDURE IF EXISTS `resetK4Data`;
DELIMITER $$
CREATE PROCEDURE `resetK4Data`()
BEGIN
	CALL resetDevice('358502062238111', 'Kirsen 4', '8111');
END$$
DELIMITER ;

-- 25 Sept 2017, resetK1K3Data stored procedure 
DROP PROCEDURE IF EXISTS `resetK1K3Data`;
DELIMITER $$
CREATE PROCEDURE `resetK1K3Data`()
BEGIN
	CALL resetDevice('358502062237949', 'Kirsen 1', '7949');
	CALL resetDevice('358502062238145', 'Kirsen 3', '8145');
END$$
DELIMITER ;


DELIMITER $$
DROP PROCEDURE IF EXISTS `resetData` $$
CREATE PROCEDURE `resetData`()
BEGIN
	CALL resetDevice('358509999900707', 'Verizon V4 Product', '10707');
END $$
DELIMITER ;
--------------------------------------- ITT 2.2 Migration ends -----------------------------------------------------

--------------------------------------- ITT 2.3 Migration starts -----------------------------------------------------
ALTER table sensor_device_log modify column attribute_value varchar(45) DEFAULT NULL;

INSERT INTO `sensor_default_alerts` values
(null, 3706,'GeoFence Alert - Shipment On Route ','GeoFence Alert -  Shipment on route, currently at <<ATTR>>.\r\n','location',1);



-- added on 9/28 for getting menu list by user type

delete from rfx_menus;

ALTER TABLE `rfx_menus` DROP INDEX `unique_index`,
 ADD UNIQUE INDEX `unique_index` USING BTREE(`menu_code`, `org_id`, `min_user_type`);
 
INSERT INTO `rfx_menus` VALUES
(1, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 2),
(2, 2, 'app.profile', 'Devices', 2, 0, 2),
(3, 3, 'app.products', 'Shipments', 3, 0, 2),
(4, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 2),
(5, 5, 'app.organization', 'Organization', 5, 0, 2),
(6, 8, 'app.auditLogs', 'Audit', 6, 0, 2),
(7, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 1),
(8, 2, 'app.profile', 'Devices', 2, 0, 1),
(9, 3, 'app.products', 'Shipments', 3, 0, 1),
(10, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 1),
(11, 5, 'app.organization', 'Organization', 5, 0, 1),
(12, 6, 'app.settings', 'Settings', 6, 0, 1),
(13, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 1),
(14, 8, 'app.auditLogs', 'Audit', 8, 0, 1),
(15, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 0),
(16, 2, 'app.profile', 'Devices', 2, 0, 0),
(17, 3, 'app.products', 'Shipments', 3, 0, 0),
(18, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 0),
(19, 5, 'app.organization', 'Organization', 5, 0, 0),
(20, 6, 'app.settings', 'Settings', 6, 0, 0),
(21, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 0),
(22, 8, 'app.auditLogs', 'Audit', 8, 0, 0),
(23, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 3),
(24, 2, 'app.profile', 'Devices', 2, 0, 3),
(25, 3, 'app.products', 'Shipments', 3, 0, 3),
(26, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 3),
(27, 5, 'app.organization', 'Organization', 5, 0, 3),
(29, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 3),
(30, 8, 'app.auditLogs', 'Audit', 8, 0, 3);

-- 28 Sept 2017, ITT-452 - Insert default System Organization(System-org).
ALTER TABLE rfx_org AUTO_INCREMENT = 0;
DELETE FROM rfx_org WHERE org_name = 'System-org';
INSERT INTO rfx_org(org_id, org_name, short_name, email, org_extid, active, org_tz, tz_id)
VALUES(0, 'System-org', 'System-org', 'test@rfxcel.com', 123456, 1, '-07:00', 2);
UPDATE rfx_org SET org_id=0 WHERE org_name='System-org';

-- build#15 , added on 10/04 for new attribute types of geofence

delete from sensor_attributes where alert_code in (3701,3702,3703,3704,3705,3706);
insert into sensor_attributes (attr_name, is_configurable, alert_code, unit, range_min, range_max)
values ('geofenceEntry', 0, 3706, '', 0.00000, 100.00000),
('geofenceExit', 0, 3704, '', 0.00000, 100.00000),
('geofenceOnRoute', 0, 3703, '', 0.00000, 100.00000),
('geofenceArrival', 0, 3701, '', 0.00000, 100.00000),
('geofenceDeparture', 0, 3702, '', 0.00000, 100.00000),
('samelocation', 0, 3705, '', 0.00000, 100.00000);

-- 06 Oct 2017, migration script which should execute only once. It will migrate/add the default missing alert types for all the existing active organization
INSERT INTO sensor_alert_types(alert_code, alert_msg, alert_detail_msg, org_id)
SELECT alert_code, alert_msg, alert_detail_msg, org.org_id FROM sensor_default_alerts alert, rfx_org org WHERE alert.is_default=1 
AND alert.alert_code NOT IN(SELECT alert_code FROM sensor_alert_types WHERE org_id=org.org_id) AND org.active=1 ORDER BY org.org_id;

-- 06 Oct 2017, elastic search parameters
DELETE FROM system_config WHERE param_name IN ('elasticsearch.data.fetch.size','elasticsearch.host.name','elasticsearch.index.name','elasticsearch.transport.tcpport','elasticsearch.data.fetch');
INSERT INTO system_config VALUES
('elasticsearch.data.fetch.size','30000','Elastic Search Fetch Size ',1),
('elasticsearch.host.name','127.0.0.1','Elastic Search Host Name ',1),
('elasticsearch.index.name','coldchainlight','Elastic Search Index Name ',1),
('elasticsearch.data.fetch','false','Elastic Search Fetch Data', 1);

-- 09 Oct 2017, Audit log table
DROP TABLE IF EXISTS `audit_log`;
CREATE TABLE `audit_log` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
`user_id` BIGINT DEFAULT NULL,
`user_name` VARCHAR(50) DEFAULT NULL,
`entity_type` VARCHAR(50) DEFAULT NULL,
`entity_id` BIGINT DEFAULT NULL,
`action` VARCHAR(50) DEFAULT NULL COMMENT 'Create, Update ,Delete',
`old_value` TEXT,
`new_value` TEXT,
`description` TEXT,
`org_id` bigint(20) NOT NULL,
`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=INNODB;

-- 12 oct 2017 , Change the attribute name from 'excursion' to 'duration'

update sensor_attributes set attr_name ='duration' where attr_name ='excursion';
update sensor_default_alerts set attribute_name ='duration' where attribute_name='excursion';
update sensor_attr_type set attr_name ='duration' where attr_name='excursion';
update sensor_attr_type set legend ='Duration' where legend='excursion';

-- 13 Oct 2017, Change the unit for vibration
update `sensor_attributes` set unit = 'g/hz²' where attr_name = 'vibration';
update `sensor_attr_type` set unit = 'g/hz²' where attr_name = 'vibration';

-- 17 Oct 2017, Release version is 2.31
UPDATE `system_config` SET `param_value`='2.31' WHERE  `param_name`='build.version';

-- 23 Oct 2017, Procedures to rename Device Type and Reset Excursion Status
DROP PROCEDURE IF EXISTS `renameDeviceType`;
DELIMITER $$
CREATE PROCEDURE `renameDeviceType`(IN oldName VARCHAR(45),IN newName VARCHAR(45), IN orgId INTEGER)
BEGIN
	IF orgId IS NULL THEN
		UPDATE sensor_device_types SET device_type=newName WHERE device_type=oldName;
	ELSE
		UPDATE sensor_device_types SET device_type=newName WHERE device_type=oldName and org_id = orgId;	
	END IF;
END$$
DELIMITER ;

DELIMITER $$
DROP PROCEDURE IF EXISTS `resetExcursionStatus` $$
CREATE PROCEDURE `resetExcursionStatus`(IN deviceId VARCHAR(45), IN productId VARCHAR(45), IN packageId VARCHAR(200))
BEGIN
	update sensor_associate set excursion_status=0 where child_id=deviceId and parent_id=packageId and product_id=productId;
	update sensor_data set excursion_status = 0 where device_id=deviceId and container_id=packageId and product_id=productId;
    delete from sensor_notifications where device_id=deviceId and container_id=packageId and product_id=productId;
	delete FROM sensor_excursion_log where device_id=deviceId;
	delete FROM sensor_device_log where device_id=deviceId;
END $$
DELIMITER ;
--------------------------------------- ITT 2.3 Migration ends -----------------------------------------------------

-- Oct 27 2017 Alter script to add range values alert details

UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Temperature (<<ATTR>>) is not within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3042";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Light (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3043";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Tilt (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3044";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Pressure (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3045";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Humidity (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3046";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The Bacteria Count Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3047";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Shock value (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3048";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Battery Percentage (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3049";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The <<ATTR>> has been out of range for last (<<ATTR1>>) minutes.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3050";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Ambient Temperature value (<<ATTR>>) is not within acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3051";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The <<ATTR>> value (<<ATTR1>>) is back within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3052";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The Bacterial Growth Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3053";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The Vibration (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3054";

UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Temperature (<<ATTR>>) is not within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3042";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Light (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3043";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Tilt (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3044";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Pressure (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3045";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Humidity (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3046";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The Bacteria Count Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3047";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Shock value (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3048";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Battery Percentage (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3049";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The <<ATTR>> has been out of range for last (<<ATTR1>>) minutes.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3050";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Ambient Temperature value (<<ATTR>>) is not within acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3051";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The <<ATTR>> value (<<ATTR1>>) is back within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3052";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The Bacterial Growth Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3053";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The Vibration (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3054";

-- 30 Oct 2017, thingspace device type
DELETE FROM system_config WHERE param_name IN ('thingspace.device.type');
INSERT INTO system_config VALUES
('thingspace.device.type','ts.device.cHeAssetTracker','Thinkspace Device Type',1);

-- 02 Nov 2017 Added config for preset shock value
INSERT INTO system_config VALUES
('shock.preset.value','0.5','Preset Shock Value',1);