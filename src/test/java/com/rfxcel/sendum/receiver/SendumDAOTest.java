package com.rfxcel.sendum.receiver;


import java.sql.SQLException;

import org.junit.Test;

import com.rfxcel.sensor.dao.SendumDAO;

public class SendumDAOTest {

	
	@Test
	public void testCheckIfDeviceIsPresent() throws SQLException{
		System.out.println("is device present "+SendumDAO.getInstance().checkIfDeviceIsPresent("88000512000353",101));
	}

	@Test
	public void testCheckIfPackageIsPresent() throws SQLException{
		System.out.println("Is package present :" + SendumDAO.getInstance().checkIfPackageIsPresent("urn:epc:id:sgtin:003701.0100000.1000000000000000353","ProductId",101));
	}
}