package com.rfxcel.sendum.receiver;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.transmitter.RestRequester;
import com.rfxcel.sensor.util.Timer;

public class TestDevice {
	final static Logger logger = Logger.getLogger(TestDevice.class);
	private static String deviceid = null;
	private static String epc = null;
	private RestRequester restRequester = null;
	private String targetURL = null;

	public TestDevice(String targetURL) {
		restRequester = new RestRequester("sendum", "Sendum123");
		this.targetURL = targetURL;
	}

	public static void main(String[] args) {
		if (args.length == 0) {
			System.out.println("TestDevice <url> <devid> <epc>");
			return;
		}
		if (args.length > 1) {
			deviceid = args[1];
		}
		if (args.length > 2) {
			epc = args[2];
		}
		TestDevice td = new TestDevice(args[0]);
		td.commissionDevice();
		//sleep(10);
		td.associateDevice();
		//sleep(10);
		//td.disassociateDevice();
		//sleep(10);
		//td.decommissionDevice();
		//sleep(10);
		td.restRequester.shutdown();
	}
	
	public static void sleep(long sec)
	{
		try { Thread.sleep(sec * 1000L); } catch(Exception e) {}
	}
	
	private String doReplace(String payload)
	{
		
		String payload2 = payload;
		if (deviceid != null) {
			payload2 = payload2.replace("99999999999999", deviceid);
		}
		if (epc != null) {
			payload2 = payload2.replace("urn:epc:id:sgtin:003701.0100000.1000000000000000353", epc);
		}
		return(payload2);
	}
	
	public void commissionDevice()
	{
		Timer timer = new Timer();
		String payload = "{\"action\":\"commission\", \"deviceid\":\"99999999999999\",  \"url\":\"https://itt-demo10.track-n-trace.net/rfxcelwss/services/IMessagingServiceSoapHttpPort?wsdl\" }";
		payload = doReplace(payload);
		restRequester.doPost(targetURL, payload);
		logger.info("Sent REST request in " + timer.getSeconds() + "s to " + targetURL + " payload " + payload);
	}

	public void associateDevice()
	{
		Timer timer = new Timer();
		//String payload = "{\"action\":\"associate\", \"deviceid\":\"99999999999999\",  \"epc\":\"urn:epc:id:sgtin:003701.0100000.1000000000000000353\" }";
		String payload = "{\"action\":\"associate\", \"deviceid\":\"99000512000707\",  \"epc\":\"urn:epc:id:sgtin:003701.0100000.1000000000000000353    \" }";
		payload = doReplace(payload);
		restRequester.doPost(targetURL, payload);
		logger.info("Sent REST request in " + timer.getSeconds() + "s to " + targetURL + " payload " + payload);
	}

	public void disassociateDevice()
	{
		Timer timer = new Timer();
		String payload = "{\"action\":\"disassociate\", \"deviceid\":\"99999999999999\" }";
		payload = doReplace(payload);
		restRequester.doPost(targetURL, payload);
		logger.info("Sent REST request in " + timer.getSeconds() + "s to " + targetURL + " payload " + payload);
	}
	
	public void decommissionDevice()
	{
		Timer timer = new Timer();
		String payload = "{\"action\":\"decommission\", \"deviceid\":\"99999999999999\" }";
		payload = doReplace(payload);
		restRequester.doPost(targetURL, payload);
		logger.info("Sent REST request in " + timer.getSeconds() + "s to " + targetURL + " payload " + payload);
	}
}
