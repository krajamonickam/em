package com.rfxcel.sendum.receiver;

import java.io.IOException;
import java.net.URISyntaxException;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rfxcel.notification.service.NotificationService;
import com.rfxcel.sensor.vo.SensorResponse;
import com.rfxcel.sensor.service.SensorService;
import com.rfxcel.auth2.AuthService;


public class SensorServiceTest {
	static Client client = null;
	static Dispatcher dispatcher = null;

	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public  static void setUpBeforeClass() throws Exception {
		//client = ClientBuilder.newBuilder()	.register(JacksonJaxbJsonProvider.class).build();
		dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory sensorService = new POJOResourceFactory(SensorService.class);
		POJOResourceFactory notificationService = new POJOResourceFactory(NotificationService.class);
		dispatcher.getRegistry().addResourceFactory(sensorService);
		dispatcher.getRegistry().addResourceFactory(notificationService);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public  static void tearDownAfterClass() throws Exception {
		//client.close();
	}

	/*@Test
	public void testItemList() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {

		String payload = "{" + "\"project\": \"rfXcel-ITT\","
				+ "\"version\": 6.0,"
				+ "\"createDate\": \"2014-06-25 17:15:19\","
				+ "\"instanceType\": \"Production\","
				+ "\"searchType\": \"device\"," 
				+ "\"itemList\": {"
				+ "\"itemId\": [\"990005\"]}" 
				+ "}";
		;

		MockHttpRequest request = MockHttpRequest.post("/sensor/getItemList")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}*/

	@Test
	public void testGetAttrInfo() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
					+ "\"deviceId\": \"99000512000353\","
				//	+ "\"showAll\": true,"
				//	+"\"id\":1482144665193,"
					+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\""
					+"}" 
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getAttrInfo")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testAttrData() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				+ "\"deviceId\": \"99000512000353\","
				+"\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\","
				+ "\"duration\": \".5\","
				+ "\"attrType\":["  
				           + "{"  
				               +"\"attrName\":\"temperature\","
				               +"\"startDateTime\":1479226500000"
				               + "},"  
				               + "{"  
				               +"\"attrName\":\"tilt\","
				               +"\"startDateTime\":1479226500000"
				               + "}"
				         +"]"
				+"}" 
				+"}";
	/*	String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				+ "\"deviceId\": \"99000512000353\","
				+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000354\","
				+ "\"duration\":\".5\""
				+"}" 
				+"}";*/

		MockHttpRequest request = MockHttpRequest.post("/sensor/getAttrData")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);  
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}

	@Test
	public void testMapData() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		String payload = "{" 
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				+ "\"deviceId\": \"99000512000353\","
				+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\""
				+"}" 
				+"}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getMapData")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}

	@Test
	public void testDeviceStatus() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				+ "\"deviceId\": 99000512000353"
				+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getDeviceStatus")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testAuthentication() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		//requesting access token	
		POJOResourceFactory noDefaults = new POJOResourceFactory(AuthService.class);
		Dispatcher dispatcherToken = MockDispatcherFactory.createDispatcher();
		dispatcherToken.getRegistry().addResourceFactory(noDefaults);
		ObjectMapper mapper = new ObjectMapper();
		String signInpayload = "{"
				+ "\"userName\": \"admin\","
				+ "\"password\": \"demo235\","
				+ "\"dateTime\": \"2014-06-25 17:15:19\""
				+ "}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(signInpayload.getBytes());
		MockHttpResponse tokenresponse = new MockHttpResponse();
		dispatcherToken.invoke(request, tokenresponse);   

		Object json = mapper.readValue(tokenresponse.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}


	@Test
	public void testPackageStatus() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		//requesting access token	
		
		ObjectMapper mapper = new ObjectMapper();
	/*	POJOResourceFactory noDefaults = new POJOResourceFactory(AuthService.class);
		Dispatcher dispatcherToken = MockDispatcherFactory.createDispatcher();
		dispatcherToken.getRegistry().addResourceFactory(noDefaults);
		String signInpayload = "{"
				+ "\"userName\": \"admin\","
				+ "\"password\": \"demo235\","
				+ "\"dateTime\": \"2014-06-25 17:15:19\""
				+ "}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
				.contentType(MediaType.APPLICATION_JSON)
				.content(signInpayload.getBytes());
		MockHttpResponse tokenresponse = new MockHttpResponse();
		dispatcherToken.invoke(request, tokenresponse);   

		SignInResponse res =  mapper.readValue(tokenresponse.getContentAsString(), SignInResponse.class);
		String accessToken = res.getAuthToken();*/
		//System.out.println(accessToken);
		//String accessToken = "c5df25659c0459c90e1120fd90a29623";

		//Making call to resource using the token in header
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\""
				+"}"
				+ "}";


		MockHttpRequest resourceRequest = MockHttpRequest.post("/sensor/getPackageStatus")
				//.header(AuthConstants.HEADER_AUTHORIZATION, accessToken)
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(resourceRequest, response);   
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testActiveShipmentList() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		String payload = "{"
				+ "\"searchType\": \"All\","
				+ "\"showMap\": true"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getActiveShipmentList")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testMapDataByDeviceNPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				+ "\"deviceId\": \"99000512000353\","
				+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\","
				+ "\"startDate\": 1481541457000"
				
				+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getMapData")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testgetNotifications() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"searchType\": \"container\","
				+ "\"sortBy\": \"Age\","
				+ "\"start\": 0,"		//TO get all the notifications
				+ "\"deviceData\": {"
				+ "\"deviceId\": \"99000512000353\","
				+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\""
				+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getNotifications")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testgetShipmentDetails() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
				//+ "\"deviceId\": \"99000512000353\","
				+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000357\""
				+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/getShipmentDetails")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}

	@Test
	public void testAddProduct() throws URISyntaxException, JsonGenerationException, JsonMappingException, IOException{
		String payload = "{"
				+"\"productDetail\": {"
					+"\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000102\","
					+"\"productId\": \"50351005088896\","
					+"\"deviceType\": \"PTS300D\","
					+"\"deviceId\": \"99000512000102\","
					+"\"shipFrom\": {"
						+"\"locationId\": \"1323165410\","
						+"\"locationName\": \"rfxcel \","
						+"\"address1\": \"12667 Alcosta Blvd\","
						+"\"address2\": \"Suite 375\","
						+"\"city\": \"San Ramon\","
						+"\"state\": \"CALIFORNIA\","
						+"\"country\": \"US\","
						+"\"postalCode\": \"94583\""
					+"},"
					+"\"shipTo\": {"
						+"\"locationId\": \"1320922552833\","
						+"\"locationName\": \"RFX Pharmaceuticals\","
						+"\"address1\": \"12667 Alcosta Blvd\","
						+"\"address2\": \"Suite 375\","
						+"\"city\": \"San Ramon\","
						+"\"state\": \"CALIFORNIA\","
						+"\"country\": \"US\","
						+"\"postalCode\": \"94583\""
						+"}"
					+"}"
		+"}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct").contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}
	@Test
	public void testfindLoctionName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		
		
		String payload = "{"
				+ "\"locationData\": {"
				+ "\"name\": \"rfx\""
				+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/findLoctionName")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String locations = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(locations);

	}
	@Test
	public void testfindAddress() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		String payload = "{"
				+ "\"locationData\": {"
				+ "\"id\": \"E5036519\""
				+"}"
				+ "}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/findAddress")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String address = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(address);

	}
	
	@Test
	public void testDecommissionDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException{
		String payload = "{"
				+ "\"dateTime\": \"2014-12-09 12:15:19\","
				+ "\"deviceData\": {"
				+ "\"deviceId\": 99000512000353"
				+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/decommissionDevice")
				.contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);   
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);

	}
	
	@Test
	public void testGetProducts() throws URISyntaxException, JsonGenerationException, JsonMappingException, IOException{
		String payload = "{"
			//	+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"searchType\": \"All\""
				+ "}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/getProducts").contentType(MediaType.APPLICATION_JSON)
				.content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		
		Object json  = mapper.readValue(response.getContentAsString(), Object.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
	@Test
	public void testGetDeviceTypes() throws JsonParseException, JsonMappingException, IOException, URISyntaxException{
		String payload = "{"
				//	+ "\"dateTime\": \"2014-06-25 17:15:19\","
					+ "\"searchType\": \"All\""
					+ "}";
		MockHttpRequest  request = MockHttpRequest.post("/sensor/getDeviceTypes").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes()) ;
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value =  mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
	@Test
	public void testCommissionPackage() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException{
		String payload = 
				"{"
				+"\"productDetail\": {"
					+"\"productList\":[{"
						+"\"productId\": \"50351005088896\","
						+"\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000101\""
					+"},"
					+"{"
					+"\"productId\": \"50351005088896\","
					+"\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000102\""
						+"}]"
					+"}"
				+"}";
		
		MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
	@Test
	public void testAssociate() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException{
		/*String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
					+ "\"deviceId\": \"99000512000355\","
					+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000355\""
					+"}"
				+ "}";*/
		String payload = "{"
					+ "\"deviceId\": \"99000512000359\","
					+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.100000000000000039\""
					+"}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/associate").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
	@Test
	public void testDisAssociate() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException{
		String payload = "{"
				+ "\"dateTime\": \"2014-06-25 17:15:19\","
				+ "\"deviceData\": {"
					+ "\"deviceId\": \"99000512000353\","
					+ "\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\""
					+"}"
				+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
	@Test
	public void testDeCommissionPackage() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException{
		String payload = 
				"{"
				+"\"deviceData\": {"
						+"\"packageId\": \"urn:epc:id:sgtin:003701.0100000.1000000000000000353\""
					+"}"
				+"}";
		
		MockHttpRequest request = MockHttpRequest.post("/sensor/deCommissionPackage").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
	
	@Test
	public void testDeCommissionDevice() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException{
		String payload = 
				"{"
				+"\"deviceData\": {"
					+ "\"deviceId\": \"99000512000353\""
					+"}"
				+"}";
		
		MockHttpRequest request = MockHttpRequest.post("/sensor/decommissionDevice").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	@Test
	public void testCommissionItems() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException{
		String payload = "{"
				+ "\"itemListDetails\": [{"
						+ "\"deviceId\": \"99000512000101\","
						+ "\"deviceType\": \"PTS300D\""
						+"},"
						+"{"
						+ "\"deviceId\": \"99000512000102\","
						+ "\"deviceType\": \"PTS300D\""
						+"}]"
			+ "}";

		MockHttpRequest request = MockHttpRequest.post("/sensor/commission").contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
		MockHttpResponse response = new MockHttpResponse();
		dispatcher.invoke(request, response);
		ObjectMapper mapper = new ObjectMapper();
		Object json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
		String value = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(value);
	}
	
}

	