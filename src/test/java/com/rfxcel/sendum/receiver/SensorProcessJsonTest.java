package com.rfxcel.sendum.receiver;

import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.junit.Assert;
import org.junit.Test;

import com.rfxcel.sensor.service.SensorProcessJSON;

/**
 * 
 * @author sachin_kohale
 * test case for same location alert here configured parameter in database are:-
 * radius=200mtr, excurstion_duration=1min and notify_limit=3
 * The test cases should be execute one by one from top to bottom
 */
public class SensorProcessJsonTest {
	private static final Logger logger = Logger.getLogger(SensorProcessJsonTest.class);

	/**
	 * test same location alert if previous message having status time 2017-03-03 8:33:34  
	 * and current message having status time 2017-03-03 8:34:34
	 * so time interval would be 1 min and if distance between previous and current location is less than 200mtr,
	 * notify count will increment to 1
	 * result would be success
	 */
	@Test
	public void test_1_SuccessSameLocationAlert(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceType", "sendum");		
		map.put("deviceIdentifier", "358509999900505");
		map.put("latitude", "41.98146");
		map.put("longitude", "89.97803");
		map.put("statusTimeStamp", "2017-03-03 8:34:34");
		boolean result = SensorProcessJSON.getInstance().sameLocationAlert(map);		
		Assert.assertTrue("Same location alert is success", result);
		if(result){
			logger.info("Same location alert is success.");
		}else{
			logger.info("Same location alert is stopped.");
		}
		
		
	}
	/**
	 * test same location alert if previous message having status time 2017-03-03 8:33:34  
	 * and current message having status time 2017-03-03 8:36:34
	 * so time interval would be more 1 min and if distance between previous and current location is less than 200mtr,
	 * notify count will increment to 2
	 * result would be success 
	 */
	@Test
	public void test_2_SuccessSameLocationAlert(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceType", "sendum");		
		map.put("deviceIdentifier", "358509999900505");
		map.put("latitude", "41.98146");
		map.put("longitude", "89.97803");
		map.put("statusTimeStamp", "2017-03-03 8:36:34");
		boolean result = SensorProcessJSON.getInstance().sameLocationAlert(map);
		Assert.assertTrue("Same location alert is success", result);
		if(result){
			logger.info("Same location alert is success.");
		}else{
			logger.info("Same location alert is stopped.");
		}
	}
	
	/**
	 * test same location alert if previous message having status time 2017-03-03 8:33:34 or less 
	 * and current message having status time 2017-03-03 8:38:34
	 * so time interval would be 1 min and if distance between previous and current location is less than 200mtr,
	 * notify count will increment to 3
	 * result would be success
	 */
	@Test
	public void test_3_SuccessSameLocationAlert(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceType", "sendum");		
		map.put("deviceIdentifier", "358509999900505");
		map.put("latitude", "41.98146");
		map.put("longitude", "89.97903");
		map.put("statusTimeStamp", "2017-03-03 8:38:34");
		boolean result = SensorProcessJSON.getInstance().sameLocationAlert(map);
		Assert.assertTrue("Same location alert is success", result);
		if(result){
			logger.info("Same location alert is success.");
		}else{
			logger.info("Same location alert is stopped.");
		}
	}
	
	/**
	 * test same location alert if previous message having status time 2017-03-03 8:33:34 or less 
	 * and current message having status time 2017-03-03 8:40:34
	 * so time interval would be 1 min and if distance between previous and current location is less than 200mtr,
	 * notify count will increment to 4
	 * result would be failed because notify counter exceeds
	 */
	@Test
	public void test_1_FailedSameLocationAlert(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceType", "sendum");		
		map.put("deviceIdentifier", "358509999900505");
		map.put("latitude", "41.98146");
		map.put("longitude", "89.97903");
		map.put("statusTimeStamp", "2017-03-03 8:40:34");
		boolean result = SensorProcessJSON.getInstance().sameLocationAlert(map);
		Assert.assertFalse("Same location alert is success", result);
		if(result){
			logger.info("Same location alert is success.");
		}else{
			logger.info("Same location alert is stopped.");
		}
	}
	
	/**
	 * test same location alert if previous message having status time 2017-03-03 8:33:34 or less 
	 * and current message having status time 2017-03-03 8:41:34
	 * so time interval would be 1 min and if distance between previous and current location is less than 200mtr,
	 * notify count will decrement to 0 and previous message status timestamp will be cleared from cache and
	 * result would be failed because distance exceeds than radius(200mtr), previous latitude=89.97803, current latitude=89.98903
	 */
	@Test
	public void test_2_FailedSameLocationAlert(){
		Map<String, String> map = new HashMap<String, String>();
		map.put("deviceType", "sendum");		
		map.put("deviceIdentifier", "358509999900505");
		map.put("latitude", "41.98146");
		map.put("longitude", "89.98903");
		map.put("statusTimeStamp", "2017-03-03 8:41:34");
		boolean result = SensorProcessJSON.getInstance().sameLocationAlert(map);
		Assert.assertFalse("Same location alert is success", result);
		if(result){
			logger.info("Same location alert is success.");
		}else{
			logger.info("Same location alert is stopped.");
		}
	}
	
	
	
}
