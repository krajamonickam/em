package com.rfxcel.sendum.receiver;

import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;


// A client program implementing TCP socket
public class QuecLinkTerminal {
	public static void main (String args[]) 
	{  
		Socket s = null; 
		try{ 
			for(int j=0; j< 10 ; j++)
			{
				callServer(j);
			}
		}
		catch(Exception e){
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		finally {
			if(s!=null) 
				try {s.close();
				} 
			catch (IOException e) {/*close failed*/}
		}
	}
	
	/**
	 * @param j
	 */
	private static void callServer(int j){
		Socket s = null; 
		try{ 
			// arguments supply message and hostname of destination
			int serverPort = 6880; 	//TODO change it based on the server used
			String ip = "10.222.40.92"; //TODO change it based on ip address
			s = new Socket(ip, serverPort); 
			DataOutputStream output = new DataOutputStream(s.getOutputStream()); 
			String ack = "+ACK:GTBSI,280001,135790246811220,,0002,20100310172830,1177"+j+"$"; 
			output.writeBytes(ack);
			//output.writeUTF("AT+GTSRI=gl300vc,4,,,116.226.44.16,9002,+8613812341234,0,1,,,,,003$");
			System.out.println("sending data");
		}
		catch (UnknownHostException e){ 
			System.out.println("UnknownHostException: "+e.getMessage());
			e.printStackTrace();
		}
		catch (EOFException e){
			System.out.println("EOF: "+e.getMessage());
			e.printStackTrace();
		}
		catch (IOException e){
			System.out.println("IO: "+e.getMessage());
			e.printStackTrace();
		} 
		catch(Exception e){
			System.out.println("Exception: "+e.getMessage());
			e.printStackTrace();
		}
		finally {
			if(s!=null) 
				try {s.close();
				} 
			catch (IOException e) {/*close failed*/}
		}
	}
}

