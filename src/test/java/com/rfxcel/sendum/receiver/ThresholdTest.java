/**
 * @ COMPANY              : RFXCEL
 * @ PROJECT              : Sendum Receiver
 * @ AUTHOR               : JEYASEELAN   
 * @ DESCRIPTION          : 
 *
 * @ MODIFICATION HISTORY:
 **********************************************************************************
 * DATE			*		NAME                *       			CHANGES           *
 **********************************************************************************
 * May 26, 2016	*		JEYASEELAN A		* CREATED                             *
 * 				*							*                                     *
 *				*							*									  *
 *				*							*									  *
 **********************************************************************************
 */
package com.rfxcel.sendum.receiver;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rfxcel.sensor.transmitter.RestRequester;


/**
 * @author User
 *
 */
public class ThresholdTest {
	private static RestRequester restRequester = null;
	private static String targetURL = null;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		restRequester = new RestRequester("sendum", "Sendum123");
		targetURL = "http://localhost:58438/sendum-1.0/sendum";
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		restRequester.shutdown();
	}
	
	/*@Test
	public void testThreshold() {
		String payload = "{\"deviceIdentifier\":\"99000512000353\",\"fixTimeStamp\":\"2016-05-23 07:24:34\",\"latitude\":\"41.98146\",\"longitude\":\"87.94426\",\"hepe\":\"64\",\"fixType\":\"MSAL\",\"valid\":\"TRUE\",\"statusTimeStamp\":\"2016-05-23 08:23:37\",\"capacityRemaining\":\"3531\",\"capacityFull\":\"3557\",\"voltage\":\"4168\",\"cycleCount\":\"10\",\"temperature\":\"22000\",\"temperatureProbe\":\"-999\",\"pressure\":\"99594\",\"humidity\":\"21\",\"orientation\":\"20,30,40\",\"light\":\"13\",\"rssi\":\"-86\",\"sid\":\"4384\"}";
		restRequester.doPost(targetURL, payload);
	}
	
	@Test
	public void testGeofenceNumber() {
		String payload = "{\"deviceIdentifier\":\"99000512000353\",\"alarmTimeStamp\":\"2016-05-17 17:08:17\",\"geofenceNumber\":\"1\",\"geofenceEvent\":\"ENTRY\"}";
		restRequester.doPost(targetURL, payload);
	}
	
	@Test
	public void testGeofenceName() {
		String payload = "{\"deviceIdentifier\":\"99000512000707\",\"alarmTimeStamp\":\"2016-05-17 17:23:20\",\"geofenceName\":\"Route\",\"geofenceEvent\":\"EXIT\",\"latitude\":\"42.394983\",\"longitude\":\"-71.272423\" }";
		restRequester.doPost(targetURL, payload);
	}*/
	
	@Test
	public void testGeofenceAlarm() {
		String payload = "{\"deviceIdentifier\":\"99000512000707\",\"alarmType\":\"HIGHTEMPERATURE\",\"alarmValue\":\"224\",\"alarmTimeStamp\":\"2016-05-13 14:12:02\"}";
		restRequester.doPost(targetURL, payload);
	}

}
