/**
 * @ COMPANY              : RFXCEL
 * @ PROJECT              : Sendum Receiver
 * @ AUTHOR               : JEYASEELAN   
 * @ DESCRIPTION          : 
 *
 * @ MODIFICATION HISTORY:
 **********************************************************************************
 * DATE			*		NAME                *       			CHANGES           *
 **********************************************************************************
 * Jun 2, 2016	*		JEYASEELAN A		* CREATED                             *
 * 				*							*                                     *
 *				*							*									  *
 *				*							*									  *
 **********************************************************************************
 */
package com.rfxcel.sendum.receiver;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rfxcel.sensor.transmitter.RestRequester;


/**
 * @author User
 *
 */
public class GeofenceTest {
	private static RestRequester restRequester = null;
	private static String targetURL = null;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		restRequester = new RestRequester("sendum", "Sendum123");
		targetURL = "http://localhost:58438/sendum-1.0/sendum";
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		restRequester.shutdown();
	}
	
	@Test
	public void testGeofenceNumber() {
		String payload = "{\"deviceIdentifier\":\"99000512000707\",\"alarmTimeStamp\":\"2016-05-17 17:08:17\",\"geofenceNumber\":\"2\",\"geofenceEvent\":\"ENTRY\"}";
		restRequester.doPost(targetURL, payload);
	}
	
	/*@Test
	public void testGeofenceName() {
		String payload = "{\"deviceIdentifier\":\"99000512000707\",\"alarmTimeStamp\":\"2016-05-17 17:23:20\",\"geofenceName\":\"Route\",\"geofenceEvent\":\"EXIT\",\"latitude\":\"42.394983\",\"longitude\":\"-71.272423\" }";
		restRequester.doPost(targetURL, payload);
	}*/
}
