/**
 * @ COMPANY              : RFXCEL
 * @ PROJECT              : Sendum Receiver
 * @ AUTHOR               : JEYASEELAN   
 * @ DESCRIPTION          : 
 *
 * @ MODIFICATION HISTORY:
 **********************************************************************************
 * DATE			*		NAME                *       			CHANGES           *
 **********************************************************************************
 * Jun 2, 2016	*		JEYASEELAN A		* CREATED                             *
 * 				*							*                                     *
 *				*							*									  *
 *				*							*									  *
 **********************************************************************************
 */
package com.rfxcel.sendum.receiver;

import java.util.Map;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rfxcel.sensor.transmitter.RestRequester;
import com.rfxcel.sensor.util.Utility;


/**
 * @author User
 *
 */
public class SensorDataTest {
	private static RestRequester restRequester = null;
	private static String targetURL = null;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		restRequester = new RestRequester("sendum", "Sendum123");
		targetURL = "http://localhost:58438/sendum-1.0/sendum";
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
		restRequester.shutdown();
	}
	
	@Test
	public void testThreshold() {
		String payload = "{\"deviceIdentifier\":\"99000512000353\",\"fixTimeStamp\":\"2016-05-23 07:24:34\",\"latitude\":\"41.98146\",\"longitude\":\"87.94426\",\"hepe\":\"64\","
				+ "\"fixType\":\"MSAL\",\"valid\":\"TRUE\",\"statusTimeStamp\":\"2016-05-23 08:23:37\",\"capacityRemaining\":\"3531\",\"capacityFull\":\"3557\",\"voltage\":\"4168\","
				+ "\"cycleCount\":\"10\",\"temperature\":\"23000\",\"temperatureProbe\":\"-999\",\"pressure\":\"99594\",\"humidity\":\"21\",\"orientation\":\"20,30,40\",\"light\":\"13\","
				+ "\"rssi\":\"-86\",\"sid\":\"4384\"}";
		restRequester.doPost(targetURL, payload);
	}
	
	
	@Test
	public void testMapConversion(){
		
		String payload = "{	\"id\": \"642109b0-6d76-6ae9-ed1f-fa73806551fb\","
				+ "\"kind\": \"ts.device.sdk.packageTracker.legacy\",\"version\": \"1.0\",\"versionid\": \"1ea82a5a-6d80-11e7-95ec-02420a0a0a19\",\"createdon\": \"2017-07-19T20:31:18.582Z\","
				+ "\"lastupdated\": \"2017-07-20T19:17:49.566Z\",\"name\": \"Package tracker\",\"providerid\": \"9dfcfa69-a1c8-4eae-8611-b282646bb113\",\"state\": \"ready\",\"qrcode\": \"VHwEmpByGoezXZss\","
				+ "\"mac\": \"6a:00:02:69:a8:60\",	\"fields\":"
				+ " {	\"AccelerationX\": \"20.1500911\",\"AccelerationY\": \"0.8238130\",	\"AccelerationZ\": \"12.4278364\",\"DropDetected\": \"true\",\"Latitude\": \"37.3272552\","
				+ "\"LightEvent\": \"44532\",\"Longitude\": \"-120.1065445\",\"MovementEvent\": \"true\",\"PackageOpened\": \"false\"}}";
		
		String pay1 = "{\"name\":\"john\",\"age\":22,\"class\":\"mca\"}";
		Map<String, Object> map = Utility.convertNestedJsonToMap(payload);
		System.out.println(map);
	}
	
	@Test
	public void testDateConversion(){
		
		System.out.println(Utility.convertDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'" ,"yyyy-MM-dd HH:mm:ss",  "2017-07-20T19:17:49.566Z"));
	}
	
}
