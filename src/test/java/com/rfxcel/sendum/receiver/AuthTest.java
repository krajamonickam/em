package com.rfxcel.sendum.receiver;

import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Date;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.oltu.oauth2.common.exception.OAuthSystemException;
import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.jaxrs.JacksonJaxbJsonProvider;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;

import com.rfxcel.sensor.service.SensorService;
import com.rfxcel.auth2.AuthConstants;
import com.rfxcel.auth2.AuthService;
import com.rfxcel.auth2.beans.AuthRequest;
import com.rfxcel.auth2.beans.SignInResponse;


public class AuthTest{

	static private URL url;
	static private Client client = null;
	static Dispatcher dispatcher = null;
	/**
	 * @throws java.lang.Exception
	 */
	@BeforeClass
	public  static void setUpBeforeClass() throws Exception {
		client = ClientBuilder.newBuilder().register(JacksonJaxbJsonProvider.class).build();
		url=new URL("http://localhost:58438/sensor");
		
		dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory authService = new POJOResourceFactory(SensorService.class);
		dispatcher.getRegistry().addResourceFactory(authService);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@AfterClass
	public  static void tearDownAfterClass() throws Exception {
		client.close();
	}


	@Test
	public void directTokenRequest() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException, OAuthSystemException{
		
		AuthRequest request = new AuthRequest();
		request.setClientId("oauth2test");
    	request.setSecret("oauth2clientsecret");
		request.setUserLogin("admin");
		request.setPassword("demo235");
		request.setDateTime(new Date().toString());

		//String accessToken = TokenGenUtility.generateToken(request).getAuthToken();
		//System.out.println(accessToken);
		URL Url = new URL("http://localhost:58438/sensor/rest/sensor" + "/user/login");
		WebTarget targetToken = client.target(Url.toURI());
		Response tokenresponse = targetToken.request().post(Entity.entity(request, "application/json"));

		String accessToken = tokenresponse.readEntity(SignInResponse.class).getAuthToken();
		
		//String accessToken = "c5df25659c0459c90e1120fd90a29623";

		if (accessToken!=null) {
			URL restUrl = new URL(url.toString() + "/resource");
			WebTarget target = client.target(restUrl.toURI());
			String entity = target.request("application/json").header(AuthConstants.HEADER_AUTHORIZATION, "Bearer " + accessToken).get(String.class);
			System.out.println("Response = " + entity);
		}
	}
	
	@Test
	public void testForgetPassword() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		//requesting access token	
		POJOResourceFactory noDefaults = new POJOResourceFactory(AuthService.class);
		Dispatcher dispatcherToken = MockDispatcherFactory.createDispatcher();
		dispatcherToken.getRegistry().addResourceFactory(noDefaults);
		ObjectMapper mapper = new ObjectMapper();
		String signInpayload = "{"
				+ "\"userName\": \"admin\","
				+ "\"url\": \"http://localhost/sensor/rest/sensor/user/forgetpass/\""
				+ "}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/user/forgetPassword").contentType(MediaType.APPLICATION_JSON)
				.content(signInpayload.getBytes());
		MockHttpResponse tokenresponse = new MockHttpResponse();
		dispatcherToken.invoke(request, tokenresponse);   

		Object json = mapper.readValue(tokenresponse.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}
	
	@Test
	public void testResetPassword() throws JsonGenerationException, JsonMappingException, IOException, URISyntaxException {
		//requesting access token	
		POJOResourceFactory noDefaults = new POJOResourceFactory(AuthService.class);
		Dispatcher dispatcherToken = MockDispatcherFactory.createDispatcher();
		dispatcherToken.getRegistry().addResourceFactory(noDefaults);
		ObjectMapper mapper = new ObjectMapper();
		String signInpayload = "{"
				+ "\"userName\": \"admin\","
				+ "\"password\": \"demo235\","
				+ "\"token\": \"3f8826f43bdb0781f85dca71eb398449\""
				+ "}";
		MockHttpRequest request = MockHttpRequest.post("/sensor/user/resetPassword").contentType(MediaType.APPLICATION_JSON)
				.content(signInpayload.getBytes());
		MockHttpResponse tokenresponse = new MockHttpResponse();
		dispatcherToken.invoke(request, tokenresponse);   

		Object json = mapper.readValue(tokenresponse.getContentAsString(), Object.class);
		String indented = mapper.writerWithDefaultPrettyPrinter().writeValueAsString(json);
		System.out.println(indented);
	}
}
