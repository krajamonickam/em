package com.rfxcel.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.rfxcel.sensor.vo.SensorResponse;
import com.rfxcel.sensor.service.SensorService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestShipmentServices {

	private static Logger logger = Logger.getLogger(TestShipmentServices.class);
	private static HashMap<String, TestCase> testCases; 
	//	private static HashMap<String , Integer> headerColumnMapping;

	static Client client = null;
	static Dispatcher dispatcher = null;
	private static final String INPUT_FILE= "TestData.xlsx";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		testCases = TestUtil.loadXlsTestData(INPUT_FILE, 3);
		dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory sensorService = new POJOResourceFactory(SensorService.class);
		dispatcher.getRegistry().addResourceFactory(sensorService);
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}
	
	
	
	@Test
	public void test_10_AddProductInValidProductId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductInValidProductId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductInValidProductId");
		}
	}
	
	@Test
	public void test_11_AddProductInValidDeviceType() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductInValidDeviceType");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductInValidDeviceType");
		}
	}
	
	@Test
	public void test_12_AddProductPackageInAssociateState() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductPackageInAssociateState");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductPackageInAssociateState");
		}
	}
	
	@Test
	public void test_13_AddProductPackageInDeCommState() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductPackageInDeCommState");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductPackageInDeCommState");
		}
	}
	
	@Test
	public void test_14_AddProductDeviceInAssociatedState() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductDeviceInAssociatedState");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductDeviceInAssociatedState");
		}
	}
	
	@Test
	public void test_15_AddProductDeviceInDeCommState() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductDeviceInDeCommState");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductDeviceInDeCommState");
		}
	}
	
	@Test
	public void test_16_AddProductNullPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductNullPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductNullPackageId");
		}
	}
	
	@Test
	public void test_17_AddProductNullDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductNullDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductNullDeviceId");
		}
	}
	
	@Test
	public void test_18_AddProduct() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProduct");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProduct");
		}
	}
	@Test
	public void test_19_AddProductNew() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAddProductNew");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/addProduct")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
				SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAddProductNew");
		}
	}
}
