package com.rfxcel.rest;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.rfxcel.auth2.AuthService;
import com.rfxcel.auth2.beans.AuthResponse;
import com.rfxcel.auth2.beans.SignInResponse;


@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestAuthService {

	private static Logger logger = Logger.getLogger(TestAuthService.class);
	private static HashMap<String, TestCase> testCases; 
	//	private static HashMap<String , Integer> headerColumnMapping;

	static Client client = null;
	static Dispatcher dispatcher = null;
	private static final String INPUT_FILE= "TestData.xlsx";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		testCases = TestUtil.loadXlsTestData(INPUT_FILE, 0);
		dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory authService = new POJOResourceFactory(AuthService.class);
		dispatcher.getRegistry().addResourceFactory(authService);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	
	@Test()
	public void test_10_SignIn() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testSignIn");
		if(testCase != null){
			//{"userName":"admin","password":"demo235"}
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignIn");
		}
	}

	@Test
	public void test_11_SignInNullUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignInNullUserName");
		if(testCase != null){
			//{"userName":null,"password":"p@ssw0rd" }
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignInNullUserName");
		}
	}
	
	@Test
	public void test_12_SignIncorrectUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignIncorrectUserName");
		if(testCase != null){
		//{"userName":"admin123","password":"p@ssw0rd"  }
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignIncorrectUserName");
		}
	}

	@Test
	public void test_13_SignInNullPassword() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignInNullPassword");
		if(testCase != null){
			//{"userName":"admin","password":null }
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignInNullPassword");
		}
	}
	
	@Test
	public void test_14_SignInIncorrectPassword() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignInIncorrectPassword");
		if(testCase != null){
			//{"userName":"admin","password":"p@ssw0rd"  }
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/login")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignInIncorrectPassword");
		}
	}

	

	@Test
	public void test_20_SignOut() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignOut");
		if(testCase != null){
			//{"userName":"admin","authToken":"token value"}
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/logout")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignOut");
		}
	}
	
	@Test
	public void test_21_SignOutNullUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignOutNullUserName");
		if(testCase != null){
			//{"userName":null,"authToken":"token value"}
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/logout")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignOutNullUserName");
		}
	}
	
	@Test
	public void test_22_SignOutIncorrectUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testSignOutInCorrectUserName");
		if(testCase != null){
			String payload = testCase.getData();
			//{"userName":"admin123","authToken":"token value"}
			MockHttpRequest request = MockHttpRequest.post("/sensor/user/logout")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SignInResponse json = mapper.readValue(response.getContentAsString(), SignInResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testSignOutInCorrectUserName");
		}
	}

	
	@Test
	public void test_30_ForgetPassword() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testForgetPassword");
		if(testCase != null){
			//{"userName":"admin","url":"http://localhost/sensor/rest/sensor/user/forgetpass/"}
			String payload = testCase.getData();
			MockHttpRequest request = MockHttpRequest.post("/sensor/user/forgetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testForgetPassword");
		}
	}
	
	@Test
	public void test_31_ForgetPasswordNullUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testForgetPasswordNullUserName");
		if(testCase != null){
			//{"userName":null,"url":"http://localhost/sensor/rest/sensor/user/forgetpass/"}
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/forgetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testForgetPasswordNullUserName");
		}
	}
	
	@Test
	public void test_32_ForgetPasswordIncorrectUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testForgetPasswordIncorrectUserName");
		if(testCase != null){
			//{"userName":"admin123","url":"http://localhost/sensor/rest/sensor/user/forgetpass/"}
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/forgetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testForgetPasswordIncorrectUserName");
		}
	}
	
	@Test
	public void test_33_ForgetPasswordNullUrl() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testForgetPasswordNullUrl");
		if(testCase != null){
			//{"userName":"admin"}
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/forgetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testForgetPasswordNullUrl");
		}
	}
	
	@Test
	public void test_40_ResetPassword() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testResetPassword");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/resetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testResetPassword");
		}
	}
	
	@Test
	public void test_41_ResetPasswordNullUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testResetPasswordNullUserName");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/resetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testResetPasswordNullUserName");
		}
	}
	
	@Test
	public void test_42_ResetPasswordInCorrectUserName() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testResetPasswordInCorrectUserName");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/resetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testResetPasswordInCorrectUserName");
		}
	}

	@Test
	public void test_43_ResetPasswordNullPassword() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testResetPasswordNullPassword");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/resetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testResetPasswordNullPassword");
		}
	}
	
	@Test
	public void test_44_ResetPasswordInvalidToken() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testResetPasswordInvalidToken");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/user/resetPassword")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			AuthResponse json = mapper.readValue(response.getContentAsString(), AuthResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testResetPasswordInvalidToken");
		}
	}
}
