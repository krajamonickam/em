package com.rfxcel.rest;

import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Iterator;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class TestUtil {

	private static Logger logger = Logger.getLogger(TestUtil.class);

	public static HashMap<String, TestCase> loadXlsTestData(String inputFile, int sheetIndex){
		InputStream inputStream = null;
		Workbook workbook;
		HashMap<String, TestCase> testCases = new HashMap<String, TestCase>();
		try
		{

			inputStream = Thread.currentThread().getContextClassLoader().getResourceAsStream(inputFile);

			workbook = new XSSFWorkbook(inputStream);
			Sheet firstSheet = workbook.getSheetAt(sheetIndex);
			Iterator<Row> iterator = firstSheet.iterator();
			if(iterator.hasNext())
				iterator.next();// skipping header row
			Boolean skip = null;
			while (iterator.hasNext()) {
				Row nextRow = iterator.next();
				Iterator<Cell> cellIterator = nextRow.cellIterator();
				TestCase testCase = new TestCase();
				while (cellIterator.hasNext()) {
					Cell nextCell = cellIterator.next();
					int columnIndex = nextCell.getColumnIndex();
					switch (columnIndex) {
					case 3:
						testCase.setTestMethod((String) getCellValue(nextCell));
						break;
					case 4:
						testCase.setData((String) getCellValue(nextCell));
						break;
					case 5:
						testCase.setResponseStatus((String) getCellValue(nextCell));
						break;
					case 6:
						testCase.setResponseMessage((String)getCellValue(nextCell));
						break;
					case 7:
						skip = ((Boolean)getCellValue(nextCell));
						break;
					}
				}
				if(skip!=null && !skip){
					testCases.put(testCase.getTestMethod(),testCase);
				}
			}
		}	catch (Exception e) 
		{
			e.printStackTrace();
		}finally{
			try {
				if(inputStream!= null)
					inputStream.close();
			} catch (IOException e) {
				logger.error(" "+e.getMessage());
				e.printStackTrace();
			}
		}
		return testCases;
	}
	/**
	 * 
	 * @param cell
	 * @return
	 */
	private static Object getCellValue(Cell cell) {
		switch (cell.getCellType()) {
		case Cell.CELL_TYPE_STRING:
			return cell.getStringCellValue();
		case Cell.CELL_TYPE_BOOLEAN:
			return cell.getBooleanCellValue();
		case Cell.CELL_TYPE_NUMERIC:
			return cell.getNumericCellValue();
		}
		return null;
	}

	/*	private Workbook getWorkbook() throws IOException {
    Workbook workbook = null;
    FileInputStream inputStream = new FileInputStream(new File(INPUT_FILE));
    if (INPUT_FILE.endsWith("xlsx")) {
        workbook = new XSSFWorkbook(inputStream);
    } else if (INPUT_FILE.endsWith("xls")) {
        workbook = new HSSFWorkbook(inputStream);
    } else {
        throw new IllegalArgumentException("The specified file is not Excel file");
    }
    return workbook;
}*/
}
