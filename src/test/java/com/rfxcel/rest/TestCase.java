package com.rfxcel.rest;

public class TestCase {

	public TestCase() {
	}

	private String testMethod;
	private String data;
	private String responseStatus;
	private  String responseMessage;
	
	
	/**
	 * @param data
	 * @param responseStatus
	 * @param responseMessage
	 */
	public TestCase(String data, String responseStatus, String responseMessage) {
		this.data = data;
		this.responseStatus = responseStatus;
		this.responseMessage = responseMessage;
	}
	
	/**
	 * 
	 * @param data
	 * @param responseStatus
	 */
	public TestCase(String data, String responseStatus) {
		this.data = data;
		this.responseStatus = responseStatus;
	}
	/**
	 * @return the testMethod
	 */
	public String getTestMethod() {
		return testMethod;
	}

	/**
	 * @param testMethod the testMethod to set
	 */
	public void setTestMethod(String testMethod) {
		this.testMethod = testMethod;
	}

	/**
	 * @return the data
	 */
	public String getData() {
		return data;
	}
	/**
	 * @param data the data to set
	 */
	public void setData(String data) {
		this.data = data;
	}
	/**
	 * @return the responseStatus
	 */
	public String getResponseStatus() {
		return responseStatus;
	}
	/**
	 * @param responseStatus the responseStatus to set
	 */
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}
	/**
	 * @param responseMessage the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	
	
}
