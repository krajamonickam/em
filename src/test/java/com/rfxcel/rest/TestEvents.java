package com.rfxcel.rest;

import static org.junit.Assert.*;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.HashMap;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.rfxcel.sensor.vo.SensorResponse;
import com.rfxcel.sensor.service.SensorService;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestEvents {

	private static Logger logger = Logger.getLogger(TestEvents.class);
	private static HashMap<String, TestCase> testCases; 

	static Client client = null;
	static Dispatcher dispatcher = null;
	private static final String INPUT_FILE= "TestData.xlsx";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		testCases = TestUtil.loadXlsTestData(INPUT_FILE, 2);
		dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory sensorService = new POJOResourceFactory(SensorService.class);
		dispatcher.getRegistry().addResourceFactory(sensorService);
	}
	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test_10_CommissionDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionDevice");
		}
	}
	
	@Test
	public void test_11_CommissionMultipleDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionMultipleDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionMultipleDevice");
		}
	}
	
	@Test
	public void test_12_CommissionDevicesDiffTypes() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionDevicesDiffTypes");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionDevicesDiffTypes");
		}
	}
	
	@Test
	public void test_13_CommissionDeviceNullDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionDeviceNullDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionDeviceNullDeviceId");
		}
	}
	
	@Test
	public void test_14_CommissionDeviceNullDeviceType() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionDeviceNullDeviceType");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionDeviceNullDeviceType");
		}
	}
	
	@Test
	public void test_15_CommissionDeviceIncorrectDeviceType() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionDeviceIncorrectDeviceType");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionDeviceIncorrectDeviceType");
		}
	}
	
	@Test
	public void test_16_AlreadyCommissionedDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAlreadyCommissionedDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commission")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAlreadyCommissionedDeviceId");
		}
	}
	
	@Test
	public void test_20_CommissionPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionPackage");
		}
	}
	
	@Test
	public void test_21_CommissionMultiplePackages() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionMultiplePackages");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionMultiplePackages");
		}
	}
	

	@Test
	public void test_22_CommissionPackagesDiffTypes() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionPackagesDiffTypes");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionPackagesDiffTypes");
		}
	}

	@Test
	public void test_23_CommissionPackageNullPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionPackageNullPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionPackageNullPackageId");
		}
	}
	
	@Test
	public void test_24_CommissionPackageNullProductId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testCommissionPackageNullProductId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionPackageNullProductId");
		}
	}
	
	@Test
	public void test_25_CommissionDeviceIncorrectProductId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testCommissionDeviceIncorrectProductId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testCommissionDeviceIncorrectProductId");
		}
	}

	public void test_26_AlreadyCommissionedPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testAlreadyCommissionedPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/commissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAlreadyCommissionedPackageId");
		}
	}
	
	
	@Test
	public void test_30_DecomissionDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDecomissionDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/decommissionDevice")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDecomissionDevice");
		}
	}
	
	@Test
	public void test_31_DecomissionPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDecomissionPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/deCommissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDecomissionPackage");
		}
	}
	
	@Test
	public void test_40_Associate() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociate");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociate");
		}
	}
	
	@Test
	public void test_41_AssociateNullDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateNullDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateNullDeviceId");
		}
	}
	
	@Test
	public void test_42_AssociateNullPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateNullPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateNullPackageId");
		}
	}
	
	@Test
	public void test_43_AssociateInvalidDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateInvalidDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateInvalidDeviceId");
		}
	}

	@Test
	public void test_44_AssociateInvalidPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateInvalidPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateInvalidPackageId");
		}
	}
	
	@Test
	public void test_45_AlreadyAssociatedDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAlreadyAssociatedDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAlreadyAssociatedDevice");
		}
	}
	
	@Test
	public void test_46_AssociateWithDecommissionedDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateWithDecommissionedDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateWithDecommissionedDeviceId");
		}
	}
	
	@Test
	public void test_47_AlreadyAssociatedPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAlreadyAssociatedPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAlreadyAssociatedPackage");
		}
	}
	
	@Test
	public void test_48_AssociateWithDecommissionedPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateWithDecommissionedPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateWithDecommissionedPackageId");
		}
	}
	
	@Test
	public void test_50_DisAssociateNullDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateNullDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateNullDeviceId");
		}
	}

	@Test
	public void test_51_DisAssociateNullPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateNullPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateNullPackageId");
		}
	}
	
	@Test
	public void test_52_DisAssociateInvalidDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateInvalidDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateInvalidDeviceId");
		}
	}
	
	@Test
	public void test_53_DisAssociateInvalidPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateInvalidPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateInvalidPackageId");
		}
	}
	
	@Test
	public void test_54_DisAssociateCommissionedDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateCommissionedDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateCommissionedDeviceId");
		}
	}
	
	@Test
	public void test_55_DisAssociateDecommissionedDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateDecommissionedDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateDecommissionedDeviceId");
		}
	}
	
	@Test
	public void test_56_DisAssociateCommissionedPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateCommissionedPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateCommissionedPackageId");
		}
	}
	
	@Test
	public void test_57_DisAssociateWithDecommissionedPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateWithDecommissionedPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateWithDecommissionedPackageId");
		}
	}
	
	@Test
	public void test_58_DisAssociate() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociate");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociate");
		}
	}
	
	@Test
	public void test_59_DisAssociateOnDisAssociated() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDisAssociateOnDisAssociated");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/disassociate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDisAssociateOnDisAssociated");
		}
	}
	
	
	@Test
	public void test_60_DeviceStatusDisAssociatedDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeviceStatusDisAssociatedDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getDeviceStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testDeviceStatusDisAssociatedDevice");
		}
	}
	@Test
	public void test_61_PackageStatusDisAssociatedPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testPackageStatusDisAssociatedPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getPackageStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testPackageStatusDisAssociatedPackage");
		}
	}
	
	@Test
	public void test_62_AssociateDisassociateDevicePackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testAssociateDisassociateDevicePackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/associate")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testAssociateDisassociateDevicePackage");
		}
	}
	
	
	
	@Test
	public void test_70_DeComAssociatedDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeComAssociatedDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/decommissionDevice")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDeComAssociatedDevice");
		}
	}
	
	@Test
	public void test_71_DeComNullDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeComNullDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/decommissionDevice")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDeComNullDeviceId");
		}
	}
	
	@Test
	public void test_72_DeComInvalidDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeComInvalidDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/decommissionDevice")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDeComInvalidDeviceId");
		}
	}
	
	@Test
	public void test_73_DeComAssociatedPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeComAssociatedPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/deCommissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDeComAssociatedPackage");
		}
	}
	
	@Test
	public void test_74_DeComNullPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeComNullPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/deCommissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDeComNullPackageId");
		}
	}
	
	@Test
	public void test_75_DeComInvalidPackageId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeComInvalidPackageId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/deCommissionPackage")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testDeComInvalidPackageId");
		}
	}
	
	@Test
	public void test_80_DeviceStatusCommissionedDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeviceStatusCommissionedDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getDeviceStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testDeviceStatusCommissionedDevice");
		}
	}
	
	@Test
	public void test_81_DeviceStatusAssociateddDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeviceStatusAssociateddDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getDeviceStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testDeviceStatusAssociateddDevice");
		}
	}
	
	@Test
	public void test_82_DeviceStatusDeCommissionedDevice() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testDeviceStatusDeCommissionedDevice");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getDeviceStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testDeviceStatusDeCommissionedDevice");
		}
	}
	
	@Test
	public void test_83_PackageStatuCommissionedPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testPackageStatuCommissionedPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getPackageStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testPackageStatuCommissionedPackage");
		}
	}
	
	@Test
	public void test_84_PackageStatusDeCommissionedPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testPackageStatusDeCommissionedPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getPackageStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			assertEquals("Expected value : ", testCase.getResponseMessage(), json.getResponseMessage());
		}
		else{
			logger.info("Skipped execution of testPackageStatusDeCommissionedPackage");
		}
	}
	
	@Test
	public void test_85_PackageStatusAssociatedPackage() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {

		TestCase testCase = testCases.get("testPackageStatusAssociatedPackage");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getPackageStatus")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			SensorResponse json = mapper.readValue(response.getContentAsString(), SensorResponse.class);
			//If package is in associated state, device id is not null
			assertNotNull(json.getDeviceId());
		}
		else{
			logger.info("Skipped execution of testPackageStatusAssociatedPackage");
		}
	}
	
	
}
