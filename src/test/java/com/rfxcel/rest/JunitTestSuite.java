package com.rfxcel.rest;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;

@RunWith(Suite.class)
@Suite.SuiteClasses({
	TestAuthService.class, TestShipmentServices.class
})
public class JunitTestSuite {
}