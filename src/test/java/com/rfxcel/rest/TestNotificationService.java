package com.rfxcel.rest;

import static org.junit.Assert.assertEquals;

import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.TimeZone;

import javax.ws.rs.client.Client;
import javax.ws.rs.core.MediaType;

import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParseException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.jboss.resteasy.core.Dispatcher;
import org.jboss.resteasy.mock.MockDispatcherFactory;
import org.jboss.resteasy.mock.MockHttpRequest;
import org.jboss.resteasy.mock.MockHttpResponse;
import org.jboss.resteasy.plugins.server.resourcefactory.POJOResourceFactory;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import com.rfxcel.notification.service.NotificationService;
import com.rfxcel.notification.vo.NotificationResVO;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TestNotificationService {

	private static Logger logger = Logger.getLogger(TestNotificationService.class);
	private static HashMap<String, TestCase> testCases; 
	//	private static HashMap<String , Integer> headerColumnMapping;

	static Client client = null;
	static Dispatcher dispatcher = null;
	private static final String INPUT_FILE= "TestData.xlsx";

	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
		testCases = TestUtil.loadXlsTestData(INPUT_FILE, 1);
		dispatcher = MockDispatcherFactory.createDispatcher();
		POJOResourceFactory notService = new POJOResourceFactory(NotificationService.class);
		dispatcher.getRegistry().addResourceFactory(notService);
	}

	@AfterClass
	public static void tearDownAfterClass() throws Exception {
	}

	@Test
	public void test_10_GetNotificationsCountAllShipments() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotificationsCountAllShipments");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotificationCount")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotificationsCountAllShipments");
		}
	}

	@Test
	public void test_11_GetNotificationsCountForContainer() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotificationsCountForContainer");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotificationCount")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotificationsCountForContainer");
		}
	}
	
	@Test
	public void test_12_GetNotificationsCountNullDeviceId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotificationsCountNullDeviceId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotificationCount")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotificationsCountNullDeviceId");
		}
	}
	
	@Test
	public void test_13_GetNotificationsCountNullPackId() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotificationsCountNullPackId");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotificationCount")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotificationsCountNullPackId");
		}
	}
	
	@Test
	public void test_15_GetNotifCountMissingSearchType() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotifCountMissingSearchType");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotificationCount")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotifCountMissingSearchType");
		}
	}

	@Test
	public void test_16_GetNotifCountInvalidSearchType() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotifCountInvalidSearchType");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotificationCount")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotifCountInvalidSearchType");
		}
	}
	
	@Test
	public void test_20_GetNotificationsAllShipments() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotificationsAllShipments");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotifications")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotificationsAllShipments");
		}
	}
	
	@Test
	public void test_21_GetNotificationsForContainer() throws URISyntaxException, JsonParseException, JsonMappingException, IOException {
		TestCase testCase = testCases.get("testGetNotificationsForContainer");
		if(testCase != null){
			String payload = testCase.getData();

			MockHttpRequest request = MockHttpRequest.post("/sensor/getNotifications")
					.contentType(MediaType.APPLICATION_JSON).content(payload.getBytes());
			MockHttpResponse response = new MockHttpResponse();
			dispatcher.invoke(request, response);   
			ObjectMapper mapper = new ObjectMapper();
			 NotificationResVO json = mapper.readValue(response.getContentAsString(), NotificationResVO.class);
			assertEquals(json.getResponseMessage()+". Response status ", testCase.getResponseStatus(), json.getResponseStatus());
		}
		else{
			logger.info("Skipped execution of testGetNotificationsForContainer");
		}
	}
	
	//TODO need to add test cases that verifies count of results returned by directly firing query 
	
	@Ignore
	@Test
	public void testDateFormatting() {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		java.sql.Timestamp t= new java.sql.Timestamp(1434567889000l);
		if(t != null){
			String createdOn = sdf.format(t);
			System.out.println(createdOn);
		}
		/*for(String id :TimeZone.getAvailableIDs()){
		System.out.println(id);
		}*/
		System.out.println(TimeZone.getTimeZone("UTC"));
		System.out.println(TimeZone.getTimeZone("GMT").getRawOffset());
	}
	
	@Ignore
	@Test
	public void testTimeZoneOffset() throws ParseException {
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		sdf.setTimeZone(TimeZone.getTimeZone("IST"));
		String dateString = sdf.format(new Date());
		System.out.println(dateString);
		
		Date d = sdf.parse(dateString);
		System.out.println(d);
		TimeZone tz = TimeZone.getTimeZone("UTC");
		int off = tz.getOffset(d.getTime());
		
		System.out.println(off);
	}
	
	@Ignore
	@Test
	public void _testDateFormatting() {
		
		
	    SimpleDateFormat sdfGMT1 = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
	    sdfGMT1.setTimeZone(TimeZone.getTimeZone("GMT"));
	    SimpleDateFormat sdfGMT2 = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");
	    sdfGMT2.setTimeZone(TimeZone.getTimeZone("GMT"));

	    SimpleDateFormat sdfLocal1 = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss");
	    SimpleDateFormat sdfLocal2 = new SimpleDateFormat("yyyy.MM.dd HH:mm:ss z");

	    try {
	        Date d = new Date();
	        String s1 = d.toString();
	        System.out.println(s1);
	        String s2 = sdfLocal1.format(d);
	        System.out.println(s2);
	        
	        // Store s3 or s4 in database.
	        String s3 = sdfGMT1.format(d);
	        System.out.println(s3);
	        String s4 = sdfGMT2.format(d);
	        System.out.println(s4);
	        // Retrieve s3 or s4 from database, using LOCAL sdf.
	        String s5 = sdfLocal1.parse(s3).toString();
	        System.out.println(s5);
	        //EXCEPTION String s6 = sdfLocal2.parse(s3).toString();
	        String s7 = sdfLocal1.parse(s4).toString();
	        System.out.println(s7);
	        String s8 = sdfLocal2.parse(s4).toString();
	        System.out.println(s8);
	        // Retrieve s3 from database, using GMT sdf.
	        // Note that this is the SAME sdf that created s3.
	        Date d2 = sdfGMT1.parse(s3);
	        String s9 = d2.toString();
	        System.out.println(s9);
	        String s10 = sdfGMT1.format(d2);
	        System.out.println(s10);
	        String s11 = sdfLocal2.format(d2);
	        System.out.println(s11);
	    } catch (Exception e) {
	        e.printStackTrace();
	    }       
	}
	
	@Ignore	
	@Test
	public void testDateConv(){
		String date = "2017-01-15 00:40:00.0";
		SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		try {
			Date d = sdf.parse(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
