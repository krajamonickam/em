package com.rfxcel.rule;


import java.util.HashMap;

import org.junit.Test;

import com.rfxcel.rule.service.RuleEvaluationService;

public class TestRuleService {

	@Test
	public void testEvaluateRulesForSendumData() {
		HashMap<String, String> map = new HashMap<String, String>();
		map.put("valid", "TRUE");
		map.put("sid", "4384");
		map.put("statusTimeStamp", "2016-11-23 17:14:37");
		map.put("cycleCount", "10");
		map.put("capacityRemaining", "35");
		map.put("temperature", "38.12");
		map.put("hepe", "64");
		map.put("capacityFull", "=4000.56");
		map.put("humidity", "45");
		map.put("cumulativeLog", "-387.7770718234");
		map.put("voltage", "4178");
		map.put("capacityRemaining", "35");
		map.put("notifyLimit", "STOP");
		map.put("battery", "0.93");
		map.put("growthLog", "0");
		map.put("longitude", "87.94426");
		map.put("sensorRecordId", "1479900935925");
		map.put("rssi", "-86");
		map.put("latitude", "41.98146");
		map.put("deviceIdentifier", "99000512000353");
		map.put("light", "62.25");
		map.put("fixType", "MSAL");
		
		RuleEvaluationService.evaluateRules(map);
	}

}
