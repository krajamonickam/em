package com.rfxcel.rule;


import org.junit.Test;

import com.rfxcel.rule.dao.RuleDAO;

public class TestRuleDAO {

	@Test
	public void testGetRulesForADeviceType() {
		System.out.println("Device Type Id : "+RuleDAO.getRulesForADeviceType(101));
	}

	@Test
	public void testGetDeviceType() {
		System.out.println("Device Type Id : "+RuleDAO.getDeviceType("99000512000353"));
	}

	@Test
	public void testGetRules() {
		System.out.println("Rules per Device are as : " + RuleDAO.getRules());
	}

}
