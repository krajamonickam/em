/**
 * 
 */
package com.rfxcel.notification.service;


import java.util.HashMap;

import org.junit.Test;

import com.rfxcel.notification.dao.NotificationDAO;
import com.rfxcel.notification.vo.AlertDetailsVO;
import com.rfxcel.notification.vo.NotificationVO;

/**
 * @author Tejshree Kachare
 *
 */
public class NotificationServiceTest {

	/**
	 * Test method to test getAlertDetails from NotificationDAO
	 */
	@Test
	public void testGetAlertDetails(){
		AlertDetailsVO alertDetailsVO = new AlertDetailsVO();
		alertDetailsVO.setAlertCode(3042L);		//Set the value from alert table
		alertDetailsVO = NotificationDAO.getAlertDetails(alertDetailsVO);
		System.out.println(alertDetailsVO.toString());
	}
	
	/**
	 * Test method to test CreateNotification from NotificationService for sensor data
	 */
	@Test
	public void testCreateNotification() {
		NotificationVO notificationVO = new NotificationVO();
		AlertDetailsVO alertDetailsVO = new AlertDetailsVO();
		alertDetailsVO.setAlertCode(3042L);		//Set the value from alert table
		notificationVO.setAlertDetails(alertDetailsVO);
		notificationVO.setDeviceId("900051200035");
		notificationVO.setRuleId(1L);
		NotificationService.createNotification(notificationVO);
	}
	
	/**
	 * Test method to test CreateNotification from NotificationService for sensor Alert
	 */
	@Test
	public void testCreateAlertNotification() {
		NotificationVO notificationVO = new NotificationVO();
		AlertDetailsVO alertDetailsVO = new AlertDetailsVO();
		alertDetailsVO.setAlertCode(3701L);		//Set the value from alert table
		notificationVO.setAlertDetails(alertDetailsVO);
		notificationVO.setDeviceId("900051200035");
		notificationVO.setRuleId(null);		//rule Id is null for alarm data
		NotificationService.createNotification(notificationVO);
	}

	/**
	 * Test method to test ProcessAlertToRaiseNotification from NotificationService for sensor Alert data
	 */
	@Test
	public void testProcessAlertToRaiseNotification(){
		HashMap <String, String> map = new HashMap<String, String>();
		//{longitude=-71.272423, geofenceName=Route, sensorRecordId=1479901688283, latitude=42.394983, geofenceEvent=EXIT, deviceIdentifier=99000512000353, alarmTimeStamp=2016-11-23 19:13:20}
		map.put("deviceIdentifier", "99000512000353");
		map.put("sensorRecordId", "1479901428084");
		map.put("longitude", "-71.272423");
		map.put("geofenceName", "Route");
		map.put("latitude", "42.394983");
		map.put("geofenceEvent", "EXIT");
		map.put("alarmTimeStamp", "2016-11-23 19:13:20");
		
		NotificationService.processAttributeAlerts(map);
	}
	
	
	
}
