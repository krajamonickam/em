--------------------------------------- ITT 2.3 Migration starts -----------------------------------------------------
ALTER table sensor_device_log modify column attribute_value varchar(45) DEFAULT NULL;

INSERT INTO `sensor_default_alerts` values
(null, 3706,'GeoFence Alert - Shipment On Route ','GeoFence Alert -  Shipment on route, currently at <<ATTR>>.\r\n','location',1);



-- added on 9/28 for getting menu list by user type

delete from rfx_menus;

ALTER TABLE `rfx_menus` DROP INDEX `unique_index`,
 ADD UNIQUE INDEX `unique_index` USING BTREE(`menu_code`, `org_id`, `min_user_type`);
 
INSERT INTO `rfx_menus` VALUES
(1, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 2),
(2, 2, 'app.profile', 'Devices', 2, 0, 2),
(3, 3, 'app.products', 'Shipments', 3, 0, 2),
(4, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 2),
(5, 5, 'app.organization', 'Organization', 5, 0, 2),
(6, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 1),
(7, 2, 'app.profile', 'Devices', 2, 0, 1),
(8, 3, 'app.products', 'Shipments', 3, 0, 1),
(9, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 1),
(10, 5, 'app.organization', 'Organization', 5, 0, 1),
(11, 6, 'app.settings', 'Settings', 6, 0, 1),
(12, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 1),
(13, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 0),
(14, 2, 'app.profile', 'Devices', 2, 0, 0),
(15, 3, 'app.products', 'Shipments', 3, 0, 0),
(16, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 0),
(17, 5, 'app.organization', 'Organization', 5, 0, 0),
(18, 6, 'app.settings', 'Settings', 6, 0, 0),
(19, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 0),
(20, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 3),
(21, 2, 'app.profile', 'Devices', 2, 0, 3),
(22, 3, 'app.products', 'Shipments', 3, 0, 3),
(23, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 3),
(24, 5, 'app.organization', 'Organization', 5, 0, 3),
(25, 6, 'app.settings', 'Settings', 6, 0, 3),
(26, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 3);

-- 28 Sept 2017, ITT-452 - Insert default System Organization(System-org).
ALTER TABLE rfx_org AUTO_INCREMENT = 0;
DELETE FROM rfx_org WHERE org_name = 'System-org';
INSERT INTO rfx_org(org_id, org_name, short_name, email, org_extid, active, org_tz, tz_id)
VALUES(0, 'SYSTEM', 'SYS', 'arunrao@rfxcel.com', 123456, 1, '-07:00', 2);
UPDATE rfx_org SET org_id=0 WHERE org_name='SYSTEM';

-- build#15 , added on 10/04 for new attribute types of geofence

delete from sensor_attributes where alert_code in (3701,3702,3703,3704,3705,3706);
insert into sensor_attributes (attr_name, is_configurable, alert_code, unit, range_min, range_max)
values ('geofenceEntry', 0, 3706, '', 0.00000, 100.00000),
('geofenceExit', 0, 3704, '', 0.00000, 100.00000),
('geofenceOnRoute', 0, 3703, '', 0.00000, 100.00000),
('geofenceArrival', 0, 3701, '', 0.00000, 100.00000),
('geofenceDeparture', 0, 3702, '', 0.00000, 100.00000),
('samelocation', 0, 3705, '', 0.00000, 100.00000);

-- 06 Oct 2017, migration script which should execute only once. It will migrate/add the default missing alert types for all the existing active organization
INSERT INTO sensor_alert_types(alert_code, alert_msg, alert_detail_msg, org_id)
SELECT alert_code, alert_msg, alert_detail_msg, org.org_id FROM sensor_default_alerts alert, rfx_org org WHERE alert.is_default=1 
AND alert.alert_code NOT IN(SELECT alert_code FROM sensor_alert_types WHERE org_id=org.org_id) AND org.active=1 ORDER BY org.org_id;

-- 06 Oct 2017, elastic search parameters
DELETE FROM system_config WHERE param_name IN ('elasticsearch.data.fetch.size','elasticsearch.host.name','elasticsearch.index.name','elasticsearch.transport.tcpport','elasticsearch.data.fetch');
INSERT INTO system_config VALUES
('elasticsearch.data.fetch.size','30000','Elastic Search Fetch Size ',1),
('elasticsearch.host.name','127.0.0.1','Elastic Search Host Name ',1),
('elasticsearch.index.name','coldchainlight','Elastic Search Index Name ',1),
('elasticsearch.transport.tcpport','9300','Elastic Search Transport TCP Port ',1),
('elasticsearch.data.fetch','false','Elastic Search Fetch Data', 1);

-- 09 Oct 2017, Audit log table
CREATE TABLE `audit_log` (
`id` BIGINT UNSIGNED NOT NULL AUTO_INCREMENT,
`user_id` BIGINT DEFAULT NULL,
`user_name` VARCHAR(50) DEFAULT NULL,
`entity_type` VARCHAR(50) DEFAULT NULL,
`entity_id` BIGINT DEFAULT NULL,
`action` VARCHAR(50) DEFAULT NULL COMMENT 'Create, Update ,Delete',
`old_value` TEXT,
`new_value` TEXT,
`description` TEXT,
`org_id` bigint(20) NOT NULL,
`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`)
) ENGINE=INNODB;

--------------------------------------- ITT 2.3 Migration ends -----------------------------------------------------