-- ITT 2.6 Migration starts --
INSERT INTO `system_config` (`param_name`, `param_value`, `display_name`)  VALUES 
    ('application.shard', '0', 'Shard for this node');
	
INSERT INTO `org_config` (`param_name`, `param_value`, `display_name`, `org_id`) 
VALUES
('kirsen.user', 'kirsen','Kirsen User Name', '0'),
('kirsen.password', 'kirsen123','Kirsen Password','0');


ALTER TABLE `rfx_users`
	ADD COLUMN `shard` INT(6) NOT NULL DEFAULT '0' AFTER `group_id`,
	ADD COLUMN `user_passwd512` VARCHAR(255) AFTER `user_passwd`
;

REPLACE INTO `org_config` (`param_name`, `param_value`, `display_name`, `org_id`) 
VALUES
('system.clicktochat', 'false', 'POC click to chat functionality','0');

REPLACE INTO org_config (param_name, param_value, display_name, org_id, active)
VALUES ('kirsen.enablecell','true','Enable cell location for kirsen',0,1);

ALTER TABLE `sensor_attributes` 
ADD COLUMN `round_digits` TINYINT(4) AFTER `sensor_attribute`;

ALTER TABLE `rfx_users`
	ADD INDEX `index_user_login` (`user_login`);
	

UPDATE `sensor_attributes` SET `round_digits`='1' WHERE  `attr_name`= 'temperature';
UPDATE `sensor_attributes` SET `round_digits`='0' WHERE  `attr_name`= 'pressure';
UPDATE `sensor_attributes` SET `round_digits`='1' WHERE  `attr_name`= 'humidity';
UPDATE `sensor_attributes` SET `round_digits`='0' WHERE  `attr_name`= 'light';
UPDATE `sensor_attributes` SET `round_digits`='0' WHERE  `attr_name`= 'tilt';
UPDATE `sensor_attributes` SET `round_digits`='5' WHERE  `attr_name`= 'latitude';
UPDATE `sensor_attributes` SET `round_digits`='5' WHERE  `attr_name`= 'longitude';
UPDATE `sensor_attributes` SET `round_digits`='2' WHERE  `attr_name`= 'shock';
UPDATE `sensor_attributes` SET `round_digits`='0' WHERE  `attr_name`= 'battery';
UPDATE `sensor_attributes` SET `round_digits`='2' WHERE  `attr_name`= 'vibration';	

UPDATE `system_config` SET `param_value`='2.6' WHERE  `param_name`='build.version';
-- ITT 2.6 Migration ends --