-- ITT 2.51 Migration starts --
ALTER TABLE `sensor_notifications`
	ADD COLUMN `state` INT(1) NOT NULL DEFAULT '0' AFTER `org_id`;
	
UPDATE `rfx_menus` SET `menu_name`='Logs' WHERE `menu_name`='Device Logs';
	
UPDATE `system_config` SET `param_value`='2.51' WHERE  `param_name`='build.version';
-- ITT 2.5 Migration ends --