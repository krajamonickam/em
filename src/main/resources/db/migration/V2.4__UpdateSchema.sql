-- ITT 2.4 Migration starts --
UPDATE `system_config` SET `param_value`='2.4' WHERE  `param_name`='build.version';

DROP TABLE IF EXISTS `alert_email_id_mapping`;
CREATE  TABLE `alert_email_id_mapping` (
   `id` INT(20) NOT NULL AUTO_INCREMENT ,
   `alert_code` BIGINT(20) UNSIGNED NOT NULL,
   `attr_name` VARCHAR(100) NOT NULL, 
   `profile_id` int(10) NOT NULL,
   `email_id` varchar(100) NOT NULL,
    `org_id` bigint(20) NOT NULL, 
   `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
DROP TABLE IF EXISTS `profile_alert_types`;
CREATE TABLE  `profile_alert_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  `alert_code` BIGINT(20) UNSIGNED NOT NULL,
  `alert_detail_msg` varchar(300) NOT NULL,
  `attr_name` varchar(45) NOT NULL,
  `profile_id` int(10) NOT NULL,
  `org_id` bigint(20) NOT NULL, 
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `profile_alert_group_mapping`;
CREATE  TABLE `profile_alert_group_mapping` (
   `id` INT(20) NOT NULL AUTO_INCREMENT ,
   `alert_code` BIGINT(20) UNSIGNED NOT NULL,
   `attr_name` varchar(45) NOT NULL,
   `profile_id` int(10) NOT NULL,
   `alert_group_id` BIGINT(20) UNSIGNED NOT NULL,
   `org_id` bigint(20) NOT NULL, 
   `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
  ) ENGINE=InnoDB DEFAULT CHARSET=utf8;
  
  INSERT INTO `org_config` VALUES 
('new.profile.screen','false','Is New Profile Screen activated', 0, 1, 0);

DELETE FROM system_config WHERE param_name IN ('elasticsearch.data.fetch.size','elasticsearch.host.name','elasticsearch.index.name','elasticsearch.data.fetch','elasticsearch.http.port');
INSERT INTO system_config VALUES
('elasticsearch.data.fetch.size','100','Elastic Search Fetch Size ',1),
('elasticsearch.host.name','127.0.0.1','Elastic Search Host Name ',1),
('elasticsearch.index.name','coldchainlight','Elastic Search Index Name ',1),
('elasticsearch.data.fetch','false','Elastic Search Fetch Data', 1),
('elasticsearch.http.port','9200','Elastic Search Rest API Http Port', 1);

ALTER table sensor_threshold modify column `alert` INT(2) DEFAULT 0 COMMENT '0 - No Alert No Notification , 1 -  Alert and Notification, 2 - Notification';

update sensor_threshold set alert =
 CASE 
	WHEN alert = 0 AND notification =0 THEN 1
	WHEN alert = 1 AND notification =1 THEN 1
    WHEN alert = 0 AND notification =1 THEN 2
    WHEN alert = 1 AND notification =0 THEN 1
END;

ALTER table sensor_threshold drop column `notification`;

ALTER TABLE `sensor_geopoints` CHANGE COLUMN `is_home` `location_type` INT(2) UNSIGNED NOT NULL DEFAULT 3;

ALTER TABLE device_log 
ADD COLUMN `org_id` BIGINT(20) NOT NULL;
UPDATE device_log dl INNER JOIN sensor_devices sd ON dl.device_id=sd.device_id SET dl.org_id=sd.org_id;

DELETE FROM system_config WHERE param_name ='alert.devicelog.search.enable';
INSERT INTO system_config VALUES
('alert.devicelog.search.enable','true','Alert And Device Log Search(true/false)', 1);

ALTER TABLE sensor_attr_type ADD COLUMN `sensor_attribute` TINYINT NOT NULL;
ALTER TABLE sensor_attributes ADD COLUMN `sensor_attribute` TINYINT NOT NULL;

UPDATE sensor_attributes SET sensor_attribute =1 where attr_name IN('temperature', 'pressure', 'humidity','light','tilt','shock','battery','vibration');
UPDATE sensor_attr_type SET sensor_attribute =1 where attr_name IN('temperature', 'pressure', 'humidity','light','tilt','shock','battery','vibration');
-- ITT 2.4 Migration ends --