-- ITT 2.81 Migration starts --

ALTER TABLE `sensor_frequency` MODIFY `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP;

UPDATE `system_config` SET `param_value`='2.81' WHERE  `param_name`='build.version';
-- ITT 2.81 Migration ends --