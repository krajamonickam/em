-- ITT 2.7 Migration starts --
ALTER table sensor_devices add column `activation_status` INT(1) DEFAULT 1 COMMENT '0 - pending , 1 completed'; 

REPLACE INTO `system_config`(`param_name`, `param_value`,`display_name`,`active`) VALUES
('create.password.text.body',' With Verizon Cold Chain, quickly track products, manage devices, and access alerts.#Please verify your email addresss by clicking the following link:# Please contact support if you have any issues with your account.', 'Set Password text Body', 1),
('reset.password.text.body',' With Verizon Cold Chain, quickly track products, manage devices, and access alerts.#Please reset password by clicking the following link:# Please contact support if you have any issues with your account.', 'ReSet Password text body', 1)
;

UPDATE `system_config` SET `param_value`='2.7' WHERE  `param_name`='build.version';
-- ITT 2.7 Migration ends --