-- ITT 2.61 Migration starts --

ALTER TABLE `sensor_alert_types` MODIFY COLUMN `alert_detail_msg` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;
ALTER TABLE `profile_alert_types` MODIFY COLUMN `alert_detail_msg` VARCHAR(1000) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL;

ALTER TABLE `sensor_attributes` ADD COLUMN `alert_on_reset` TINYINT(4) UNSIGNED NOT NULL DEFAULT 1 COMMENT '0 - Do not alert on reset , 1 - Alert on reset' AFTER `round_digits`;

ALTER TABLE `sensor_attr_type` ADD COLUMN `alert_on_reset` TINYINT(2) NOT NULL DEFAULT 1 COMMENT '\'0 - Do not alert on reset , 1 - Alert on reset\'' AFTER `org_id`;

-- trigger to copy default attribute settings automatically to device attributes table.
DROP TRIGGER IF EXISTS `attributeSettings`;
DELIMITER $$
CREATE TRIGGER `attributeSettings` AFTER UPDATE ON sensor_attributes FOR EACH ROW
BEGIN

    IF (NEW.alert_on_reset <> OLD.alert_on_reset) THEN
      update sensor_attr_type set alert_on_reset = NEW.alert_on_reset where attr_name = OLD.attr_name;
    END IF;

    IF (NEW.round_digits <> OLD.round_digits) THEN
      update sensor_attr_type set round_digits = NEW.round_digits where attr_name = OLD.attr_name;
    END IF;

    IF (NEW.unit <> OLD.unit) THEN
      update sensor_attr_type set unit = NEW.unit where attr_name = OLD.attr_name;
    END IF;

    IF (NEW.range_max <> OLD.range_max) THEN
      update sensor_attr_type set range_max = NEW.range_max where attr_name = OLD.attr_name;
    END IF;

    IF (NEW.range_min <> OLD.range_min) THEN
      update sensor_attr_type set range_min = NEW.range_min where attr_name = OLD.attr_name;
    END IF;


END $$
DELIMITER ;

-- disable reset alert for shock
update sensor_attributes set alert_on_reset = 0 where attr_name ='shock';

UPDATE `system_config` SET `param_value`='2.61' WHERE  `param_name`='build.version';
-- ITT 2.6 Migration ends --