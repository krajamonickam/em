-- ITT 2.8.9 Migration starts --
ALTER TABLE `rfx_users`
	ADD COLUMN `privilege` SMALLINT(1) NOT NULL DEFAULT '0' AFTER `tz_id`;
	
DROP PROCEDURE IF EXISTS `resetOrg`;
DELIMITER $$
CREATE PROCEDURE `resetOrg`(
	IN `orgID` VARCHAR(10)
)
BEGIN
	delete from `sensor_notifications` where `org_id` = orgID;
	delete from `device_log` where `device_id` in (select `device_id` from `sensor_devices` where `org_id` = orgID);
	delete from `sensor_device_log` where `device_id` in (select `device_id` from `sensor_devices` where `org_id` = orgID);
	delete from `sensor_excursion_log` where `org_id` = orgID;
	delete from `sensor_data` where `device_id` in (select `device_id` from `sensor_devices` where `org_id` = orgID);
	delete from `rfx_users` where `org_id` = orgID and (`user_email` not like '%verizon%') and (`user_email` not like '%rfxcel%');
	delete from `sensor_geopoints` where `profile_id` in (select `id` from `sensor_profile` where `org_id` = orgID);
	delete from `sensor_threshold` where `org_id` = orgID;
	delete from `sensor_profile` where `org_id` = orgID;
	delete from `sensor_associate` where `org_id` = orgID;
	delete from `sensor_frequency` where `org_id` = orgID;
	delete from `sensor_package` where `org_id` = orgID;
	delete from `sensor_profile` where `org_id` = orgID;
	delete from `sensor_threshold` where `org_id` = orgID;
	delete from `trans_history` where `org_id` = orgID;
	delete from `audit_log` where `org_id` = orgID;
END$$
DELIMITER ;

UPDATE `system_config` SET `param_value`='2.8.9' WHERE  `param_name`='build.version';
-- ITT 2.8.9 Migration ends --