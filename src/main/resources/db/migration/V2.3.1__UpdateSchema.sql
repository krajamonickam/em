--------------------------------------- ITT 2.3 Migration starts -----------------------------------------------------
-- 13 Oct 2017, Change the unit for vibration
update `sensor_attributes` set unit = 'g/hz�' where attr_name = 'vibration';
update `sensor_attr_type` set unit = 'g/hz�' where attr_name = 'vibration';
-- 17 Oct 2017, Release version is 2.3
UPDATE `system_config` SET `param_value`='2.31' WHERE  `param_name`='build.version';
--------------------------------------- ITT 2.3 Migration ends -----------------------------------------------------