 
--------------------------------------- ITT 2.1 Migration starts-----------------------------------------------------
delete from system_config where param_name in ('kirsen.config.template');
INSERT INTO `system_config`(`param_name`, `param_value`,`display_name`,`active`) VALUES
('kirsen.config.template','{@temp_min@:-200.0,@temp_max@:200.0,@temp_mode@:@transfer@,@communication_period@:<profile.comm.freq>,@gps_period@:<profile.gps.freq>,@temperature_period@:<profile.attr.freq>}','Kirsen Configuration Template', 1)
;

ALTER TABLE `sensor_package`  
	DROP KEY `package_product_id`,
 	ADD UNIQUE KEY `package_product_id`  (`org_id`,`package_id`,`product_id`) ;
 	
DELETE FROM `system_config` WHERE (param_name in ('create.password.text.body','reset.password.text.body') );
INSERT INTO `system_config`(`param_name`, `param_value`,`display_name`,`active`) VALUES
('create.password.text.body',' With Verizon Intelligent Track & Trace, quickly track products, manage devices, and access alerts.#Please verify your email addresss by clicking the following link:# Please contact support if you have any issues with your account.', 'Set Password text Body', 1),
('reset.password.text.body',' With Verizon Intelligent Track & Trace, quickly track products, manage devices, and access alerts.#Please reset password by clicking the following link:# Please contact support if you have any issues with your account.', 'ReSet Password text body', 1)
;


delete from org_config where param_name in ('sensor.data.copyfields','dashboard.map.center.latitude','dashboard.map.center.longitude','dashboard.map.zoom','dashboard.map.type');
INSERT INTO `org_config` VALUES
('sensor.data.copyfields','','List of Fields to Propagate', 0, 1, 0),
('dashboard.map.center.latitude', '40', 'Dashboard Map Center (latitude)', 0, 1, 0),
('dashboard.map.center.longitude', '-95', 'Dashboard Map Center (longitude)', 0, 1, 0),
('dashboard.map.zoom', '6', 'Dashboard Map Zoom Factor', 0, 1, 0),
('dashboard.map.type', 'google.maps.MapTypeId.HYBRID', 'Dashboard Map Type', 0, 1, 0);
-- added on 7/27 for identifying home geo location
ALTER TABLE `sensor_geopoints` ADD COLUMN `is_home` INT(2) UNSIGNED NOT NULL DEFAULT 0 AFTER `radius`;
ALTER TABLE `sensor_associate` DROP COLUMN `geopoint_name` ;

-- Table to capture Excursion at an attribute level
CREATE TABLE `sensor_excursion_log` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(45) NOT NULL,
  `container_id` varchar(255) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `attribute` varchar(45)NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;

delete from system_config where param_name ='build.version';
insert into system_config values ('build.version','v2.1','Build Version Number',0);

delete from system_config where param_name ='build.version';
insert into system_config values ('build.version','v2.1','Build Version Number',0);

DELETE FROM system_config WHERE param_name = 'queclink.config.template';
INSERT INTO system_config VALUES
('queclink.config.template', 'AT+GTRTO=gl300vc,4,,,,,,FFFF$AT+GTSRI=gl300vc,2,,1,<queclink.device.url>,<queclink.device.port>,216.218.206.20,6880,,0,0,0,,,,FFFF$AT+GTFKS=gl300vc,1,1,3,1,1,3,3,10,3,3,FFFF$AT+GTCFG=gl300vc,gl300vc,GL300VC,0,0.0,1,10,1F,,,823,0,1,1,<profile.attr.freq>,0,0,20491231235959,1,0,,FFFF$AT+GTTMA=gl300vc,+,0,0,0,,0,,,,FFFF$AT+GTFRI=gl300vc,1,0,,,0000,0000,<profile.gps.freq>,<profile.gps.freq>,,9999,9999,,1000,1000,0,5,50,5,0,0,FFFF$AT+GTTEM=gl300vc,0,-20,60,900,3600,2,,,,,,FFFF$AT+GTTPR=gl300vc,0,<profile.attr.freq>,<profile.comm.count>,1,,,,,FFFF$ AT+GTDOG=gl300vc,0,60,30,0200,,1,0,0,360,360,1,FFFF$AT+GTPDS=gl300vc,1,FB,,,,,,,FFFF$AT+GTRTO=gl300vc,1,,,,,0,FFFF$AT+GTDAT=gl300vc,0,,Device <deviceid> setup 2.1 completed,1,,,,FFFF$', 'Queclink Device Configuration Template', 1)
;

delete from org_config where param_name in ('sensor.data.copyexpiry');
INSERT INTO `org_config` VALUES
('sensor.data.copyexpiry','0','Seconds to Expire Propagation Data', 0, 1, null)
;

-- 18th Aug 2017, Table to manage menus with menu_code
DROP TABLE IF EXISTS `rfx_menus`;
CREATE TABLE `rfx_menus` (
`id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`menu_code` INT(20) NOT NULL,
`state` VARCHAR(200) NOT NULL,
`menu_name` VARCHAR(40) NOT NULL,
`order` INT(10) NOT NULL,
`org_id` BIGINT(20) DEFAULT 0,
`min_user_type` int(2) unsigned DEFAULT '2' COMMENT '0-System, 1-Admin, 2-Normal',
PRIMARY KEY (`id`),
UNIQUE KEY `unique_index` (`menu_code`,`org_id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

-- 23rd Aug 2017, JIRA:ITT-374-Changed menu name from "Profile" to "Device"
delete from rfx_menus;
INSERT INTO `rfx_menus` VALUES
(1, 1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 2),
(2, 2, 'app.profile', 'Devices', 2, 0, 2),
(3, 3, 'app.products', 'Shipments', 3, 0, 2),
(4, 4, 'app.dashboard.alert', 'Alerts', 4, 0, 2),
(5, 5, 'app.organization', 'Organization', 5, 0, 2),
(6, 6, 'app.settings', 'Settings', 6, 0, 1),
(7, 7, 'app.deviceLogs', 'Device Logs', 7, 0, 1);

delete from system_config where param_name like 'thinkspace%';
delete from system_config where param_name like 'thingspace%';
INSERT INTO system_config VALUES
('thingspace.integration.url','https://preprodiwk.thingspace.verizon.com/api/v2/devices','ThingSpace Integration Url',1),
('thingspace.integration.retry.count','3','ThingSpace Integration retry count for request',1),
('thingspace.integration.get.interval','1','ThingSpace Integration get request interval (minute))',1),
('thingspace.logFolder','c:\\thingspaceLog','ThingSpace Log Folder',1),
('thingspace.poll.service.start','true','Start ThingSpace poll service (true/false)',1);

delete from org_config where param_name in ('thinkspace.auth.token','thingspace.auth.token','date.format');
INSERT INTO `org_config` VALUES 
('thingspace.auth.token','','Think space authorization token', 0, 1, null),
('date.format','yyyy-MM-dd HH:mm:ss','Date Format', 0, 1, null);

-- 22nd Aug 2017, added gps_com_freq_limit, receive_com_freq_limit, send_com_freq_limit in table sensor_device_types*/
ALTER TABLE sensor_device_types 
ADD COLUMN gps_com_freq_limit INT(20) DEFAULT 86400 COMMENT 'frequency limit unit is seconds';

ALTER TABLE sensor_device_types 
ADD COLUMN receive_com_freq_limit INT(20) DEFAULT 86400 COMMENT 'frequency limit unit is seconds';

ALTER TABLE sensor_device_types 
ADD COLUMN send_com_freq_limit INT(20) DEFAULT 86400 COMMENT 'frequency limit unit is seconds';

delete from system_config where param_name in ('email.disclaimer');
INSERT INTO system_config VALUES
('email.disclaimer','Recommended browser is Chrome version 58 and above.','Email Disclaimer ',1);

-- 29 Aug for Locate now support for Queclink
delete from system_config where param_name in ('queclink.config.locate.cmd');
INSERT INTO system_config VALUES
('queclink.config.locate.cmd','AT+GTRTO=gl300vc,1,,,,,,FFFF$','Queclink Locate now command',1);


ALTER TABLE `rfx_org` MODIFY COLUMN `email` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL;
ALTER TABLE `rfx_users` MODIFY COLUMN `user_email` VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci DEFAULT NULL,
MODIFY COLUMN `last_login` DATETIME;

--------------------------------------- ITT 2.1 Migration ends -----------------------------------------------------
