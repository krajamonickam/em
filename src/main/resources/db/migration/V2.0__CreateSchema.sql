SET FOREIGN_KEY_CHECKS=0;
CREATE TABLE  `sensor_associate` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `parent_id` varchar(255) NOT NULL,
  `child_id` varchar(45) NOT NULL,
  `create_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_datetime` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `relation_status` int(10) unsigned NOT NULL COMMENT '0 - Not associated ,1 - Associated',
  `excursion_status` int(10) unsigned DEFAULT '0' COMMENT '0-Green, 1 -yellow, 2 - Red',
  `add_favorite` int(10) unsigned DEFAULT '0' COMMENT '0-Not favorite, 1 -Favorite',
  `user_id` varchar(45) DEFAULT NULL,
  `zoom_factor` int(10) unsigned DEFAULT '7',
  `center_latitude` varchar(50) DEFAULT NULL,
  `center_longitude` varchar(50) DEFAULT NULL,
  `demo_flag` int(4) DEFAULT 0 COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
  `geopoint_name` varchar(45) DEFAULT NULL,
  `org_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `profile_id`int(10) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sensor_attr_type` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `attr_id` bigint(20) NOT NULL,
  `device_type_id` bigint(20) NOT NULL,
  `attr_name` varchar(45) NOT NULL,
  `scope` int(10) unsigned NOT NULL DEFAULT '0',
  `factor` decimal(20,0) DEFAULT NULL,
  `legend` varchar(255) DEFAULT NULL,
  `excursion_duration` int(2) unsigned DEFAULT '0' COMMENT '0 - Dont calculate time duration, 1- Minimum Duration, 2-Maximum Duration, 3-Minimum and Maximum Duration',
  `multiply_factor` decimal(20,4) DEFAULT '1.0000',
  `round_digits` int(4) DEFAULT NULL,
  `notify_limit` int(2) unsigned DEFAULT '0' COMMENT '0- always , other then 0 will limit the notification',
  `unit` varchar(15) DEFAULT NULL,
  `show_graph` TINYINT(1) NOT NULL COMMENT '0 - Do not show graph, 1 - Show graph for this attribute',
  `range_min` decimal(10,5) NOT NULL,
  `range_max` decimal(10,5) NOT NULL,
 `demo_flag` int(4) DEFAULT 0 COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
 `filter_value` int(4) DEFAULT NULL,
 `org_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sensor_bacterial_growth_log` (
  `growth_log_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(45) NOT NULL,
  `fahrenheit` int(11) DEFAULT NULL,
  `growth_log` double DEFAULT NULL,
  `log_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `start_time` timestamp NOT NULL,
  `end_time` timestamp NOT NULL,
  PRIMARY KEY (`growth_log_id`),

  KEY `Index_deviceid` (`device_id`)
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;


CREATE TABLE  `sensor_data` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `device_id` varchar(45) NOT NULL,
  `container_id` varchar(255) DEFAULT NULL,
  `temperature` varchar(100) DEFAULT NULL,
  `pressure` varchar(100) DEFAULT NULL,
  `humidity` varchar(100) DEFAULT NULL,
  `light` varchar(100) DEFAULT NULL,
  `tilt` varchar(100) DEFAULT NULL,
  `battery_rem` decimal(10,2) unsigned DEFAULT NULL,
  `growth_log` varchar(100) DEFAULT NULL,
  `cumulative_log` varchar(100) DEFAULT NULL,
  `latitude` varchar(50) DEFAULT NULL,
  `longitude` varchar(50) DEFAULT NULL,
  `position_error` varchar(100) DEFAULT NULL,
  `fix_type` varchar(100) DEFAULT NULL,
  `fix_valid` varchar(100) DEFAULT NULL,
  `temp_probe` varchar(100) DEFAULT NULL,
  `status_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `capacity_rem` decimal(10,2) unsigned DEFAULT NULL,
  `voltage` decimal(10,2) unsigned DEFAULT NULL,
  `cycle_count` varchar(100) DEFAULT NULL,
  `signal_strength` varchar(100) DEFAULT NULL,
  `cell_sys_id` varchar(100) DEFAULT NULL,
  `ambient_temp` varchar(100) DEFAULT NULL,
  `excursion_status` int(2) unsigned DEFAULT '0' COMMENT '0-Green, 1 -yellow, 2 - Red',
  `excursion_attributes` varchar(400) DEFAULT NULL,
  `alarm_type` varchar(100) DEFAULT NULL,
  `alarm_value` varchar(45) DEFAULT NULL,
  `geo_fence_no` varchar(45) DEFAULT NULL,
  `geo_fence_event` varchar(45) DEFAULT NULL,
  `geo_fence_name` varchar(100) DEFAULT NULL,
  `is_alert` tinyint(1) DEFAULT '0' COMMENT '0 - sensor data , 1 sensor alert',
  `demo_flag` int(4) DEFAULT '0' COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
  PRIMARY KEY (`id`),
  UNIQUE KEY `Unique_Key_deviceId_containerId_statusTime` (`device_id`,`container_id`,`status_time`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;



CREATE TABLE `sensor_device_log` (
  `log_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `device_id` varchar(45) NOT NULL,
  `attribute_name` varchar(100) NOT NULL,
  `attribute_value` int(11) DEFAULT NULL,
  `start_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `end_time` timestamp DEFAULT CURRENT_TIMESTAMP,
  `device_log_type` int(10) unsigned DEFAULT NULL COMMENT '0- Excurstion log,2- Notification limit log',
  `notify_count` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`log_id`),

  KEY `Index_deviceid` (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sensor_device_types` (
  `device_type_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `device_type` varchar(45) NOT NULL,
  `device_manufacturer` varchar(45) DEFAULT NULL,
  `org_id` bigint(20) NOT NULL,
  `active` TINYINT(1) DEFAULT 1 COMMENT '0 - Inactive , 1 Active',
  PRIMARY KEY (`device_type_id`),
  KEY `Index_device_type_id` (`device_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `sensor_device_types` VALUES
(103,'Kirsen 4GLTEVT','Kirsen',0,1),
(104,'Verizon VZCATM1','Verizon',0,1);


CREATE TABLE `sensor_devices` (
  `device_id` varchar(45) NOT NULL,
  `device_type_id` int unsigned NOT NULL,
  `active` TINYINT(1) DEFAULT 1 COMMENT '0 - Inactive , 1 Active',
  `state` smallint(6) DEFAULT NULL COMMENT 'commissioned(1)/associated(2)/disassociated(3)/decommissioned(4)',
  `org_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `battery_value` BIGINT(20) DEFAULT 3760,
  `demo_flag` int(4) DEFAULT 0 COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
  `device_key` VARCHAR(25) DEFAULT NULL,
  PRIMARY KEY (`device_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE `sensor_notifications` (
  `not_id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_code` bigint(20) NOT NULL,
  `notification` varchar(3000) DEFAULT NULL,
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `device_id` varchar(45) NOT NULL,
  `container_id` varchar(255) DEFAULT NULL,
  `rule_id` bigint(20) unsigned DEFAULT NULL,
  `sensor_data_id` bigint(20) unsigned DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `demo_flag` int(4) DEFAULT 0 COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
  `org_id` bigint(20) NOT NULL,
  PRIMARY KEY (`not_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



CREATE TABLE `sensor_package` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `package_id` varchar(200) NOT NULL,
  `product_id` varchar(45) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `state` smallint(6) DEFAULT NULL COMMENT 'commissioned(1)/associated(2)/disassociated(3)/decommissioned(4)',
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `updated_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `demo_flag` int(4) DEFAULT 0 COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
  PRIMARY KEY (`id`),
  UNIQUE KEY `package_product_id` (`org_id`,`package_id`,`product_id`)
 );
 
 
CREATE TABLE `sensor_systems` (
  `system_code` varchar(20) NOT NULL,
  `system_url` varchar(255) NOT NULL,
  PRIMARY KEY (`system_code`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

INSERT INTO `sensor_systems` VALUES 
('PROD','https://coldchain.verizon.com'),
('BETA','https://ittbeta.track-n-trace.net'),
('CLEAR','https://mobile.track-n-trace.net'),
('DEMO','https://ittdemo.track-n-trace.net'),
('DEMO11','https://ittdemo11.track-n-trace.net'),
('JENN','https://ittdemo1.track-n-trace.net')
;

CREATE TABLE `sensor_threshold` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  `profile_id` int(10) NOT NULL,
  `attr_name` varchar(255) NOT NULL,
  `min_value` decimal(20,4) NOT NULL,
  `max_value` decimal(20,4) NOT NULL,
  `device_type_id` int(11) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=utf8;


CREATE TABLE  `trans_history` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `container_id` varchar(100) NOT NULL,
  `product_id` varchar(100) NOT NULL,
  `device_id` varchar(45) NOT NULL,
  `org_id` decimal(20,0) NOT NULL,
  `group_id` decimal(20,0) NOT NULL,
  `addr_type` int(11) NOT NULL COMMENT '201 - Ship From , 202 - Ship To',
  `addr_1` varchar(255) NOT NULL,
  `addr_2` varchar(255) DEFAULT NULL,
  `city` varchar(45) NOT NULL,
  `zip` varchar(45) DEFAULT NULL,
  `state` varchar(75) DEFAULT NULL,
  `country` varchar(75) NOT NULL,
  `create_datetime` timestamp NOT NULL,
  `update_datetime` timestamp NOT NULL,
  `user_id` bigint(20) DEFAULT NULL,
  is_active TINYINT(1) DEFAULT 1 COMMENT '0 - Inactive , 1 Active',
  `demo_flag` int(4) DEFAULT 0 COMMENT '1 or greater - do not delete this row when reset - for demo instances only',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1000022 DEFAULT CHARSET=utf8;

-- geopoints entry for all device types

CREATE TABLE `sensor_geopoints` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `created_on` timestamp DEFAULT CURRENT_TIMESTAMP,
  `profile_id` int(10) NOT NULL,
  `point_name` varchar(45) DEFAULT NULL,
  `geofence_number` varchar(100) DEFAULT NULL,
  `latitude` varchar(50) NOT NULL,
  `longitude` varchar(50) NOT NULL,
  `address` varchar(256) DEFAULT NULL,
  `radius` int DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;



-- Master tables

CREATE TABLE  `rfx_org` (
  `org_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `org_name` varchar(150) NOT NULL,
  `short_name` varchar(20) NOT NULL,
  `addr_1` varchar(100) DEFAULT NULL,
  `addr_2` varchar(100) DEFAULT NULL,
  `city` varchar(50) DEFAULT NULL,
  `state_code` varchar(5) DEFAULT NULL,
  `state_name` varchar(20) DEFAULT NULL,
  `postal_code` varchar(20) DEFAULT NULL,
  `phone` varchar(75) DEFAULT NULL,
  `country` varchar(20) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `org_extid` varchar(50) DEFAULT NULL,
  `active` int(1) unsigned DEFAULT '1',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

CREATE TABLE  `rfx_users` (
  `user_id` bigint(20) unsigned NOT NULL,
  `user_login` varchar(50) NOT NULL,
  `user_name` varchar(50) NOT NULL,
  `user_passwd` varchar(50) DEFAULT NULL,
  `user_email` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `last_login` datetime DEFAULT NULL,
  `active` int(1) unsigned DEFAULT '1',
  `user_type` int(2) unsigned DEFAULT '2' COMMENT '0-System, 1-Admin, 2-Normal',
  `org_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `user_tz` varchar(45) DEFAULT '+00:00',
  `token` varchar(45) DEFAULT NULL,
  `token_expiry_time` datetime DEFAULT NULL,
  `last_passwd_changed` datetime DEFAULT NULL,
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;

-- Insert default System User(System/demo235).
insert into rfx_users(user_id,user_name, user_login, user_passwd, user_email, last_login, active, user_type, org_id, group_id)
values(1,'System', 'System', 'dd018353a92dd0c8544475be08bb2e4fe16fbda6', null, null, 1, 0, 0, 0);

CREATE TABLE  `sensor_profile` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(150) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `device_type_Id` int(11) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `active` TINYINT(1) DEFAULT 1 COMMENT '0 - Inactive , 1 Active',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


CREATE TABLE  `alert_groups` (
  `id`  bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `group_name` varchar(150) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `group_id` bigint(20) NOT NULL,
  `description` varchar(300) DEFAULT NULL,
  `active` TINYINT(1) DEFAULT 1 COMMENT '0 - Inactive , 1 Active',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

CREATE TABLE  `sensor_alert_types` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,  
  `alert_code` bigint(20) NOT NULL,
  `alert_msg` varchar(150) NOT NULL,
  `alert_detail_msg` varchar(300) NOT NULL,
  `corrective_action` varchar(300) DEFAULT NULL,
  `alert_group_id` bigint(20) DEFAULT NULL,
  `org_id` bigint(20) NOT NULL, 
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;


-- System config and org config tables

CREATE TABLE  `system_config` (
  `param_name` varchar(50) NOT NULL,
  `param_value` varchar(1000) NOT NULL,
  `display_name` varchar(250) DEFAULT "",
  `active` int(2) DEFAULT 1 COMMENT '0 - Inactive , 1 Active',
  PRIMARY KEY (`param_name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `system_config`(`param_name`, `param_value`,`display_name`,`active`) VALUES
('sensor.location.decimal.limit','3','Decimal Limit for lat/lang',1),
('sensor.notification.autorefresh.freq','60000','Notification- Auto refresh',1),
('sensor.report.directory','D:/sensor/report/','Report Directory',1),
('token_expires_in', '1800000','Token Expiry Time (milli seconds)',1),
('replicate.notification', 'true','Replicate Notification (true/false)',1),
('sendum.user', 'sendum','Sendum User Name',1),
('sendum.password', 'Sendum123','Sendum Password',1),
('kirsen.user', 'kirsen','Kirsen User Name',1),
('kirsen.password', 'kirsen123','Kirsen Password',1),
('sendum.logFolder','c:\\devicelog','Sendum Log Folder',1),
('queclink.tcpport','6880','Queclink Port',1),
('queclink.logFolder','c:\\queclinkLog','Queclink Log Folder',1),
('queclink.logFile','queclink','Queclink Log File',1),
('queclink.temp.report.interval.in.sec','60','Queclink Temp report interval (seconds)',1),
('kirsen.logFolder','c:\\kirsenLog','Kirsen Log Folder',1),
('diagnostic.logFolder','c:\\diagnosticLog','Diagnostic Log Folder',1),
('application.url','http://localhost:8080/sensor/#/','Application URL',1),
('mail.authenticate','true','Email Authentication Required (true/false)',1),
('mail.smtphost','216.218.206.29','SMTP Host',1),
('mail.smtpport','587','SMTP Port',1),
('mail.smtp.password','demo','SMTP Password',1),
('mail.smtp.username','demo@verizonitt.net','SMTP User Name',1),
('reset.pass.token.expiry','1','Reset Password Expiry Time (days)',1),
('set.pass.token.expiry','2','Create Password Expiry Time (days)',1),
('sensor.authenticate.request','true','Authentication Required',1),
('aws.accesskey','AKIAJGI5G7ME252F5JHQ', 'AWS Access Key', 1),
('aws.secretkey','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', 'AWS Secret Key', 1),
('aws.region','us-west-2', 'AWS SNS Region', 1),
('aws.smstype', 'Transactional', 'SMS Type is Transactional or Promotional', 1),
('kirsen.controller.url','https://control.kirsenglobalsecurity.com/api/v1/<verb>/<deviceKey>/', 'Kirsen Controller URL', 1),
('kirsen.controller.userid','Rfxcel', 'Kirsen Controller Userid', 1),
('kirsen.controller.password','da2head', 'Kirsen Controller Password', 1),
('create.password.title','Your job just got a whole lot simpler.', 'Set Password Title', 1),
('create.password.text.body',' With Verizon Intelligent Track & Trace, quickly track products, manage devices, and access alerts.#Please verify your email addresss by clicking the following link:# Please contact support if you have any issues with your account.', 'Set Password text Body', 1),
('reset.password.title','Your job just got a whole lot simpler.', 'Reset Password Title', 1),
('reset.password.text.body',' With Verizon Intelligent Track & Trace, quickly track products, manage devices, and access alerts.#Please reset password by clicking the following link:# Please contact support if you have any issues with your account.', 'ReSet Password text body', 1),
('reset.password.subject','Reset Password Request', 'Reset Password Subject', 1),
('create.password.subject','Set Password Request', 'Set Password Subject', 1),
('queclink.controller.url','coldchain.verizon.com', 'Queclink Controller URL', 1),
('queclink.controller.port','6880', 'Queclink Controller Port', 1),
('queclink.config.template', 'AT+GTRTO=gl300vc,4,,,,,,FFFF$AT+GTSRI=gl300vc,2,,1,<queclink.device.url>,<queclink.device.port>,216.218.206.20,6880,,0,0,0,,,,FFFF$AT+GTFKS=gl300vc,1,1,3,1,1,3,3,10,3,3,FFFF$AT+GTCFG=gl300vc,gl300vc,GL300VC,0,0.0,1,10,1F,,,823,0,1,1,<profile.attr.freq>,0,0,20491231235959,1,0,,FFFF$AT+GTTMA=gl300vc,+,0,0,0,,0,,,,FFFF$AT+GTFRI=gl300vc,1,0,,,0000,0000,<profile.gps.freq>,<profile.gps.freq>,,9999,9999,,1000,1000,0,5,50,5,0,0,FFFF$AT+GTTEM=gl300vc,0,-20,60,900,3600,2,,,,,,FFFF$AT+GTTPR=gl300vc,0,<profile.attr.freq>,<profile.comm.count>,1,,,,,FFFF$ AT+GTDOG=gl300vc,0,60,30,0200,,1,0,0,360,360,1,FFFF$AT+GTPDS=gl300vc,1,FB,,,,,,,FFFF$AT+GTRTO=gl300vc,1,,,,,0,FFFF$AT+GTDAT=gl300vc,0,,Device <deviceid> setup 2.1 completed,1,,,,FFFF$', 'Queclink Device Configuration Template', 1),
('queclink.config.url','216.218.206.20/', 'Queclink Configuration URL', 1),
('queclink.config.cmd','AT+GTUPC=gl300vc,3,6,0,1,0,<queclink.config.url>,1,,,,FFFF$', 'Queclink Configuration Command', 1),
('kirsen.config.url','https://coldchain.verizon.com/kirsen', 'Kirsen Configuration URL', 1),
('kirsen.config.template','{@temp_min@:-200.0,@temp_max@:200.0,@temp_mode@:@transfer@,@communication_period@:<profile.comm.freq>,@gps_period@:<profile.gps.freq>,@temperature_period@:<profile.attr.freq>}','Kirsen Configuration Template', 1),
('queclink.config.dir','C:\\Program Files\\Apache2.2\\htdocs', 'Queclink Download Directory', 1),
('geolocation.radius.value','500', 'Geolocation Radius Value (meters)', 1),
('geolocation.radius.format','meters', 'Geolocation Radius Format', 1),
('sendum.controller.url','https://restapi.sendum.com/<verb>', 'Sendum Controller URL', 1),
('sendum.controller.userid','arunrao@rfxcel.com', 'Sendum Controller Userid', 1),
('sendum.controller.password','sendum', 'Sendum Controller Password', 1),
('sendum.config.url','https://coldchain.verizon.com/sendum', 'Sendum Configuration URL', 1),
('bact.growth.module.url','http://localhost:8080/bacterialGrowth/rest/calcBactGrowth', 'Bacterial Growth Module URL', 1),
('bact.growth.module.connection.timeout','7', 'Bacterial Growth Module URL', 1)
;


CREATE TABLE  `org_config` (
  `param_name` varchar(50) NOT NULL,
  `param_value` varchar(400) NOT NULL,
  `display_name` varchar(250) NOT NULL,
  `org_id` bigint(20) NOT NULL,
  `active` int(2) DEFAULT '1' COMMENT '0 - Inactive , 1 - Active',
  PRIMARY KEY (`param_name`,`org_id`) USING BTREE,
  UNIQUE KEY `unique_index` (`param_name`,`org_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `org_config` VALUES 
('sensor.graph.autorefresh','true','Graph Auto Refresh',0,1),
('sensor.graph.autorefresh.freq','60000','Graph Auto Refresh Frequency (milli seconds) ',0,1),
('sensor.temperature.format','F','Temperature Format',0,1),
('dashboard.map.center.latitude', '40', 'Dashboard Map Center (latitude)', 0, 1),
('dashboard.map.center.longitude', '-95', 'Dashboard Map Center (longitude)', 0, 1),
('dashboard.map.zoom', '6', 'Dashboard Map Zoom Factor', 0, 1)
;


-- create alert_group_user_mapping table
CREATE TABLE `alert_group_user_mapping` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `alert_group_id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB DEFAULT CHARSET=utf8;

-- Master table to include all attributes
CREATE TABLE  `sensor_attributes` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `attr_name` varchar(45) NOT NULL,
  `is_configurable` tinyint(1) DEFAULT '0' COMMENT '0 - Do not show/display this attribute to users. 1- Show/display this attribute to users',
  `alert_code` bigint(20) NOT NULL,
  `unit` varchar(15) DEFAULT NULL,
  `range_min` decimal(10,5) NOT NULL,
  `range_max` decimal(10,5) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=utf8;


INSERT INTO `sensor_attributes` VALUES
 (1,'temperature',1, 3042,'° F',-4,140),
 (2,'pressure',1, 3045,'kPa',0,2000),
 (3,'humidity',1, 3046,'%',0,100),
 (4,'light',1, 3043,'LUX',0,25000),
 (5,'tilt',1, 3044,'° ',0,180),
 (6,'growthLog',1, 3053,'',0,100),
 (7,'cumulativeLog',1, 3047,'',0,100),
 (8,'deviceIdentifier',0, 0,'',0,100),
 (9,'latitude',0, 0,'',0,100),
 (10,'longitude',0, 0,'',0,100),
 (11,'fixTimeStamp',0, 0,'UTC',0,100),
 (12,'shock',1, 3048,'G',0,8),
 (13,'battery',1, 3049,'%',0,100),
 (14,'ambientTemperature',1, 3051,'',0,100),
 (15,'statusTimeStamp',0, 0,'',0,100),
 (16,'deviceType',0, 0,'',0,100),
 (17,'excursion',1, 3050,'',0,100),
 (18,'excursionAttributes',0, 0,'',0,100),
 (19,'reset',1, 3052,'',0,100);

-- Master table to include all default alerts
CREATE TABLE `sensor_default_alerts` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `alert_code` BIGINT(20) NOT NULL,
  `alert_msg` VARCHAR(300) NOT NULL,
  `alert_detail_msg` VARCHAR(300) NOT NULL,  
  `attribute_name` VARCHAR(100) DEFAULT NULL, 
  `is_default` TINYINT(1) DEFAULT 1 COMMENT '0 - not default , 1 default', 
  PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=latin1;

-- Entry default alerts
INSERT INTO `sensor_default_alerts` VALUES
(1,3042,'Alert Notification - Temperature Threshold Exceeded ','Alert Notification - The Temperature (<<ATTR>>) is not within the acceptable range.','temperature',0),
(2,3043,'Alert Notification - Light Threshold Exceeded ','Alert Notification - The Light (<<ATTR>>) is not within the acceptable range.\r\n','light',0),
(3,3044,'Alert Notification - Tilt Threshold Exceeded ','Alert Notification - The Tilt (<<ATTR>>) is not within the acceptable range.\r\n','tilt',0),
(4,3045,'Alert Notification - Pressure Threshold Exceeded ','Alert Notification - The Pressure (<<ATTR>>) is not within the acceptable range.\r\n','pressure',0),
(5,3046,'Alert Notification - Humidity Threshold Exceeded ','Alert Notification - The Humidity (<<ATTR>>) is not within the acceptable range.\r\n','humidity',0),
(6,3047,'Alert Notification - Bacteria Log Threshold Exceeded  ','Alert Notification- The Bacteria Count Log (<<ATTR>>) is not within the acceptable range.\r\n','cumulativeLog',0),
(7,3048,'Alert Notification - Shock Threshold Exceeded','Alert Notification - The Shock value (<<ATTR>>) is not within the acceptable range.\r\n','shock',0),
(8,3049,'Alert Notification - Low Battery','Alert Notification - The Battery Percentage (<<ATTR>>) is not within the acceptable range.\r\n','battery',0),
(9,3050,'Alert Notification - Attribute Excursion Unresolved','Alert Notification- The <<ATTR>> has been out of range for last (<<ATTR1>>) minutes. \r\n','excursion',0),
(10,3051,'Alert Notification - Ambient Temperature Exceeded ','Alert Notification - The Ambient Temperature value (<<ATTR>>) is not within acceptable range.','ambientTemperature',0),
(11,3052,'Alert Notification - Attribute Back to Acceptable Range','Alert Notification - The <<ATTR>> value (<<ATTR1>>) is back within the acceptable range.','reset',1),
(12,3053,'Alert Notification - Bacterial Growth Log Threshold Exceeded  ','Alert Notification- The Bacterial Growth Log (<<ATTR>>) is not within the acceptable range.\r\n','growthLog',0),
(13,3701,'GeoFence Alert - Shipment Arrived ','GeoFence Alert - The shipment has arrived at <<ATTR>>. Latitude (<<ATTR1>>) and Longitude (<<ATTR2>>).','',1),
(14,3702,'GeoFence Alert - Shipment Departed','GeoFence Alert - The shipment has left from <<ATTR>>.  Latitude (<<ATTR1>>) and Longitude (<<ATTR2>>).','',1),
(15,3703,'GeoFence Alert - Shipment Route Resumed','GeoFence Alert - Shipment has rejoined route, currently at <<ATTR>>.\r\n','',0),
(16,3704,'GeoFence Alert - Shipment Route Deviation ','GeoFence Alert - Shipment has deviated from route, currently at <<ATTR>>.\r\n','',0),
(17,3705,'GeoFence Alert - Shipment Without Motion ','GeoFence Alert - Shipment location has not change for <<ATTR>> minutes.\r\n','',1);

-- device_log table
CREATE TABLE `device_log` (
`id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`device_id` VARCHAR(45) NOT NULL,
`user_id` BIGINT(20) DEFAULT NULL,
`log_msg` TEXT,
`create_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
PRIMARY KEY (`id`),
KEY `Index_deviceid` (`device_id`)
) ENGINE=INNODB AUTO_INCREMENT=1875 DEFAULT CHARSET=utf8;

/*To add location column*/
ALTER TABLE trans_history ADD location varchar(45) NOT NULL;

/*To add Primary Key id column in Org Config Table */
ALTER TABLE org_config ADD COLUMN `id` BIGINT NOT NULL AUTO_INCREMENT  AFTER `active` 
, DROP PRIMARY KEY 
, ADD PRIMARY KEY USING BTREE (`id`, `org_id`) ;


-- to be deleted
SET FOREIGN_KEY_CHECKS=1;

/*Insert the frequency data on profile page*/
CREATE TABLE `sensor_frequency` (
  `id` BIGINT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
  `profile_id` INT(10) UNSIGNED NOT NULL,
  `common_freq` DECIMAL(20,4) DEFAULT NULL,
  `gps_freq` DECIMAL(20,4) DEFAULT NULL,
  `attr_freq` DECIMAL(20,4) DEFAULT NULL,
  `org_id` BIGINT(20) UNSIGNED NOT NULL,
  `group_id` BIGINT(20) UNSIGNED NOT NULL,
  `created_time` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`)
)
ENGINE = InnoDB;

/*Master table rfx_menus to add menus and show*/
CREATE TABLE `rfx_menus` (
`id` INT(20) UNSIGNED NOT NULL AUTO_INCREMENT,
`state` VARCHAR(200) NOT NULL,
`menu_name` VARCHAR(40) NOT NULL,
`order` INT(10) NOT NULL,
`org_id` BIGINT(20) DEFAULT 0,
`min_user_type` int(2) unsigned DEFAULT '2' COMMENT '0-System, 1-Admin, 2-Normal',
PRIMARY KEY (`id`)
) ENGINE=INNODB DEFAULT CHARSET=utf8;

INSERT INTO `rfx_menus` VALUES
(1, 'app.dashboard.showMap', 'Dashboard', 1, 0, 2),
(2, 'app.profile', 'Profile', 2, 0, 2),
(3, 'app.products', 'Shipments', 3, 0, 2),
(4, 'app.dashboard.alert', 'Alerts', 4, 0, 2),
(5, 'app.organization', 'Organization', 5, 0, 2),
(6, 'app.settings', 'Settings', 6, 0, 1),
(7, 'app.deviceLogs', 'Device Logs', 7, 0, 1);

/*Insert the Time Zone field in Organisation*/
ALTER TABLE `rfx_org` ADD COLUMN `org_tz` VARCHAR(45) NULL DEFAULT '+00:00'  AFTER `updated_time` ;

/*TimeZone Master Table*/

CREATE  TABLE `rfx_timezone` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT ,
  `timezone_name` VARCHAR(45) NOT NULL ,
  `timezone_code` VARCHAR(45) NOT NULL ,
  `timezone_offset` VARCHAR(45) NOT NULL ,
  PRIMARY KEY (`id`) ,
  UNIQUE INDEX `timezone_code_UNIQUE` (`timezone_code` ASC) );
  
 ALTER TABLE `rfx_timezone` CHANGE COLUMN `id` `id` INT(20) NOT NULL AUTO_INCREMENT  ;

  
  -- Entry US Timezones
INSERT INTO `rfx_timezone` VALUES
(1,'America/Los_Angeles','Pacific Standard Time (PST)','-08:00'),
(2,'America/Los_Angeles','Pacific Daylight Time (PDT)','-07:00'),
(3,'America/Denver','Mountain Standard Time (MST)','-07:00'),
(4,'America/Denver','Mountain Daylight Time (MDT)','-06:00'),
(5,'America/Chicago','Central Standard Time (CST)','-06:00'),
(6,'America/Chicago','Central Daylight Time (CDT)','-05:00'),
(7,'America/New_York','Eastern Standard Time (EST)','-05:00'),
(8,'America/New_York','Eastern Daylight Time (EDT)','-04:00'),
(9,'Europe/London','Greenwich Mean Time (GMT)','+00:00')
;

-- To insert timezone id in Org Table
ALTER TABLE `rfx_org` ADD COLUMN `tz_id` BIGINT(20) NOT NULL  AFTER `org_tz` ;

-- To insert timezone id in User Table
ALTER TABLE `rfx_users` ADD COLUMN `tz_id` BIGINT(20) NULL  AFTER `updated_time` ;

Alter table sensor_frequency 
  ADD COLUMN `common_freq_unit` int(2) unsigned NOT NULL COMMENT '0 - minutes, 1- hours, 2 -days' after `common_freq`,
  ADD COLUMN `gps_freq_unit` int(2) unsigned NOT NULL COMMENT '0 - minutes, 1- hours, 2 -days' after `gps_freq`,
  ADD COLUMN `attr_freq_unit` int(2) unsigned  NOT NULL COMMENT '0 - minutes, 1- hours, 2 -days' after `attr_freq` ;