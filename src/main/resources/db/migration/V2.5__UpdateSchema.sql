-- ITT 2.5 Migration starts --
-- disable settings from admin menu
DELETE FROM `rfx_menus` WHERE  `state` = 'app.settings' and `min_user_type` = 1;
UPDATE `system_config` SET `param_value`='m' WHERE  `param_name`='geolocation.radius.format';
DELETE FROM system_config WHERE param_name IN ('helpdesk.contact.email','helpdesk.contact.phone.primary',
'helpdesk.contact.phone.other','helpdesk.contact.message');
INSERT INTO system_config VALUES
('helpdesk.contact.message','Please call 800-525-0481, select option 1 for all issues.\n\nWhen calling you will need the Mobile Device Number(MDN). The MDN can be found under the Organization tab, then Device list. If you do not have the MDN of the device you are calling about, then you may press the # key after making your selection in the IVR.','Helpdesk Contact Message',1)
;
UPDATE `sensor_attr_type` SET `unit`='°F' WHERE  `attr_name`='temperature' and `unit` like '%F%';
UPDATE `sensor_attr_type` SET `unit`='°' WHERE  `attr_name`='tilt';
UPDATE `sensor_attr_type` set unit = 'g/hz²' WHERE attr_name = 'vibration';
UPDATE `sensor_attributes` set unit = 'g/hz²' WHERE attr_name = 'vibration';
UPDATE `sensor_attributes` SET `unit`='°' WHERE  `attr_name`='tilt';
UPDATE `sensor_attributes` SET `unit`='°F' WHERE  `attr_name`='temperature';
-- added to store address in individual columns
ALTER TABLE `sensor_geopoints` 
 ADD COLUMN `addr1` VARCHAR(75) AFTER `location_type`,
 ADD COLUMN `addr2` VARCHAR(75) AFTER `addr1`,
 ADD COLUMN `city` VARCHAR(45) AFTER `addr2`,
 ADD COLUMN `state` VARCHAR(45) AFTER `city`,
 ADD COLUMN `zip` VARCHAR(20) AFTER `state`,
 ADD COLUMN `country` VARCHAR(45) AFTER `zip`;
 
update `sensor_attributes` 
set range_min=0, range_max=8
where attr_name = 'shock';
update `sensor_attr_type` 
set range_min=0, range_max=8
where attr_name = 'shock';

update `sensor_attributes` 
set range_min=0, range_max=6
where attr_name = 'vibration';
update `sensor_attr_type` 
set range_min=0, range_max=6
where attr_name = 'vibration';

UPDATE `sensor_attr_type` SET `round_digits`='1' WHERE  `attr_name`= 'temperature';
UPDATE `sensor_attr_type` SET `round_digits`='0' WHERE  `attr_name`= 'pressure';
UPDATE `sensor_attr_type` SET `round_digits`='1' WHERE  `attr_name`= 'humidity';
UPDATE `sensor_attr_type` SET `round_digits`='0' WHERE  `attr_name`= 'light';
UPDATE `sensor_attr_type` SET `round_digits`='0' WHERE  `attr_name`= 'tilt';
UPDATE `sensor_attr_type` SET `round_digits`='5' WHERE  `attr_name`= 'latitude';
UPDATE `sensor_attr_type` SET `round_digits`='5' WHERE  `attr_name`= 'longitude';
UPDATE `sensor_attr_type` SET `round_digits`='2' WHERE  `attr_name`= 'shock';
UPDATE `sensor_attr_type` SET `round_digits`='0' WHERE  `attr_name`= 'battery';
UPDATE `sensor_attr_type` SET `round_digits`='2' WHERE  `attr_name`= 'vibration';

ALTER TABLE `sensor_attr_type`
	CHANGE COLUMN `round_digits` `round_digits` INT(4) NULL DEFAULT '0' AFTER `multiply_factor`;

UPDATE `system_config` SET `param_value`='true' WHERE  `param_name`='alert.devicelog.search.enable';
UPDATE `system_config` SET `param_value`='true' WHERE  `param_name`='elasticsearch.data.fetch';
 
UPDATE `system_config` SET `param_value`='2.5' WHERE  `param_name`='build.version';
-- ITT 2.5 Migration ends --