-- ITT 2.62 Migration starts --
INSERT INTO `system_config` (`param_name`, `param_value`, `display_name`)  VALUES 
    ('application.urllist', '', 'List of Federated URLs');


UPDATE `system_config` SET `param_value`='2.62' WHERE  `param_name`='build.version';
-- ITT 2.62 Migration ends --