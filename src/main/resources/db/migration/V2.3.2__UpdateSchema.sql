--------------------------------------- ITT 2.3 Migration starts -----------------------------------------------------
-- Oct 27 2017 Alter script to add range values alert details

UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Temperature (<<ATTR>>) is not within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3042";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Light (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3043";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Tilt (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3044";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Pressure (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3045";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Humidity (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3046";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The Bacteria Count Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3047";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Shock value (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3048";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Battery Percentage (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3049";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The <<ATTR>> has been out of range for last (<<ATTR1>>) minutes.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3050";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The Ambient Temperature value (<<ATTR>>) is not within acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3051";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification - The <<ATTR>> value (<<ATTR1>>) is back within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3052";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The Bacterial Growth Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3053";
UPDATE sensor_default_alerts SET alert_detail_msg ="Alert Notification- The Vibration (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3054";

UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Temperature (<<ATTR>>) is not within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3042";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Light (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3043";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Tilt (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3044";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Pressure (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3045";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Humidity (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3046";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The Bacteria Count Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3047";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Shock value (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3048";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Battery Percentage (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3049";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The <<ATTR>> has been out of range for last (<<ATTR1>>) minutes.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3050";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The Ambient Temperature value (<<ATTR>>) is not within acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3051";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification - The <<ATTR>> value (<<ATTR1>>) is back within the acceptable range. Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3052";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The Bacterial Growth Log (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3053";
UPDATE sensor_alert_types SET alert_detail_msg ="Alert Notification- The Vibration (<<ATTR>>) is not within the acceptable range.\r\n Range set (<<MIN>> - <<MAX>>)" WHERE alert_code = "3054";


-- 13 Oct 2017, Change the unit for vibration
delete from system_config where param_name like 'thingspace%';
INSERT INTO system_config VALUES
('thingspace.integration.url','https://preprodiwk.thingspace.verizon.com/api/v2/devices','ThingSpace Integration Url',1),
('thingspace.integration.retry.count','3','ThingSpace Integration retry count for request',1),
('thingspace.integration.get.interval','1440','ThingSpace Integration get request interval (minute))',1),
('thingspace.logFolder','c:\\thingspaceLog','ThingSpace Log Folder',1),
('thingspace.poll.service.start','false','Start ThingSpace poll service (true/false)',1);


-- 30 Oct 2017, thingspace device type
DELETE FROM system_config WHERE param_name IN ('thingspace.device.type');
INSERT INTO system_config VALUES
('thingspace.device.type','ts.device.cHeAssetTracker','Thinkspace Device Type',1);

DELETE FROM system_config WHERE param_name IN ('thingspace.device.provider');
INSERT INTO system_config VALUES
('thingspace.device.provider','9dfcfa69-a1c8-4eae-8611-b282646bb113','Thinkspace CedarHill Provider ID',1);

-- 02 Nov 2017 Added config for preset shock value
DELETE FROM system_config WHERE param_name IN ('shock.preset.value');
INSERT INTO system_config VALUES
('shock.preset.value','0.5','Preset Shock Value',1);

-- 03 Nov 2017 Updated Verizon CedarHill device name
UPDATE `sensor_device_types` SET `device_type`='Verizon VZCATM1' WHERE  `device_manufacturer`='Verizon'; 

-- 04 Nov 2017 Updated CedarHill attribute types
update `sensor_attributes` 
set unit = 'hPA', range_min=0, range_max=2000 
where attr_name = 'pressure';
update `sensor_attr_type` 
set unit = 'hPA', range_min=0, range_max=2000 
where attr_name = 'pressure';

update `sensor_attributes` 
set unit = 'Lux', range_min=0, range_max=25000
where attr_name = 'light';
update `sensor_attr_type` 
set unit = 'Lux', range_min=0, range_max=25000
where attr_name = 'light';

update `sensor_attributes` 
set range_min=0, range_max=100
where attr_name = 'shock';
update `sensor_attr_type` 
set range_min=0, range_max=100
where attr_name = 'shock';

update `sensor_attributes` 
set range_min=0, range_max=100
where attr_name = 'vibration';
update `sensor_attr_type` 
set range_min=0, range_max=100
where attr_name = 'vibration';

UPDATE `system_config` SET `param_value`='2.32' WHERE  `param_name`='build.version';
--------------------------------------- ITT 2.3 Migration ends -----------------------------------------------------