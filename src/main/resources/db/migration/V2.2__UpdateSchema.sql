--------------------------------------- ITT 2.2 Migration starts -----------------------------------------------------
-- Build#12
-- 04 Sept 2017, ITT-400 : Master table alert_group_mapping
CREATE TABLE `alert_group_mapping` (
  `id` BIGINT(20) NOT NULL AUTO_INCREMENT,
  `alert_id` BIGINT(20) UNSIGNED NOT NULL,
  `alert_group_id` BIGINT(20) UNSIGNED NOT NULL,
  `created_time` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
)
ENGINE = INNODB DEFAULT CHARSET=utf8;

-- 04 Sept 2017, ITT-400 : migration script to migrate data from table sensor_alert_types to table alert_group_mapping
INSERT INTO alert_group_mapping(alert_id, alert_group_id, created_time)
SELECT id, alert_group_id, created_time FROM sensor_alert_types WHERE alert_group_id IS NOT NULL AND alert_group_id>0;
ALTER TABLE sensor_alert_types DROP COLUMN alert_group_id;

-- 11 Sept, ITT-390, support for same packageId in different products
ALTER TABLE sensor_associate ADD COLUMN `product_id`  varchar(100) AFTER `child_id`;
ALTER TABLE sensor_notifications ADD COLUMN `product_id` varchar(100) AFTER `container_id`;
ALTER TABLE sensor_data ADD COLUMN `product_id` varchar(100) AFTER `container_id`;

-- This needs to be run only once, to populate product_id values from sensor_package to sensor_associate, sensor_data, sensor_notifications
-- NOTE uncomment this and run it once for migrating the data
/* UPDATE sensor_associate sa
INNER JOIN sensor_package sp
ON sa.parent_id = sp.package_id
SET sa.product_id  = sp.product_id
WHERE sa.parent_id =  sp.package_id;

UPDATE sensor_data sd
INNER JOIN sensor_package sp
ON sd.container_id = sp.package_id
SET sd.product_id  = sp.product_id
WHERE sd.container_id =  sp.package_id;

UPDATE sensor_notifications sn
INNER JOIN sensor_package sp
ON sn.container_id = sp.package_id
SET sn.product_id  = sp.product_id
WHERE sn.container_id =  sp.package_id; */
-- End 
ALTER TABLE sensor_data MODIFY container_id varchar(255) NOT NULL;
ALTER TABLE sensor_notifications MODIFY container_id varchar(255) NOT NULL;

ALTER TABLE sensor_associate MODIFY product_id varchar(100) NOT NULL;
ALTER TABLE sensor_data MODIFY product_id varchar(100) NOT NULL;
ALTER TABLE sensor_notifications MODIFY product_id varchar(100) NOT NULL;

-- ITT 411 : Added column to store shock and vibration value
Alter TABLE sensor_data 
ADD COLUMN vibration varchar(45) DEFAULT NULL,
ADD COLUMN shock varchar(45) DEFAULT '0';

INSERT INTO `sensor_attributes` VALUES
(20,'vibration',1, 3054,'g/hz2',0,6);

UPDATE sensor_attributes SET range_max = 180 WHERE attr_name = "tilt";

INSERT INTO `sensor_default_alerts` VALUES
(18,3054,'Alert Notification - Vibration Threshold Exceeded ','Alert Notification- The Vibration (<<ATTR>>) is not within the acceptable range.\r\n','vibration',0);

-- 11 Sept 2017, ITT-424 : help desk information
DELETE FROM system_config WHERE param_name IN ('helpdesk.contact.email','helpdesk.contact.phone.primary',
'helpdesk.contact.phone.other');
INSERT INTO system_config VALUES
('helpdesk.contact.email','IoTTechSupport@VerizonWireless.com','Helpdesk Contact Email',1),
('helpdesk.contact.phone.primary','800-525-0481','Helpdesk Contact Primary Phone',1),
('helpdesk.contact.phone.other','800-922-0204','Helpdesk Contact Other Phone',1);

-- 11 Sept 2017, ITT-429 : Added support for shock attribute
UPDATE sensor_attributes SET range_max = 8 WHERE attr_name = "shock";

-- 12 Sept 2017, ITT:412 
Alter TABLE sensor_associate 
ADD COLUMN left_home tinyint(1) DEFAULT 0;

-- 13 Sept 2017, ITT-422
ALTER TABLE sensor_devices ADD COLUMN `mdn` VARCHAR(25) DEFAULT NULL;

-- 14 Sept 2017, ITT-431
ALTER TABLE sensor_threshold 
ADD COLUMN `alert` TINYINT(1) DEFAULT 1 COMMENT '0 - Disable , 1 -  Enable' AFTER `max_value`,
ADD COLUMN `notification` TINYINT(1) DEFAULT 1 COMMENT '0 - Disable , 1 - Enable' AFTER `alert`;

DELETE FROM org_config WHERE param_name IN ('system.demo','data.simulator.path');
INSERT INTO `org_config` VALUES 
('system.demo','false','Is demo system', 0, 1, 0),
('data.simulator.path','c:\simulator.bat','Data Simulator path', 0, 1, 0);

-- 22 Sept 2017, added show_graph in sensor_attr_type
ALTER TABLE sensor_attr_type 
ADD COLUMN `show_map` TINYINT(1) DEFAULT 0 COMMENT '0 - Do not show on map , 1 -  Show on map' AFTER `show_graph`;


--------- 26 sept ,2017 , reset stored procedure.

DELIMITER $$
CREATE PROCEDURE `resetDevice`(IN deviceId VARCHAR(70), IN productId VARCHAR(100), IN packageId VARCHAR(200))
BEGIN
	delete from sensor_notifications where device_id=deviceId and container_id=packageId and product_id=productId;
	delete from sensor_data where device_id=deviceId and container_id=packageId and product_id=productId;
	update sensor_associate set excursion_status=0 where child_id=deviceId and parent_id=packageId and product_id=productId;
	update sensor_devices set state=2 where device_id=deviceId and state <>1;
	delete FROM sensor_excursion_log where device_id=deviceId;
	delete FROM sensor_device_log where device_id=deviceId;
	delete FROM sensor_bacterial_growth_log where device_id=deviceId;
END $$
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE `resetK4Data`()
BEGIN
	CALL resetDevice('358502062238111', 'Kirsen 4', '8111');
END$$
DELIMITER ;

-- 25 Sept 2017, resetK1K3Data stored procedure 
DELIMITER $$
CREATE PROCEDURE `resetK1K3Data`()
BEGIN
	CALL resetDevice('358502062237949', 'Kirsen 1', '7949');
	CALL resetDevice('358502062238145', 'Kirsen 3', '8145');
END$$
DELIMITER ;


DELIMITER $$
CREATE PROCEDURE `resetData`()
BEGIN
	CALL resetDevice('358509999900707', 'Verizon V4 Product', '10707');
END $$
DELIMITER ;

-- Sep 29th 2017 --

ALTER TABLE `sensor_devices`
	CHANGE COLUMN `device_key` `device_key` VARCHAR(128) NULL DEFAULT NULL AFTER `demo_flag`;
--------------------------------------- ITT 2.2 Migration ends -----------------------------------------------------

