angular.module('rfxcelApp').config(function($provide) {
    $provide.decorator('rzsliderDirective', function($delegate) {
      var directive = $delegate[0],
      compile = directive.compile;
      directive.compile = function(tElement, tAttrs) {
	      var link = compile.apply(this, arguments);
	      $elem=angular.element(tElement.children()[0]).children();
	      /* binding class when slider max value is 0*/ 
          //$elem.attr('ng-class', '{"rz-bar-initial": rzSliderHigh==0}');
          $elem.attr('ng-class', '{"rz-bar-initial": rzSliderModel==rzSliderHigh}');
	      return function(scope, tElement, tAttrs) {
	        link.apply(this, arguments);
	        // We can extend the link function here
	      }
	    }
      return $delegate;
    });
});