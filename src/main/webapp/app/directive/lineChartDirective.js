angular.module('rfxcelApp').directive("lineChart", function() {
    
    var uniqueId = 1;
    
    return {
    	scope: {
            attrName: '=',
            data: '=',
            minThreshVal: '=',
            maxThreshVal: '=',
            legend: "=",
            unit: "=",
            rangeMin: "=",
            rangeMax: "=",
            startdatetime: "=",
            enddatetime: "=",
            alertFlag: "="
        },
        template: '<header class="">'+
                      '<span class="attr-header-title" id="attr-header-title">{{selectedAttribute}}</span>'+
                  '</header>'+
                  '<div class="lineChart"></div>',
        
    	controller: 'chartDirCtrl',
        controllerAs: 'vm',
        
        link: function($scope, $element, $attrs){
                $element.find('div').attr('id' , $scope.attrName);
                $scope.drapGraph();
        }
    }
})