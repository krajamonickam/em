angular.module('rfxcelApp').service('profileListService', profileListService);

profileListService.$inject = [];

function profileListService() {

    var arrProfileList = [];

    this.setProfileList = function (list) {
        arrProfileList = list;
    }

    this.getProfileList = function () {
        return arrProfileList;
    }

}