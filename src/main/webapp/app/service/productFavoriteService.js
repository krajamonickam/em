angular.module('rfxcelApp').factory('productFavoriteService', productFavoriteService);
productFavoriteService.$inject = ['$resource', '$location', 'sharedDataService'];

function productFavoriteService($resource, $location, sharedDataService) {
    return {
        getfavoriteService: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/favourite', {}, {
            post: {
                method: 'POST'
            }
        })
    }
}