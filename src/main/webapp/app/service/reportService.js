angular.module('rfxcelApp').factory("reportService", reportService);

reportService.$inject = ['$http', '$location', 'sharedDataService'];


function reportService($http, $location, sharedDataService) {

    var factory = {};

    factory.downloadReports = function (reqObj) {

        var headerObj = {
        };

        return $http.post($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getShipmentReport', reqObj, headerObj);
    };

    return factory;

}