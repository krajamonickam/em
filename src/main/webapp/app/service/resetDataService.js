angular.module('rfxcelApp').factory("resetDataService", resetDataService);

resetDataService.$inject = ['$resource', '$location', 'sharedDataService'];


function resetDataService($resource, $location, sharedDataService) {
    return {
    	resetData: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/resetData', {}, {
            post: {
                method: 'POST'
            }
        }),
        resetOrg: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/resetOrg', {}, {
            post: {
                method: 'POST'
            }
        })
    }
}