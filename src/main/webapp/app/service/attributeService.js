angular.module('rfxcelApp').factory("attributeService", attributeService);

attributeService.$inject = ['$resource', '$location', 'sharedDataService'];

function attributeService($resource, $location, sharedDataService) {

    //"http://localhost:58438/sendum-1.0/sensor/getAttrList"
    //local-server/getAttributeList-res.json

    return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getAttrList', {}, {
        post: {
            method: 'POST'
        }
    });
}