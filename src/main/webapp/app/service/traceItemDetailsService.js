angular.module('rfxcelApp').factory("traceItemDetailsService", traceItemDetailsService);

traceItemDetailsService.$inject = ['$resource', '$location', 'sharedDataService'];

function traceItemDetailsService($resource, $location, sharedDataService) {

    var factory = {
            getItemSummaryByTraceEntityId: getItemSummaryByTraceEntityId,
            getItemDetailsByTraceEntityId: getItemDetailsByTraceEntityId,
            getSubItemsByTraceEntityId: getSubItemsByTraceEntityId,
            getParentTraceItemByTraceEntityId: getParentTraceItemByTraceEntityId,
            getEventsByTraceEntityId: getEventsByTraceEntityId,
            getRelatedNotificationByItemId: getRelatedNotificationByItemId,
            getForwardItemDetailsByTraceEntityId: getForwardItemDetailsByTraceEntityId
        },
        hostUrl = $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest';


    function getItemSummaryByTraceEntityId() {
        return $resource(hostUrl + '/item/getItemSummaryByTraceEntityId')
    }

    function getItemDetailsByTraceEntityId() {
        return $resource(hostUrl + '/item/getItemDetailsByTraceEntityId')
    }

    function getForwardItemDetailsByTraceEntityId() {
        return $resource(hostUrl + '/item/getForwardItemDetailsByTraceEntityId')
    }

    function getSubItemsByTraceEntityId() {
        return $resource(hostUrl + '/item/getSubItemsByTraceEntityId')
    }

    function getParentTraceItemByTraceEntityId() {
        return $resource(hostUrl + '/item/getParentTraceItemByTraceEntityId')
    }

    function getEventsByTraceEntityId() {
        return $resource(hostUrl + '/item/getEventsByTraceEntityId')
    }

    function getRelatedNotificationByItemId() {
        return $resource(hostUrl + '/item/getRelatedNotificationByItemId')
    }
    return factory;

}