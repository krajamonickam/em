angular.module('rfxcelApp').factory("DeviceTypeService", DeviceTypeService);
DeviceTypeService.$inject=['$resource','$location', 'sharedDataService'];

function DeviceTypeService($resource, $location, sharedDataService){
    var service={
        'getDeviceTypes' : getDeviceTypes,
        'addDeviceTypes' : addDeviceTypes,
        'getDeviceList' : getDeviceList,
        'addDevice' : addDevice,
        'modifyDevice' : modifyDevice,
        'deleteDevice' : deleteDevice,
        'modifyDeviceTypes' : modifyDeviceTypes,
        'deleteDeviceTypes' : deleteDeviceTypes,
        'getAttributeList' : getAttributeList,
        'getAttributesByDeviceType': getAttributesByDeviceType,
        'getDeviceIdDetails': getDeviceIdDetails
    }
    
    function getDeviceTypes(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port  + sharedDataService.getURLPath() + '/rest/deviceType/getDeviceTypes')
    }
    
    function addDeviceTypes(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port  + sharedDataService.getURLPath() + '/rest/deviceType/addDeviceType')
    }
    
    function modifyDeviceTypes(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/deviceType/updateDeviceType')
    }
    
    function deleteDeviceTypes(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/deviceType/deleteDeviceType')
    }
    
    function getAttributeList(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/deviceType/getAttributeList')
    }
    
     function getAttributesByDeviceType(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/deviceType/getAttributesByDeviceType')
    }
    
     function addDevice(){
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port  + sharedDataService.getURLPath() + '/rest/sensor/commission')
     }
     
     function modifyDevice(){
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port  + sharedDataService.getURLPath() + '/rest/sensor/updateDevice')
     }
     
     function deleteDevice(){
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port  + sharedDataService.getURLPath() + '/rest/sensor/deleteDevice')
     }
     
     function getDeviceList(){
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port  + sharedDataService.getURLPath() + '/rest/sensor/getDeviceList')
     }
     
     function getDeviceIdDetails(){
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getDevices')
     }
    
    return service;
}
