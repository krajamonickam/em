angular.module('rfxcelApp').factory('sessionService',['$http', function($http){
	return{
		set: function(key, value){ return localStorage.setItem(key, value); },
		get: function(key){ return localStorage.getItem(key); },
		destroy: function(key){ return localStorage.removeItem(key); },
		clear: function(){localStorage.clear();}
	};
}])