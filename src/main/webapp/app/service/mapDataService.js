angular.module('rfxcelApp').factory("mapDataService", mapDataService);

mapDataService.$inject = ['$resource', '$location', 'sharedDataService'];


function mapDataService($resource, $location, sharedDataService) {

    //"http://localhost:58438/sendum-1.0/sensor/getMapData"
    //local-server/getMapData-res.json

    return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getMapData', {}, {
        post: {
            method: 'POST'
        }
    });
}