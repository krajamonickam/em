function generalUtilityService($localStorage) {
    var helperObj = {
        Helpers: []
    };

    helperObj.getLastUpdatedTime = function (serverDateTimeParam) {
        
        var serverDateTime = serverDateTimeParam,
            clientDateTime = Date.now(),
            secs = (clientDateTime - serverDateTime) / 1000,
            days = Math.floor(secs / (60 * 60 * 24)),
            divisor_for_hours = secs % (60 * 60 * 24),
            hours = Math.floor(divisor_for_hours / (60 * 60)),
            divisor_for_minutes = divisor_for_hours % (60 * 60),
            minutes = Math.floor(divisor_for_minutes / 60),
            lastUpdated = '',
            daysText,
            hrText,
            minText;
        if (days > 0) {
            daysText = (days === 1) ? 'day, ' : 'days, ';
            lastUpdated = days + ' ' + daysText;
        }
        if (hours > 0) {
            hrText = (hours === 1) ? 'hour, ' : 'hours, ';
            lastUpdated += hours + ' ' + hrText;
        }
        if (minutes >= 0) {
            minText = (minutes === 1) ? 'minute ago' : 'minutes ago';
            lastUpdated += minutes + ' ' + minText;
        }
        if (lastUpdated.length == 0) {
        	lastUpdated = '0 minutes ago';
        }
        return lastUpdated;
    };
    
	helperObj.unixTimeToDateTime = function (unixTimeParam) {
		
		
		/* unixTimeParam: in millisec */
		var timeZone = $localStorage.timeZoneOffset,
			offsetOp = timeZone.slice(0,1),
			offsetHours = timeZone.split(':')[0],
			offsetMinutes = offsetOp + timeZone.split(':')[1],
			offsetTimeZone = (offsetHours*60*60 + offsetMinutes*60) * 1000,
			unixTime = (unixTimeParam + offsetTimeZone),
			a = new Date(unixTime),
			months =['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
			year = a.getUTCFullYear(),
			month = months[a.getUTCMonth()],
			date = a.getUTCDate(),
			hour = a.getUTCHours().toString().length==2?a.getUTCHours():'0' + a.getUTCHours(),
			min = a.getUTCMinutes().toString().length==2?a.getUTCMinutes():'0' + a.getUTCMinutes(),
			sec = a.getUTCSeconds().toString().length==2?a.getUTCSeconds():'0' + a.getUTCSeconds(),
			time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec ;
		
		
		return time;
	}
	
	helperObj.convertUTCToLocalTimezone = function (unixTimeParam){
		var timeZone = $localStorage.timeZoneOffset,
		offsetOp = timeZone.slice(0,1),
		offsetHours = timeZone.split(':')[0],
		offsetMinutes = offsetOp + timeZone.split(':')[1],
		offsetTimeZone = (offsetHours*60*60 + offsetMinutes*60) * 1000,
		unixTime = (unixTimeParam - offsetTimeZone);
		return unixTime;
	}
	
	
	/*helperObj.getAddressForShipment = function(fullAddress) {
		var fullAddArr=fullAddress.split(',');
        var fullAddArrLen=fullAddArr.length;
        var formatAddressObj={};
        if(fullAddArrLen<2){
            return formatAddressObj;
        }
        if(fullAddArrLen == 2){
        	// 1234 Noname Street, MyCity
            formatAddressObj.address1=fullAddArr[0].trim();
            formatAddressObj.address2='';
            formatAddressObj.city=fullAddArr[1].trim();
            formatAddressObj.postalCode='';
            formatAddressObj.state='';
            formatAddressObj.country='';
            return formatAddressObj;
        }
        if(fullAddArrLen == 3){
        	// 1234 Noname Street, MyCity, WA 99999
            formatAddressObj.address1=fullAddArr[0].trim();
            formatAddressObj.address2='';
            formatAddressObj.city=fullAddArr[1].trim();
            formatAddressObj.postalCode='';
            formatAddressObj.state=fullAddArr[2].trim();
            formatAddressObj.country='';
            var statePinCodeArr=formatAddressObj.state.split(' ');
            if (statePinCodeArr.length > 1){
                formatAddressObj.state=statePinCodeArr[0].trim();
                formatAddressObj.postalCode=statePinCodeArr[statePinCodeArr.length-1].trim();
            }
            return formatAddressObj;
        }
        if(fullAddArrLen == 4){
        	// 1234 Noname Street, MyCity, WA 99999, USA
            formatAddressObj.address1=fullAddArr[0].trim();
            formatAddressObj.address2='';
            formatAddressObj.city=fullAddArr[1].trim();
            formatAddressObj.postalCode='';
            formatAddressObj.state=fullAddArr[2].trim();
            formatAddressObj.country=fullAddArr[3].trim();
            var statePinCodeArr=formatAddressObj.state.split(' ');
            if (statePinCodeArr.length > 1){
                formatAddressObj.state=statePinCodeArr[0].trim();
                formatAddressObj.postalCode=statePinCodeArr[statePinCodeArr.length-1].trim();
            }
            return formatAddressObj;
        }
        var country=fullAddArr[fullAddArrLen-1];
        var city=fullAddArr[fullAddArrLen-3];
        var statePinCodeArr=fullAddArr[fullAddArrLen-2].split(' ');
        var addressHome=fullAddArr.slice(0,fullAddArrLen-3);
        var postalCode='';
        var state='';
        var address1='';
        var address2='';
        for(var i in statePinCodeArr){
            if(i==(statePinCodeArr.length-1)){
                postalCode=statePinCodeArr[i];
            }else if(statePinCodeArr[i]!==''){
                state= state + statePinCodeArr[i];
            }
        }
        address1=addressHome.slice(0,Math.ceil(addressHome.length/2)).join(',').trim();
        address2=addressHome.slice(Math.ceil(addressHome.length/2)).join(',').trim();
        
        formatAddressObj.address1=address1;
        formatAddressObj.address2=address2;
        formatAddressObj.city=city.trim();
        formatAddressObj.postalCode=postalCode.trim();
        formatAddressObj.state=state.trim();
        formatAddressObj.country=country.trim();
        return formatAddressObj;
    }*/
	
	helperObj.getSelectedShipmentLocationdata = function (shipmentLocationData,selectedProduct) {
		var shipmentMapData='';
		angular.forEach(shipmentLocationData, function (value, key) {
			if (selectedProduct.deviceId == value.deviceId && selectedProduct.packageId == value.packageId &&  selectedProduct.productId == value.productId) {
				shipmentMapData = shipmentLocationData[key];
			}
		});
		return shipmentMapData;
	}
	
	helperObj.getSelectedShipmentIndex = function (activeShipmentsArr,selectedProduct) {
		var shipmentIndex='';
		angular.forEach(activeShipmentsArr, function (value, key) {
			if (selectedProduct.deviceId == value.deviceId && selectedProduct.packageId == value.packageId &&  selectedProduct.productId == value.productId) {
				shipmentIndex = key;
			}
		});
		return shipmentIndex;
	}
	
	helperObj.validateLatLong = function (latitude,longitude) {
		var isValidLat = function(val){
	        return (isNumeric(val) && (val >= -90.0) && (val <= 90.0));
	    }

	    var isValidLng = function (val) {
	        return (isNumeric(val) && (val >= -180.0) && (val <= 180.0));
	    }

	    var isNumeric = function (n) {
	        return !isNaN(parseFloat(n)) && isFinite(n);
	    }
	    
		return (isValidLat(latitude) && isValidLng(longitude));
	}
	
    // return the helper object ...
    return helperObj;
}

angular.module('rfxcelApp').factory("generalUtilityService", generalUtilityService);

generalUtilityService.$inject = ['$localStorage'];