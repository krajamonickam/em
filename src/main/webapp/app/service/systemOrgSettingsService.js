angular.module('rfxcelApp').factory("systemOrgSettingsService", systemOrgSettingsService);

systemOrgSettingsService.$inject = ['$resource', '$location', 'sharedDataService', '$localStorage'];

function systemOrgSettingsService($resource, $location, sharedDataService, $localStorage) {
	
	//alert("EDIT WORKING");
	// getOrgSettigns.json
	// getSystemSettigns.json
	
	//   return {settingValList: $resource('app/local-server/getSystemSettigns.json', {}, {
	if($localStorage.username==='System'){
	    //return {getSettingList: $resource('app/local-server/getSystemSettigns.json', {}, {
		return {
			getSettingList: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/setting' + '/getSettingList'),
			editSettingVal: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/setting' + '/addSettings')
	    }
	}else{

	    //return {getSettingList: $resource('app/local-server/getOrgSettigns.json', {}, {
		return {
			getSettingList: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/setting' + '/getSettingList'),
	    editSettingVal: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/setting' + '/addSettings')
	    
	    }
    
		
	}
}
