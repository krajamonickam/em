angular.module('rfxcelApp').service('OrgnizationLocalService', OrgnizationLocalService);

OrgnizationLocalService.$inject = [];

function OrgnizationLocalService() {

    var arrOrgList = [];
    var selectedOrg = {};


    this.setOrgList = function (list) {
        arrOrgList = list;
    }

    this.getOrgList = function () {
        return arrOrgList;
    }

    this.setSelectedOrg = function (obj) {
        selectedOrg = obj;
    }
    this.getSelectedOrg = function () {
        return selectedOrg;
    }
}
