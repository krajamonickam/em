angular.module('rfxcelApp').factory("deviceDataService", deviceDataService);

deviceDataService.$inject = ['$resource', '$location','sharedDataService'];
    
		
function deviceDataService($resource, $location, sharedDataService){
 	    
    //"http://localhost:58438/sendum-1.0/sensor/getAttrData"
    //local-server/getAttributeData-res.json

	return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getAttrData')   
}	
