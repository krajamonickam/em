angular.module('rfxcelApp').factory('AuditLogService', AuditLogService);
AuditLogService.$inject = ['$resource', '$location', 'sharedDataService'];

function AuditLogService($resource, $location, sharedDataService) {
    
    var service={
    	getAuditLogs: getAuditLogs
    }
    
    function getAuditLogs(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/audit/getAuditLogs')   
    }

    return service;
}