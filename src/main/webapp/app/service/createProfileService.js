angular.module('rfxcelApp').factory('createProfileService', createProfileService);
createProfileService.$inject = ['$resource', '$location','sharedDataService'];

function createProfileService($resource, $location, sharedDataService) {
    return {
        
        createProfile: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/profile/createProfile', {}, {
            post: {
                method: 'POST'
            }
        }),       
        
        modifyProfile: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/profile/updateProfile', {}, {
            post: {
                method: 'POST'
            }
        }),
        
        getProfile: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port +  sharedDataService.getURLPath() + '/rest/profile/getProfile', {}, {
            post: {
                method: 'POST'
            }
        }),
              
        getAttributesByDeviceType: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/deviceType/getAttributesByDeviceType', {}, {
            post: {
                method: 'POST'
            }
        }),
		
        getGeolocationRadiusAndFormat: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/profile/getGeolocationRadiusAndFormat', {}, {
            post: {
                method: 'POST'
            }
        }),
    }
}