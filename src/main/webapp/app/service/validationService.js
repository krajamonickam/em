angular.module('rfxcelApp').service('validationService', validationService);

//sharedDataService.$inject = ['$location'];

function validationService() {
	
	this.isValidEmail = function (email) {
		var atpos = email.indexOf("@"),
			dotpos = email.lastIndexOf("."),
			validationStatus = false,
			message = "";
		if (atpos<1 || dotpos<atpos+2 || dotpos+2>=email.length) {
			validationStatus = true;
			message = "Expecting email format abc@xyz.com";
		}
		return {'status': validationStatus, 'message': message};
	};
	
	this.isValidUserLogin = function (userLogin) {
		var validationStatus = false,
			message = "";
		for (i = 0, length = userLogin.length; i < length; i++) {
			keycode = userLogin.charCodeAt(i);
		    if (!(keycode ===32) && // white space
		    	!(keycode ===45) && // hyphen
				!(keycode ===46) && // dot
				!(keycode ===64) && // at-sign
				!(keycode ===95) && // underscore
	  	        !(keycode > 47 && keycode < 58) && // numeric (0-9)
	  	        !(keycode > 64 && keycode < 91) && // upper alpha (A-Z)
	  	        !(keycode > 96 && keycode < 123)) { // lower alpha (a-z)
	  				validationStatus = true;
					message = "Expecting login ID format alpha-numeric";
			}
		}
		return {'status': validationStatus, 'message': message};
	};

	this.isValidPhone = function(phone) {
		var isValid = false, message = "";
		if (phone === '' || phone === undefined) {
			isValid = true;
		} 
		else if (phone.match(/^(\+1[-. ])?\d{3}[-. ]?\d{3}[-. ]?\d{4}$/)) {
				isValid = true;
		}
		return {
			'status' : isValid,
			'message' : isValid ? '': 'Expecting phone number format [+1-]999-999-9999'
		};
	};
}