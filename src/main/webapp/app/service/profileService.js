angular.module('rfxcelApp').factory('profileService', profileService);
profileService.$inject = ['$resource', '$location', 'sharedDataService'];

function profileService($resource, $location, sharedDataService) {

    /*var service = {
        getProfile: getProfile,
        addProfile: addProfile,
        editProfile: editProfile,
        deleteProfile: deleteProfile
    };

    function getProfile() {
        return $resource('app/local-server/getProfileList.json').get().$promise;
    }

    function addProfile() {
        return $resource('app/local-server/getProfileList.json').get().$promise;
    }

    function editProfile() {
        return $resource('app/local-server/getProfileList.json').get().$promise;
    }

    function deleteProfile() {
        return $resource('app/local-server/getProfileList.json').get().$promise;
    }

    return service;*/
	
	return {
    	//getProfileData: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/getProfiles', {}, {
    	getProfileData: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/profile/getProfiles', {}, {
            post: {
                method: 'POST'
            }
        }),
		
		deleteProfile: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/profile/deleteProfile', {}, {
            post: {
                method: 'POST'
            }
        }),
		
    }
}
