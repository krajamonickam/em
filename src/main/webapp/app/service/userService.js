angular.module('rfxcelApp').factory('UserService', UserService);
UserService.$inject = ['$resource', '$location', 'sharedDataService'];

function UserService($resource, $location, sharedDataService) {

	  return {
	    getUser: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/getUserList'),
	    getUsers: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/getUsers'),
	    getAlertGroup: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/getAlertGroupList'),
	    getUser: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/getUserList'),
	    addUser: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/addUser'),
	    updateUser: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/updateUser'),
	    deleteUser: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/deleteUser'),
	    setPassword: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/user/setPassword'),
	    loginAsSupportUser: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/loginAsSupportUser'),
	    updateKirsenDeviceKeys: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/updateKirsenDeviceKeys')
	  }
	}