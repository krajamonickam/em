angular.module('rfxcelApp').factory("simulatorService", simulatorService);

simulatorService.$inject = ['$resource', '$location', 'sharedDataService'];
    
		
function simulatorService($resource, $location, sharedDataService){
 	    
	return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/startSimulator', {}, {
      post: {
            method: 'POST'
        }
    });   
}	
