angular.module('rfxcelApp').service('commonConfigService', commonConfigService);

function commonConfigService() {
    
    var nullAttrResponseCode = -999;
    
    this.getNullAttrResponseCode = function () {
        return nullAttrResponseCode;
    }

}