angular.module('rfxcelApp').service('sharedDataService', sharedDataService);

//sharedDataService.$inject = ['$location'];

function sharedDataService() {

    var arrActiveshipment = [];
    var selectedShipment;
    var configRfxcelItem;
    var favoriteOption;
    var deviceId;
    var packageId;
    var productId;
    var lastVisitedUIState;
    var urlPath;
    var rtsItemId;
    var rtsSerialNumber;

    var nearestLocation;
    var selectedDeviceMarkerColor;

    var selectedShipmentType;
    var selectedUser;
    var groupId;
    var editOrg;
    var selectedProfile;
	var alert=[];
    var alertGroup=[];
    var modifyAlert;
    var selectedOrgId;
    var successMessage;
    var navMenu;
    var showMapAttribute;
    var shipmentList;
    var alertMapData;
    var traceId;
    var shipmentTypeArr = [{
            type: "Pending Shipments",
            typeId: 2
        },{
            type: "Completed Shipments",
            typeId: 3
        },{
        //     type: "Events",
        //     typeId: 4
        //},{
        //    type: "Items",
        //    typeId: 5
        //},{
            type: "Current Shipments",
            typeId: 1
        }]

    var productTypeArr = [
        {
            type: "Current Products",
            typeId: 1
        },
        {
            type: "Pending Products",
            typeId: 2
        },
        {
            type: "Completed Products",
            typeId: 3
        }]

    this.setURLPath = function (path) {
        urlPath = path;
    }

    this.getURLPath = function () {
        return urlPath;
    }

    this.setConfigRfxcelItem = function (item) {
        configRfxcelItem = item;
    }

    this.getConfigRfxcelItem = function () {
        return configRfxcelItem;
    }

    this.activeShipmentList = function (value) {
        arrActiveshipment = value;
    }

    this.activeShipmentList = function () {
        return arrActiveshipment;
    }

    //IMP: setter for selected device id
    this.setSelectedDeviceId = function (value) {
        deviceId = value;
    }
    this.getSelectedDeviceId = function () {
        return deviceId;
    }

    //IMP: setter for selected shipment id
    this.setSelectedShipmentId = function (value) {
        packageId = value;
    }
    this.getSelectedShipmentId = function () {
        return packageId;
    }

    //IMP: setter for selected Product id
    this.setSelectedProductId = function (value) {
        productId = value;
    }
    this.getSelectedProductId = function () {
        return productId;
    }

    //item type to show in shipment filter drop downlist
    this.getItemTypeArr = function () {

        if (configRfxcelItem == "Shipments") {
            return shipmentTypeArr;
        }
        return productTypeArr;

    }

    this.setSelectedShipment = function (value) {
		selectedShipment = value;
    }

    this.selectedShipment = function () {
		return selectedShipment;
    }

    this.setNearestLocation = function (value) {
        nearestLocation = value;
    }

    this.nearestLocation = function () {
        return nearestLocation;
    }

    this.setSelectedDeviceMarkerColor = function (value) {
        selectedDeviceMarkerColor = value;
    }

    this.selectedDeviceMarkerColor = function () {
        return selectedDeviceMarkerColor;
    }

    this.setSelectedFavoriteOption = function (value) {
        favoriteOption = value;
    }

    this.getSelectedFavoriteOption = function () {
        return favoriteOption;
    }

    this.setLastVisitedUIState = function (value) {
        lastVisitedUIState = value;
    }

    this.getLastVisitedUIState = function () {
        return lastVisitedUIState;
    }

    this.setSelectedShipmentType = function (value) {
        selectedShipmentType = value;
    }

    this.getSelectedShipmentType = function () {
        return selectedShipmentType;
    }
    this.setSelectedUser = function (value) {
        selectedUser = value;
    }

    this.getSelectedUser = function () {
        return selectedUser;
    }
    this.setGroupId = function (value) {
        groupId = value;
    }
    this.getGroupId = function () {
        return groupId;
    }
    this.setEditOrg = function (value) {
        editOrg = value;
    }
    this.getEditOrg = function () {
        return editOrg;
    }
    
    this.setSelectedProfile = function (value) {
        selectedProfile = value;
    }

    this.getSelectedProfile = function () {
        return selectedProfile;
    }
	this.setAlert = function (value) {
        alert = value;
    }
    this.getAlert = function () {
        return alert;
    }
    this.setAlertGroup = function (value) {
        alertGroup = value;
    }
    this.getAlertGroup = function () {
        return alertGroup;
    }
    this.setModifyAlert = function (value) {
        modifyAlert = value;
    }
    this.getModifyAlert = function () {
        return modifyAlert;
    }
    this.setSelectedOrgId = function (value) {
    	selectedOrgId = value;
    }

    this.getSelectedOrgId = function () {
        return selectedOrgId;
    }
    this.setSuccessMessage = function (value) {
    	successMessage = value;
    }

    this.getSuccessMessage = function () {
        return successMessage;
    }
    this.setNavMenuList = function (value) {
    	navMenu = value;
    }

    this.getNavMenuList = function () {
        return navMenu;
    }
    
    this.setShowMapAttribute = function (value) {
    	showMapAttribute = value;
    }

    this.getShowMapAttribute = function () {
        return showMapAttribute;
    }
    
    this.setShipmentList = function (value) {
    	shipmentList = value;
    }

    this.getShipmentList = function () {
        return shipmentList;
    }
    this.setAlertMapData = function (value) {
    	alertMapData = value;
    }
    this.getAlertMapData = function () {
        return alertMapData;
    }
    this.setTraceId = function (value) {
    	traceId = value;
    }

    this.getTraceId = function () {
        return traceId;
    }
    
    this.setRTSTraceId = function (value) {
    	rtsItemId = value;
    }

    this.getRTSTraceId = function () {
        return rtsItemId;
    }
    
    this.setRTSSerialNumber = function (value) {
    	rtsSerialNumber = value;
    }

    this.getRTSSerialNumber = function () {
        return rtsSerialNumber;
    }
}
