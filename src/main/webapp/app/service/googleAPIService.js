angular.module('rfxcelApp').service('googleAPIService', googleAPIService);

googleAPIService.$inject = ['$http', '$q', 'mapConfigService', '$localStorage'];

function googleAPIService($http, $q, mapConfigService, $localStorage) {

    return {
        getNearestLocation: function (lat, lng) {
            var url = mapConfigService.getGeolocationApi() + lat + ',' + lng + '&key=' + mapConfigService.getGoogleApiKey();

            return $q(function (resolve, reject) {

                $http.defaults.headers.common['Authorization'] = undefined;
                $http.defaults.headers.common['Content-Type'] = undefined;
                $http.defaults.headers.common['Access-Control-Allow-Origin'] = undefined;
                $http.defaults.headers.common['Shard'] = undefined;
                $http.defaults.headers.common['OrgId'] = undefined;

                var promise = $http.post(url);

                if (promise) {
                    resolve(promise);
                    $http.defaults.headers.common['Authorization'] = $localStorage.authToken;
                    $http.defaults.headers.common['Content-Type'] = "application/json";
                    $http.defaults.headers.common['Access-Control-Allow-Origin'] = "*";
                    $http.defaults.headers.common['Shard'] = $localStorage.shard;
                    $http.defaults.headers.common['OrgId'] = $localStorage.orgId;
                }
            })
        },
        
        getLatlngByAddress: function (address) {
            var url = mapConfigService.getGeoaddressApi() + address + '&key=' + mapConfigService.getGoogleApiKey();

            return $q(function (resolve, reject) {

                $http.defaults.headers.common['Authorization'] = undefined;
                $http.defaults.headers.common['Content-Type'] = undefined;
                $http.defaults.headers.common['Access-Control-Allow-Origin'] = undefined;
                $http.defaults.headers.common['Shard'] = undefined;
                $http.defaults.headers.common['OrgId'] = undefined;

                var promise = $http.post(url);

                if (promise) {
                    resolve(promise);
                    $http.defaults.headers.common['Authorization'] = $localStorage.authToken;
                    $http.defaults.headers.common['Content-Type'] = "application/json";
                    $http.defaults.headers.common['Access-Control-Allow-Origin'] = "*";
                    $http.defaults.headers.common['Shard'] = $localStorage.shard;
                    $http.defaults.headers.common['OrgId'] = $localStorage.orgId;
                }
            })
        }
    }
}