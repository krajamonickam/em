angular.module('rfxcelApp').factory("itemMapService", itemMapService);

itemMapService.$inject = ['$resource', '$location', 'sharedDataService'];


function itemMapService($resource, $location, sharedDataService) {
	var url=$location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath()+'/rest/';
	
	 return {
		 getTraceItemMapLocations: $resource(url + 'item/getClusterMapItemLocations'),
		 getItemListForMap: $resource(url + 'item/getItemListForMap'),
		 getNearByItemListForMap: $resource(url + 'item/getFindNearByListForMap'),
		 getEventListForMap: $resource(url + 'event/getEventListForMap'),
		 getNotificationListForMap: $resource(url + 'notification/getNotificationListForMap'),
	  }
	
}