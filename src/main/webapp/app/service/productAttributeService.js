angular.module('rfxcelApp').factory("productAttributeService", productAttributeService);

productAttributeService.$inject = ['$resource', '$location','sharedDataService'];
    
		
function productAttributeService($resource, $location, sharedDataService){
 	    
	return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getAttrInfo', {}, {
	      post: {
	            method: 'POST'
	        }
	    });   
}	
