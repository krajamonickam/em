angular.module('rfxcelApp').factory('OrgService', OrgService);
OrgService.$inject = ['$resource', '$location', 'sharedDataService'];

function OrgService($resource, $location, sharedDataService) {

  return {
    getOrg: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/organization/getOrganizations'),
    addOrg: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/organization/addOrganization'),
    modifyOrg: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/organization/modifyOrganization'),
    deleteOrg: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/organization/deleteOrganization'),
    getTimeZones: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/organization/getTimeZones')
  }
}