angular.module('rfxcelApp').factory('DeviceLogService', DeviceLogService);
DeviceLogService.$inject = ['$resource', '$location', 'sharedDataService'];

function DeviceLogService($resource, $location, sharedDataService) {
    
    var service={
        getDeviceLogs: getDeviceLogs
    }
    
    function getDeviceLogs(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/deviceLog/getDeviceLogs')   
    }
    
    return service;
}


