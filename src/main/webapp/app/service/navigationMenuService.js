angular.module('rfxcelApp').factory('NavigationMenuService', NavigationMenuService);
NavigationMenuService.$inject = ['$resource', '$location', 'sharedDataService'];

function NavigationMenuService($resource, $location, sharedDataService) {
	return {
    getNavMenus: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/menu/getMenuList')
  }
}