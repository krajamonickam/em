angular.module('rfxcelApp').service('UserLocalService', UserLocalService);

UserLocalService.$inject = [];

function UserLocalService() {

    var arrUserList = [];
    var arrAlertGroup = [];
    var arrAlertList = [];
    var selectedUsersList = [];
    var arrDeviceTypeList=[];
    var arrAttributes=[];
    var arrDeviceList=[];

    this.setUserList = function (list) {
        arrUserList = list;
    }

    this.getUserList = function () {
        return arrUserList;
    }
    
    this.setAlertGroupList = function (list) {
    	arrAlertGroup = list;
    }
    
    this.getAlertGroupList = function () {
        return arrAlertGroup;
    }

    
    this.setAlertList = function (list) {
        arrAlertList = list;
    }
   
    this.getAlertList = function () {
        return arrAlertList;
    }
    
    this.setSelectedUsersList = function (list) {
    	selectedUsersList = list;
    }
    
    this.getSelectedUsersList = function () {
        return selectedUsersList;
    }
    this.setDeviceTypeList = function (list) {
    	arrDeviceTypeList = list;
    }
    
    this.getDeviceTypeList = function () {
        return arrDeviceTypeList;
    }
    this.setAttributeList = function (list) {
    	arrAttributes = list;
    }
    
    this.getAttributeList = function () {
        return arrAttributes;
    }
    
    this.setDeviceList = function (list) {
        arrDeviceList = list;
    }

    this.getDeviceList = function () {
        return arrDeviceList;
    }

}
