angular.module('rfxcelApp').factory('productAlertService', productAlertService);
productAlertService.$inject = ['$resource', '$location', 'sharedDataService'];

function productAlertService($resource, $location, sharedDataService) {
    return {
        getNotificationsAlerts: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getShipmentNotifications', {}, {
            post: {
                method: 'POST'
            }
        }),
        getNotificationsAlertsCount: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getNotificationCount', {}, {
            post: {
                method: 'POST'
            }
        }),
        getMonitorAlerts: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getMonitorAlerts', {}, {
            post: {
                method: 'POST'
            }
        })
    }
}