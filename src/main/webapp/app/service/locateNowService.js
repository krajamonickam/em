angular.module('rfxcelApp').factory("locateNowService", locateNowService);

locateNowService.$inject = ['$resource', '$location', 'sharedDataService'];

function locateNowService($resource, $location, sharedDataService) {
    
	var service={
			setLocationNow: setLocationNow,
	    }
	    
	    function setLocationNow(){
	        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/locateDevice')   
	    }
	return service;
}