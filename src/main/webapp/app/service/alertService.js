angular.module('rfxcelApp').factory('AlertService', AlertService);
AlertService.$inject = ['$resource', '$location', 'sharedDataService'];

function AlertService($resource, $location, sharedDataService) {
    
    var service={
        getAlert: getAlert,
        addAlert: addAlert,
        modifyAlert: modifyAlert,
        deleteAlert: deleteAlert,
        getAlertAttributeList: getAlertAttributeList,
        getAlertDetailsByAttrName: getAlertDetailsByAttrName,
        getDefaultAlertList: getDefaultAlertList
    }
    
    function getAlert(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/getAlertList')   
    }
    
    function addAlert(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/addAlert')
    }
    
    function modifyAlert(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/modifyAlert')
    }
    
    function deleteAlert(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/deleteAlert')
    }
    
    function getAlertAttributeList(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/getAlertAttributeList')
    }
    
    function getAlertDetailsByAttrName(){
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/getAlertDetailsByAttrName')
    }
    
    function getDefaultAlertList(){
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alert/getDefaultAlertList')
   }
	   
    return service;	  
}

    



