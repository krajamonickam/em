angular.module('rfxcelApp').factory("productDetailsService", productDetailsService);

productDetailsService.$inject = ['$resource', '$location', 'sharedDataService'];


function productDetailsService($resource, $location, sharedDataService) {

    return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getShipmentDetails', {}, {
        post: {
            method: 'POST'
        }
    });
}