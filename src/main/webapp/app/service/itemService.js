angular.module('rfxcelApp').factory("itemService", itemService);

itemService.$inject = ['$resource', '$location', 'sharedDataService'];


function itemService($resource, $location, sharedDataService) {

    return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/items', {}, {
        post: {
            method: 'POST'/*,
            headers: {
                "Content-Type": "application/json",
                "Authorization": $localStorage.authToken,
                "Access-Control-Allow-Origin": "*"
            }*/
        }
    });
}