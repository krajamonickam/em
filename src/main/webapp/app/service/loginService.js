angular.module('rfxcelApp').factory('loginService', loginService);
loginService.$inject = ['$resource', '$location', 'sharedDataService'];

function loginService($resource, $location, sharedDataService) {
    return {
        login: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/login', {}, {
            post: {
                method: 'POST'
            }
        }),
        logout: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/logout', {}, {
            post: {
                method: 'POST'
            }
        }),
        forgotPassword: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/forgetPassword', {}, {
            post: {
                method: 'POST'
            }
        }),
        resetPassword: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/setPassword', {}, {
            post: {
                method: 'POST'
            }
        }),
        getConfigData: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/getConfigData', {}, {
            post: {
                method: 'POST'
            }
        }),
        getShard: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/user/shard/:userLogin', {federated:'true'}, {
            get: {
                method: 'GET'
            }
        })
    }
}