angular.module('rfxcelApp').factory('AlertGroupService', AlertGroupService);
AlertGroupService.$inject = ['$resource', '$location', 'sharedDataService'];

function AlertGroupService($resource, $location, sharedDataService) {

    var service = {
        getAlertGroup: getAlertGroup,
        addAlertGroup: addAlertGroup,
        editAlertGroup: editAlertGroup,
        deleteAlertGroup: deleteAlertGroup
    };
    
    function getAlertGroup() {
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alertGroup/getAlertGroupList')
    }

    function addAlertGroup() {
    	
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alertGroup/addAlertGroup')
    }

    function editAlertGroup() {
         return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alertGroup/updateAlertGroup')
    }

    function deleteAlertGroup() {
        return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/alertGroup/deleteAlertGroup')
    }

    return service;
}

