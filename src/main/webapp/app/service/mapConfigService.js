angular.module('rfxcelApp').service('mapConfigService', mapConfigService);

function mapConfigService() {
    this.getMarkerColorCode = function () {
        var green = '#58B820';
        var yellow = '#F7C527';
        var red = '#F1575A';
        return [green, yellow, red];
    }
    
    this.getGeolocationApi = function () {
        return "https://maps.googleapis.com/maps/api/geocode/json?latlng=";
    }
    
    this.getGoogleApiKey = function () {
        return "AIzaSyC2SS3-yvoiTsjLC2DmC1tM8sOMgw7i_wA";
    }
    
    this.getGeoaddressApi = function () {
        return "https://maps.googleapis.com/maps/api/geocode/json?address=";
    }
}