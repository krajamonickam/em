angular.module('rfxcelApp').factory("activeShipmentsService", activeShipmentsService);

activeShipmentsService.$inject = ['$resource', '$location', 'sharedDataService'];

function activeShipmentsService($resource, $location, sharedDataService) {

    //"http://localhost:58438/sendum-1.0/sensor/getShipmentList"
    //local-server/getActiveShipment-res.json
    return $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getShipmentList', {}, {
        post: {
            method: 'POST'
        }
    });
}