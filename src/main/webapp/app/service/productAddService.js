angular.module('rfxcelApp').factory('productAddService', productAddService);
productAddService.$inject = ['$resource', '$location', 'sharedDataService'];

function productAddService($resource, $location, sharedDataService) {
    return {
        getLocationName: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/findLoctionName', {}, {
            post: {
                method: 'POST'
            }
        }),
        getLocationAddress: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/findAddress', {}, {
            post: {
                method: 'POST'
            }
        }),
        addProductDetail: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/addShipment', {}, {
            post: {
                method: 'POST'
            }
        }),
        getProductDetails: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getShipmentDetails', {}, {
            post: {
                method: 'POST'
            }
        }),
        getProduct: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getProducts', {}, {
            post: {
                method: 'POST'
            }
        }),
		getDeviceByID: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getDeviceByID', {}, {
            post: {
                method: 'POST'
            }
        }),
		getProfiles: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port +  sharedDataService.getURLPath() + '/rest/profile/getProfiles', {}, {
            post: {
                method: 'POST'
            }
        }),
        getProfilesByDeviceType: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port +  sharedDataService.getURLPath() + '/rest/profile/getProfilesByDeviceType', {}, {
            post: {
                method: 'POST'
            }
        }),
        disassociate: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/disassociate', {}, {
            post: {
                method: 'POST'
            }
        }),
        getPackage: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/getPackageId', {}, {
            post: {
                method: 'POST'
            }
        }),
        modifyProductDetail: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/sensor/updateShipment', {}, {
            post: {
                method: 'POST'
            }
        }),
        getHomeAddressByProfileId: $resource($location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest/profile/getProfileGeopoints')
    }
}