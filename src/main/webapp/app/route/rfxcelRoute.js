angular.module('rfxcelApp').config(function ($stateProvider, $urlRouterProvider) {
    $urlRouterProvider.otherwise("/app/dashboard/showMap");
    
    var pathValue;
    $urlRouterProvider.rule(function($injector, $location) {
        if ($location.path() == '/resetPassword') {
            pathValue = $location.url();
            return pathValue;
        }      
    });
	
    $stateProvider

        .state('login', {
        url: '/login',
        templateUrl: 'app/view/login.html',
        controller: 'loginController'

    })

    .state('app', {
        url: '/app',
        abstract: true, //marking abstract state, can't be loaded directly!
        /*resolve: {

            // A string value resolves to a service
            activeShipmentsSerrvice: 'activeShipmentsSerrvice',
           
            // A function value resolves to the return
            activeShipmentsData: function (activeShipmentsSerrvice) {
                
                return activeShipmentsSerrvice.post().$promise;
            }
        },*/
        templateUrl: 'app/view/main.html',
        controller: 'mainController',
        controllerAs: 'vm'
    })

    .state('app.dashboard', {
        url: "/dashboard",
        views: {
            '': {
                templateUrl: 'app/view/dashboard.html',
                controller: 'dashboardController',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.products', {
        url: "/products",
        views: {
            '': {
                templateUrl: 'app/view/products.html',
                controller: 'productController',
                controllerAs: 'vm'
            }
        }

    })

    .state('app.productsDetails', {
        url: "/productDetails",
        views: {
            '': {
                templateUrl: 'app/view/productDetails.html',
                controller: 'productDetailController',
                controllerAs: 'vm'
            }
        }

    })

    .state('app.productsDetails.map', {
        url: "/map",
        views: {
            '': {
                templateUrl: 'app/view/partials/product-map.html',
                controller: 'productMapController'
                    /*,
                                        controllerAs: 'vm'*/
            }
        }
    })

    .state('app.productsDetails.data', {
        url: "/data",
        views: {
            '': {
                templateUrl: 'app/view/partials/product-data.html',
                controller: 'productDataCtrl',
            }
        }
    })

    .state('app.productsDetails.alert', {
        url: "/alerts",
        views: {
            '': {
                templateUrl: 'app/view/partials/product-alert.html',
                controller: 'productAlertCtrl',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.dashboard.showMap', {
        url: '/showMap',
        views: {
            '': {
                templateUrl: 'app/view/dashboardShowMap.html',
                controller: 'dashboardShowMapController'
            }
        }
    })

    .state('app.dashboard.mapMarkerPath', {
        url: "/mapMarkerPath",
        views: {
            '': {
                templateUrl: 'app/view/mapMarkerPath.html',
                //templateUrl: 'view/app.dashboard.mapMarkerPath.html',
                //controller: 'mapMarkerPathController',
                controller: 'mapPathParentController'
            }
        },
    })

    .state('app.dashboard.alert', {
        url: "/alerts",
        views: {
            '': {
                templateUrl: 'app/view/dashboard-alert.html',
                controller: 'productAlertCtrl',
                controllerAs: 'vm'
            }
        }
    })

    .state('app.addproduct', {
        url: '/addproduct',
        views: {
            '': {
                templateUrl: 'app/view/productAdd.html',
                controller: 'productAddCtrl'
            }
        }
    })

    .state('forgotPassword', {
        url: '/forgotPassword',
        views: {
            '': {
                templateUrl: 'app/view/forgotPassword.html',
                controller: 'forgotPasswordCtrl'
            }
        }
    })

    .state('resetPassword', {
        url: pathValue,
        views: {
            '': {
                templateUrl: 'app/view/resetPassword.html',
                controller: 'forgotPasswordCtrl'
            }
        }
    })

    .state('passwordLanding', {
        url: '/passwordLanding',
        views: {
            '': {
                templateUrl: 'app/view/passwordLanding.html'
            }
        }
    })

    .state('app.logout', {
        url: "/logout",
        views: {
            '': {
                templateUrl: 'app/view/logout.html',
                controller: 'loginController',
            }
        },
    })
    .state('app.rts', {
        url: "/rts",
        external: true
    })
    
	.state('app.profile', {
        url: "/profile",
		templateUrl: 'app/view/profile/profileList.html',
        controller: 'profileController',
	})
	
    .state('app.createProfile', {
        url: '/createProfile',
        templateUrl: 'app/view/profile/createProfile.html',
        controller: 'createProfileCtrl',
        controllerAs: 'vm'
    })

    .state('app.modifyProfile', {
            url: '/modify',
            templateUrl: 'app/view/profile/modifyProfile.html',
            controller: 'modifyProfileCtrl',
            controllerAs: 'vm'
        })
        .state('app.settings', {
            url: "/sysOrgSettings",
            views: {
                '': {
                    templateUrl: 'app/view/systemSettings.html',
                    controller: 'systemOrgSettingsCtrl',
                    controllerAs: 'vm'
                }
            }
        })

    .state('createPassword', {
        url: '/createUserPassword?token',
        views: {
            '': {
                templateUrl: 'app/view/createPassword.html',
                controller: 'forgotPasswordCtrl'
            }
        }
    })

    .state('redirectLoginPage', {
        url: '/redirectLoginPage',
        templateUrl: 'app/view/redirectLoginPage.html',
        controller: 'forgotPasswordCtrl'
    })

    /***Organization states starts***/
    .state('app.organization', {
            url: "/organization",
            templateUrl: 'app/view/organization.html',
            controller: 'organizationCtrl',
            controllerAs: 'vm'
        })
        .state('app.addOrg', {
            url: "/addOrg",
            templateUrl: 'app/view/partials/org/addOrganization.html',
            controller: 'organizationAddCtrl',
            controllerAs: 'vm'
        })
        .state('app.modifyOrg', {
            url: "/modifyOrganization",
            templateUrl: 'app/view/partials/org/modifyOrganization.html',
            controller: 'modifyOrgCtrl',
            controllerAs: 'vm'
        })
        .state('app.user', {
            url: "/user/:selectedOrgId",
            templateUrl: 'app/view/user.html',
            controller: 'userCtrl',
            controllerAs: 'vm'
        })
        .state('app.addUser', {
            url: "/addUser",
            templateUrl: 'app/view/partials/user/addUser.html',
            controller: 'addUserCtrl'
        })
        .state('app.modifyUser', {
            url: "/modifyUser",
            templateUrl: 'app/view/partials/user/modifyUser.html',
            controller: 'modifyUserCtrl',
            controllerAs: 'vm'

        })
        .state('app.addAlert', {
            url: "/addAlert",
            templateUrl: 'app/view/partials/user/addAlert.html',
            controller: 'addAlertCtrl',
            controllerAs: 'vm'

        })
		.state('app.modifyAlert', {
            url: "/modifyAlert",
            templateUrl: 'app/view/partials/user/modifyAlert.html',
            controller: 'modifyAlertCtrl',
            controllerAs: 'vm'

        })
         .state('app.addAlertGroup', {
            url: "/addAlertGroup",
            templateUrl: 'app/view/partials/user/addAlertGroup.html',
            controller: 'addAlertGroupCtrl',
            controllerAs: 'vm'

        })
        
        .state('app.modifyAlertGroup', {
            url: "/modifyAlertGroup",
            templateUrl: 'app/view/partials/user/modifyAlertGroup.html',
            controller: 'modifyAlertGroupCtrl',
            controllerAs: 'vm'
        })
        .state('app.addDeviceType', {
            url: "/addDeviceType",
            templateUrl: 'app/view/partials/user/addDeviceType.html',
            controller: 'addDeviceTypeCtrl',
            controllerAs: 'vm'
        })
        .state('app.addDevice', {
            url: "/addDevice",
            templateUrl: 'app/view/partials/user/addDevice.html',
            controller: 'addDeviceCtrl',
            controllerAs: 'vm'
        })
        .state('app.modifyDevice', {
            url: "/modifyDevice",
            templateUrl: 'app/view/partials/user/modifyDevice.html',
            controller: 'modifyDeviceCtrl',
            controllerAs: 'vm'
        })
        .state('app.modifyDeviceType', {
            url: "/modifyDeviceType",
            templateUrl: 'app/view/partials/user/modifyDeviceType.html',
            controller: 'modifyDeviceTypeCtrl',
            controllerAs: 'vm'
        })
        .state('app.deviceLogs', {
            url: "/deviceLogs",
            templateUrl: 'app/view/deviceLogs.html',
            controller: 'deviceLogsCtrl'
        })
        .state('app.modifyproduct', {
            url: "/modifyproduct",
            templateUrl: 'app/view/productModify.html',
            controller: 'productModifyCtrl'
        })
        .state('app.auditLogs', {
            url: "/auditLogs",
            templateUrl: 'app/view/auditLogs.html',
            controller: 'auditLogsCtrl'
        })
        /*Event Details*/
        .state('app.traceEventDetails', {
            url: "/trace/traceEventDetails/:eventId/:deviceId",
            templateUrl: 'app/view/traceEventsDetails.html',
            controller: 'traceEventsDetailsController',
        })
		.state('app.traceItemDetails', {
            url: "/trace/traceItemDetails/:traceId/:deviceId",
            views: {
                '': {
                    templateUrl: 'app/view/trace_items_details.html',
                    controller: 'traceItemDetailsController',
                    controllerAs: 'vm'
                }
            }
		})
		 .state('app.trackItem', {
            url: "/trace/trackItem/:traceId",
            views: {
                '': {
                    templateUrl: 'app/view/trace/trace_item_map.html',
                    controller: 'itemMapController',
                    controllerAs: 'vm'
                }
            }
        })
        /***End: Organization states***/

});