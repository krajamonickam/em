'use strict';

/*angular.module('rfxcelApp', ['ngMaterial', 'ui.router', 'ngResource', 'ngMessages', 'ngStorage'])

.run(appInit);*/

angular.module('rfxcelApp', ['ngMaterial', 'ui.router', 'ngResource', 'ngMessages', 'ngStorage', 'rzModule', 'ui.bootstrap', 'ui.multiselect', 'ui.grid', 'ui.grid.pagination','ui.grid.treeView','ui.grid.expandable', 'ui.grid.resizeColumns','ngAnimate','ui.grid.selection', 'ngTouch'])

.run(appInit);


function appInit($rootScope, $state, $location, sessionService, sharedDataService, $localStorage, $http, $window) {
    sharedDataService.setConfigRfxcelItem("Shipments");
    sharedDataService.setURLPath("");
    //sharedDataService.setURLPath("/sensor");
    $rootScope.appTitle = "Integrated Monitoring";
    //$rootScope.appTitle = "Critical Asset Tracking";
    
    if ($location.path() == '/resetPassword') {
        $state.go('resetPassword');
    }

    //On state change start
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (toState.external) {
            event.preventDefault();
            var token = window.btoa( $localStorage.username + " " + $localStorage.password);
            var rtsURL = $localStorage.rtsURL;
            rtsURL = rtsURL.replace('rts/rest/', '');
            rtsURL = rtsURL + '#/autologin/' + token;
            $window.open(rtsURL);
            return;
        }

        var isLogin = toState.name === "login";
        if (isLogin) {
            return; // no need to redirect 
        } else {
            $rootScope.userName = $localStorage.username;
            $rootScope.systemDemo = $localStorage.systemdemo;
            $rootScope.clickToChat = $localStorage.clickToChat;
            $rootScope.fetchESData = $localStorage.fetchESData;
            $rootScope.newProfileScreen = $localStorage.newProfileScreen;
            $rootScope.alertDeviceLogSearch = $localStorage.alertDeviceLogSearch;
            $http.defaults.headers.common['Authorization'] = $localStorage.authToken;
            $http.defaults.headers.common['Content-Type'] = "application/json";
            $http.defaults.headers.common['Access-Control-Allow-Origin'] = "*";
            $http.defaults.headers.common['Shard'] = $localStorage.shard;
            $http.defaults.headers.common['OrgId'] = $localStorage.orgId;
        }
        
        if(toState.name==="app.dashboard.alert"){
            $rootScope.viewAlertState="app.dashboard.alert";
        }
        
        if ( ($localStorage.authToken === null || $localStorage.authToken === undefined) && (toState.name !== 'forgotPassword') ) {
        	event.preventDefault();
            $state.go('login');
        }
        
     

        if (fromState) {
            if (fromState.name === "app.productsDetails.data" || fromState.name === "app.productsDetails.alert" ||
                fromState.name === "app.productsDetails.map" || fromState.name === "app.productsDetails") {
                return;
            }
            sharedDataService.setLastVisitedUIState(fromState.name);
        }
    });

    /*On state change success*/
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams, error) {


    });

    /* on state change error, print error in console*/
    $rootScope.$on('stateChangeError', function (event, toState, toParams, fromState, fromParams, error) {
        console.log(error);
    })
}