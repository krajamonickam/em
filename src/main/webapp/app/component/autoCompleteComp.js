angular.module('rfxcelApp').component('autoCompleteComp', {
    templateUrl: 'app/view/partials/autocomplete.html',
    controller: autoCompleteCtrl,
    controllerAs: 'ctrl',
    bindings: {
        onDeviceSelection: '&',
        onTextEntered: '&'
    }
});