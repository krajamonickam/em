angular.module('rfxcelApp').controller('mainController', mainController);

mainController.$inject = ['$scope', '$rootScope', "$state", 'sharedDataService', '$localStorage', '$mdDialog','NavigationMenuService', '$mdSidenav'];

function mainController($scope, $rootScope, $state, sharedDataService, $localStorage, $mdDialog,NavigationMenuService,  $mdSidenav) {
	$scope.appTitle = $rootScope.appTitle;
    $scope.$on('genericErrorEvent', genericErrorHandler);
    var errorShown = false;
    getNavigationMenus();

    /**this generic error handling method for handling common
    errors like invalid authentication token for all the rest API's**/
    function genericErrorHandler(event, data) {

        if (!errorShown) {
            var confirm = $mdDialog.confirm({
                    onComplete: function afterShowAnimation() {
                        var $dialog = angular.element(document.querySelector('md-dialog')),
                            $actionsSection = $dialog.find('md-dialog-actions'),
                            $cancelButton = $actionsSection.children()[0];
                        angular.element($cancelButton).addClass('mdCancelBtn');
                    }
                })
                .title('Session Expired!')
                .textContent(data.responseMessage)
                .ok('Login');

            errorShown = true;

            $mdDialog.show(confirm).then(function () {
                errorShown = false;
                $localStorage.$reset();
                $state.go('login');
            });

        }
    }

    $scope.confTopNav = sharedDataService.getConfigRfxcelItem();

    $scope.showHelp = function () {
        window.open('./help/index.html', '_blank');
    };

    /*$rootScope.activeTabNumbTopNav = 1;
    $scope.selectedTabTopNav = function (tabNum) {
        $rootScope.activeTabNumbTopNav = tabNum;
    };*/
    
    function getNavigationMenus() {
    	var reqObject = {
        	"orgId": $localStorage.orgId
        };

        NavigationMenuService.getNavMenus.save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus === "success") {
                    sharedDataService.setNavMenuList(response.menuList);
                    $scope.navMenuList=sharedDataService.getNavMenuList();
                    $rootScope.activeTabNav = $scope.navMenuList[0].menuCode;
                    if($rootScope.viewAlertState=="app.dashboard.alert"){
                        $rootScope.activeTabNav = 4;
                    }
                } else {
                    $scope.message = response.responseMessage;
                }
            },
            function (error) {
                console.log(error);
            });
    }
    $scope.selectedTabTopNav = function (tabNum) {
        $rootScope.activeTabNav = tabNum;
    };

    //loads default state
    //$state.go('app.dashboard');
    
    $scope.isNotPopUp = true; // If the state is a popup or not
	
	/* register module here*/
	
	$scope.$on('isShowHeaderFooter', function(event, data) { // Disabling nav menu for pop up
		$scope.isNotPopUp = data;
	});

    $scope.logoClickedHandler = function () {
    	$rootScope.activeTabNav = $scope.navMenuList[0].menuCode;
        $state.go('app.dashboard.showMap');
       
    };
    
    
    $scope.toggleLeft = buildToggler('left');

    function buildToggler(componentId) {
      return function() {
        //$mdSidenav(componentId).toggle();
      };
    }
    $scope.displayMenu = function(){
    	//$mdSidenav('left').open();
    }
    $scope.hideMenu = function(){
    	//$mdSidenav('left').close();
    }
    
}
