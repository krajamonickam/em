angular.module('rfxcelApp').controller('deviceLogsCtrl', deviceLogsCtrl);
deviceLogsCtrl.$inject = ['$scope', '$state','sharedDataService', 'DeviceLogService','$localStorage'];

function deviceLogsCtrl($scope, $state,sharedDataService,DeviceLogService,$localStorage) {
    $scope.noLogsFound='';
    
    $scope.getDeviceLogs = function(deviceId){
    	getDeviceLogs(deviceId);
	}
    function getDeviceLogs(deviceId){
        var reqObject = {
    	  "searchString":$scope.deviceLog,
          "deviceLog": {
		      "deviceId": deviceId,
		      "orgId": $localStorage.orgId
	       }
    	}

        DeviceLogService.getDeviceLogs().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.deviceLogs = response.deviceLogList;
                if($scope.deviceLogs.length==0){
                	if($localStorage.fetchESData === true || $localStorage.alertDeviceLogSearch === true){
                		$scope.noLogsFound=response.responseMessage;
                	}else{
                		$scope.noLogsFound='No Device logs Available';
                	}
                }else {
                	$scope.noLogsFound="";
                }
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }


    $scope.$on('deviceSelected', onItemSelectHandler);
    function onItemSelectHandler(event, selectedDevice) {
        if (selectedDevice !== undefined){
        		getDeviceLogs(selectedDevice.deviceId);
        }else{
        	 $scope.deviceLogs = "";
        }
    }
}
