angular.module('rfxcelApp').controller('DialogController', function ($scope, helpdeskContactMessage, $mdDialog) {
	  $scope.helpdeskContactMessage = helpdeskContactMessage;
	  $scope.helpdeskContactLines = helpdeskContactMessage.split("\n");
	  $scope.closeDialog = function() {
		  $mdDialog.cancel();
	  }
});