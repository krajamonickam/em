angular.module('rfxcelApp').controller('dashboardController', dashboardController);

dashboardController.$inject = ['$scope', '$rootScope', '$state', 'activeShipmentsService', 'sharedDataService', '$filter', 'productAlertService', 'productFavoriteService', 'sessionService', 'mapConfigService', 'generalUtilityService', '$localStorage','productAddService','$mdDialog','locateNowService','$sessionStorage'];

function dashboardController($scope, $rootScope, $state, activeShipmentsService, sharedDataService, $filter, productAlertService, productFavoriteService, sessionService, mapConfigService, generalUtilityService, $localStorage,productAddService,$mdDialog,locateNowService,$sessionStorage) {
	if(typeof($sessionStorage.selectedOrgId)!=="undefined"){
		delete $sessionStorage.selectedOrgId;
	}
	var colorConversionArr = mapConfigService.getMarkerColorCode();
	$scope.shipmentType = sharedDataService.getConfigRfxcelItem();
	
	$scope.showHideSearchAction = function(searchAction){
		if(searchAction === true){
			$scope.btnSearchEnable = false;
			$scope.shipment = "";
	    	$scope.noShipmentFound = "";
		}else{
			$scope.btnSearchEnable = true;
			$scope.shipment = "";
	    	$scope.noShipmentFound = "";
		}
	}

	// Display the Dashboard tab as selected 
	$rootScope.activeDashboardTabNumb = 1;
	$scope.selectedDashboardTab = function (tabNum) {
		$rootScope.activeDashboardTabNumb = tabNum;
	};
	
	var loadShipmentCountInterval,
    refreshLoadActiveShipments,
    refreshShipmentCountInterval;
	$scope.isRtsDataFetch = false;

	getAlertsCount();

	// Get shipment types to be shown in select shipment type dropdown
	$scope.shipmentTypeArr = sharedDataService.getItemTypeArr();
	
	// By default show the current shipments as selected
	// Show Current shipment
	$scope.selectedItemType = $scope.shipmentTypeArr[4];
	//sharedDataService.setSelectedShipmentType($scope.selectedItemType.typeId);
	
	// On selecting the shipment type get the shipments by tupeId
	$scope.shipmentSelectionChange = function (typeId) {
    	$scope.noShipmentFound = "";
    	$scope.shipment = "";
		sharedDataService.setSelectedShipmentType(typeId);
		$scope.selectedItemType.typeId = typeId;
		loadActiveShipments(typeId);
	};

	// Filter to eliminate pending shipments to be shown in select shipment type dropdown
	$scope.shipmentType = function(item) {
		return (item.typeId !== 2) ? true: false;
	};
	
	$scope.loadActiveShipmentsSearch = function(selectedItemType){
    	$scope.noShipmentFound = "";
    	sharedDataService.setSelectedShipmentType(selectedItemType.typeId);
    	$scope.selectedItemType.typeId = selectedItemType.typeId;
        loadActiveShipments(selectedItemType.typeId);
    }
    
	
	/* Get Shipments based on shipment type
	 * typeId 1 : Current Shipments
	 * typeId 2 : Pending Shipments
	 * typeId 3 : Completed Shipments
	 */
	function loadActiveShipments(typeId) {
		if(typeId == 4 || typeId == 5 || typeId == 6){
			$scope.isRtsDataFetch = true;
		}else{
			$scope.isRtsDataFetch = false;
		}
		var date = new Date(),
			currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
			reqObject = {
				"orgId": $localStorage.orgId,
				"groupId": $localStorage.groupId,
				"dateTime": currentDate,
				/*"searchType": "1",*/
				"searchType": typeId,
				"showMap": true,
				"searchString": $scope.shipment,
				"rtsDataFetch" : $scope.isRtsDataFetch
			};

		activeShipmentsService.post(reqObject).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			
			if (response.responseStatus == "success") {
				$scope.activeShipmentsArr = response.sensor.shipmentList;
				$scope.shipmentLocationData = response.sensor.map.locations;
				$scope.updatedTimeArr = [];
				
				if($scope.activeShipmentsArr.length === 0){
           		 if($localStorage.fetchESData === true){
                	$scope.noShipmentFound=response.responseMessage;
           		  }
				}

				for (var i = 0; i < response.sensor.shipmentList.length; i++) {
					var updatedTime=generalUtilityService.getLastUpdatedTime(response.sensor.shipmentList[i].updatedOn);
               	 	if(updatedTime!=="" && updatedTime!=null){
                       $scope.updatedTimeArr[i] = 'updated ' + updatedTime;    
                    }
				}

				/*if ($state.current.name === 'app.dashboard') {*/
				if ($state.current.name === 'app.dashboard' || $state.current.name === 'app.dashboard.mapMarkerPath') {
					$state.go('app.dashboard.showMap');
				}
			}
		}, function (error) {
			console.log(error);
		});
	} // function loadActiveShipments ENDS

	function getAlertsCount() {
		var date = new Date();
		var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
		var reqCountObject = {
			"dateTime": currentDate,
			"searchType": "All",
			"orgId": $localStorage.orgId
		}
		productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			if (response.responseStatus == "success") {
				$scope.allNotificationCount = response.count;
			}
		}, function (error) {
			console.log(error);
		})
	}

	/*loadActiveShipments();*/
	
    var lastState = sharedDataService.getLastVisitedUIState();
    if(sharedDataService.getSelectedShipmentType()=='3' && (lastState=='app.productsDetails.map' || lastState=='app.productsDetails.data' || lastState=='app.productsDetails.alert')){
		// TODO : Check
		loadActiveShipments("1");
        $scope.selectedItemType = $scope.shipmentTypeArr[4];
    }else{
        loadActiveShipments("1");
        $scope.selectedItemType = $scope.shipmentTypeArr[4];
    }
    /*loadActiveShipments("1"); // pass typeIs as 1, to by default show current shipment in the dropdown*/

	$scope.showMapMarkerPath = function (i, productId, selectedItemFromList) {
		//Alerts map requirement
		if(selectedItemFromList.latitude !== undefined && selectedItemFromList.longitude !== undefined && $scope.selectedItemType.typeId == 6){
			sharedDataService.setAlertMapData({
				"createdOn" : selectedItemFromList.createdOn,
				"latitude" : selectedItemFromList.latitude,
				"longitude" : selectedItemFromList.longitude,
				"pairStatus" : selectedItemFromList.pairStatus
			});
		}
		sharedDataService.setSelectedProductId(productId);
		// Highlight the selected product in product list on the left when its clicked
		$rootScope.selectedProductIndex = i;
		var selectShipment = generalUtilityService.getSelectedShipmentLocationdata($scope.shipmentLocationData,$scope.activeShipmentsArr[i]);
		if(selectShipment === undefined || selectShipment === null || selectShipment === ''){
			sharedDataService.setSelectedShipment($scope.activeShipmentsArr[i]);
		}else{
			sharedDataService.setSelectedShipment(selectShipment);
		}
		sharedDataService.setSelectedDeviceMarkerColor(colorConversionArr[$scope.activeShipmentsArr[i].pairStatus]);
		sharedDataService.setSelectedFavoriteOption($scope.activeShipmentsArr[i].isfavorite);
        sharedDataService.setSelectedShipmentType($scope.selectedItemType.typeId);
        sharedDataService.setSelectedProductId($scope.activeShipmentsArr[i].productId);
        sharedDataService.setShowMapAttribute(true);
		if ($state.current.name === 'app.dashboard.mapMarkerPath') {
			$state.reload('app.dashboard.mapMarkerPath');
		}
		if ($state.current.name === 'app.dashboard.alert' || $state.current.name === 'app.dashboard.showMap') {
			$rootScope.activeTabNumbTopNav = 1;
			$rootScope.activeDashboardTabNumb = 1;
			$state.go('app.dashboard.mapMarkerPath');
		}
	};

	$scope.onFavoriteSelect = function (selectedProduct, favorite) {
		var date = new Date();
		var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
		var reqObject = {
			"orgId": $localStorage.orgId,
			"groupId": $localStorage.groupId,
			"dateTime": currentDate,
			"user": $localStorage.username,
			"action": favorite,
			"deviceData": {
				"deviceId": selectedProduct.deviceId,
				"packageId": selectedProduct.packageId,
				"productId": selectedProduct.productId
			}
		};
		productFavoriteService.getfavoriteService.post(reqObject).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			if (response.responseStatus == "success") {
				angular.forEach($scope.activeShipmentsArr, function (value, key) {
					if (selectedProduct.deviceId == value.deviceId && selectedProduct.packageId == value.packageId &&  selectedProduct.productId == value.productId) {
						if (selectedProduct.isfavorite == 1) {
							$scope.activeShipmentsArr[key].isfavorite = 0;
							$scope.activeShipmentsArr = $filter('orderBy')($scope.activeShipmentsArr, 'isfavorite',true);							
							for (var i = 0; i < $scope.activeShipmentsArr.length; i++) {
								var updatedTime=generalUtilityService.getLastUpdatedTime($scope.activeShipmentsArr[i].updatedOn);
								if(updatedTime!=="" && updatedTime!=null){
								   $scope.updatedTimeArr[i] = 'updated ' + updatedTime;    
								}
							}
							sharedDataService.setShipmentList($scope.activeShipmentsArr);
						} else {
							$scope.activeShipmentsArr[key].isfavorite = 1;
							$scope.activeShipmentsArr = $filter('orderBy')($scope.activeShipmentsArr, 'isfavorite',true);							
							for (var i = 0; i < $scope.activeShipmentsArr.length; i++) {
								var updatedTime=generalUtilityService.getLastUpdatedTime($scope.activeShipmentsArr[i].updatedOn);
								if(updatedTime!=="" && updatedTime!=null){
								   $scope.updatedTimeArr[i] = 'updated ' + updatedTime;    
								}
							}
							sharedDataService.setShipmentList($scope.activeShipmentsArr);
						}
					}
				});
			} else {
				console.log(error);
			}
		}, function (error) {
			console.log(error);
		});
	};
    
    $scope.modifyProduct=function(modifyObj){
    	sharedDataService.setSelectedShipmentId(modifyObj.packageId);
    	sharedDataService.setSelectedShipmentType($scope.selectedItemType.typeId);
        sharedDataService.setSelectedDeviceId(modifyObj.deviceId);
        $state.go('app.modifyproduct');
    }
    
    $scope.locateNow=function(deviceId){
    	  var alert;
    	  var reqObj = {
    		  "orgId": $localStorage.orgId,
    	      "userId"  : $localStorage.userId,
              "deviceDetail": {
                  "deviceId": deviceId
              }
          }
    	  locateNowService.setLocationNow().save(JSON.stringify(reqObj)).$promise.then(function (response) {
              if (response.responseStatus === "error" && response.responseCode === 403) {
                  $scope.$emit('genericErrorEvent', response);
                  return;
              }
              if (response.responseStatus == "success") {
            	  alert = $mdDialog.alert({
                      title:  response.responseMessage,
                      ok: 'Close'
                    });
                    $mdDialog
                      .show(alert )
                      .finally(function() {
                      });	
            	} else {
            		alert = $mdDialog.alert({
                        title:  response.responseMessage,
                        ok: 'Close'
                      });
                      $mdDialog
                        .show(alert )
                        .finally(function() {
                        });
              }       
  			}, function (error) {
  			 console.log(error);
          });
    }
    
     $scope.disassociate = function (deviceId, packageId) {
      	 var confirm = $mdDialog.confirm().title('Are you sure you want to disassociate?').ok('Yes').cancel('No');
           $mdDialog.show(confirm).then(function() {
          	 var date = new Date();
          	 var alert;
               var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
               var reqObject = {
                   "dateTime": currentDate,
                   "orgId": $localStorage.orgId,
                   "groupId" : $localStorage.groupId,
                   "deviceData": {
                       "deviceId": deviceId,
                       "packageId": packageId
                   }
               };
               productAddService.disassociate.post(reqObject).$promise.then(function (response) {
                   if (response.responseStatus === "error" && response.responseCode === 403) {
                       $scope.$emit('genericErrorEvent', response);
                       return;
                   }
                   if (response.responseStatus == "success") {
                	   	alert = $mdDialog.alert({
                           title:  response.responseMessage,
                           ok: 'Close'
                         });
                         $mdDialog
                           .show(alert)
                           .finally(function() {
                        	    $state.reload('app.dashboard');
                           });	
                	   var confirm = $mdDialog.confirm().title().ok('Ok');
                    } else {
                    	alert = $mdDialog.alert({
                            title:  response.responseMessage,
                            ok: 'Close'
                          });
                          $mdDialog
                            .show(alert)
                            .finally(function() {
                         	   $state.reload('app.dashboard');
                            });	
                 	   var confirm = $mdDialog.confirm().title().ok('Ok');
                       
                   }
               }, function (error) {
            	   console.log(error);
               });
           });   
      }
     
     $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
    	 if (fromState.name === 'app.dashboard.showMap' || fromState.name === 'app.dashboard.alert') {
             clearTimeout(refreshShipmentCountInterval);
             clearTimeout(refreshLoadActiveShipments);
         }
     });
     
     loadShipmentCountInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
     refreshShipmentCountInterval = setInterval(getAlertsCount, loadShipmentCountInterval);
     refreshLoadActiveShipments = setInterval(function(){loadActiveShipments($scope.selectedItemType.typeId); }, loadShipmentCountInterval);
}