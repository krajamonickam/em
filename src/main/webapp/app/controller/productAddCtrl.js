angular.module('rfxcelApp').controller('productAddCtrl', productAddCtrl);
productAddCtrl.$inject = ['$scope', '$state', 'productAddService', '$filter', 'sharedDataService','$localStorage','DeviceTypeService','$mdDialog','generalUtilityService'];

function productAddCtrl($scope, $state, productAddService, $filter, sharedDataService,$localStorage,DeviceTypeService,$mdDialog,generalUtilityService) {
    
	var currentProfileId='';
	var isManualAddress=false;
	$scope.shipmentFrom = '';
	$scope.shipmentTo = '';
	//getProduct();    
	 $scope.productData = {
	            productId: '',
	            rtsItemId: '',
	            rtsSerialNumber: '',
	            shipFrom : {
	            	locationId: '',
	 	            location: '',
	 	            add1: '',
	 	            add2:'',
	 	            city:'',
	 	            zip:'',
	 	            state: '',
	 	            country:''
                },
                shipTo: {
                	locationId: '',
                	location: '',
	 	            add1: '',
	 	            add2:'',
	 	            city:'',
	 	            zip:'',
	 	            state: '',
	 	            country:''
                }
	    };
	 $scope.deviceTypeClassValue = 0;
	 /*Get the searched deviceId */
    $scope.$on('deviceSelected', onItemSelectHandler);
    function onItemSelectHandler(event, selectedDevice) {
        if (selectedDevice !== undefined){
            $scope.deviceTypeValue = selectedDevice.deviceType;
            if($scope.deviceTypeValue != null){
            	$scope.deviceTypeClassValue = 1;
            	$scope.deviceProfileClassValue = 1;
            	$scope.disableDeviceType=true;
            	$scope.disableDeviceProfile=true;
            }
            $scope.deviceId = selectedDevice.deviceId;
            $scope.active = selectedDevice.active;
            getPackageId($scope.deviceId);
            getProfilesByDevice(selectedDevice.deviceType);
        } else {
        	 $scope.deviceTypeValue = "";
        	 $scope.deviceProfile="";
        	 $scope.packageId = "";
        	 $scope.productData.productId="",
             $scope.deviceTypeClassValue = 0;
        	 $scope.deviceProfileClassValue = 0;
             $scope.disableDeviceType=false;
             $scope.disableDeviceProfile=false;
			 $scope.profiles = [];
			 $scope.shipmentFrom = '';
			 $scope.shipmentTo = '';
			 currentProfileId='';
        } 
    }
    
    /*Get the user entered deviceId */
    $scope.$on('deviceEntered', deviceEnteredHandler);
    function deviceEnteredHandler(event, enterDevice) {
        $scope.deviceId = enterDevice;
        $scope.disableDeviceType=false;
        $scope.disableDeviceProfile=false;
        $scope.disableSave=false;
        $scope.disableDisassociate=true;
        $scope.deviceTypeClassValue = 0;
        $scope.deviceProfileClassValue = 0;
        $scope.packageId = "";
        $scope.deviceTypeValue = "";
        $scope.deviceProfile = "";
        $scope.productData = {
 	            productId: '',
 	            shipFrom : {
 	            	locationId: '',
 	 	            location: '',
 	 	            add1: '',
 	 	            add2:'',
 	 	            city:'',
 	 	            zip:'',
 	 	            state: '',
 	 	            country:''
                 },
                 shipTo: {
                 	locationId: '',
                 	location: '',
 	 	            add1: '',
 	 	            add2:'',
 	 	            city:'',
 	 	            zip:'',
 	 	            state: '',
 	 	            country:''
                 }
 	    };
    }
    
    $scope.disableDisassociate=true;
	 
    getDeviceType();
    getAllProfiles();
    $scope.shippingFromLocationList = [];
    $scope.shippingToLocationList = [];
    $scope.selectedShipfromlocation = [];
    $scope.selectedShiptolocation = [];
    $scope.profiles = []
    
    $scope.shipmentType = sharedDataService.getConfigRfxcelItem();
    /*$scope.shippinFromLocationName = function (shippingFromName) {
        var shippingFromLocationNames = [];
        if (shippingFromName === '' || shippingFromName === undefined) {
            return;
        }
        var reqObject = {
                "locationData": {
                    "name": shippingFromName
                }
            }
            // Service to get the shipping from locationame
        productAddService.getLocationName.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            $scope.shippingFromLocationList = response.locationData.locationList;
            angular.forEach($scope.shippingFromLocationList, function (value, key) {
                shippingFromLocationNames.push(value.locationName);
            });
            $("#tags").autocomplete({
                source: shippingFromLocationNames,
                select: function (event, ui) {
                    onShippingFromHandler(ui.item.value);
                    return false;
                }
            });
        }, function (error) {
            console.log(error);
        })
    };

    function onShippingFromHandler(shippingFromName) {
        $scope.selectedShipfromlocation = shippingFromName;
        angular.forEach($scope.shippingFromLocationList, function (value, key) {
            if (shippingFromName.trim() === value.locationName.trim()) {
                var reqObject = {
                        "locationData": {
                            "id": value.locationId
                        }
                    }
                    // Service to get the shipping from location address
                productAddService.getLocationAddress.post(reqObject).$promise.then(function (response) {
                    if (response.responseStatus === "error" && response.responseCode === 403) {
                        $scope.$emit('genericErrorEvent', response);
                        return;
                    }
                    $scope.fromLocationList = response.locationData.locationList;
                    angular.forEach($scope.fromLocationList, function (value, key) {
                        if ($scope.selectedShiptolocation == shippingFromName) {
                            $scope.message = "Ship from & Ship to address should not be same";
                            return false;
                        } else {
                        	$scope.productData.shipFrom.locationId = value.locationId;
                        	$scope.productData.shipFrom.location = value.locationName;
                        	$scope.productData.shipFrom.add1 = value.address1;
                        	$scope.productData.shipFrom.add2 = value.address2;
                        	$scope.productData.shipFrom.city = value.city;
                        	$scope.productData.shipFrom.zip = value.postalCode;
                        	$scope.productData.shipFrom.state = value.state;
                        	$scope.productData.shipFrom.country = value.country;
                        }
                    });
                }, function (error) {
                    console.log(error);
                })
            }
        });
    }
    $scope.shippinToLocationName = function (shippingToName) {
        var shippingToLocationName = [];
        if (shippingToName === '' || shippingToName === undefined) {
            return;
        }
        var reqObject = {
                "locationData": {
                    "name": shippingToName
                }
            }
            // Service to get the shipping to locationame
        productAddService.getLocationName.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            $scope.shippingToLocationList = response.locationData.locationList;
            angular.forEach($scope.shippingToLocationList, function (value, key) {
                shippingToLocationName.push(value.locationName);
            });
            $("#tags1").autocomplete({
                source: shippingToLocationName,
                select: function (event, ui) {
                    onShippingToHandler(ui.item.value);
                    return false;
                }
            });
        }, function (error) {
            console.log(error);
        });
    }

    function onShippingToHandler(shippingToName) {
        $scope.selectedShiptolocation = shippingToName;
        angular.forEach($scope.shippingToLocationList, function (value, key) {
            if (shippingToName.trim() === value.locationName.trim()) {
                var reqObject = {
                        "locationData": {
                            "id": value.locationId
                        }
                    }
                    // Service to get the shipping to locationame address
                productAddService.getLocationAddress.post(reqObject).$promise.then(function (response) {
                    if (response.responseStatus === "error" && response.responseCode === 403) {
                        $scope.$emit('genericErrorEvent', response);
                        return;
                    }
                    $scope.toLocationList = response.locationData.locationList;
                    angular.forEach($scope.toLocationList, function (value, key) {
                        if ($scope.selectedShipfromlocation == shippingToName) {
                            $scope.message = "Ship from & Ship to address should not be same";
                            return false;
                        } else {
                        	$scope.productData.shipTo.locationId = value.locationId;
                        	$scope.productData.shipTo.location = value.locationName;
                        	$scope.productData.shipTo.add1 = value.address1;
                        	$scope.productData.shipTo.add2 = value.address2;
                        	$scope.productData.shipTo.city = value.city;
                        	$scope.productData.shipTo.zip = value.postalCode;
                        	$scope.productData.shipTo.state = value.state;
                        	$scope.productData.shipTo.country = value.country;
                        }
                    });
                }, function (error) {
                    console.log(error);
                })
            }
        });
    }*/
    
    $scope.addProduct = function () {
    	if (0 === $scope.profiles.length || 0 === $scope.deviceProfile.length || undefined === $scope.deviceProfile || "" === $scope.deviceProfile) {
			$scope.message = "Please select device profile";
			return false;
		}
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject = {
        		"orgId": $localStorage.orgId,
                "productDetail": {
                    "packageId": $scope.packageId,
                    "productId": $scope.productData.productId,
                    "deviceId": $scope.deviceId,
                    "deviceType": $scope.deviceTypeValue,
                    "profileId": $scope.deviceProfile,
                    "deviceIsActive": $scope.active,
                    "rtsItemId":$scope.productData.rtsItemId == "" ? null : $scope.productData.rtsItemId,
                    "rtsSerialNumber":$scope.productData.rtsSerialNumber == "" ? null : $scope.productData.rtsSerialNumber,
                    "shipFrom": {
                        "locationId": $scope.productData.shipFrom.locationId,
                        "locationName": $scope.productData.shipFrom.location,
                        "address1": $scope.productData.shipFrom.add1,
                        "address2": $scope.productData.shipFrom.add2,
                        "city": $scope.productData.shipFrom.city,
                        "state": $scope.productData.shipFrom.state,
                        "country": $scope.productData.shipFrom.country,
                        "postalCode": $scope.productData.shipFrom.zip
                    },
                    "shipTo": {
                        "locationId": $scope.productData.shipTo.locationId,
                        "locationName": $scope.productData.shipTo.location,
                        "address1": $scope.productData.shipTo.add1,
                        "address2": $scope.productData.shipTo.add2,
                        "city": $scope.productData.shipTo.city,
                        "state": $scope.productData.shipTo.state,
                        "country": $scope.productData.shipTo.country,
                        "postalCode": $scope.productData.shipTo.zip
                    }
                }
            }
            // Service to add the product
        productAddService.addProductDetail.post(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            
            if (response.responseStatus == "success") {
                	$state.go('app.products');
            } else {
            	if(response.responseStatus === "error" && response.responseCode !== null && response.responseCode === 333){
            		$state.go('app.products');
            	}else{
            		$scope.message = response.responseMessage;
            	}
            }
        }, function (error) {
            console.log(error);
        });
    }
    
    
    function getPackageId(deviceId) {
        
        if (deviceId === '' || deviceId === undefined) {
            return false;
        }
        var reqObject = {
        		"orgId": $localStorage.orgId,
                "deviceData": {
                    "deviceId": deviceId
                }
            }
            // Service to get the associated device details
        productAddService.getPackage.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                    $scope.packageId = response.packageId;
                    checkSerialNumberAlreadyAssociated($scope.packageId);
            }
            if($scope.packageId === '' || $scope.packageId === undefined) {
            	$scope.deviceProfileClassValue = 0;
            	$scope.disableDeviceProfile=false;
            }
        }, function (error) {
            console.log(error);
        });
    }
    
    function checkSerialNumberAlreadyAssociated(packageId) {
        
        $scope.disableSave=false;
        $scope.disableDisassociate=true;
        
        if (packageId === '' || packageId === undefined) {
            return false;
        }
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject = {
        		"orgId": $localStorage.orgId,
        		"groupId" : $localStorage.groupId,
                "dateTime": currentDate,
                "deviceData": {
                    "packageId": packageId,
					"deviceId": $scope.deviceId
                }
            }
            // Service to get the associated device details
        productAddService.getProductDetails.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.productDetailList = response.sensor.shipmentData;
                if (JSON.stringify($scope.productDetailList) == '{}') {
                    return false;
                } else {
                    $scope.$broadcast('populateDeviceId', response.deviceId);
                    $scope.packageId = response.packageId;
                    $scope.deviceId = response.deviceId;
                    $scope.deviceTypeValue = $scope.productDetailList.deviceCode;
                    $scope.deviceProfile = $scope.productDetailList.deviceProfile;
                    $scope.productData = $scope.productDetailList;
                    $scope.productData.shipFrom = $scope.productDetailList.shipFrom;
                    $scope.productData.shipTo = $scope.productDetailList.shipTo;
                    $scope.disableDisassociate=false;
                    $scope.disableSave=false;
                    currentProfileId = $scope.productDetailList.deviceProfile;
                    $scope.shipmentFrom = $scope.productDetailList.shipFrom;
                    $scope.shipmentTo = $scope.productDetailList.shipTo;
                    if($scope.deviceProfile != null){
                    	 for(var i in $scope.profileList){
                    		 if($scope.profileList[i].profileId == $scope.deviceProfile){
                    			 $scope.profiles = [];
                    			 $scope.profiles.push($scope.profileList[i]);
                    			 $scope.deviceProfile={
                    					 name:$scope.profileList[i].name,
                    					 profileId:$scope.profileList[i].profileId
                    			 }
                    		 }
                    		 
                    	 }
                    }
                }
            }
        }, function (error) {
            console.log(error);
        });
    };

    function getProduct() {
        var reqObject = {
            "searchType": "All"
        }
        productAddService.getProduct.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            $scope.productList = response.productDetail.productList;
        }, function (error) {
            console.log(error);
        });
    };

    function getDeviceType() {
        var reqObject = {
            "searchType": "All",
            "orgId" : $localStorage.orgId
        };
        DeviceTypeService.getDeviceTypes().save(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.deviceList = response.sensor.deviceList;

            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    };
    
    function getAllProfiles() {
        var reqObject = {
        	       			"profile":						
        	       			{
        	       				"orgId": $localStorage.orgId,
        	       				"groupId" : $localStorage.groupId
        	       			}
        	       		}
        productAddService.getProfiles.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.profileList = response.profileList;
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
    
    /*Get the profileList based on deviceType*/
    $scope.deviceTypeSelection = function (deviceTypeValue) {
		$scope.deviceProfile = '';
		/*$scope.productData.shipFrom.location = '';
		$scope.productData.shipFrom.locationId = '';
		$scope.productData.shipFrom.add1 = '';
		$scope.productData.shipFrom.add2 = '';
		$scope.productData.shipFrom.city = '';
		$scope.productData.shipFrom.state = '';
		$scope.productData.shipFrom.country = '';
		$scope.productData.shipFrom.zip = '';
		$scope.productData.shipTo.location = '';
		$scope.productData.shipTo.locationId = '';
		$scope.productData.shipTo.add1 = '';
		$scope.productData.shipTo.add2 = '';
		$scope.productData.shipTo.city = '';
		$scope.productData.shipTo.state = '';
		$scope.productData.shipTo.country = '';
		$scope.productData.shipTo.zip = '';	*/
		getProfilesByDevice(deviceTypeValue);
        angular.forEach($scope.deviceList, function (value, key) {
        	if(deviceTypeValue == value.deviceType){
				if(value.manufacturer.indexOf('Queclink') >= 0){
					$scope.active = false;
				}else{
					$scope.active = true;
				}        		
        	}
        });
    };
    
    /*Get the profileList based on deviceType*/
    function getProfilesByDevice(deviceTypeValue)
    {
        var reqObject = {
    						"deviceTypeId": deviceTypeValue,
        	       			"profile":
        	       			{
        	       				"orgId": $localStorage.orgId,
        	       				"groupId": $localStorage.groupId
        	       			}
        	       		}
        productAddService.getProfilesByDeviceType.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	 $scope.profiles = [];
            	$scope.profiles = response.profileList;
            	 /*for(var i in $scope.profileList){
            		 $scope.profiles = [];
        			 $scope.profiles.push($scope.profileList[i]);
            		 $scope.deviceProfile={
        					 name:$scope.profileList[i].name,
        					 profileId:$scope.profileList[i].profileId
        			 }
            	 }*/
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    
    }
    
	$scope.getHomeAddressByProfileId=function(profileId){
		if(isManualAddress){
			return;
		}
		if(currentProfileId === profileId.toString()){
			if($scope.shipmentFrom !== undefined) {
				if($scope.shipmentFrom !== null) {
					$scope.productData.shipFrom.location=$scope.shipmentFrom.location;
					$scope.productData.shipFrom.locationId=$scope.shipmentFrom.locationId;
	            	$scope.productData.shipFrom.add1=$scope.shipmentFrom.add1;
	            	$scope.productData.shipFrom.add2=$scope.shipmentFrom.add2;
	            	$scope.productData.shipFrom.city=$scope.shipmentFrom.city;
	            	$scope.productData.shipFrom.state=$scope.shipmentFrom.state;
	            	$scope.productData.shipFrom.country=$scope.shipmentFrom.country;
	            	$scope.productData.shipFrom.zip=$scope.shipmentFrom.zip;
				}
			}
			if($scope.shipmentTo !== undefined) {
				if($scope.shipmentTo !== null) {
					$scope.productData.shipTo.location=$scope.shipmentTo.location;
					$scope.productData.shipTo.locationId=$scope.shipmentTo.locationId;
	            	$scope.productData.shipTo.add1=$scope.shipmentTo.add1;
	            	$scope.productData.shipTo.add2=$scope.shipmentTo.add2;
	            	$scope.productData.shipTo.city=$scope.shipmentTo.city;
	            	$scope.productData.shipTo.state=$scope.shipmentTo.state;
	            	$scope.productData.shipTo.country=$scope.shipmentTo.country;
	            	$scope.productData.shipTo.zip=$scope.shipmentTo.zip;
				}
			}
		}else{
			$scope.productData.shipFrom.location = '';
			$scope.productData.shipFrom.locationId = '';
			$scope.productData.shipFrom.add1 = '';
			$scope.productData.shipFrom.add2 = '';
			$scope.productData.shipFrom.city = '';
			$scope.productData.shipFrom.state = '';
			$scope.productData.shipFrom.country = '';
			$scope.productData.shipFrom.zip = '';
			
			$scope.productData.shipTo.location = '';
			$scope.productData.shipTo.locationId = '';
			$scope.productData.shipTo.add1 = '';
			$scope.productData.shipTo.add2 = '';
			$scope.productData.shipTo.city = '';
			$scope.productData.shipTo.state = '';
			$scope.productData.shipTo.country = '';
			$scope.productData.shipTo.zip = '';	
			getHomeAddressByProfileIdDetail(profileId);
		}
	}
	
    function getHomeAddressByProfileIdDetail(profileId){
    	var reqObject = {
    			"profile": {
    				"profileId": profileId,
    				"orgId": $localStorage.orgId
    			}
    		}

		productAddService.getHomeAddressByProfileId.save(reqObject).$promise.then(function (response) {
		if (response.responseStatus === "error" && response.responseCode === 403) {
		    $scope.$emit('genericErrorEvent', response);
		    return;
		}
		if (response.responseStatus == "success") {
			var homeAddObj={};
			var destAddObj={};
			if(response.profile.geopoints !== undefined && response.profile.geopoints !== null && response.profile.geopoints.length !== 0){
				//if(response.profile.geopoints.length === 2){
					for(var i=0;i<response.profile.geopoints.length;i++){
						if(response.profile.geopoints[i].locationType === 1){
							$scope.productData.shipFrom.location = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
							$scope.productData.shipFrom.add1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
							$scope.productData.shipFrom.add2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
							$scope.productData.shipFrom.city = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
							$scope.productData.shipFrom.zip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
							$scope.productData.shipFrom.state= typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
							$scope.productData.shipFrom.country= typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
						} else {
							$scope.productData.shipTo.location = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
							$scope.productData.shipTo.add1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
							$scope.productData.shipTo.add2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
							$scope.productData.shipTo.city = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
							$scope.productData.shipTo.zip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
							$scope.productData.shipTo.state= typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
							$scope.productData.shipTo.country= typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
						}
					}
				/*}else{
					for(var i in response.profile.geopoints.length){
						if(response.profile.geopoints[i].locationType === 1){
							$scope.productData.shipFrom.location = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
							$scope.productData.shipFrom.add1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
							$scope.productData.shipFrom.add2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
							$scope.productData.shipFrom.city = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
							$scope.productData.shipFrom.zip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
							$scope.productData.shipFrom.state= typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
							$scope.productData.shipFrom.country= typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
						} else {
							$scope.productData.shipTo.location = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
							$scope.productData.shipTo.add1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
							$scope.productData.shipTo.add2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
							$scope.productData.shipTo.city = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
							$scope.productData.shipTo.zip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
							$scope.productData.shipTo.state= typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
							$scope.productData.shipTo.country= typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
						}
					}
				}*/
			}
        } else {
		    $scope.message = response.responseMessage;
		}
		}, function (error) {
		console.log(error);
		});
    }
    
    $scope.disassociate = function (deviceId, packageId, productId) {
    	 var confirm = $mdDialog.confirm().title('Are you sure you want to disassociate?').ok('Yes').cancel('No');
         $mdDialog.show(confirm).then(function() {
        	 var date = new Date();
             var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
             var reqObject = {
                 "dateTime": currentDate,
                 "orgId": $localStorage.orgId,
                 "deviceData": {
                     "deviceId": deviceId,
                     "packageId": packageId,
                     "productId":productId
                 }
             };
             productAddService.disassociate.post(reqObject).$promise.then(function (response) {
                 if (response.responseStatus === "error" && response.responseCode === 403) {
                     $scope.$emit('genericErrorEvent', response);
                     return;
                 }
                 if (response.responseStatus == "success") {
                     $scope.message = response.responseMessage;
                     $scope.$broadcast('populateDeviceId', '');
                     $scope.packageId = "";
                     $scope.deviceTypeValue="";
                     $scope.deviceProfile="";
                     $scope.productData = {
             	            productId: '',
             	            shipFrom : {
             	            	locationId: '',
             	 	            location: '',
             	 	            add1: '',
             	 	            add2:'',
             	 	            city:'',
             	 	            zip:'',
             	 	            state: '',
             	 	            country:''
                             },
                             shipTo: {
                             	locationId: '',
                             	location: '',
             	 	            add1: '',
             	 	            add2:'',
             	 	            city:'',
             	 	            zip:'',
             	 	            state: '',
             	 	            country:''
                             }
             	    };
                     $scope.deviceId = "";
                     $scope.disableSave=false;
                     $scope.disableDisassociate=true;
                 } else {
                     $scope.message = response.responseMessage;
                 }
             }, function (error) {
                 console.log(error);
             });
         });   
    }
    
    $scope.manualAddress=function(){
    	isManualAddress=true;
    }
    $scope.Cancel = function () {
        $state.go('app.products');
    };
}