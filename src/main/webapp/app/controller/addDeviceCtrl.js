angular.module('rfxcelApp').controller('addDeviceCtrl', addDeviceCtrl);
addDeviceCtrl.$inject = ['$scope', '$state','sharedDataService','DeviceTypeService','$localStorage','UserLocalService','$sessionStorage'];

function addDeviceCtrl($scope, $state,sharedDataService,DeviceTypeService,$localStorage,UserLocalService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
    var selectedDeviceType='';
    getDeviceType();
     
	 $scope.device = {
			 deviceType: '',
			 deviceKey: '',
			 deviceId:'',
			 active : true	 
	 }
	 
	 function getDeviceType() {
	       $scope.deviceTypeList= UserLocalService.getDeviceTypeList();
     }
    
    $scope.changeDeviceType=function(selectDeviceType){
    	selectedDeviceType=selectDeviceType.deviceType;
    	if(selectDeviceType.manufacturer.indexOf('Queclink') >= 0){
    		$scope.device.active = false;
    	}else{
    		$scope.device.active = true;
    	}
        
    }
    
    $scope.add=function(){
        
    var reqObj={
           "orgId": selectedOrgId,
           "itemListDetails": [{
        	   "deviceType": selectedDeviceType,
        	   "deviceKey": $scope.device.deviceKey,
        	   "mdn": $scope.device.mdn,
               "deviceId":  $scope.device.deviceId,
               "active": $scope.device.active
           		}]
       };
        
       DeviceTypeService.addDevice().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
               $state.go('app.user');
            } else {
            	if(response.responseStatus === "error" && response.responseCode !== null && response.responseCode === 333){
            		$state.go('app.user');
            	}else{
            		$scope.message = response.responseMessage;
            	}
            }       
			}, function (error) {
			 console.log(error);
        });
    }
    
    $scope.cancel = function () {
        $state.go("app.user");
    };
}