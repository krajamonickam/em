angular.module('rfxcelApp').controller('editUserCtrl', editUserCtrl);
editUserCtrl.$inject = ['$scope', '$state'];

function editUserCtrl($scope, $state) {

    $scope.editUser = function () {
        $state.go('app.organization.editUser');
    }

}
