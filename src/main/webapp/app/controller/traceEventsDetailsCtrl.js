angular.module('rfxcelApp').controller('traceEventsDetailsController', traceEventsDetailsController);

traceEventsDetailsController.$inject = ['$scope', '$state', 'sharedDataService', 'traceEventsDetailsService', 'uiGridTreeViewConstants', 'uiGridTreeBaseService', 'uiGridConstants', '$http', '$rootScope', '$stateParams', '$location', '$window', '$mdDialog'];

function traceEventsDetailsController($scope, $state, sharedDataService, traceEventsDetailsService, uiGridTreeViewConstants, uiGridTreeBaseService, uiGridConstants, $http, $rootScope, $stateParams, $location, $window, $mdDialog) {
    var selectedEventId = $stateParams.eventId;
    var deviceId = $stateParams.deviceId;
    $scope.$emit('isShowHeaderFooter', false);

    $scope.traceIdPrefix = {
        isVisible: false
    }

    $scope.traceIdinputItemInfoPrefix = {
        isVisible: false
    }

    $scope.collapse = {
        isItemInfoOpen: true,
        isInputItemInfoOpen: true,
        isReportsOpen: false,
        isRelatedEventsOpen: false,
        isNotificationOpen: false,
        isEventInfoOpen: false,
        isInputSummaryOpen: true,
        isOutputSummaryOpen: true
    }
    
    $scope.eventOutputItemSummaryList = {
        columnDefs: [
            { name: 'ItemName', enableColumnMenu: false },
            { name: 'ItemId', enableColumnMenu: false },
            { name: 'itemType', enableColumnMenu: false },
            { name: 'qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' }
		],
        data: []
    }

    $scope.eventInputItemSummaryList = {
        columnDefs: [
            { name: 'ItemName', enableColumnMenu: false },
            { name: 'ItemId', enableColumnMenu: false },
            { name: 'itemType', enableColumnMenu: false },
            { name: 'qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' }
			],
        data: []
    }

    $scope.notificationsList = {
        columnDefs: [
            { name: 'notificationId', width: '15%', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.clickNotificationId(row.entity.notificationId)">{{row.entity.notificationId}}</span></a></div>' },
            { name: 'eventId', width: '15%', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><span ng-if="row.entity.eventExtId == null || !row.entity.eventExtId">{{row.entity.eventId}}</span><span ng-if="row.entity.eventExtId || row.entity.eventExtId != null ">{{row.entity.eventExtId}}</span></div>' },
            { name: 'Action', width: '5%', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a>Close</a></div>' },
            { name: 'notificationType', enableColumnMenu: false, width: '10%' },
            { name: 'subType', enableColumnMenu: false, width: '8%' },
            { name: 'raisedTimeStamp', enableColumnMenu: false, width: '12%' },
            { name: 'notification', enableColumnMenu: false, width: '35%' }
		 ],
        data: []
    }

    $scope.relatedEventsList = {
        /*paginationPageSizes: [2, 4, 6],
			paginationPageSize: 4,*/
        virtualizationThreshold: 30,
        enableSorting: true,
        columnDefs: [
            { name: 'eventId', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.clickEventId(row.entity.eventId)"><span ng-if="row.entity.eventExtId == null || !row.entity.eventExtId">{{row.entity.eventId}}</span><span ng-if="row.entity.eventExtId || row.entity.eventExtId != null ">{{row.entity.eventExtId}}</span></a></div>' },
            { name: 'eventType', enableColumnMenu: false },
            { name: 'eventStatus', enableColumnMenu: false },
            { name: 'eventDateTime', enableColumnMenu: false },
            { name: 'eventLocation', enableColumnMenu: false },
					/*{ name: 'bizEventType', enableColumnMenu: false},*/
            { name: 'bizTransaction', enableColumnMenu: false },
            { name: 'sender', enableColumnMenu: false },
            { name: 'receiver', enableColumnMenu: false }
				],
        data: []
    }

    var paginationOptions = {
        pageNumber: 1,
        pageSize: 50,
        sort: null
    };

    var paginationOptionsInputInfo = {
        pageNumber: 1,
        pageSize: 50,
        sort: null
    };

    $scope.eventItemDetailsList = {
        /*paginationPageSizes: [2, 4, 6],
        paginationPageSize: 4,*/
        virtualizationThreshold: 5000,
        data: [],
        enablePaginationControls: false,
        useExternalPagination: true,
        enableSorting: true,
        showTreeExpandNoChildren: true,
        showTreeRowHeader: false,
        expandableRowTemplate: '<div ui-grid=\'row.entity.subGridOptions\'></div>',
        expandableRowHeight: 100,
        //subGridVariable will be available in subGrid scope
        expandableRowScope: {
            subGridVariable: 'subGridScopeVariable'
        },
        enableExpandableRowHeader: false,
        // added Data column
        /*columnDefs: [
            { name: 'traceId', width: 220, enableColumnMenu: false, cellTemplate: "<div class=\"ui-grid-cell-contents\"><div style=\"float:left;\" class=\"ui-grid-tree-base-row-header-buttons\" ng-class=\"{'ui-grid-tree-base-header': row.treeLevel > -1 }\" ng-click=\"grid.appScope.toggleRow(row,evt)\"><i ng-class=\"{'ui-grid-lastLevel':  row.entity.hasChild==0 ,'ui-grid-icon-minus-squared': ( ( grid.options.showTreeExpandNoChildren && row.treeLevel > -1 ) || ( row.treeNode.children && row.treeNode.children.length > 0 ) ) && row.treeNode.state === 'expanded', 'ui-grid-icon-plus-squared': ( ( grid.options.showTreeExpandNoChildren && row.treeLevel > -1 ) || ( row.treeNode.children && row.treeNode.children.length > 0 ) ) && row.treeNode.state === 'collapsed'}\" ng-style=\"{'padding-left': grid.options.treeIndent * row.treeLevel + 'px'}\"></i> &nbsp;</div><a ng-click=\"grid.appScope.clickTraceId(row.entity.traceId)\"><span ng-if=\"!grid.appScope.traceIdPrefix.isVisible\">{{row.entity.hidePrefix}}</span><span ng-if=\"grid.appScope.traceIdPrefix.isVisible\">{{row.entity.traceEntExtId}}</span></a></div>" },
            { name: 'itemType', enableColumnMenu: false },
            { name: 'itemId', enableColumnMenu: false },
            { name: 'itemName', enableColumnMenu: false },
            { name: 'itemLocation', enableColumnMenu: false },
            { name: 'disposition', enableColumnMenu: false },
            { name: 'lotId', enableColumnMenu: false },
            { name: 'qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' },
            { name: 'mfrName', enableColumnMenu: false },
            { name: 'data', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents">Actual</div>' }
			  ],*/
	
		columnDefs: [
            { name: 'traceId', width: 220, enableColumnMenu: false, cellTemplate: "<div class=\"ui-grid-cell-contents\"><div style=\"float:left;\" class=\"ui-grid-tree-base-row-header-buttons\" ng-class=\"{'ui-grid-tree-base-header': row.treeLevel > -1 }\" ng-click=\"grid.appScope.toggleRow(row,evt)\"><i ng-class=\"{'ui-grid-lastLevel':  row.entity.hasChild==0 , 'display-none': (row.entity.hasChild==0 && row.treeLevel==0) ,'ui-grid-icon-minus-squared': ( ( grid.options.showTreeExpandNoChildren && row.treeLevel > -1 ) || ( row.treeNode.children && row.treeNode.children.length > 0 ) ) && row.treeNode.state === 'expanded', 'ui-grid-icon-plus-squared': ( ( grid.options.showTreeExpandNoChildren && row.treeLevel > -1 ) || ( row.treeNode.children && row.treeNode.children.length > 0 ) ) && row.treeNode.state === 'collapsed'}\" ng-style=\"{'padding-left': grid.options.treeIndent * row.treeLevel + 'px'}\"></i> &nbsp;</div><a ng-click=\"grid.appScope.clickTraceId(row.entity.traceId)\"><span ng-if=\"!grid.appScope.traceIdPrefix.isVisible\">{{row.entity.hidePrefix}}</span><span ng-if=\"grid.appScope.traceIdPrefix.isVisible\">{{row.entity.traceEntExtId}}</span></a></div>" },
            { name: 'itemType', enableColumnMenu: false },
            { name: 'itemId', enableColumnMenu: false },
            { name: 'itemName', enableColumnMenu: false },
            { name: 'itemLocation', enableColumnMenu: false },
            { name: 'disposition', enableColumnMenu: false },
            { name: 'lotId', enableColumnMenu: false },
            { name: 'qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' },
            { name: 'mfrName', enableColumnMenu: false },
            { name: 'data', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents">Actual</div>' }
			  ],

		
        onRegisterApi: function(gridApi) {
            $scope.gridApi = gridApi;
            gridApi.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
                paginationOptions.pageNumber = newPage;
                paginationOptions.pageSize = pageSize;
                getEventItemDetails(selectedEventId);
            });

            $scope.gridApi.treeBase.on.rowExpanded($scope, function(row) {
                for (var i in $scope.eventItemDetailsList.data) {
                    var setTreeLevel = $scope.eventItemDetailsList.data[i].$$treeLevel + 1;
                    if (row.entity.$$hashKey == $scope.eventItemDetailsList.data[i].$$hashKey && !$scope.eventItemDetailsList.data[i].nodeLoaded) {
                        getEventSubItemDetails(i, setTreeLevel, row.entity.traceId, row.entity.rootEntId);
                        $scope.eventItemDetailsList.data[i].nodeLoaded = true;
                        break;
                    }
                }

            });

        }
    }

    $scope.eventInputItemDetailsList = {
        data: [],
        enablePaginationControls: false,
        useExternalPagination: true,
        enableSorting: true,
        virtualizationThreshold: 50,
        // added Data column
        columnDefs: [
            { name: 'traceId', width: 220, enableColumnMenu: false, cellTemplate: "<div class=\"ui-grid-cell-contents\"><a ng-click=\"grid.appScope.clickTraceId(row.entity.traceId)\"><span ng-if=\"!grid.appScope.traceIdinputItemInfoPrefix.isVisible\">{{row.entity.hidePrefix}}</span><span ng-if=\"grid.appScope.traceIdinputItemInfoPrefix.isVisible\">{{row.entity.traceEntExtId}}</span></a></div>" },
            { name: 'itemType', enableColumnMenu: false },
            { name: 'itemId', enableColumnMenu: false },
            { name: 'itemName', enableColumnMenu: false },
            { name: 'itemLocation', enableColumnMenu: false },
            { name: 'disposition', enableColumnMenu: false },
            { name: 'lotId', enableColumnMenu: false },
            { name: 'qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' },
            { name: 'mfrName', enableColumnMenu: false },
            { name: 'data', enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents">Actual</div>' }
    			  ],

        onRegisterApi: function(gridApiInputItemInfo) {
            $scope.gridApiInputItemInfo = gridApiInputItemInfo;
            gridApiInputItemInfo.pagination.on.paginationChanged($scope, function(newPage, pageSize) {
                paginationOptionsInputInfo.pageNumber = newPage;
                paginationOptionsInputInfo.pageSize = pageSize;
                getEventInputItemDetails(selectedEventId);
            });
        }
    }

    $scope.complianceReportsList = {
        columnDefs: [
            {
                name: 'reportName',
                enableColumnMenu: false,
                cellTemplate: '<div class="ui-grid-cell-contents">' +
                    '<a ng-if="row.entity.enabled>=1" title="{{row.entity.reportName}}" ng-click="grid.appScope.downloadReports(row.entity.reportType)"><span data-report-kind="{{row.entity.dataReportKind}}">{{row.entity.reportName}}</span></a>' +
                    '<span style="cursor: auto;" ng-if="row.entity.enabled==0" title="{{row.entity.reportName}}" data-report-kind="{{row.entity.dataReportKind}}">{{row.entity.reportName}}</span>' +
                    '</div>'
					  },
            { name: 'sent', enableColumnMenu: false },
            { name: 'response', enableColumnMenu: false },
			]
    }

    getEventInformation(selectedEventId);
    getEventOutputItemSummary(selectedEventId);
    getEventItemDetails(selectedEventId);
    getEventNotifications(selectedEventId);
    getRelatedEvents(selectedEventId);
    getCompilanceReportFlags(selectedEventId);

    function getEventInputItemSummary(eventId) {
    	var reqObject = {
            "eventId": eventId
        };

        traceEventsDetailsService.getEventInputItemSummary().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                for (var i = 0; i < response.data.length; i++) {
                    $scope.eventInputItemSummaryList.data[i] = {
                        "ItemName": response.data[i].prodName,
                        "ItemId": response.data[i].itemTypeExtId,
                        "itemType": response.data[i].itemType,
                        "qty": response.data[i].quantity
                    }
                }
            }
        }, function(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getEventOutputItemSummary(eventId) {

        var reqObject = {
            "eventId": eventId
        };

        traceEventsDetailsService.getEventItemSummary().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
            	for (var i = 0; i < response.data.length; i++) {
                    $scope.eventOutputItemSummaryList.data[i] = {
                        "ItemName": response.data[i].prodName,
                        "ItemId": response.data[i].itemTypeExtId,
                        "itemType": response.data[i].itemType,
                        "qty": response.data[i].quantity
                    }
                }
            }
        }, function(error) {
        	if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getEventItemDetails(eventId) {

        var limit;
        var offset;

        limit = paginationOptions.pageSize;
        offset = paginationOptions.pageNumber;
        var reqObject = {
            "eventId": eventId,
            "limit": limit,
            "offset": offset
        };

        console.log(reqObject);

        traceEventsDetailsService.getEventItemDetails().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            console.log(response);
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $scope.eventItemDetailsList.data = [];
                for (var i in response.data.pageItems) {
                	
                	 /*if ($scope.eventInformation.eventTypeId === 32) {
                		 response.data.pageItems[i].itemLocation =  $scope.eventInformation.mfrLocName;
                     }*/
                	
                    var obj = {
                        "eventId": selectedEventId,
                        "traceEntExtId": response.data.pageItems[i].traceEntExtId,
                        "hidePrefix": response.data.pageItems[i].traceEntExtId.substring(response.data.pageItems[i].traceEntExtId.lastIndexOf('.') + 1),
                        "traceId": response.data.pageItems[i].traceEntId,
                        "itemType": response.data.pageItems[i].itemType,
                        "itemId": response.data.pageItems[i].itemId,
                        "itemName": response.data.pageItems[i].itemName,
                        "itemLocation": response.data.pageItems[i].itemLocation,
                        "disposition": response.data.pageItems[i].disposition,
                        "lotId": response.data.pageItems[i].lotId,
                        "qty": response.data.pageItems[i].quantity,
                        "hasChild": response.data.pageItems[i].hasChild,
                        "rootEntId": response.data.pageItems[i].rootEntId,
                        "mfrName": response.data.pageItems[i].mfrName,
                        "$$treeLevel": 0
                    };

                    $scope.eventItemDetailsList.data.push(obj);
                }
                $scope.eventItemDetailsList.totalItems = response.data.totalFilteredRecords;
                $scope.eventItemDetailsList.paginationPageSize = limit;
                $scope.eventItemDetailsList.paginationCurrentPage = offset;
            }
        }, function(error) {
        	if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getEventInputItemDetails(eventId) {

        var limit;
        var offset;

        limit = paginationOptionsInputInfo.pageSize;
        offset = paginationOptionsInputInfo.pageNumber;
        var reqObject = {
            "eventId": eventId,
            "limit": limit,
            "offset": offset
        };

        console.log(reqObject);

        traceEventsDetailsService.getEventInputItemDetails().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            console.log(response);
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $scope.eventInputItemDetailsList.data = [];
                for (var i in response.data.pageItems) {
                	
                	/*if ($scope.eventInformation.eventTypeId === 32) {
                		 response.data.pageItems[i].itemLocation =  $scope.eventInformation.mfrLocName;
                     }*/
                	
                    var obj = {
                        "eventId": selectedEventId,
                        "traceEntExtId": response.data.pageItems[i].traceEntExtId,
                        "hidePrefix": response.data.pageItems[i].traceEntExtId.substring(response.data.pageItems[i].traceEntExtId.lastIndexOf('.') + 1),
                        "traceId": response.data.pageItems[i].traceEntId,
                        "itemType": response.data.pageItems[i].itemType,
                        "itemId": response.data.pageItems[i].itemId,
                        "itemName": response.data.pageItems[i].itemName,
                        "itemLocation": response.data.pageItems[i].itemLocation,
                       //"disposition": response.data.pageItems[i].disposition,
                        "disposition": "Destroyed",
                        "lotId": response.data.pageItems[i].lotId,
                        "qty": response.data.pageItems[i].quantity,
                        "rootEntId": response.data.pageItems[i].rootEntId,
                        "mfrName": response.data.pageItems[i].mfrName
                    };

                    $scope.eventInputItemDetailsList.data.push(obj);
                }
                $scope.eventInputItemDetailsList.totalItems = response.data.totalFilteredRecords;
                $scope.eventInputItemDetailsList.paginationPageSize = limit;
                $scope.eventInputItemDetailsList.paginationCurrentPage = offset;
            }
        }, function(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getEventSubItemDetails(currentIndex, level, traceId, rootEntId) {
        var reqObject = {
            "traceEntId": traceId,
            "rootEntId": rootEntId,
            "eventId": selectedEventId,
        }

        traceEventsDetailsService.getEventSubItemDetails().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {

                for (var i in response.data) {

                    var obj = {
                        "traceId": response.data[i].traceEntId,
                        "traceEntExtId": response.data[i].traceEntExtId,
                        "hidePrefix": response.data[i].traceEntExtId.substring(response.data[i].traceEntExtId.lastIndexOf('.') + 1),
                        "itemType": response.data[i].itemType,
                        "itemId": response.data[i].itemId,
                        "itemName": response.data[i].itemName,
                        "itemLocation": response.data[i].itemLocation,
                        "disposition": response.data[i].disposition,
                        "lotId": response.data[i].lotId,
                        "qty": response.data[i].quantity,
                        "hasChild": response.data[i].hasChild,
                        "rootEntId": response.data[i].rootEntId,
                        "mfrName": response.data[i].mfrName,
                        "$$treeLevel": level
                    }

                    $scope.eventItemDetailsList.data.splice(+currentIndex + 1 + +i, 0, obj);
                }
            }
        }, function(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getEventInformation(eventId) {
        var reqObject = {
            "eventId": eventId
        };

        traceEventsDetailsService.getEventInformation().save(reqObject).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $scope.eventInformation = response.data;
                /*if ($scope.eventInformation.eventTypeId === 32) {
                    getEventInputItemSummary(selectedEventId);
                    getEventInputItemDetails(selectedEventId);
                }*/
            }
        }, function(error) {
        	if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getEventNotifications(eventId) {

        var reqObject = {
            "eventId": eventId
        };

        traceEventsDetailsService.getEventNotifications().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                for (var i in response.listData) {
                    $scope.notificationsList.data[i] = {
                        "notificationId": response.listData[i].notificationId,
                        "eventId": response.listData[i].eventId,
                        "eventExtId": response.listData[i].eventExtId,
                        "notificationType": response.listData[i].notificationType != null ? response.listData[i].notificationType : '',
                        "subType": response.listData[i].notificationSubType != null ? response.listData[i].notificationSubType : '',
                        "raisedTimeStamp": response.listData[i].createDate,
                        "notification": response.listData[i].subject
                    }
                }
            }
        }, function(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getRelatedEvents(eventId) {
        var reqObject = {
            "eventId": eventId /*"621519360851"*/
        }

        traceEventsDetailsService.getRelatedEvents().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                for (var i in response.listData) {

                    var bizTranaction;
                    if (!angular.equals({}, response.listData[i].bizTransactions)) {
                        var bizTranaction = response.listData[i].bizTransactions.bizTransTypeValue + ':' + response.listData[i].bizTransactions.bizTransId;
                    } else {
                        bizTranaction = '';
                    }

                    var obj = {
                        "eventId": response.listData[i].eventId,
                        "eventExtId": response.listData[i].eventExtId,
                        "eventType": response.listData[i].eventType,
                        "eventStatus": response.listData[i].eventStatusId,
                        "eventDateTime": response.listData[i].eventDateTime,
                        "eventLocation": response.listData[i].occuredAt,
                        /*"bizEventType": '',*/
                        "bizTransaction": bizTranaction,
                        "sender": response.listData[i].senderLocName !== undefined ? response.listData[i].senderLocName : '',
                        "receiver": response.listData[i].receiverLocName !== undefined ? response.listData[i].receiverLocName : ''
                    }
                    $scope.relatedEventsList.data.push(obj);
                }
            }
        }, function(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function downloadCompilanceReport(eventId, reportType) {
        var reqObject = {
            "eventId": eventId,
            "fileType": reportType
        }
        var rService;

        rService = traceEventsDetailsService.downloadEPCISComplianceReport(reqObject);
        rService.success(onReportDownloadSuccess);
        rService.error(onReportDownloadFault);


        function onReportDownloadSuccess(data, status, headers) {
            if (data.responseStatus === "error" && data.responseCode === 403) {
                $scope.$emit('genericErrorEvent', data);
                return;
            }
            headers = headers();
            var full_filename = headers['content-disposition'];
            if (typeof(full_filename) !== 'undefined') {
                var fileNameArray = full_filename.split("=");
                var filename = fileNameArray[1];
                var contentType = headers['content-type'];
                var linkElement = document.createElement('a');
                try {

                    var blob = new Blob([data], {
                        type: contentType
                    });

                    if (window.navigator.msSaveOrOpenBlob) {
                        window.navigator.msSaveOrOpenBlob(blob, filename);
                    } else {
                        var url = window.URL.createObjectURL(blob);
                        linkElement.setAttribute('href', url);
                        linkElement.setAttribute("download", filename);
                        var clickEvent = new MouseEvent("click", {
                            "view": window,
                            "bubbles": true,
                            "cancelable": false
                        });
                        linkElement.dispatchEvent(clickEvent);
                    }
                } catch (ex) {
                    console.log(ex);
                }
            }else{
            	var alert = $mdDialog.alert({
                    title: '',
                    //textContent: 'Some Error Occured',
                    htmlContent: 'The compliance report is being generated or the report generation has failed. Please check ' + '<br>' + 'the notifications and/or check the compliance report again later. Thank you.',
                    ok: 'OK'
                });

                $mdDialog
                    .show(alert)
                    .finally(function () {
                        alert = undefined;
                    });
            }
        }

        function onReportDownloadFault(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            } else {
                alert("The system cannot find the file");
            }
        }
    }

    function downloadPdfReport(eventId, reportType) {
        var reqObject = {
            "eventId": eventId,
            "fileType": reportType
        }

        var url = $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/download/downloadPDFComplianceReport';
        console.log(url);
        console.log(reqObject);

        $http({
            method: 'POST',
            url: url,
            //params: reqObject,
            data: reqObject,
            headers: {
                'Content-Type': 'application/json'
            },
            responseType: 'arraybuffer'
        }).success(function(data, status, headers) {
            headers = headers();
            var full_filename = headers['content-disposition'];
            if (typeof(full_filename) !== 'undefined') {
                var fileNameArray = full_filename.split("=");
                var filename = fileNameArray[1];
                var contentType = headers['content-type'];
                var linkElement = document.createElement('a');
                try {
                    var blob = new Blob([data], { type: contentType });
                    var url = window.URL.createObjectURL(blob);

                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);

                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                } catch (ex) {
                    console.log(ex);
                }
            }else{
            	var alert = $mdDialog.alert({
                    title: '',
                    //textContent: 'Some Error Occured',
					htmlContent: 'The compliance report is being generated or the report generation has failed. Please check ' + '<br>' + 'the notifications and/or check the compliance report again later. Thank you.',
                    ok: 'OK'
                });

                $mdDialog
                    .show(alert)
                    .finally(function () {
                        alert = undefined;
                    });
            }
            
        }).error(function(data) {
            console.log(data);
        });
    }

    function getCompilanceReportFlags(eventId) {
        var reqObject = {
            "eventId": eventId
        }

        traceEventsDetailsService.getCompilanceReportFlags().save(JSON.stringify(reqObject)).$promise.then(function(response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
			if (response.responseStatus === "success") {
                $scope.complianceReportsList.data = [
                    { reportName: 'Transaction Report', reportType: 'TraceReport', sent: '', response: '', dataReportKind: response.data.transactionReport >= 1 ? 'pdf' : 'pdf-disable', enabled: response.data.transactionReport },
                    { reportName: 'PDF', reportType: 'Envelope', sent: '', response: '', dataReportKind: response.data.pdf >= 1 ? 'pdf' : 'pdf-disable', enabled: response.data.pdf },
                    { reportName: 'ASN', reportType: 'asnXML', sent: '', response: '', dataReportKind: response.data.asn >= 1 ? 'asn' : 'epcis-disable', enabled: response.data.asn },
					{ reportName: 'Brazil EPCIS', sent: '', reportType: 'epcisBrazilXML', response: '', dataReportKind: response.data.epcisBrazil >= 1 ? 'epcis' : 'epcis-disable', enabled: response.data.epcisBrazil },
                    { reportName: 'South Korea EPCIS', reportType: 'epcisSKXML', sent: '', response: '', dataReportKind: response.data.epcisSk >= 1 ? 'epcis' : 'epcis-disable', enabled: response.data.epcisSk },
                    /*{ reportName: 'Eisai South Korea EPCIS', reportType: 'epcisEKIXML', sent: '', response: '', dataReportKind: response.data.epcisEki >= 1 ? 'epcis' : 'epcis-disable', enabled: response.data.epcisEki },*/
					{ reportName: 'South Korea ES-EPCIS', reportType: 'epcisEKIXML', sent: '', response: '', dataReportKind: response.data.epcisEki >= 1 ? 'epcis' : 'epcis-disable', enabled: response.data.epcisEki },
                    { reportName: 'EPCIS', reportType: 'epcisXML', sent: '', response: '', dataReportKind: response.data.epcis >= 1 ? 'epcis' : 'epcis-disable', enabled: response.data.epcis },
                    { reportName: '3PL Ship', reportType: '3PLShip', sent: '', response: '', dataReportKind: response.data.pl >= 1 ? 'epcis' : 'epcis-disable', enabled: response.data.pl },
                    { reportName: 'EU', reportType: 'EU', sent: '', response: '', dataReportKind: response.data.eu >= 1 ? 'eu' : 'eu-disable', enabled: response.data.eu }
			            ]
            }
        }, function(error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }



    $scope.toggleRow = function(row, evt) {
        uiGridTreeBaseService.toggleRowTreeState($scope.gridApi.grid, row, evt);
        //$scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
    };

    $scope.toggleExpandNoChildren = function() {
        $scope.gridOptions.showTreeExpandNoChildren = !$scope.gridOptions.showTreeExpandNoChildren;
        $scope.gridApi.grid.refresh();
    }
    $scope.clickTraceId = function(traceId) {
        //sharedDataService.setTraceEntId(traceId);
        $state.go('app.traceItemDetails', { traceId: traceId, deviceId: deviceId });
    }

    $scope.showItemPrefix = function(itemInfo) {
        if (itemInfo == 'input') {
            $scope.traceIdinputItemInfoPrefix.isVisible = $scope.traceIdinputItemInfoPrefix.isVisible === false ? true : false;
            $scope.eventInputItemDetailsList.columnDefs[0].width = $scope.traceIdinputItemInfoPrefix.isVisible === true ? 320 : 220;
            $scope.gridApiInputItemInfo.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        } else {
            $scope.traceIdPrefix.isVisible = $scope.traceIdPrefix.isVisible === false ? true : false;
            $scope.eventItemDetailsList.columnDefs[0].width = $scope.traceIdPrefix.isVisible === true ? 320 : 220;
            $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        }

    }

    $scope.clickEventId = function(selectedEventId) {
        $scope.eventOutputItemSummaryList.data = [];
        $scope.eventInputItemSummaryList.data = [];
        $scope.eventItemDetailsList.data = [];
        $scope.eventInputItemDetailsList.data = [];
        $scope.eventInformation.data = [];
        $scope.notificationsList.data = [];
        $scope.relatedEventsList.data = [];

        getEventInformation(selectedEventId);
        getEventOutputItemSummary(selectedEventId);
        getEventItemDetails(selectedEventId);
        getEventNotifications(selectedEventId);
        getRelatedEvents(selectedEventId);
    }

    $scope.downloadReports = function(reportType) {
		if (reportType == 'TraceReport' || reportType == 'Envelope') {
            downloadPdfReport(selectedEventId, reportType);
        } else {
            downloadCompilanceReport(selectedEventId, reportType);
        }
    }

    $scope.clickNotificationId = function(notificationId) {
        $window.close(); // Close the existing popup and open the new one
        console.log(notificationId);
        sharedDataService.setNotificationId(notificationId);
        var url = $state.href('app.viewNotification', { notificationId: notificationId }),
            height = $window.innerHeight * .90,
            width = $window.innerWidth * .95;

        $window.open(url, "", "width=" + width + ",height=" + height + ",left=40,top=40,scrollbars=1,resizable=1");
    };

    $scope.showMap = function(eventInformation) {

        var url = $state.href('app.traceEventDetailsMap', { eventId: eventInformation.eventId }),
            height = $window.innerHeight * .90,
            width = $window.innerWidth * .95;
        $window.eventInformation = JSON.stringify(eventInformation);
        $window.open(url, "", "width=" + width + ",height=" + height + ",left=40,top=40,scrollbars=1,resizable=1");
    };

    $scope.showCollapse = function(panelModel, panelName) {
        if (panelName == 'notification') {
            $scope.collapse.isNotificationOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'relatedEvents') {
            $scope.collapse.isRelatedEventsOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'itemInfo') {
            $scope.collapse.isItemInfoOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'inputItemInfo') {
            $scope.collapse.isInputItemInfoOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'reporting') {
            $scope.collapse.isReportsOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'eventInfo') {
            $scope.collapse.isEventInfoOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'inputSummary') {
            $scope.collapse.isInputSummaryOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'outputSummary') {
            $scope.collapse.isOutputSummaryOpen = (panelModel === true) ? false : true;
        }
    }
}