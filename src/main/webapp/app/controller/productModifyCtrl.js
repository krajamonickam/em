angular.module('rfxcelApp').controller('productModifyCtrl', productModifyCtrl);

productModifyCtrl.$inject = ['$scope', '$state','$localStorage','sharedDataService','$filter','productDetailsService','productAddService','$mdDialog','generalUtilityService'];

function productModifyCtrl($scope,$state,$localStorage,sharedDataService,$filter,productDetailsService,productAddService,$mdDialog,generalUtilityService) {
    loadProductProfileDetails();
	var currentProfileId='';
	var isManualAddress=false;
	$scope.shipmentFrom = '';
	$scope.shipmentTo = '';
	$scope.rtsItemId = sharedDataService.getRTSTraceId();
	$scope.rtsSerialNumber = sharedDataService.getRTSSerialNumber();
	function loadProductProfileDetails() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject = {
            "dateTime": currentDate,
			"orgId": $localStorage.orgId,
			"groupId": $localStorage.groupId,
            "deviceData": {
                "packageId": sharedDataService.getSelectedShipmentId(),
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "shipmentStatus": sharedDataService.getSelectedShipmentType(),
                "rtsItemId": sharedDataService.getRTSTraceId(),
                "rtsSerialNumber": sharedDataService.getRTSSerialNumber()
            }
        };
        productDetailsService.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.packageId=response.packageId;
            	var shipmentData=response.sensor.shipmentData;
            	$scope.deviceId=response.deviceId;
            	$scope.devicetype=shipmentData.deviceCode;
            	$scope.productId=shipmentData.productId;
            	$scope.shipmentFrom = shipmentData.shipFrom;
            	$scope.shipFromLocation=shipmentData.shipFrom.location;
            	$scope.shipFromLocationId=shipmentData.shipFrom.locationId;
            	$scope.shipFromAdd1=shipmentData.shipFrom.add1;
            	$scope.shipFromAdd2=shipmentData.shipFrom.add2;
            	$scope.shipFromCity=shipmentData.shipFrom.city;
            	$scope.shipFromState=shipmentData.shipFrom.state;
            	$scope.shipFromCountry=shipmentData.shipFrom.country;
            	$scope.shipFromZip=shipmentData.shipFrom.zip;
            	$scope.shipmentTo = shipmentData.shipTo;
            	$scope.shipToLocation=shipmentData.shipTo.location;
            	$scope.shipToLocationId=shipmentData.shipTo.locationId;
            	$scope.shipToAdd1=shipmentData.shipTo.add1;
            	$scope.shipToAdd2=shipmentData.shipTo.add2;
            	$scope.shipToCity=shipmentData.shipTo.city;
            	$scope.shipToState=shipmentData.shipTo.state;
            	$scope.shipToCountry=shipmentData.shipTo.country;
            	$scope.shipToZip=shipmentData.shipTo.zip;
            	var profileName=shipmentData.profileName;
            	currentProfileId=shipmentData.deviceProfile;
            	getProfilesByDevice(shipmentData.deviceCode);
            	$scope.deviceProfile={
            			profileId:  currentProfileId,
            			name: profileName
            	}
            }else{
            	$scope.message = response.responseMessage;
            }            
        }, function (error) {
            console.log(error);
        })
    }
	
	function getProfilesByDevice(deviceTypeValue){
		        var reqObject = {
		        		"deviceTypeId" : deviceTypeValue,
		        		"profile" : {
		        		"orgId" : $localStorage.orgId,
		        		"groupId" : $localStorage.groupId
		        		}
		        }
        productAddService.getProfilesByDeviceType.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	 $scope.profiles = [];
            	$scope.profiles = response.profileList;
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
	
	$scope.manualAddress=function(){
    	isManualAddress=true;
    }
	
	$scope.getHomeAddressByProfileId=function(profileId){
		if(isManualAddress || !checkIfAddressIsBlank()){
			return;
		}
		if(currentProfileId === profileId.toString()){
			if($scope.shipmentFrom !== undefined) {
				if($scope.shipmentFrom !== null) {
					$scope.shipFromLocation=$scope.shipmentFrom.location;
	            	$scope.shipFromLocationId=$scope.shipmentFrom.locationId;
	            	$scope.shipFromAdd1=$scope.shipmentFrom.add1;
	            	$scope.shipFromAdd2=$scope.shipmentFrom.add2;
	            	$scope.shipFromCity=$scope.shipmentFrom.city;
	            	$scope.shipFromState=$scope.shipmentFrom.state;
	            	$scope.shipFromCountry=$scope.shipmentFrom.country;
	            	$scope.shipFromZip=$scope.shipmentFrom.zip;
				}
			}
			if($scope.shipmentTo !== undefined) {
				if($scope.shipmentTo !== null) {
					$scope.shipToLocation=$scope.shipmentTo.location;
					$scope.shipToLocationId=$scope.shipmentTo.locationId;
					$scope.shipToAdd1=$scope.shipmentTo.add1;
	            	$scope.shipToAdd2=$scope.shipmentTo.add2;
	            	$scope.shipToCity=$scope.shipmentTo.city;
	            	$scope.shipToState=$scope.shipmentTo.state;
	            	$scope.shipToCountry=$scope.shipmentTo.country;
	            	$scope.shipToZip=$scope.shipmentTo.zip;
				}
			}
		}else{
			$scope.shipFromLocation='';
			//$scope.shipFromLocationId='';
			$scope.shipFromAdd1='';
			$scope.shipFromAdd2='';
			$scope.shipFromCity='';
			$scope.shipFromState='';
			$scope.shipFromCountry='';
			$scope.shipFromZip='';
			$scope.shipToLocation='';
			//$scope.shipToLocationId='';
			$scope.shipToAdd1='';
			$scope.shipToAdd2='';
			$scope.shipToCity='';
			$scope.shipToState='';
			$scope.shipToCountry='';
			$scope.shipToZip='';
			getHomeAddressByProfileIdDetail(profileId);
		}
	}
    
    function getHomeAddressByProfileIdDetail(profileId){
    	var reqObject = {
    			"profile": {
    				"profileId": profileId,
    				"orgId": $localStorage.orgId
    			}
    		}

		productAddService.getHomeAddressByProfileId.save(reqObject).$promise.then(function (response) {
		if (response.responseStatus === "error" && response.responseCode === 403) {
		    $scope.$emit('genericErrorEvent', response);
		    return;
		}
		
		if (response.responseStatus == "success") {
				var homeAddObj={};
				var destAddObj={};
				if(response.profile.geopoints !== undefined && response.profile.geopoints !== null && response.profile.geopoints.length !== 0){
					//if(response.profile.geopoints.length === 2){
						for(var i=0;i<response.profile.geopoints.length;i++){
							if(response.profile.geopoints[i].locationType === 1){
					            $scope.shipFromLocation = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
								$scope.shipFromAdd1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
								$scope.shipFromAdd2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
								$scope.shipFromCity = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
								$scope.shipFromZip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
								$scope.shipFromState = typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
								$scope.shipFromCountry = typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
							}else{
								$scope.shipToLocation = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
								$scope.shipToAdd1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
								$scope.shipToAdd2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
								$scope.shipToCity = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
								$scope.shipToZip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
								$scope.shipToState = typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
								$scope.shipToCountry = typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
							}
						}
					/*}else{
						for(var i in response.profile.geopoints.length){
							if(response.profile.geopoints[0].locationType === 1){
								$scope.shipFromLocation = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
								$scope.shipFromAdd1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
								$scope.shipFromAdd2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
								$scope.shipFromCity = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
								$scope.shipFromZip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
								$scope.shipFromState = typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
								$scope.shipFromCountry = typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
							}else{
								$scope.shipToLocation = typeof(response.profile.geopoints[i])!=="undefined"?response.profile.geopoints[i].pointName:'';
								$scope.shipToAdd1 = typeof(response.profile.geopoints[i].addr1)!=="undefined"?response.profile.geopoints[i].addr1:'';
								$scope.shipToAdd2 = typeof(response.profile.geopoints[i].addr2)!=="undefined"?response.profile.geopoints[i].addr2:'';
								$scope.shipToCity = typeof(response.profile.geopoints[i].city)!=="undefined"?response.profile.geopoints[i].city:'';
								$scope.shipToZip =  typeof(response.profile.geopoints[i].zip)!=="undefined"?response.profile.geopoints[i].zip:'';
								$scope.shipToState = typeof(response.profile.geopoints[i].state)!=="undefined"?response.profile.geopoints[i].state:'';
								$scope.shipToCountry = typeof(response.profile.geopoints[i].country)!=="undefined"?response.profile.geopoints[i].country:'';
							}
						}
					}*/
				}
        } else {
		    $scope.message = response.responseMessage;
		}
		}, function (error) {
		console.log(error);
		});
	}
    
    $scope.modifyProduct=function(){
         var date = new Date();
         var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
         var reqObject = {
         		"orgId": $localStorage.orgId,
                 "productDetail": {
                     "packageId": $scope.packageId,
                     "productId": $scope.productId,
                     "deviceId": $scope.deviceId,
                     "deviceType": $scope.devicetype,
                     "profileId": $scope.deviceProfile.profileId,
                     "rtsItemId":$scope.rtsItemId,
                     "rtsSerialNumber":$scope.rtsSerialNumber,                     
                     "shipFrom": {
                         "locationId": $scope.shipFromLocationId,
                         "locationName": $scope.shipFromLocation,
                         "address1": $scope.shipFromAdd1,
                         "address2": $scope.shipFromAdd2,
                         "city": $scope.shipFromCity,
                         "state": $scope.shipFromState,
                         "country": $scope.shipFromCountry,
                         "postalCode": $scope.shipFromZip
                     },
                     "shipTo": {
                         "locationId": $scope.shipToLocationId,
                         "locationName": $scope.shipToLocation,
                         "address1": $scope.shipToAdd1,
                         "address2": $scope.shipToAdd2,
                         "city": $scope.shipToCity,
                         "state": $scope.shipToState,
                         "country": $scope.shipToCountry,
                         "postalCode": $scope.shipToZip
                     }
                 }
             }
         
         productAddService.modifyProductDetail.post(JSON.stringify(reqObject)).$promise.then(function (response) {
             if (response.responseStatus === "error" && response.responseCode === 403) {
                 $scope.$emit('genericErrorEvent', response);
                 return;
             }
             if (response.responseStatus == "success") {
                 if(sharedDataService.getLastVisitedUIState()==="app.dashboard.showMap"){
                    $state.go('app.dashboard.showMap');
                 }else{
                    $state.go('app.products');    
                 }
            }else {
                 $scope.message = response.responseMessage;
             }
         }, function (error) {
             console.log(error);
         });
    }
    
    
    
    $scope.disassociate = function (deviceId, packageId, productId) {
    var confirm = $mdDialog.confirm().title('Are you sure you want to disassociate?').ok('Yes').cancel('No');
        $mdDialog.show(confirm).then(function() {
       	 var date = new Date();
            var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
            var reqObject = {
                "dateTime": currentDate,
                "orgId": $localStorage.orgId,
                "deviceData": {
                    "deviceId": deviceId,
                    "packageId": packageId,
                    "productId": productId
                }
            }
            productAddService.disassociate.post(reqObject).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus == "success") {
                	var alert = $mdDialog.alert({
                        title:  response.responseMessage,
                        ok: 'Close'
                      });
                      $mdDialog
                        .show(alert )
                        .finally(function() {
                        	$state.go('app.addproduct');
                        });	
                	
                } else {
                    $scope.message = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });   
   }
    $scope.Cancel = function () {
        $state.go('app.products');
    }
    
    function checkIfAddressIsBlank(){
    	if($scope.shipFromLocation==='' && $scope.shipFromAdd1==='' && $scope.shipFromAdd2==='' && $scope.shipFromCity==='' && $scope.shipFromState==='' && $scope.shipFromCountry==='' && $scope.shipFromZip==='' &&
    			$scope.shipToLocation==='' && $scope.shipToAdd1==='' && $scope.shipToAdd2==='' && $scope.shipToCity==='' && $scope.shipToState==='' && $scope.shipToCountry==='' && $scope.shipToZip===''){
    		return true;
    	}
    	return false;
    }
}