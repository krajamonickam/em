angular.module('rfxcelApp').controller('systemOrgSettingsCtrl', systemOrgSettingsCtrl);
systemOrgSettingsCtrl.$inject = ['$scope', '$rootScope', '$state', 'systemOrgSettingsService', '$localStorage', '$mdDialog'];

function systemOrgSettingsCtrl($scope, $rootScope, $state, systemOrgSettingsService, $localStorage, $mdDialog) {
	$scope.appTitle = $rootScope.appTitle;
	getSettingValues();
    
	function getSettingValues(){
    	var reqObject = {
    			"setting": {
    				"orgId": $localStorage.orgId
    			}	
		};
	
      	systemOrgSettingsService.getSettingList.save(reqObject).$promise.then(function(response){
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
    			$scope.settingValList = response.settingList;
            }
            else {
            	$scope.message = response.responseMessage;
            }
		}, function(error){
        	console.log(error);
        });
            
    }

	$scope.editSettingVal = function (settingVal) {
    	var settingValJson = [];
    	angular.forEach(settingVal, function(value, key) {
    		settingValJson.push({
    			"paramName": value.paramName,
				"paramValue": value.paramValue,
				"displayName": value.displayName
            });
    	});

        var reqObject = {
            	"orgId": $localStorage.orgId,
            	"settingList": settingValJson
          }
        
        systemOrgSettingsService.editSettingVal.save(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                /*$mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).textContent("Settings saved successfully").ok('Close'));*/
            	$mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title("Settings saved successfully").ok('Close'));
                /*$state.go('app.systemSettings');*/
				$state.reload('app.settings');
			} else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        })
    }
	$scope.cancel=function(){
		getSettingValues();
	}
}