'use strict';

angular.module('rfxcelApp').controller('autoCompleteCtrl', autoCompleteCtrl);

autoCompleteCtrl.$inject = ['$timeout', '$scope', '$q', '$log', 'DeviceTypeService', 'sharedDataService', '$localStorage'];

function autoCompleteCtrl($timeout, $scope, $q, $log, DeviceTypeService, sharedDataService, $localStorage) {
    
    var self = this;
	self.simulateQuery = true;
    self.isDisabled    = true;
    self.querySearch   = querySearch;
    self.selectedItemChange = selectedItemChange;
    self.searchTextChange   = searchTextChange;
    
    /*display the deviceId based on product serial number*/
    $scope.$on("populateDeviceId", populateDeviceIdHandler);    
    function populateDeviceIdHandler(event, deviceId) {
        self.searchText = deviceId;
    }
    
    /*Search the deviceId*/
    function querySearch(query) {
        if (query == "" || query == null) {
            return;
        }
        var reqObject = {
            "orgId": $localStorage.orgId,
            "groupId": $localStorage.groupId,
            "searchType": 1,
            "searchString": query
        }
        $scope.$emit('deviceEntered', query);
        return DeviceTypeService.getDeviceIdDetails().save(reqObject).$promise.then(function (response) {
            return response.sensor.deviceList;
        }, function (error) {
            console.log(error);
        });
    }
    

    function searchTextChange(text) {
        if (text == "") {
            return;
        }
        //$scope.$emit('itemSearchChanged', text);
        self.querySearch = querySearch;
    }

    function selectedItemChange(deviceID) {
        if (angular.isUndefined(deviceID)) {
            $scope.$emit('deviceSelected', deviceID);
            return;
        }
        /*TBD: Remove event and develope input output API approch*/
        // $scope.onDeviceSelection({deviceID: item});
        $scope.$emit('deviceSelected', deviceID);
    }
}