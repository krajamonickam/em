
	angular.module('rfxcelApp').controller('itemMapController', itemMapController);

	itemMapController.$inject = ['$scope', '$state', 'itemService', 'sharedDataService', '$filter', 'sessionService', 'mapConfigService',  '$timeout', '$window','$stateParams','itemMapService'];

	function itemMapController($scope, $state, itemService, sharedDataService, $filter, sessionService, mapConfigService,  $timeout, $window ,$stateParams, itemMapService) {
		
		var traceId = ($stateParams.traceId === '') ? sharedDataService.getTraceEntId() : $stateParams.traceId;
		if(traceId  != undefined && traceId != ''){
			var requestObject = {
					"traceItemId" : traceId
			};
			itemMapService.getTraceItemMapLocations.save(JSON.stringify(requestObject)).$promise.then(function (response) {
				if (response.responseStatus === "error" && response.responseCode === 403) {
				 $scope.$emit('genericErrorEvent', response);
				 return;
				}
				if (response.responseStatus === "success") {
					$scope.mapData = response.listData;
					if($scope.mapData.length > 0){
						var markerColorArr = mapConfigService.getMarkerColorCode();
						var latLngPath=[];
						var markersHalo=[];
						var markers = [];
						
					    var map = new google.maps.Map(document.getElementById('traceItemMap'), {
					      zoom: 6,
					      center: new google.maps.LatLng($scope.mapData[0].lat,$scope.mapData[0].lng),
					      mapTypeId: google.maps.MapTypeId.HYBRID
					    });
					    //map.setCenter(new google.maps.LatLng(37.4419, -122.1419));
					    
					    var latLngFirstPoint = new google.maps.LatLng($scope.mapData[0].lat, $scope.mapData[0].lng);
					    $scope.productLocation  = $scope.mapData[0].lat + ',' + $scope.mapData[0].lng;
					    myTrip = [];
					    
					    for (var i = 0; i < $scope.mapData.length; i++) {
					        var latLng = new google.maps.LatLng($scope.mapData[i].lat, $scope.mapData[i].lng);
					        myTrip.push(latLng);
					        // Push the 1st datapoint but don't draw the flightpath. Flightpath must be drawn only if more than one datapoint
					        if (i === 0) {
					            latLngPath.push(latLng);
					        }
					        if (i > 0) { // Push the datapoint and draw the flightpath.
					            latLngPath.push(latLng);
					            drawFlightPath(map, $scope.mapData[i-1].pairStatus);
					        }
					    }
					    
					    
					    if ($scope.mapData.length === 1) { // plot only the start point
					        loopCount = 1;
					        startEndPointArr = [
					            {
					                lat: $scope.mapData[0].lat,
					                lng: $scope.mapData[0].lng,
					                productAttrIdIndex: 0,
					                pairStatus: $scope.mapData[0].pairStatus
					            }
					        ];
					    } else if ($scope.mapData.length > 1) {
					        loopCount = 2;
					        mapLoclastIndex = $scope.mapData.length - 1;
					        startEndPointArr = [
					            {
					                lat: $scope.mapData[0].lat,
					                lng: $scope.mapData[0].lng,
					                productAttrIdIndex: 0,
					                pairStatus: $scope.mapData[0].pairStatus
					            },
					            {
					                lat: $scope.mapData[mapLoclastIndex].lat,
					                lng: $scope.mapData[mapLoclastIndex].lng,
					                productAttrIdIndex: mapLoclastIndex,
					                pairStatus: $scope.mapData[mapLoclastIndex].pairStatus
					            }
					        ];
					    }
					    
					    var latLngStartPoint = new google.maps.LatLng(startEndPointArr[0].lat, startEndPointArr[0].lng);
					    
					    var latLngLastPoint = new google.maps.LatLng(startEndPointArr[loopCount - 1].lat, startEndPointArr[loopCount - 1].lng);
					    plotHaloMarker(latLngLastPoint, startEndPointArr[loopCount - 1].productAttrIdIndex, startEndPointArr[loopCount - 1].pairStatus);
					    
					    for (var i = 0; i < $scope.mapData.length; i++) {
					        var latLng = new google.maps.LatLng($scope.mapData[i].lat, $scope.mapData[i].lng);
					        plotMarker(latLng, i, $scope.mapData[i].pairStatus, 8, markerColorArr[$scope.mapData[i].pairStatus],$scope.mapData[i]);   
					    }
					    
					    if (loopCount === 2) { // Show the white center marker only when there are more than 1 point 
					    	plotRoundMarker(latLngStartPoint, 0, 3, 4, '#FFFFFF',$scope.mapData[0]); // plot start point white center marker
					    }
					    
					    function plotHaloMarker(latLng, lastDataPointIndex, pairStatus) {
					        // remove previous halo marker
					        if (markersHalo.length > 0) {
					            markersHalo[0].setMap(null);
					            markersHalo = [];
					        }
					        //var markerColorArr = sharedDataService.getMarkerColorCode();
					        var scale = 20,
					            fillOpacity = 0.3,
					            markerHalo = new google.maps.Marker({
					                position: latLng,
					                icon: {
					                    path: google.maps.SymbolPath.CIRCLE,
					                    scale: scale,
					                    fillColor: markerColorArr[pairStatus],
					                    fillOpacity: fillOpacity,
					                    strokeWeight: 0.2
					                },
					                draggable: false,
					                map: map
					            });
					        markerHalo.setMap(map);
					        markersHalo.push(markerHalo);
					    }
					    
					    
					    function plotMarker(latLng, lastDataPointIndex, pairStatus, scale, fillColor,mapTempData) {
					    	
					    	/*var icons = "";
					    	
					    	switch (mapTempData.pairStatus) {
							case "1":
								icons = "//maps.google.com/mapfiles/ms/icons/green-dot.png";
								break;
							case "2":
								icons = "//maps.google.com/mapfiles/ms/icons/red-dot.png";
							default:
								icons = "//maps.google.com/mapfiles/ms/icons/red-dot.png";
								break;
							}*/
					    	var textNum = [];
					    	
					    	 for(var j=0 ; j< $scope.mapData.length; j++){
			            		 if($scope.mapData[j].lat == mapTempData.lat && $scope.mapData[j].lng == mapTempData.lng){
			            			 textNum.push(j+1);
			            		 }
			            	 }
					    	
					    	
					        var marker = new google.maps.Marker({
					            position: latLng,
					           // icon : icons,
					            data : mapTempData,
					            draggable: false,
					            map: map,
					            label: {color: '#000', fontSize: '12px', fontWeight: '600',text: ''+textNum.toString()+''}
					        });

					        marker.setMap(map);
					        markers.push(marker);
					        var addListener = function (lastDataPointIndex) {
					        	
					        	var events = "";
					        	for(var j=0;j<$scope.mapData.length;j++){
					        		if(mapTempData.lat  == $scope.mapData[j].lat && mapTempData.lng == $scope.mapData[j].lng){
					        			events = events + $scope.mapData[j].title;
					        		}
					        	}
					        	
					        	
					            google.maps.event.addListener(marker, 'click', function (event) {
					                // GET Attributes for the selected data point
					                //getProductAttributes(event, lastDataPointIndex);
					            	//console.log(marker.data);
					            	var markerData = marker.data;
					            	
					            	 var content = [];
					            	 var eventDate = [];
					            	 for(var j=0 ; j< $scope.mapData.length; j++){
					            		 if($scope.mapData[j].lat == markerData.lat && $scope.mapData[j].lng == markerData.lng){
					            			 content.push($scope.mapData[j].title);
					            			 eventDate.push($scope.mapData[j].eventDate);
					            		 }
					            	 }
					            	 var data = '<span> <b>Events : </b> <label class="text-primary">'+content.toString()+'<label></span><br />'
					            	 			//+'<span> <b>Date-time : </b> <label class="text-primary">'+eventDate.toString()+' <label></span>'
					            	 			+'<span> <b>Location : </b> <label class="text-primary">'+mapTempData.locationName+' <label></span>';
					            	 
						        	 var infowindow = new google.maps.InfoWindow({
						        	      content: data
						        	    });
					            	 infowindow.open(map,marker);
					            });
					        };
					        addListener(lastDataPointIndex);
					    }
					    function plotRoundMarker(latLng, lastDataPointIndex, pairStatus, scale, fillColor) {
				            var marker = new google.maps.Marker({
				                position: latLng,
				                icon: {
				                    path: google.maps.SymbolPath.CIRCLE,
				                    scale: scale,
				                    fillColor:  markerColorArr[0],
				                    fillOpacity: 1,
				                    strokeWeight: 0.2
				                },
				                draggable: false,
				                map: map
				            });

				            marker.setMap(map);
				            markers.push(marker);
				            var addListener = function (lastDataPointIndex) {
				                google.maps.event.addListener(marker, 'click', function (event) {
				                    // GET Attributes for the selected data point
				                    getProductAttributes(event, lastDataPointIndex);
				                });
				            };
				            addListener(lastDataPointIndex);
				        }
					    function drawFlightPath(map, pairStatus) {
					      /*  var flightPath = new google.maps.Polyline({
					            path: latLngPath,
					            strokeColor: markerColorArr[pairStatus],
					            strokeOpacity: 1,
					            strokeWeight: 6,
					            icons: [{
					                repeat: '100px'
					            }]
					        });
					        flightPath.setMap(map);
					        // get the new datapoint 
					        var lastLatLng = latLngPath.slice(-1)[0];
					        latLngPath = [];
					        latLngPath.push(lastLatLng);*/
					    	var flightPath = new google.maps.Polyline({
						        	path: latLngPath,
						        	strokeColor: markerColorArr[2],
						        	strokeOpacity: 1,
						        	strokeWeight: 4,
						        	zIndex:300,
						        	/*geodesic: true,
						        	zIndex: 300,*/
						        	icons: [{
							        	icon:{
							        	path: google.maps.SymbolPath.FORWARD_CLOSED_ARROW,
							        	//path: google.maps.SymbolPath.CIRCLE,
							        	strokeColor: markerColorArr[2],
							        	fillOpacity: 1,
							        	 scale: 3,
							        	 offset: '100%'
							        	},
						        	repeat: '100px'
						        	}]
					        	});
					        	flightPath.setMap(map);
					        	// get the new datapoint 
					        	var lastLatLng = latLngPath.slice(-1)[0];
					        	latLngPath = [];
					        	latLngPath.push(lastLatLng);
					    }
					    
					    function indexOfMarker(lat, lng) {
					        var res = -1;
					        
					        for (var i = 0, len = markers.length; i < len; i++) {
					            var position = markers[i].marker.getPosition();

					            if ((Math.abs(position.lat() - lat) < SM_ERROR) && 
					                (Math.abs(position.lng() - lng) < SM_ERROR)) {
					                // known position
					                res = i;
					                break;
					            }
					        }
					        return res;
					    }
					}
				}
			}, function (error) {
				console.log(error);
			});
		}
		/*$scope.mapData = [{"lat": '18.641400',"lng": '72.872200','pairStatus':0},
						{"lat": '18.964700',"lng": '72.825800','pairStatus':0},
							{"lat": '18.523600',"lng": '73.847800','pairStatus':0},
							{"lat": '19.2032',"lng": '73.8743','pairStatus':0},
							{"lat": '29.54492299999999',"lng": '-98.57727499999999','pairStatus':0},
				   ];*/
		
	    
	    
	}