angular.module('rfxcelApp').controller('addDeviceTypeCtrl', addDeviceTypeCtrl);
addDeviceTypeCtrl.$inject = ['$scope', '$state','sharedDataService','DeviceTypeService','UserLocalService','$sessionStorage'];

function addDeviceTypeCtrl($scope, $state,sharedDataService,DeviceTypeService,UserLocalService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
    $scope.deviceType='';
    $scope.manufacturer='';
    var attributesArrList=[];
    var attributes=UserLocalService.getAttributeList();
    getAttributes();
    
    $scope.addDeviceType=function(){    
         $state.go("app.user");
    }
    
    function getAttributes(){
        var setAttributeList=[];
		      for(var i in attributes){
                    setAttributeList.push({
                        attrId: attributes[i].attrId,
                        attrName: attributes[i].attrName,
                        unit: attributes[i].unit,
                        excursionDuration: 0,
                        legend: attributes[i].attrName,
                        rangeMin: attributes[i].rangeMin,
                        rangeMax: attributes[i].rangeMax,
                        notifyLimit: 1,
                        showGraph: false,
                        checkAttributes: false,
                        showMap:false,
                        rangeMin:attributes[i].rangeMin,
                        rangeMax:attributes[i].rangeMax,
                        isSensorAttribute:attributes[i].isSensorAttribute,
                        roundOffTo:attributes[i].roundOffTo,
                        alertOnReset:attributes[i].alertOnReset
                    });
                }
                $scope.attributeList=setAttributeList;
    }

    $scope.addDeviceTypes=function(){
       $scope.errorMessageMinRange = [];
       $scope.errorMessageMaxRange = [];
       for (var i in $scope.attributeList){
   	    	if($scope.attributeList[i].checkAttributes===true){
   	    		if($scope.attributeList[i].rangeMin === '' && $scope.attributeList[i].rangeMax !== ''){
    	   	    	$scope.errorMessageMinRange[i] = 'Min value is required';
    	   	    	return;
    	   	    }
   	    		else if(isNaN($scope.attributeList[i].rangeMin)){
    	   	    	$scope.errorMessageMinRange[i] = 'Min value should be an integer';
    	   	    	return;
    	   	    }
    	   	    else if($scope.attributeList[i].rangeMax === '' && $scope.attributeList[i].rangeMin !== ''){
    	   	    	$scope.errorMessageMaxRange[i] = 'Max value is required';
    	   	    	return;
    	   	    }
	    	   	else if(isNaN($scope.attributeList[i].rangeMax)){
	 	   	    	$scope.errorMessageMaxRange[i] = 'Max value should be an integer';
	 	   	    	return;
	 	   	    }
	    	   	else if($scope.attributeList[i].rangeMin !== '' && $scope.attributeList[i].rangeMax !== '' && parseFloat($scope.attributeList[i].rangeMin) === parseFloat($scope.attributeList[i].rangeMax)){
    	   	    	$scope.errorMessageMinRange[i] = 'Min & Max values cannot be same';
    	   	    	return;
    	   	    }
    	   	    else if($scope.attributeList[i].rangeMin !== '' && $scope.attributeList[i].rangeMax !== '' && parseFloat($scope.attributeList[i].rangeMin) > parseFloat($scope.attributeList[i].rangeMax)){
    	   	    	$scope.errorMessageMinRange[i] = "Min cannot exceed Max";
    	   	    	return;
    	   	    }
    	   	    else {
    	   	    	delete $scope.attributeList[i].checkAttributes;
    	   	    	attributesArrList.push($scope.attributeList[i]);
    	   	    }
            }
       }
        
    var reqObj={
           "orgId": selectedOrgId,
           "deviceDetail": {
               "deviceType": $scope.deviceType,
               "manufacturer":  $scope.manufacturer,
               "attributes": attributesArrList
           }
       }    
       DeviceTypeService.addDeviceTypes().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus == "success") {
                 
               $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }       
			}, function (error) {
			 console.log(error);
        });
    }
    
    $scope.cancel = function () {
        $state.go("app.user");
    };
}
