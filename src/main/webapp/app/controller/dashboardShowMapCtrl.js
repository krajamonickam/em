angular.module('rfxcelApp').controller('dashboardShowMapController', dashboardShowMapController);

dashboardShowMapController.$inject = ['$scope', '$rootScope', '$interval', '$filter', '$location', '$state', 'activeShipmentsService', 'sharedDataService', 'mapConfigService', '$localStorage','generalUtilityService'];

function dashboardShowMapController($scope, $rootScope, $interval, $filter, $location, $state, activeShipmentsService, sharedDataService, mapConfigService, $localStorage, generalUtilityService) {
    var loadActiveShipmentsInterval,
        refreshLoadActiveShipments,
        mapCenter = new google.maps.LatLng($localStorage.centerLat, $localStorage.centerLong),
        maxZoomVal = $localStorage.mapZoom,
        markers = [],
        mapParams = {
    		mapTypeId: google.maps.MapTypeId.HYBRID
    	},
        map = new google.maps.Map(document.getElementById("map-1"), mapParams);
    	var markerBounds = new google.maps.LatLngBounds();
    	if (!maxZoomVal) {
    		maxZoomVal = 6;
    	}
    	if (!$localStorage.centerLat) {
    		mapCenter = new google.maps.LatLng(40, -95);
    	}
    	
    function dashboardMap(isLatLongAvailable) {
        var marker,
            colorConversionArr = mapConfigService.getMarkerColorCode();
        if (markers.length > 0) {
            for (var i = 0; i < markers.length; i++) {
                markers[i].setMap(null);
            }
            markers = [];
        }

        for (var i = 0; i < $scope.shipmentLocationData.length; i++) {
            var latLng = new google.maps.LatLng($scope.shipmentLocationData[i].lat, $scope.shipmentLocationData[i].lang),
                pairStatus = $scope.shipmentLocationData[i].pairStatus;

            if (generalUtilityService.validateLatLong($scope.shipmentLocationData[i].lat, $scope.shipmentLocationData[i].lang)) {
                markerBounds.extend(latLng);
            }

            marker = new google.maps.Marker({
                position: latLng,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 8,
                    fillColor: colorConversionArr[pairStatus],
                    fillOpacity: 1,
                    strokeWeight: 0.3
                },
                draggable: false,
                map: map
            });
            marker.setMap(map);
            $scope.marker = marker;
            attachOnHover(marker, $scope.shipmentLocationData[i].productId + '-' + $scope.shipmentLocationData[i].packageId + '<br/> updated ' + generalUtilityService.getLastUpdatedTime($scope.shipmentLocationData[i].dateTime));

            // Push your newly created marker into the array:
            markers.push(marker);
            var addListener = function (i) {
                google.maps.event.addListener($scope.marker, 'click', function (event, data) {
                    // Highlight the selected product in product list on the left when marker is clicked
                    $rootScope.selectedProductIndex = generalUtilityService.getSelectedShipmentIndex($scope.activeShipmentsArr, $scope.shipmentLocationData[i]);
                    sharedDataService.setSelectedShipment($scope.shipmentLocationData[i]);
                    sharedDataService.setSelectedDeviceMarkerColor(colorConversionArr[$scope.activeShipmentsArr[$rootScope.selectedProductIndex].pairStatus]);
                    sharedDataService.setSelectedFavoriteOption($scope.activeShipmentsArr[$rootScope.selectedProductIndex].isfavorite);
                    sharedDataService.setSelectedProductId($scope.activeShipmentsArr[$rootScope.selectedProductIndex].productId);
                    sharedDataService.setSelectedDeviceId($scope.activeShipmentsArr[$rootScope.selectedProductIndex].deviceId);
                    sharedDataService.setSelectedShipmentId($scope.activeShipmentsArr[$rootScope.selectedProductIndex].packageId);
                    sharedDataService.setSelectedShipmentType($scope.activeShipmentsArr[$rootScope.selectedProductIndex].typeId);
                    sharedDataService.setRTSTraceId($scope.activeShipmentsArr[$rootScope.selectedProductIndex].rtsItemId);
                    sharedDataService.setRTSSerialNumber($scope.activeShipmentsArr[$rootScope.selectedProductIndex].rtsSerialNumber);
                    sharedDataService.setShowMapAttribute(true);
                    $scope.$apply(function () {
                        $state.go('app.dashboard.mapMarkerPath');
                    });
                });

            };
            addListener(i);

        } // for loop ends


        //if (markerBounds.isEmpty()) {
        //disabling the fitbounds code
        if (!isLatLongAvailable) {
            map.setCenter(mapCenter);
            map.setZoom(maxZoomVal);
        }
        else {
            map.fitBounds(markerBounds);
            var listener = google.maps.event.addListener(map, "idle", function () {
                if (map.getZoom() > maxZoomVal) map.setZoom(maxZoomVal);
                console.log('current zoom level for map : ' + map.getZoom());
                google.maps.event.removeListener(listener);
            });
        }
    } // function dashboardMap() ends
    

    function attachOnHover(marker, secretMessage) {
        var infowindow = new google.maps.InfoWindow({
            content: secretMessage
        });


        marker.addListener('mouseover', function () {
            infowindow.open(marker.get('map'), marker);
        });

        marker.addListener('mouseout', function () {
            infowindow.close();
        });

    }
    function loadActiveShipments() {
    	
    	/*
        if($scope.selectedItemType.typeId == 4 || $scope.selectedItemType.typeId == 5 || $scope.selectedItemType.typeId == 6){
            $scope.isRtsDataFetch = true;
        }else{
            $scope.isRtsDataFetch = false;
        }
        */
    	
    	$scope.isRtsDataFetch = false;
        
        var date = new Date(),
            currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
            reqObject = {
        		"orgId": $localStorage.orgId,
    			"groupId": $localStorage.groupId,
                "dateTime": currentDate,
                /*"searchType": $scope.selectedItemType.typeId,*/
                "rtsDataFetch" : $scope.isRtsDataFetch,
                "showMap" : true
            };

        activeShipmentsService.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.shipmentLocationData =  response.sensor.map.locations;
                $scope.activeShipmentsArr = response.sensor.shipmentList;
                sharedDataService.setShipmentList($scope.activeShipmentsArr);
                if (response.sensor.map.locations.length > 0) {
                    dashboardMap(true);
                } else {
                	dashboardMap(false);
                	console.log('no map data available');
                }
            }
        }, function (error) {
            console.log(error);
        });
    } // function loadActiveShipments ends
    
    // Display the Dashboard tab as selected in the Top Main Navigation tab
    $rootScope.activeTabNumbTopNav = 1;
    // Display the Map tab as selected in Dashboard Map/ Alerts tab
    $rootScope.activeDashboardTabNumb = 1;
    // Variable to highlight the selected product in product list on the left
    $rootScope.selectedProductIndex = null;
    // Show the MapAlertsBtn for app.dashboard.showMap state
    $rootScope.isShowMapAlertsBtn = true;
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.dashboard.showMap') {
            clearTimeout(refreshLoadActiveShipments);
            $rootScope.isShowMapAlertsBtn = false;  // hide the MapAlertsBtn for other states 
        }
    });
    
    $scope.$watch('selectedItemType', function(value) {
		loadActiveShipments();
	});
    
    $scope.$on('favouriteSelect', function(event, selectedProduct){
    	for(var i in $scope.activeShipmentsArr){
    		var value = $scope.activeShipmentsArr;
    		if (selectedProduct.deviceId == value[i].deviceId && selectedProduct.packageId == value[i].packageId &&  selectedProduct.productId == value[i].productId) {
    			$scope.activeShipmentsArr[i].isfavorite = selectedProduct.isfavorite === 1 ? 0 : 1;
    		}
    	}
    });
    
    loadActiveShipmentsInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
    refreshLoadActiveShipments = setInterval(loadActiveShipments, loadActiveShipmentsInterval);
}