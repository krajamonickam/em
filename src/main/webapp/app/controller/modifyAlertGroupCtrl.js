angular.module('rfxcelApp').controller('modifyAlertGroupCtrl', modifyAlertGroupCtrl);
modifyAlertGroupCtrl.$inject = ['$scope', '$state','UserService','UserLocalService','AlertGroupService','sharedDataService', '$localStorage','$sessionStorage'];

function modifyAlertGroupCtrl($scope, $state,UserService,UserLocalService,AlertGroupService,sharedDataService, $localStorage,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	$scope.alertGroup = UserLocalService.getAlertGroupList();
    getAlertGroups();
	getUserList();
	$scope.userList = [];
	$scope.selectedUserList = [];
	var selectedUsers = [];
	
	function getAlertGroups() {
        var reqObject = {
            "user": {
                "orgId": selectedOrgId/*,
                "groupId": $localStorage.groupId*/
            }
        };

        UserService.getUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                for(var i in response.userList){
            		var isUserExist=false;
                    for(var j in $scope.alertGroup.users){
                        if(response.userList[i].userId == $scope.alertGroup.users[j]){
                        	isUserExist=true;
                        }
                    }
                    if(isUserExist==false){
                        $scope.userList.push(response.userList[i]);
                    }
                    
            	}
            	
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
	
	function getUserList() {
		
		var reqObject = {
				"alertGroup": {
					"orgId": selectedOrgId, 
					 "id": $scope.alertGroup.id
				}
		}
		

        UserService.getUsers.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.users = response.userList;
                for(var i in $scope.alertGroup.users){
        			for(var j in $scope.users){
        				if( $scope.alertGroup.users[i] == $scope.users[j].userId){
        					$scope.selectedUserList.push($scope.users[j]);
        				}
        			}
        		}
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
	
	
	
	$scope.addUsersToGroup = function(availableUsers){
		angular.forEach(availableUsers, function($value, $key) {
			$scope.selectedUserList.push($value);
	    });
		if(availableUsers.length != 0){
		for(var i in $scope.selectedUserList){
			for(var j in $scope.userList){
				if( $scope.selectedUserList[i].userId == $scope.userList[j].userId){
					$scope.userList.splice(j,1);
				}
			}
			}
		}
	};
	
	$scope.removeUsersFromGroup = function(selectedUsers){
		angular.forEach(selectedUsers, function($value, $key) {
			$scope.userList.push($value);
	    });
		if(selectedUsers.length != 0){
		for(var i in $scope.userList){
			for(var j in $scope.selectedUserList){
				if( $scope.userList[i].userId == $scope.selectedUserList[j].userId){
					$scope.selectedUserList.splice(j,1);
				}
			}
		}
		}

	};	
	
	
    $scope.modify = function (alertGroupObj) {
    	for(var j in $scope.selectedUserList){
    		selectedUsers.push($scope.selectedUserList[j].userId);
    	}
    	var reqObject = {
    			"alertGroup": {
    				 	"id": $scope.alertGroup.id,
    	                "groupName": $scope.alertGroup.groupName,
    	   		     	"description":$scope.alertGroup.description ,
    	   		     	"orgId": selectedOrgId,
    	   		     	"groupId":$localStorage.groupId, 
    	   		     	"users" :selectedUsers
				}
    	};
    
    	AlertGroupService.editAlertGroup().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });

    };
    
    $scope.cancel = function () {
        $state.go('app.user');
    };
   

}
