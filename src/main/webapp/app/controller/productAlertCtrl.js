angular.module('rfxcelApp').controller('productAlertCtrl', productAlertCtrl);
productAlertCtrl.$inject = ['$scope', '$rootScope', 'productAlertService', '$filter', 'sharedDataService', '$state', '$localStorage'];

function productAlertCtrl($scope, $rootScope, productAlertService, $filter, sharedDataService, $state, $localStorage) {
	if(sharedDataService.getSelectedDeviceId() === undefined || sharedDataService.getSelectedShipmentId() === undefined || sharedDataService.getSelectedProductId() === undefined){
		var lastState = sharedDataService.getLastVisitedUIState();
		if($state.current.name === 'app.dashboard.alert'){
			 sharedDataService.setLastVisitedUIState($state.current.name);
		     //$state.go('app.dashboard.alert');
		} else{
			 sharedDataService.setLastVisitedUIState($state.current.name);
		     $state.go('app.dashboard');
		     return;
		}
	}
	
    $scope.isSelectedAlertSortBy = true;
    
    var loadNotificationInterval,
    refreshNotificationInterval;
    
    var loadSelectedShipmentCountInterval,
    refreshSelectedShipmentCountInterval;

	
	/* Variable to show/hide dashboard alerts */
	$scope.isShowAlerts = false;
	
    $rootScope.activeTabNumbTopNav = 3;
    getAlerts();
    
    $scope.getAlerts = function(){
    	$scope.noAlertsFound='';
    	getAlerts();
    }
    function getAlerts() {
        var reqObject;
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        if ($state.current.name === 'app.dashboard.alert') {
            reqObject = {
                "dateTime": currentDate,
                "sortBy": "Age",
                "searchType": "All",
                "groupBy": "device",
                "start": 0,
                "limit": "100",
				"orgId": $localStorage.orgId,
				"searchString":$scope.alert
			}
        } else {
            reqObject = {
                "dateTime": currentDate,
                "sortBy": "Age",
                "searchType": "container",
                "groupBy": "device",
                "start": 0,
                "limit": "100",
                "orgId": $localStorage.orgId,
                "searchString":$scope.alert,
                "deviceData": {
                    "deviceId": sharedDataService.getSelectedDeviceId(),
                    "packageId": sharedDataService.getSelectedShipmentId(),
                    "productId": sharedDataService.getSelectedProductId()
				}
            }
        }
        // Service to get the notification list
        productAlertService.getNotificationsAlerts.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.notificationList = response.shipmentNotifications;
            	
				/* Show/Hide alerts based on alert count */
				$scope.isShowAlerts = (response.shipmentNotifications.length > 0) ? true : false;
				/*$scope.isShowAlerts = false;*/
				if($scope.isShowAlerts === false){
					if($localStorage.fetchESData === true || $localStorage.alertDeviceLogSearch === true){                		
						if(response.responseMessage !== undefined){
							$scope.noAlertsFound=response.responseMessage;
						}else{
							$scope.noAlertsFound='No alert found';
						}
                	}
				}
            } else {
            	$scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        })
    } // function getAlerts ends
    
    if ($state.current.name == "app.dashboard.alert") {
        // Display the Dashboard Alert tab as selected 
        $rootScope.activeDashboardTabNumb = 2;
        $state.go('app.dashboard.alert');
    }
    if ($state.current.name == "app.productsDetails.alert") {
        $rootScope.activeTabNumbTopNav = 2;
        $state.go('app.productsDetails.alert');
    }
    
    //getAlertsCount();
    function getAlertsCount() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqCountObject = {
            "dateTime": currentDate,
            "searchType": "container",
			"orgId": $localStorage.orgId,
            "deviceData": {
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "packageId": sharedDataService.getSelectedShipmentId(),
                "productId": sharedDataService.getSelectedProductId()
            }
        }
        productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$rootScope.notificationCount = response.count;
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.dashboard.alert' || fromState.name === 'app.productsDetails.alert' ) {
           clearTimeout(refreshNotificationInterval);
           clearTimeout(refreshSelectedShipmentCountInterval);
        }
    });
    
    loadNotificationInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
    refreshNotificationInterval = setInterval(getAlerts, loadNotificationInterval);
    if ($state.current.name == "app.productsDetails.alert") {
    	refreshSelectedShipmentCountInterval = setInterval(getAlertsCount, loadNotificationInterval);
    }
}