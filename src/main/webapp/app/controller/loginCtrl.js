angular.module('rfxcelApp').controller('loginController', loginController);
loginController.$inject = ['$scope', '$state', 'loginService', '$filter', '$rootScope', '$mdDialog', 'resetDataService', 'simulatorService', '$localStorage', '$http', 'sharedDataService'];

function loginController($scope, $state, loginService, $filter, $rootScope, $mdDialog, resetDataService, simulatorService, $localStorage, $http, sharedDataService) {
    $scope.appTitle = $rootScope.appTitle;
    console.log('login page')
    /*Display the success message on login page*/
    $scope.passwordMessage = sharedDataService.getSuccessMessage();

    $scope.login = function () {
    	console.log('login page');
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject = {
            "userLogin": $scope.username,
            "password": $scope.password,
            "dateTime": currentDate
        };
        $http.defaults.headers.common['Shard'] = undefined;
        loginService.getShard.get({userLogin:$scope.username}).$promise.then(function (response) {
        	if (response.shard) {
                $localStorage.shard = response.shard;
                $http.defaults.headers.common['Shard'] = $localStorage.shard;
                loginService.login.post(reqObject).$promise.then(function (response) {
                    $localStorage.authToken = response.authToken;
                    $localStorage.username = $scope.username;
                    $localStorage.password = $scope.password;
                    $localStorage.shard = response.shard;
        			$http.defaults.headers.common['Authorization'] = $localStorage.authToken;
                    $http.defaults.headers.common['Content-Type'] = "application/json";
                    $http.defaults.headers.common['Access-Control-Allow-Origin'] = "*";
                    $http.defaults.headers.common['Shard'] = $localStorage.shard;
                    $rootScope.userName = $localStorage.username;
                    if (response.responseStatus == "success") {
                    	$localStorage.orgId = response.currentUser.orgId;
        	            $localStorage.groupId = response.currentUser.groupId;
        	            $localStorage.userType = response.currentUser.userType;
        	            $localStorage.userId = response.currentUser.userId;
        	            $localStorage.timeZoneOffset = response.currentUser.userTz;
        	            $localStorage.privilege = response.currentUser.privilege;
        	            var reqObject1 = {
        	            		"orgId": $localStorage.orgId
                        }
                    	loginService.getConfigData.post(reqObject1).$promise.then(function (response) {
                    		$localStorage.autoRefresh = response.autoRefresh;
                            $localStorage.autoRefreshFreq = response.autoRefreshFreq;
                    		$localStorage.centerLat = response.centerLat;
            	            $localStorage.centerLong = response.centerLong;
            	            $localStorage.mapZoom = response.mapZoom;
            	            $localStorage.mapType = response.mapType;
                    		$localStorage.helpdeskContactMessage = response.helpdeskContactMessage;
            				$localStorage.systemdemo = response.systemDemo;
                            $localStorage.clickToChat = response.clickToChat;
                            $localStorage.rtsURL = response.rtsURL;
            	            $rootScope.systemDemo = $localStorage.systemdemo;
            	            $rootScope.showResetOrg = ($localStorage.privilege > 0);
            	            $rootScope.clickToChat = $localStorage.clickToChat;
            	            $localStorage.fetchESData = response.fetchESData;
            	            $localStorage.newProfileScreen = response.newProfileScreen;
            	            $localStorage.alertDeviceLogSearch = response.alertDeviceLogSearch;
            	            if($rootScope.viewAlertState=="app.dashboard.alert"){
                                $state.go('app.dashboard.alert');
                           }else{
                       	     $state.go('app.dashboard');
                           }
                    	}, function (error) {
                            console.log(error);
                        });
                    } else {
                    	$scope.passwordMessage = null;
                        $scope.errorMessage = response.responseMessage;
                    }
                }, function (error) {
                    console.log(error);
                });
        	}
        	else {
            	$scope.passwordMessage = null;
                $scope.errorMessage = 'Unknown user or password';
        	}
        }, function (error) {
        	$scope.passwordMessage = null;
            $scope.errorMessage = 'Unknown user or password';
        });
    }
    $scope.logout = function () {
        $rootScope.userName = $localStorage.username;
        $rootScope.authToken = $localStorage.authToken;
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject = {
            "userLogin": $localStorage.username,
            "authToken": $localStorage.authToken,
            "dateTime": currentDate,
            "orgId": $localStorage.orgId
        }
        loginService.logout.post(reqObject).$promise.then(function (response) {
            /* $scope.successStatus = response.responseStatus;
             if ($scope.successStatus == "success") {*/
            $localStorage.$reset();
            $scope.passwordMessage = null;
            sharedDataService.setSuccessMessage(null);
            delete $rootScope.viewAlertState;
            $state.go('login');
            /*}*/
        }, function (error) {
            console.log(error);
        })
    }
    $scope.showHelpDialog = function (event) {
		$mdDialog.show({
     	parent: angular.element(document.querySelector('#rootContainer')),
     	clickOutsideToClose:true,
		targetEvent: event,
     	templateUrl: 'app/view/helpPopup.html',
     	controller: 'DialogController',
     	locals: {
     		helpdeskContactMessage: $localStorage.helpdeskContactMessage
        }
	  });
    }
    $scope.resetOrg = function (event) {
     	 var confirm = $mdDialog.confirm().title('Are you sure you want to reset this Organization?').ok('Yes').cancel('No');
         $mdDialog.show(confirm).then(function() {
             var reqObject = {
                 	"orgId": $localStorage.orgId
             }
             resetDataService.resetOrg.post(reqObject).$promise.then(function (response) {
                 if (response.responseStatus === "error" && response.responseCode === 403) {
                     $scope.$emit('genericErrorEvent', response);
                     return;
                 }
                 if (response.responseStatus === "success") {
                     $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title('Reset status!').textContent(response.responseMessage).ok('Close').targetEvent(event));
                 }
             }, function (error) {
                 console.log(error);
             })
         });   
    }
    $scope.resetData = function (event) {
        var reqObject = {
        	"orgId": $localStorage.orgId
        }
        resetDataService.resetData.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title('Reset status!').textContent(response.responseMessage).ok('Close').targetEvent(event));
            }
        }, function (error) {
            console.log(error);
        })
    }
    $scope.startSimulator = function (event) {
        var reqObject = {
        	"orgId": $localStorage.orgId
        }
        simulatorService.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title('Simulator status!').textContent(response.responseMessage).ok('Close').targetEvent(event));
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $scope.resetK4Data = function (event) {
        var reqObject = {
        	"orgId": $localStorage.orgId
        }
        resetDataService.resetK4Data.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title('Reset K4 status!').textContent(response.responseMessage).ok('Close').targetEvent(event));
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $scope.resetK13Data = function (event) {
        var reqObject = {
        	"orgId": $localStorage.orgId
        }
        resetDataService.resetK13Data.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title('Reset K1-3 status!').textContent(response.responseMessage).ok('Close').targetEvent(event));
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $scope.forgotPassword = function () {
        //$state.go('forgotPassword', {});
        $state.transitionTo('forgotPassword', null, {'reload':true});
    };
}