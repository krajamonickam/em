angular.module('rfxcelApp').controller('productController', productController);

productController.$inject = ['$scope', '$rootScope', '$state', 'activeShipmentsService', 'sharedDataService', '$filter', 'productFavoriteService', 'sessionService', 'mapConfigService', 'generalUtilityService', '$localStorage','productAddService','$mdDialog','locateNowService'];

function productController($scope, $rootScope, $state, activeShipmentsService, sharedDataService, $filter, productFavoriteService, sessionService, mapConfigService, generalUtilityService, $localStorage,productAddService,$mdDialog,locateNowService) {
    
	var loadLoadActiveShipments,
    refreshLoadActiveShipments;
	
	$scope.orgId=$localStorage.orgId;
	$scope.rtsItemId = "";
	$scope.rtsSerialNumber = "";
    $scope.shipmentTypeArr = sharedDataService.getItemTypeArr();
    $scope.selectedItemType = $scope.shipmentTypeArr[2];
    //sharedDataService.setSelectedShipmentType($scope.selectedItemType.typeId);
    var lastState = sharedDataService.getLastVisitedUIState();
    if(lastState=='app.productsDetails.map' || lastState=='app.productsDetails.data' || lastState=='app.productsDetails.alert' || lastState=='app.modifyproduct'){
        if(sharedDataService.getSelectedShipmentType()=='3'){
            loadShipment(2);    
        }else if(sharedDataService.getSelectedShipmentType()=='2'){
            loadShipment(1);        
        }else{
            loadShipment(1);       
        }
    }else{
        loadShipment( 1 );
    }
    function loadShipment(currentShipmentTypeId){
        loadActiveShipments(currentShipmentTypeId);
        $scope.selectedItemType = sharedDataService.getItemTypeArr().find(function(element) {
            return element.typeId == currentShipmentTypeId;
        });

        //$scope.selectedItemType.typeId = currentShipmentIndex;
    }
    
    $scope.isSelectedProductSortBy = true;
    $scope.shipmentSelectionChange = function (typeId) {
    	$scope.shipment = "";
    	$scope.noShipmentFound = "";
    	sharedDataService.setSelectedShipmentType(typeId);
    	$scope.selectedItemType.typeId = typeId;
    	loadActiveShipments(typeId);
    };
    
    $scope.loadActiveShipmentsSearch = function(selectedItemType){
    	$scope.noShipmentFound = "";
    	//sharedDataService.setSelectedShipmentType(selectedItemType.typeId);
    	//$scope.selectedItemType.typeId = selectedItemType.typeId;
        loadActiveShipments(selectedItemType.typeId);
    }
    
    function loadActiveShipments(typeId) {
    	if(typeId == 4 || typeId == 5 || typeId == 6){
			$scope.isRtsDataFetch = true;
		}else{
			$scope.isRtsDataFetch = false;
		}
        var date = new Date(),
            currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
            reqObject = {
        		"orgId": $localStorage.orgId,
            	"groupId": $localStorage.groupId,
        		"dateTime": currentDate,
                "searchType": typeId,
                "showMap" : false,
                "searchString":$scope.shipment,
                "rtsDataFetch" : $scope.isRtsDataFetch
            };
        activeShipmentsService.post(reqObject).$promise.then(function (response) {
             if (response.responseStatus === "error" && response.responseCode === 403) {
                 $scope.$emit('genericErrorEvent', response);
                 return;
             }
             if (response.responseStatus == "success") {
            	 $scope.shipmentLocationData = response.sensor.shipmentList;
            	 if($scope.shipmentLocationData.length === 0){
            		 if($localStorage.fetchESData === true){
                 		if(response.responseMessage !== undefined){
                 			$scope.noShipmentFound=response.responseMessage;
						}else{
							if($scope.selectedItemType.typeId=='1'){
								$scope.noShipmentFound='No Current Shipments available';
					        }else if($scope.selectedItemType.typeId=='2'){
					        	$scope.noShipmentFound='No Pending Shipments available';      
					        }else{
					        	$scope.noShipmentFound='No Completed Shipments available';      
					        }
						}
                 	 }else{
						if($scope.selectedItemType.typeId=='1'){
							$scope.noShipmentFound='No Current Shipments available';
				        }else if($scope.selectedItemType.typeId=='2'){
				        	$scope.noShipmentFound='No Pending Shipments available';      
				        }else{
				        	$scope.noShipmentFound='No Completed Shipments available';    
				        }
					}
            	 }
                 $scope.updatedTimeArr = [];
                 $scope.createdDate = [];
                 $scope.createdTime = [];

                 for (var i = 0; i < response.sensor.shipmentList.length; i++) {
                     if (response.sensor.shipmentList.length > 0) {
						 if (response.sensor.shipmentList[i].rtsItemId !== undefined ) {
							 $scope.rtsItemId = response.sensor.shipmentList[i].rtsSerialNumber;
						 }
						 
						 if (response.sensor.shipmentList[i].rtsSerialNumber !== undefined ) {
							 $scope.rtsSerialNumber = response.sensor.shipmentList[i].rtsSerialNumber;
						 }
						 
						 if (response.sensor.shipmentList[i].createdOn !== undefined ) {
                         createdOnArr = response.sensor.shipmentList[i].createdOn.split(' ');
                         $scope.createdDate[i] = createdOnArr[0];
                         $scope.createdTime[i] = createdOnArr[1];
						 }
                         if (typeId !== 2) { // Don't display updated date time for pending shipments (pending shipment typeId is 2)
                        	 var updatedTime=generalUtilityService.getLastUpdatedTime(response.sensor.shipmentList[i].updatedOn);
                        	 if(updatedTime!=="" && updatedTime!=null){
                                $scope.updatedTimeArr[i] = 'updated ' + updatedTime;    
                             }
                         }
                     }
                 }
             } else {
                 $scope.message = response.responseMessage;
             }
        }, function (error) {
            console.log(error);
        });
    }
    
    $scope.onProductSelect = function (selectedProduct){
        var colorConversionArr = mapConfigService.getMarkerColorCode();
        sharedDataService.setSelectedDeviceMarkerColor(colorConversionArr[selectedProduct.pairStatus]);
        sharedDataService.setSelectedDeviceId(selectedProduct.deviceId);
        sharedDataService.setSelectedShipmentId(selectedProduct.packageId);
        sharedDataService.setSelectedShipment(selectedProduct);
        sharedDataService.setSelectedProductId(selectedProduct.productId);
        sharedDataService.setSelectedFavoriteOption(selectedProduct.isfavorite);
        sharedDataService.setSelectedShipmentType($scope.selectedItemType.typeId);
        sharedDataService.setRTSTraceId(selectedProduct.rtsItemId);
        sharedDataService.setRTSSerialNumber(selectedProduct.rtsSerialNumber);
        sharedDataService.setShowMapAttribute(true);
        $state.go('app.productsDetails');
    }
    
    $scope.onFavoriteSelect = function(selectedProduct,favorite){
    	$scope.updatedTimeArr = [];
        $scope.createdDate = [];
        $scope.createdTime = [];
    	var date = new Date();
  	    var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
    	var reqObject = {
            "orgId": $localStorage.orgId,
            "groupId": $localStorage.groupId,
    		"dateTime": currentDate,
    		"user"  : $localStorage.username,
    		"action" : favorite,
    		"deviceData": {
    			"deviceId": selectedProduct.deviceId,
    			"packageId": selectedProduct.packageId,
    			"productId": selectedProduct.productId
    		}
    	}
    	productFavoriteService.getfavoriteService.post(reqObject).$promise.then(function(response){
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
    		if(response.responseStatus == "success"){
    			loadActiveShipments($scope.selectedItemType.typeId);
    		}else{
    			console.log(error);
    		}
        }, function(error){
        	console.log(error);
        })
    }
    $scope.modifyProduct=function(modifyObj){
    	sharedDataService.setSelectedShipmentId(modifyObj.packageId);
    	sharedDataService.setSelectedShipmentType($scope.selectedItemType.typeId);
        sharedDataService.setSelectedDeviceId(modifyObj.deviceId);
        sharedDataService.setRTSTraceId(modifyObj.rtsItemId);
        sharedDataService.setRTSSerialNumber(modifyObj.rtsSerialNumber);
        $state.go('app.modifyproduct');
    }
    
    $scope.locateNow=function(deviceId){
    	  var alert;
    	  var reqObj = {
    		  "orgId": $localStorage.orgId,
    	      "userId"  : $localStorage.userId,
              "deviceDetail": {
                  "deviceId": deviceId
              }
          }
    	  locateNowService.setLocationNow().save(JSON.stringify(reqObj)).$promise.then(function (response) {
              if (response.responseStatus === "error" && response.responseCode === 403) {
                  $scope.$emit('genericErrorEvent', response);
                  return;
              }
              if (response.responseStatus == "success") {
            	  alert = $mdDialog.alert({
                      title:  response.responseMessage,
                      ok: 'Close'
                    });
                    $mdDialog
                      .show(alert )
                      .finally(function() {
                      });	
            	} else {
            		alert = $mdDialog.alert({
                        title:  response.responseMessage,
                        ok: 'Close'
                      });
                      $mdDialog
                        .show(alert )
                        .finally(function() {
                        });
              }       
  			}, function (error) {
  			 console.log(error);
          });
      
    }
    
    $scope.disassociate = function (deviceId, packageId, productId) {
      	 var confirm = $mdDialog.confirm().title('Are you sure you want to disassociate?').ok('Yes').cancel('No');
           $mdDialog.show(confirm).then(function() {
          	 var date = new Date();
          	 var alert;
               var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
               var reqObject = {
                   "dateTime": currentDate,
                   "orgId": $localStorage.orgId,
                   "deviceData": {
                       "deviceId": deviceId,
                       "packageId": packageId,
                       "productId":productId
                   }
               };
               productAddService.disassociate.post(reqObject).$promise.then(function (response) {
                   if (response.responseStatus === "error" && response.responseCode === 403) {
                       $scope.$emit('genericErrorEvent', response);
                       return;
                   }
                   if (response.responseStatus == "success") {
                	   	alert = $mdDialog.alert({
                           title:  response.responseMessage,
                           ok: 'Close'
                         });
                         $mdDialog
                           .show(alert)
                           .finally(function() {
                        	   $state.reload('app.products');
                           });	
                	   var confirm = $mdDialog.confirm().title().ok('Ok');
                    } else {
                    	alert = $mdDialog.alert({
                            title:  response.responseMessage,
                            ok: 'Close'
                          });
                          $mdDialog
                            .show(alert)
                            .finally(function() {
                         	   $state.reload('app.products');
                            });	
                 	   var confirm = $mdDialog.confirm().title().ok('Ok');
                       
                   }
               }, function (error) {
            	   console.log(error);
               });
           });   
      }
    
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
   	 if (fromState.name === 'app.products') {
            clearTimeout(refreshLoadActiveShipments);
        }
    });
    
    loadLoadActiveShipments = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '300000';
    console.log ($scope.selectedItemType);
    refreshLoadActiveShipments = setInterval(function(){loadActiveShipments($scope.selectedItemType.typeId); }, loadLoadActiveShipments);
}