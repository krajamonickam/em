angular.module('rfxcelApp').controller('productMapController', productMapController);
productMapController.$inject = ['$scope', '$rootScope', '$filter', 'mapDataService', 'productAttributeService', 'sharedDataService', '$state', '$http', 'mapConfigService', 'googleAPIService', '$localStorage', 'commonConfigService', 'generalUtilityService', 'productAlertService', '$window'];

function productMapController($scope, $rootScope, $filter, mapDataService, productAttributeService, sharedDataService, $state, $http, mapConfigService, googleAPIService, $localStorage, commonConfigService, generalUtilityService, productAlertService, $window) {
	/*if(sharedDataService.getSelectedDeviceId() === undefined || sharedDataService.getSelectedShipmentId() === undefined || sharedDataService.getSelectedProductId() === undefined){*/
	if(sharedDataService.getSelectedShipmentId() === undefined || sharedDataService.getSelectedProductId() === undefined){
		var lastState = sharedDataService.getLastVisitedUIState();
        sharedDataService.setLastVisitedUIState($state.current.name);
        $state.go('app.dashboard');
        return;
    }
	$scope.mapInfo = false;

    $scope.onViewDetailsClick = function(id, typeId, traceEntId){
    	traceEntId = sharedDataService.getTraceId();
    	var deviceId = sharedDataService.getSelectedDeviceId();
    	var loadState,
        stateParam;
        console.log(id)
        console.log(typeId)
        console.log(traceEntId)
    	if(typeId === 4){
    		loadState = 'app.traceEventDetails';
    		stateParam = {
    				eventId: id,
    				deviceId:deviceId
    		}
    	}else if(typeId === 5){
    		loadState = 'app.traceItemDetails';
    		stateParam = {
    				traceId: traceEntId,
    				deviceId:deviceId
    		}
    	}else if(typeId === 1 || typeId === 2 || typeId === 3){
    		loadState = 'app.traceItemDetails';
    		stateParam = {
    				traceId: traceEntId,
    				deviceId:deviceId
    		}
    	}
    		
		var url = $state.href(loadState, stateParam),
			height = $window.innerHeight * .85,
   			width = $window.innerWidth * .90;
		$window.open(url, "", "width="+width+",height="+height+",left=60,top=50,scrollbars=1,resizable=1");
	};
    
    console.log($scope.selectedItemType);
    
    $scope.selectedItemType = sharedDataService.getItemTypeArr().find(function(element) {
        return element.typeId == sharedDataService.getSelectedShipmentType();
    });

    //$scope.selectedItemType = sharedDataService.getItemTypeArr()[sharedDataService.getSelectedShipmentType()-1];
    
    console.log($scope.selectedItemType);
	// latLngPath: Array stores the last and next datapoint to plot the flightpath between the two points
    var refreshAddMapPathMarker, map, latLngPath = [],
        markerColorArr = mapConfigService.getMarkerColorCode(),
        maxZoomVal = $localStorage.mapZoom,
        mapParams = {
	    		mapTypeId: google.maps.MapTypeId.HYBRID
    	},
    	map = new google.maps.Map(document.getElementById("map-3"), mapParams);
    
    var loadSelectedShipmentCountInterval,
    refreshSelectedShipmentCountInterval;
    var popup;

    var locationCoordinates;
    function drawFlightPath(map, pairStatus) {
        var flightPath = new google.maps.Polyline({
            path: latLngPath,
            strokeColor: markerColorArr[pairStatus],
            strokeOpacity: 1,
            strokeWeight: 5,
            icons: [{
                repeat: '100px'
            }]
        });
        flightPath.setMap(map);
        // get the new datapoint 
        var lastLatLng = latLngPath.slice(-1)[0];
        latLngPath = [];
        latLngPath.push(lastLatLng);
    }

    function getNearestLocation(lat, lng) {
        var reqObj = googleAPIService.getNearestLocation(lat, lng);
        reqObj.then(function (response) {
			$scope.nearestLocation = (response.data.status !== "ZERO_RESULTS") ? response.data.results[0].formatted_address : 'N.A';
        }, function (error) {
            console.log(error);
        });
    }
    // Get product attributes
    function getProductAttributes(event, i, isLastDatapoint) {
        var date = new Date(),
            currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
            reqObject = {
                "dateTime": currentDate,
				"orgId": $localStorage.orgId,
                "deviceData": {
                    "packageId": sharedDataService.selectedShipment().packageId,
                    "deviceId": sharedDataService.selectedShipment().deviceId,
                    "id": $scope.mapData.locations[i].id,
                    "productId": sharedDataService.getSelectedProductId(),
                    "showMapAttribute": sharedDataService.getShowMapAttribute(),
                    "rtsItemId": sharedDataService.getRTSTraceId(),
                    "rtsSerialNumber": sharedDataService.getRTSSerialNumber()
                }
            };
		// june1
		console.log($scope.selectedItemType);
		console.log($scope.mapData);
		if ($scope.selectedItemType.typeId === 6 || $scope.selectedItemType.typeId === 1) {
        productAttributeService.post(JSON.stringify(reqObject)).$promise.then(function (response) {
        	//alert('mappath1');

            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.mapInfo = true;
            	sharedDataService.setTraceId(response.traceId);
            	sharedDataService.setSelectedDeviceId(response.deviceId);
            	
            	if(response.sensor.statusTime!= null){
					$scope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime(response.sensor.statusTime);
            	}else{
            		$scope.productLastUpdateTime = 'N.A';
            	}
            	
				
				
            	if(response.sensor.attributeList[0] !== undefined) {
            		//$scope.isColorRedAttr1 = (response.sensor.attributeList[0].excursion) ? true : false;
            		var minValue = response.sensor.attributeList[0].min;
            		var maxValue = response.sensor.attributeList[0].max;
            		var rangeValue = response.sensor.attributeList[0].attrValue;
            		var alertNotify = response.sensor.attributeList[0].alert;
            		if(alertNotify === false){
            			$scope.isColorRedAttr1 = true;
            		} else {
            			if(minValue !== undefined && maxValue !== undefined){
                			$scope.isColorRedAttr1 = (rangeValue >= minValue && rangeValue <= maxValue);
                		} else {
                			$scope.isColorRedAttr1 = true;
                		}
            		}
    				$scope.productAttrLegend1 = response.sensor.attributeList[0].legend;
    				
    				if (response.sensor.attributeList[0].attrValue === commonConfigService.getNullAttrResponseCode()) {
    					$scope.productAttrVal1 = 'N.A';
    					$scope.productAttrUnit1 = '';
    					$scope.isColorRedAttr1 = true;
    				} else {
    					$scope.productAttrVal1 = response.sensor.attributeList[0].attrValue;
    					$scope.productAttrUnit1 = response.sensor.attributeList[0].unit
    				}
    			}

    			if(response.sensor.attributeList[1] !== undefined) {
    				//$scope.isColorRedAttr2 = (response.sensor.attributeList[1].excursion) ? true : false;
    				var minValue = response.sensor.attributeList[1].min;
            		var maxValue = response.sensor.attributeList[1].max;
            		var rangeValue = response.sensor.attributeList[1].attrValue;
            		var alertNotify = response.sensor.attributeList[1].alert;
            		if(alertNotify === false) {
            			$scope.isColorRedAttr2 = true;
            		} else {
            			if(minValue !== undefined && maxValue !== undefined){
                			$scope.isColorRedAttr2 = (rangeValue >= minValue && rangeValue <= maxValue);
                		}else{
                			$scope.isColorRedAttr2 = true;
                		}
            		}
    				$scope.productAttrLegend2 = response.sensor.attributeList[1].legend;
    				
    				if (response.sensor.attributeList[1].attrValue === commonConfigService.getNullAttrResponseCode()) {
    					$scope.productAttrVal2 = 'N.A';
    					$scope.productAttrUnit2 = '';
    					$scope.isColorRedAttr2 = true;
    				} else {
    					$scope.productAttrVal2 = response.sensor.attributeList[1].attrValue;
    					$scope.productAttrUnit2 = response.sensor.attributeList[1].unit;
    				}
    			}
            }

            /*$scope.isColorRedAttr1 = (response.sensor.attributeList[0].excursion) ? true : false;
            $scope.isColorRedAttr2 = (response.sensor.attributeList[1].excursion) ? true : false;

            if (response.sensor.attributeList[0].attrValue === commonConfigService.getNullAttrResponseCode()) {
                $scope.productAttrVal1 = 'N.A';
                $scope.productAttrUnit1 = '';
            } else {
                $scope.productAttrVal1 = response.sensor.attributeList[0].attrValue;
                $scope.productAttrUnit1 = response.sensor.attributeList[0].unit
            }
            if (response.sensor.attributeList[1].attrValue === commonConfigService.getNullAttrResponseCode()) {
                $scope.productAttrVal2 = 'N.A';
                $scope.productAttrUnit2 = '';
            } else {
                $scope.productAttrVal2 = response.sensor.attributeList[1].attrValue;
                $scope.productAttrUnit2 = response.sensor.attributeList[1].unit;
            }
            $scope.productAttrLegend1 = response.sensor.attributeList[0].legend;
            $scope.productAttrLegend2 = response.sensor.attributeList[1].legend;*/
        }, function (error) {
            console.log(error);
        });
		} else {
			$scope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime($scope.mapData.locations[i].dateTime);
		} // if $scope.selectedItemType === 6
        if (!isLastDatapoint) {
            $scope.productLocationLat = parseFloat(event.latLng.lat()).toFixed(5);
            $scope.productLocationLng = parseFloat(event.latLng.lng()).toFixed(5);
            locationCoordinates = $scope.productLocationLat + ', ' + $scope.productLocationLng; 
            $scope.productLocationCoordinates = (event.latLng.lat() === undefined) ? 'N.A' : locationCoordinates;
        }
        //$scope.nearestLocation = $scope.mapData.locations[i].address; // nearestLocation from API
        getNearestLocation($scope.productLocationLat, $scope.productLocationLng);
    }
    /*draws the map for selected device*/
    function drawMap(isLatLongAvailable) {
        var markers = [],
            markersHalo = [],
            lastIndex = $scope.mapData.locations.length - 1;
            var center;
        if(isLatLongAvailable===false){
                center= new google.maps.LatLng($localStorage.centerLat, $localStorage.centerLong);
                map.setZoom(6);
                map.setCenter(center);
                console.log('current zoom level for map : ' +  map.getZoom());
                return;
        }
        center= new google.maps.LatLng($scope.mapData.locations[lastIndex].lat, $scope.mapData.locations[lastIndex].lang);
        myTrip = [];
        var markerBounds = new google.maps.LatLngBounds();
        $scope.productLocation = $scope.mapData.locations[0].lat + ',' + $scope.mapData.locations[0].lang;
        var latLngFirstPoint = new google.maps.LatLng($scope.mapData.locations[0].lat, $scope.mapData.locations[0].lang);
        latLngPath.push(latLngFirstPoint);
        // ***********  draw path for all the points - START *************
        for (var i = 0; i < $scope.mapData.locations.length; i++) {
            var latLng = new google.maps.LatLng($scope.mapData.locations[i].lat, $scope.mapData.locations[i].lang);
            myTrip.push(latLng);
            // plot all points
            plotMarker(latLng, i, 7, 1, $scope.mapData.locations[i].pairStatus); // latLng, lastIndex, scale, fillOpacity
            // plot opaque datapoints over the last datapoint
            if (i === lastIndex) {
                // latLng, lastIndex, scale, fillOpacity, pairStatus
                plotHaloMarker(latLng, lastIndex, $scope.mapData.locations[lastIndex].pairStatus);
                if (generalUtilityService.validateLatLong($scope.mapData.locations[i].lat,$scope.mapData.locations[i].lang)){
                	markerBounds.extend(latLng);
                }
            }
            if (i > 0) { // Flightpath must be drawn only if more than one datapoint
                latLngPath.push(latLng);
                drawFlightPath(map, $scope.mapData.locations[i - 1].pairStatus);
            }
            
            if (generalUtilityService.validateLatLong($scope.mapData.locations[i].lat,$scope.mapData.locations[i].lang)){
            	markerBounds.extend(latLng);
            }
        }
        
        map.fitBounds(markerBounds); //Displays all Points and set a zoom level
        console.log('Fit bound map : ' +  map.getZoom());
        if (map.getZoom() > maxZoomVal) map.setZoom(maxZoomVal);
        var listener = google.maps.event.addListener(map, "idle", function() { 
          console.log('current zoom level for map listenerer : ' +  map.getZoom());
          google.maps.event.removeListener(listener); 
        });
        
        
        // ***********  draw path for all the points - END *************
        $scope.productLocationLat = parseFloat($scope.mapData.locations[lastIndex].lat).toFixed(5);
        $scope.productLocationLng = parseFloat($scope.mapData.locations[lastIndex].lang).toFixed(5);
        locationCoordinates = $scope.productLocationLat + ', ' + $scope.productLocationLng;
        $scope.productLocationCoordinates = ($scope.mapData.locations[lastIndex].lat === undefined) ? 'N.A' : locationCoordinates;

        // call this after specific interval
        addMapPathMarkerInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
        refreshAddMapPathMarker = setInterval(addMapPathMarker, addMapPathMarkerInterval);
        
        definePopupClass();

        function plotHaloMarker(latLng, lastDataPointIndex, pairStatus) {
            // remove previous halo marker
            if (markersHalo.length > 0) {
                markersHalo[0].setMap(null);
                markersHalo = [];
            }
            var scale = 20,
                fillOpacity = 0.3,
                markerHalo = new google.maps.Marker({
                    position: latLng,
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: scale,
                        fillColor: markerColorArr[pairStatus],
                        fillOpacity: fillOpacity,
                        strokeWeight: 0.2
                    },
                    draggable: false,
                    map: map
                });
            markerHalo.setMap(map);
            markersHalo.push(markerHalo);
        }

        function plotMarker(latLng, lastDataPointIndex, scale, fillOpacity, pairStatus) {
            var marker = new google.maps.Marker({
                position: latLng,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: scale,
                    fillColor: markerColorArr[pairStatus],
                    fillOpacity: fillOpacity,
                    strokeWeight: 0.2
                },
                draggable: false,
                map: map
            });
            marker.setMap(map);
            markers.push(marker);
            var addListener = function (lastDataPointIndex) {
                google.maps.event.addListener(marker, 'mouseover', function (event) {
                    // GET Attributes for the selected data point
                    getProductAttributes(event, lastDataPointIndex);
                    //lastClickedMarker = marker;
                    plotHaloMarker(latLng, lastDataPointIndex, $scope.mapData.locations[lastDataPointIndex].pairStatus);
                });
            };
            addListener(lastDataPointIndex);
        }
        /*  Check if new datapoints are added. 
         *  If yes, then plot them on the map and draw the path.
         */
        function addMapPathMarker() {
        	if($scope.selectedItemType.typeId == 4 || $scope.selectedItemType.typeId == 5 || $scope.selectedItemType.typeId == 6){
    			$scope.isRtsDataFetch = true;
    		}else{
    			$scope.isRtsDataFetch = false;
    		}
            var date = new Date(),
                currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
                reqObject = {
                    "dateTime": currentDate,
					"orgId": $localStorage.orgId,
                    "deviceData": {
                        "packageId": sharedDataService.selectedShipment().packageId,
                        "deviceId": sharedDataService.selectedShipment().deviceId,
                        "startDate": $scope.mapMarkerPathLastTimeStamp,
                        "productId": sharedDataService.getSelectedProductId()
                    },
                    "searchType": $scope.selectedItemType.typeId,
                    "rtsDataFetch" : $scope.isRtsDataFetch
                };
            // Service to get the new data points
            mapDataService.post(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                var mapData = response.sensor.map,
                    loopCount = 0;
                // Execute this only if new datapoints are fetched   
                if (mapData.locations.length > 0) {
                    // lastTimeStamp to be used for next API request
                    $scope.mapMarkerPathLastTimeStamp = mapData.lastTimeStamp;
                    // plot new datapoint 
                    var lastIndex = myTrip.length - 1,
                        lastVal = myTrip[lastIndex];
                    myTrip = [];
                    myTrip.push(lastVal);
                    loopCount = $scope.mapData.locations.length + mapData.locations.length;
                    for (var i = $scope.mapData.locations.length, newLocationCount = 0; i < loopCount; i++, newLocationCount++) {
                        // push the new datapoint in $scope.mapData.locations  
                        $scope.mapData.locations.push(mapData.locations[newLocationCount]);
                        var latLng = new google.maps.LatLng(mapData.locations[newLocationCount].lat, mapData.locations[newLocationCount].lang);
                        myTrip.push(latLng);
                        // push the new datapoint in the array that contains the last datapoint
                        latLngPath.push(latLng);
                        // draw flight between the existing last datapoint and the new datapoint
                        drawFlightPath(map, $scope.mapData.locations[i - 1].pairStatus);
                    }
                    // remove old markers
                    if (markers.length > 0) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(null);
                        }
                        markers = [];
                    }
                    for (var j = 0; j < $scope.mapData.locations.length; j++) {
                        // push the new datapoint in $scope.mapData.locations  
                        var latLng = new google.maps.LatLng($scope.mapData.locations[j].lat, $scope.mapData.locations[j].lang);
                        plotMarker(latLng, j, 6, 1, $scope.mapData.locations[j].pairStatus);
                        if (j === $scope.mapData.locations.length - 1) {
                            // params: event, count, ifLastIndex
                            getProductAttributes({}, j, true) // update bottom panel to show latest datapoint attributes
                                // latLng, lastIndex, scale, pairStatus
                            plotHaloMarker(latLng, lastIndex, $scope.mapData.locations[j].pairStatus);
                            map.setCenter(latLng); // re-center the map to show the latest datapoint at center
                            $scope.productLocationLat = parseFloat($scope.mapData.locations[j].lat).toFixed(5);
                            $scope.productLocationLng = parseFloat($scope.mapData.locations[j].lang).toFixed(5);
                            locationCoordinates = $scope.productLocationLat + ', ' + $scope.productLocationLng; 
                            $scope.productLocationCoordinates = ($scope.mapData.locations[j].lat === undefined) ? 'N.A' : locationCoordinates;
                        }
                    }
                } else {
                    console.log('no map data available');
                }
            }, function (error) {
                console.log(error);
            });
        } // addMapPathMarker ends here
    } // drawMap ends here
    /*fetch map datapoint for device*/
    function loadMapDataforDevice() {
        console.log(sharedDataService.selectedShipment());
    	if($scope.selectedItemType.typeId == 4 || $scope.selectedItemType.typeId == 5){
			$scope.isRtsDataFetch = true;
		}else{
			$scope.isRtsDataFetch = false;
		}
        $scope.$emit('selectedProductPackageId', sharedDataService.selectedShipment().packageId);
        $scope.$emit('selectedProductDeviceId', sharedDataService.selectedShipment().deviceId);
        var date = new Date(),
            currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
            reqObject = {
                "dateTime": currentDate,
				"orgId": $localStorage.orgId,
                "deviceData": {
                    "packageId": sharedDataService.selectedShipment().packageId,
                    "deviceId": sharedDataService.selectedShipment().deviceId,
                    "productId": sharedDataService.getSelectedProductId()
                },
                "searchType": $scope.selectedItemType.typeId,
                "rtsDataFetch" : $scope.isRtsDataFetch,
            };
            if ( $scope.selectedItemType.typeId == 5){
                reqObject.deviceData.deviceId = sharedDataService.selectedShipment().traceId
            }
        // getMapData
        mapDataService.post(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.mapData = response.sensor.map;
            	$rootScope.mapData3Locations = response.sensor.map.locations; // june1
                if ($scope.mapData.locations.length > 0) {
                    var lastIndex = $scope.mapData.locations.length - 1;
                    // fetch the new datapoint based on the date time of last datapoint
                    $scope.mapMarkerPathLastTimeStamp = $scope.mapData.lastTimeStamp;
                    //$scope.nearestLocation = $scope.mapData.locations[lastIndex].address; // nearestLocation from API
                    getNearestLocation($scope.mapData.locations[lastIndex].lat, $scope.mapData.locations[lastIndex].lang);
                    drawMap(true);
                } else {
                    drawMap(false);
                    console.log('no map data available');
                }
            }else {
                $scope.message = response.responseMessage;
            }
            
			intialBottomPanel(); // june1
        }, function (error) {
            console.log(error);
        });
    } // function loadMapDataforDevice ENDS
    $rootScope.isShowProductAlertsBtn = true;
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.productsDetails.map') {
            clearTimeout(refreshAddMapPathMarker);
            $rootScope.isShowProductAlertsBtn = false; // hide the AlertsBtn for other states 
        }
    });
    $scope.selectedPointFavoriteStatus = sharedDataService.getSelectedFavoriteOption();
    loadMapDataforDevice();
    /***************** Bottom Info Panel ****************/
    function onMapDataPointSelected(event, location, isLastDataPoint) {
        var productLocationArr = location.split(',');
        $scope.productLocationLat = parseFloat(productLocationArr[0]).toFixed(5);
        $scope.productLocationLng = parseFloat(productLocationArr[1]).toFixed(5);
        getNearestLocation($scope.productLocationLat, $scope.productLocationLng);
        if (isLastDataPoint === undefined) {
            $scope.$apply();
        }
    }

    function onSelectedProductPackageId(event, packageId) {
        $scope.packageId = packageId;
    }

    function onSelectedProductDeviceId(event, deviceId) {
        $scope.deviceId = deviceId;
    }
    $scope.$on('mapDataPointSelected', onMapDataPointSelected);
    $scope.$on('selectedProductPackageId', onSelectedProductPackageId);
    $scope.$on('selectedProductDeviceId', onSelectedProductDeviceId);
    $scope.packageId = sharedDataService.selectedShipment().packageId;
    $scope.deviceId = sharedDataService.selectedShipment().deviceId;
    $scope.productId = sharedDataService.getSelectedProductId();
    $scope.traceId = sharedDataService.getTraceId();
    //$scope.selectedPointPairStatus = sharedDataService.selectedShipment().pairStatus;
    var date = new Date(),
        currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
        reqObject = {
            "dateTime": currentDate,
			"orgId": $localStorage.orgId,
            "deviceData": {
                "packageId": sharedDataService.selectedShipment().packageId,
                "deviceId": sharedDataService.selectedShipment().deviceId,
                "productId": sharedDataService.getSelectedProductId(),
                "showMapAttribute": sharedDataService.getShowMapAttribute(),
                "rtsItemId": sharedDataService.getRTSTraceId(),
                "rtsSerialNumber": sharedDataService.getRTSSerialNumber()
            }
        };
	function intialBottomPanel() {
		// june1
		console.log($scope.selectedItemType);
		console.log($scope.mapData);
		if ($scope.selectedItemType.typeId === 6 || $scope.selectedItemType.typeId === 1 ) {
    productAttributeService.post(JSON.stringify(reqObject)).$promise.then(function (response) {

        if (response.responseStatus === "error" && response.responseCode === 403) {
            $scope.$emit('genericErrorEvent', response);
            return;
        }

        /*$scope.isColorRedAttr1 = (response.sensor.attributeList[0].excursion) ? true : false;
        $scope.isColorRedAttr2 = (response.sensor.attributeList[1].excursion) ? true : false;*/
        if (response.responseStatus == "success") {
        	if(response.sensor.statusTime!= null){
				$scope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime(response.sensor.statusTime);
        	}else{
        		$scope.productLastUpdateTime = 'N.A';
        	}
			
        	if(response.sensor.attributeList[0] !== undefined) {
        		//$scope.isColorRedAttr1 = (response.sensor.attributeList[0].excursion) ? true : false;
        		var minValue = response.sensor.attributeList[0].min;
        		var maxValue = response.sensor.attributeList[0].max;
        		var rangeValue = response.sensor.attributeList[0].attrValue;
        		var alertNotify = response.sensor.attributeList[0].alert;
        		if(alertNotify === false){
        			$scope.isColorRedAttr1 = true;
        		} else {
        			if(minValue !== undefined && maxValue !== undefined){
            			$scope.isColorRedAttr1 = (rangeValue >= minValue && rangeValue <= maxValue);
            		} else {
            			$scope.isColorRedAttr1 = true;
            		}
        		}
				$scope.productAttrLegend1 = response.sensor.attributeList[0].legend;
				
    			if (response.sensor.attributeList[0].attrValue === commonConfigService.getNullAttrResponseCode()) {
    				$scope.productAttrVal1 = 'N.A';
    				$scope.productAttrUnit1 = '';
    				$scope.isColorRedAttr1 = true;
    			} else {
    				$scope.productAttrVal1 = response.sensor.attributeList[0].attrValue;
    				$scope.productAttrUnit1 = response.sensor.attributeList[0].unit
    			}
    		}
    		
    		if(response.sensor.attributeList[1] !== undefined) {
    			//$scope.isColorRedAttr2 = (response.sensor.attributeList[1].excursion) ? true : false;
				var minValue = response.sensor.attributeList[1].min;
        		var maxValue = response.sensor.attributeList[1].max;
        		var rangeValue = response.sensor.attributeList[1].attrValue;
        		var alertNotify = response.sensor.attributeList[1].alert;
        		if(alertNotify === false) {
        			$scope.isColorRedAttr2 = true;
        		} else {
        			if(minValue !== undefined && maxValue !== undefined){
            			$scope.isColorRedAttr2 = (rangeValue >= minValue && rangeValue <= maxValue);
            		}else{
            			$scope.isColorRedAttr2 = true;
            		}
        		}
				$scope.productAttrLegend2 = response.sensor.attributeList[1].legend;
				
    			if (response.sensor.attributeList[1].attrValue === commonConfigService.getNullAttrResponseCode()) {
    				$scope.productAttrVal2 = 'N.A';
    				$scope.productAttrUnit2 = '';
    				$scope.isColorRedAttr2 = true;
    			} else {
    				$scope.productAttrVal2 = response.sensor.attributeList[1].attrValue;
    				$scope.productAttrUnit2 = response.sensor.attributeList[1].unit;
    			}
    		}
        }
		
			}, function (error) {
				console.log(error);
			});
        } else {
			var lastLocationIndex = $rootScope.mapData3Locations.length -1;
			var lastLocationTime3 = $rootScope.mapData3Locations[lastLocationIndex].dateTime;
			$scope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime(lastLocationTime3);
        }
        }
		
    
    getAlertsCount();
    function getAlertsCount() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqCountObject = {
            "dateTime": currentDate,
            "searchType": "container",
			"orgId": $localStorage.orgId,
            "deviceData": {
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "packageId": sharedDataService.getSelectedShipmentId(),
                "productId": sharedDataService.getSelectedProductId()
            }
        }
        productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$rootScope.notificationCount = response.count;
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.productsDetails.map') {
            clearTimeout(refreshSelectedShipmentCountInterval);
        }
    });
    
    loadSelectedShipmentCountInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
    refreshSelectedShipmentCountInterval = setInterval(getAlertsCount, loadSelectedShipmentCountInterval);
    var template = document.getElementById('map-3-popup');
	var html = "";
	var showPopup = true;
    
    function getMonitorAlerts() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqCountObject = {
            "dateTime": currentDate,
            "searchType": "container",
			"orgId": $localStorage.orgId,
            "deviceData": {
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "packageId": sharedDataService.getSelectedShipmentId(),
                "productId": sharedDataService.getSelectedProductId()
            }
        }
        productAlertService.getMonitorAlerts.post(reqCountObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	if (popup == null) {
            		if (response.monitorAlerts.alertsFinished) {
            			return;
            		}
                    var lastIndex = $scope.mapData.locations.length-1;
                    popup = new Popup(
                    	      new google.maps.LatLng($scope.mapData.locations[lastIndex].lat, $scope.mapData.locations[lastIndex].lang),
                    	      template);
                	popup.doHide();
                    popup.setMap(map);
                	popup.doShow(response.monitorAlerts.alertsMsg, response.monitorAlerts.alertsIndex);
            	}
            	else {
            		popup.doShow(response.monitorAlerts.alertsMsg, response.monitorAlerts.alertsIndex);
            		if (response.monitorAlerts.alertsFinished) {
                    	popup.doHide();
                    	popup.setMap(null);
                    	popup = null;
                    	return;
            		}
            	}
            }
        }, function (error) {
            console.log(error);
        })
    }
    refreshSelectedShipmentCountInterval = setInterval(getMonitorAlerts, loadSelectedShipmentCountInterval);

    $scope.onViewDataClick = function (deviceId, packageId) {
        sharedDataService.setSelectedDeviceId(deviceId);
        sharedDataService.setSelectedShipmentId(packageId);
        $state.go('app.productsDetails.data');
    };
    $scope.onViewAlertClick = function (deviceId, packageId) {
        sharedDataService.setSelectedDeviceId(deviceId);
        sharedDataService.setSelectedShipmentId(packageId);
        $state.go('app.productsDetails.alert');
    };
    
    function definePopupClass() {
    	  Popup = function(position, content) {
    	    this.position = position;
    	    this.content = content;
    	    content.classList.add('popup-bubble-content');
    	    
    	    var pixelOffset = document.createElement('div');
    	    pixelOffset.classList.add('popup-bubble-anchor');
    	    pixelOffset.appendChild(content);

    	    this.anchor = document.createElement('div');
    	    this.anchor.classList.add('popup-tip-anchor');
    	    this.anchor.appendChild(pixelOffset);

    	    this.stopEventPropagation();
    	  };
    	  Popup.prototype = Object.create(google.maps.OverlayView.prototype);

    	  Popup.prototype.doShow = function(msg, index) {
          	this.content.style.display = 'block';
        	var c = this.content.childNodes;
          	var count = 0;
          	var msgText = "";
          	if (index < msg.length) {
          		msgText = msg[index];
          	}
          	for (var i = 0; i < c.length; i++) {
	       		if ((c[i].tagName != undefined) && c[i].tagName == 'DIV') {
	       			c[i].style.display = 'block';
	       			var msgText = "";
					if (count < msg.length) {
						msgText = msg[count];
					}
	       			if (msgText != "") {
	       				c[i].innerText = msgText;
	       			}
	       			if (count++ > index) {
	       				break;
	       			};
	       		}
          	}
      	  };

    	  Popup.prototype.doHide = function() {
        	this.content.style.display = 'none';
        	var c = this.content.childNodes;
        	for (var i = 0; i < c.length; i++) {
        		if ((c[i].tagName != undefined) && c[i].tagName == 'DIV') {
        			c[i].style.display = 'none';
        		}
        	}
          };
        	  
      	  Popup.prototype.onAdd = function() {
    	    this.getPanes().floatPane.appendChild(this.anchor);
    	  };

    	  Popup.prototype.onRemove = function() {
    	    if (this.anchor.parentElement) {
    	      this.anchor.parentElement.removeChild(this.anchor);
    	    }
    	  };

    	  Popup.prototype.draw = function() {
    	    var divPosition = this.getProjection().fromLatLngToDivPixel(this.position);
    	    var display =
    	        Math.abs(divPosition.x) < 4000 && Math.abs(divPosition.y) < 4000 ?
    	        'block' :
    	        'none';

    	    if (display === 'block') {
    	      this.anchor.style.left = divPosition.x + 'px';
    	      this.anchor.style.top = divPosition.y + 'px';
    	    }
    	    if (this.anchor.style.display !== display) {
    	      this.anchor.style.display = display;
    	    }
    	  };

    	  Popup.prototype.stopEventPropagation = function() {
    	    var anchor = this.anchor;
    	    anchor.style.cursor = 'auto';
    	    ['click', 'dblclick', 'contextmenu', 'wheel', 'mousedown', 'touchstart',
    	     'pointerdown']
    	        .forEach(function(event) {
    	          anchor.addEventListener(event, function(e) {
    	            e.stopPropagation();
    	          });
    	        });
    	  };
   	 }   
    /***************** Bottom Info Panel ****************/
}