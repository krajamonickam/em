angular.module('rfxcelApp').controller('mapMarkerPathController', mapMarkerPathController);

mapMarkerPathController.$inject = ['$scope', '$rootScope', '$filter', 'mapDataService', 'productAttributeService', 'sharedDataService', '$state', 'mapConfigService', '$localStorage', 'commonConfigService', 'generalUtilityService', '$window'];

function mapMarkerPathController($scope, $rootScope, $filter, mapDataService, productAttributeService, sharedDataService, $state, mapConfigService, $localStorage, commonConfigService, generalUtilityService,$window) {
	if(sharedDataService.selectedShipment() === undefined || sharedDataService.getSelectedProductId() === undefined){
		var lastState = sharedDataService.getLastVisitedUIState();
        sharedDataService.setLastVisitedUIState($state.current.name);
        $state.go('app.dashboard');
        return;
	}
    $scope.traceId = sharedDataService.getTraceId();
    $scope.mapInfo = false;
    // latLngPath: Array stores the last and next datapoint to plot the flightpath between the two points
    var refreshAddMapPathMarker,
	    mapParams = {
	    		mapTypeId: google.maps.MapTypeId.HYBRID
    	},
	    latLngPath = [],
        markerColorArr = mapConfigService.getMarkerColorCode(),
    	maxZoomVal = $localStorage.mapZoom,
    	map = new google.maps.Map(document.getElementById("map-2"), mapParams);
    	
    
    // Show the MapAlertsBtn for app.dashboard.showMap state
    $rootScope.isShowMapAlertsBtn = true;
    
    // Display the Dashboard tab as selected in the Top Main Navigation tab
    $rootScope.activeTabNumbTopNav = 1;
    
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.dashboard.mapMarkerPath') {
            clearTimeout(refreshAddMapPathMarker);
            $rootScope.isShowMapAlertsBtn = false;  // hide the MapAlertsBtn for other states
        }
    });
    
    $scope.selectedPointFavoriteStatus = sharedDataService.getSelectedFavoriteOption();

    // Get product attributes
    function getProductAttributes(event, i, locationLatLng) {
        var location,
            isLastDataPoint,
            nearestLocation = $scope.mapData.locations[i].address,
            date = new Date(),
            currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
            /*reqObject = {
                "dateTime": currentDate,
                "deviceData": {
                    "packageId": sharedDataService.selectedShipment().packageId,
                    "deviceId" : sharedDataService.selectedShipment().deviceId,
                    "id" : $scope.mapData.locations[i].id
                }
            };*/
			
			reqObject = {
                "dateTime": currentDate,
				"orgId": $localStorage.orgId,
                "deviceData": {
                    "packageId": sharedDataService.selectedShipment().packageId,
                    "deviceId" : sharedDataService.selectedShipment().deviceId,
                    "id" : $scope.mapData.locations[i].id,
                    "productId": sharedDataService.getSelectedProductId(),
                    "showMapAttribute": sharedDataService.getShowMapAttribute(),
                    "rtsItemId": sharedDataService.getRTSTraceId(),
                    "rtsSerialNumber": sharedDataService.getRTSSerialNumber()
                }
            };
			
		

        productAttributeService.post(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	//alert('mappath0');
            	$scope.mapInfo = true;
            	$scope.traceId = response.traceId;
            	sharedDataService.setTraceId(response.traceId);
            	sharedDataService.setSelectedDeviceId(response.deviceId);
            	if(response.sensor.statusTime!= null){
					$scope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime(response.sensor.statusTime);
            	}else{
            		$scope.productLastUpdateTime = 'N.A';
            	}
				
            	if(response.sensor.attributeList[0] !== undefined) {
            		var minValue = response.sensor.attributeList[0].min;
            		var maxValue = response.sensor.attributeList[0].max;
            		var rangeValue = response.sensor.attributeList[0].attrValue;
            		var alertNotify = response.sensor.attributeList[0].alert;
            		if(alertNotify === false){
            			$scope.isColorRedAttr1 = true;
            		} else {
            			if(minValue !== undefined && maxValue !== undefined){
                			$scope.isColorRedAttr1 = (rangeValue >= minValue && rangeValue <= maxValue);
                		} else {
                			$scope.isColorRedAttr1 = true;
                		}
            		}
    				$scope.productAttrLegend1 = response.sensor.attributeList[0].legend;
    				
    				if (response.sensor.attributeList[0].attrValue === commonConfigService.getNullAttrResponseCode()) {
    					$scope.productAttrVal1 = 'N.A';
    					$scope.productAttrUnit1 = '';
    					$scope.isColorRedAttr1 = true;
    				} else {
    					$scope.productAttrVal1 = response.sensor.attributeList[0].attrValue;
    					$scope.productAttrUnit1 = response.sensor.attributeList[0].unit
    				}
    			}

    			if(response.sensor.attributeList[1] !== undefined) {
    				var minValue = response.sensor.attributeList[1].min;
            		var maxValue = response.sensor.attributeList[1].max;
            		var rangeValue = response.sensor.attributeList[1].attrValue;
            		var alertNotify = response.sensor.attributeList[1].alert;
            		if(alertNotify === false) {
            			$scope.isColorRedAttr2 = true;
            		} else {
            			if(minValue !== undefined && maxValue !== undefined){
                			$scope.isColorRedAttr2 = (rangeValue >= minValue && rangeValue <= maxValue);
                		}else{
                			$scope.isColorRedAttr2 = true;
                		}
            		}
    				$scope.productAttrLegend2 = response.sensor.attributeList[1].legend;
    				
    				if (response.sensor.attributeList[1].attrValue === commonConfigService.getNullAttrResponseCode()) {
    					$scope.productAttrVal2 = 'N.A';
    					$scope.productAttrUnit2 = '';
    					$scope.isColorRedAttr2 = true;
    				} else {
    					$scope.productAttrVal2 = response.sensor.attributeList[1].attrValue;
    					$scope.productAttrUnit2 = response.sensor.attributeList[1].unit;
    				}
    			}
            }
        }, function (error) {
            console.log(error);
        });
        
        if (locationLatLng) {
            location = locationLatLng;
            isLastDataPoint = true;
        } else {
            location = event.latLng.lat() + ',' + event.latLng.lng();
        }
        
        $scope.$emit('mapDataPointSelected', location, isLastDataPoint, nearestLocation);
    }
    
  
    
    function drawFlightPath(map, pairStatus) {
        var flightPath = new google.maps.Polyline({
            path: latLngPath,
            strokeColor: markerColorArr[pairStatus],
            strokeOpacity: 1,
            strokeWeight: 6,
           
            icons: [{
                repeat: '100px'
            }]
        });
        flightPath.setMap(map);
        // get the new datapoint 
        var lastLatLng = latLngPath.slice(-1)[0];
        latLngPath = [];
        latLngPath.push(lastLatLng);
    }
    
    /*draws the map for selected device*/
    function drawMap(isLatLongAvailable) {
        var markers = [],
            markersHalo = [],
            lastLocationArrIndex = $scope.mapData.locations.length - 1,
            loopCount,
            startEndPointArr = [],
            mapLoclastIndex,
            center = null;
            if(isLatLongAvailable===false){
                    center= new google.maps.LatLng($localStorage.centerLat, $localStorage.centerLong);
                    map.setZoom(6);
                    map.setCenter(center);
                    console.log('current zoom level for map : ' +  map.getZoom());
                    return;
            }
        
        if ($scope.mapData.centerLat === undefined || $scope.mapData.centerLat === null) {
            center = new google.maps.LatLng($scope.mapData.locations[lastLocationArrIndex].lat, $scope.mapData.locations[lastLocationArrIndex].lang);
        } else {
            center = new google.maps.LatLng($scope.mapData.centerLat, $scope.mapData.centerLong);
        }
        var markerBounds = new google.maps.LatLngBounds();
        var latLngFirstPoint = new google.maps.LatLng($scope.mapData.locations[0].lat, $scope.mapData.locations[0].lang);
        
        $scope.productLocation  = $scope.mapData.locations[0].lat + ',' + $scope.mapData.locations[0].lang;
        myTrip = [];
        // ***********  draw path for all the points - START *************
        for (var i = 0; i < $scope.mapData.locations.length; i++) {
            var latLng = new google.maps.LatLng($scope.mapData.locations[i].lat, $scope.mapData.locations[i].lang);
            myTrip.push(latLng);
            // Push the 1st datapoint but don't draw the flightpath. Flightpath must be drawn only if more than one datapoint
            if (i === 0) {
                latLngPath.push(latLng);
            }
            if (i > 0) { // Push the datapoint and draw the flightpath.
                latLngPath.push(latLng);
                drawFlightPath(map, $scope.mapData.locations[i-1].pairStatus);
            }
            
            if (generalUtilityService.validateLatLong($scope.mapData.locations[i].lat,$scope.mapData.locations[i].lang)){
            	markerBounds.extend(latLng);
            }
        }
        
        map.fitBounds(markerBounds); //Displays all Points and set a zoom level
        var listener = google.maps.event.addListener(map, "idle", function() { 
          if (map.getZoom() > maxZoomVal) map.setZoom(maxZoomVal);
          console.log('current zoom level for map : ' +  map.getZoom());
          google.maps.event.removeListener(listener); 
        });
        
        
  

        // ***********  draw path for all the points - END *************
        
        // *******  plot marker for 1st and last datapoint - STARTS ********
        if ($scope.mapData.locations.length === 1) { // plot only the start point
            loopCount = 1;
            startEndPointArr = [
                {
                    lat: $scope.mapData.locations[0].lat,
                    lang: $scope.mapData.locations[0].lang,
                    productAttrIdIndex: 0,
                    pairStatus: $scope.mapData.locations[0].pairStatus
                }
            ];
        } else if ($scope.mapData.locations.length > 1) {
            loopCount = 2;
            mapLoclastIndex = $scope.mapData.locations.length - 1;
            startEndPointArr = [
                {
                    lat: $scope.mapData.locations[0].lat,
                    lang: $scope.mapData.locations[0].lang,
                    productAttrIdIndex: 0,
                    pairStatus: $scope.mapData.locations[0].pairStatus
                },
                {
                    lat: $scope.mapData.locations[mapLoclastIndex].lat,
                    lang: $scope.mapData.locations[mapLoclastIndex].lang,
                    productAttrIdIndex: mapLoclastIndex,
                    pairStatus: $scope.mapData.locations[mapLoclastIndex].pairStatus
                }
            ];
        }
        
        var latLngStartPoint = new google.maps.LatLng(startEndPointArr[0].lat, startEndPointArr[0].lang);
        
        // Plot the halo marker for the last point
        var latLngLastPoint = new google.maps.LatLng(startEndPointArr[loopCount - 1].lat, startEndPointArr[loopCount - 1].lang);
        plotHaloMarker(latLngLastPoint, startEndPointArr[loopCount - 1].productAttrIdIndex, startEndPointArr[loopCount - 1].pairStatus);
        
        for (var i = 0; i < loopCount; i++) {
            var latLng = new google.maps.LatLng(startEndPointArr[i].lat, startEndPointArr[i].lang);
            plotMarker(latLng, startEndPointArr[i].productAttrIdIndex, startEndPointArr[i].pairStatus, 8, markerColorArr[startEndPointArr[i].pairStatus]);   
        }
        
        if (loopCount === 2) { // Show the white center marker only when there are more than 1 point 
            plotMarker(latLngStartPoint, 0, 3, 4, '#FFFFFF'); // plot start point white center marker
        }
        // *******  plot marker for 1st and last datapoint - ENDS ********
        
        // call addMapPathMarker after specific interval
        addMapPathMarkerInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
		/*
        if($scope.selectedItemType.typeId != 6 && $scope.selectedItemType.typeId != 4 && $scope.selectedItemType.typeId != 5 ){
        refreshAddMapPathMarker = setInterval(addMapPathMarker, addMapPathMarkerInterval);
        }
		*/
        function plotHaloMarker(latLng, lastDataPointIndex, pairStatus) {
            // remove previous halo marker
            if (markersHalo.length > 0) {
                markersHalo[0].setMap(null);
                markersHalo = [];
            }
            //var markerColorArr = sharedDataService.getMarkerColorCode();
            var scale = 20,
                fillOpacity = 0.3,
                markerHalo = new google.maps.Marker({
                    position: latLng,
                    icon: {
                        path: google.maps.SymbolPath.CIRCLE,
                        scale: scale,
                        fillColor: markerColorArr[pairStatus],
                        fillOpacity: fillOpacity,
                        strokeWeight: 0.2
                    },
                    draggable: false,
                    map: map
                });
            markerHalo.setMap(map);
            markersHalo.push(markerHalo);
        }
        
        function plotMarker(latLng, lastDataPointIndex, pairStatus, scale, fillColor) {
            var marker = new google.maps.Marker({
                position: latLng,
                icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: scale,
                    fillColor: fillColor,
                    fillOpacity: 1,
                    strokeWeight: 0.2
                },
                draggable: false,
                map: map
            });

            marker.setMap(map);
            markers.push(marker);
            var addListener = function (lastDataPointIndex) {
                google.maps.event.addListener(marker, 'mouseover', function (event) {
                    // GET Attributes for the selected data point
                    getProductAttributes(event, lastDataPointIndex);
                });
            };
            addListener(lastDataPointIndex);
        }
        
        /*  Check if new datapoints are added. 
         *  If yes, then plot them on the map and draw the path.
         */
        function addMapPathMarker() {
            var date = new Date(),
                currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
                reqObject = {
                    "dateTime": currentDate,
					"orgId": $localStorage.orgId,
                    "deviceData": {
                        "packageId": sharedDataService.selectedShipment().packageId,
                        "deviceId" : sharedDataService.selectedShipment().deviceId,
                        "startDate" : $scope.mapMarkerPathLastTimeStamp,
                        "productId": sharedDataService.getSelectedProductId()
                    },
                    "rtsDataFetch" : $scope.isRtsDataFetch
                    //"searchType": $scope.selectedItemType.typeId,
                };

            // Service to get the new data points
            mapDataService.post(JSON.stringify(reqObject)).$promise.then(function (response) {
            	if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                var mapData = response.sensor.map,
                    loopCount = 0;
                
                // Execute this only if new datapoints are fetched   
                if (mapData.locations.length > 0) {
                    // lastTimeStamp to be used for next API request
                    $scope.mapMarkerPathLastTimeStamp = mapData.lastTimeStamp;

                    // plot new datapoint 
                    var lastIndex = myTrip.length - 1,
                        lastVal = myTrip[lastIndex];
                    myTrip = [];
                    myTrip.push(lastVal);
                    loopCount = $scope.mapData.locations.length + mapData.locations.length;

                    for (var i = $scope.mapData.locations.length, newLocationCount = 0 ; i < loopCount ; i++, newLocationCount++) {
                        // push the new datapoint in $scope.mapData.locations  
                        $scope.mapData.locations.push(mapData.locations[newLocationCount]);
                        var latLng = new google.maps.LatLng(mapData.locations[newLocationCount].lat, mapData.locations[newLocationCount].lang);
                        myTrip.push(latLng);
                        // push the new datapoint in the array that contains the last datapoint
                        latLngPath.push(latLng);
                        // draw flight between the existing last datapoint and the new datapoint
                        drawFlightPath(map, $scope.mapData.locations[i-1].pairStatus);
                    }

                    // remove old markers
                    if (markers.length > 0) {
                        for (var i = 0; i < markers.length; i++) {
                            markers[i].setMap(null);
                        }
                        markers = [];
                    }
                    
                    var plotMarkerLastIndex = loopCount - 1,
                        lastDataPointIndex = mapData.locations.length - 1;
                    var latLng = new google.maps.LatLng(mapData.locations[lastDataPointIndex].lat, mapData.locations[lastDataPointIndex].lang);

                    plotHaloMarker(latLng, plotMarkerLastIndex, $scope.mapData.locations[plotMarkerLastIndex].pairStatus);
                    plotMarker(latLngStartPoint, 0, $scope.mapData.locations[0].pairStatus, 8, markerColorArr[$scope.mapData.locations[0].pairStatus]);

                    plotMarker(latLng, plotMarkerLastIndex, $scope.mapData.locations[plotMarkerLastIndex].pairStatus, 8, markerColorArr[$scope.mapData.locations[plotMarkerLastIndex].pairStatus]);

                    plotMarker(latLngStartPoint, 0, 3, 4, '#FFFFFF'); // plot start point white center marker

                    // Update the product attributes in the bottom panel
                    // to show last datapoint attibutes
                    var locationLatLng = mapData.locations[lastDataPointIndex].lat + ',' + mapData.locations[lastDataPointIndex].lang;
                    getProductAttributes(event, plotMarkerLastIndex, locationLatLng);
                } else {
                  console.log('no map data available');
                }
                
            }, function (error) {
                console.log(error);
            });
        } // addMapPathMarker ends here
    } // drawMap ends here
    
    /*fetch map datapoint for device*/
    function loadMapDataforDevice() {
        $scope.$emit('selectedProductPackageId', sharedDataService.selectedShipment().packageId);
        $scope.$emit('selectedProductDeviceId', sharedDataService.selectedShipment().deviceId);
        var date = new Date(),
            currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
            reqObject = {
                "dateTime": currentDate,
				"orgId": $localStorage.orgId,
                "deviceData": {
                    "packageId": sharedDataService.selectedShipment().packageId,
                    "deviceId" : sharedDataService.selectedShipment().deviceId,
                    "productId": sharedDataService.getSelectedProductId()
                },
                "rtsDataFetch" : $scope.isRtsDataFetch
                //"searchType": $scope.selectedItemType.typeId,
            };
        
        //getMapData
        mapDataService.post(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
				/*
            	if($scope.selectedItemType.typeId == 6){
        			var alertData = sharedDataService.getAlertMapData();
        			$scope.mapData = {
        					lastTimeStamp : alertData.createdOn,
        					locations : [{
        						"dateTime" : alertData.createdOn,
        						"lang" : alertData.longitude,
        						"lat" : alertData.latitude,
        						"pairStatus" : alertData.pairStatus
        					}]
        			}
        		}else{
            	$scope.mapData = response.sensor.map;
        		}
				*/
				$scope.mapData = response.sensor.map;
                if ($scope.mapData.locations.length > 0) {
                    var lastIndex = $scope.mapData.locations.length - 1;
                    // fetch the new datapoint based on the date time of last datapoint
                    $scope.mapMarkerPathLastTimeStamp = $scope.mapData.lastTimeStamp;
                    $scope.$emit('nearestLocation', $scope.mapData.locations[lastIndex].address);
                    var coordinatesBottomPanel = {
                    		lat : $scope.mapData.locations[lastIndex].lat,
                    		lang : $scope.mapData.locations[lastIndex].lang
                    }
					/*
                    if($scope.selectedItemType.typeId == 4 || $scope.selectedItemType.typeId == 5){
                    	$scope.$emit('locationForBottomPanel', coordinatesBottomPanel);
                    }
					$scope.$emit('locationForBottomPanel', coordinatesBottomPanel);
                    if($scope.selectedItemType.typeId == 6){
                    	$rootScope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime(parseInt($scope.mapData.lastTimeStamp));
                    }else{
                    	$rootScope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime($scope.mapData.lastTimeStamp);
                    }
					*/
					$rootScope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime($scope.mapData.lastTimeStamp);
                    drawMap(true);
                } else {
                    drawMap(false);
                    console.log('no map data available');
                }
            }
        }, function (error) {
            console.log(error);
        });
    }

    loadMapDataforDevice();
        
    $scope.onViewDataClick = function (deviceId, packageId) {
        sharedDataService.setSelectedDeviceId(deviceId);
        sharedDataService.setSelectedShipmentId(packageId);
        $rootScope.activeTabNumbTopNav = 2;
        $state.go('app.productsDetails.data');
    };
    
	 
    $scope.onViewDetailsClick = function(id, typeId){
        $scope.traceId = sharedDataService.getTraceId();
    	var selectedDeviceId = sharedDataService.getSelectedDeviceId();

        var traceEntId;
    	if($scope.traceId != null || $scope.traceId != ""){
        	traceEntId =  $scope.traceId;
    	}else{
    		return
    	}
    	var loadState;
    	var stateParam;
    	if(typeId === 4){
    		loadState = 'app.traceEventDetails';
    		stateParam = {
    				eventId: id,
    				deviceId: selectedDeviceId
    		}
    	}else if(typeId === 5){
    		loadState = 'app.traceItemDetails';
    		stateParam = {
    				traceId: traceEntId,
    				deviceId: selectedDeviceId
    		}
    	}else if(typeId === 1 || typeId === 2 || typeId === 3){
    		loadState = 'app.traceItemDetails';
    		stateParam = {
    				traceId: traceEntId,
    				deviceId:selectedDeviceId
    		}
    	}
		var url = $state.href(loadState, stateParam),
			height = $window.innerHeight * .85,
   			width = $window.innerWidth * .90;
		$window.open(url, "", "width="+width+",height="+height+",left=60,top=50,scrollbars=1,resizable=1");
	};
	

	 
    $scope.onViewAlertClick = function (deviceId, packageId) {
		sharedDataService.setSelectedDeviceId(deviceId);
		sharedDataService.setSelectedShipmentId(packageId);
		$state.go('app.productsDetails.alert');
    };
}