/*jslint plusplus: true */
/*jslint devel: true */
angular.module('rfxcelApp').controller('modifyProfileCtrl', modifyProfileCtrl);
function modifyProfileCtrl($scope, $state, $rootScope, createProfileService, profileListService, sharedDataService, $localStorage, googleAPIService, DeviceTypeService, AlertGroupService, AlertService) {
	getDefaultAlertList();
	getAlertGroups();
	
	var freqComm,
		freqGps,
		freqAttrFreq,
		freqTimeUnitArr = [],
		eachSlider,
		multiplier = [60, 3600, 86400],
		frequencyMax={};
	
	/* Variables to hold error messages */
	$scope.profileNameErrorMsg = "";
	$scope.profileDescErrorMsg = "";
	$scope.commFrqErrorMsg = "";
	$scope.gpsFrqErrorMsg = "";
	$scope.attrFrqErrorMsg = "";
	$scope.alertList = "";
	
	/* Stores the index of selected home location */
	/*$scope.homeLocationIndex = 0;*/
	
	/* Right side attribute section must be hidden untill the device type is selected */
	$scope.isDeviceTypeSelcted = true;
    /* Device attribute array*/
	$scope.deviceAttributes = [];
	/* Geo location data array*/
	$scope.geopointsData = [];
	/* Array to store error messages for different attributes */
	$scope.errorMessageProfileAttr = [];
	$scope.errorMessageProfileAttrEmail = [];
	/* To store the geo location value*/
	$scope.geolocationRadiusValue = '';
	/* To store the geo location format*/
	$scope.geolocationRadiusFormat = '';
	/* Add the geolocation row based on add button*/
	
	
	$scope.slider = [];
	
	$scope.frequencyTimeUnit = function (frequencyType, timeUnitValue) {
		freqTimeUnitArr[frequencyType] = timeUnitValue;
		for (i = 0; i < 3; i++) {
			if (i === timeUnitValue) {
				$scope.isActiveFrequency[frequencyType].timeUnit[i] = 1;
			} else {
				$scope.isActiveFrequency[frequencyType].timeUnit[i] = 0;
			}
		}
	};
	
	$scope.addGeopoints = function () {
		$scope.profile.geopointsData.push({
			"address": "",
			"latitude": "",
			"longitude": "",
			"radius": $scope.geolocationRadiusValue,
			"pointNumber": "",
            "locationType": $scope.profile.geopointsData.length===1?2:3
		});
	};
	
	/* remove the geolocation row based on remove button*/
	
	/* Remove the geolocation row based on remove button*/
	$scope.removeGeopoints = function (index) {
        if ($scope.profile.geopointsData.length !== 1) {
			$scope.profile.geopointsData.splice(index, 1);
		} else {
			$scope.profile.geopointsData = [];
	        $scope.profile.geopointsData = [{
				"address": "",
				"latitude": "",
				"longitude": "",
				"radius": $scope.geolocationRadiusValue,
				"pointNumber": "",
				"locationType": 3
			}];
		}
	}
	
	/*$scope.homeLocationChange=function(index){
        for(var i in $scope.profile.geopointsData){
            if(i==index){
            	$scope.profile.geopointsData[i].isHome=1;
            }else{
            	$scope.profile.geopointsData[i].isHome=0;
            }
        }
    }*/
	
	/* Set Max Frequency Limit Object */
	function setFrequency(deviceTypeObj){
		frequencyMax.gpsComFreqLimit=deviceTypeObj.gpsComFreqLimit;
		frequencyMax.receiveComFreqLimit=deviceTypeObj.receiveComFreqLimit;
		frequencyMax.sendComFreqLimit=deviceTypeObj.sendComFreqLimit;
	}
	
	 $scope.onChangeSliderFn = function(index,min,rangeMaxVal){
	      //$scope.attributeMaxChange(rangeMaxVal,index);
	 }
	    
    /*$scope.attributeMaxChange=function(rangeMaxVal,index){
        if(rangeMaxVal > 0){
            $scope.profile.deviceAttributes[index].isAlertActive=true;
            $scope.profile.deviceAttributes[index].isNotificationActive=true;
        }else{
            $scope.profile.deviceAttributes[index].isAlertActive=false;
            $scope.profile.deviceAttributes[index].isNotificationActive=false;
        }
    }*/
	
	/* Get the deviceType based on orgId */
	function getDeviceType() {
		var reqObject = {
			"orgId": $localStorage.orgId,
			"searchType": "All"
        };
		DeviceTypeService.getDeviceTypes().save(JSON.stringify(reqObject)).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			$scope.deviceList = response.sensor.deviceList;
			
			for (i = 0; i < response.sensor.deviceList.length; i++) {
				/* Loop through the device list
				 * Set the device type for the selected device. This device type will be shown in disabled Device Type dropdown
				 */
				if (response.sensor.deviceList[i].deviceTypeId === sharedDataService.getSelectedProfile().deviceTypeId) {
					$scope.deviceListSelectedOption =  {
						availableOptions: $scope.deviceList,
						selectedOption: {
							deviceTypeId: sharedDataService.getSelectedProfile().deviceTypeId,
							deviceType: response.sensor.deviceList[i].deviceType
						}
					}
					$scope.setDeviceType=response.sensor.deviceList[i].deviceType;
					setFrequency(response.sensor.deviceList[i]);
				}
			}
		}, function (error) {
			console.log(error);
		});
	}
    
    /* Edit the profile based on profileId */
    function getEditProfileData() {
		var reqObject = {
            "profile": {
                "profileId": sharedDataService.getSelectedProfile().profileId,
                "orgId": $localStorage.orgId,
                "groupId": $localStorage.groupId
            }
        };
        
        createProfileService.getProfile.save(JSON.stringify(reqObject)).$promise.then(function (response) {
			
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
				
				/* Check min and max values for all the attributes.
				   If both min & max are 0 change their values to  empty string i.e ''.
				 */
				for (i = 0; i < response.profile.attrType.length; i++) {
					/*if (response.profile.attrType[i].minValue === 0 && response.profile.attrType[i].maxValue === 0) {
						response.profile.attrType[i].minValue = '';
						response.profile.attrType[i].maxValue = '';
					}*/
					if($rootScope.newProfileScreen === true){
						if(response.profile.attrType[i].emailIds !== undefined && response.profile.attrType[i].emailIds !== null){
							response.profile.attrType[i].emailIds = response.profile.attrType[i].emailIds.toString();
						}else{
							response.profile.attrType[i].emailIds = '';
						}
						if(response.profile.attrType[i].alertDetailMsg !== undefined && response.profile.attrType[i].alertDetailMsg !== null){
							response.profile.attrType[i].alertDetailMsg = response.profile.attrType[i].alertDetailMsg;
						}else{
							for (j = 0; j < $scope.alertList.length; j++) {
								if($scope.alertList[j].attrName == response.profile.attrType[i].name){
									response.profile.attrType[i].alertDetailMsg = $scope.alertList[j].alertDetailMsg;
									response.profile.attrType[i].alertCode = $scope.alertList[j].alertCode;
								}
							}
						}
					}
					
					if (response.profile.attrType[i].minValue === 0 && response.profile.attrType[i].maxValue === 0) {
						response.profile.attrType[i].minValue = response.profile.attrType[i].rangeMin;
						response.profile.attrType[i].maxValue = response.profile.attrType[i].rangeMin;
                        response.profile.attrType[i].isAlertActive=true;                 
					}else{
						if(response.profile.attrType[i].alert === 1){
							response.profile.attrType[i].isAlertActive=false;
						}else{
							response.profile.attrType[i].isAlertActive=true;
						}
                    }
					
					/* add slider*/
					eachSlider = {
						minValue: response.profile.attrType[i].minValue,
						maxValue: response.profile.attrType[i].maxValue,
						options: {
							noSwitching: true,
							floor: response.profile.attrType[i].rangeMin,
							ceil: response.profile.attrType[i].rangeMax,
							step: 1,
							precision: 2,
							enforceStep: false,
							getSelectionBarColor: function (value) {
								return '#58B820';
							},
							getPointerColor: function (value) {
								return '#07C';
							},
	                        onChange : $scope.onChangeSliderFn,
	                        id: i
						}
					};
					$scope.slider.push(eachSlider);			
				}
                
				/* Loop through all the geopoint locations */
				for (i = 0; i < response.profile.geopoints.length; i++) {
					/* If radius become 0 change their values to  empty string i.e ''. */
					if (response.profile.geopoints[i].radius === 0) {
						response.profile.geopoints[i].radius = '';
					}
					
					/* Get the home location from all the locations */
					/*if (response.profile.geopoints[i].isHome === 1 ) {
						$scope.homeLocationIndex = i;
					}*/
				}
				$scope.alertIdDetails = '';
				
				if($rootScope.newProfileScreen === true){
					var groupIds = [];
					if(response.profile.additionalAlerts.length > 0){
						var obj;
						for(i=0;i<response.profile.additionalAlerts.length;i++){
							obj = {
								"alertGroupIds":response.profile.additionalAlerts[0].alertGroupIds
							};
						}
						groupIds.push(obj);
						if(response.profile.additionalAlerts[0].emailIds !== undefined && response.profile.additionalAlerts[0].emailIds !== null){
							$scope.locationGroupEmailId = response.profile.additionalAlerts[0].emailIds.toString();
						}else{
							$scope.locationGroupEmailId = '';
						}
						if(response.profile.additionalAlerts[0].alertDetailMsg !== undefined && response.profile.additionalAlerts[0].alertDetailMsg !== null){
							$scope.locationEntryAlertMsg = response.profile.additionalAlerts[0].alertDetailMsg;
							$scope.locationEntryAlertCode = response.profile.additionalAlerts[0].alertCode;
							$scope.locationEntryAlertName = response.profile.additionalAlerts[0].name;
						}if(response.profile.additionalAlerts[1].alertDetailMsg !== undefined && response.profile.additionalAlerts[1].alertDetailMsg !== null){
							$scope.locationExitAlertMsg = response.profile.additionalAlerts[1].alertDetailMsg;
							$scope.locationExitAlertCode = response.profile.additionalAlerts[1].alertCode;
							$scope.locationExitAlertName = response.profile.additionalAlerts[1].name;
						}
					}else{
						$scope.locationGroupEmailId = '';
						groupIds = '';
						for (j = 0; j < $scope.alertList.length; j++) {
							if($scope.alertList[j].alertCode == '3701'){
								$scope.locationEntryAlertMsg = $scope.alertList[j].alertDetailMsg;
								$scope.locationEntryAlertCode = $scope.alertList[j].alertCode;
								$scope.locationEntryAlertName = $scope.alertList[j].alertMsg;
							}
							if($scope.alertList[j].alertCode == '3702'){
								$scope.locationExitAlertMsg = $scope.alertList[j].alertDetailMsg;
								$scope.locationExitAlertCode = $scope.alertList[j].alertCode;
								$scope.locationExitAlertName = $scope.alertList[j].alertMsg;
							}
						}
					}
				}
                
				var commFrequency,
					gpsFrequency,
					attbFrequency;
				// Congiguration to show active time unit in frequency section. Default is minutes
				$scope.isActiveFrequency = [
					{timeUnit: [0, 0, 0]},
					{timeUnit: [0, 0, 0]},
					{timeUnit: [0, 0, 0]}
				];
				
				/* Set selected frequency unit */
				$scope.isActiveFrequency[0].timeUnit[response.profile.commFrequencyUnit] = 1;
				$scope.isActiveFrequency[1].timeUnit[response.profile.gpsFrequencyUnit] = 1;
				$scope.isActiveFrequency[2].timeUnit[response.profile.attbFrequencyUnit] = 1;
				
                /* Check frequency values for all the Frequency.
				   If frequency become 0 change their values to  empty string i.e ''.
				 */
                commFrequency = response.profile.commFrequency === 0 ? 0 : response.profile.commFrequency / multiplier[response.profile.commFrequencyUnit];
                gpsFrequency = response.profile.gpsFrequency === 0 ? 0 : response.profile.gpsFrequency / multiplier[response.profile.gpsFrequencyUnit];
                attbFrequency = response.profile.attbFrequency === 0 ? 0 : response.profile.attbFrequency / multiplier[response.profile.attbFrequencyUnit];
				
                $scope.profile = {
                    profileId: response.profile.profileId,
                    profileName: response.profile.name,
                    profileDesc: response.profile.description,
                    deviceTypeId: response.profile.deviceTypeId.toString(),
                    deviceAttributes: response.profile.attrType,
                    geopointsData: response.profile.geopoints,
                    comm: commFrequency,
                    gps: gpsFrequency,
                    attrFreq: attbFrequency,
                    addAlerts: groupIds
                };
				
				/* If no geolocation present show one location with empty text value */
				if (response.profile.geopoints.length === 0) {
					$scope.profile.geopointsData.push({
						"address": '',
						"latitude": '',
						"longitude": '',
						"pointName": '',
						"radius": $scope.geolocationRadiusValue,
                        "pointNumber": '',
                        "locationType": 1
					});
				}
				
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
    
    /* Get the attributes based on orgId, groupId and deviceId */
	$scope.getAttributesByDeviceType = function (deviceId) {
		if (deviceId === '' || deviceId === undefined) {
			$scope.isDeviceTypeSelcted = false;
            return;
        }
        
		var attrMinValue,
			attrMaxValue,
			reqdevice = {
				"orgId": $localStorage.orgId,
				"deviceDetail": {
					"deviceTypeId": deviceId
				}
			};
		
		DeviceTypeService.getAttributesByDeviceType().save(JSON.stringify(reqdevice)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            $scope.profile.deviceAttributes = response.sensor.attributeList;
		}, function (error) {
			console.log(error);
        });
		
		$scope.geopointsData = [{
			"address": "",
			"latitude": "",
			"longitude": "",
			"radius": $scope.geolocationRadiusValue,
			"pointNumber": ""
		}];
		$scope.isDeviceTypeSelcted = true;
		$scope.profileNameErrorMsg = "";
		$scope.profileDescErrorMsg = "";
	};
	
	function getAlertGroups() {
        var reqObj = {
        		"alertGroup": {
        			 "orgId": $localStorage.orgId
                }
        }

        AlertGroupService.getAlertGroup().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.alertGroupList = response.alertGroupList;
            } else {
                $scope.message = response.responseMessage;
            }

        }, function (error) {
            console.log(error);
        });
    }
	
	function getDefaultAlertList() {
		var data = "";
        var reqObject = {
            "alert": {
            	"orgId": $localStorage.orgId
            }
        }

        AlertService.getDefaultAlertList().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.alertList = response.alertList;
				
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
	
	$scope.alertGroupChanged=function(alertObj){
    }
	
	$scope.increase = function (param) {
        if (param === 'profilecomm') {
            $scope.profile.comm = $scope.profile.comm + 1;
        } else if (param === 'profilegps') {
            $scope.profile.gps = $scope.profile.gps + 1;
        } else {
            $scope.profile.attrFreq = $scope.profile.attrFreq + 1;
        }
	};
    
    $scope.decrease = function (param) {
		if (param === 'profilecomm' && $scope.profile.comm > 1) {
			$scope.profile.comm = $scope.profile.comm - 1;
		} else if (param === 'profilegps' && $scope.profile.gps > 1) {
			$scope.profile.gps = $scope.profile.gps - 1;
		} else if (param === 'attrFreq' && $scope.profile.attrFreq > 1) {
			$scope.profile.attrFreq = $scope.profile.attrFreq - 1;
		}
    }
    /* Convert Time into minutes/days/hours and show max 
	 * limit error messages */
    function convertMaxFreq(maxFrequency,freqTimeUnit){
		var freqLimit;
		if(freqTimeUnit==0){
			freqLimit= maxFrequency/60 + ' minutes';
		}else if(freqTimeUnit==1){
			freqLimit=maxFrequency/(60*60)  + ' hours';
		}else{
			freqLimit=maxFrequency/(24*60*60);
			freqLimit=(freqLimit==1? freqLimit + ' day':freqLimit + ' days') ;
		}
		return freqLimit;
	}
    /* Redirect to the profileList page based on action*/
	$scope.cancel = function () {
        $state.go('app.profile');
    };
	
    // Convert special char from location to ASCII 
    var UTF8 = {
    		encode: function(s){
    			for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
    				s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
    			);
    			return s.join("");
    		},
    		decode: function(s){
    			for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
    				((a = s[i][c](0)) & 0x80) &&
    				(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
    				o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
    			);
    			return s.join("");
    		}
    	};

    
    /* Get the lat & lng based on address value */
	$scope.getLatlngByAddress = function (location, geopointsData,index) {
        $scope.errorMessageProfileLocation = [];
        if (location.address === '' || location.address === undefined) {
            return;
        }
        var loc = UTF8.encode(location.address)     
        var resObj = googleAPIService.getLatlngByAddress(loc);
        resObj.then(function (response) {
            if (response.data.results.length > 0) {
                angular.forEach(geopointsData, function (value, key) {
                	if (key === index) {
                        geopointsData[key].address = location.address;
                        geopointsData[key].latitude = response.data.results[0].geometry.location.lat;
                        geopointsData[key].longitude = response.data.results[0].geometry.location.lng;
                    }
                });
                $scope.profile.geopointsData = geopointsData;
            } else {
                $scope.errorMessageProfileLocation[index] = "Please provide valid address or latitude and longitude value";
            }
        }, function (error) {
            console.log(error);
        });
    };
    
    /* Get the address based on lat & lng value */
    $scope.getAddressByLatlng = function (location, geopointsData, index) {
        /* Array to store error messages for different geolocation */
        $scope.errorMessageProfileLocation = [];
        
        /*if (location.latitude === '' || location.latitude === undefined) {
            return;
        }
        if (location.longitude === '' || location.longitude === undefined) {
            return;
        }*/
		if (location.latitude === '' || location.latitude === undefined || location.longitude === '' || location.longitude === undefined) {
            return;
        }
        var resObj = googleAPIService.getNearestLocation(location.latitude, location.longitude);
		resObj.then(function (response) {
            if (response.data.results.length > 0) {
                angular.forEach(geopointsData, function (value, key) {
                	if (key === index) {
                        geopointsData[key].address = response.data.results[0].formatted_address;
                        geopointsData[key].latitude = location.latitude;
                        geopointsData[key].longitude = location.longitude;
                    }
                });
                $scope.profile.geopointsData = geopointsData;
            } else {
                $scope.errorMessageProfileLocation[index] = "Please provide valid latitude and longitude value";
            }
        }, function (error) {
            console.log(error);
            $scope.errorMessageProfileLocation[index] = "Please provide valid latitude and longitude value";
        });
    };
    
    /*Get the radius value and format for geo location*/
	function getGeolocationRadiusAndFormat() {
		var reqObject = {
			"orgId" : $localStorage.orgId,
			"configData": {
	            "params": ["geolocation.radius.value", "geolocation.radius.format"]
	        }
		};
		createProfileService.getGeolocationRadiusAndFormat.save(JSON.stringify(reqObject)).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			$scope.configValue = response.sensor.configMap;
			$scope.geolocationRadiusValue = $scope.configValue[reqObject.configData.params[0]];
			$scope.geolocationRadiusFormat = $scope.configValue[reqObject.configData.params[1]];
		}, function (error) {
			console.log(error);
		});
	}
	getDeviceType();
    getEditProfileData();
    getGeolocationRadiusAndFormat();
    /* Modify the profile details */
	$scope.modifyProfile = function (homeLocationIndex) {
		var deviceAttributeJson = [],
			geoLocationJson = [],
			geoLocationAlertsJson = [],
			profileName = $scope.profile.profileName,
			profileDesc = $scope.profile.profileDesc,
			keycode,
			i,
			j,
			length,
			isShowValidationMsg = false,
			profileDescription = document.getElementById("profileDesc"),
			min,
			max,
			geoAddress,
			geoLat,
			geoLng,
            geopoint,
            radius,
			timeUnitValue;

        $scope.profiledeviceTypeErrorMsg = "";
        $scope.profileNameErrorMsg = "";
        $scope.profileDescErrorMsg = "";
		$scope.commFrqErrorMsg = "";
		$scope.gpsFrqErrorMsg = "";
		$scope.attrFrqErrorMsg = "";
        
        if ("" === $scope.profile.deviceTypeId || "select" === $scope.profile.deviceTypeId || undefined === $scope.profile.deviceTypeId) {
			$scope.profiledeviceTypeErrorMsg = "Please provide Profile device type.";
			isShowValidationMsg = true;
        }
        if ("" === $scope.profile.profileName) {
			$scope.profileNameErrorMsg = "Please provide profile name.";
			isShowValidationMsg = true;
		}
        /* Check profile description length cannot exceed 250 character*/
        if (profileDescription.value.length > 250) {
			$scope.profileDescErrorMsg = "Description length cannot exceed 250 char";
			isShowValidationMsg = true;
		}
		
		$scope.errorMessageProfileAttr = [];
		$scope.errorMessageProfileAttrEmail = [];
		
		angular.forEach($scope.profile.deviceAttributes, function (value, key) {
		
			min = (value.minValue === '' || value.minValue === null || value.minValue === undefined) ? '' :  parseFloat(value.minValue);
			max = (value.maxValue === '' || value.maxValue === null || value.maxValue === undefined) ? '' :  parseFloat(value.maxValue);
			
			if (min === '' && max === '') {
				$scope.errorMessageProfileAttr[key] = "Min value is required";
				isShowValidationMsg = true;
			} else if (min === '' && max !== '') {
				$scope.errorMessageProfileAttr[key] = "Min Value is required";
				isShowValidationMsg = true;
			} else if (isNaN(min)) {
				$scope.errorMessageProfileAttr[key] = "Min value should be an integer";
				isShowValidationMsg = true;
            } else if (max === '' && min !== '') {
				$scope.errorMessageProfileAttr[key] = "Max Value is required";
				isShowValidationMsg = true;
			} else if (isNaN(max)) {
				$scope.errorMessageProfileAttr[key] = "Max value should be an integer";
				isShowValidationMsg = true;
            } else if (min === max && min !== '' && max !== '' && $scope.profile.deviceAttributes[key].rangeMin !== min && (min!==0 && max!==0)) {
				$scope.errorMessageProfileAttr[key] = "Min & Max values cannot be same";
				isShowValidationMsg = true;
			} else if (min !== '' && max !== '' && min > max) {
				$scope.errorMessageProfileAttr[key] = "Min cannot exceed Max";
				isShowValidationMsg = true;
			} else if (min < $scope.profile.deviceAttributes[key].rangeMin) {
				$scope.errorMessageProfileAttr[key] = "Min value cannot be less than " + $scope.profile.deviceAttributes[key].rangeMin;
				isShowValidationMsg = true;
			} else if (max > $scope.profile.deviceAttributes[key].rangeMax) {
				$scope.errorMessageProfileAttr[key] = "Max value cannot be more than " + $scope.profile.deviceAttributes[key].rangeMax;
				isShowValidationMsg = true;
			} else {
				$scope.errorMessageProfileAttr[key] = "";
				if (min !== '' && max !== '' && value.minValue >= $scope.profile.deviceAttributes[key].rangeMin && value.maxValue > $scope.profile.deviceAttributes[key].rangeMin) {
					if($rootScope.newProfileScreen === true){
						var emailIds = [];
						var mailid = '';
						var mailformat = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
						if(value.emailIds !== undefined && value.emailIds !== null && value.emailIds !== ''){
							mailid = value.emailIds.split(',');
							for(i=0;i<mailid.length;i++){
								if(mailid[i].match(mailformat)){
									emailIds.push(mailid[i]);
								}else{
									$scope.errorMessageProfileAttrEmail[key] = 'Not a valid emailId!';
									isShowValidationMsg = true;
								}
							}
						}else{
							var emailIds = [];
						}
						deviceAttributeJson.push({
							"name": value.name,
							"minValue": value.minValue,
							"maxValue": value.maxValue,
							"alert": value.isAlertActive == false ? 1 : 2,
							"alertCode": value.alertCode,		
							"alertGroupIds": value.alertGroupIds,
							"alertDetailMsg":value.alertDetailMsg,
							"emailIds":emailIds
						});
					}else{
						deviceAttributeJson.push({
							"name": value.name,
							"minValue": value.minValue,
							"maxValue": value.maxValue,
							"alert": value.isAlertActive == false ? 1 : 2
						});
					}
				}
				
			} /*else {
				$scope.errorMessageProfileAttr[key] = "";
				if (min !== '' && max !== '') {
					deviceAttributeJson.push({
						"name": value.name,
						"minValue": value.minValue,
						"maxValue": value.maxValue
					});
				}
			}*/
		});
		
		if($rootScope.newProfileScreen === true){
			var emailIds = [];
			var mailid = '';
			var mailformat = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
			if($scope.locationGroupEmailId !== undefined && $scope.locationGroupEmailId !== null && $scope.locationGroupEmailId !== ''){
				mailid = $scope.locationGroupEmailId.split(',');
				for(i=0;i<mailid.length;i++){
					if(mailid[i].match(mailformat)){
						emailIds.push(mailid[i]);
					}else{
						$scope.mailIdErrorMsg = 'Not a valid emailId!';
						return;
					}
				}
			}else{
				emailIds = [];
			}
			if($scope.profile.addAlerts.length>0){
				for(i=0;i<$scope.profile.addAlerts.length;i++){
					groupIds = $scope.profile.addAlerts[0].alertGroupIds;
				}
			}else{
				groupIds = $scope.profile.alertGroupIds;
			}
			
			geoLocationAlertsJson.push({
				"name": $scope.locationExitAlertName,
				"alertGroupIds": groupIds,
				"alertCode": $scope.locationEntryAlertCode,
				"alertDetailMsg": $scope.locationEntryAlertMsg,
				"emailIds":emailIds
			},{
				"name": $scope.locationEntryAlertName,
				"alertGroupIds": groupIds,
				"alertCode": $scope.locationExitAlertCode,
				"alertDetailMsg": $scope.locationExitAlertMsg,
				"emailIds":emailIds
			});
			
			if(deviceAttributeJson.length>0){
				var defaultAttribute = addDefaultAttribute(deviceAttributeJson);
				geoLocationAlertsJson.push(defaultAttribute);
			}
		}
		
		/* Frequency */
		for (j = 0; j < 3; j++) { // Loop through all the frequencies i.e comm, gps, attr
			for (k = 0; k < 3; k++) { // Loop through all the time units i.e minutes, hours, days
				if ($scope.isActiveFrequency[j].timeUnit[k] === 1) {
				// j: frequency type; 0 - comm, 1 - gps, 2 - attr	
				// k: time unit; 0 - minute, 1 - hours, 2 - days
					$scope.frequencyTimeUnit(j, k);
				}
			}
			// Convert frequency value for different frequencies into seconds
			timeUnitValue = freqTimeUnitArr[j];
			if (j === 0) {
				freqComm = ($scope.profile.comm === 0) ? null : $scope.profile.comm * multiplier[timeUnitValue];
			} else if (j === 1) {
				freqGps	= ($scope.profile.gps === 0) ? null : $scope.profile.gps * multiplier[timeUnitValue];
			} else if (j === 2) {
				freqAttrFreq = ($scope.profile.attrFreq === 0) ? null : $scope.profile.attrFreq * multiplier[timeUnitValue];
			}
        }
		
		if($scope.profile.comm >= 1){
			if(freqComm>frequencyMax.receiveComFreqLimit){
				$scope.commFrqErrorMsg = "Send sensor data Frequency cannot be more than " + convertMaxFreq(frequencyMax.receiveComFreqLimit,freqTimeUnitArr[0]) ;
				isShowValidationMsg=true;
			}
		}else{
			if($localStorage.systemdemo === false){
				$scope.commFrqErrorMsg = "Send sensor data Frequency cannot be less than 1";
				isShowValidationMsg = true;
			}
		}
		
		if ($scope.profile.gps >= 1) {
			if(freqGps>frequencyMax.gpsComFreqLimit){
				$scope.gpsFrqErrorMsg = "Send GPS location Frequency cannot be more than " + convertMaxFreq(frequencyMax.gpsComFreqLimit,freqTimeUnitArr[1]);
				isShowValidationMsg=true;
			}
		}else{
			if($localStorage.systemdemo === false){
				$scope.gpsFrqErrorMsg = "Send GPS location Frequency cannot be less than 1";
				isShowValidationMsg = true;
			}
		}
		
		if ($scope.profile.attrFreq >= 1) {
			if(freqAttrFreq>frequencyMax.sendComFreqLimit){
				$scope.attrFrqErrorMsg = "Capture sensor values Frequency cannot be more than " + convertMaxFreq(frequencyMax.sendComFreqLimit,freqTimeUnitArr[2]);;
				isShowValidationMsg=true;
			}
		}else{
			if($localStorage.systemdemo === false){
				$scope.attrFrqErrorMsg = "Capture sensor values Frequency cannot be less than 1";
				isShowValidationMsg = true;
			}
		}
		    
        $scope.errorMessageProfileLocation = [];
		//var isHomeLocationChecked=false;
        var pendingRequestsCount=0;
        angular.forEach($scope.profile.geopointsData, function (value, key) {
            geoAddress = (value.address === '' || value.address === undefined) ? '' : value.address;
			geoLat = (value.latitude === '' || value.latitude === undefined) ? '' : value.latitude;
			geoLng = (value.longitude === '' || value.longitude === undefined) ? '' : value.longitude;
            geopoint = (value.pointName === '' || value.pointName === undefined) ? '' : value.pointName;
            radius = (value.radius === '' || value.radius === undefined) ? '' : value.radius;
            /*Check the validation if lan & Lng value is not empty then pointname & radius field is mandatory*/
            if (geoLat !== '' && geoLng !== '' && geopoint === '') {
				$scope.errorMessageProfileLocation[key] = "Point name value is required";
				isShowValidationMsg = true;
			} else if (geoLat !== '' && geoLng !== '' && radius === '') {
				$scope.errorMessageProfileLocation[key] = "Radius value is required";
				isShowValidationMsg = true;
			} else if (isNaN(radius)) {
                $scope.errorMessageProfileLocation[key] = "Radius value should be an integer";
				isShowValidationMsg = true;
            } else if (isNaN(geoLat)) {
                $scope.errorMessageProfileLocation[key] = "Latitude value should be an integer";
				isShowValidationMsg = true;
            } else if (isNaN(geoLng)) {
                $scope.errorMessageProfileLocation[key] = "Longitude value should be an integer";
				isShowValidationMsg = true;
            } else {
                if (geoAddress !== '' && geoLat !== '' && geoLng !== '') {
                	pendingRequestsCount=pendingRequestsCount + 1;
                	var streetno='';
                	var address1='';
    				var address2='';
    				var city='';
    				var state='';
    				var zipcode='';
    				var country='';
    		        var loc = UTF8.encode(geoAddress);
                	var resObj = googleAPIService.getLatlngByAddress(loc);
                    resObj.then(function (response) {
                    	if (response.data.results.length > 0) {
                    		var address_component = response.data.results[0].address_components;
    						angular.forEach(address_component, function (value, key) {
    							if (value.types[0] == "street_number"){
    								streetno = value.short_name;
    						    }
    							if (value.types[0] == "route"){
    								address1 = value.short_name;
    						    }
    							if (value.types[0] == "locality"){
    								city = value.short_name;
    						    }
    							if (value.types[0] == "administrative_area_level_1"){
    								state = value.short_name;
    						    }
    							if (value.types[0] == "postal_code"){
    								zipcode = value.short_name;
    						    }
    							if (value.types[0] == "country"){
    								country = value.short_name;
    						    }
    	    				});
    						
    						geoLocationJson.push({
								"address": value.address,
								"latitude": value.latitude,
								"longitude": value.longitude,
								"pointName": value.pointName,
								"radius": value.radius,
								"pointNumber": value.pointNumber,
								"locationType": value.locationType,
								"addr1": streetno !== '' ? streetno+" "+address1 : address1,
								"addr2": address2,
								"city": city,
								"state": state,
								"zip": zipcode,
								"country": country
							});
    						if(pendingRequestsCount===geoLocationJson.length){
    			            	validateGeoOrgDest();
    			            }
    					} else {
            				$scope.errorMessageProfileLocation[key] = "Please provide valid address or latitude and longitude value";
            				isShowValidationMsg = true;
            			}
            		}, function (error) {
            				$scope.errorMessageProfileLocation[key] = "Please provide valid address or latitude and longitude value";
            				isShowValidationMsg = true;
            		});
                } else {
                	if (isShowValidationMsg) {
						return false;
					}else{
						if(pendingRequestsCount===0 && key===$scope.profile.geopointsData.length-1){
			            	validateGeoOrgDest();
			            }
					}
                }
            }
        });
        
        function validateGeoOrgDest(){
        	if(geoLocationJson !== undefined && geoLocationJson !== null && geoLocationJson.length>1){
    			for (i = 0; i < geoLocationJson.length; i++) {
    			     for (j = i + 1 ; j < geoLocationJson.length; j++) {
    			          if (geoLocationJson[i].locationType === geoLocationJson[j].locationType) {
    						  if(geoLocationJson[i].locationType === 1){
    							  $scope.errorMessageProfileLocation[i] = "Origin can't be more than one!";
    							  isShowValidationMsg = true;
    						  }else if(geoLocationJson[i].locationType === 2){
    							  $scope.errorMessageProfileLocation[i] = "Destination can't be more than one!";
    							  isShowValidationMsg = true;
    						  }
    			          }
    			     }
    			 }
    		}
    		 /*  isShowValidationMsg : true, validation is false. Show error message. */
    		if (isShowValidationMsg) {
    			return false;
    		} else {
    			updateProfile(deviceAttributeJson, geoLocationAlertsJson, geoLocationJson, freqComm, freqGps, freqAttrFreq, freqTimeUnitArr[0], freqTimeUnitArr[1], freqTimeUnitArr[2]);
    		}
        }
	};
	
	function updateProfile(deviceAttributeJson, geoLocationAlertsJson, geoLocationJson, freqComm, freqGps, freqAttrFreq, freqTimeUnitArr0, freqTimeUnitArr1, freqTimeUnitArr2){
		var reqObject = {
            "profile": {
                "profileId": $scope.profile.profileId,
                "name": $scope.profile.profileName,
                "description": $scope.profile.profileDesc,
                "orgId": $localStorage.orgId,
                "groupId": $localStorage.groupId,
                "deviceTypeId": $scope.profile.deviceTypeId,
                "attrType": deviceAttributeJson,
                "additionalAlerts":geoLocationAlertsJson,
                "geopoints": geoLocationJson,
                "commFrequency": freqComm,
                "gpsFrequency": freqGps,
                "attbFrequency": freqAttrFreq,
				"commFrequencyUnit": freqTimeUnitArr0,
				"gpsFrequencyUnit": freqTimeUnitArr1,
				"attbFrequencyUnit": freqTimeUnitArr2
            }
        };
		/* Frequency */
		
        createProfileService.modifyProfile.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $state.go('app.profile');
            } else {
                $scope.errorMessage = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
	}
	
	function addDefaultAttribute(attributeArrays){
		var reqObject = '';
		var groupIds = concatGroupIds(attributeArrays);
		for (j = 0; j < $scope.alertList.length; j++) {
			if($scope.alertList[j].alertCode == '3052'){
				reqObject = {
					"name": $scope.alertList[j].attrName,
					"alertGroupIds": groupIds,
					"alertCode": $scope.alertList[j].alertCode,
					"alertDetailMsg": $scope.alertList[j].alertDetailMsg,
					"emailIds":[]
			    }
			}
		}
		return reqObject;
	}
	
	function concatGroupIds(attributeArrays){
		var groupIds = '';
		if(attributeArrays.length === 1){
			groupIds = attributeArrays[0].alertGroupIds;
		}else{
			for (i = 0; i < attributeArrays.length; i++) {
			     for (j = i + 1 ; j < attributeArrays.length; j++) {
			    	 if(attributeArrays[i].alertGroupIds !== undefined && attributeArrays[j].alertGroupIds !== undefined){
			    		 groupIds = attributeArrays[i].alertGroupIds.concat(attributeArrays[j].alertGroupIds);
			    	 }else{
			    		 groupIds = attributeArrays[i].alertGroupIds;
			    	 }
			     }
			 }
		}
		
		return groupIds;
	}
}

modifyProfileCtrl.$inject = ['$scope', '$state', '$rootScope', 'createProfileService', 'profileListService', 'sharedDataService', '$localStorage', 'googleAPIService', 'DeviceTypeService', 'AlertGroupService', 'AlertService'];
