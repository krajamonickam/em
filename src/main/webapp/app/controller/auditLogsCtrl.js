angular.module('rfxcelApp').controller('auditLogsCtrl', auditLogsCtrl);
auditLogsCtrl.$inject = ['$scope', '$state','sharedDataService', 'AuditLogService','$localStorage'];

function auditLogsCtrl($scope, $state,sharedDataService,AuditLogService,$localStorage) {
	
	getAuditLogs();
	$scope.noLogsFound=''; 
	
	$scope.getAuditLogs = function(){
		getAuditLogs();
	}
	
    function getAuditLogs(){
        var reqObject = {
		   "orgId": $localStorage.orgId,
		   "searchString":$scope.deviceLog
        }

        AuditLogService.getAuditLogs().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.auditLogs = response.auditLogs;
                if($scope.auditLogs.length==0){
                	if($localStorage.fetchESData === true){
                		$scope.noLogsFound=response.responseMessage;
                	}else{
                		$scope.noLogsFound='No Audit logs Available';
                	}
                } else {
                	$scope.noLogsFound="";
                }
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
}
