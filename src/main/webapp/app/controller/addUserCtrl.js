angular.module('rfxcelApp').controller('addUserCtrl', addUserCtrl);
addUserCtrl.$inject = ['$scope', '$state', 'UserService', '$http', '$localStorage','sharedDataService','$location', 'validationService','OrgService','$sessionStorage'];

function addUserCtrl($scope, $state, UserService, $http, $localStorage,sharedDataService,$location, validationService,OrgService,$sessionStorage) {
    var selectedTimeZone='';
    var selectedOrgId=$sessionStorage.selectedOrgId;
	$scope.timeZoneList = [];
	getTimeZoneList();
	function getTimeZoneList(){
		 var reqObject = {
			"orgId": $localStorage.orgId 
	     };

	 OrgService.getTimeZones.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.timeZoneList = response.timeZoneList;

            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            $scope.message = error.data.responseMessage;
        });
	}
	
    $scope.user = {
        name: '',
        userLogin: '',
        email: '',
        phone: '',
        userTz: '',
        tzId:''
    };
    
    $scope.changeTimeZone=function(timeZoneId){
        selectedTimeZone=timeZoneId;
    }
	$scope.addUser = function () {
    	
		var isShowValidationMsg = false;
		var userLogin = $scope.user.userLogin;
		$scope.userLoginErrorMsg = "";
		$scope.userNameErrorMsg = "";
		$scope.emailErrorMsg = "";
		$scope.phoneErrorMsg = "";
		
		if ($scope.user.userLogin === "") {
			$scope.userLoginErrorMsg = "Expecting a login ID";
			isShowValidationMsg = true;
		}
		if ($scope.user.name === "") {
			$scope.userNameErrorMsg = "Expecting a user name";
			isShowValidationMsg = true;
		}
		if ($scope.user.email === "") {
			$scope.emailErrorMsg = "Expecting an email address";
			isShowValidationMsg = true;
		}
		/*if ($scope.user.phone === "") {
			$scope.phoneErrorMsg = "Expecting a phone number";
			isShowValidationMsg = true;
		}*/
		
		// UserLogin validation
		isValidUserLogin = validationService.isValidUserLogin($scope.user.userLogin);
		if (isValidUserLogin.status) {
			isShowValidationMsg = true;
			$scope.userLoginErrorMsg = isValidUserLogin.message;
		}
		
		// Email validation
		isValidEmail = validationService.isValidEmail($scope.user.email);
		if (isValidEmail.status) {
			isShowValidationMsg = true;
			$scope.emailErrorMsg = isValidEmail.message;
		}
		
		// Phone validation
		isValidPhone = validationService.isValidPhone($scope.user.phone);
		if (!isValidPhone.status) {
			isShowValidationMsg = true;
			$scope.phoneErrorMsg = isValidPhone.message;
		}

		/*  isShowValidationMsg : true, validation is false. Show error message. */
		if (isShowValidationMsg) {
			return false;
		}
		
    	var tmpUserType = 2 ; // 0-System, 1-Admin, 2-Normal
    	if ($localStorage.userType == 0 && selectedOrgId == 0) {
    		tmpUserType = 3;
    	} else if($localStorage.userType == 0 && selectedOrgId != 0) {
    		tmpUserType = 1;
    	} else if($localStorage.userType == 1) {
    		tmpUserType = 2;
    	}
        
        var forgotPaswdurl= $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/#/resetPassword';
        
        var reqObject = {
            "user": {
                "name": $scope.user.name,
                "userLogin": $scope.user.userLogin,
                "email": $scope.user.email,
                "phone": $scope.user.phone,
                "userType":tmpUserType,
                "orgId": selectedOrgId,
                "tzId": selectedTimeZone
            },
            "url": forgotPaswdurl
        };
        UserService.addUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
	};
	$scope.cancel = function () {
		$state.go('app.user');
    };
}
