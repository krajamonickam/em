angular.module('rfxcelApp').controller('mapPathParentController', mapPathParentController);

mapPathParentController.$inject = ['$scope', '$state', '$rootScope', '$filter', 'mapDataService', 'productAttributeService', 'sharedDataService', 'productAlertService', '$http', 'googleAPIService', 'commonConfigService', '$localStorage', 'generalUtilityService'];

function mapPathParentController($scope, $state, $rootScope, $filter, mapDataService, productAttributeService, sharedDataService, productAlertService, $http, googleAPIService, commonConfigService, $localStorage, generalUtilityService) {
	if(sharedDataService.selectedShipment() === undefined || sharedDataService.getSelectedProductId() === undefined){
		var lastState = sharedDataService.getLastVisitedUIState();
        sharedDataService.setLastVisitedUIState($state.current.name);
        $state.go('app.dashboard');
        return;
	}
	$scope.mapInfo = false;

	
    var locationCoordinates;
    var loadSelectedShipmentCountInterval,
    refreshSelectedShipmentCountInterval,
    refreshShipmentCountInterval,
    latestDataPointsRTS;

	getAlertsCount();
	
    function getNearestLocation(lat, lng) {
        var reqObj = googleAPIService.getNearestLocation(lat, lng);
        reqObj.then(function (response) {
            /*$scope.nearestLocation = response.data.results[0].formatted_address;*/
			$scope.nearestLocation = (response.data.status !== "ZERO_RESULTS") ? response.data.results[0].formatted_address : 'N.A';
            //$scope.nearestLocation = (response.data.results.length > 0) ? response.data.results[0].formatted_address : '';
        }, function (error) {
            console.log(error);
        });
    }
    function onMapDataPointSelected(event, location, isLastDataPoint, nearestLocation) {
        var productLocationArr = location.split(',');
        $scope.productLocationLat = parseFloat(productLocationArr[0]).toFixed(5);
        $scope.productLocationLng = parseFloat(productLocationArr[1]).toFixed(5);
        /*$scope.productLocationCoordinates = (sharedDataService.selectedShipment().lat === undefined) ? 'N.A' : locationCoordinates;*/
        locationCoordinates = $scope.productLocationLat + ', ' + $scope.productLocationLng;
        $scope.productLocationCoordinates = (productLocationArr[0] === undefined) ? 'N.A' : locationCoordinates;
        getNearestLocation($scope.productLocationLat, $scope.productLocationLng);
        if (isLastDataPoint === undefined) {
            $scope.$apply();
        }
    }
    function onSelectedProductPackageId(event, packageId, productId) {
        $scope.packageId = packageId;
        $scope.productId = productId;
    }
    function onSelectedProductDeviceId(event, deviceId) {
        $scope.deviceId = deviceId;
        $scope.productId = sharedDataService.getSelectedProductId();
    }
    function onNearestLocation(event, nearestLocation) {
        getNearestLocation($scope.productLocationLat, $scope.productLocationLng);
    }
    $scope.$on('mapDataPointSelected', onMapDataPointSelected);
    $scope.$on('selectedProductPackageId', onSelectedProductPackageId);
    $scope.$on('selectedProductDeviceId', onSelectedProductDeviceId);
    $scope.$on('nearestLocation', onNearestLocation);
    $scope.$on('locationForBottomPanel', function (event,data){
    	latestDataPointsRTS = data;
    	if(latestDataPointsRTS !== undefined && ($scope.selectedItemType.typeId == 4 || $scope.selectedItemType.typeId == 5)){
        	$scope.productLocationLat = parseFloat(latestDataPointsRTS.lat).toFixed(5);
            $scope.productLocationLng = parseFloat(latestDataPointsRTS.lang).toFixed(5);
        }
    	locationCoordinates = $scope.productLocationLat + ', ' + $scope.productLocationLng;
        $scope.productLocationCoordinates = locationCoordinates;
        getNearestLocation($scope.productLocationLat, $scope.productLocationLng);
        /*$scope.$apply();*/
    });
    $scope.traceId = sharedDataService.getTraceId();
    $scope.packageId = sharedDataService.selectedShipment().packageId;
    $scope.deviceId = sharedDataService.selectedShipment().deviceId;
    $scope.productId = sharedDataService.getSelectedProductId();
    $scope.productLocationLat = parseFloat(sharedDataService.selectedShipment().lat).toFixed(5);
    $scope.productLocationLng = parseFloat(sharedDataService.selectedShipment().lang).toFixed(5);
    $scope.selectedItemType = sharedDataService.getSelectedShipmentType();
    
    /*var locationCoordinates = parseFloat(sharedDataService.selectedShipment().lat).toFixed(5) + ', ' + parseFloat(sharedDataService.selectedShipment().lang).toFixed(5);*/
    /*$scope.productLocationCoordinates = (sharedDataService.selectedShipment().lat === undefined) ? 'N.A' : locationCoordinates;*/
    locationCoordinates = $scope.productLocationLat + ', ' + $scope.productLocationLng; 
    $scope.productLocationCoordinates = (sharedDataService.selectedShipment().lat === undefined) ? 'N.A' : locationCoordinates;
    var date = new Date(),
        currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss'),
        reqObject = {
            "dateTime": currentDate,
			"orgId": $localStorage.orgId,
            "deviceData": {
                "packageId": sharedDataService.selectedShipment().packageId,
                "deviceId" : sharedDataService.selectedShipment().deviceId,
                "productId": sharedDataService.getSelectedProductId(),
                "showMapAttribute": sharedDataService.getShowMapAttribute(),
                "rtsItemId": sharedDataService.getRTSTraceId(),
                "rtsSerialNumber": sharedDataService.getRTSSerialNumber()
            }
        };
    
    productAttributeService.post(reqObject).$promise.then(function (response) {
        if (response.responseStatus === "error" && response.responseCode === 403) {
            $scope.$emit('genericErrorEvent', response);
            return;
        }
        if (response.responseStatus == "success") {
        	//alert('mappath');
        	$scope.mapInfo = true;
        	$scope.traceId = response.traceId;
        	sharedDataService.setTraceId(response.traceId);
        	sharedDataService.setSelectedDeviceId(response.deviceId);

        	if(response.sensor.statusTime!= null){
				$scope.productLastUpdateTime = generalUtilityService.unixTimeToDateTime(response.sensor.statusTime);
        	}else{
        		$scope.productLastUpdateTime = 'N.A';
        	}
        	
			
			
        	if(response.sensor.attributeList[0] !== undefined) {
        		//$scope.isColorRedAttr1 = (response.sensor.attributeList[0].excursion) ? true : false;
        		var minValue = response.sensor.attributeList[0].min;
        		var maxValue = response.sensor.attributeList[0].max;
        		var rangeValue = response.sensor.attributeList[0].attrValue;
        		var alertNotify = response.sensor.attributeList[0].alert;
        		if(alertNotify === false){
        			$scope.isColorRedAttr1 = true;
        		} else {
        			if(minValue !== undefined && maxValue !== undefined){
            			$scope.isColorRedAttr1 = (rangeValue >= minValue && rangeValue <= maxValue);
            		} else {
            			$scope.isColorRedAttr1 = true;
            		}
        		}
				$scope.productAttrLegend1 = response.sensor.attributeList[0].legend;
				
    			if (response.sensor.attributeList[0].attrValue === commonConfigService.getNullAttrResponseCode()) {
    				$scope.productAttrVal1 = 'N.A';
    				$scope.productAttrUnit1 = '';
    				$scope.isColorRedAttr1 = true;
    			} else {
    				$scope.productAttrVal1 = response.sensor.attributeList[0].attrValue;
    				$scope.productAttrUnit1 = response.sensor.attributeList[0].unit
    			}
    		}
    		
    		if(response.sensor.attributeList[1] !== undefined) {
    			//$scope.isColorRedAttr2 = (response.sensor.attributeList[1].excursion) ? true : false;
				var minValue = response.sensor.attributeList[1].min;
        		var maxValue = response.sensor.attributeList[1].max;
        		var rangeValue = response.sensor.attributeList[1].attrValue;
        		var alertNotify = response.sensor.attributeList[1].alert;
        		if(alertNotify === false) {
        			$scope.isColorRedAttr2 = true;
        		} else {
        			if(minValue !== undefined && maxValue !== undefined){
            			$scope.isColorRedAttr2 = (rangeValue >= minValue && rangeValue <= maxValue);
            		}else{
            			$scope.isColorRedAttr2 = true;
            		}
        		}
				$scope.productAttrLegend2 = response.sensor.attributeList[1].legend;
    			
    			if (response.sensor.attributeList[1].attrValue === commonConfigService.getNullAttrResponseCode()) {
    				$scope.productAttrVal2 = 'N.A';
    				$scope.productAttrUnit2 = '';
    				$scope.isColorRedAttr2 = true;
    			} else {
    				$scope.productAttrVal2 = response.sensor.attributeList[1].attrValue;
    				$scope.productAttrUnit2 = response.sensor.attributeList[1].unit;
    			}
    		}
        }

        /*if (response.sensor.attributeList[0].attrValue === commonConfigService.getNullAttrResponseCode()) {
            $scope.productAttrVal1 = 'N.A';
            $scope.productAttrUnit1 = '';
        } else {
            $scope.productAttrVal1 = response.sensor.attributeList[0].attrValue;
            $scope.productAttrUnit1 = response.sensor.attributeList[0].unit
        }*/

        /*if (response.sensor.attributeList[1].attrValue === commonConfigService.getNullAttrResponseCode()) {
            $scope.productAttrVal2 = 'N.A';
            $scope.productAttrUnit2 = '';
        } else {
            $scope.productAttrVal2 = response.sensor.attributeList[1].attrValue;
            $scope.productAttrUnit2 = response.sensor.attributeList[1].unit;
        }*/
        /*$scope.productAttrLegend1 = response.sensor.attributeList[0].legend;*/
        /*$scope.productAttrLegend2 = response.sensor.attributeList[1].legend;*/

    }, function (error) {
        console.log(error);
    });
    
    function getAlertsCount(){
    	var date = new Date();
  	    var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
  	    var reqCountObject = {
  	    	"dateTime": currentDate,
  	    	"searchType": "container",
			"orgId": $localStorage.orgId,
  	    	"deviceData":{
  	    		"deviceId":  sharedDataService.selectedShipment().deviceId,
  	    		"packageId": sharedDataService.selectedShipment().packageId,
  	    		"productId": sharedDataService.getSelectedProductId()
			}
  		  }
  	   productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function(response){
           if (response.responseStatus === "error" && response.responseCode === 403) {
        	   $scope.$emit('genericErrorEvent', response);
               return;
           }     
           if (response.responseStatus == "success") {
        	   $rootScope.notificationCount = response.count;
           }
      	}, function(error){
	    	console.log(error);
	    })
    }
	
	function getAlertsTotalCount() {
		var date = new Date();
		var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
		var reqCountObject = {
			"dateTime": currentDate,
			"searchType": "All",
			"orgId": $localStorage.orgId
		}
		productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			if (response.responseStatus == "success") {
				$rootScope.allNotificationCount = response.count;
			}
		}, function (error) {
			console.log(error);
		})
	}
	
		
    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.dashboard.mapMarkerPath') {
            clearTimeout(refreshSelectedShipmentCountInterval);
            clearTimeout(refreshShipmentCountInterval);
        }
    });
    
    loadSelectedShipmentCountInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
    refreshSelectedShipmentCountInterval = setInterval(getAlertsCount, loadSelectedShipmentCountInterval);
    refreshShipmentCountInterval = setInterval(getAlertsTotalCount, loadSelectedShipmentCountInterval);
}
