/*jshint -W065 */
/*jslint plusplus: true */
/*jslint devel: true */
angular.module('rfxcelApp').controller('createProfileCtrl', createProfileCtrl);
function createProfileCtrl($scope, $state, $rootScope, createProfileService, profileService, profileListService, $localStorage, googleAPIService, DeviceTypeService, AlertGroupService, AlertService) {
	var freqComm,
		freqGps,
		freqAttrFreq,
		freqTimeUnitArr = [],
		frequencyMax={};

	/* Variables to hold error messages */
	$scope.profiledeviceTypeErrorMsg = "";
	$scope.profileNameErrorMsg = "";
	$scope.profileDescErrorMsg = "";
	$scope.commFrqErrorMsg = "";
	$scope.gpsFrqErrorMsg = "";
	$scope.attrFrqErrorMsg = "";
	$scope.alertList = "";
	//$scope.selectedAlertGroup = [1];
	/* Right side attribute section must be hidden untill the device type is selected */
	$scope.isDeviceTypeSelcted = false;
	/* Device attribute array*/
	$scope.deviceAttributes = [];
	/* Geo location data array*/
	$scope.geopointsData = [];
	/* Array to store error messages for different attributes */
	$scope.errorMessageProfileAttr = [];
	/* To store the geo location value*/
	$scope.geolocationRadiusValue = '';
	/* To store the geo location format*/
	$scope.geolocationRadiusFormat = '';
	/* Add the geolocation row based on add button*/
	
	// Congiguration to show active time unit in frequency section. Default is minutes
	$scope.isActiveFrequency = [
		{timeUnit: [1, 0, 0]},
		{timeUnit: [1, 0, 0]},
		{timeUnit: [1, 0, 0]}
	];
	
	$scope.frequencyTimeUnit = function (frequencyType, timeUnitValue) {
		freqTimeUnitArr[frequencyType] = timeUnitValue;
		for (i = 0; i < 3; i++) {
			if (i === timeUnitValue) {
				$scope.isActiveFrequency[frequencyType].timeUnit[i] = 1;
			} else {
				$scope.isActiveFrequency[frequencyType].timeUnit[i] = 0;
			}
		}
	}
    
    $scope.onChangeSliderFn = function(index,min,rangeMaxVal){
        //$scope.attributeMaxChange(rangeMaxVal,index);
    }
    
    /*$scope.attributeMaxChange=function(rangeMaxVal,index){
        if(rangeMaxVal > 0){
            $scope.deviceAttributes[index].isAlertActive=true;
			$scope.deviceAttributes[index].isNotificationActive=true;
        }else{
            $scope.deviceAttributes[index].isAlertActive=false;
			$scope.deviceAttributes[index].isNotificationActive=false;
        }
        
    }*/
	
	/* Create the profile object*/
	$scope.profile = {
		profileName: '',
		profileDesc: '',
		deviceType: '',
		min: '',
		max: '',
		comm: 15,
		gps: 15,
		attrFreq: 15
	};
	
	/* Get the attributes based on orgId, groupId and deviceId */
	$scope.getAttributesByDeviceType = function (deviceId) {
		$scope.profile.deviceTypeId = deviceId;
		if (deviceId === '' || deviceId === undefined) {
			$scope.isDeviceTypeSelcted = false;
			return;
		}

		var eachSlider,
			reqdevice = {
				"orgId": $localStorage.orgId,
				"deviceDetail": {
					"deviceTypeId": deviceId
				}
			};

		$scope.slider = [];
		DeviceTypeService.getAttributesByDeviceType().save(JSON.stringify(reqdevice)).$promise.then(function (response) {
			
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			$scope.deviceAttributes = response.sensor.attributeList;

			for (i = 0; i < $scope.deviceAttributes.length; i++) {
				$scope.deviceAttributes[i].min = response.sensor.attributeList[i].rangeMin;
				$scope.deviceAttributes[i].max = response.sensor.attributeList[i].rangeMin;
                $scope.deviceAttributes[i].isAlertActive=true;
                    
				eachSlider = {
					minValue: response.sensor.attributeList[i].rangeMin,
					maxValue: response.sensor.attributeList[i].rangeMax,
					options: {
						noSwitching: true,
						floor: response.sensor.attributeList[i].rangeMin,
						ceil: response.sensor.attributeList[i].rangeMax,
						step: 1,
						precision: 2,
						enforceStep: false,
						showOuterSelectionBars: true,
						getSelectionBarColor: function (value) {
							return '#58B820';
						},
						getPointerColor: function (value) {
							return '#07C';
						},
                        onChange : $scope.onChangeSliderFn,
                        id: i
					}
				};
				$scope.slider.push(eachSlider);
				if($rootScope.newProfileScreen === true){
					for (j = 0; j < $scope.alertList.length; j++) {
						if($scope.alertList[j].attrName == $scope.deviceAttributes[i].attrName){
							$scope.deviceAttributes[i].alertDetailMsg = $scope.alertList[j].alertDetailMsg;
							$scope.deviceAttributes[i].alertCode = $scope.alertList[j].alertCode;
						}
					}
				}
			}
			
			if($rootScope.newProfileScreen === true){
				for (j = 0; j < $scope.alertList.length; j++) {
					if($scope.alertList[j].alertCode == '3701'){
						$scope.locationEntryAlertMsg = $scope.alertList[j].alertDetailMsg;
						$scope.locationEntryAlertCode = $scope.alertList[j].alertCode;
						$scope.locationEntryAlertName = $scope.alertList[j].alertMsg;
					}
					if($scope.alertList[j].alertCode == '3702'){
						$scope.locationExitAlertMsg = $scope.alertList[j].alertDetailMsg;
						$scope.locationExitAlertCode = $scope.alertList[j].alertCode;
						$scope.locationExitAlertName = $scope.alertList[j].alertMsg;
					}
				}
			}
		}, function (error) {
			console.log(error);
		});

		/* Add a new profile show one location with empty text value */
		$scope.geopointsData = [{
			"address": "",
			"latitude": "",
			"longitude": "",
			"radius": $scope.geolocationRadiusValue,
			"pointNumber": "",
            "locationType": 1
		}];
		
		$scope.isDeviceTypeSelcted = true;
		$scope.profileNameErrorMsg = "";
		$scope.profileDescErrorMsg = "";
	};
	
    function getAlertGroups() {
        var reqObj = {
        		"alertGroup": {
        			 "orgId": $localStorage.orgId
                }
        }

        AlertGroupService.getAlertGroup().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.alertGroupList = response.alertGroupList;
            } else {
                $scope.message = response.responseMessage;
            }

        }, function (error) {
            console.log(error);
        });
    }
    
    function getDefaultAlertList() {
        var reqObject = {
            "alert": {
            	"orgId": $localStorage.orgId
            }
        }

        AlertService.getDefaultAlertList().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.alertList = response.alertList;
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
    
    $scope.alertGroupChanged=function(alertObj){
    }
	
	$scope.increase = function (param) {
        if (param === 'profilecomm') {
            $scope.profile.comm = $scope.profile.comm + 1;
        } else if (param === 'profilegps') {
            $scope.profile.gps = $scope.profile.gps + 1;
        } else {
            $scope.profile.attrFreq = $scope.profile.attrFreq + 1;
        }
	};
    
    $scope.decrease = function (param) {
		if (param === 'profilecomm' && $scope.profile.comm > 1) {
			$scope.profile.comm = $scope.profile.comm - 1;
		} else if (param === 'profilegps' && $scope.profile.gps > 1) {
			$scope.profile.gps = $scope.profile.gps - 1;
		} else if (param === 'attrFreq' && $scope.profile.attrFreq > 1) {
			$scope.profile.attrFreq = $scope.profile.attrFreq - 1;
		}
    };
    
    $scope.addGeopoints = function () {
		$scope.geopointsData.push({
			"address": "",
			"latitude": "",
			"longitude": "",
			"radius": $scope.geolocationRadiusValue,
			"pointNumber": "",
            "locationType": $scope.geopointsData.length===1?2:3
		});
	};
    
    /* Remove the geolocation row based on remove button*/
	$scope.removeGeopoints = function (index) {
		
		if ($scope.geopointsData.length !== 1) {
			$scope.geopointsData.splice(index, 1);
		} else {
			$scope.geopointsData = [];
	        $scope.geopointsData = [{
				"address": "",
				"latitude": "",
				"longitude": "",
				"radius": $scope.geolocationRadiusValue,
				"pointNumber": "",
				"locationType": 3
			}];
		}
	};
	
	/*$scope.homeLocationChange=function(index){
        for(var i in $scope.geopointsData){
            if(i==index){
            	$scope.geopointsData[i].isHome=1;
            }else{
            	$scope.geopointsData[i].isHome=0;
            }
          }		
    }*/
    
    /* Set Max Frequency Limit Object */
	$scope.setFrequency=function(deviceTypeId){
		for(var i in $scope.deviceList){
			if($scope.deviceList[i].deviceTypeId==deviceTypeId){
				frequencyMax.gpsComFreqLimit=$scope.deviceList[i].gpsComFreqLimit;
				frequencyMax.receiveComFreqLimit=$scope.deviceList[i].receiveComFreqLimit;
				frequencyMax.sendComFreqLimit=$scope.deviceList[i].sendComFreqLimit;
				break;
			}
		}
		
	}
	
	/* Convert Time into minutes/days/hours and show max 
	 * limit error messages */
	function convertMaxFreq(maxFrequency,freqTimeUnit){
		var freqLimit;
		if(freqTimeUnit==0){
			freqLimit= maxFrequency/60 + ' minutes';
		}else if(freqTimeUnit==1){
			freqLimit=maxFrequency/(60*60)  + ' hours';
		}else{
			freqLimit=maxFrequency/(24*60*60);
			freqLimit=(freqLimit==1? freqLimit + ' day':freqLimit + ' days') ;
		}
		return freqLimit;
	}
    
	/* Redirect to the profileList page based on action*/
	$scope.cancel = function () {
		$state.go('app.profile');
	};
	
   // Convert special char from location to ASCII
    var UTF8 = {
    		encode: function(s){
    			for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
    				s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
    			);
    			return s.join("");
    		},
    		decode: function(s){
    			for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
    				((a = s[i][c](0)) & 0x80) &&
    				(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
    				o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
    			);
    			return s.join("");
    		}
    	};


	/* Get the lat & lng based on address value */
	$scope.getLatlngByAddress = function (location, geopointsData, index) {
		/* Array to store error messages for different geolocation */
		$scope.errorMessageProfileLocation = [];

		if (location.address === '' || location.address === undefined) {
			return;
		}
	    var loc = UTF8.encode(location.address)     
        var resObj = googleAPIService.getLatlngByAddress(loc);
		resObj.then(function (response) {
			if (response.data.results.length > 0) {
				angular.forEach(geopointsData, function (value, key) {
					if (key === index) {
						geopointsData[key].address = location.address;
						geopointsData[key].latitude = response.data.results[0].geometry.location.lat;
						geopointsData[key].longitude = response.data.results[0].geometry.location.lng;
                        
					}
				});
				$scope.geopointsData = geopointsData;
			} else {
				$scope.errorMessageProfileLocation[index] = "Please provide valid address or latitude and longitude value";
			}
		}, function (error) {
			console.log(error);
		});
	};

	/* Get the address based on lat & lng value */
	$scope.getAddressByLatlng = function (location, geopointsData, index) {

		/* Array to store error messages for different geolocation */
		$scope.errorMessageProfileLocation = [];

		/*if (location.latitude === '' || location.latitude === undefined) {
			return;
		}
		if (location.longitude === '' || location.longitude === undefined) {
			return;
		}*/
		if (location.latitude === '' || location.latitude === undefined || location.longitude === '' || location.longitude === undefined) {
			return;
		}
		var resObj = googleAPIService.getNearestLocation(location.latitude, location.longitude);
		resObj.then(function (response) {
			if (response.data.results.length > 0) {
				angular.forEach(geopointsData, function (value, key) {
					if (key === index) {
						geopointsData[key].address = response.data.results[0].formatted_address;
						geopointsData[key].latitude = location.latitude;
						geopointsData[key].longitude = location.longitude;
                    }
				});
				$scope.geopointsData = geopointsData;
			} else {
				$scope.errorMessageProfileLocation[index] = "Please provide valid latitude and longitude value";
			}
		}, function (error) {
				$scope.errorMessageProfileLocation[index] = "Please provide valid latitude and longitude value";
		});
	};
    
   
	
	/* Get the deviceType based on orgId */
	function getDeviceType() {
		var reqObject = {
			"orgId": $localStorage.orgId,
			"searchType": "All"
		};
		
		
		DeviceTypeService.getDeviceTypes().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
				$scope.deviceList = response.sensor.deviceList;
            } else {
                $scope.message = response.responseMessage;
            }

        }, function (error) {
            console.log(error);
        });
	}
	
	/*Get the radius value and format for geo location*/
	function getGeolocationRadiusAndFormat() {
		var reqObject = {
			"orgId" : $localStorage.orgId,
			"configData": {
	            "params": ["geolocation.radius.value", "geolocation.radius.format"]
	        }
		};
		createProfileService.getGeolocationRadiusAndFormat.save(JSON.stringify(reqObject)).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			$scope.configValue = response.sensor.configMap;
			$scope.geolocationRadiusValue = $scope.configValue[reqObject.configData.params[0]];
			$scope.geolocationRadiusFormat = $scope.configValue[reqObject.configData.params[1]];
		}, function (error) {
			console.log(error);
		});
	}
	getDeviceType();
	getDefaultAlertList();
	getAlertGroups();
	getGeolocationRadiusAndFormat();
	/* Save the profile details */
	/* Save the profile details */
	$scope.createProfile = function () {
		var deviceAttributeJson = [],
			geoLocationJson = [],
			geoLocationAlertsJson = [],
			profileName = $scope.profile.profileName,
			profileDesc = $scope.profile.profileDesc,
			keycode,
			i,
			j,
			length,
			min,
			max,
			isShowValidationMsg = false,
			profileDescription = document.getElementById("profileDesc"),
			geoAddress,
			geoLat,
			geoLng,
            geopoint,
            radius,
			timeUnitValue,
			multiplier = [60, 3600, 86400]; // mins, hours, days. 
		
		$scope.profiledeviceTypeErrorMsg = "";
		$scope.profileNameErrorMsg = "";
		$scope.profileDescErrorMsg = "";
		$scope.commFrqErrorMsg = "";
		$scope.gpsFrqErrorMsg = "";
		$scope.attrFrqErrorMsg = "";

		if ("" === $scope.profile.deviceTypeId || "0" === $scope.profile.deviceTypeId || undefined === $scope.profile.deviceTypeId) {
			$scope.profiledeviceTypeErrorMsg = "Please provide Profile device type.";
			isShowValidationMsg = true;
		}
		if ("" === $scope.profile.profileName) {
			$scope.profileNameErrorMsg = "Please provide profile name.";
			isShowValidationMsg = true;
		}
		
		/* Check profile description length cannot exceed 250 character*/
		if (profileDescription.value.length > 250) {
			$scope.profileDescErrorMsg = "Description length cannot exceed 250 char";
			isShowValidationMsg = true;
		}
		
		
		
		$scope.errorMessageProfileAttr = [];
		$scope.errorMessageProfileAttrEmail = [];
		
		angular.forEach($scope.deviceAttributes, function (value, key) {
			
            min = (value.min === '' || value.min === null || value.min === undefined) ? '' :  parseFloat(value.min);
			max = (value.max=== '' || value.max === null || value.max === undefined) ? '' :  parseFloat(value.max);
			
			
			
			if (min === '' && max === '') {
				$scope.errorMessageProfileAttr[key] = "Min value is required";
				isShowValidationMsg = true;
			} else if (min === '' && max !== '') {
				$scope.errorMessageProfileAttr[key] = "Min value is required";
				isShowValidationMsg = true;
			} else if (isNaN(min)) {
				$scope.errorMessageProfileAttr[key] = "Min value should be an integer";
				isShowValidationMsg = true;
			} else if (max === '' && min !== '') {
				$scope.errorMessageProfileAttr[key] = "Max value is required";
				isShowValidationMsg = true;
			} else if (isNaN(max)) {
				$scope.errorMessageProfileAttr[key] = "Max value should be an integer";
				isShowValidationMsg = true;
			} else if (min === max && min !== '' && max !== '' && $scope.deviceAttributes[key].rangeMin !== min && (min!==0 && max!==0)) {
				$scope.errorMessageProfileAttr[key] = "Min & Max values cannot be same ";
				isShowValidationMsg = true;
			} else if (min !== '' && max !== '' && min > max) {
				$scope.errorMessageProfileAttr[key] = "Min cannot exceed Max";
				isShowValidationMsg = true;
			} else if (min < $scope.deviceAttributes[key].rangeMin) {
				$scope.errorMessageProfileAttr[key] = "Min value cannot be less than " + $scope.deviceAttributes[key].rangeMin;
				isShowValidationMsg = true;
			} else if (max > $scope.deviceAttributes[key].rangeMax) {
				$scope.errorMessageProfileAttr[key] = "Max value cannot be more than " + $scope.deviceAttributes[key].rangeMax;
				isShowValidationMsg = true;
			} else {
				// check if attribute value is change from default rangeMin value and then only save that aatribute
				if (value.min >= $scope.deviceAttributes[key].rangeMin && value.max > $scope.deviceAttributes[key].rangeMin) {
					$scope.errorMessageProfileAttr[key] = "";
					if($rootScope.newProfileScreen === true){
						var emailIds = [];
						var mailid = '';
						var mailformat = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
						if(value.emailIds !== undefined && value.emailIds !== null && value.emailIds !== ''){
							mailid = value.emailIds.split(',');
							for(i=0;i<mailid.length;i++){
								if(mailid[i].match(mailformat)){
									emailIds.push(mailid[i]);
								}else{
									$scope.errorMessageProfileAttrEmail[key] = 'Not a valid emailId!';
									isShowValidationMsg = true;
								}
							}
						}else{
							emailIds = [];
						}
						deviceAttributeJson.push({
							"name": value.attrName,
							"minValue": value.min,
							"maxValue": value.max,
							"alert": value.isAlertActive == false ? 1 : 2,
							"alertGroupIds": value.selectedAlertGroup,
							"alertCode": value.alertCode,
							"alertDetailMsg":value.alertDetailMsg,
							"emailIds":emailIds
						});
					}else{
						deviceAttributeJson.push({
							"name": value.attrName,
							"minValue": value.min,
							"maxValue": value.max,
							"alert": value.isAlertActive == false ? 1 : 2
						});
					}
				}
			} 
			/*else {
				$scope.errorMessageProfileAttr[key] = "";
				deviceAttributeJson.push({
					"name": value.attrName,
					"minValue": value.min,
					"maxValue": value.max
				});
			}*/
		});
		
		if($rootScope.newProfileScreen === true){
			var emailIds = [];
			var mailid = '';
			var mailformat = '[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$';
			if($scope.locationGroupEmailId !== undefined && $scope.locationGroupEmailId !== null && $scope.locationGroupEmailId !== ''){
				mailid = $scope.locationGroupEmailId.split(',');
				for(i=0;i<mailid.length;i++){
					if(mailid[i].match(mailformat)){
						emailIds.push(mailid[i]);
					}else{
						$scope.mailIdErrorMsg = 'Not a valid emailId!';
						return;
					}
				}
			}else{
				emailIds = [];
			}
			geoLocationAlertsJson.push({
				"name": $scope.locationExitAlertName,
				"alertGroupIds": $scope.selectedAlertGroup,
				"alertCode": $scope.locationEntryAlertCode,
				"alertDetailMsg": $scope.locationEntryAlertMsg,
				"emailIds":emailIds
			},{
				"name": $scope.locationEntryAlertName,
				"alertGroupIds": $scope.selectedAlertGroup,
				"alertCode": $scope.locationExitAlertCode,
				"alertDetailMsg": $scope.locationExitAlertMsg,
				"emailIds":emailIds
			});
			
			if(deviceAttributeJson.length>0){
				var defaultAttribute = addDefaultAttribute(deviceAttributeJson);
				geoLocationAlertsJson.push(defaultAttribute);
			}
		}
		
		for (j = 0; j < 3; j++) {
			// If minutes tab is selected
			if ($scope.isActiveFrequency[j].timeUnit[0] === 1) {
				// If user has not clicked on any time unit, call the default time unit i.e  minutes by passing 0 
				// as 2nd param to frequencyTimeUnit timeUnitValue
				$scope.frequencyTimeUnit(j, 0);
			}
		
			// Convert frequency value for different frequencies into seconds
			timeUnitValue = freqTimeUnitArr[j];
			if (j === 0) {
				freqComm = ($scope.profile.comm === 0) ? null : $scope.profile.comm * multiplier[timeUnitValue];
			} else if (j === 1) {
				freqGps	= ($scope.profile.gps === 0) ? null : $scope.profile.gps * multiplier[timeUnitValue];
			} else if (j === 2) {
				freqAttrFreq = ($scope.profile.attrFreq === 0) ? null : $scope.profile.attrFreq * multiplier[timeUnitValue];
			}
		}
		
		
		if($scope.profile.comm >= 1){
			if(freqComm>frequencyMax.receiveComFreqLimit){
				$scope.commFrqErrorMsg = "Send sensor data Frequency cannot be more than " + convertMaxFreq(frequencyMax.receiveComFreqLimit,freqTimeUnitArr[0]) ;
				isShowValidationMsg=true;
			}
		}else{
			if($localStorage.systemdemo === false){
				$scope.commFrqErrorMsg = "Send sensor data Frequency cannot be less than 1";
				isShowValidationMsg = true;
			}
		}
		
		if ($scope.profile.gps >= 1) {
			if(freqGps>frequencyMax.gpsComFreqLimit){
				$scope.gpsFrqErrorMsg = "Send GPS location Frequency cannot be more than " + convertMaxFreq(frequencyMax.gpsComFreqLimit,freqTimeUnitArr[1]);
				isShowValidationMsg=true;
			}
		}else{
			if($localStorage.systemdemo === false){
				$scope.gpsFrqErrorMsg = "Send GPS location Frequency cannot be less than 1";
				isShowValidationMsg = true;
			}
		}
		
		if ($scope.profile.attrFreq >= 1) {
			if(freqAttrFreq>frequencyMax.sendComFreqLimit){
				$scope.attrFrqErrorMsg = "Capture sensor values Frequency cannot be more than " + convertMaxFreq(frequencyMax.sendComFreqLimit,freqTimeUnitArr[2]);;
				isShowValidationMsg=true;
			}
		}else{
			if($localStorage.systemdemo === false){
				$scope.attrFrqErrorMsg = "Capture sensor values Frequency cannot be less than 1";
				isShowValidationMsg = true;
			}
		}
		
        $scope.errorMessageProfileLocation = [];
        //var isHomeLocationChecked=false;
		var pendingRequestsCount=0;
        angular.forEach($scope.geopointsData, function (value, key) {
            geoAddress = (value.address === '' || value.address === undefined) ? '' : value.address;
			geoLat = (value.latitude === '' || value.latitude === undefined) ? '' : value.latitude;
			geoLng = (value.longitude === '' || value.longitude === undefined) ? '' : value.longitude;
            geopoint = (value.pointName === '' || value.pointName === undefined) ? '' : value.pointName;
            radius = (value.radius === '' || value.radius === undefined) ? '' : value.radius;
            /*Check the validation if lan & Lng value is not empty then pointname & radius field is mandatory*/
            if (geoLat !== '' && geoLng !== '' && geopoint === '') {
				$scope.errorMessageProfileLocation[key] = "Point name value is required";
				isShowValidationMsg = true;
			} else if (geoLat !== '' && geoLng !== '' && radius === '') {
				$scope.errorMessageProfileLocation[key] = "Radius value is required";
				isShowValidationMsg = true;
			} else if (isNaN(radius)) {
                $scope.errorMessageProfileLocation[key] = "Radius value should be an integer";
				isShowValidationMsg = true;
            } else if (isNaN(geoLat)) {
                $scope.errorMessageProfileLocation[key] = "Latitude value should be an integer";
				isShowValidationMsg = true;
            } else if (isNaN(geoLng)) {
                $scope.errorMessageProfileLocation[key] = "Longitude value should be an integer";
				isShowValidationMsg = true;
            } else {
                if (geoAddress !== '' && geoLat !== '' && geoLng !== '') {
                	pendingRequestsCount=pendingRequestsCount + 1;
                	var streetno='';
                	var address1='';
    				var address2='';
    				var city='';
    				var state='';
    				var zipcode='';
    				var country='';
    				var loc = UTF8.encode(geoAddress);
                 	var resObj = googleAPIService.getLatlngByAddress(loc);
                    resObj.then(function (response) {
                    	if (response.data.results.length > 0) {
                    		var address_component = response.data.results[0].address_components;
    						angular.forEach(address_component, function (value, key) {
    							if (value.types[0] == "street_number"){
    								streetno = value.short_name;
    						    }
    							if (value.types[0] == "route"){
    								address1 = value.short_name;
    						    }
    							if (value.types[0] == "locality"){
    								city = value.short_name;
    						    }
    							if (value.types[0] == "administrative_area_level_1"){
    								state = value.short_name;
    						    }
    							if (value.types[0] == "postal_code"){
    								zipcode = value.short_name;
    						    }
    							if (value.types[0] == "country"){
    								country = value.short_name;
    						    }
    	    				});
    						
    						geoLocationJson.push({
								"address": value.address,
								"latitude": value.latitude,
								"longitude": value.longitude,
								"pointName": value.pointName,
								"radius": value.radius,
								"pointNumber": value.pointNumber,
								"locationType": value.locationType,
								"addr1": streetno !== '' ? streetno+" "+address1 : address1,
								"addr2": address2,
								"city": city,
								"state": state,
								"zip": zipcode,
								"country": country
							});
    						if(pendingRequestsCount===geoLocationJson.length){
    			            	validateGeoOrgDest();
    			            }
    					} else {
            				$scope.errorMessageProfileLocation[key] = "Please provide valid address or latitude and longitude value";
            				isShowValidationMsg = true;
            			}
            		}, function (error) {
            				$scope.errorMessageProfileLocation[key] = "Please provide valid address or latitude and longitude value";
            				isShowValidationMsg = true;
            		});
                } else {
                	if (isShowValidationMsg) {
						return false;
					}else{
						if(pendingRequestsCount===0 && key===$scope.geopointsData.length-1){
			            	validateGeoOrgDest();
			            }
					}
                }
            }
        });
        function validateGeoOrgDest(){
        	if(geoLocationJson !== undefined && geoLocationJson !== null && geoLocationJson.length>1){
    			for (i = 0; i < geoLocationJson.length; i++) {
    			     for (j = i + 1 ; j < geoLocationJson.length; j++) {
    			          if (geoLocationJson[i].locationType === geoLocationJson[j].locationType) {
    						  if(geoLocationJson[i].locationType === 1){
    							  $scope.errorMessageProfileLocation[i] = "Origin can't be more than one!";
    							  isShowValidationMsg = true;
    						  }else if(geoLocationJson[i].locationType === 2){
    							  $scope.errorMessageProfileLocation[i] = "Destination can't be more than one!";
    							  isShowValidationMsg = true;
    						  }
    			          }
    			     }
    			 }
    		}
    		 /*  isShowValidationMsg : true, validation is false. Show error message. */
    		if (isShowValidationMsg) {
    			return false;
    		} else {
    			saveProfile(deviceAttributeJson, geoLocationAlertsJson, geoLocationJson, freqComm, freqGps, freqAttrFreq, freqTimeUnitArr[0], freqTimeUnitArr[1], freqTimeUnitArr[2]);
    		}
        }
    }
	
	function saveProfile(deviceAttributeJson, geoLocationAlertsJson, geoLocationJson, freqComm, freqGps, freqAttrFreq, freqTimeUnitArr0, freqTimeUnitArr1, freqTimeUnitArr2){
		var reqObject = {
			"profile": {
				"name": $scope.profile.profileName,
				"description": $scope.profile.profileDesc,
				"orgId": $localStorage.orgId,
				"groupId": $localStorage.groupId,
				"deviceTypeId": $scope.profile.deviceTypeId,
				"attrType": deviceAttributeJson,
				"additionalAlerts":geoLocationAlertsJson,
				"geopoints": geoLocationJson,
				"commFrequency": freqComm,
				"gpsFrequency": freqGps,
				"attbFrequency": freqAttrFreq,
				"commFrequencyUnit": freqTimeUnitArr0,
				"gpsFrequencyUnit": freqTimeUnitArr1,
				"attbFrequencyUnit": freqTimeUnitArr2
			}
		};
		
		//console.log(JSON.stringify(reqObject));
		createProfileService.createProfile.save(JSON.stringify(reqObject)).$promise.then(function (response) {
			if (response.responseStatus === "error" && response.responseCode === 403) {
				$scope.$emit('genericErrorEvent', response);
				return;
			}
			if (response.responseStatus === "success") {
				$state.go('app.profile');
			} else {
				$scope.errorMessage = response.responseMessage;
			}
		}, function (error) {
			console.log(error);
		});
	}
	
	function addDefaultAttribute(attributeArrays){
		var reqObject = '';
		var groupIds = concatGroupIds(attributeArrays);
		for (j = 0; j < $scope.alertList.length; j++) {
			if($scope.alertList[j].alertCode == '3052'){
				reqObject = {
					"name": $scope.alertList[j].attrName,
					"alertGroupIds": groupIds,
					"alertCode": $scope.alertList[j].alertCode,
					"alertDetailMsg": $scope.alertList[j].alertDetailMsg,
					"emailIds":[]
			    }
			}
		}
		return reqObject;
	}
	
	function concatGroupIds(attributeArrays){
		var groupIds = '';
		if(attributeArrays.length === 1){
			groupIds = attributeArrays[0].alertGroupIds;
		}else{
			for (i = 0; i < attributeArrays.length; i++) {
			     for (j = i + 1 ; j < attributeArrays.length; j++) {
			    	 if(attributeArrays[i].alertGroupIds !== undefined && attributeArrays[j].alertGroupIds !== undefined){
			    		 groupIds = attributeArrays[i].alertGroupIds.concat(attributeArrays[j].alertGroupIds);
			    	 }else{
			    		 groupIds = attributeArrays[i].alertGroupIds;
			    	 }
			     }
			 }
		}
		return groupIds;
	}

}

createProfileCtrl.$inject = ['$scope', '$state', '$rootScope', 'createProfileService', 'profileService', 'profileListService', '$localStorage', 'googleAPIService', 'DeviceTypeService', 'AlertGroupService', 'AlertService'];
