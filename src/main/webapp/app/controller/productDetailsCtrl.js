angular.module('rfxcelApp').controller('productDetailController', productDetailController);

productDetailController.$inject = ['$scope', '$state', '$filter', 'activeShipmentsService', 'sharedDataService', 'productDetailsService', 'productAlertService', '$rootScope', '$http', '$localStorage', 'googleAPIService', 'commonConfigService'];

function productDetailController($scope, $state, $filter, activeShipmentsService, sharedDataService, productDetailsService, productAlertService, $rootScope, $http, $localStorage, googleAPIService, commonConfigService) {
    /*if(sharedDataService.getSelectedShipmentId() === undefined || sharedDataService.getSelectedDeviceId() === undefined || sharedDataService.getSelectedProductId() === undefined){ */
    if(sharedDataService.getSelectedShipmentId() === undefined || sharedDataService.getSelectedProductId() === undefined){
        var lastState = sharedDataService.getLastVisitedUIState();
         sharedDataService.setLastVisitedUIState($state.current.name);
         $state.go('app.dashboard');
         return;
    }
	var shipToArray = [];
	var shipFromArray = [];
	var loadSelectedShipmentCountInterval,
    refreshSelectedShipmentCountInterval;
	$scope.selectedTab = function (tabNum) {
        $scope.activeTabNumb = tabNum;
    }
    $scope.shipmentType = sharedDataService.getConfigRfxcelItem();
    if ($state.current.name == "app.productsDetails.data") {
        $scope.activeTabNumb = 2;
    } else if ($state.current.name == "app.productsDetails.alert") {
        $scope.activeTabNumb = 3;
    } else {
        $scope.activeTabNumb = 1;
    }
    $scope.selectedPointFavoriteStatus = sharedDataService.getSelectedFavoriteOption();
    loadProductProfileDetails();
    getAlertsCount();
    //product id refernce in order to show along with shipment id
    $scope.productId = sharedDataService.getSelectedProductId();

    function loadProductProfileDetails() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject = {
            "dateTime": currentDate,
			"orgId": $localStorage.orgId,
			"groupId": $localStorage.groupId,
            "deviceData": {
                "packageId": sharedDataService.getSelectedShipmentId(),
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "shipmentStatus": sharedDataService.getSelectedShipmentType(),
                "productId": sharedDataService.getSelectedProductId(),
                "rtsItemId": sharedDataService.getRTSTraceId(),
                "rtsSerialNumber": sharedDataService.getRTSSerialNumber()
            }
        };
        productDetailsService.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$scope.traceId=response.traceId;
            	sharedDataService.setTraceId(response.traceId);
            	sharedDataService.setSelectedDeviceId(response.deviceId);
            	var shipmentData = response.sensor.shipmentData;
                $scope.packageId = response.packageId;
                $scope.dateTime = response.dateTime;
                $scope.deviceId = response.deviceId;
                $scope.profileName=shipmentData.profileName;
                //if (shipmentData.lat === commonConfigService.getNullAttrResponseCode() || shipmentData.lang === commonConfigService.getNullAttrResponseCode()) {
                if (shipmentData.lat === undefined || shipmentData.lang === undefined || shipmentData.lat === -999 || shipmentData.lang === -999 || shipmentData.lat === null || shipmentData.lang === null) {
                    $scope.productLocationCoordinates = 'N.A';
                } else {
                    $scope.productLocationCoordinates = shipmentData.lat + ', ' + shipmentData.lang;
                    getNearestLocation(shipmentData.lat, shipmentData.lang);
                }

                $scope.deviceCode = shipmentData.deviceCode;
                
				/*if(shipmentData.shipTo !== null){*/
                
				if(shipmentData.shipTo !== undefined) {
					if(shipmentData.shipTo !== null){
						if(shipmentData.shipTo.location === '' && shipmentData.shipTo.city === ''){
							$scope.shipTo = 'N.A';
						}else {
							var shipToResult = shipmentData.shipTo;
							//var shipToArray = Object.keys(shipToResult).map(e=>shipToResult[e]);
							for(var x in shipToResult){
								if(x !== 'dateTime' && x !== 'locationId') {
									shipToArray.push(shipToResult[x]);
								}
							}
							var shipToArr = shipToArray.join(",");
							$scope.shipTo = shipToArr.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',').replace(/,/g, ', ');
						}
					}else{
						$scope.shipTo = 'N.A';
					}
				}else{
						$scope.shipTo = 'N.A';
				}
                
				/*if(shipmentData.shipFrom !== null ) {*/
				if(shipmentData.shipFrom !== undefined) {
					if(shipmentData.shipFrom !== null){
						if(shipmentData.shipFrom.location === '' && shipmentData.shipFrom.city === ''){
							$scope.shipFrom = 'N.A';
						}else {
							var shipFromResult = shipmentData.shipFrom;
							//var shipFromArray = Object.keys(shipFromResult).map(e=>shipFromResult[e]);
							for(var x in shipFromResult){
								if(x !== 'dateTime' && x !== 'locationId') {
									shipFromArray.push(shipFromResult[x]);
								}
							}
							var shipFromArr= shipFromArray.join(',');
							$scope.shipFrom = shipFromArr.replace(/^[,\s]+|[,\s]+$/g, '').replace(/,[,\s]*,/g, ',').replace(/,/g, ', ');
						}
					}else {
						$scope.shipFrom = 'N.A';
					}
				} else {
					$scope.shipFrom = 'N.A';
				}
            }            
        }, function (error) {
            console.log(error);
        })
    }

    function getAlertsCount() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqCountObject = {
            "dateTime": currentDate,
            "searchType": "container",
			"orgId": $localStorage.orgId,
            "deviceData": {
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "packageId": sharedDataService.getSelectedShipmentId(),
                "productId": sharedDataService.getSelectedProductId()
            }
        }
        productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$rootScope.notificationCount = response.count;
            }
        }, function (error) {
            console.log(error);
        })
    }
    // On state change start
    /*$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        sharedDataService.setLastVisitedUIState(fromState.name);
    });*/
    $scope.productBackClicked = function () {
        var lastState = sharedDataService.getLastVisitedUIState();
        sharedDataService.setLastVisitedUIState($state.current.name);
        $state.go(lastState);
        /*var lastState = sharedDataService.getLastVisitedUIState();

        if (lastState != "app.productsDetails.map" && lastState != "app.productsDetails.data" && lastState != "app.productsDetails.alert") {
            if (lastState == "app.productsDetails") {
                $state.go("app.products");
            }else{
               $state.go(lastState); 
            }
            
        }else{
            $state.go("app.products");
        }*/
    }

    function getNearestLocation(lat, lng) {
        var reqObj = googleAPIService.getNearestLocation(lat, lng);
        reqObj.then(function (response) {
            $scope.productLastLocation = response.data.results[0].formatted_address;
        }, function (error) {
            console.log(error);
        })
    }
    if ($state.current.name != "app.productsDetails.data") {
        $state.go('app.productsDetails.map');
    }
    
    /*$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.productsDetails.map' || fromState.name === 'app.productsDetails.data' || fromState.name === 'app.productsDetails.alert') {
            clearTimeout(refreshSelectedShipmentCountInterval);
        }
    });
    
    loadSelectedShipmentCountInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
    refreshSelectedShipmentCountInterval = setInterval(getAlertsCount, loadSelectedShipmentCountInterval);*/
}