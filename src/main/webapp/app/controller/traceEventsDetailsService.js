angular.module('rfxcelApp').factory("traceEventsDetailsService", traceEventsDetailsService);

traceEventsDetailsService.$inject = ['$resource', '$location', 'sharedDataService', '$http', '$localStorage'];

function traceEventsDetailsService($resource, $location, sharedDataService, $http, $localStorage) {
	
	var rtsAuthToken = $localStorage.rtsAuthToken;
	var path = $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/rest'; 
	/*var path = $location.$$protocol + '://' + $location.$$host + ':' + '9000' + '/rts/rest';*/
	
	/*var customHeaders = {
			'Authorization' : rtsAuthToken,
			'Content-Type' : 'application/json',
			'Access-Control-Allow-Origin' : '*',
	}*/
	
	

    var factory = {
        getEventItemSummary: getEventItemSummary,
        getEventItemDetails: getEventItemDetails,
        getEventInputItemDetails: getEventInputItemDetails,
        getEventSubItemDetails: getEventSubItemDetails,
        getEventInformation: getEventInformation,
        getEventNotifications: getEventNotifications,
        getRelatedEvents: getRelatedEvents,
        getCompilanceReportFlags: getCompilanceReportFlags,
        downloadEPCISComplianceReport: downloadEPCISComplianceReport,
        downloadPDFComplianceReport: downloadPDFComplianceReport,
        downloadAuditExcelReport: downloadAuditExcelReport,
        gerLocationDetail: gerLocationDetail,
        downloadEventFile: downloadEventFile,
        getEventInputItemSummary: getEventInputItemSummary
    }

    function getEventItemSummary() {
        return $resource(path + '/event/getEventItemSummary')
    }

    function getEventInputItemSummary() {
        return $resource(path + '/event/getEventInputItemSummary')
    }


    function getEventItemDetails() {
        return $resource(path + '/event/getEventItemDetails')
    }

    function getEventInputItemDetails() {
        return $resource(path + '/event/getEventInputItemDetails')
    }

    function getEventSubItemDetails() {
        return $resource(path + '/event/getEventSubItemDetails')
    }

    function getEventInformation() {
        return $resource(path + '/event/selectEventDetails')
    }

    function getEventNotifications() {
        return $resource(path + '/event/getRelatedNotificationByEventId')
    }

    function getRelatedEvents() {
        return $resource(path + '/event/getRelatedEventsByEventId')
    }

    function gerLocationDetail() {
        return $resource(path + '/event/getLocationDetail')
    }

    function getCompilanceReportFlags() {
        return $resource(path + '/event/getCompilanceReportFlags')
    }

    function downloadEPCISComplianceReport(reqObject) {
        var headerObj = {};
        return $http.post(path + '/event/downloadEPCISComplianceReport', reqObject, headerObj);
    }

    function downloadPDFComplianceReport(reqObject) {
        var headerObj = {};
        return $http.post(path + '/event/downloadPDFComplianceReport', reqObject, headerObj);
    }

    function downloadAuditExcelReport(reqObject) {
        return $http.post(path + '/audit/auditLogActionReport', reqObject, { responseType: "arraybuffer" });
    }

    function downloadEventFile(reqObject) {
        return $http.post(path + '/event/downloadEventFile', reqObject);
    }

    return factory;

}