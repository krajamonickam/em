angular.module('rfxcelApp').controller('chartDirCtrl', chartDirCtrl);

chartDirCtrl.$inject = ['$scope', 'sharedDataService','$localStorage'];


function chartDirCtrl($scope, sharedDataService,$localStorage) {
    var elId;
    var endDateTimeData;
    var globalGraphMin;
    var gloablGraphMax;
    var interval;
    
    /*min and max threshold values ref
    in order to render threshold lines*/
    var minThreshVal = $scope.minThreshVal;
    var maxThreshVal = $scope.maxThreshVal;
    
    var startDateTime = $scope.startdatetime;
    var endDateTime = $scope.enddatetime;
    
    var graphMinValue = $scope.rangeMin;
    var graphMaxValue = $scope.rangeMax;
    if($scope.data != undefined && $scope.data.length != 0){
    	$scope.dataPointMin = Math.min.apply(Math,$scope.data.map(function(item){return item.dataPoint;}));
        $scope.dataPointMax = Math.max.apply(Math,$scope.data.map(function(item){return item.dataPoint;}));
    }
    if(($scope.dataPointMin == undefined && $scope.dataPointMax == undefined) || ($scope.dataPointMin == 0 && $scope.dataPointMax == 0)){
    	$scope.dataPointMin = graphMinValue;
    	$scope.dataPointMax = graphMaxValue;
    }
    
    if($scope.dataPointMin == $scope.dataPointMax){
    	$scope.dataPointMin = 0;
    }
    interval = Math.floor($scope.dataPointMax-$scope.dataPointMin)/20;
    if(interval === 0){
    	interval = Math.ceil($scope.dataPointMax-$scope.dataPointMin)/20;
    }
    //console.log("interval...1..."+interval);
	interval = null;
    
    var zonesArr=[];
    var plotLines = [];
    var alert = $scope.alertFlag;
    var GREEN = '#2f9e51';
    var RED = '#f1575a';
    var outColor = RED;
    var inColor = GREEN;
    if (!alert) {
    	outColor = GREEN;
    }
   
    if(typeof(minThreshVal)=='undefined' && typeof(maxThreshVal)=='undefined'){
        zonesArr=[{color: GREEN}];
     }
    else if (minThreshVal <= graphMinValue) {
        zonesArr= [{
            value: maxThreshVal + 0.01, 
            color: GREEN
        },
        {
            color: outColor 
        }];
        plotLines = [{
            value: maxThreshVal,
            color: outColor,
            dashStyle: 'shortdash',
            width: 2,
            label: {
                text: 'max=' +maxThreshVal
            }
        }];
     }
    else if (maxThreshVal >= graphMaxValue) {
        zonesArr= [{
                     value: minThreshVal, 
                     color: outColor 
                 },
                 {
                     color: GREEN 
                 }];
        plotLines = [{
            value: minThreshVal,
            color: outColor,
            dashStyle: 'shortdash',
            width: 2,
            label: {
                text: 'min=' + minThreshVal
            }
        }];
     }
     else{
        zonesArr= [{
                     value: minThreshVal, 
                     color: outColor 
                 },
                 {
                     value: maxThreshVal + 0.01, 
                     color: GREEN 
                 },
                 {
                     color: outColor
                 }];
        plotLines = [{
            value: minThreshVal,
            color: outColor,
            dashStyle: 'shortdash',
            width: 2,
            label: {
                text: 'min=' + minThreshVal
            }
        }, {
            value: maxThreshVal,
            color: outColor,
            dashStyle: 'shortdash',
            width: 2,
            label: {
                text: 'max=' +maxThreshVal
            }
        }];
     }
    
    var unit=$scope.unit;
    
    /*legend to show in tooltip as well as in y-axis*/
    var legend = $scope.legend;
    if(unit !==''){
    	legend=legend + " (" + unit + ")";
    }
    
    var timeZone=$localStorage.timeZoneOffset;
    var offsetOp=timeZone.slice(0,1);
    var offsetHours=timeZone.split(':')[0];
    var offsetMinutes= offsetOp + timeZone.split(':')[1];
    
    var startDateTimeValue = startDateTime + (offsetHours*60*60*1000 + offsetMinutes*60*1000);
    var endDateTimeValue = endDateTime + (offsetHours*60*60*1000 + offsetMinutes*60*1000);
                    
    
    $scope.drapGraph = function(){

        Highcharts.setOptions({
            global: {
            	useUTC : true
            }
        });
        elId = $scope.attrName ;
        var chartType = ($scope.attrName == 'shock') ? 'column' : 'spline';
        
        // Create the chart
       var chart= Highcharts.chart(elId.toString(), {
            chart: {
                type: chartType,
                animation: Highcharts.svg, // don't animate in old IE
                marginRight: 10,
                marginTop:20,
                title: {
                    text: '',
                    style: {
                        display: 'none'
                    }
                },
                events: {
                    load: function () {
                        var series = this.series[0];
                        
                        $scope.$on('newDPReceived', function(event, data){
                           
                            for(i = 0; i < data.length; i++){
                                if($scope.attrName == data[i].attrName){
                                    defineMinAndMaxValue(data[i].dataPoints);
                                	//addDataPoints(data[i].dataPoints);
                                    break;
                               }
                            }
                        });
                        
                        function defineMinAndMaxValue(dpValue){
                        	var isMinMaxChanged=false;
                        	if(dpValue != undefined && dpValue != 0){
                        		for(i = 0; i < dpValue.length; i++){
                                	if(series.data.length === 0){
                                		if($scope.dataPointMin < dpValue[i].dataPoint && $scope.dataPointMax > dpValue[i].dataPoint){
                                			$scope.dataPointMin = 0;
                            		    	$scope.dataPointMax = dpValue[i].dataPoint;
                            		    	isMinMaxChanged=true;
                                		}
                                	} 
                                	else {
                                		//console.log("*****"+series.data[0]);
                                		//console.log("*****"+series.data[0].x);
                                		if(series.data != undefined && series.data.length != 0){
                                			$scope.dataPointMin = Math.min.apply(Math,series.data.map(function(item){return item.y;}));
                                            $scope.dataPointMax = Math.max.apply(Math,series.data.map(function(item){return item.y;}));
                                		}
                                		
                                        /*if(($scope.dataPointMin == undefined && $scope.dataPointMax == undefined) || ($scope.dataPointMin == 0 && $scope.dataPointMax == 0)){
                                        	$scope.dataPointMin = graphMinValue;
                                        	$scope.dataPointMax = dpValue[i].dataPoint;
                                        }
                                        
                                        if($scope.dataPointMin == $scope.dataPointMax){
                                        	$scope.dataPointMin = 0;
                                        }*/
                                		
                                		if(dpValue[i].dataPoint < $scope.dataPointMin){
                            				$scope.dataPointMin = dpValue[i].dataPoint;
                            				isMinMaxChanged=true;
                            			}else if(dpValue[i].dataPoint > $scope.dataPointMax){
                                			$scope.dataPointMax = dpValue[i].dataPoint;
                                			isMinMaxChanged=true;
                                		}
                                	}
                        		}
                        		if(isMinMaxChanged===true){
                        			interval = Math.floor($scope.dataPointMax-$scope.dataPointMin)/20;
                        			if(interval === 0){
                        		    	interval = Math.ceil($scope.dataPointMax-$scope.dataPointMin)/20;
                        		    }
                        		}
                        		//console.log("interval......."+interval);
                        		addDataPoints(dpValue);
                            }
                        }
                        
                        function addDataPoints(dpValue){
                        	var shift = series.data.length <=1 ? false : true;
                            for(i = 0; i < dpValue.length; i++){
	                            endDateTimeValue = dpValue[i].dateTime + (offsetHours*60*60*1000 + offsetMinutes*60*1000);
	                            dpValue[i].dateTime=dpValue[i].dateTime + ((offsetHours*60*60*1000) + (offsetMinutes*60*1000));
	                            	var x = dpValue[i].dateTime,
	                                y = dpValue[i].dataPoint;
	                            	series.addPoint([x, y], false, shift);
	                            	console.log("***** addDataPoints.addPoint("+ x + "," + y + ", false, " + shift+ ")");
	                            }
                            //chart.yAxis[0].options.tickInterval = interval;
                            chart.redraw();
                        }
                        
                    }
                },
                //spacingTop: 30,
                spacingRight: 20,
                spacingBottom:5,
	        	height: 380
            },
            title: {
                    text: '',
                    style: {
                        display: 'none',
                        color: '#ffffff'
                    }
                },
            xAxis: {
            		type: 'datetime',
                    title: {
                            text: 'Time'
                    }
            },
            yAxis: {
            	min: graphMinValue,
                max: graphMaxValue,
                title: {
                    text: legend
                },
                plotLines: plotLines
            },
            plotOptions: {
                series: {
                    pointWidth: 50,
                    pointStart: startDateTimeValue,
                    pointEnd: endDateTimeValue
                }
            },
			tooltip: {
				xDateFormat: '%A, %b %e, %H:%M:%S',
				pointFormat: '<p><span style="color:{point.color}">\u25CF</span> {series.name}: <b>{point.y}</b><br/>'
			},
            /*navigator: {
                enabled: false
            },*/
            legend: {
                enabled: false
            },
            exporting: {
                enabled: false
            },
            credits: {
                  enabled: false
            },
            series: [{
                name: legend,
                marker: {
                    enabled: true,
                    radius: 4,
                    color: RED
                },
                zones: zonesArr,
                maxPointWidth: 3,
                data: (function () {
                    
                    var data = [];
                    var dpArr = $scope.data;
                    
                    /*var startDateTimeValue = startDateTime + (offsetHours*60*60*1000 + offsetMinutes*60*1000);
                    var endDateTimeValue = endDateTime + (offsetHours*60*60*1000 + offsetMinutes*60*1000);*/
                    for (var i = 0; i < dpArr.length; i++) {
                    	dpArr[i].dateTime=dpArr[i].dateTime + (offsetHours*60*60*1000 + offsetMinutes*60*1000);
                		data.push([
                            dpArr[i].dateTime,
                            dpArr[i].dataPoint
                        ]);
                    	console.log("***** series.data("+ dpArr[i].dateTime + "," + dpArr[i].dataPoint + ")");
                    }
                    return data;
                  
                    
                }())
            }]
        });
    }
    //Draw chart end
    
    $scope.onProductSelect = function(){
        
        $state.go('productsDetails'); 
    }

}