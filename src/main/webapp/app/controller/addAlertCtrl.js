angular.module('rfxcelApp').controller('addAlertCtrl', addAlertCtrl);
addAlertCtrl.$inject = ['$scope', '$state','sharedDataService','createProfileService','$localStorage','AlertService','UserLocalService','$sessionStorage'];

function addAlertCtrl($scope, $state,sharedDataService,createProfileService,$localStorage,AlertService,UserLocalService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	$scope.attrCode='';
	var alertGroupIds = [];	
    $scope.alert={
        message: '',
        detailMessage: ''
    }
    var getAttributeList=UserLocalService.getAttributeList();
    getAttributes();

    $scope.alertGroupList=UserLocalService.getAlertGroupList();
    
    function getAttributes(){
		$scope.attributeNameList = getAttributeList;
    }
    
    function getAlertDetailsByAttrName(attributeName){
        
        var reqObject = {
            "alert": {
                "orgId": selectedOrgId,
                "attrName": attributeName
            }
        }

        
        AlertService.getAlertDetailsByAttrName().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.alert.message=response.alert.alertMsg;
                $scope.alert.detailMessage=response.alert.alertDetailMsg;
                
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
        
    }
    
    $scope.changeAttributeName=function(attObj){
        getAlertDetailsByAttrName(attObj.attrName);    
        $scope.attrCode=attObj.alertCode;
    }
    
    $scope.alertGroupChanged=function(alertObj){
		alertGroupIds=alertObj;
    }
		
    $scope.addAlert=function(){
		
        var reqObj={
            "alert": {
        	orgId: selectedOrgId,
            alertCode: $scope.attrCode,
            alertMsg: $scope.alert.message,
            alertDetailMsg: $scope.alert.detailMessage,
            alertGroupIds: alertGroupIds
        }        
        }
        AlertService.addAlert().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cancel = function () {
        $state.go("app.user");
    };
}