angular.module('rfxcelApp').controller('modifyUserCtrl', modifyUserCtrl);
modifyUserCtrl.$inject = ['$scope', '$state', 'UserService', 'UserLocalService', '$localStorage','sharedDataService', 'validationService','OrgService','$sessionStorage'];

function modifyUserCtrl($scope, $state, UserService, UserLocalService, $localStorage,sharedDataService, validationService,OrgService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	$scope.user = UserLocalService.getUserList();
	$scope.timeZoneList = [];
	getTimeZoneList();
    var selectedTimeZoneId='';
   
	function getTimeZoneList(){
		 var reqObject = {
			"orgId": $localStorage.orgId       
	     };
	 OrgService.getTimeZones.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                return;
            }
            if (response.responseStatus == "success") {
                $scope.timeZoneList = response.timeZoneList;
                for(var i in response.timeZoneList){
                    if(response.timeZoneList[i].id==$scope.user.userTz){
                        selectedTimeZoneId=response.timeZoneList[i].id;
                        $scope.selectedTimeZone={
                            timeZoneCode: response.timeZoneList[i].timeZoneCode,
                            id: response.timeZoneList[i].id
                        }
                        break;
                    }
                }
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            $scope.message = error.data.responseMessage;
        });
	}
    
    $scope.updateTimeZone=function(timeZoneId){
        selectedTimeZoneId=timeZoneId;
    }
   
    $scope.cancel = function () {
    	$state.go('app.user');
    }

    $scope.modifyuser = function (userObj) {
    	
		var isShowValidationMsg = false;
		$scope.userLoginErrorMsg = "";
		$scope.userNameErrorMsg = "";
		$scope.emailErrorMsg = "";
		$scope.phoneErrorMsg = "";
		
		
		if ($scope.user.name === "" || $scope.user.name === undefined) {
			$scope.userNameErrorMsg = "Expecting a user name";
			isShowValidationMsg = true;
		}
		if ($scope.user.email === "" || $scope.user.email === undefined) {
			$scope.emailErrorMsg = "Expecting an email address";
			isShowValidationMsg = true;
			return false;
		}
		
		/*if ($scope.user.phone === "") {
			$scope.phoneErrorMsg = "Expecting a phone number";
			isShowValidationMsg = true;
		}*/
		
		// Email validation
		isValidEmail = validationService.isValidEmail($scope.user.email);
		if (isValidEmail.status) {
			isShowValidationMsg = true;
			$scope.emailErrorMsg = isValidEmail.message;
		}

		// Phone validation
		isValidPhone = validationService.isValidPhone($scope.user.phone);
		if (!isValidPhone.status) {
			isShowValidationMsg = true;
			$scope.phoneErrorMsg = isValidPhone.message;
		}

		/*  isShowValidationMsg : true, validation is false. Show error message. */
		if (isShowValidationMsg) {
			return false;
		}
		
    	var tmpUserType = 2 ; // 0-System, 1-Admin, 2-Normal
    	if($localStorage.userType == 0 && selectedOrgId == 0){
    		tmpUserType = 3;
    	}if($localStorage.userType == 0 && selectedOrgId != 0){
    		tmpUserType = 1;
    	}else if($localStorage.userType == 1){
    		tmpUserType = 2;
    	}
    	
        var reqObject = {
            "user": {
                "userId": $scope.user.userId,
                "name": $scope.user.name,
                "userLogin": $scope.user.userLogin,
                "email": $scope.user.email,
                "phone": $scope.user.phone,
                "userType": tmpUserType,
                "orgId": selectedOrgId,
                "tzId": selectedTimeZoneId
            }
        }
        UserService.updateUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });

    }
}
