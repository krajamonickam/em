angular.module('rfxcelApp').controller('modifyAlertCtrl', modifyAlertCtrl);
modifyAlertCtrl.$inject = ['$scope', '$state','UserLocalService','AlertService','UserLocalService','sharedDataService','$sessionStorage'];

function modifyAlertCtrl($scope, $state,UserLocalService,AlertService,UserLocalService,sharedDataService,$sessionStorage) {  
	var selectedOrgId=$sessionStorage.selectedOrgId;
	$scope.alert = UserLocalService.getAlertList();
    $scope.alertGroupList=UserLocalService.getAlertGroupList();
    $scope.selectedAlertGroup = $scope.alert.alertGroupIds;

    $scope.alertGroupChanged=function(alertObj){
		$scope.selectedAlertGroup = [];
		$scope.selectedAlertGroup=alertObj;
    }
    $scope.modifyAlert=function(){
        var reqObj={
            alert: {
                alertId: $scope.alert.alertId,
                alertGroupId: $scope.alertGroupId,
                alertMsg: $scope.alert.alertMsg,
                alertDetailMsg: $scope.alert.alertDetailMsg,
                alertGroupIds: $scope.selectedAlertGroup,
                orgId: selectedOrgId
            }
        }
           
        AlertService.modifyAlert().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }
    
    $scope.cancel = function () {
        $state.go('app.user')
    }
}