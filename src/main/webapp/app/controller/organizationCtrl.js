angular.module('rfxcelApp').controller('organizationCtrl', organizationCtrl);
organizationCtrl.$inject = ['$scope', '$state', 'OrgService', 'OrgnizationLocalService', '$http', '$localStorage','sharedDataService','$mdDialog','$sessionStorage'];

function organizationCtrl($scope, $state, OrgService, OrgnizationLocalService, $http, $localStorage,sharedDataService,$mdDialog,$sessionStorage) {

    $scope.userType=$localStorage.userType;
    
    if($scope.userType === 1 || $scope.userType === 2){
    	$sessionStorage.selectedOrgId=$localStorage.orgId;
    	$state.go('app.user');
    } else {
    	getOrgList();
    }

    function getOrgList() {
        var reqObject = {
            "orgId": $localStorage.orgId
        };

        OrgService.getOrg.save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus === "success") {
                    OrgnizationLocalService.setOrgList(response.organizationList);
                    $scope.organizationList = OrgnizationLocalService.getOrgList();
                    //console.log('@@@:' + JSON.stringify(response.organizationList.length));
                } else {
                    $scope.message = response.responseMessage;
                }
            },
            function (error) {
                console.log(error);
            });
    }

    $scope.deleteOrg = function (orgId) {
        var confirm = $mdDialog.confirm().title('Are you sure you want to delete this organization?').ok('Yes').cancel('No');
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
            "organization": {
                "orgId": orgId
            }
            }
            OrgService.deleteOrg.save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus == "success") {
                    $state.go('app.organization');
                    getOrgList();
                } else {
                    $scope.message = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });
    };

    $scope.modifyOrg = function (orgObj) {
    	orgObj.orgTz = orgObj.tzId.toString();
        OrgnizationLocalService.setSelectedOrg(orgObj);
        $state.go('app.modifyOrg');
    };

    $scope.showUserList = function (orgId) {
    	$sessionStorage.selectedOrgId=orgId;
    	$state.go('app.user');
        
    }

}
