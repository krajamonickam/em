angular.module('rfxcelApp').controller('modifyDeviceCtrl', modifyDeviceCtrl);
modifyDeviceCtrl.$inject = ['$scope', '$state','sharedDataService','DeviceTypeService','$localStorage','UserLocalService','$sessionStorage'];

function modifyDeviceCtrl($scope, $state,sharedDataService,DeviceTypeService,$localStorage,UserLocalService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	$scope.device=UserLocalService.getDeviceList();
	 
    $scope.modify=function(){
        
    var reqObj={
           "orgId": selectedOrgId,
           "groupId": $localStorage.groupId,
           "device": {
        	   "deviceType": $scope.device.deviceType,
        	   "deviceKey": $scope.device.deviceKey,
        	   "mdn": $scope.device.mdn,
               "deviceId":  $scope.device.deviceId,
               "active": $scope.device.active
           		}
       };
        
       DeviceTypeService.modifyDevice().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
               $state.go('app.user');
            } else {
            	if(response.responseStatus === "error" && response.responseCode !== null && response.responseCode === 333){
            		$state.go('app.user');
            	}else{
            		$scope.message = response.responseMessage;
            	}
            }       
			}, function (error) {
			 console.log(error);
        });
    }
    
    $scope.cancel = function () {
        $state.go("app.user");
    };
}
