angular.module('rfxcelApp').controller('FileUploadController', FileUploadController);
FileUploadController.$inject = ['$scope', '$state', '$location', 'sharedDataService', '$http', 'DeviceTypeService', '$mdDialog', '$sessionStorage'];
function FileUploadController ($scope, $state, $location, sharedDataService, $http, DeviceTypeService, $mdDialog, $sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	getDeviceTypes();
	function getDeviceTypes(){
        var reqObj={
            "orgId": selectedOrgId,
            "searchType": "All"
        }
        
        DeviceTypeService.getDeviceTypes().save(JSON.stringify(reqObj)).$promise.then(function (response) {
           if (response.responseStatus === "error" && response.responseCode === 403) {
               $scope.$emit('genericErrorEvent', response);
               return;
           }
           if (response.responseStatus == "success") {
               $scope.deviceTypeList=response.sensor.deviceList;
           } else {
               $scope.message = response.responseMessage;
           }

        }, function (error) {
            console.log(error);
        });       
    }
	
	$scope.close = function(){
		$mdDialog.cancel();
	}
	
 	
 	$scope.uploadFile = function(){
 	   var file = $scope.myFile;
 	   var selectedDeviceType = $scope.selectedDeviceType;
 	   $scope.errorDeviceType = "";
 	   $scope.errorFile = "";
 	   $scope.fileUploadMsg = '';
 	   $scope.fileUploadErrorMsg = '';
 	   
   	   if(selectedDeviceType === undefined || selectedDeviceType === '') {	   
 		  $scope.errorDeviceType = 'Please select a device type';
 		  return false;
	   }
 	   if(file === undefined || file === ''){
 		   $scope.errorFile = 'Please choose a file';
 		   return false;
 	   }
 	   var allowedFiles = [".xls", ".xlsx"];
 	   var regex = new RegExp("([a-zA-Z0-9\s_\\.\-:])+(" + allowedFiles.join('|') + ")$");
 	   if (!regex.test(file.name.toLowerCase())) {
    	  $scope.errorFile = "Please choose xls or xlsx file formats";
          return false;
 	   }

 	   var uploadUrl = $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + "/rest/sensor/importDevices";
	   var fd = new FormData();
 	   fd.append('file', file);
 	   fd.append('deviceType', selectedDeviceType.deviceType);
 	   fd.append('orgId', selectedOrgId);
 	   
 	   $http.post(uploadUrl, fd, {
 		  transformRequest: angular.identity,
 		  headers: {'Content-Type': undefined}
 	   })

 	   .success(function(response){
 		   if(response.responseStatus == 'success'){
 			  //$scope.fileUploadMsg = response.responseMessage;
 			  $state.go('app.user', {}, { reload: 'app.user' })
 			  $mdDialog.cancel();
 		   }else{
 			  $scope.fileUploadErrorMsg = response.responseMessage;
 			  $('#upload-file').val('');
 			  $scope.myFile = '';
 			  selectedDeviceType = '';
 		   }
 	   })

 	   .error(function(){
 		   $scope.fileUploadErrorMsg = response.responseMessage;
 	   });
 	};
 	
 	$scope.changeDeviceType=function(selectDeviceType){

    }
 }

 angular.module('rfxcelApp').directive('fileModel', ['$parse', function ($parse) {
 	return {
 	   restrict: 'A',
 	   link: function(scope, element, attrs) {
 		  var model = $parse(attrs.fileModel);
 		  var modelSetter = model.assign;

 		  element.bind('change', function(){
 			 scope.$apply(function(){
 				modelSetter(scope, element[0].files[0]);
 			 });
 		  });
 	   }
 	};
  }]);