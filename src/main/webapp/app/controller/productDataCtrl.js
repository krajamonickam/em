angular.module('rfxcelApp').controller('productDataCtrl', productDataCtrl);
productDataCtrl.$inject = ['$scope', '$state', 'deviceDataService', '$filter', 'sharedDataService', '$rootScope', 'reportService', '$http', '$location', '$localStorage', 'productAlertService', 'generalUtilityService'];

function productDataCtrl($scope, $state, deviceDataService, $filter, sharedDataService, $rootScope,
    reportService, $http, $location, $localStorage, productAlertService, generalUtilityService) {
	
	if(sharedDataService.getSelectedDeviceId() === undefined || sharedDataService.getSelectedShipmentId() === undefined || sharedDataService.getSelectedProductId() === undefined){
		var lastState = sharedDataService.getLastVisitedUIState();
        sharedDataService.setLastVisitedUIState($state.current.name);
        $state.go('app.dashboard');
        return;
	}
	
    var attrListArr = [];
    var selectedDuration = 24;
    var startDate;
    var endDate;
    /*refresh timeout callback refrence stoared*/
    var refreshTimeout;
    
    var loadSelectedShipmentCountInterval,
    refreshSelectedShipmentCountInterval;

    $rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name == 'app.productsDetails.data') {
            clearTimeout(refreshTimeout);
            clearTimeout(refreshSelectedShipmentCountInterval);
        }
    });
    
    $scope.startDate='';
    $scope.endDate='';

    $scope.duration = [
        {
            lable: "Past 30 minutes",
            value: "0.5",
            selected: "false"
        },
        {
            lable: "Past 1 hour",
            value: "1",
            selected: "false"
        },
        {
            lable: "Past 6 hours",
            value: "6",
            selected: "false"
        },
        {
            lable: "Past 24 hours",
            value: "24",
            selected: "true"
        },
        {
            lable: "All",
            value: null,
            selected: "false"
        }
        
    ];

    $scope.selectedDuration = $scope.duration[3];

    $scope.durationSelected = function (duration) {
    	$scope.message=null;
        selectedDuration = duration.value;
        $scope.deviceGraphData = [];
        startDate=null;
        endDate=null;
        $scope.startDate=null;
        $scope.endDate=null;
        getGraphData(false);
    }
    $scope.disableEndDate=true;
    $scope.pickDate=function(selectedDate){
        selectedDuration=null;
        if($scope.startDate !=null && typeof($scope.startDate)!=="undefined" && selectedDate==='startDate'){
        	startDate=Date.UTC($scope.startDate.getFullYear(), $scope.startDate.getMonth(), $scope.startDate.getDate(),0,0,0,0);
        	startDate = generalUtilityService.convertUTCToLocalTimezone(startDate);
        }else if($scope.endDate !=null && typeof($scope.endDate)!=="undefined" && selectedDate==='endDate'){
        	endDate=Date.UTC($scope.endDate.getFullYear(), $scope.endDate.getMonth() , $scope.endDate.getDate(),23,59,59,999);
        	endDate = generalUtilityService.convertUTCToLocalTimezone(endDate);
        }
        
        if(startDate != null && typeof(startDate) != 'undefined' && endDate != null && typeof(endDate) != 'undefined'){
            if(startDate>endDate){
                $scope.message = "Start date should be lesser than end date";
                return false;
            }else{
                $scope.message=null;
                getGraphData(false);
            }
            
        }
    }

    function getReqObj(isRefresh) {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqObject;

        if (isRefresh) {
            reqObject = {
            	"dateTime": currentDate,
				"orgId": $localStorage.orgId,
				"deviceData": {
                    "deviceId": sharedDataService.getSelectedDeviceId(),
                    "packageId": sharedDataService.getSelectedShipmentId(),
                    "duration": selectedDuration,
                    "startDate": startDate,
                    "endDate": endDate,
                    "attrType": attrListArr,
                    "productId": sharedDataService.getSelectedProductId()
                }
            }
        } else {
            reqObject = {
                "dateTime": currentDate,
				"orgId": $localStorage.orgId,
                "deviceData": {
                    "deviceId": sharedDataService.getSelectedDeviceId(),
                    "packageId": sharedDataService.getSelectedShipmentId(),
                    "duration": selectedDuration,
                    "startDate": startDate,
                    "endDate": endDate,
                    "attrType": null,
                    "productId": sharedDataService.getSelectedProductId()
                }
            }
        }
        return reqObject;
    }

    getGraphData(false);

    function getGraphData(isRefresh) {
        deviceDataService.save(getReqObj(isRefresh)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (!isRefresh) {
            	if (response.responseStatus == "success") {
            		$scope.startDateTime = response.sensor.startDate;
            		$scope.endDateTime = response.sensor.endDate;
                    $scope.deviceGraphData = response.sensor.attributes;
            	}
            } else {
            	if (response.responseStatus == "success") {
            		 var currentAttList = response.sensor.attributes;
                     if (currentAttList.length > 0) {
                         //if (currentAttList[0].dataPoints.length > 0) {
                             $scope.$broadcast('newDPReceived', currentAttList);
                         //}
                     }
            	}
            }
            attrListArr = [];

            var lastDateTime;
            if (response.responseStatus == "success") {
            	angular.forEach(response.sensor.attributes, function (value, key) {
        			attrListArr.push({
                        'attrName': value.attrName,
                        'startDateTime': value.lastDateTime !== undefined ? value.lastDateTime : null
                    });
                });
            }
            
            function getLastDateTime(data) {
                if (lastDateTime != undefined) {
                    return lastDateTime;
                } else {
                    angular.forEach(data, function (value, key) {
                        if (value.lastDateTime != undefined) {
                            lastDateTime = value.lastDateTime;
                            return lastDateTime;
                        }
                    })
                }
                return lastDateTime;
            }

            if (refreshTimeout) {
                clearInterval(refreshTimeout);
            }
            if ($localStorage.autoRefresh) {
                refreshTimeout = setTimeout(refreshGraph, $localStorage.autoRefreshFreq);
            }

        }, function (error) {
            console.log('Error in graph data: ' + error)
        })
    }

    function refreshGraph() {
    	if(startDate == null && endDate == null){
    		getGraphData(true);
    	}
    }

    $scope.downloadReports = function () {
        /*var dataObj = {
    		"orgId": $localStorage.orgId,
            "deviceId": sharedDataService.getSelectedDeviceId(),
            "packageId": sharedDataService.getSelectedShipmentId()
        };*/

		var dataObj = {
			"orgId": $localStorage.orgId,
			"user": $localStorage.username,
			"deviceData": {
				"deviceId": sharedDataService.getSelectedDeviceId(),
				"packageId": sharedDataService.getSelectedShipmentId(),
				"productId": sharedDataService.getSelectedProductId()
			}
		};
		
        var rService = reportService.downloadReports(dataObj);
        rService.success(onReportDownloadSuccess);
        rService.error(onReportDownloadFault);

        function onReportDownloadSuccess(data, status, headers) {
            if (data.responseStatus === "error" && data.responseCode === 403) {
                $scope.$emit('genericErrorEvent', data);
                return;
            }
            headers = headers();
            var full_filename = headers['content-disposition'];
            var fileNameArray = full_filename.split("=");
            var filename = fileNameArray[1];
            var contentType = headers['content-type'];
            var linkElement = document.createElement('a');

            try {

                var blob = new Blob([data], {
                    type: contentType
                });
                
                if(window.navigator.msSaveOrOpenBlob) {
            		window.navigator.msSaveOrOpenBlob(blob, filename);
                }else{
                	var url = window.URL.createObjectURL(blob);
                    linkElement.setAttribute('href', url);
                    linkElement.setAttribute("download", filename);
                    var clickEvent = new MouseEvent("click", {
                        "view": window,
                        "bubbles": true,
                        "cancelable": false
                    });
                    linkElement.dispatchEvent(clickEvent);
                }
            } catch (ex) {
                console.log(ex);
            }
        }

        function onReportDownloadFault(error) {
            console.log(error);
        }
    }
    
    getAlertsCount();
    function getAlertsCount() {
        var date = new Date();
        var currentDate = $filter('date')(new Date(), 'yyyy/MM/dd HH:mm:ss');
        var reqCountObject = {
            "dateTime": currentDate,
            "searchType": "container",
			"orgId": $localStorage.orgId,
            "deviceData": {
                "deviceId": sharedDataService.getSelectedDeviceId(),
                "packageId": sharedDataService.getSelectedShipmentId(),
                "productId": sharedDataService.getSelectedProductId()
            }
        }
        productAlertService.getNotificationsAlertsCount.post(reqCountObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	$rootScope.notificationCount = response.count;
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    /*$rootScope.$on('$stateChangeStart', function (event, toState, toParams, fromState, fromParams, error) {
        if (fromState.name === 'app.productsDetails.data') {
            clearTimeout(refreshSelectedShipmentCountInterval);
        }
    });*/
    
    loadSelectedShipmentCountInterval = ($localStorage.autoRefresh) ? $localStorage.autoRefreshFreq : '30000';
    refreshSelectedShipmentCountInterval = setInterval(getAlertsCount, loadSelectedShipmentCountInterval);
}