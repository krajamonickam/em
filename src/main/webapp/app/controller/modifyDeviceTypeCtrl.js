angular.module('rfxcelApp').controller('modifyDeviceTypeCtrl', modifyDeviceTypeCtrl);
modifyDeviceTypeCtrl.$inject = ['$scope', '$state','sharedDataService','UserLocalService','DeviceTypeService','$sessionStorage'];

function modifyDeviceTypeCtrl($scope, $state,sharedDataService,UserLocalService,DeviceTypeService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
    $scope.deviceTypes=UserLocalService.getDeviceTypeList();
    getAttributesByDeviceType();
    $scope.attributeList=[];
    var selectedAttributes=[];
    var attributesArrList=[];
    
    function getAttributesByDeviceType(){
        
          var reqObj={
                "orgId": selectedOrgId,
                "deviceDetail": {
                    "deviceTypeId":  $scope.deviceTypes.deviceTypeId
                }
            }       
        
       DeviceTypeService.getAttributesByDeviceType().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                var attributes=UserLocalService.getAttributeList();
                var setAttributeList=[];
                for(var i in attributes){
                        setAttributeList.push({
                            attrId: attributes[i].attrId,
                            attrName: attributes[i].attrName,
                            excursionDuration: 0,
                            legend: attributes[i].attrName,
                            notifyLimit: 1,
                            showGraph: false,
                            checkAttributes: false,
                            unit: attributes[i].unit,
                            rangeMin: attributes[i].rangeMin,
                            rangeMax: attributes[i].rangeMax,
                            showMap:false,
                            rangeMin:attributes[i].rangeMin,
                            rangeMax:attributes[i].rangeMax,
                            isSensorAttribute:attributes[i].isSensorAttribute,
                            roundOffTo:attributes[i].roundOffTo,
                            alertOnReset:attributes[i].alertOnReset
                        });
                    
                        for(var j in response.sensor.attributeList){
                            if(response.sensor.attributeList[j].attrId===attributes[i].attrId){
                                setAttributeList[i].excursionDuration=response.sensor.attributeList[j].excursionDuration;
                                setAttributeList[i].legend=response.sensor.attributeList[j].legend;
                                setAttributeList[i].notifyLimit=response.sensor.attributeList[j].notifyLimit;
                                setAttributeList[i].showGraph=response.sensor.attributeList[j].showGraph;
                                setAttributeList[i].checkAttributes=true;
                                setAttributeList[i].showMap=response.sensor.attributeList[j].showMap;
                                setAttributeList[i].rangeMin=response.sensor.attributeList[j].rangeMin;
                                setAttributeList[i].rangeMax=response.sensor.attributeList[j].rangeMax;
                            }   
                        }      
                }
                $scope.attributeList=setAttributeList;
            } else {
                $scope.message = response.responseMessage;
            }       
			}, function (error) {
			 console.log(error);
        })
    }
  
    
    
    $scope.modifyDeviceType=function(){
    	$scope.errorMessageMinRange = [];
        $scope.errorMessageMaxRange = [];
        for (var i in $scope.attributeList){
   	    	if($scope.attributeList[i].checkAttributes===true){
   	    		if($scope.attributeList[i].rangeMin === '' && $scope.attributeList[i].rangeMax !== ''){
    	   	    	$scope.errorMessageMinRange[i] = 'Min value is required';
    	   	    	return;
    	   	    }
   	    		else if(isNaN($scope.attributeList[i].rangeMin)){
    	   	    	$scope.errorMessageMinRange[i] = 'Min value should be an integer';
    	   	    	return;
    	   	    }
    	   	    else if($scope.attributeList[i].rangeMax === '' && $scope.attributeList[i].rangeMin !== ''){
    	   	    	$scope.errorMessageMaxRange[i] = 'Max value is required';
    	   	    	return;
    	   	    }
	    	   	else if(isNaN($scope.attributeList[i].rangeMax)){
	 	   	    	$scope.errorMessageMaxRange[i] = 'Max value should be an integer';
	 	   	    	return;
	 	   	    }
	    	   	else if($scope.attributeList[i].rangeMin !== '' && $scope.attributeList[i].rangeMax !== '' && parseFloat($scope.attributeList[i].rangeMin) === parseFloat($scope.attributeList[i].rangeMax)){
    	   	    	$scope.errorMessageMinRange[i] = 'Min & Max values cannot be same';
    	   	    	return;
    	   	    }
    	   	    else if($scope.attributeList[i].rangeMin !== '' && $scope.attributeList[i].rangeMax !== '' && parseFloat($scope.attributeList[i].rangeMin) > parseFloat($scope.attributeList[i].rangeMax)){
    	   	    	$scope.errorMessageMinRange[i] = "Min cannot exceed Max";
    	   	    	return;
    	   	    }
    	   	    else {
    	   	    	delete $scope.attributeList[i].checkAttributes;
                    attributesArrList.push($scope.attributeList[i]);
    	   	    }
            }
       }
        
        var reqObj={
           "orgId": selectedOrgId,
           "deviceDetail": {
               "deviceType": $scope.deviceTypes.deviceType,
               "deviceTypeId": $scope.deviceTypes.deviceTypeId,
               "manufacturer":  $scope.deviceTypes.manufacturer,
               "attributes": attributesArrList
           }
       }    
        
       DeviceTypeService.modifyDeviceTypes().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {             
               $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }       
			}, function (error) {
			 console.log(error);
        });
    }

    $scope.cancel = function () {
        $state.go("app.user");
    };
}
