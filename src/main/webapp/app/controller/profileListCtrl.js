angular.module('rfxcelApp').controller('profileController', profileController);
profileController.$inject = ['$scope', '$state', 'profileService', 'profileListService', 'createProfileService', 'sharedDataService', '$localStorage', '$mdDialog'];

function profileController($scope, $state, profileService, profileListService, createProfileService, sharedDataService, $localStorage, $mdDialog) {
	loadProfiles();
	$scope.orgId=$localStorage.orgId;
	
    function loadProfiles(){

		var reqObject = {
			"profile": {
				"orgId": $localStorage.orgId,
				"groupId": $localStorage.groupId
			}
		};
         // Service to get the profile list
       profileService.getProfileData.post(JSON.stringify(reqObject)).$promise.then(function (response) {
		    if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
			$scope.profileList = response.profileList;
        }, function (error) {
            console.log(error);
        });
    
    }

	/* Delete profile based on profileId */
	/*$scope.deleteProfile = function (profileId) {
    	
		var confirm = $mdDialog.confirm({
			controller: DialogController,
			templateUrl: 'app/view/profile/deleteProfile.html',
			clickOutsideToClose:true,
		});
		
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
                "profile": {
                    "profileId": profileId
                }
            };
            profileService.deleteProfile.post(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                $state.reload('app.profile', {});
            }, function (error) {
                console.log(error);
            });
        });
	};
	
	function DialogController($scope, $mdDialog) {
		$scope.hide = function() {
	  		$mdDialog.hide();
		};
		
		$scope.confirmationNo = function() {
	  		$mdDialog.cancel();
		};

		$scope.confirmationYes = function(answer) {
			$mdDialog.hide(answer);
		};
  	}*/
	
    $scope.deleteProfile = function (profileId) {
    	 //var confirm = $mdDialog.confirm().title('Delete Profile').content('Are you sure you want to delete this profile?').ok('Yes').cancel('No').clickOutsideToClose(true);
    	 var confirm = $mdDialog.confirm().title('Are you sure you want to delete this device?').ok('Yes').cancel('No').clickOutsideToClose(true);
		
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
                "profile": {
                    "profileId": profileId,
                    "orgId": $localStorage.orgId
                }
            };
            
            profileService.deleteProfile.post(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                
                if(response.responseStatus === "success"){
                	$state.reload('app.profile', {});
                } else {
                	$scope.message = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });
	};
	
	/*$scope.deleteProfile = function (profileId) {
        
		var confirm = $mdDialog.confirm({
		   templateUrl: 'app/view/profile/deleteProfile.html',
		});
		
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
                "profile": {
                    "profileId": profileId
                }
            };
            
            profileService.deleteProfile.post(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                $state.reload('app.profile', {});
            }, function (error) {
                console.log(error);
            });
        });
	};*/
	
	$scope.addGeopoints = function() {
		var newItemNo = $scope.profile.geopointsData.length+1;
		$scope.profile.geopointsData.push({'id':'geopointsData'+newItemNo});
	};
		    
	$scope.removeGeopoints = function(location) {
		for (var i = 0; i <  $scope.profile.geopointsData.length; i++) {
			if ($scope.profile.geopointsData[i].address === location.address) {
				$scope.profile.geopointsData.splice(i, 1);
			}
		}
	};

    $scope.editProfile = function(profile){
        sharedDataService.setSelectedProfile(profile);
        $state.go('app.modifyProfile');
    }
}