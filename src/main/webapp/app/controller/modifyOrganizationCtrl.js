angular.module('rfxcelApp').controller('modifyOrgCtrl', modifyOrgCtrl);
modifyOrgCtrl.$inject = ['$scope', '$state', 'OrgService', 'OrgnizationLocalService','$localStorage'];

function modifyOrgCtrl($scope, $state, OrgService, OrgnizationLocalService,$localStorage) {
	
    $scope.org = OrgnizationLocalService.getSelectedOrg();
    var selectedTimeZoneId='';
	
	$scope.timeZoneList = [];
	getTimeZoneList();
	function getTimeZoneList(){
		var reqObject = {
		  "orgId": $localStorage.orgId
		};

		 OrgService.getTimeZones.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.timeZoneList = response.timeZoneList;
                for(var i in response.timeZoneList){
                    if(response.timeZoneList[i].id==$scope.org.orgTz){
                        selectedTimeZoneId=response.timeZoneList[i].id;
                        $scope.selectedTimeZone={
                            timeZoneCode: response.timeZoneList[i].timeZoneCode,
                            id: response.timeZoneList[i].id
                        }
                        break;
                    }
                }
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            $scope.message = error.data.responseMessage;
        });
	}
    $scope.updateTimeZone=function(timeZoneId){
        selectedTimeZoneId=timeZoneId;
    }

    $scope.modifyOrgConfirm = function (orgObj) {

        var reqObject = {
            "organization": {
            	"extId": $scope.org.extId,
            	"orgId": $scope.org.orgId,
                "name": $scope.org.name,
                "shortName": $scope.org.shortName,
                "email": $scope.org.email,
                "orgTz": selectedTimeZoneId,
                "tzId": selectedTimeZoneId
            }
        };
        OrgService.modifyOrg.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $state.go('app.organization');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.cancel = function () {
        $state.go('app.organization')
    }
}
