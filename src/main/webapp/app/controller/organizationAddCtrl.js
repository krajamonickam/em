angular.module('rfxcelApp').controller('organizationAddCtrl', organizationAddCtrl);
organizationAddCtrl.$inject = ['$scope', '$state', 'OrgService', '$http','$localStorage'];

function organizationAddCtrl($scope, $state, OrgService, $http,$localStorage) {
    var selectedTimeZone='';
	$scope.timeZoneList = [];
	getTimeZoneList();
	function getTimeZoneList(){
		var reqObject = {
		  "orgId": $localStorage.orgId
		};

		OrgService.getTimeZones.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.timeZoneList = response.timeZoneList;

            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            $scope.message = error.data.responseMessage;
        });
	}
	  
    $scope.org = {
        name: '',
        emailId: '',
        shortName: '',
        orgTz: '',
        tzId:''
    };
    
    $scope.changeTimeZone=function(timeZoneId){
        selectedTimeZone=timeZoneId;
    }
    $scope.addOrg = function () {
        var reqObject = {
        	"orgId": $localStorage.orgId,
        	"organization": {
            	"extId": $scope.org.extId,
            	"name": $scope.org.name,
                "shortName": $scope.org.shortName,
                "email": $scope.org.email,
                "orgTz": selectedTimeZone,
                "tzId": selectedTimeZone
            }
        };
        OrgService.addOrg.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $state.go('app.organization');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    };
    $scope.cancel = function () {
        $state.go("app.organization");
    };
}
