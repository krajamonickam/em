angular.module('rfxcelApp').controller('userCtrl', userCtrl);

userCtrl.$inject = ['$scope', '$state', 'UserService', 'UserLocalService', 'AlertService', 'AlertGroupService', '$localStorage', 'sharedDataService','DeviceTypeService','$mdDialog','$location','$rootScope','$http','loginService', 'OrgService','$sessionStorage'];


function userCtrl($scope, $state, UserService, UserLocalService, AlertService, AlertGroupService, $localStorage, sharedDataService,DeviceTypeService,$mdDialog,$location,$rootScope,$http, loginService, OrgService,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	getOrgList();
	getUsers();
    getAlerts();
    getAlertGroups();
    getDeviceTypes();
    getDevices();
    getAttributes();
    
    $scope.userType=$localStorage.userType;
    $scope.currentUser=$localStorage.username;
    
    
    
    function getOrgList() {
        var reqObject = {
            "orgId": selectedOrgId
        };
        OrgService.getOrg.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
            	if(response.organizationList.length > 0){
            		$scope.organizationName = response.organizationList[0].name;
            	}
            } else {
                $scope.message = response.responseMessage;
            }
        },
        function (error) {
            console.log(error);
        });
    }

    function getUsers() {
        var reqObject = {
            "user": {
                "orgId": selectedOrgId/*,
                "groupId": $localStorage.groupId,*/
            }
        };

        UserService.getUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                UserLocalService.setUserList(response.userList);
                $scope.userList = UserLocalService.getUserList();
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }

    function getAlerts() {
		var data = "";
        var reqObject = {
            "alert": {
                "orgId": selectedOrgId
            }
        }

        AlertService.getAlert().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
				angular.forEach(response.alertList, function (value, key) {
					var groupArr = value.alertGroupNameList.toString();				
					response.alertList[key].alertGroupNameList = groupArr;
				});
                UserLocalService.setAlertList(response.alertList);
                $scope.alertList = UserLocalService.getAlertList();
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });


    }

    function getAlertGroups() {
        var reqObj = {
        		"alertGroup": {
        			 "orgId": selectedOrgId/*,
        	            "groupId": $localStorage.groupId*/
                }
        }

        AlertGroupService.getAlertGroup().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                UserLocalService.setAlertGroupList(response.alertGroupList);
                $scope.alertGroupList = UserLocalService.getAlertGroupList();
            } else {
                $scope.message = response.responseMessage;
            }

        }, function (error) {
            console.log(error);
        });
    }
    
    function getDeviceTypes(){
        var reqObj={
            "orgId": selectedOrgId,
            "searchType": "All"
        }
        
         DeviceTypeService.getDeviceTypes().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                UserLocalService.setDeviceTypeList(response.sensor.deviceList);
                $scope.deviceTypesList=UserLocalService.getDeviceTypeList();
            } else {
                $scope.message = response.responseMessage;
            }

        }, function (error) {
            console.log(error);
        });       
    }
    
    function getDevices(){
        var reqObj={
        		"device": {
       			 "orgId": selectedOrgId/*,
       	            "groupId": $localStorage.groupId*/
               }
        }
        
         DeviceTypeService.getDeviceList().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.deviceList = response.sensor.devices;
            } else {
                $scope.message = response.responseMessage;
            }

        }, function (error) {
            console.log(error);
        });       
    }
    
    function getAttributes(){
    	var reqObj={
            "orgId": selectedOrgId
        }
		DeviceTypeService.getAttributeList().save(JSON.stringify(reqObj)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                UserLocalService.setAttributeList(response.sensor.attributeList);
                $scope.attributeNameList=UserLocalService.getAttributeList();
            } else {
                $scope.message = response.responseMessage;
            }       
			}, function (error) {
			 console.log(error);
        });
    }

    $scope.deleteUser = function (userId) {
        var confirm = $mdDialog.confirm().title('Are you sure you want to delete this user?').ok('Yes').cancel('No').clickOutsideToClose(true);
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
            "user": {
                "userId": userId,
                "orgId": selectedOrgId
            }
            }
            UserService.deleteUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus == "success") {
                    getUsers();
                    $state.go('app.user');
                } else {
                    $scope.message = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });    
        
    }

    $scope.deleteAlertGroup = function (Id) {
        var confirm = $mdDialog.confirm().title('Are you sure you want to delete this alert group?').ok('Yes').cancel('No').clickOutsideToClose(true);
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
            "alertGroup": {
                "id": Id,
                "orgId": selectedOrgId
                }
            }
        AlertGroupService.deleteAlertGroup().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	getAlertGroups();
                getAlerts();
                $state.go('app.user');
            } else {
                $scope.alertGroupMessage = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
        });    
    }

    $scope.deleteAlert = function (alertId) {
        var confirm = $mdDialog.confirm().title('Are you sure you want to delete this alert?').ok('Yes').cancel('No').clickOutsideToClose(true);
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
            "alert": {
                "alertId": alertId,
                "orgId": selectedOrgId
            }
            }
            AlertService.deleteAlert().save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus == "success") {
                    getAlerts();
                    $state.go('app.user');
                } else {
                    $scope.alertMessage = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });    
    };
    
   $scope.deleteDeviceType = function (deviceTypeId) {
       
       
       var confirm = $mdDialog.confirm().title('Are you sure you want to delete this device Type?').ok('Yes').cancel('No').clickOutsideToClose(true);
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
            "orgId": selectedOrgId,
            "deviceDetail": {
                    "deviceTypeId": deviceTypeId
                }

            }

            DeviceTypeService.deleteDeviceTypes().save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus == "success") {
                    getDeviceTypes();
                    $state.go('app.user');
                } else {
                    $scope.deviceTypeMessage = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });    
    };
    
    $scope.deleteDeviceId = function (deviceId) {
        var confirm = $mdDialog.confirm().title('Are you sure you want to delete this device?').ok('Yes').cancel('No').clickOutsideToClose(true);
        $mdDialog.show(confirm).then(function() {
            var reqObject = {
           "orgId": selectedOrgId,
            "deviceData": {
                "deviceId": deviceId
            	}
            };
            DeviceTypeService.deleteDevice().save(JSON.stringify(reqObject)).$promise.then(function (response) {
                if (response.responseStatus === "error" && response.responseCode === 403) {
                    $scope.$emit('genericErrorEvent', response);
                    return;
                }
                if (response.responseStatus == "success") {
                	getDevices();
                    $state.go('app.user');
                } else {
                    $scope.deviceMessage = response.responseMessage;
                }
            }, function (error) {
                console.log(error);
            });
        });    
        
    }

    $scope.editUser = function (userobj) {
    	userobj.userTz = userobj.tzId.toString();
        UserLocalService.setUserList(userobj);
        $state.go('app.modifyUser');

    };

    $scope.modifyAlert = function (alertObj) {
        UserLocalService.setAlertList(alertObj);
        $state.go('app.modifyAlert');

    };
    
    $scope.modifyDeviceType=function(deviceTypeObj){
        UserLocalService.setDeviceTypeList(deviceTypeObj);
        $state.go('app.modifyDeviceType');
    }
    
    $scope.modifyDevice=function(deviceObj){
        UserLocalService.setDeviceList(deviceObj);
        $state.go('app.modifyDevice');
    }

    $scope.editAlertGroup = function (alertGroupObj) {
        UserLocalService.setAlertGroupList(alertGroupObj);
        $state.go('app.modifyAlertGroup');
    };
     $scope.cancel = function () {
        $state.go('app.user');
    };
    
 $scope.userPassword = function (event,userObj) {
	 var forgotPasswdurl= $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() + '/#/resetPassword';
	 $scope.user = userObj;
        var reqObject = {
            "user": {
                "name": $scope.user.name,
                "userLogin": $scope.user.userLogin,
                "email": $scope.user.email,
                "phone": $scope.user.phone,
                "userType":$scope.user.userType,
                "orgId": $scope.user.orgId,
                "groupId": $scope.user.groupId,
                "userTz": $scope.user.userTz,
                "active":$scope.user.active,
                "tzId": $scope.user.tzId
            },
            "url": forgotPasswdurl
        };
        UserService.setPassword.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	 $mdDialog.show($mdDialog.alert().parent(angular.element(document.querySelector('#rootContainer'))).clickOutsideToClose(true).title(response.responseMessage).ok('Ok').targetEvent(event));
                $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });

    };
    
    $scope.loginAsSupportUser = function(event,userObj){
    	var reqObject = {
			  "authToken": $localStorage.authToken, 	
              "userLogin": userObj.userLogin
    	}
    	UserService.loginAsSupportUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            $localStorage.authToken = response.authToken;
            $localStorage.username = userObj.userLogin;
            $localStorage.shard = userObj.shard;
			$http.defaults.headers.common['Authorization'] = $localStorage.authToken;
            $http.defaults.headers.common['Content-Type'] = "application/json";
            $http.defaults.headers.common['Access-Control-Allow-Origin'] = "*";
            $http.defaults.headers.common['Shard'] = $localStorage.shard;
            $rootScope.userName = $localStorage.username;
            if (response.responseStatus == "success") {
            	$localStorage.orgId = response.currentUser.orgId;
	            $localStorage.groupId = response.currentUser.groupId;
	            $localStorage.userType = response.currentUser.userType;
	            $localStorage.userId = response.currentUser.userId;
	            $localStorage.timeZoneOffset = response.currentUser.userTz;
	            $localStorage.privilege = response.currentUser.privilege;
	            var reqObject1 = {
	            		"orgId": $localStorage.orgId
                }
	            loginService.getConfigData.post(reqObject1).$promise.then(function (response) {
            		$localStorage.autoRefresh = response.autoRefresh;
                    $localStorage.autoRefreshFreq = response.autoRefreshFreq;
            		$localStorage.centerLat = response.centerLat;
    	            $localStorage.centerLong = response.centerLong;
    	            $localStorage.mapZoom = response.mapZoom;
    	            $localStorage.mapType = response.mapType;
            		$localStorage.helpdeskContactMessage = response.helpdeskContactMessage;
    				$localStorage.systemdemo = response.systemDemo;
    				$localStorage.clickToChat = response.clickToChat;
    	            $rootScope.systemDemo = $localStorage.systemdemo;
    	            $rootScope.showResetOrg = ($localStorage.privilege > 0);
    	            $rootScope.clickToChat = $localStorage.clickToChat;
    	            $localStorage.fetchESData = response.fetchESData;
    	            $localStorage.newProfileScreen = response.newProfileScreen;
    	            $localStorage.alertDeviceLogSearch = response.alertDeviceLogSearch;
					$state.transitionTo('app.dashboard', null, {'reload':true});
            	}, function (error) {
                    console.log(error);
                })
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            console.log(error);
        });
    }

    $scope.updateKirsenDeviceKeys = function(){
    	var reqObj = {
        	"orgId": selectedOrgId
        }
		 UserService.updateKirsenDeviceKeys.save(JSON.stringify(reqObj)).$promise.then(function (response) {
	         if (response.responseStatus === "error" && response.responseCode === 403) {
	             $scope.$emit('genericErrorEvent', response);
	             return;
	         }
	         if (response.responseStatus == "success") {
	         } else {
	         }
	     }, function (error) {
	         console.log(error);
	     });
    }
    
    $scope.deviceFileUpload = function(event){
    	$mdDialog.show({
         	parent: angular.element(document.querySelector('#rootContainer')),
         	//clickOutsideToClose:true,
    		targetEvent: event,
         	templateUrl: 'app/view/fileUpload.html',
         	controller: 'FileUploadController'
         	/*locals: {
         		selectedDeviceType: $scope.selectedDeviceType
             }*/
    	  });
    }
}