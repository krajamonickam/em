angular.module('rfxcelApp').controller('addAlertGroupCtrl', addAlertGroupCtrl);
addAlertGroupCtrl.$inject = ['$scope', '$state','UserService','UserLocalService','AlertGroupService','sharedDataService', '$localStorage','$sessionStorage'];

function addAlertGroupCtrl($scope, $state,UserService,UserLocalService,AlertGroupService,sharedDataService, $localStorage,$sessionStorage) {
	var selectedOrgId=$sessionStorage.selectedOrgId;
	getUsers();
	$scope.userList = [];
	$scope.selectedUserList = [];
	var selectedUsers = [];
	 $scope.alertGroup={
			 groupName: '' ,
		     description: '' ,
		     users :''
	     };
	function getUsers() {
        var reqObject = {
            "user": {
                "orgId": selectedOrgId/*,
                "groupId": $localStorage.groupId*/
            }
        };

        UserService.getUser.save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                UserLocalService.setUserList(response.userList);
                $scope.userList = UserLocalService.getUserList();

            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            $scope.message = error.data.responseMessage;
        });
    };
	
	
	$scope.addUsersToGroup = function(availableUsers){
		angular.forEach(availableUsers, function($value, $key) {
			$scope.selectedUserList.push($value);
	    });
		if(availableUsers.length != 0){
			for(var i in $scope.selectedUserList){
				for(var j in  $scope.userList){
					if( $scope.selectedUserList[i].userId == $scope.userList[j].userId){
						$scope.userList.splice(j,1);
					}
				}
			}
		}
		
	};
	
	$scope.removeUsersFromGroup = function(selectedUsers){
		angular.forEach(selectedUsers, function($value, $key) {
			$scope.userList.push($value);
	    });
		if(selectedUsers.length != 0){
			for(var i in $scope.userList){
				for(var j in $scope.selectedUserList){
					if( $scope.userList[i].userId == $scope.selectedUserList[j].userId){
						$scope.selectedUserList.splice(j,1);
					}
				}
			}
		}
	};
    
    $scope.add = function () {
    	for(var j=0;j < $scope.selectedUserList.length;j++){
    		selectedUsers.push($scope.selectedUserList[j].userId);
    	}
    	
        var reqObject = {
        		"alertGroup": {
        		     "groupName": $scope.alertGroup.groupName,
        		     "description":$scope.alertGroup.description ,
        		     "orgId": selectedOrgId,
        		     "groupId":$localStorage.groupId, 
        		     "users" :selectedUsers
        		}
            }
     
        AlertGroupService.addAlertGroup().save(JSON.stringify(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $state.go('app.user');
            } else {
                $scope.message = response.responseMessage;
            }
        }, function (error) {
            $scope.message = error.data.responseMessage;
        });
        
    };

    $scope.cancel = function () {
        $state.go("app.user");
    };
};
