function traceItemDetailsController($scope, $state, sharedDataService, $filter, traceItemDetailsService, uiGridTreeViewConstants, uiGridTreeBaseService, uiGridConstants, $stateParams, $window, $mdDialog,$timeout) {
    var childtraceEntityId = $stateParams.traceId,
        parentTracecolumnDefsArr,
        eventColumnDefArr,
        topLevelItem;
    
    var deviceId = $stateParams.deviceId;
    console.log('traceItemDetailsController ' );
    $scope.traceIdPrefix = {
        isVisible: false
    };
    $scope.toggle = {
        isForwardTracing: false
    };
    
    $scope.collapse = {
        isParentTraceOpen : false,
        isItemsSummaryOpen: true
    };
    
    $scope.findNearByProp = {
        latitude: '',
        longitude: '',
        itemId: ''
    };
    $('#legend').css('display', 'none');
    $scope.$emit('isShowHeaderFooter', false);
    $scope.isParentVisible = false;
    $scope.itemSummaryList = {
        columnDefs: [
            { name: 'prodName', displayName: 'Item Name', enableColumnMenu: false },
            { name: 'itemTypeExtId', displayName: 'Item Id', enableColumnMenu: false },
            { name: 'itemType', displayName: 'Item Type', enableColumnMenu: false },
            { name: 'quantity', displayName: 'Qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' }
        ]
    };
    $scope.notificationsList = {
        columnDefs: [
            { visible: true, id: '1', name: 'notificationId', displayName: 'Notification ID', width: 180, /*width: '**',*/ enableColumnMenu: false, cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.clickNotificationId(row.entity.notificationId)">{{row.entity.notificationId}}</span></a></div>'},
            {
                visible: true,
                id: '2',
                name: 'eventId',
                displayName: 'Event ID',
                minWidth: 120,
                maxWidth: 140,
                enableColumnMenu: false,
                cellTemplate: '<div class="ui-grid-cell-contents"><a ng-click="grid.appScope.clickEventId(row.entity.eventId)"><span ng-if="row.entity.eventExtId == null || !row.entity.eventExtId">{{row.entity.eventId}}</span><span ng-if="row.entity.eventExtId || row.entity.eventExtId != null ">{{row.entity.eventExtId}}</span></a></div>'
            },
            { visible: true, id: '3', name: 'createDate', displayName: 'Raised Date', /*minWidth: 170,*/ width: '165', enableColumnMenu: false },
            { visible: true, id: '4', name: 'notificationType', displayName: 'Notification Type', minWidth: 80, maxWidth: 150, width: '**', enableColumnMenu: false },
            { visible: true, id: '5', name: 'notificationSubType', displayName: 'Sub-Type', minWidth: 80, maxWidth: 130, width: '**', enableColumnMenu: false },
            { visible: true, id: '6', name: 'subject', displayName: 'Notification', minWidth: 300, width: '**', enableColumnMenu: false }
        ]
    };

    parentTracecolumnDefsArr = [
        { name: 'traceId', width: 220, enableColumnMenu: false, cellTemplate: "<div class=\"ui-grid-cell-contents\"><a ng-click=\"grid.appScope.clickTraceId(row.entity.traceId,row.entity.itemId)\"><span ng-if=\"!grid.appScope.isParentVisible\">{{row.entity.hidePrefix}}</span><span ng-if=\"grid.appScope.isParentVisible\">{{row.entity.traceExtId}}</span></a></div>" },
        { name: "traceExtId", visible: false },
        { name: "hidePrefix", visible: false },
        { name: 'itemType', enableColumnMenu: false },
        {
            name: 'actions',
            enableColumnMenu: false,
            cellTemplate: '<div class="cursor ui-grid-cell-contents"' +
                'ng-click="grid.appScope.showDialog($event,row.entity.traceId, row.entity.hidePrefix,row.entity.itemId,row.entity.deviceId)" title="Show Events">' +
                '<i ng-hide="row.isExpanded"  class="fa fa-caret-square-o-right" aria-hidden="true"></i>' +
                '<i ng-show="row.isExpanded">Hide Events</i>' +
                '</div>'
        },
        { name: 'itemId', enableColumnMenu: false },
        { name: 'itemName', enableColumnMenu: false },
        { name: 'itemLocation', enableColumnMenu: false },
        { name: 'disposition', enableColumnMenu: false },
        { name: 'lotId', enableColumnMenu: false },
        { name: 'expDate', enableColumnMenu: false, displayName: 'Exp. Date' },
        { name: 'qty', enableColumnMenu: false, headerCellClass: 'text-right', cellClass: 'text-right' },
        { name: 'mfrName', enableColumnMenu: false, displayName: 'Mfr. Name' }
    ];

    eventColumnDefArr = [
        {
            name: 'eventId',
            enableColumnMenu: false,
            cellTemplate: '<div class="ui-grid-cell-contents"><a ng-model="row.entity.delete" ng-click="grid.appScope.clickEventIdSubGrid(row.entity.eventId,row.entity.eventType,row.entity.eventStatusId)"><span ng-if="row.entity.eventExtId == null || !row.entity.eventExtId">{{row.entity.eventId}}</span><span ng-if="row.entity.eventExtId || row.entity.eventExtId != null ">{{row.entity.eventExtId}}</span></a></div>'
        },
        { name: 'eventType', enableColumnMenu: false },
        { name: 'eventDateTime', enableColumnMenu: false },
        { name: 'disposition', enableColumnMenu: false },
        { name: 'eventLocation', enableColumnMenu: false },
        { name: 'bizTransType', enableColumnMenu: false },
        { name: 'bizTransID', enableColumnMenu: false },
        { name: 'bizTransDate', enableColumnMenu: false },
        { name: 'sender', enableColumnMenu: false },
        { name: 'receiver', enableColumnMenu: false },
        { name: 'Attach', enableColumnMenu: false }
    ];

    $scope.eventItemList = {
        columnDefs: eventColumnDefArr,
        data: [],
        expandableRowTemplate: '<div ui-grid="row.entity.subGridOptions"></div>',
        expandableRowScope: {
          subGridVariable: 'subGridScopeVariable'
        },
        onRegisterApi: function (gridApi) {
            $scope.gridApiEvent = gridApi;
        }
    };
    
    function getSubItemsByTraceEntityId(currentIndex, level, traceId) {
        console.log('getSubItemsByTraceEntityId ' )
        var reqObject = {
            "traceEntityId": traceId,
            "isForward": $scope.toggle.isForwardTracing
        };
		
        traceItemDetailsService.getSubItemsByTraceEntityId().save(angular.toJson(reqObject)).$promise.then(function (response) {
            console.log('getSubItemsByTraceEntityId ' )
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                angular.forEach(response.data, function (v, i) {
                    var obj = {
                        "traceId": response.data[i].traceEntId,
                        "traceExtId": response.data[i].traceEntExtId,
                        "hidePrefix": response.data[i].traceEntExtId.substring(response.data[i].traceEntExtId.lastIndexOf('.') + 1),
                        "itemType": response.data[i].itemType,
                        "actions": "",
                        "itemId": response.data[i].itemTypeExtId,
                        "itemName": response.data[i].prodName,
                        "itemLocation": response.data[i].locName,
                        "disposition": response.data[i].iStringValue,
                        "lotId": response.data[i].lotId,
                        "expDate": response.data[i].expDate,
                        "qty": response.data[i].quantity,
                        "mfrName": response.data[i].mfrLocName,
                        "relatedQty" : response.data[i].relatedQty,
                        "hasChild": response.data[i].hasChild,
                        "$$treeLevel": level,
                        "deviceId": deviceId
                    }
                    $scope.itemInformationList.data.splice(+currentIndex + 1 + +i, 0, obj);
                });
            }
        }, function (error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }
    $scope.itemInformationList = {
        paginationPageSizes: [2, 4, 6],
        paginationPageSize: 4,
        virtualizationThreshold: 5000,
        enableSorting: true,
        showTreeExpandNoChildren: true,
        showTreeRowHeader: false,
        columnDefs: [

            {
                name: 'traceId',
                minWidth: 180,
                enableColumnMenu: false,
                width: '**',
                cellTemplate: "<div class=\"ui-grid-cell-contents\">" +
                    "<div style=\"float:left;\" class=\"ui-grid-tree-base-row-header-buttons\" ng-class=\"{'ui-grid-tree-base-header': row.treeLevel > -1 }\" ng-click=\"grid.appScope.toggleRow(row,evt)\">" +
                    "<i ng-class=\"{'ui-grid-lastLevel':  row.entity.hasChild==0 ,'ui-grid-icon-minus-squared': ( ( grid.options.showTreeExpandNoChildren && row.treeLevel > -1 ) || ( row.treeNode.children && row.treeNode.children.length > 0 ) ) && row.treeNode.state === 'expanded', 'ui-grid-icon-plus-squared': ( ( grid.options.showTreeExpandNoChildren && row.treeLevel > -1 ) || ( row.treeNode.children && row.treeNode.children.length > 0 ) ) && row.treeNode.state === 'collapsed'}\" ng-style=\"{'padding-left': grid.options.treeIndent * row.treeLevel + 'px'}\"></i> &nbsp;</div>" +
                    "<a ng-if=\"row.entity.itemType!=='Pallet'\" ng-click=\"grid.appScope.clickTraceId(row.entity.traceId,row.entity.itemId)\">" +
                    "<span ng-if=\"!grid.appScope.traceIdPrefix.isVisible\">{{row.entity.hidePrefix}}</span>" +
                    "<span ng-if=\"grid.appScope.traceIdPrefix.isVisible\">{{row.entity.traceExtId}}</span>" +
                    "</a>" +
                    "<span ng-if=\"row.entity.itemType==='Pallet'\">" +
                    "<span ng-if=\"!grid.appScope.traceIdPrefix.isVisible\">{{row.entity.hidePrefix}}</span>" +
                    "<span ng-if=\"grid.appScope.traceIdPrefix.isVisible\">{{row.entity.traceExtId}}</span>" +
                    "</span>" +
                    "</div>"
            },
            { name: 'itemType', enableColumnMenu: false, width: 100 },
            { name: 'traceExtId', visible: false, enableColumnMenu: false },
            { name: 'hidePrefix', visible: false, enableColumnMenu: false },
			{
                name: 'actions',
                enableColumnMenu: false,
                cellTemplate: '<div class="cursor ui-grid-cell-contents">' +
                    '<span ng-click="grid.appScope.showDialog($event,row.entity.traceId, row.entity.hidePrefix,row.entity.itemId,row.entity.lotId)" title="Show Events">' +
                    '<i ng-hide="row.isExpanded" class="fa fa-caret-square-o-right" aria-hidden="true"></i>' +
                    '<i ng-show="row.isExpanded">Hide Events</i></span>' +
                    '<span ng-click="grid.appScope.showMapForTracking(row.entity.traceId)" class="font-size-14 padding-left-15" title="Route Map" role="button" tabindex="0"><i class="fa fa-map-marker" aria-hidden="true"></i></span>'+
                    '</div>',
                width: 75
            },
            { name: 'itemId', enableColumnMenu: false, width: 150 },
            { name: 'itemName', enableColumnMenu: false, width: 120 },
            { name: 'itemLocation', enableColumnMenu: false, width: 150 },
            { name: 'disposition', enableColumnMenu: false, width: 120 },
            { name: 'lotId', enableColumnMenu: false, width: 80 },
            { name: 'expDate', enableColumnMenu: false, displayName: 'Exp. Date', width: 120 },
            { name: 'qty', enableColumnMenu: false, width: 60, headerCellClass: 'text-right', cellClass: 'text-right' },
            { name: 'relatedQty', enableColumnMenu: false, width: 120},
            { name: 'mfrName', enableColumnMenu: false, displayName: 'Mfr. Name', minWidth: 320, width: '**' }
        ],
        onRegisterApi: function (gridApi) {
            $scope.gridApi = gridApi;
            $scope.gridApi.treeBase.on.rowExpanded($scope, function (row) {
                /*$scope.gridApi.expandable.collapseAllRows();*/
                var brk = false;
                angular.forEach($scope.itemInformationList.data, function (v, i) {
                    var setTreeLevel = $scope.itemInformationList.data[i].$$treeLevel + 1;
                    if (row.entity.$$hashKey == $scope.itemInformationList.data[i].$$hashKey && !$scope.itemInformationList.data[i].nodeLoaded && !brk) {
                        getSubItemsByTraceEntityId(i, setTreeLevel, row.entity.traceId);
                        $scope.itemInformationList.data[i].nodeLoaded = true;
                        brk = true;
                    }
                });
            });

            $scope.gridApi.treeBase.on.rowCollapsed($scope, function (row) {
            /* $scope.gridApi.expandable.collapseAllRows();*/
            });
        }
    }

    $scope.topLevelTraceItemList = {
        showTreeExpandNoChildren: true,
        showTreeRowHeader: false,
        enableExpandableRowHeader: false,
        columnDefs : angular.copy(parentTracecolumnDefsArr),
        onRegisterApi: function (gridApi) {
            $scope.gridApiTopLevel = gridApi;
        },
        data : []
    }

    $scope.parentTraceItemList = {
        showTreeExpandNoChildren: true,
        showTreeRowHeader: false,
        columnDefs: angular.copy(parentTracecolumnDefsArr),
        onRegisterApi: function (gridApi) {
            $scope.gridApiParentLevel = gridApi;
        },
        data : []
    };

    $scope.itemSummaryList.data = [];

    getItemDetailsSummary(childtraceEntityId);
    getItemDetailsByTraceEntityId(childtraceEntityId);
    getParentTraceItemByTraceEntityId(childtraceEntityId);
    getRelatedNotificationByItemId(childtraceEntityId);

    function getItemDetailsSummary(traceId) {
        console.log("getItemDetailsSummary " , traceId);
        var reqObject = {
            "traceEntityId": traceId
        };

        traceItemDetailsService.getItemSummaryByTraceEntityId().save(angular.toJson(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $scope.itemSummaryList.data = response.data;
                
//                angular.forEach(response.data, function (v, i) {
//                    $scope.itemSummaryList.data.push({"ItemName": response.data[i].prodName, "ItemId": response.data[i].itemTypeExtId, "itemType": response.data[i].itemType, "qty": response.data[i].quantity}) ;
//                });
            }
        }, function (error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getParentTraceItemByTraceEntityId(traceId) {
        var reqObject = {
            "traceEntityId": traceId
        }

        traceItemDetailsService.getParentTraceItemByTraceEntityId().save(angular.toJson(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                var topLevelTraceItem = response.data.topLevelTraceItem;
                var parentTraceItem = response.data.parentTraceItem;
                if (topLevelTraceItem !== null) {
                    var topLevelTraceItemObj = {
                        "traceId": topLevelTraceItem.traceEntId,
                        "traceExtId": topLevelTraceItem.traceEntExtId,
                        "hidePrefix": topLevelTraceItem.traceEntExtId.substring(topLevelTraceItem.traceEntExtId.lastIndexOf('.') + 1),
                        "itemType": topLevelTraceItem.itemType,
                        "actions": "",
                        "itemId": topLevelTraceItem.itemTypeExtId,
                        "itemName": topLevelTraceItem.prodName,
                        "itemLocation": topLevelTraceItem.locName,
                        "disposition": topLevelTraceItem.iStringValue,
                        "lotId": topLevelTraceItem.lotId,
                        "expDate": topLevelTraceItem.expDate,
                        "qty": topLevelTraceItem.quantity,
                        "mfrName": topLevelTraceItem.mfrLocName
                    }
                    $scope.topLevelTraceItemList.data.push(topLevelTraceItemObj);
                    topLevelItem = {
                    		"traceId" : topLevelTraceItemObj.traceId,
                    		"packageId" : topLevelTraceItemObj.hidePrefix,
                    		"productId" : topLevelTraceItemObj.itemId
                    			}; 
                }
                if (parentTraceItem !== null) {
                    var parentTraceItemObj = {
                        "traceId": parentTraceItem.traceEntId,
                        "traceExtId": parentTraceItem.traceEntExtId,
                        "hidePrefix": parentTraceItem.traceEntExtId.substring(parentTraceItem.traceEntExtId.lastIndexOf('.') + 1),
                        "itemType": parentTraceItem.itemType,
                        "actions": "",
                        "itemId": parentTraceItem.itemTypeExtId,
                        "itemName": parentTraceItem.prodName,
                        "itemLocation": parentTraceItem.locName,
                        "disposition": parentTraceItem.iStringValue,
                        "lotId": parentTraceItem.lotId,
                        "expDate": parentTraceItem.expDate,
                        "qty": parentTraceItem.quantity,
                        "mfrName": parentTraceItem.mfrLocName
                    }
                    $scope.parentTraceItemList.data.push(parentTraceItemObj);
                }

            }

        }, function (error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    function getItemDetailsByTraceEntityId(traceId) {
        var reqObject = {
            "traceEntityId": traceId,
            "isForward": $scope.toggle.isForwardTracing
        };

        traceItemDetailsService.getItemDetailsByTraceEntityId().save(angular.toJson(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                $scope.itemInformationList.data = []; // CLear the list
                for (var i in response.data) {
                    var obj = {
                        "traceId": response.data[i].traceEntId,
                        "traceExtId": response.data[i].traceEntExtId,
                        "hidePrefix": response.data[i].traceEntExtId.substring(response.data[i].traceEntExtId.lastIndexOf('.') + 1),
                        "itemType": response.data[i].itemType,
                        "actions": "",
                        "itemId": response.data[i].itemTypeExtId,
                        "itemName": response.data[i].prodName,
                        "itemLocation": response.data[i].locName,
                        "disposition": response.data[i].iStringValue,
                        "lotId": response.data[i].lotId,
                        "expDate": response.data[i].expDate,
                        "qty": response.data[i].quantity,
                        "mfrName": response.data[i].mfrLocName,
                        "relatedQty" : response.data[i].relatedQty,
                        "hasChild": response.data[i].hasChild,
                        "$$treeLevel": 0
                    };
                    
                    $scope.findNearByProp.latitude = response.data[i].locLatitude;
                    $scope.findNearByProp.longitude = response.data[i].locLongitude;
                    $scope.findNearByProp.itemId = response.data[i].itemTypeExtId;
                    $scope.itemInformationList.data.push(obj);
                }
            }
        }, function (error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }
	//var treeLevelCount = 0; // remove me
    

    function getRelatedNotificationByItemId(traceId) {

        var reqObject = {
            "itemId": traceId
        }

        traceItemDetailsService.getRelatedNotificationByItemId().save(angular.toJson(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                for (var i in response.listData) {
                    if (response.listData[i].notificationId != null) {
                        $scope.notificationsList.data[i] = {
                            "notificationId": response.listData[i].notificationId,
                            "eventId": response.listData[i].eventId,
                            "notificationType": response.listData[i].notificationType,
                            "notificationSubType": response.listData[i].notificationSubType,
                            "createDate": response.listData[i].createDate,
                            "subject": response.listData[i].subject
                        }
                    }
                }
            }
        }, function (error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });
    }

    $scope.toggleRow = function (row, evt) {
        uiGridTreeBaseService.toggleRowTreeState($scope.gridApi.grid, row, evt);
        //$scope.gridApi.treeBase.toggleRowTreeState($scope.gridApi.grid.renderContainers.body.visibleRowCache[rowNum]);
    };

    $scope.toggleExpandNoChildren = function () {
        $scope.gridOptions.showTreeExpandNoChildren = !$scope.gridOptions.showTreeExpandNoChildren;
        $scope.gridApi.grid.refresh();
    }

    $scope.clickTraceId = function (selectedTraceId) {
        $scope.itemSummaryList.data = [];
        $scope.itemInformationList.data = [];
        $scope.topLevelTraceItemList.data = [];
        $scope.parentTraceItemList.data = [];
        $scope.itemInformationList.data = [];
        $scope.notificationsList.data = [];
        getItemDetailsSummary(selectedTraceId);
        getParentTraceItemByTraceEntityId(selectedTraceId);
        getItemDetailsByTraceEntityId(selectedTraceId);
        getItemDetailsSummary(selectedTraceId);
        getRelatedNotificationByItemId(selectedTraceId);
        childtraceEntityId = selectedTraceId;
    }

    $scope.clickEventId = function (eventId) {

        sharedDataService.setEventId(eventId);
        sharedDataService.setItemType(itemType);
        sharedDataService.setEventStatus(eventStatus);
        /*var url = $state.href('app.traceEventDetails', {eventId: eventId});
        $window.open(url, '_blank');*/

        var url = $state.href('app.traceEventDetails', { eventId: eventId }),
            height = $window.innerHeight * .90,
            width = $window.innerWidth * .92;
        $window.open(url, "", "width=" + width + ",height=" + height + ",left=60,top=50,scrollbars=1,resizable=1");
    }

    $scope.clickNotificationId = function (notificationId) {
        sharedDataService.setNotificationId(notificationId);
        $state.go('app.viewNotification');
    }

    $scope.showPrefixChild = function () {
        $scope.traceIdPrefix.isVisible = $scope.traceIdPrefix.isVisible === false ? true : false;
        //$scope.itemInformationList.columnDefs[0].width=$scope.traceIdPrefix.isVisible===true?320:220;
        $scope.itemInformationList.columnDefs[0].width = $scope.traceIdPrefix.isVisible === true ? 370 : 220;
        $scope.gridApi.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
    }
    $scope.showPrefixParent = function () {
        $scope.isParentVisible = $scope.isParentVisible === false ? true : false;
        $scope.parentTraceItemList.columnDefs[0].width = $scope.isParentVisible === true ? 320 : 220;
        $scope.topLevelTraceItemList.columnDefs[0].width = $scope.isParentVisible === true ? 320 : 220;

        $scope.gridApiTopLevel.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
        $scope.gridApiParentLevel.core.notifyDataChange(uiGridConstants.dataChange.COLUMN);
    }

    function clickEventIdSubGrid(eventId) {
        /*var url = $state.href('app.traceEventDetails', {eventId: eventId}); 
        $window.open(url, '_blank'); */
        $window.close();
        var url = $state.href('app.traceEventDetails', { eventId: eventId }),
            height = $window.innerHeight * .90,
            width = $window.innerWidth * .92;
        $window.open(url, "", "width=" + width + ",height=" + height + ",left=60,top=50,scrollbars=1,resizable=1");
    }
    $scope.showMapForTracking = function (traceId) {
    	var childTraceID;
		$scope.$emit("isShowHeaderFooter", false); // open the state as popup hence don't show header & footer
        //$scope.$broadcast("isShowHeaderFooter", false);
		
		if(traceId != null){
			childTraceID = traceId;
		}else{
			childTraceID = childtraceEntityId;

		}
        var url = $state.href('app.trackItem', { traceId: childTraceID }),
            height = $window.innerHeight * .85,
            width = $window.innerWidth * .90;

        $window.open(url, "", "width=" + width + ",height=" + height + ",left=60,top=50,scrollbars=1,resizable=1");
    }

    $scope.changeTraceToggle = function () {
    	$scope.toggle.isForwardTracing = !$scope.toggle.isForwardTracing;
        $scope.findNearByProp.latitude = '';
        $scope.findNearByProp.longitude = '';
        $scope.findNearByProp.itemId = '';
        getItemDetailsByTraceEntityId(childtraceEntityId);
    }

    $scope.openParentWindow = function () {
        $window.opener.parentCallback($scope.findNearByProp); //Registered in item controller
        $window.close();
    }
    
    $scope.showCollapse = function(panelModel, panelName) {
        if (panelName == 'parentTrace') {
            $scope.collapse.isParentTraceOpen = (panelModel === true) ? false : true;
        } else if (panelName == 'itemsSummary'){
            $scope.collapse.isItemsSummaryOpen = (panelModel === true) ? false : true;
        }
    }

    $scope.showDialog = showDialog;

    function showDialog($event, traceId, packageId, productId,selectedDeviceId ) {
        $scope.eventItemList.data = [];
        var currentLotId;
        if(topLevelItem === undefined){
        	topLevelItem = {
            		"traceId" : $scope.itemInformationList.data[0].traceId,
            		"packageId" : $scope.itemInformationList.data[0].hidePrefix,
            		"deviceId" : selectedDeviceId,
            		"productId" : $scope.itemInformationList.data[0].itemId
            			}
        }
      
         var reqObject = {
                "traceEntityId": traceId,
                "deviceId" : deviceId,
                "packageId" : topLevelItem.packageId,
                "productId" : topLevelItem.productId
            },
            alert;

        traceItemDetailsService.getEventsByTraceEntityId().save(angular.toJson(reqObject)).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus === "success") {
                var eventList = [];
                for (var i in response.listData) {
                    var obj = {
                        "eventId": response.listData[i].eventId,
                        "eventExtId": response.listData[i].eventExtId,
                        "eventType": response.listData[i].eventType,
                        "eventDateTime": response.listData[i].eventDateTime,
                        "disposition": response.listData[i].disposition,
                        "eventLocation": response.listData[i].eventLocationName,
                        "bizTransType": response.listData[i].bizTransType,
                        "bizTransID": response.listData[i].bizTransId,
                        "bizTransDate": response.listData[i].bizTransDate,
                        "eventStatusId": response.listData[i].eventStatusId,
                        "sender": response.listData[i].sender,
                        "receiver": response.listData[i].receiver,
                        "Attach": ''
                    }
                    eventList.push(obj);
                    /*$scope.eventItemList.data[i].subGridOptions.disableRowExpandable = true;*/
                }
                $scope.eventItemList.data = eventList;
                for(var i in $scope.eventItemList.data){
                	$scope.eventItemList.data[i].subGridOptions ={
                			 disableRowExpandable: true
                	}
                }
                if(response.sensorDataList.length){
                	var sensorDataList = [];
                	for(var i in response.sensorDataList){
                		sensorDataList.push(response.sensorDataList[i]);
                		var obj ={};
                		if(response.sensorDataList[i].excursionAttributes != undefined){
                			obj = {
                    				temperature : response.sensorDataList[i].excursionAttributes.indexOf("temperature") > -1,
                    				battery : response.sensorDataList[i].excursionAttributes.indexOf("battery") > -1,
                    				pressure : response.sensorDataList[i].excursionAttributes.indexOf("pressure") > -1,
                    				shock : response.sensorDataList[i].excursionAttributes.indexOf("shock") > -1,
                    				humidity : response.sensorDataList[i].excursionAttributes.indexOf("humidity") > -1,
                    				light : response.sensorDataList[i].excursionAttributes.indexOf("light") > -1,
                    				tilt : response.sensorDataList[i].excursionAttributes.indexOf("tilt") > -1
           
                    		}
                		}else{
                			
                			obj = {
                    				temperature : false,
                    				battery : false,
                    				pressure : false,
                    				shock : false,
                    				humidity : false,
                    				light : false,
                    				tilt : false
           
                    		}
                			
                		}
                		
                		sensorDataList[i].excursionAttr = obj;
                	}
                	
	                $scope.eventItemList.data[response.listData.length-1].subGridOptions = {
	                        columnDefs: [{
	                            name: 'deviceId',
	                            field: 'deviceIdentifier'
	                        },
	                        {
	                            name: 'temperature',
	                            field: 'temperature',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.excursionStatus==2 && row.entity.excursionAttr.temperature" >{{row.entity.temperature}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.temperature" >{{row.entity.temperature}}</span></div>'
	                        },
	                        {
	                            name: 'pressure',
	                            field: 'pressure',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.excursionStatus==2 && row.entity.excursionAttr.pressure" >{{row.entity.pressure}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.pressure" >{{row.entity.pressure}}</span></div>'
	                        },
	                        {
	                            name: 'humidity',
	                            field: 'humidity',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.excursionStatus==2 && row.entity.excursionAttr.humidity" >{{row.entity.humidity}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.humidity" >{{row.entity.humidity}}</span></div>'
	                        },
	                        {
	                            name: 'light',
	                            field: 'light',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.light==2 && row.entity.excursionAttr.light" >{{row.entity.light}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.light" >{{row.entity.light}}</span></div>'
	                        },
	                        {
	                            name: 'tilt',
	                            field: 'tilt',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.excursionStatus==2 && row.entity.excursionAttr.tilt" >{{row.entity.tilt}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.tilt" >{{row.entity.tilt}}</span></div>'
	                        },
	                        {
	                            name: 'battery',
	                            field: 'battery',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.excursionStatus==2 && row.entity.excursionAttr.battery" >{{row.entity.battery}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.battery" >{{row.entity.battery}}</span></div>'
	                        },
	                        {
	                            name: 'shock',
	                            field: 'shock',
	                            cellTemplate: '<div class="ui-grid-cell-contents"><span class="color-red font-weight-600" ng-if="row.entity.excursionStatus==2 && row.entity.excursionAttr.shock" >{{row.entity.shock}}</span><span class="font-weight-600" ng-if="!row.entity.excursionAttr.shock" >{{row.entity.shock}}</span></div>'
	                        },

	                        {
	                            name: 'statusTimeStamp',
	                            field: 'statusTimeStamp',
	                            visible: false
	                        },
	                        {
	                            name: 'latitude',
	                            field: 'latitude',
	                            visible: false
	                        },
	                        {
	                            name: 'longitude',
	                            field: 'longitude',
	                            visible: false
	                        },
	                        {
	                            name: 'excursionStatus',
	                            field: 'excursionStatus',
	                            visible: false
	                        },
	                        {
	                            name: 'excursionAttr',
	                            field: 'excursionAttr',
	                            visible: false
	                        }

	                    ],
	                        data: sensorDataList,
	                        disableRowExpandable: false
	                };
	                
	                $timeout(function() {
	                    var rows = $scope.gridApiEvent.grid.rows;
	                    var entity = (rows.length && rows[rows.length-1]) ? rows[rows.length-1].entity : null;
	                    $scope.gridApiEvent.expandable.toggleRowExpansion(entity);
	                });
                }
                
            }
        }, function (error) {
            if (error.status == 401 && error.statusText == "Unauthorized") {
                $scope.$emit('genericErrorEvent', error);
            }
        });

        var parentEl = angular.element(document.querySelector('md-content'));
        alert = $mdDialog.alert({
            parent: parentEl,
            targetEvent: $event,
            template: '<md-dialog aria-label="Sample Dialog">' +
                '<md-toolbar>' +
                '  <div class="md-toolbar-tools">' +
                ' <h2>Events (Trace Id: {{traceId}})</h2>' +
                ' <span flex></span>' +
                ' <button type="button" class="close" aria-label="Close" ng-click="closeDialog()">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button> ' +
                ' </div>' +
                '</md-toolbar>' +
                '  <md-content>' +
                ' <div ui-grid="eventItemList" class="grid" ui-grid-resize-columns ui-grid-expandable></div>' +
                '  </md-content>' +
                '  <div class="md-actions">' +
                '    <div class="clear button_secondary" data-dismiss="modal" ng-click="closeDialog()">' +
                '  <span class="font-size-14">Cancel</span>' +
                '  </div>' +
                '   </div>' +
                '</md-dialog>',
            locals: {
                eventItemList: $scope.eventItemList,
                closeDialog: $scope.closeDialog,
                traceId: $scope.traceId
            },
            bindToController: true,
            controller: DialogController
        });

        function DialogController($scope, $mdDialog, eventItemList) {
            $scope.eventItemList = eventItemList;
            $scope.traceId = traceId;
            $scope.closeDialog = function () {
                $mdDialog.hide();
            }

            $scope.clickEventIdSubGrid = function (eventId, itemType, eventStatus) {
                /*var url = $state.href('app.traceEventDetails', {eventId: eventId}); 
                $window.open(url, '_blank'); */
                var obj = { itemType: itemType, eventStatus: eventStatus };

                /*$sessionStorage.itemType=itemType;
                $sessionStorage.eventStatus=eventStatus;*/
                $window.eventObj = JSON.stringify(obj);
                window.opener.eventObj = undefined;
                $state.go('app.traceEventDetails', { eventId: eventId, deviceId:deviceId });
                $mdDialog.hide();
            }
        }
        $mdDialog
            .show(alert)
            .finally(function () {
                alert = undefined;
            });
    }
}
angular.module('rfxcelApp').controller('traceItemDetailsController', traceItemDetailsController);

traceItemDetailsController.$inject = ['$scope', '$state', 'sharedDataService', '$filter', 'traceItemDetailsService', 'uiGridTreeViewConstants', 'uiGridTreeBaseService', 'uiGridConstants', '$stateParams', '$window', '$mdDialog','$timeout'];