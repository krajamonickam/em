angular.module('rfxcelApp').controller('forgotPasswordCtrl', forgotPasswordCtrl);
forgotPasswordCtrl.$inject = ['$scope', '$state', 'loginService', '$filter', '$rootScope', '$location', 'sharedDataService'];

function forgotPasswordCtrl($scope, $state, loginService, $filter, $rootScope, $location, sharedDataService) {
	$scope.appTitle = $rootScope.appTitle;
    $scope.username = $location.search().userName;
    $scope.authToken = $location.search().token;
    $scope.orgId = $location.search().orgId;
    
    $scope.loginId = $location.search().loginId;
    $scope.userId = $location.search().userId;
    $scope.action = $location.search().action;
   
    $scope.forgotPassword = function () {
        var reqObject = {
            "userLogin": $scope.username,
            "url": $location.$$protocol + '://' + $location.$$host + ':' + $location.$$port + sharedDataService.getURLPath() +'/#/resetPassword'
        }
        loginService.forgotPassword.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
                $scope.userName = $scope.username;
                $state.go('passwordLanding');
            } else {
                $scope.errorMessage = response.responseMessage;
                $state.go('forgotPassword');
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $scope.resetPassword = function () {
        if ($scope.password != $scope.confirmpassword) {
            $scope.errorMessage = 'Password and Confirmed Password did not match';
            return false;
        }
        var reqObject = {
            "userLogin": $scope.username,
            "password": $scope.password,
            "token": $scope.authToken,
            "orgId": $scope.orgId
        }
        loginService.resetPassword.post(reqObject).$promise.then(function (response) {
            if (response.responseStatus === "error" && response.responseCode === 403) {
                $scope.$emit('genericErrorEvent', response);
                return;
            }
            if (response.responseStatus == "success") {
            	sharedDataService.setSuccessMessage(response.responseMessage);
                $state.go('login');
            } else {
                $scope.errorMessage = response.responseMessage;
                $state.go('resetPassword');
            }
        }, function (error) {
            console.log(error);
        })
    }
    
    $scope.createPassword = function () {   	
     	 $state.go('redirectLoginPage');
   };
}