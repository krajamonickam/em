package com.rfxcel.cache;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.beans.Geofence;
import com.rfxcel.sensor.dao.CommissionDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.Sensor;
/**
 * @author Tejshree kachare
 * @author Sachin Kohale
 * @since Feb 24 2017
 *
 */
public class DeviceCache {

	private static Logger logger = Logger.getLogger(DeviceCache.class);
	private static DeviceCache singleton;
	
	/**
	 * Cache of Devices' last location
	 */
	private ConcurrentHashMap<String, Sensor> deviceLastLocations;
	/**
	 * Cache to store Geopoints for each profile
	 */
	private HashMap<Integer, HashMap<String, Geofence>> profileGeopoints;
	/**
	 * Cache to store device's geopoint state
	 */
	private HashMap<String, HashMap<String, Integer>> deviceGeopointState;
	/**
	 * Cache to store attributes defined for a given deviceType
	 */
	private HashMap<Integer, HashMap<String, Attribute>> deviceTypeAttributeCache;
	/**
	 * Cache to store deviceType of each deviceId
	 */
	private ConcurrentHashMap<String,Integer> deviceIdTypeMapping;
	/**
	 * Cache to store deviceId's battery value mapping
	 */
	private HashMap<String, Long> deviceIdBatteryValueMapping;
	/**
	 * Cache to store orgId for each deviceId
	 */
	private HashMap<String, Integer> deviceOrgMapping;


	private DeviceCache(){
		deviceLastLocations = new ConcurrentHashMap<String, Sensor>();
		profileGeopoints = new HashMap<Integer, HashMap<String, Geofence>>();
		deviceGeopointState = new HashMap<String, HashMap<String, Integer>>();
		deviceTypeAttributeCache = new HashMap<Integer, HashMap<String, Attribute>>();
		deviceIdTypeMapping = new ConcurrentHashMap<String,Integer>();
		deviceIdBatteryValueMapping = new HashMap<String, Long>();
		deviceOrgMapping = new HashMap<String, Integer>();
	
	}

	/**
	 * 
	 * @return
	 */
	public static DeviceCache getInstance() {
		if(singleton == null){
			synchronized (DeviceCache.class) {
				if(singleton == null){
					singleton = new DeviceCache();
					singleton.populateDeviceLastLocation();
					singleton.populateProfileGeopoints();
					singleton.populateDeviceTypeAttrCache();
					singleton.populateDeviceIdTypeMapping();
					singleton.populateDeviceIdBatteryValueMapping();
					singleton.populateDeviceOrgMapping();
				
				}
			}
		}
		return singleton;
	}

	/**
	 * 
	 */
	public void reset(){
		populateDeviceLastLocation();
		populateProfileGeopoints();
		populateDeviceTypeAttrCache();
		populateDeviceIdTypeMapping();
		populateDeviceIdBatteryValueMapping();
		populateDeviceOrgMapping();
		
	}


	/**
	 * Populate Device's Last Location for all the active associations
	 */
	private void populateDeviceLastLocation() {
		clearDeviceLastLocation();
		deviceLastLocations.putAll(SendumDAO.getInstance().getAssociatedSensorData());
	}

	/**
	 * clear devices last location data
	 */
	private void clearDeviceLastLocation(){
		deviceLastLocations.clear();
	}
	/**
	 * <p> Get last location data for device</p>
	 * @param deviceId
	 * @return
	 */
	public Sensor getDeviceLastLocation(String deviceId){
		if(deviceId==null){
			return null;
		}
		Sensor sensor = deviceLastLocations.get(deviceId);
		return sensor;
	}

	/**
	 * <p>Add last Location data for a device </p>
	 * @param deviceId
	 * @param sensor
	 */
	public void putDeviceLastLocation(String deviceId, Sensor sensor){
		deviceLastLocations.put(deviceId, sensor);
	}
	
	/**
	 * <p> remove last location data for device from cache </p>
	 * @param deviceId
	 */
	public void removeDeviceLastLocation(String deviceId){
		deviceLastLocations.remove(deviceId);
	}
	
	/**
	 * <p> remove last location data for devices from cache </p>
	 * @param deviceIds
	 */
	public void removeDeviceLastLocation(ArrayList<String> deviceIds) {
		try{
			for(String deviceId: deviceIds){
				deviceLastLocations.remove(deviceId);
			}
		}catch(Exception e){
			logger.error("Exception while removing deviceLastLocation from cache");
		}
	}
	/**  ***************** Profile Geopoint cache********************** **/
	/**
	 * 
	 */
	private void populateProfileGeopoints() {
		synchronized (profileGeopoints) {
			clearProfileGeopoints();
			profileGeopoints.putAll(SendumDAO.getInstance().getProfileIdGeoPoints());
		}
	}

	/**
	 * Clear profile Geopoints
	 */
	private void clearProfileGeopoints(){
		profileGeopoints.clear();
	}

	/**
	 * 
	 * @param profileId
	 * @param geofenceNumber
	 * @return
	 */
	public Geofence getGeofence(Integer profileId, String geofenceNumber){
		synchronized (profileGeopoints) {
			if(profileId != null && geofenceNumber!= null){
				// if geopoint is not present in cache read the values from DB
				if(profileGeopoints.get(profileId) == null){
					addGeoPoints(profileId);
				}
				if(profileGeopoints.get(profileId) == null){
					return null;
				}else{
					return profileGeopoints.get(profileId).get(geofenceNumber);
				}
			}
			return null;
		}
	}


	/**
	 * <p> Get geo points for profile
	 * @param profileId
	 * @return
	 */
	public  Collection<Geofence> getGeopoints(Integer profileId){
		synchronized (profileGeopoints) {
			// if geopoint is not present in cache read the values from DB
			if(profileId != null){
				if(profileGeopoints.get(profileId) == null){
					addGeoPoints(profileId);
				}

				if(profileGeopoints.get(profileId) == null){
					return null;
				}else{
					return profileGeopoints.get(profileId).values();
				}
			}
			return null;
		}
	}
	
	/**
	 * <p> Add profile geo points mapping </p>
	 * @param profileId
	 */
	public void addGeoPoints(Integer profileId){
		synchronized (profileGeopoints) {
			HashMap<String, Geofence> geopointsMap = SendumDAO.getInstance().getGeoPointsForProfile(profileId);
			if(geopointsMap.size() > 0) {
				profileGeopoints.put(profileId, geopointsMap);
			} else {
				profileGeopoints.remove(profileId);
			}
		}
	}

	/**
	 * <p> Remove profile geopoints mapping
	 * @param profileId
	 */
	public void removeProfileGeoPoints(Integer profileId){
		synchronized (profileGeopoints) {
			profileGeopoints.remove(profileId);
		}
	}
	
	/**
	 * <p> Remove profile geopoints mapping
	 * @param profileIds
	 */
	public void removeProfileGeoPoints(ArrayList<Integer> profileIds) {
		synchronized (profileGeopoints) {
			for(Integer profileId : profileIds){
				profileGeopoints.remove(profileId);
			}
		}
	}
	
	/**  ***************** Device Geopoint State cache********************** **/
	/**
	 * <p> get state of geo point for a device and geofence number
	 * @param deviceId
	 * @param geofenceNumber
	 * @return
	 */
	public Integer getGeoPointState(String deviceId, String geofenceNumber){
		synchronized (deviceGeopointState) {
			if(deviceId != null && geofenceNumber!= null){
				// if geopoint is not present in cache that means device is reaching geo point for the first time
				if(deviceGeopointState.get(deviceId) == null){
					return null;
				}else{
					return deviceGeopointState.get(deviceId).get(geofenceNumber);
				}
			}
			return null;
		}
	}

	/**
	 * <p> Set geopoint state of geofence number for a device
	 * @param deviceId
	 * @param geofenceNumber
	 * @param state
	 */
	public void setGeopointState(String deviceId, String geofenceNumber, Integer state){
		synchronized (deviceGeopointState) {
			HashMap<String, Integer> geopointsState = deviceGeopointState.get(deviceId) ;
			if(geopointsState == null){
				geopointsState = new HashMap<String, Integer>();
				geopointsState.put(geofenceNumber, state);
				deviceGeopointState.put(deviceId, geopointsState);
			}else{
				geopointsState.put(geofenceNumber, state);
			}
		}
	}

	/**
	 * <p> Reset geopoints' state for a device
	 * @param deviceId
	 */
	public void resetGeopointsState(String deviceId){
		synchronized (deviceGeopointState) {
			deviceGeopointState.remove(deviceId);
		}
	}

	/**
	 * <p> remove devices geopoint state from cache
	 * @param deviceIds
	 */
	public void removeDeviceGeopointState(ArrayList<String> deviceIds) {
		try{
			synchronized (deviceGeopointState) {
				for(String deviceId: deviceIds){
					deviceGeopointState.remove(deviceId);
				}
			}
		}catch(Exception e){
			logger.error("Exception while removing device geopoint state from cache");
		}
	}
	/**  ***************** Device Type Attribute cache********************** **/
	

	// Populate cache
	public void populateDeviceTypeAttrCache() {
		//Synchronize on cache while clearing and populating, so that thread executing getAttribute will have to wait for it to complete
		synchronized (deviceTypeAttributeCache) {
			// Clear cache
			clearDeviceAttrCache();
			deviceTypeAttributeCache.putAll(SendumDAO.getInstance().getAttributesForDeviceTypes());
		}
	}

	/**
	 * clear all the data from ruleCache
	 */
	private void clearDeviceAttrCache() {
		deviceTypeAttributeCache.clear();
	}

	/**
	 * <p> to insert attributes for a device type in cache
	 * @param deviceTypeId
	 */
	public void addDeviceTypeAttrbute(Integer deviceTypeId){
		synchronized (deviceTypeAttributeCache) {
			HashMap<String, Attribute> attributes = SendumDAO.getInstance().getAttributesForDeviceType(deviceTypeId);
			deviceTypeAttributeCache.put(deviceTypeId, attributes);
		}
	}
	/***
	 * <p> get attribute with given attribute name for a device
	 * @param deviceId
	 * @param attributeName
	 * @return
	 */
	public Attribute getAttribute(String deviceId, String attributeName) {
		HashMap<String, Attribute> attrMap;
		Attribute attribute = null;
		try {
			Integer deviceTypeId = getDeviceTypeId(deviceId);
			attrMap = deviceTypeAttributeCache.get(deviceTypeId);
			if(attrMap == null){
				return null;
			}else{
				attribute = attrMap.get(attributeName);
			}
		} catch (Exception e) {
			logger.error(" Exception while getting attribute : " +attributeName+ " for device Id "+ deviceId+ " " + e.getMessage());
		}
		return attribute;
	}
	
	/**
	 * <p> Get attribute name list </p>
	 * @return
	 */
	public List<String> getAtttributeNameList(String deviceId){
		List<String> attrNameList = new ArrayList<String>();
		Integer deviceTypeId = deviceIdTypeMapping.get(deviceId);
		HashMap<String, Attribute> attributes = deviceTypeAttributeCache.get(deviceTypeId);
			for(String attrName : attributes.keySet()){
				attrNameList.add(attrName);
			}			
		return attrNameList;
	}
	
	/**
	 * <p>Remove attributes for deviceType</p>
	 * @param deviceTypeId
	 */
	public void removeDeviceType(Integer deviceTypeId){
		synchronized (deviceTypeAttributeCache) {
			deviceTypeAttributeCache.remove(deviceTypeId);
		}
	}
	
	/**
	 * <p> Remove device type attribute mapping from cache
	 * @param deviceTypeIds
	 */
	public void removeDeviceType(ArrayList<Integer> deviceTypeIds) {
		synchronized (deviceTypeAttributeCache) {
			for(Integer deviceTypeId: deviceTypeIds){
				deviceTypeAttributeCache.remove(deviceTypeId);
			}
		}
	}
	
	/**  ***************** DeviceId-Type cache********************** **/
	/**
	 * 
	 */
	private void populateDeviceIdTypeMapping() {
		clearDeviceIdTypeMapping();
		deviceIdTypeMapping.putAll(CommissionDAO.getInstance().getDeviceIdTypeMapping()); 
	}

	/**
	 * clear 
	 */
	private void clearDeviceIdTypeMapping() {
		deviceIdTypeMapping.clear();
	}
	/**
	 * <p> Get device type of a device
	 * @param deviceId
	 * @return
	 */
	public Integer getDeviceTypeId(String deviceId){
		return deviceIdTypeMapping.get(deviceId);
	}

	/**
	 * <p> Add device Id and device Type mapping
	 * @param deviceId
	 * @param deviceType
	 */
	public void addDeviceIdTypeMapping(String deviceId, Integer deviceTypeId){
		deviceIdTypeMapping.put(deviceId, deviceTypeId);
	}

	/**
	 * <p> Remove device Id - device Type mapping
	 * @param deviceId
	 */
	public void removeDeviceIdTypeMapping(String deviceId){
		deviceIdTypeMapping.remove(deviceId);
	}

	/**
	 * <p> Remove deviceIds' type mapping
	 * @param deviceIds
	 */
	public void removeDeviceIdTypeMapping(ArrayList<String> deviceIds) {
		try{
			for(String deviceId: deviceIds){
				deviceIdTypeMapping.remove(deviceId);
			}
		}catch(Exception e){
			logger.error("Exception while removing deviceId type mapping from cache");
		}
	}
	
	/**  ***************** Device Id battery value mapping********************** **/
	/**
	 * Populate deviceId battery value mapping cache
	 */
	private void populateDeviceIdBatteryValueMapping() {	
		synchronized (deviceIdBatteryValueMapping) {
			deviceIdBatteryValueMapping.putAll(CommissionDAO.getInstance().getDeviceIdBatteryValueMapping()); 
		}
	}
	/**
	 * <p> Get battery value for a cache
	 * @param deviceId
	 * @return
	 */
	public Long getBatteryValue(String deviceId){
		return deviceIdBatteryValueMapping.get(deviceId);
	}
	/**
	 * <p> Add deviceId battery value mapping to cache
	 * @param deviceId
	 * @param batteryValue
	 */
	public void putDeviceIdBatteryValueMapping(String deviceId, Long batteryValue){
		synchronized (deviceIdBatteryValueMapping) {
			deviceIdBatteryValueMapping.put(deviceId, batteryValue);
		}
	}
	
	/**
	 * <p> Remove deviceId battery Value mapping
	 * @param deviceId
	 */
	public void removeDeviceIdBatteryValueMapping(String deviceId){
		synchronized (deviceIdBatteryValueMapping) {
			deviceIdBatteryValueMapping.remove(deviceId);
		}
	}

	/**
	 * <p> Remove deviceIds batteryValue mapping
	 * @param deviceId
	 */
	public void removeDeviceIdBatteryValueMapping(ArrayList<String> deviceIds) {
		try{
			synchronized (deviceIdBatteryValueMapping) {
				for(String deviceId: deviceIds){
					deviceIdBatteryValueMapping.remove(deviceId);
				}
			}
		}catch(Exception e){
			logger.error("Exception while removing deviceId battery value mapping from cache");
		}
	}
	/**  ***************** Device Id Org mapping********************** **/
	/**
	 * Populate deviceId and org mapping in cache
	 */
	private void populateDeviceOrgMapping() {
		synchronized (deviceOrgMapping) {
			clearDeviceOrgMapping();
			deviceOrgMapping.putAll(CommissionDAO.getInstance().getDeviceOrgMapping()); 
		}
	}

	/**
	 * clear device Id org mapping from cache
	 */
	private void clearDeviceOrgMapping() {
		deviceOrgMapping.clear();
	}
	/**
	 * <p> Get org id for a device
	 * @param deviceId
	 * @return
	 */
	public Integer getDeviceOrgId(String deviceId){
		return deviceOrgMapping.get(deviceId);
	}

	/**
	 * <p> Add deviceId org mapping to cache
	 * @param deviceId
	 * @param deviceType
	 */
	public void addDeviceOrgMapping(String deviceId, Integer orgId){
		synchronized (deviceOrgMapping) {
			deviceOrgMapping.put(deviceId, orgId);
		}
	}
	
	/**
	 * <p>Remove deviceId org mapping from cache </p>
	 * @param deviceId
	 */
	public void removeDeviceOrgMapping(String deviceId){
		synchronized (deviceOrgMapping) {
			deviceOrgMapping.remove(deviceId);
		}
	}

	/**
	 * <p>Remove deviceIds org mapping from cache </p>
	 * @param deviceIds
	 */
	public void removeDeviceOrgMapping(ArrayList<String> deviceIds) {
		try{
			synchronized (deviceOrgMapping) {
				for(String deviceId: deviceIds){
					deviceOrgMapping.remove(deviceId);
				}
			}
		}catch(Exception e){
			logger.error("Exception while removing device Org mapping from cache");
		}
	}
}
