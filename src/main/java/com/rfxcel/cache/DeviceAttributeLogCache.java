package com.rfxcel.cache;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.dao.SendumDAO;

/**
 * 
 * @author Tejshree Kachare
 * @Since Feb 21 2017
 *
 */
public class DeviceAttributeLogCache {

	private static Logger logger = Logger.getLogger(DeviceAttributeLogCache.class);
	private static DeviceAttributeLogCache singleton ;

	private static ConcurrentHashMap<String,ConcurrentHashMap<String, DeviceInfo>> deviceAttrNotificationLimitLog ;
	private static ConcurrentHashMap<String,ConcurrentHashMap<String, DeviceInfo>> deviceAttrExcursionTimeLog ;
	private static ConcurrentHashMap<String, DeviceInfo> sameLocationdeviceNotificationLog;
	private static ConcurrentHashMap<String, DeviceInfo> deviceBactorialLog;

	private DeviceAttributeLogCache() {
		deviceAttrNotificationLimitLog = new ConcurrentHashMap<String,ConcurrentHashMap<String, DeviceInfo>>();
		deviceAttrExcursionTimeLog = new ConcurrentHashMap<String,ConcurrentHashMap<String, DeviceInfo>>();
		sameLocationdeviceNotificationLog = new ConcurrentHashMap<String, DeviceInfo>();
		deviceBactorialLog = new ConcurrentHashMap<String, DeviceInfo>();
	}

	public static DeviceAttributeLogCache getInstance() {
		if (singleton == null) {
			synchronized (DeviceAttributeLogCache.class) {
				if (singleton == null) {
					singleton = new DeviceAttributeLogCache();
					populateDeviceAttrNotificationLimitLog();
					populateDeviceAttrExcursionTimeLog();
					populateSameLocationDeviceNotificationLimitLog();
					populateDeviceBacterialLog();
				}
			}
		}
		return singleton;
	}

	public void reset(){
		populateDeviceAttrNotificationLimitLog();
		populateDeviceAttrExcursionTimeLog();
		populateSameLocationDeviceNotificationLimitLog();
		populateDeviceBacterialLog();
	}
	/**
	 * 
	 */
	private static void populateDeviceAttrExcursionTimeLog() {
		deviceAttrExcursionTimeLog = SendumDAO.getInstance().getLattestDeviceAttributeLog(0);

	}

	/**
	 * 
	 */
	private static void populateDeviceAttrNotificationLimitLog() {
		deviceAttrNotificationLimitLog = SendumDAO.getInstance().getLattestDeviceAttributeLog(1);
	}

	/**
	 *  
	 */
	private static void populateSameLocationDeviceNotificationLimitLog(){
		sameLocationdeviceNotificationLog = SendumDAO.getInstance().getDeviceLogForLatitude();
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 * @return
	 */
	public DeviceInfo getNotificationLimitLog(String deviceId, String attrName) {
		if(deviceId == null){
			return null;
		}
		if(attrName == null){
			return null;
		}
		ConcurrentHashMap<String, DeviceInfo> attriMap = deviceAttrNotificationLimitLog.get(deviceId);
		if(attriMap == null){
			return null;
		}else{
			return attriMap.get(attrName);
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 * @param deviceInfo
	 */
	public void putNotificationLimitLog(String deviceId, String attrName, DeviceInfo deviceInfo) {

		if(deviceAttrNotificationLimitLog.get(deviceId) == null){
			ConcurrentHashMap<String, DeviceInfo> attriMap = new ConcurrentHashMap<>();
			attriMap.put(attrName, deviceInfo);
			deviceAttrNotificationLimitLog.put(deviceId, attriMap);
		}
		else{
			deviceAttrNotificationLimitLog.get(deviceId).put(attrName, deviceInfo);
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 */
	public void removeNotificationLimitLog(String deviceId, String attrName) {
		if(deviceAttrNotificationLimitLog.get(deviceId) != null){
			deviceAttrNotificationLimitLog.get(deviceId).remove(attrName);
		}
	}

	/**
	 * 
	 * @param deviceId
	 */
	public void removeNotificationLimitLog(String deviceId) {
		if(deviceAttrNotificationLimitLog.get(deviceId) != null){
			deviceAttrNotificationLimitLog.remove(deviceId);
		}
	}
	/**
	 * <p> Remove notification limit log
	 * @param deviceIds
	 */
	public void removeNotificationLimitLog(ArrayList<String> deviceIds) {
		try{
			for(String deviceId : deviceIds){
				deviceAttrNotificationLimitLog.remove(deviceId);
			}
		}catch(Exception e){
			logger.error("Exception while removing Notification limit log from cache");
		}
	}
	
	
	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 * @return
	 */
	public DeviceInfo getExcursionTimeLog (String deviceId, String attrName){
		if(deviceId == null){
			return null;
		}
		if(attrName == null){
			return null;
		}
		ConcurrentHashMap<String, DeviceInfo> attriMap = deviceAttrExcursionTimeLog.get(deviceId);
		if(attriMap == null){
			return null;
		}else{
			return attriMap.get(attrName);
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 * @param deviceInfo
	 */
	public void putExcursionTimeLog(String deviceId, String attrName, DeviceInfo deviceInfo) {
		if(deviceAttrExcursionTimeLog.get(deviceId) == null){
			ConcurrentHashMap<String, DeviceInfo> attriMap = new ConcurrentHashMap<>();
			attriMap.put(attrName, deviceInfo);
			deviceAttrExcursionTimeLog.put(deviceId, attriMap);
		}
		else{
			deviceAttrExcursionTimeLog.get(deviceId).put(attrName, deviceInfo);
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 */
	public void removeExcursionTimeLog(String deviceId, String attrName) {
		if(deviceAttrExcursionTimeLog.get(deviceId) != null){
			deviceAttrExcursionTimeLog.get(deviceId).remove(attrName);
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 */
	public void removeExcursionTimeLog(String deviceId) {
		if(deviceAttrExcursionTimeLog.get(deviceId) != null){
			deviceAttrExcursionTimeLog.remove(deviceId);
		}
	}

	/**
	 * <p> Remove excursion time logs for deviceIds
	 * @param deviceIds
	 */
	public void removeExcursionTimeLog(ArrayList<String> deviceIds) {
		try{
			for(String deviceId: deviceIds){
				deviceAttrExcursionTimeLog.remove(deviceId);
			}
		}catch(Exception e){
			logger.error("Exception while removing Excursion time log from cache");
		}
	}
	/**
	 * 
	 * @param deviceId
	 * @return
	 */
	public DeviceInfo getSameLocationLimitLog(String deviceId) {
		if(deviceId == null){
			return null;
		}
		DeviceInfo deviceInfo = sameLocationdeviceNotificationLog.get(deviceId);
		return deviceInfo;
	}

	/**
	 * 
	 * @param deviceId
	 * @param deviceInfo
	 */
	public void putSameLocationLimitLog(String deviceId, DeviceInfo deviceInfo) {
		sameLocationdeviceNotificationLog.put(deviceId, deviceInfo);		
	}

	/**
	 * <p> Remove same location Notification limit for device
	 * @param deviceId
	 * 
	 */
	public void removeSameLocationNotificationLimitLog(String deviceId) {
		if(sameLocationdeviceNotificationLog.get(deviceId) != null){
			sameLocationdeviceNotificationLog.remove(deviceId);
		}
	}
	
	/**
	 * <p> Remove same location Notification limit for devices
	 * @param deviceIds
	 */
	public void removeSameLocationNotificationLimitLog(ArrayList<String> deviceIds) {
		try{
			for(String deviceId :  deviceIds){
				sameLocationdeviceNotificationLog.remove(deviceId);
			}
		}catch(Exception e){
			logger.error("Exception while removing Sam eLocationNotification limit count from cache");
		}
	}

	/**
	 * 
	 */
	private static void populateDeviceBacterialLog() {
		deviceBactorialLog.putAll(SendumDAO.getInstance().getDeviceBactLog());
	}


	/**
	 * get BacterialLog for given Device
	 * @param deviceId
	 * @return
	 */
	public DeviceInfo getDeviceBacterialLog(String deviceId) {
		if(deviceId == null){
			return null;
		}
		DeviceInfo deviceInfo = deviceBactorialLog.get(deviceId);
		return deviceInfo;
	}


	/**
	 * 
	 * @param deviceId
	 * @param deviceInfo
	 */
	public void putDeviceBacterialLog(String deviceId, DeviceInfo deviceInfo) {
		deviceBactorialLog.put(deviceId, deviceInfo);
	}

	/**
	 * <p> remove device bacterial growth logs for device
	 * @param deviceId
	 */
	public void removeDeviceBacterialGrowthLog(String deviceId) {
		if(deviceBactorialLog.get(deviceId) != null){
			deviceBactorialLog.remove(deviceId);
		}
	}
	
	/**
	 * <p> remove device bacterial growth logs for devices
	 * @param deviceIds
	 */
	public void removeDeviceBacterialGrowthLog(ArrayList<String> deviceIds) {
		try{
			for(String deviceId : deviceIds){
				deviceBactorialLog.remove(deviceId);
			}
		}catch(Exception e){
			logger.error("Exception while removing device bacterial growth log from cache");
		}

	}

}