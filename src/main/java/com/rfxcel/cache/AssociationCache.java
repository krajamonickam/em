package com.rfxcel.cache;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;

import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.dao.ExcursionLogDAO;

public class AssociationCache {

	private static Logger logger = Logger.getLogger(AssociationCache.class);
	private static AssociationCache singleton;

	/**
	 * Cache storing active shipments with deviceId as key and PackageId as value
	 * DeviceIds are unique across all the organizations
	 */
	private ConcurrentHashMap<String, String> activeAssociationCache = new ConcurrentHashMap<String, String>();

	/**
	 * Cache storing shipment's excursion status with deviceId-packageId combination as key and excursion status as value
	 */
	private ConcurrentHashMap<MultiKey, String> shipmentStatus =  new ConcurrentHashMap<MultiKey, String>();

	/**
	 * Cache to store geopoint home for each profile, so profileId is key and geopoint name is value
	 */
	private ConcurrentHashMap<Integer, String> shipmentHomeGeopoint =  new ConcurrentHashMap<Integer, String>();

	/**
	 * Cache to store profile mapped to a shipment
	 */
	private ConcurrentHashMap<MultiKey, Integer> activeShipmentProfileMapping = new ConcurrentHashMap<MultiKey, Integer>();

	private ConcurrentHashMap<Integer, Integer> profileTempFrequency = new ConcurrentHashMap<Integer, Integer>();
	
	private static ConcurrentHashMap<MultiKey, HashSet<String>> shipmentExcursionAttrList = new ConcurrentHashMap<MultiKey, HashSet<String>>();
	
	private AssociationCache() {
	}

	public static AssociationCache getInstance() {
		if (singleton == null) {
			synchronized (AssociationCache.class) {
				if (singleton == null) {
					singleton = new AssociationCache();
					singleton.populateActiveAssociationCache();
					singleton.populateAssociationStatus();
					singleton.populateShipmentsHomeGeopoint();
					singleton.populateActiveShipmentProfileMapping();
					singleton.populateProfileTempFrequency();
					singleton.populateShipmentExcursionAttr();
				}
			}
		}
		return singleton;
	}

	/**
	 * Reset all the cache present in AssociationCache
	 */
	public void reset(){
		populateActiveAssociationCache();
		populateAssociationStatus();
		populateShipmentsHomeGeopoint();
		populateActiveShipmentProfileMapping();
		populateProfileTempFrequency();
		populateShipmentExcursionAttr();
	}


	/**  ***************** Association cache********************** **/
	/**
	 * get active shipments with deviceId as key and PackageId as value
	 */
	private void populateActiveAssociationCache() {
		//Synchronize on cache
		synchronized (activeAssociationCache) {
			clearActiveAssociationCache();
			activeAssociationCache.putAll(AssociationDAO.getInstance().getActiveAssociations());
		}
	}

	/**
	 * clear all the data from association Cache
	 */
	private void clearActiveAssociationCache() {
		activeAssociationCache.clear();
	}

	/**
	 * <p>
	 * Add device-package association in cache
	 * </p>
	 * @param deviceId
	 * @param packageId
	 */
	public void addAssociation(String deviceId ,String packageId){
		synchronized (activeAssociationCache) {
		activeAssociationCache.put(deviceId , packageId);
		}
	}

	/**
	 * Remove device-package association from cache
	 * @param deviceId
	 */
	public void removeAssociation(String deviceId ){
		if(activeAssociationCache.get(deviceId) != null){
			synchronized (activeAssociationCache) {
			activeAssociationCache.remove(deviceId );
			}
		}
	}

	/**
	 * Get Container Id associated with deviceId 
	 * @param deviceId
	 * @return
	 */
	public String getContainerId(String deviceId){
		synchronized (activeAssociationCache) {
			return activeAssociationCache.get(deviceId);
		}
	}


	/**  ***************** Association Status cache********************** **/
	/**
	 * populate active shipment's excursion status with deviceId-packageId combination as key and excursion status as value
	 */
	private void populateAssociationStatus() {
		//Synchronize on cache
		synchronized (shipmentStatus) {
			// Clear cache
			clearAssociationStatus();
			//Get the data from Db and populate the cache
			shipmentStatus.putAll(AssociationDAO.getInstance().getAssociationStatus());
		}
	}

	/**
	 * clear all the data from association status Cache
	 */
	private void clearAssociationStatus() {
		shipmentStatus.clear();
	}

	/**
	 * Get Collection of status
	 * @param key
	 * @return
	 */
	public String getShipmentStatus(MultiKey key){
		return shipmentStatus.get(key);
	}

	/**
	 * <p>
	 * Make an entry into association status cache for newly associated device - package pair
	 * </p>
	 * @param deviceId
	 * @param packageId
	 */
	public void addAssociationStatus(String deviceId, String packageId) {
		synchronized (shipmentStatus) {
			MultiKey key = new MultiKey(deviceId,packageId);
			shipmentStatus.put(key , "0");	//Default value set for excursion is 0 for new association
		}
	}

	/**
	 * Remove association status from cache
	 * @param deviceId
	 * @param packageId
	 */
	public void removeAssociationStatus(String deviceId, String packageId) {
		synchronized (shipmentStatus) {
			MultiKey key = new MultiKey(deviceId,packageId);
			shipmentStatus.remove(key);	//Default value set for excursion is 0 for new association
		}
	}

	/**
	 * Add association status in cache
	 * @param deviceId
	 * @param packageId
	 * @param status
	 */
	public void putAssociationStatus(String deviceId, String packageId, int status) {
		synchronized (shipmentStatus) {
			MultiKey key = new MultiKey(deviceId,packageId);
			shipmentStatus.put(key, String.valueOf(status));	
		}
	}

	/**  ***************** Shipment Geopoint cache ********************** **/

	/**
	 * Populate shipment Geopoint home mapping
	 */
	public void populateShipmentsHomeGeopoint() {
		//Synchronize on cache
		synchronized (shipmentHomeGeopoint) {
			// Clear cache
			clearShipmentHomeGeopoint();
			//Get the data from Db and populate the cache
			shipmentHomeGeopoint.putAll(AssociationDAO.getInstance().getShipmentsHomeGeopoint());
		}
	}

	/**
	 * clear shipment Geopoint home
	 */
	private void clearShipmentHomeGeopoint() {
		shipmentHomeGeopoint.clear();
	}

	/**
	 * <p> Get geopoint home for Shipment/profile </p>
	 * @param key
	 * @return
	 */
	public String getShipmentHomeGeopoint(Integer profileId){
		synchronized (shipmentHomeGeopoint) {
			return shipmentHomeGeopoint.get(profileId);
		}
	}

	/**
	 * <p> Add shipment-geopointHome mapping to cache</p>
	 * @param deviceId
	 * @param containerId
	 * @param pointName
	 */
	public void addShipmentHomeGeopoint(Integer profileId, String pointName){
		synchronized (shipmentHomeGeopoint) {
			shipmentHomeGeopoint.put(profileId, pointName);
		}
	}

	/**
	 * <p> Remove shipment home geopoint </p>
	 * @param deviceId
	 * @param packageId
	 */
	public void removeShipmentHomeGeopoint(Integer profileId){
		synchronized (shipmentHomeGeopoint) {
			shipmentHomeGeopoint.remove(profileId);
		}
	}
	/**
	 * <p> Remove shipment home geopoint</p>
	 * @param profileIds
	 */
	public void removeShipmentHomeGeopoint(ArrayList<Integer> profileIds){
		synchronized (shipmentHomeGeopoint) {
			for(Integer profileId : profileIds){
				shipmentHomeGeopoint.remove(profileId);
			}
			
		}
	}
	/**  ***************** Shipment Profile cache ********************** **/
	/**
	 * populate profile mapped to a shipment
	 */
	private void populateActiveShipmentProfileMapping() {
		synchronized (activeShipmentProfileMapping) {
			// Clear cache
			clearActiveShipmentProfileMapping();
			//Get the data from Db and populate the cache
			activeShipmentProfileMapping.putAll(AssociationDAO.getInstance().getActiveShipmentProfileMapping());
		}
	}

	/**
	 * clear profile mapping for shipments
	 */
	private void clearActiveShipmentProfileMapping() {
		activeShipmentProfileMapping.clear();
	}

	/**
	 * <p> get profile Id associated with Shipment</p>
	 * @param deviceId
	 * @param packageId
	 * @return
	 */
	public Integer getActiveShipmentProfileId(String deviceId, String packageId){
		MultiKey key = new MultiKey(deviceId, packageId);
		return activeShipmentProfileMapping.get(key);
	}

	/**
	 * <p> Add profile Id associated with Shipment to cache</p>
	 * @param deviceId
	 * @param packageId
	 * @param profileId
	 */
	public void addActiveShipmentProfileId(String deviceId, String packageId, Integer profileId){
		MultiKey key = new MultiKey(deviceId, packageId);
		activeShipmentProfileMapping.put(key, profileId);
	}

	/**
	 * <p>Remove association -profile Id mapping from cache </p>
	 * @param deviceId
	 * @param packageId
	 */
	public void removeActiveShipmentProfileMapping(String deviceId, String packageId){
		MultiKey key = new MultiKey(deviceId, packageId);
		activeShipmentProfileMapping.remove(key);
	}

	/**
	 * 
	 */
	public void populateProfileTempFrequency(){
		synchronized (profileTempFrequency) {
			// Clear cache
			clearProfileTempMapping();
			//Get the data from Db and populate the cache
			profileTempFrequency.putAll(AssociationDAO.getInstance().getProfileTempFreqMapping());
		}
	}

	private void clearProfileTempMapping() {
		profileTempFrequency.clear();
	}

	/**
	 * 
	 * @param profileId
	 * @return
	 */
	public Integer getProfileTempFrequency(Integer profileId){
		return profileTempFrequency.get(profileId);
	}

	/**
	 * @param profileId
	 * @param tempFrequency
	 */
	public void addProfileTempFrequency(Integer profileId, Integer tempFrequency){
		profileTempFrequency.put(profileId, tempFrequency);
	}

	/**
	 * 
	 * @param profileId
	 */
	public void removeProfileTempFrequency(Integer profileId){
		profileTempFrequency.remove(profileId);
	}
	
	/**
	 * <p>remove profileTempFrequency</p>
	 * @param profileIds
	 */
	public void removeProfileTempFrequency(ArrayList<Integer> profileIds){
		synchronized (profileTempFrequency) {
			for(Integer profileId : profileIds){
				profileTempFrequency.remove(profileId);
			}
			
		}
	}
	
	
	public void populateShipmentExcursionAttr(){

		synchronized (shipmentExcursionAttrList) {
			// Clear cache
			clearShipmentExcursionAttr();
			//Get the data from Db and populate the cache
			shipmentExcursionAttrList.putAll(ExcursionLogDAO.getInstance().getExcursionAttr());
		}
	}

	private void clearShipmentExcursionAttr() {
		shipmentExcursionAttrList.clear();
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param attrList
	 */
	public void addShipmentExcursionAttr(String deviceId, String containerId, ArrayList<String> attrList){
		if(attrList != null && attrList.size() > 0){
			MultiKey key = new MultiKey(deviceId, containerId);
			if(shipmentExcursionAttrList.containsKey(key)){
				HashSet<String> shipmentExcursionAttr = shipmentExcursionAttrList.get(key);
				shipmentExcursionAttr.addAll(attrList);
			}else{
				HashSet<String> shipmentExcursionAttr = new HashSet<String>();
				shipmentExcursionAttr.addAll(attrList);
				shipmentExcursionAttrList.put(key, shipmentExcursionAttr);
			}
		}
	}
	
	
	/**
	 * @param deviceId
	 * @param containerId
	 * @param attr
	 */
	public void addShipmentExcursionAttr(String deviceId, String containerId, String attr){
		if(attr != null ){
			MultiKey key = new MultiKey(deviceId, containerId);
			if(shipmentExcursionAttrList.containsKey(key)){
				HashSet<String> shipmentExcursionAttr = shipmentExcursionAttrList.get(key);
				shipmentExcursionAttr.add(attr);
			}else{
				HashSet<String> shipmentExcursionAttr = new HashSet<String>();
				shipmentExcursionAttr.add(attr);
				shipmentExcursionAttrList.put(key, shipmentExcursionAttr);
			}
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param attribute
	 */
	public void removeShipmentExcursionAttr(String deviceId, String packageId, String attribute){
		MultiKey key = new MultiKey(deviceId, packageId);
		if(shipmentExcursionAttrList.containsKey(key)){
			shipmentExcursionAttrList.get(key).remove(attribute);
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @return
	 */
	public HashSet<String> getShipmentExcursionAttr(String deviceId, String packageId){
		MultiKey key = new MultiKey(deviceId, packageId);
		if(shipmentExcursionAttrList.containsKey(key)){
			return shipmentExcursionAttrList.get(key);
		}
		return null;
	}

	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 */
	public void removeShipmentExcursion(String deviceId, String packageId) {
		MultiKey key = new MultiKey(deviceId, packageId);
		shipmentExcursionAttrList.remove(key);
	}
}