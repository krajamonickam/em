package com.rfxcel.cache;

import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import com.rfxcel.org.dao.ConfigDAO;

/**
 * <p>Class to cache org and system level configuration  </p>
 * @author sachin_kohale
 * @since March 6 2017
 *
 */

public class ConfigCache {
	private static ConfigCache singleton;
	/**
	 * stem level configuartion cache
	 */
	private static ConcurrentHashMap<String,String> systemConfig;
	private static ConcurrentHashMap<Integer, HashMap<String,String>> orgConfig;
	private static ConfigDAO configDao = ConfigDAO.getInstance();
	
	private ConfigCache(){
		systemConfig = new ConcurrentHashMap<String, String>() ;
		orgConfig = new ConcurrentHashMap<Integer,HashMap<String, String>>();
	}
	
	public static ConfigCache getInstance(){
		if(singleton == null){
			synchronized (ConfigCache.class) {
				if(singleton == null){
					singleton = new ConfigCache();
					singleton.populateSystemConfig();
					singleton.populateOrgConfig();
				}
			}
		}
		return singleton;
	}
	
	/**
	 * Reset org and system level configuration cache
	 */
	public void reset(){
		populateSystemConfig();
		populateOrgConfig();
	}
		
	/**
	 * 
	 */
	public void populateSystemConfig(){
		systemConfig.putAll(configDao.getSystemConfig());
	}
	
	/**
	 * <p> Get system level configurations </p>
	 * @param paramName
	 * @return
	 */
	public String getSystemConfig(String paramName){
		if(paramName == null){
			return null;
		}
		if (systemConfig.get(paramName) == null) {
			return configDao.getSystemConfig().get(paramName);
		}
		return systemConfig.get(paramName);
		//return systemConfig.get(paramName); //ITT-718  
	}
	
	/**
	 * 
	 */
	public void populateOrgConfig(){
		HashMap<Integer, HashMap<String, String>> config = configDao.getOrgConfig();
		if(config!= null)
			orgConfig.putAll(configDao.getOrgConfig());
	}
	
	/**
	 * <p> Get org level configurations </p>
	 * @param paramName
	 * @return
	 */
	
	/*
	public String getOrgConfig(String paramName, int orgId){
		if(paramName == null){
			return null;
		}
		return configDao.getOrgConfig(paramName, orgId);
	}
	*/
	
	
	public String getOrgConfig(String paramName, int orgId){
		if(paramName == null){
			return null;
		}
		HashMap<String, String> config = orgConfig.get(orgId);
		if(config == null){
			return null;
		}else{
			return config.get(paramName);
		}
	}
	
	
	public String getOrgConfigUncached(String paramName, int orgId){
		String paramValue = configDao.getOrgConfig(paramName, orgId);
		if (paramValue == null) return null;
		HashMap<String, String> config = orgConfig.get(orgId);
		if (config != null){
			config.put(paramName, paramValue);
		}
		return(paramValue);
	}
	/**
	 * <p> remove org related config from cache
	 * @param orgId
	 */
	public void removeOrgConfig(int orgId){
		orgConfig.remove(orgId);
	}

	
	/**
	 * @param orgId
	 */
	public void updateOrgSettings(Integer orgId) {
		orgConfig.remove(orgId);
		HashMap<String, String> config = configDao.getOrgConfig(orgId);
		if(config != null)
			orgConfig.put(orgId, config);
	}
	
	/**
	 * @param property
	 * @param value
	 */
	public void setSystemConfig(String property,String value) {
		if(!property.isEmpty() && !value.isEmpty())
		{
			systemConfig.put(property, value);
		}
	}
	
	public void setOrgConfig(int orgId, String property,String value) {
		HashMap<String, String> config = orgConfig.get(orgId);
		config.put(property, value);
	}
}
