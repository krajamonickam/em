package com.rfxcel.cache;

import java.util.ArrayList;
import java.util.HashMap;

import com.rfxcel.auth2.dao.AuthDao;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class UserTimeZoneCache {


	private static UserTimeZoneCache singleton;
	private HashMap<String,String> userTimeZoneOffset = new HashMap<String, String>() ;
	private HashMap<String,String> userTimeZone = new HashMap<String, String>();

	private UserTimeZoneCache(){
		populateUserTzOffset();
		populateUserTimeZone();
	}

	/**
	 * 
	 */
	private void populateUserTzOffset() {
		synchronized (userTimeZoneOffset) {
			clearUserTzOffset();
			userTimeZoneOffset.putAll(AuthDao.getInstance().getUsersTimeZoneOffset());
		}
	}

	/**
	 * 
	 */
	private void clearUserTzOffset() {
		userTimeZoneOffset.clear();
	}

	/**
	 * 
	 */
	public void reset(){
		populateUserTzOffset();
		populateUserTimeZone();
	}
	
	/**
	 * @return
	 */
	public static UserTimeZoneCache getInstance(){
		if(singleton == null){
			synchronized (UserTimeZoneCache.class) {
				if(singleton == null){
					singleton = new UserTimeZoneCache();
				}
			}
		}
		return singleton;
	}

	/**
	 * <p> get timezone offset for a user
	 * @param user
	 * @return
	 */
	public String getTimeZoneOffset(String user){
		if (user == null)
			return("+00:00");
		String offset;
		offset = userTimeZoneOffset.get(user);
		//if offset is not present in cache, fetch it from DB
		if(offset == null){
			offset = AuthDao.getInstance().getUserTimeZoneOffset(user);
			userTimeZoneOffset.put(user, offset);
		}
		return offset;
	}

	/**
	 * @param user
	 * @param offset
	 */
	public void addUserTimeZoneOffset(String user, String offset){
		synchronized (userTimeZoneOffset) {
			userTimeZoneOffset.put(user, offset);
		}
	}

	/**
	 * <p> remove user timeZoneoffset mapping </p>
	 * @param userLogin
	 */
	public void removeUserTimeZoneOffset(String userLogin){
		synchronized (userTimeZoneOffset) {
			userTimeZoneOffset.remove(userLogin);
		}
	}


	/**
	 *  <p> Remove users from cache
	 * @param userLogins
	 */
	public void removeUserTimeZoneOffset(ArrayList<String> userLogins) {
		synchronized (userTimeZoneOffset) {
			for(String login :userLogins){
				userTimeZoneOffset.remove(login);
			}
		}
	}


	/**
	 * 
	 */
	public void populateUserTimeZone(){
		synchronized (userTimeZone) {
			clearUserTimeZone();
			userTimeZone.putAll(AuthDao.getInstance().getUsersTimeZone());
		}
	}

	/**
	 * 
	 */
	private void clearUserTimeZone() {
		userTimeZone.clear();
	}

	/**
	 * @param user
	 * @return
	 */
	public String getTimeZone(String user){
		if (user == null)
			return "";
		String timeZone;
		timeZone = userTimeZone.get(user);
		//if offset is not present in cache, fetch it from DB
		if(timeZone == null){
			timeZone = AuthDao.getInstance().getUserTimeZone(user);
			userTimeZone.put(user, timeZone);
		}
		return timeZone;
	}
	
	/**
	 * @param user
	 * @param tzCode
	 */
	public void addUserTimeZone(String userLogin, String tzCode){
		synchronized (userTimeZone) {
			userTimeZone.put(userLogin, tzCode);
		}
	}
	
	/**
	 * <p> remove user timeZone mapping
	 * @param userLogin
	 */
	public void removeUserTimeZone(String userLogin){
		synchronized (userTimeZone) {
			userTimeZone.remove(userLogin);
		}
	}
	
	/**
	 * <p> remove user timeZone mapping </p>
	 * @param userLogins
	 */
	public void removeUserTimeZone(ArrayList<String> userLogins) {
		synchronized (userTimeZone) {
			for(String login : userLogins){
				userTimeZone.remove(login);
			}
		}
	}
}
