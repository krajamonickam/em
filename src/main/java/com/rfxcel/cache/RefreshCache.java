package com.rfxcel.cache;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;

import com.rfxcel.rule.service.RuleCache;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceAttributeLogCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.cache.UserTimeZoneCache;

@WebServlet("/refreshCache")
public class RefreshCache extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(RefreshCache.class);
	private Properties prop = null;
	

	public RefreshCache() {
		super();
		this.prop = new Properties();
		String userName = null;
		String password = null;	

		userName = ConfigCache.getInstance().getSystemConfig("sendum.user");
		password = ConfigCache.getInstance().getSystemConfig("sendum.password");
		
		this.prop.put("sendum.user", userName);
		this.prop.put("sendum.password", password);
	}

	@Override
	/**
	 * this is the servlet that accepts the https post request from the calling service.
	 * performs the basic authentication
	 * accepts the json and calls the processRequest method to refresh cache data.
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		final String authorization = request.getHeader("Authorization");
		String requestType = request.getMethod();
		if (authorization != null && authorization.startsWith("Basic") && "POST".equals(requestType)) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			logger.debug("Encoded Credentials " + base64Credentials);
			String credentials[] = Utility.decrypytUserNamePwd(base64Credentials);
			String user = getUserName();
			String pass = getPassword();
			if (user.equals(credentials[0].trim())&& pass.equals(credentials[1].trim())) {
				InputStream inputStream = request.getInputStream();
				StringBuilder stringBuilder = new StringBuilder();
				BufferedReader bufferedReader = null;
				String payload = null;
				try {
					if (inputStream != null) {
						bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
						char[] charBuffer = new char[128];
						int bytesRead = -1;
						while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
							stringBuilder.append(charBuffer, 0, bytesRead);
						}
					} else {
						stringBuilder.append("");
					}
				} catch (IOException ex) {
					logger.error(ex.getMessage());
				} finally {
					if (bufferedReader != null) {
						try {
							bufferedReader.close();
						} catch (IOException ex) {
							logger.error(ex.getMessage());
						}
					}
				}
				payload = stringBuilder.toString();
				if (!(payload.equals(""))) {
					processRequest(payload);
				} else {
					logger.error("HTTP Payload is empty !!");
				}
			} else {
				logger.error("Authentication Failed !!");
			}
		}
	}
	
	
	
	public void processRequest(String payload) 
	{
		Map<String, String> map = new HashMap<String, String>();
		map = Utility.convertJsonToMap(payload);
		String action = map.get("action");
		if ((action == null) || (action.trim().length() == 0)) {
			logger.error("Invalid action in payload " + payload);
			return;
		}
		if (action.equals("refreshCache")) {
			doReset();
			return;
		}
		logger.error("Invalid action in payload");
	}
	
	public static void doReset()
	{
	
		//Reset all the caches' present in AssociationCache class ie ActiveAssociation's and associationStatus and packageProduct mapping
		AssociationCache.getInstance().reset();
		//reset device attribute logs DeviceAttrNotificationLimitLog, DeviceAttrExcursionTimeLog and SameLocationDeviceNotificationLimitLog
		DeviceAttributeLogCache.getInstance().reset();
		// Reset device's last location information and deviceTypeGeo points information and not device's geo point's state
		DeviceCache.getInstance().reset();		
		// reset threshold rules cached for a given device Type
		RuleCache.getInstance().reset();
		// reset user's timezone information
		UserTimeZoneCache.getInstance().reset();
		//Reset ConfigPropertyCache i.e. it reset the configuration parameter values e.g. log folder path
		ConfigCache.getInstance().reset();		
			
		logger.info("Cached Data is now refreshed... ");
	}

	private String getUserName() {
		return prop.getProperty("sendum.user");
	}

	private String getPassword() {
		return prop.getProperty("sendum.password");
	}
}
