package com.rfxcel.notification.dao;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.notification.entity.AlertGroup;



/**
 * 
 * @author Albeena
 * @since Apr 21, 2017
 *
 */
public class AlertGroupDAO {
	private static final Logger logger = Logger.getLogger(AlertGroupDAO.class);	
	private static AlertGroupDAO singleton;
	
	public static AlertGroupDAO getInstance(){
		if(singleton == null){
			synchronized (AlertGroupDAO.class) {
				if(singleton == null){
					singleton = new AlertGroupDAO();
				}
			}
		}
		return singleton;
	}
	
	
	/**
	 * 
	 * @param alertGroupList
	 * @throws SQLException
	 */
	public Long addAlertGroup(AlertGroup alertGroup) throws SQLException{
		Connection conn = null;
		Long alertGroupId = -1L ;
		try {
					conn = DAOUtility.getInstance().getConnection();
					String query ="INSERT INTO alert_groups (group_name,description,org_id,group_id) VALUES (?,?,?,?)";
					PreparedStatement stmt = conn.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
					stmt.setString(1,alertGroup.getGroupName());
					String description = alertGroup.getDescription();
					if(description != null){
						stmt.setString(2, description);
					}else{
						stmt.setNull(2, Types.NULL);
					}
					stmt.setLong(3, alertGroup.getOrgId());
					stmt.setLong(4, alertGroup.getGroupId());
					stmt.executeUpdate();
					ResultSet rs=stmt.getGeneratedKeys();
					if (rs.next()) {
						alertGroupId= rs.getLong(1);
					}
					addAlertGroupMapping(conn,alertGroupId,alertGroup);
					rs.close();
					stmt.close();
		} finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertGroupId;
	}

	
	/**
	 * 
	 * @param groupName
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfAlertGroupIsPresent(AlertGroup alertGroup) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from alert_groups WHERE group_name = ? AND org_id=? AND active=1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, alertGroup.getGroupName());
			stmt.setLong(2, alertGroup.getOrgId());
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	
	/**
	 * 
	 * @return alertGroupList 
	 * @throws SQLException 
	 */
	public List<AlertGroup> getAlertGroupList(AlertGroup alertGroup) throws SQLException {
		Connection conn = null;
		List<AlertGroup> alertGroupList = null;		
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT DISTINCT a.id, group_name,description,org_id, group_id, a.created_time, a.updated_time,group_concat(u.user_id) As user_id FROM alert_groups a LEFT JOIN alert_group_user_mapping u ON a.id = u.alert_group_id "
					+ " WHERE org_id = ? AND active=1 GROUP BY a.id";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, alertGroup.getOrgId());
			ResultSet rs = stmt.executeQuery();
			alertGroupList =  new ArrayList<AlertGroup>();
			AlertGroup alertGroups = null;
			while(rs.next()){
				String[] users = null;
				if(rs.getString("user_id") != null){
					users = rs.getString("user_id").split(",");
				}
				alertGroups = new AlertGroup(rs.getLong("id"), rs.getString("group_name"), rs.getString("description"),rs.getLong("org_id"), rs.getLong("group_id"),rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),users);
				alertGroupList.add(alertGroups);
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertGroupList;
	}
	
	/**
	 * 
	 * @param alertGroup
	 * @throws SQLException
	 */
	public void updateAlertGroup(AlertGroup alertGroup) throws SQLException{
		try {
			DAOUtility.runInTransaction(conn->{
				String query = "UPDATE alert_groups SET group_name = ?, description = ?,updated_time = NOW() WHERE id = ?";
				PreparedStatement stmt = conn.prepareStatement(query);
				String groupName, description;
							
				groupName = alertGroup.getGroupName();
				description = alertGroup.getDescription();
								
				if(groupName != null){
					stmt.setString(1, groupName);
				}else{
					stmt.setNull(1, Types.NULL);
				}
				
				if(description != null){
					stmt.setString(2, description);
				}else{
					stmt.setNull(2, Types.NULL);
				}
				
				stmt.setLong(3, alertGroup.getId());
				stmt.executeUpdate();
				deleteAlertGroupMapping(conn,alertGroup.getId());
				//adding alertGroup mapping for alertgroupId
				addAlertGroupMapping(conn,alertGroup.getId(), alertGroup);
				stmt.close();	
				return null;
			});
		} finally{
		}
	}

	
	
	
	/**
	 * @throws SQLException 
	 * 
	 */
	public void deleteAlertGroup(Long alertGroupId) throws SQLException {
		try {
			DAOUtility.runInTransaction(conn->{
				String query = "UPDATE alert_groups SET active=0 WHERE id=?";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setLong(1, alertGroupId);			
				stmt.executeUpdate();
				stmt.close();
				//delete user-alertgroup mapping
				deleteAlertGroupMapping(conn,alertGroupId);
				//delete alert-alertgroup mapping
				AlertDAO.getInstance().deleteAlertGroupMappingByAlertGrp(conn, alertGroupId);
				return null;
			});
		} finally {
		}
	}
	
	/**
	 * 
	 * @param alertGroupList
	 * @throws SQLException
	 */
	public void addAlertGroupMapping(Connection conn,Long Id,AlertGroup alertGroup) throws SQLException{
			String query ="INSERT INTO alert_group_user_mapping (alert_group_id,user_id) VALUES (?,?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			if(alertGroup.getUsers() != null && alertGroup.getUsers().length > 0){
				for(String user : alertGroup.getUsers()){
					Long userId = Long.valueOf(user);				
					stmt.setLong(1,Id);
					stmt.setLong(2,userId);
					stmt.addBatch();					
				}
				stmt.executeBatch();
				stmt.close();
			}
	}
	
		
	/**
	 * @throws SQLException 
	 * 
	 */
	public void deleteAlertGroupMapping(Connection conn,Long Id) throws SQLException {
			String query = "DELETE FROM alert_group_user_mapping WHERE alert_group_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, Id);
			stmt.executeUpdate();
			stmt.close();
	}
	
	/**
	 * 
	 * @param conn
	 * @param alertGroupIds
	 * @throws SQLException
	 */
	public void deleteAlertGroupMapping(Connection conn, ArrayList<Long> alertGroupIds) throws SQLException {
		String query = "DELETE FROM alert_group_user_mapping WHERE alert_group_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		for(Long alertGroupId : alertGroupIds){
			stmt.setLong(1, alertGroupId);
			stmt.addBatch();
		}		
		stmt.executeBatch();
		stmt.close();
	}
	
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<Long> getAlertGroupIdsByOrg(Connection conn, int orgId) throws SQLException{
		String sqlQuery = "SELECT id FROM alert_groups WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(sqlQuery);
		stmt.setInt(1, orgId);
		ResultSet rs = stmt.executeQuery();
		ArrayList<Long> alertGroupIds = new ArrayList<Long>();
		while(rs.next()){
			Long alertId = rs.getLong("id");
			alertGroupIds.add(alertId);
		}
		rs.close();
		stmt.close();
		return alertGroupIds;
	}
	

	/**
	 * 
	 * @param orgId
	 * @throws SQLException
	 */
	public void deactivateAlertGroupsForOrg(Connection conn,Integer orgId) throws SQLException {
		String query = "Update alert_groups set active=0 where org_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
		
		//deleting alertgroup-user mapping
		ArrayList<Long> alertGroupIds = getAlertGroupIdsByOrg(conn, orgId);
		deleteAlertGroupMapping(conn, alertGroupIds);
	}
	
	
	/**
	 * 
	 * @param alertGroupUsersCount
	 * @return
	 * @throws SQLException
	 */
	public int alertGroupUsersCount(Integer orgId) throws SQLException{
		Connection conn = null;
		int count = 0;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT DISTINCT COUNT(um.user_id) FROM alert_group_user_mapping um JOIN rfx_users u ON um.user_id = u.user_id WHERE u.org_id = ? AND u.active=1 AND u.user_type != 0";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				count = rs.getInt(1);
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return count;
	}
	
	/**
	 * 
	 * @param alertGroup
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfActiveAlertGroupIsPresent(AlertGroup alertGroup) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from alert_groups WHERE id = ? AND org_id=? AND active=1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, alertGroup.getId());
			stmt.setLong(2, alertGroup.getOrgId());
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	
	/**
	 * @param id
	 * @return
	 * @throws SQLException
	 */
	public AlertGroup getAlertGroupById(Long id) throws SQLException{
		Connection conn = null;
		AlertGroup alertGroup = new AlertGroup();
		try{
			conn =DAOUtility.getInstance().getConnection();
			String query = "Select group_name, description from alert_groups where id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				alertGroup.setGroupName(rs.getString("group_name"));
				alertGroup.setDescription(rs.getString("description"));
			}
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertGroup;
	}
}
