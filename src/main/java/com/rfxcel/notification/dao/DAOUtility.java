package com.rfxcel.notification.dao;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.Properties;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.sql.DataSource;

import org.apache.log4j.Logger;
import org.elasticsearch.client.RestClient;

import com.rfxcel.sensor.util.Config;


/**
 * 
 * @author Tejshree Kachare
 * <p>
 * Utility class for DAO utilities
 * </p>
 *
 */
public class DAOUtility {

	private static Logger logger = Logger.getLogger(DAOUtility.class);
	private static DAOUtility singelton;
	private Properties prop = null;


	private DAOUtility(){
	}

	public static DAOUtility getInstance(){
		if(singelton ==  null){
			synchronized (DAOUtility.class) {
				singelton = new DAOUtility();
				singelton.prop = Config.getInstance().getProp();
			}
		}
		return singelton;
	}

	/**
	 * @return the prop
	 */
	public Properties getProp() {
		return prop;
	}
	
	/**
	 * This is to create connection with rTS Database
	 * @return Connection
	 */
	public Connection getConnection(){
		Connection connection = null;
		DataSource dataSource = null;
		try {
			String jndiName = singelton.getProp().getProperty("jndi_name");
			Context initContext = new InitialContext();
			//dataSource = (DataSource) initContext.lookup("java:"+jndiName);
			dataSource = (DataSource) initContext.lookup(jndiName);
			connection = dataSource.getConnection();
		} catch (Exception exe) {
			if (connection != null)
				closeConnection(connection);
		}
		return connection;
	}

	/*public Connection getConnection(){
	Connection connection = null;
	try {
		connection = DriverManager.getConnection("jdbc:mysql://127.0.0.1:3306/sendum", "root", "password");
	} catch (SQLException e) {
		e.printStackTrace();
	}
   return connection;
	}*/
	
	/**
	 * This is to close the connection after Database access
	 * @param connection
	 */
	public void closeConnection(Connection connection) {
		try {
			if ( connection!= null && !(connection.isClosed())) {
				connection.close();
			}
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}
	
	/**
	 * This is to close the ES connection after search is done
	 * @param connection
	 */
	public void closeConnection(RestClient client) {
		try {
			if (client!= null ) {
				client.close();
			}
		} catch (Exception exe) {
			exe.printStackTrace();
		}
	}
	
	public static Object runInTransaction(Transaction transaction) throws SQLException {

        Connection dbConnection = getInstance().getConnection();
        dbConnection.setAutoCommit(false);
        Object result = null;
        try {
        	result = transaction.execute(dbConnection);
            dbConnection.commit();
        } catch (SQLException e) {
        	logger.error("Error in runInTransaction() :: "+e.getMessage());
            dbConnection.rollback();
            throw e;
        } finally {
            dbConnection.close();
        }
        return result;
    }

}
