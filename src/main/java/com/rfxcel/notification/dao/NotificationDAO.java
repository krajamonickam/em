package com.rfxcel.notification.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.Map.Entry;

import org.apache.log4j.Logger;

import com.rfxcel.notification.util.NotificationConstant;
import com.rfxcel.notification.vo.AlertDetailsVO;
import com.rfxcel.notification.vo.NotificationReqVO;
import com.rfxcel.notification.vo.NotificationResVO;
import com.rfxcel.notification.vo.NotificationVO;
import com.rfxcel.notification.vo.ShipmentNotifications;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.util.Config;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.UserTimeZoneCache;

/**
 * 
 * @author Tejshree Kachare
 * @since 4 Nov 2016
 *
 */
public class NotificationDAO {
	private static SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static Logger logger = Logger.getLogger(NotificationDAO.class);
	private static NotificationDAO notificationDAO;
	private static UserTimeZoneCache userTimeZoneCache = UserTimeZoneCache.getInstance();
	private static ConfigCache configCache = ConfigCache.getInstance();
	private final String keySeparator = "@#";
	
	public static NotificationDAO getInstance(){
		if(notificationDAO == null){
			notificationDAO = new NotificationDAO();
		}
		Config.getInstance().getProp();
		return notificationDAO;
	}

	NotificationDAO(){
		//simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
	}
	/**
	 * 
	 * @param notificationVO
	 */
	public static void saveNotification(NotificationVO notificationVO){
		Connection conn = null;
		try{
			 conn = DAOUtility.getInstance().getConnection();;
			String query = "INSERT INTO  sensor_notifications (alert_code, notification, device_id, rule_id, sensor_data_id, container_id, org_id,created_on, product_id, state) VALUES (?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(query,Statement.RETURN_GENERATED_KEYS);
			stmt.setLong(1, notificationVO.getAlertCode());
			stmt.setString(2, notificationVO.getNotification());
			stmt.setString(3,notificationVO.getDeviceId());
			if(notificationVO.getRuleId()!= null){
				stmt.setLong(4,notificationVO.getRuleId() );
			}
			else{
				stmt.setNull(4, Types.NULL);
			}
			stmt.setLong(5, notificationVO.getSensorDataId());
			stmt.setString(6, notificationVO.getContainerId());
			stmt.setInt(7, notificationVO.getAlertDetails().getOrgId());
			if (notificationVO.getCreatedDate() == null) {
				stmt.setString(8, Utility.getDateFormat().format(new Date()));
			}
			else {
				stmt.setString(8, Utility.getDateFormat().format(notificationVO.getCreatedDate()));
			}
			stmt.setString(9, notificationVO.getProductId());
			stmt.setInt(10, notificationVO.getState());
			stmt.executeUpdate();
			Long id = null;
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getLong(1);
			}
			notificationVO.setNotificationId(id);
			stmt.close();
		}catch(Exception e){
			logger.error("Error while saving notification: " +e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param alertcode, orgId
	 * @return AlertDetailsVO
	 */
	public static AlertDetailsVO getAlertDetails(AlertDetailsVO alertDetailsVO){
		Connection conn = null;
		AlertDetailsVO alertDetails = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			Integer orgId = alertDetailsVO.getOrgId();			
			String sqlQuery = "SELECT id, alert_msg, alert_detail_msg, corrective_action FROM sensor_alert_types WHERE alert_code = ? and org_id =?"; 
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertDetailsVO.getAlertCode());
			stmt.setInt(2, orgId);
			
			ResultSet rs = stmt.executeQuery();
			while( rs.next()){
				alertDetails = new AlertDetailsVO(alertDetailsVO.getAlertCode(), alertDetailsVO.getAttributeValueMap(),orgId);
				alertDetails.setId(rs.getInt("id"));				
				alertDetails.setAlertMsg(rs.getString("alert_msg"));
				alertDetails.setAlertDetailMsg(rs.getString("alert_detail_msg"));
				alertDetails.setCorrectiveAction(rs.getString("corrective_action"));				
				alertDetails.setAttributeName(alertDetailsVO.getAttributeName());
			}
			rs.close();
			stmt.close();
		}catch(Exception e){
			logger.error("Error while getting alert details for alert_code "+ alertDetailsVO.getAlertCode() + " for organization : " +alertDetailsVO.getOrgId()+" "+ e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertDetails;
	}
	


	/**
	 * get email address of all the members of group tagged to an email alert
	 * @param alertDetails
	 * @return
	 */
	public static String getEmailAddressesForAnAlert(long alertCode, int orgId) {
		Connection conn = null;
		StringBuilder sb =  new StringBuilder(100);
		String emailIds = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			/*String query = "select user_email from rfx_users where user_id in"
					+ "( select user_id from alert_group_user_mapping where alert_group_id ="
					+ " (select alert_group_id from sensor_alert_types where alert_code = ? and org_id = ? ))";*/
			//updated query to fetch user_email list by multiple groups for alert
			String query = "SELECT user_email FROM rfx_users WHERE user_id IN ( SELECT user_id FROM alert_group_user_mapping WHERE alert_group_id IN"
					+ "(SELECT alert_group_id FROM alert_group_mapping WHERE alert_id="
					+ "(SELECT id FROM sensor_alert_types WHERE alert_code = ? AND org_id = ? )))";
			PreparedStatement stmt = conn.prepareStatement(query); 
			stmt.setLong(1, alertCode);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			while( rs.next()){
				sb.append(rs.getString("user_email")).append(",");
			}
			if(sb.length() > 0){
				emailIds = sb.substring(0, sb.length()-1);
			}
			stmt.close();
		
		}catch(Exception e){
			logger.error("Error while fetching email ids for alert group: " +e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return emailIds;
	}


	/**
	 * <p> get Notifications based on search type All, or device ID </p> 
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public NotificationResVO getShipmentNotifications(NotificationReqVO request) throws SQLException, ParseException {
		Connection conn = null;
		NotificationResVO res = null;
		try{
			res = new NotificationResVO();
			conn = DAOUtility.getInstance().getConnection();
			String searchType = request.getSearchType();
			Integer orgId = request.getOrgId();
			String containerId= null;
			String deviceId = null;
			String productId = null;
			String dateFormatString = configCache.getOrgConfig("date.format", orgId);
			SimpleDateFormat configDateFormat;
			if(dateFormatString != null){
				configDateFormat =  new SimpleDateFormat(dateFormatString);
			}else{
				configDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			}
			String userLogin = request.getUser();
			String timeZoneOffset = userTimeZoneCache.getTimeZoneOffset(userLogin);
			String tzCode = userTimeZoneCache.getTimeZone(userLogin);
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}
			String searchQuery = "SELECT not_id,notification,CONVERT_TZ(created_on,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as created_on, device_id, "
					+ "container_id, product_id, alert_code, state  from sensor_notifications where org_id=?";
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				containerId = request.getDeviceData().getPackageId();
				deviceId = request.getDeviceData().getDeviceId();
				productId = request.getDeviceData().getProductId();
				searchQuery += " and device_id = ? and container_id = ? and product_id = ? and status = 1 ";
			}

			if (request.getSortBy().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SORTBY_AGE)) {
				searchQuery += " order by created_on DESC, alert_code ASC " ;
			}else {
				searchQuery += " order by alert_code DESC " ; 
			}

			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setInt(1, orgId);
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				stmt.setString(2, deviceId);
				stmt.setString(3, containerId);
				stmt.setString(4, productId);
			}

			LinkedHashMap<String, ArrayList<NotificationVO>> groupedNotifications = new LinkedHashMap<String, ArrayList<NotificationVO>>();
			ResultSet  rs = stmt.executeQuery();
			String packageId;
			String key;
			Long alertCode;
			int state;
			while (rs.next()) {
				packageId = rs.getString("container_id");
				deviceId = rs.getString("device_id");
				alertCode = rs.getLong("alert_code");
				productId = rs.getString("product_id");
				key = productId + keySeparator+ packageId+ keySeparator+ deviceId;
				String createdOn ="";
				if(rs.getString("created_on") != null){
					createdOn = configDateFormat.format(simpleDateFormat.parse(rs.getString("created_on")).getTime());
					if(tzCode!= null){
						createdOn = createdOn +" " +tzCode;
					}
					
				}
				state = rs.getInt("state");
				NotificationVO notiVO = new NotificationVO(rs.getLong("not_id"), rs.getString("notification"), createdOn,productId);
				notiVO.setState(state);
				if(groupedNotifications.get(key)!= null){
					groupedNotifications.get(key).add(notiVO);
				}else{
					 ArrayList<NotificationVO> temp = new  ArrayList<NotificationVO>();
					temp.add( notiVO);
					groupedNotifications.put(key, temp);
				}
			}
			ArrayList<ShipmentNotifications> shipmentNotifications =  new ArrayList<ShipmentNotifications>();
			String id;
			ArrayList<NotificationVO> notifications ;
			ShipmentNotifications shipmentNot;
			for(Entry<String, ArrayList<NotificationVO>> entry : groupedNotifications.entrySet()){
				id = entry.getKey();
				// get shipment status from sensor_Associate table
				String[] values = id.split(keySeparator);
				productId = values[0];
				packageId = values[1];
				deviceId = values[2];
				state = AssociationDAO.getInstance().getShipmentExcursionStatus(deviceId, packageId, productId);
				notifications = entry.getValue();
				shipmentNot = new ShipmentNotifications(id, notifications, productId, packageId, deviceId, state);
				shipmentNotifications.add(shipmentNot);
			}
			res.setShipmentNotifications(shipmentNotifications);
			res.setDateTime(simpleDateFormat.format(new Date()));
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;

	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public NotificationResVO getNotifications(NotificationReqVO request) throws SQLException, ParseException{
		Connection conn = null;
		NotificationResVO res = new NotificationResVO();
		try{
			conn = DAOUtility.getInstance().getConnection();
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			String searchType = request.getSearchType();
			String containerId= null;
			String deviceId = null;
			String productId = null;
			String timeZoneOffset = userTimeZoneCache.getTimeZoneOffset(request.getUser());
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}

			String searchQuery = "SELECT not_id,notification, CONVERT_TZ(created_on,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as created_on ,device_id,container_id, state from sensor_notifications where org_id = ?";
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				containerId = request.getDeviceData().getPackageId();
				deviceId = request.getDeviceData().getDeviceId();
				productId = request.getDeviceData().getProductId();
				searchQuery += " and device_id = ? and container_id = ? and product_id = ? and status = 1 ";
			}

			if (request.getSortBy().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SORTBY_AGE)) {
				searchQuery += "order by created_on DESC " ;
			}else {
				searchQuery += "order by alert_code DESC " ; 
			}

			int offset = request.getStart();
			int limit = request.getLimit();

			searchQuery += "LIMIT "+offset+","+limit; 

			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setInt(1, request.getOrgId());
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				stmt.setString(2, deviceId);
				stmt.setString(3, containerId);
				stmt.setString(4, productId);
			}
			//NotificationVO notiVO = null;
			ArrayList<NotificationVO> notifications = new ArrayList<NotificationVO>();
			ResultSet  rs = stmt.executeQuery();

			while (rs.next()) {
				String createdOn ="";
				if(rs.getTimestamp("created_on") != null){
					createdOn = sdf.format(rs.getTimestamp("created_on"));
				}
				NotificationVO notiVO = new NotificationVO(rs.getLong("not_id"), rs.getString("notification"), rs.getString("device_id"), rs.getString("container_id"),createdOn);
				notiVO.setProductId(rs.getString("product_id"));
				notiVO.setState(rs.getInt("state"));
				notifications.add(notiVO);
			}
			res.setNotifications(notifications);
			res.setCount(notifications.size());
			res.setDateTime(simpleDateFormat.format(new Date()));
			rs.close();
			stmt.close();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}
	/**
	 * <p> Get notification count based on search type All, or device ID</p>
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public NotificationResVO getNotificationsCount(NotificationReqVO request) throws SQLException {
		NotificationResVO res = null ;
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			res = new NotificationResVO();
			String searchType = request.getSearchType();
			String containerId= null;
			String deviceId = null;
			String productId = null;
			String searchQuery = "SELECT count(*) from sensor_notifications where org_id =? ";
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				containerId = request.getDeviceData().getPackageId();
				deviceId = request.getDeviceData().getDeviceId();
				productId = request.getDeviceData().getProductId();
				searchQuery += " and device_id = ? AND container_id = ? AND product_id = ? AND status = 1";
			}
			
			/*if(searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_ALL)){
				searchQuery += " AND status = 1";
			}*/
			
			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setInt(1, request.getOrgId());
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				stmt.setString(2, deviceId);
				stmt.setString(3, containerId);
				stmt.setString(4, productId);
			}
			
			ResultSet  rs = stmt.executeQuery();	
			while (rs.next()) {
				res.setCount(rs.getInt(1));
			}
			res.setDateTime(simpleDateFormat.format(new Date()));
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}
	
	/**
	 * 
	 * @param alertCode
	 * @return
	 */
	public int getStateFromAlertCode(Long alertCode){
		if(alertCode.equals(NotificationConstant.ALERT_CODE_ATTR_RESET) ||  alertCode.equals(NotificationConstant.ALERT_CODE_ROUTE_RESUMED)){
			return 1;
		}else if(alertCode.equals(NotificationConstant.ALERT_CODE_GEO_POINT_ENTRY) || alertCode.equals(NotificationConstant.ALERT_CODE_GEO_POINT_EXIT)
				|| alertCode.equals(NotificationConstant.ALERT_CODE_SHIPMENT_ON_ROUTE) || alertCode.equals(NotificationConstant.ALERT_CODE_WITHOUT_MOTION)){
			return 0;
		}else{
			return 2;
		}
	}

	/**
	 * @param alertDetailsVO
	 * @return
	 */
	public static AlertDetailsVO getProfileAttrAlertDetails(NotificationVO notificationVO) {
		Connection conn = null;
		AlertDetailsVO alertDetailsVO = notificationVO.getAlertDetails();
		AlertDetailsVO alertDetails = null;
		Long alertCode = alertDetailsVO.getAlertCode();
		String alertDetailedMessage = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			Integer orgId = alertDetailsVO.getOrgId();	

			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(notificationVO.getDeviceId(), notificationVO.getContainerId());
			String sqlQuery = "SELECT alert_detail_msg FROM profile_alert_types WHERE profile_id =? and alert_code =? "; 
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, profileId);
			stmt.setLong(2,alertCode);
			ResultSet rs = stmt.executeQuery();
			while( rs.next()){
				alertDetailedMessage = rs.getString("alert_detail_msg");
			}
			// ie if the alert is defined for a profile then get additional details
			if(alertDetailedMessage != null){
				sqlQuery = "SELECT id, alert_msg FROM sensor_default_alerts WHERE alert_code = ?"; 
				stmt = conn.prepareStatement(sqlQuery);
				stmt.setLong(1, alertCode);
				rs = stmt.executeQuery();
				while( rs.next()){
					alertDetails = new AlertDetailsVO(alertCode, alertDetailsVO.getAttributeValueMap(),orgId);
					//alertDetails.setId(rs.getInt("id"));				
					alertDetails.setAlertMsg(rs.getString("alert_msg"));
					alertDetails.setAttributeName(alertDetailsVO.getAttributeName());
					alertDetails.setAlertDetailMsg(alertDetailedMessage);
				}
			}

			rs.close();
			stmt.close();
		}catch(Exception e){
			logger.error("Error while getting alert details for alert_code "+ alertCode + " for organization : " +alertDetailsVO.getOrgId()+" "+ e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertDetails;
	}
	
	/**
	 * get email address of all the members of group tagged to an email alert
	 * @param alertDetails
	 * @return
	 */
	public static String getEmailAddressesForAttrAlert(Long alertCode, Integer profileId) {
		Connection conn = null;
		StringBuilder sb =  new StringBuilder(100);
		String emailIds = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_email FROM rfx_users WHERE user_id IN ( SELECT user_id FROM alert_group_user_mapping WHERE alert_group_id IN"
					+ "(SELECT alert_group_id FROM profile_alert_group_mapping WHERE alert_code = ? and profile_id =? ))";
			PreparedStatement stmt = conn.prepareStatement(query); 
			stmt.setLong(1, alertCode);
			stmt.setInt(2, profileId);
			ResultSet rs = stmt.executeQuery();
			while( rs.next()){
				sb.append(rs.getString("user_email")).append(",");
			}
			query = "SELECT email_id FROM alert_email_id_mapping WHERE profile_id=? and alert_code = ?";
			stmt = conn.prepareStatement(query); 
			stmt.setInt(1,profileId );
			stmt.setLong(2, alertCode);
			rs = stmt.executeQuery();
			while( rs.next()){
				sb.append(rs.getString("email_id")).append(",");
			}
			if(sb.length() > 0){
				emailIds = sb.substring(0, sb.length()-1);
			}
		}catch(Exception e){
			logger.error("Error while fetching email ids for alert group: " +e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return emailIds;
	}
	
	/**
	 * get email address of all the members of group tagged to an email alert
	 * @param alertDetails
	 * @return
	 */
	public static String getPhoneForAlertCode(long alertCode, int orgId) {
		Connection conn = null;
		StringBuilder sb =  new StringBuilder(100);
		String emailIds = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			//updated query to fetch user_email list by multiple groups for alert
			String query = "SELECT phone FROM rfx_users WHERE user_id IN ( SELECT user_id FROM alert_group_user_mapping WHERE alert_group_id IN"
					+ "(SELECT alert_group_id FROM alert_group_mapping WHERE alert_id="
					+ "(SELECT id FROM sensor_alert_types WHERE alert_code = ? AND org_id = ? )))";
			PreparedStatement stmt = conn.prepareStatement(query); 
			stmt.setLong(1, alertCode);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			while( rs.next()){
				sb.append(rs.getString("phone")).append(",");
			}
			if(sb.length() > 0){
				emailIds = sb.substring(0, sb.length()-1);
			}
			stmt.close();
		
		}catch(Exception e){
			logger.error("Error while fetching email ids for alert group: " +e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return emailIds;
	}
}
