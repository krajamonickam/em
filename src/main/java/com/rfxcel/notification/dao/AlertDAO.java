package com.rfxcel.notification.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.rfxcel.notification.entity.Alert;
import com.rfxcel.rule.util.RuleConstants;

/**
 * 
 * @author sachin_kohale
 * @since Apr 20, 2017
 */
public class AlertDAO {
	private static AlertDAO alertDAO;
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
	
	public static AlertDAO getInstance(){
		if(alertDAO == null){
			alertDAO = new AlertDAO();
		}
		return alertDAO;
	}
	
	
	/**
	 * 
	 * @param orgId
	 * @param groupId
	 * @return list of Alert
	 * @throws SQLException
	 */
	public List<Alert> getAlertList(Integer orgId) throws SQLException{
		Connection conn =null;
		List<Alert> alertList = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			/*String sqlQury = "SELECT alert.id, alert.alert_code, alert.alert_msg, alert.alert_detail_msg,"
					+ " alert.corrective_action, alert.org_id, alertgroup.group_name"
					+ " FROM sensor_alert_types alert LEFT JOIN alert_groups alertgroup ON alertgroup.id=alert.alert_group_id"
					+ " WHERE alert.org_id=?";*/
			String sqlQuery = "SELECT alert.id, alert.alert_code, alert.alert_msg, alert.alert_detail_msg, alert.corrective_action, alert.org_id"
					+ " FROM sensor_alert_types alert WHERE alert.org_id=? ORDER BY alert.id";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			
			ResultSet rs = stmt.executeQuery();
			Alert alert = null;
			alertList = new ArrayList<Alert>();
			while(rs.next()){
				Long alertCode = rs.getLong("alert_code");
				String attributeName = (String) getKeyFromValue(RuleConstants.ATTRIBUTE_ALERT_MAP, alertCode);
				ArrayList<Long> alertGroupIds = getAlertGroupIds(rs.getLong("id"), conn);
				ArrayList<String> alertGroupNameList = getAlertGroupNameList(rs.getLong("id"), conn);
				alert = new Alert(rs.getLong("id"), rs.getLong("alert_code"), 
						rs.getString("alert_msg"), rs.getString("alert_detail_msg"), rs.getString("corrective_action"), alertGroupIds,
						rs.getInt("org_id"), alertGroupNameList, attributeName);
				alertList.add(alert);
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertList;
	}
	
	/**
	 * 
	 * @return list of Alert
	 * @throws SQLException
	 */
	public List<Alert> getDefaultAlertList() throws SQLException{
		Connection conn =null;
		List<Alert> alertList = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT alert_code, alert_msg, alert_detail_msg, attribute_name FROM sensor_default_alerts";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			ResultSet rs = stmt.executeQuery();
			Alert alert = null;
			alertList = new ArrayList<Alert>();
			while(rs.next()){
				alert = new Alert();
				alert.setAlertCode(rs.getLong("alert_code"));
				alert.setAlertMsg(rs.getString("alert_msg"));
				alert.setAlertDetailMsg(rs.getString("alert_detail_msg"));
				alert.setAttrName(rs.getString("attribute_name"));
				alertList.add(alert);
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertList;
	}
	
	/**
	 * 
	 * @param alert
	 * @throws SQLException
	 */
	public void addAlert(Alert alert) throws SQLException{
		try {
			DAOUtility.runInTransaction(conn->{
				String sqlQury = "INSERT INTO sensor_alert_types(alert_code, alert_msg, alert_detail_msg, corrective_action, org_id) VALUES(?,?,?,?,?)";
				PreparedStatement stmt = conn.prepareStatement(sqlQury,	Statement.RETURN_GENERATED_KEYS);
				String correctiveAction;
				correctiveAction = alert.getCorrectiveAction();
				stmt.setLong(1, alert.getAlertCode());			
				stmt.setString(2, alert.getAlertMsg());
				stmt.setString(3, alert.getAlertDetailMsg());

				if(correctiveAction != null){
					stmt.setString(4, alert.getCorrectiveAction());
				}else{
					stmt.setNull(4, Types.NULL);
				}

				stmt.setLong(5, alert.getOrgId());	
				stmt.executeUpdate();
				ResultSet rs = stmt.getGeneratedKeys();
				long id = -1;
				if (rs.next()) {
					id = rs.getLong(1);
				}
				rs.close();
				stmt.close();
				alert.setAlertId(id);
				addAlertGroupMapping(alert, conn);
				return null;
			});
		} finally{
		}
	}
	
	/**
	 * 
	 * @param alert
	 * @param conn
	 */
	private void addAlertGroupMapping(Alert alert, Connection conn) throws SQLException {
		String query = "INSERT INTO alert_group_mapping (alert_id, alert_group_id) VALUES (?,?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		Long alertId = alert.getAlertId();
		if (alert.getAlertGroupIds() != null && alert.getAlertGroupIds().size() > 0) {
			for (Long alertGroupId : alert.getAlertGroupIds()) {
				stmt.setLong(1, alertId);
				stmt.setLong(2, alertGroupId);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		}

	}
	
	
	/**
	 * 
	 * @param alert
	 * @param profileId 
	 * @param conn
	 */
	public void addProfileAlertGroupMapping(Alert alert, Integer profileId, Connection conn) throws SQLException {
		String query = "INSERT INTO profile_alert_group_mapping (alert_code, profile_id, alert_group_id, org_id,attr_name) VALUES (?,?,?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		if (alert.getAlertGroupIds() != null && alert.getAlertGroupIds().size() > 0) {
			for (Long alertGroupId : alert.getAlertGroupIds()) {
				stmt.setLong(1, alert.getAlertCode());
				stmt.setInt(2, profileId);
				stmt.setLong(3, alertGroupId);
				stmt.setInt(4, alert.getOrgId());
				stmt.setString(5, alert.getAttrName());
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		}
	}
	
	
	/**
	 * 
	 * @param alertId
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<Long> getAlertGroupIds(Long alertId, Connection conn) throws SQLException{
		String sqlQuery = "SELECT alert_group_id FROM alert_group_mapping WHERE alert_id=?";
		PreparedStatement stmt = conn.prepareStatement(sqlQuery);
		stmt.setLong(1, alertId);
		ResultSet rs = stmt.executeQuery();
		ArrayList<Long> alertGroupIds = new ArrayList<Long>();
		while(rs.next()){
			Long alertGroupId = rs.getLong("alert_group_id");
			alertGroupIds.add(alertGroupId);
		}
		rs.close();
		stmt.close();
		return alertGroupIds;		
	}
	
	/**
	 * 
	 * @param alertId
	 * @param conn
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<String> getAlertGroupNameList(Long alertId, Connection conn) throws SQLException{
		String sqlQuery = "SELECT alertgroup.group_name FROM alert_groups alertgroup WHERE alertgroup.id IN(SELECT alert_group_id FROM alert_group_mapping WHERE alert_id=?)";
		PreparedStatement stmt = conn.prepareStatement(sqlQuery);
		stmt.setLong(1, alertId);
		ResultSet rs = stmt.executeQuery();
		ArrayList<String> alertGroupNameList = new ArrayList<String>();
		while(rs.next()){
			String alertGroupName = rs.getString("group_name");
			alertGroupNameList.add(alertGroupName);
		}
		rs.close();
		stmt.close();
		return alertGroupNameList;		
	}
	
	/**
	 * 
	 * @param conn
	 * @param alertId
	 * @throws SQLException
	 */
	public void deleteAlertGroupMapping(Connection conn, Long alertId) throws SQLException{
		String deleteQuery = "DELETE FROM alert_group_mapping WHERE alert_id=?";
		PreparedStatement stmt = conn.prepareStatement(deleteQuery);
		stmt.setLong(1, alertId);
		stmt.executeUpdate();
		stmt.close();
	}
	/**
	 * <p>delete alert-alert group mapping by alertGroupId</p>
	 * @param conn
	 * @param alertGroupId
	 * @throws SQLException
	 */
	public void deleteAlertGroupMappingByAlertGrp(Connection conn, Long alertGroupId) throws SQLException{
		String deleteQuery = "DELETE FROM alert_group_mapping WHERE alert_group_id=?";
		PreparedStatement stmt = conn.prepareStatement(deleteQuery);
		stmt.setLong(1, alertGroupId);
		stmt.executeUpdate();
		stmt.close();
	}

	/**
	 * <p>Its is used to add alert list</P>
	 * @param alerts
	 * @throws SQLException
	 */
	public void addAlerts(Connection conn, List<Alert> alerts, Integer orgId) throws SQLException {
		try{			
			String sqlQury = "INSERT INTO sensor_alert_types(alert_code, alert_msg, alert_detail_msg, corrective_action, org_id) VALUES(?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQury);
			
			for(Alert alert : alerts){
				String correctiveAction;
				correctiveAction = alert.getCorrectiveAction();
				stmt.setLong(1, alert.getAlertCode());			
				stmt.setString(2, alert.getAlertMsg());
				stmt.setString(3, alert.getAlertDetailMsg());
				
				if(correctiveAction != null){
					stmt.setString(4, alert.getCorrectiveAction());
				}else{
					stmt.setNull(4, Types.NULL);
				}
				
				stmt.setInt(5, orgId);	
				stmt.addBatch();				
			}
			stmt.executeBatch();			
			stmt.close();
			
		}finally{
		}
	}
	
	/**
	 * 
	 * @param alert
	 * @throws SQLException
	 */
	public void updateAlert(Alert alert) throws SQLException{
		Connection conn = null;
		int rowUpdated = -1;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "UPDATE sensor_alert_types SET alert_msg=?, alert_detail_msg=?, updated_time=NOW() WHERE id=?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);			
			stmt.setString(1, alert.getAlertMsg());
			stmt.setString(2, alert.getAlertDetailMsg());
			stmt.setLong(3, alert.getAlertId());
			rowUpdated = stmt.executeUpdate();
			stmt.close();
			
			if(rowUpdated > 0){
				//delete alertGroupId from table alert_group_mapping by alertId				
				deleteAlertGroupMapping(conn, alert.getAlertId());
				
				//add alert-alertGroup mapping
				addAlertGroupMapping(alert, conn);
			}
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param alertId
	 * @return
	 * @throws SQLException 
	 */
	public Boolean checkIfAlertIsPresent(Long alertId) throws SQLException{
		Boolean isPresent = false;
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT EXISTS(SELECT 1 FROM sensor_alert_types WHERE id = ?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count != 0){
				isPresent = true;
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * 
	 * @param alertId
	 * @throws SQLException
	 */
	public void deleteAlert(Long alertId) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "DELETE FROM sensor_alert_types WHERE id=?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertId);
			stmt.executeUpdate();
			stmt.close();
			
			//delete alert-alertGroup Mapping
			deleteAlertGroupMapping(conn, alertId);
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param alertId
	 * @throws SQLException
	 */
	public void deleteAlertsByOrg(Connection conn, int orgId) throws SQLException {
		//get alertId list by orgId
		ArrayList<Long> alertIds = getAlertIdsByOrg(conn, orgId);
		
		String sqlQuery = "DELETE FROM sensor_alert_types WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(sqlQuery);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
		
		//delete alert-alertGroup mapping by alertId list
		deleteAlertGroupMappingByAlertIds(conn, alertIds);
	}
	
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	private ArrayList<Long> getAlertIdsByOrg(Connection conn, int orgId) throws SQLException{
		String sqlQuery = "SELECT id FROM sensor_alert_types WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(sqlQuery);
		stmt.setInt(1, orgId);
		ResultSet rs = stmt.executeQuery();
		ArrayList<Long> alertIds = new ArrayList<Long>();
		while(rs.next()){
			Long alertId = rs.getLong("id");
			alertIds.add(alertId);
		}
		rs.close();
		stmt.close();
		return alertIds;
	}
	
	/**
	 * 
	 * @param conn
	 * @param alertIds
	 * @throws SQLException
	 */
	private void deleteAlertGroupMappingByAlertIds(Connection conn, ArrayList<Long> alertIds) throws SQLException{
		String deleteQuery = "DELETE FROM alert_group_mapping WHERE alert_id=?";
		PreparedStatement stmt = conn.prepareStatement(deleteQuery);
		for(Long alertId : alertIds){
			stmt.setLong(1, alertId);
			stmt.addBatch();
		}
		stmt.executeBatch();
		stmt.close();
		
	}

	/**
	 * 
	 * @param map
	 * @param value
	 * @return
	 */
	public Object getKeyFromValue(Map map, Object value) {
		for (Object key : map.keySet()) {			
			if (map.get(key).equals(value)) {				
				return key;
			}
		}
		return null;
	}
	
	/**
	 * 
	 * @param alertCode
	 * @param orgId
	 * @return
	 * @throws SQLException 
	 */
	public Boolean checkIfAlertCodeIsPresent(Long alertCode, Integer orgId) throws SQLException{
		Connection conn = null;
		Boolean isPresent = false;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT EXISTS (SELECT 1 FROM sensor_alert_types WHERE alert_code = ?  AND org_id = ?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertCode);
			stmt.setInt(2, orgId);			
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count != 0){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * <p>get alert details by attrName</p>
	 * @param attrName
	 * @return
	 * @throws SQLException
	 */
	public Alert getAlertDetailsByAttrName(String attrName) throws SQLException{
		Connection conn = null;
		Alert alert = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT alert_code, alert_msg, alert_detail_msg FROM sensor_default_alerts WHERE attribute_name=?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, attrName);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				alert = new Alert();
				alert.setAlertCode(rs.getLong("alert_code"));
				alert.setAlertMsg(rs.getString("alert_msg"));
				alert.setAlertDetailMsg(rs.getString("alert_detail_msg"));
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alert;
	}

/**
 * 
 * @param alertId
 * @return
 * @throws SQLException
 */
	public Alert getAlertById(Long alertId) throws SQLException{
		Connection conn = null;
		Alert alert = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT alert_code, alert_msg, alert_detail_msg,corrective_action FROM sensor_alert_types where id = ?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				alert = new Alert();
				alert.setAlertId(alertId);
				alert.setAlertCode(rs.getLong("alert_code"));
				alert.setAlertMsg(rs.getString("alert_msg"));
				alert.setAlertDetailMsg(rs.getString("alert_detail_msg"));
				alert.setCorrectiveAction(rs.getString("corrective_action"));
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alert;
	}
	
	/**
	 * 
	 * @param attrName
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Long getAlertId(String attrName, Integer orgId) throws SQLException{
		Connection conn = null;
		Long id =null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT id from sensor_alert_types where alert_code=(Select alert_code from sensor_default_alerts where attribute_name=?) and org_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, attrName);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				id = rs.getLong("id");
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return id;
	}
	
	/**
	 * @param alert
	 * @param profileId
	 * @param conn
	 * @throws SQLException
	 */
	public void addProfileAlertDetailMsg(Alert alert, Integer profileId, Connection conn)throws SQLException{
		String query = "INSERT INTO profile_alert_types (alert_code, alert_detail_msg,profile_id, org_id,attr_name) VALUES (?,?,?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setLong(1, alert.getAlertCode());
		stmt.setString(2, alert.getAlertDetailMsg());
		stmt.setInt(3, profileId);
		stmt.setLong(4, alert.getOrgId());
		stmt.setString(5, alert.getAttrName());
		stmt.addBatch();
		stmt.executeBatch();
		stmt.close();
	}
	
	/**
	 * @param alert
	 * @param conn
	 * @throws SQLException
	 */
	public void addProfileAlertEmailIdMapping(Alert alert,Integer profileId, Connection conn) throws SQLException {
		String query = "INSERT INTO alert_email_id_mapping (alert_code, attr_name, profile_id, email_id, org_id) VALUES (?,?,?,?,?)";
		PreparedStatement stmt = conn.prepareStatement(query);
		if (alert.getEmailIds() != null && alert.getEmailIds().size() > 0) {
			for (String emailId : alert.getEmailIds()) {
				stmt.setLong(1, alert.getAlertCode());
				stmt.setString(2, alert.getAttrName());
				stmt.setInt(3, profileId);
				stmt.setString(4, emailId);
				stmt.setInt(5, alert.getOrgId());
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		}
	}
	
	/**
	 * @param attrName
	 * @param profileId 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> getProfileAttrAlertEmailIds(String attrName, Integer profileId) throws SQLException{
		Connection conn = null;
		ArrayList<String> emailIds =new ArrayList<String>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT email_id from alert_email_id_mapping where attr_name=? and profile_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, attrName);
			stmt.setInt(2, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				emailIds.add(rs.getString("email_id"));
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return emailIds;
	}
	
	
	/**
	 * 
	 * @param alertCode
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> getProfileAttrAlertEmailIds(Long alertCode, Integer profileId) throws SQLException{
		Connection conn = null;
		ArrayList<String> emailIds =new ArrayList<String>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT email_id from alert_email_id_mapping where alert_code=? and profile_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertCode);
			stmt.setInt(2, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				emailIds.add(rs.getString("email_id"));
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return emailIds;
	}
	
	/**
	 * @param attrName
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Long> getProfileAttrAlertGroupIds(String attrName, Integer profileId) throws SQLException{
		Connection conn = null;
		ArrayList<Long> alertGroupIds =new ArrayList<Long>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT alert_group_id from profile_alert_group_mapping where attr_name=? and profile_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, attrName);
			stmt.setInt(2, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				alertGroupIds.add(rs.getLong("alert_group_id"));
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertGroupIds;
	}
	
	/**
	 * 
	 * @param alertCode
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Long> getProfileAttrAlertGroupIds(Long alertCode, Integer profileId) throws SQLException{
		Connection conn = null;
		ArrayList<Long> alertGroupIds =new ArrayList<Long>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT alert_group_id from profile_alert_group_mapping where alert_code=? and profile_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setLong(1, alertCode);
			stmt.setInt(2, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				alertGroupIds.add(rs.getLong("alert_group_id"));
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return alertGroupIds;
	}
	
	/**
	 * @param attrName
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public String getProfileAttrAlertMessage(String attrName, Integer profileId) throws SQLException{
		Connection conn = null;
		String detailMessage= null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT alert_detail_msg from profile_alert_types where attr_name=? and profile_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, attrName);
			stmt.setInt(2, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				detailMessage = rs.getString("alert_detail_msg");
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return detailMessage;
	}
	
	/**
	 * @param profileId
	 * @param conn
	 * @throws SQLException
	 */
	public void deleteProfileAlertGroupMapping(Integer profileId, Connection conn) throws SQLException{
		String query = "DELETE FROM profile_alert_group_mapping where profile_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, profileId);
		stmt.executeUpdate();
		stmt.close();
	}
	
	/**
	 * @param profileId
	 * @param conn
	 * @throws SQLException
	 */
	public void deleteProfileAlertDetailMsg(Integer profileId, Connection conn) throws SQLException{
		String query = "DELETE FROM profile_alert_types where profile_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, profileId);
		stmt.executeUpdate();
		stmt.close();
	}
	
	/**
	 * @param profileId
	 * @param conn
	 * @throws SQLException
	 */
	public void deleteProfileAlertEmailIdMapping(Integer profileId, Connection conn) throws SQLException{
		String query = "DELETE FROM alert_email_id_mapping where profile_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, profileId);
		stmt.executeUpdate();
		stmt.close();
	}
	
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteProfileAlerts(Connection conn, int orgId) throws SQLException {
		String sqlQuery = "DELETE FROM profile_alert_group_mapping WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(sqlQuery);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		
		sqlQuery = "DELETE FROM profile_alert_types WHERE org_id=?";
		stmt = conn.prepareStatement(sqlQuery);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		
		sqlQuery = "DELETE FROM alert_email_id_mapping WHERE org_id=?";
		 stmt = conn.prepareStatement(sqlQuery);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	
	}
}