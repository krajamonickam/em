/**
 * 
 */
package com.rfxcel.notification.dao;

import java.sql.Connection;
import java.sql.SQLException;

/**
 * @author Albeena May 16, 2017
 *
 */
public interface Transaction {
	
	public Object execute(Connection connection) throws SQLException;

}
