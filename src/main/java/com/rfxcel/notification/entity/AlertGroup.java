package com.rfxcel.notification.entity;

import java.sql.Timestamp;
import java.util.Arrays;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;



/**
 * 
 * @author Albeena 
 * @Since Apr 21, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AlertGroup {

	private Long id;
	private String groupName;
	private String description;
	private Long orgId;
	private Long groupId;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private String[] users;
	
	public AlertGroup() {
		
	}

	/**
	 * @param id
	 * @param groupName
	 * @param description
	 * @param orgId
	 * @param groupId
	 * @param createdOn
	 * @param updatedOn
	 * @param users
	 */
	public AlertGroup(Long id, String groupName, String description, Long orgId,
			Long groupId, Timestamp createdOn, Timestamp updatedOn,
			String[] users) {
		this.id = id;
		this.groupName = groupName;
		this.description = description;
		this.orgId = orgId;
		this.groupId = groupId;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.users = users;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the groupName
	 */
	public String getGroupName() {
		return groupName;
	}

	/**
	 * @param groupName the groupName to set
	 */
	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the orgId
	 */
	public Long getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the users
	 */
	public String[] getUsers() {
		return users;
	}

	/**
	 * @param users the users to set
	 */
	public void setUsers(String[] users) {
		this.users = users;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ALertGroup ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (groupName != null ? "groupName=" + groupName + ", " : "")
				+ (description != null ? "description=" + description + ", "
						: "") + "orgId=" + orgId + ", "
				+ (groupId != null ? "groupId=" + groupId + ", " : "")
				+ (createdOn != null ? "createdOn=" + createdOn + ", " : "")
				+ (updatedOn != null ? "updatedOn=" + updatedOn + ", " : "")
				+ (users != null ? "users=" + Arrays.toString(users) : "")
				+ "]";
	}

	
	/**
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
}
