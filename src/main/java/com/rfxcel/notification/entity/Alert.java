package com.rfxcel.notification.entity;

import java.sql.Timestamp;
import java.util.ArrayList;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * 
 * @author sachin_kohale
 * @since Apr 20, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Alert {
	
	private Long alertId;
	
	private Long alertCode;
	private String alertMsg;
	private String alertDetailMsg;
	private String correctiveAction;
	private ArrayList<Long> alertGroupIds;
	private Integer orgId;
	private ArrayList<String> alertGroupNameList;
	private String attrName;
	private Timestamp createdTime;
	private Timestamp updatedTime;
	private ArrayList<String> emailIds;
	
	public Alert(){
		
	}
	
	/**
	 * 
	 * @param alertId
	 * @param alertCode
	 * @param alertMsg
	 * @param alertDetailMsg
	 * @param correctiveAction
	 * @param alertGroupIds
	 * @param orgId
	 * @param alertGroupName
	 * @param attrName
	 */
	public Alert(Long alertId, Long alertCode, String alertMsg, String alertDetailMsg, String correctiveAction,
			ArrayList<Long> alertGroupIds, Integer orgId, ArrayList<String> alertGroupNameList, String attrName) {
		super();
		this.alertId = alertId;		
		this.alertCode = alertCode;
		this.alertMsg = alertMsg;
		this.alertDetailMsg = alertDetailMsg;
		this.correctiveAction = correctiveAction;
		this.alertGroupIds = alertGroupIds;
		this.orgId = orgId;		
		this.alertGroupNameList = alertGroupNameList;
		this.attrName = attrName;
	}

	/**
	 * 
	 * @return
	 */
	public Long getAlertId() {
		return alertId;
	}

	/**
	 * 
	 * @param alertId
	 */
	public void setAlertId(Long alertId) {
		this.alertId = alertId;
	}

	/**
	 * 
	 * @return
	 */
	public Long getAlertCode() {
		return alertCode;
	}

	/**
	 * 
	 * @param alertCode
	 */
	public void setAlertCode(Long alertCode) {
		this.alertCode = alertCode;
	}

	public String getAlertMsg() {
		return alertMsg;
	}

	/**
	 * 
	 * @param alertMsg
	 */
	public void setAlertMsg(String alertMsg) {
		this.alertMsg = alertMsg;
	}

	/**
	 * 
	 * @return
	 */
	public String getAlertDetailMsg() {
		return alertDetailMsg;
	}

	/**
	 * 
	 * @param alertDetailMsg
	 */
	public void setAlertDetailMsg(String alertDetailMsg) {
		this.alertDetailMsg = alertDetailMsg;
	}

	/**
	 * 
	 * @return
	 */
	public String getCorrectiveAction() {
		return correctiveAction;
	}

	/**
	 * 
	 * @param correctiveAction
	 */
	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}


	/**
	 * 
	 * @return
	 */
	public ArrayList<Long> getAlertGroupIds() {
		return alertGroupIds;
	}

	/**
	 * 
	 * @param alertGroupIds
	 */
	public void setAlertGroupIds(ArrayList<Long> alertGroupIds) {
		this.alertGroupIds = alertGroupIds;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * 
	 * @param orgId
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * 
	 * @return
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * 
	 * @param createdTime
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * 
	 * @return
	 */
	public Timestamp getUpdatedTime() {
		return updatedTime;
	}

	/**
	 * 
	 * @param updatedTime
	 */
	public void setUpdatedTime(Timestamp updatedTime) {
		this.updatedTime = updatedTime;
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<String> getAlertGroupNameList() {
		return alertGroupNameList;
	}

	/**
	 * 
	 * @param alertGroupNameList
	 */
	public void setAlertGroupNameList(ArrayList<String> alertGroupNameList) {
		this.alertGroupNameList = alertGroupNameList;
	}

	/**
	 * 
	 * @return
	 */
	public String getAttrName() {
		return attrName;
	}

	/**
	 * 
	 * @param attrName
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	
	/**
	 * @return the emailIds
	 */
	public ArrayList<String> getEmailIds() {
		return emailIds;
	}

	/**
	 * @param emailIds the emailIds to set
	 */
	public void setEmailIds(ArrayList<String> emailIds) {
		this.emailIds = emailIds;
	}

	/**
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
}
