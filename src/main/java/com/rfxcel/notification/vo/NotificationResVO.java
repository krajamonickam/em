package com.rfxcel.notification.vo;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.sensor.vo.Response;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class NotificationResVO extends Response{
	
	private String dateTime;
	private ArrayList<ShipmentNotifications> shipmentNotifications;
	private ArrayList<NotificationVO> notifications;
	private Integer count;
	
	/**
	 * @param responseStatus the responseStatus to set
	 */
	public NotificationResVO setResponseStatus(String responseStatus) {
		super.setResponseStatus(responseStatus);
		return this;
	}
	
	/**
	 * @param responseMessage the responseMessage to set
	 */
	public NotificationResVO setResponseMessage(String responseMessage) {
		super.setResponseMessage(responseMessage);
		return this;
	}
	
	/**
	 * @return the dateTime
	 */
	@JsonProperty("dateTime")
	public String getDateTime() {
		return dateTime;
	}
	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return the shipmentNotifications
	 */
	@JsonProperty("shipmentNotifications")
	public ArrayList<ShipmentNotifications> getShipmentNotifications() {
		return shipmentNotifications;
	}
	/**
	 * @param shipmentNotifications the shipmentNotifications to set
	 */
	public void setShipmentNotifications(ArrayList<ShipmentNotifications> shipmentNotifications) {
		this.shipmentNotifications = shipmentNotifications;
	}
	/**
	 * @return the notifications
	 */
	@JsonProperty("notifications")
	public ArrayList<NotificationVO> getNotifications() {
		return notifications;
	}
	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(ArrayList<NotificationVO> notifications) {
		this.notifications = notifications;
	}
	/**
	 * @return the count
	 */
	@JsonProperty("count")
	public Integer getCount() {
		return count;
	}
	/**
	 * @param count the count to set
	 */
	public void setCount(Integer count) {
		this.count = count;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NotificationResVO ["
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (shipmentNotifications != null ? "shipmentNotifications="	+ shipmentNotifications + ", " : "")
				+ (notifications != null ? "notifications=" + notifications	+ ", " : "")
				+ (count != null ? "count=" + count + ", " : "")
				+ (getDateTime() != null ? "getDateTime()=" + getDateTime()	+ ", " : "")
				+ (getShipmentNotifications() != null ? "getShipmentNotifications()="+ getShipmentNotifications() + ", ": "")
				+ (getNotifications() != null ? "getNotifications()="+ getNotifications() + ", " : "")
				+ (getCount() != null ? "getCount()=" + getCount() : "") + "]";
	}
	

}
