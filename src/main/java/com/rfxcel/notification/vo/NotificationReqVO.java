package com.rfxcel.notification.vo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.sensor.vo.Request;


@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class NotificationReqVO extends Request{
	private String dateTime;
	private String sortBy;
	private String searchType;
	private String viewType;
	private String groupBy;
	private Integer start;
	private Integer limit;
	private DeviceDataVO deviceData;
	private String user;
	private String searchString;
	/**
	 * 
	 * @return
	 * The dateTime
	 */
	@JsonProperty("dateTime")
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * 
	 * @param dateTime
	 * The dateTime
	 */
	@JsonProperty("dateTime")
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * 
	 * @return
	 * The sortBy
	 */
	@JsonProperty("sortBy")
	public String getSortBy() {
		return sortBy;
	}

	/**
	 * 
	 * @param sortBy
	 * The sortBy
	 */
	@JsonProperty("sortBy")
	public void setSortBy(String sortBy) {
		this.sortBy = sortBy;
	}

	/**
	 * 
	 * @return
	 * The searchType
	 */
	@JsonProperty("searchType")
	public String getSearchType() {
		return searchType;
	}

	/**
	 * 
	 * @param searchType
	 * The searchType
	 */
	@JsonProperty("searchType")
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}

	/**
	 * 
	 * @return
	 * The viewType
	 */
	@JsonProperty("viewType")
	public String getViewType() {
		return viewType;
	}

	/**
	 * 
	 * @param viewType
	 * The viewType
	 */
	@JsonProperty("viewType")
	public void setViewType(String viewType) {
		this.viewType = viewType;
	}

	/**
	 * @return the groupBy
	 */
	@JsonProperty("groupBy")
	public String getGroupBy() {
		return groupBy;
	}

	/**
	 * @param groupBy the groupBy to set
	 */
	public void setGroupBy(String groupBy) {
		this.groupBy = groupBy;
	}

	/**
	 * 
	 * @return
	 * The start
	 */
	@JsonProperty("start")
	public Integer getStart() {
		return start;
	}

	/**
	 * 
	 * @param start
	 * The start
	 */
	@JsonProperty("start")
	public void setStart(Integer start) {
		this.start = start;
	}

	/**
	 * 
	 * @return
	 * The limit
	 */
	@JsonProperty("limit")
	public Integer getLimit() {
		return limit;
	}

	/**
	 * 
	 * @param limit
	 * The limit
	 */
	@JsonProperty("limit")
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	/**
	 * 
	 * @return
	 * The deviceData
	 */
	@JsonProperty("deviceData")
	public DeviceDataVO getDeviceData() {
		return deviceData;
	}

	/**
	 * 
	 * @param deviceData
	 * The deviceData
	 */
	@JsonProperty("deviceData")
	public void setDeviceData(DeviceDataVO deviceData) {
		this.deviceData = deviceData;
	}

	/**
	 * @return the user
	 */
	@JsonProperty("user")
	public String getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(String user) {
		this.user = user;
	}
	/**
	 * 
	 * @return searchString
	 */
	public String getSearchString() {
		return searchString;
	}

	/**
	 * 
	 * @param searchString the searchString to set
	 */
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NotificationReqVO ["
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (sortBy != null ? "sortBy=" + sortBy + ", " : "")
				+ (searchType != null ? "searchType=" + searchType + ", " : "")
				+ (viewType != null ? "viewType=" + viewType + ", " : "")
				+ (groupBy != null ? "groupBy=" + groupBy + ", " : "")
				+ (start != null ? "start=" + start + ", " : "")
				+ (limit != null ? "limit=" + limit + ", " : "")
				+ (deviceData != null ? "deviceData=" + deviceData + ", " : "")
				+ (searchString != null ? "searchString=" + searchString : "") + "]";
	}

}