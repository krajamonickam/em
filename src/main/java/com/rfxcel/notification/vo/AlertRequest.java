package com.rfxcel.notification.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.notification.entity.Alert;
import com.rfxcel.sensor.vo.Request;

/**
 * 
 * @author sachin_kohale
 * @since Apr 20, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AlertRequest extends Request{
	private Alert alert;
	private List<Alert> alertList;
	
	/**
	 * 
	 * @return
	 */
	public Alert getAlert() {
		return alert;
	}

	/**
	 * 
	 * @param alert
	 */
	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	/**
	 * 
	 * @return
	 */
	public List<Alert> getAlertList() {
		return alertList;
	}

	/**
	 * 
	 * @param alertList
	 */
	public void setAlertList(List<Alert> alertList) {
		this.alertList = alertList;
	}
	

}
