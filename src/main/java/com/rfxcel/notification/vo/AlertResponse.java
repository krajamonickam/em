package com.rfxcel.notification.vo;

import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.notification.entity.Alert;
import com.rfxcel.sensor.vo.Response;

/**
 * 
 * @author sachin_kohale
 * @since Apr 20, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_EMPTY)
public class AlertResponse extends Response{
	private List<Alert> alertList;
	private Alert alert;
	
	/**
	 * 
	 * @return
	 */
	public Alert getAlert() {
		return alert;
	}

	/**
	 * 
	 * @param alert
	 */
	public void setAlert(Alert alert) {
		this.alert = alert;
	}

	HashMap <String, Long> attributeAlertMap = new HashMap<String, Long>();
	
	public AlertResponse(){		
	}
	
	public AlertResponse(String dateTime){
		super.setDateTime(dateTime);
	}

	/**
	 * 
	 * @return
	 */
	public List<Alert> getAlertList() {
		return alertList;
	}

	/**
	 * 
	 * @param alertList
	 */
	public void setAlertList(List<Alert> alertList) {
		this.alertList = alertList;
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<String, Long> getAttributeAlertMap() {
		return attributeAlertMap;
	}

	/**
	 * 
	 * @param attributeAlertMap
	 */
	public void setAttributeAlertMap(HashMap<String, Long> attributeAlertMap) {
		this.attributeAlertMap = attributeAlertMap;
	}
	
}
