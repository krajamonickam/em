package com.rfxcel.notification.vo;

import java.util.ArrayList;

import com.rfxcel.cache.ConfigCache;

public class EmailDetailsVO {

	private String toAddresses;
	private String subject;
	private String message;
	private String title;
	private String saluation = "Dear User";
	private ArrayList<String> imageMapping;
	/**
	 * @param toAddress
	 * @param subject
	 * @param message
	 * @param title
	 */
	public EmailDetailsVO(String toAddresses, String subject, String message,	String title, ArrayList<String> imageMappings) {
		this.toAddresses = toAddresses;
		this.subject = subject;
		this.message = message;
		this.title = title;
		if(ConfigCache.getInstance().getSystemConfig("sensor.owner.instance.name")!= null && 
				!ConfigCache.getInstance().getSystemConfig("sensor.owner.instance.name").isEmpty() ){
			this.saluation = ConfigCache.getInstance().getSystemConfig("sensor.owner.instance.name");
		}
		this.imageMapping = imageMappings;
	}
	
	/**
	 * 
	 * @param toAddresses
	 * @param subject
	 * @param message
	 * @param title
	 * @param salutation
	 */
	public EmailDetailsVO(String toAddresses, String subject, String message,	String title, String salutation) {
		this.toAddresses = toAddresses;
		this.subject = subject;
		this.message = message;
		this.title = title;
		this.saluation = salutation;
	}
	
	/**
	 * @return the toAddresses
	 */
	public String getToAddresses() {
		return toAddresses;
	}

	/**
	 * @param toAddresses the toAddresses to set
	 */
	public void setToAddresses(String toAddresses) {
		this.toAddresses = toAddresses;
	}

	/**
	 * @return the subject
	 */
	public String getSubject() {
		return subject;
	}
	/**
	 * @param subject the subject to set
	 */
	public void setSubject(String subject) {
		this.subject = subject;
	}
	
	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
	/**
	 * @param message the message to set
	 */
	public void setMessage(String message) {
		this.message = message;
	}
	
	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}
	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the saluation
	 */
	public String getSaluation() {
		return saluation;
	}

	/**
	 * @param saluation the saluation to set
	 */
	public void setSaluation(String saluation) {
		this.saluation = saluation;
	}

	public ArrayList<String> getImageMapping() {
		return imageMapping;
	}

	public void setImageMapping(ArrayList<String> imageMapping) {
		this.imageMapping = imageMapping;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "EmailDetailsVO ["
				+ (toAddresses != null ? "toAddresses=" + toAddresses + ", ": "")
				+ (subject != null ? "subject=" + subject + ", " : "")
				+ (message != null ? "message=" + message + ", " : "")
				+ (title != null ? "title=" + title : "") + "]";
	}
}
