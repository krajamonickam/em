package com.rfxcel.notification.vo;

import com.rfxcel.notification.entity.AlertGroup;
import com.rfxcel.sensor.vo.Request;

public class AlertGroupRequest extends Request{
	
	private AlertGroup alertGroup;

	/**
	 * 
	 */
	public AlertGroupRequest() {
	}

	/**
	 * @return the alertGroup
	 */
	public AlertGroup getAlertGroup() {
		return alertGroup;
	}

	/**
	 * @param alertGroup the alertGroup to set
	 */
	public void setAlertGroup(AlertGroup alertGroup) {
		this.alertGroup = alertGroup;
	}

}
