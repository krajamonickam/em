package com.rfxcel.notification.vo;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DeviceDataVO {
	private String deviceId;
	private String packageId;
	private String productId;

	/**
	 * 
	 * @return
	 * The deviceId
	 */
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * 
	 * @param deviceId
	 * The deviceId
	 */
	@JsonProperty("deviceId")
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * 
	 * @return
	 * The packageId
	 */
	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}

	/**
	 * 
	 * @param packageId
	 * The packageId
	 */
	@JsonProperty("packageId")
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * 
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * 
	 * @param productId
	 */
	@JsonProperty("productId")
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeviceDataVO ["
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (packageId != null ? "packageId=" + packageId : "") + "]";
	}

}
