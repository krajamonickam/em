package com.rfxcel.notification.vo;

import java.util.List;

import com.rfxcel.notification.entity.AlertGroup;
import com.rfxcel.sensor.vo.Response;

/**
 * 
 * @author sachin_kohale
 * @since Apr 13, 2017 
 */
public class AlertGroupResponse extends Response {
	private List<AlertGroup> alertGroupList;
	
	/**
	 * 
	 */
	public AlertGroupResponse() {
	}
	
	/**
	 * 
	 * @param dateTime
	 */
	public AlertGroupResponse(String dateTime){
		super.setDateTime(dateTime);
	}

	/**
	 * 
	 * @return alertGroupList
	 */
	public List<AlertGroup> getAlertGroupList() {
		return alertGroupList;
	}

	/**
	 * 
	 * @param alertGroupList to alertGroupList
	 */
	public void setAlertGroupList(List<AlertGroup> alertGroupList) {
		this.alertGroupList = alertGroupList;
	}
	
}
