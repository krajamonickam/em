package com.rfxcel.notification.vo;

import java.util.HashMap;

import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * 
 * @author Tejshree Kachare
 * @since 07 Nov 2016
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AlertDetailsVO {

	private Integer id;
	private String alertMsg;
	private String alertDetailMsg;
	private Integer alertGroupId;
	private Long alertCode;
	private String correctiveAction;
	private Boolean status;
	private HashMap<String, String> attributeValueMap = new HashMap<String, String>();
	private Integer orgId;
	private String attributeName;
	
	
	public AlertDetailsVO() {
	}
	
	public AlertDetailsVO(Long alertCode, HashMap<String, String> attrValueMap, Integer orgId) {
		this.alertCode = alertCode;
		this.attributeValueMap = attrValueMap;
		this.orgId = orgId;
	}	
	
	/**
	 * @return
	 */
	public Integer getId() {
		return id;
	}

	/**
	 * @param id
	 */
	public void setId(Integer id) {
		this.id = id;
	}


	/**
	 * @return
	 */
	public String getAlertMsg() {
		return alertMsg;
	}

	/**
	 * 
	 * @param alertMsg
	 */
	public void setAlertMsg(String alertMsg) {
		this.alertMsg = alertMsg;
	}

	/**
	 * @return
	 */
	public String getAlertDetailMsg() {
		return alertDetailMsg;
	}

	/**
	 * @param alertDetailMsg
	 */
	public void setAlertDetailMsg(String alertDetailMsg) {
		this.alertDetailMsg = alertDetailMsg;
	}

	/**
	 * @return
	 */
	public Integer getAlertGroupId() {
		return alertGroupId;
	}

	/**
	 * @param alertGroupId
	 */
	public void setAlertGroupId(Integer alertGroupId) {
		this.alertGroupId = alertGroupId;
	}
	/**
	 * @param alertCode the alertCode to set
	 */
	public void setAlertCode(Long alertCode) {
		this.alertCode = alertCode;
	}
	
	/**
	 * @return the alertCode
	 */
	public Long getAlertCode() {
		return alertCode;
	}
	
	/**
	 * @return the correctiveAction
	 */
	public String getCorrectiveAction() {
		return correctiveAction;
	}
	
	/**
	 * @param correctiveAction the correctiveAction to set
	 */
	public void setCorrectiveAction(String correctiveAction) {
		this.correctiveAction = correctiveAction;
	}

	/**
	 * @return the status
	 */
	public Boolean getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Boolean status) {
		this.status = status;
	}
	
	/**
	 * @return the attributeValueMap
	 */
	public HashMap<String, String> getAttributeValueMap() {
		return attributeValueMap;
	}
	
	/**
	 * @param attributeValueMap the attributeValueMap to set
	 */
	public void setAttributeValueMap(HashMap<String, String> attributeValueMap) {
		this.attributeValueMap = attributeValueMap;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return
	 */
	public String getAttributeName() {
		return attributeName;
	}

	/**
	 * @param attributeName
	 */
	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AlertDetailsVO ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (alertMsg != null ? "alertMsg=" + alertMsg + ", " : "")
				+ (alertDetailMsg != null ? "alertDetailMsg=" + alertDetailMsg+ ", " : "")
				+ (alertGroupId != null ? "alertGroupId=" + alertGroupId + ", "	: "")
				+ (alertCode != null ? "alertCode=" + alertCode + ", " : "")
				+ (correctiveAction != null ? "correctiveAction="+ correctiveAction + ", " : "")
				+ (status != null ? "status=" + status + ", " : "")
				+ (attributeValueMap != null ? "attributeValueMap="	+ attributeValueMap + ", " : "")
				+ (orgId != null ? "orgId=" + orgId : "") + "]";
	}
	
}
