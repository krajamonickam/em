package com.rfxcel.notification.vo;

import java.util.Date;

import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * 
 * @author Tejshree Kachare
 * @since 07 Nov 2016
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class NotificationVO {
	public static final int STATE_GREEN  = 0;
	public static final int STATE_YELLOW = 1;
	public static final int STATE_RED    = 2;
	private Long notificationId;
	private AlertDetailsVO alertDetails;
	private String notification;
	private String deviceId;
	private String containerId;
	private String productId;
	private Long ruleId;
	private Long status;
	private Long sensorDataId;
	private String createdOn;
	private Boolean replicateNotification;
	public String getLatitude() {
		return latitude;
	}


	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}


	public String getLongitude() {
		return longitude;
	}


	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}


	private String latitude;
	private String longitude;
	
	
	@JsonIgnore
	private Date createdDate;
	private Integer state; 	
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param ruleId
	 * @param sensorDataId
	 * @param alertDetailsVO
	 */
	public NotificationVO(String deviceId, String containerId,String productId, Long ruleId, Long sensorDataId, AlertDetailsVO alertDetailsVO) {
		this.deviceId =  deviceId;
		this.containerId = containerId;
		this.ruleId = ruleId;
		this.alertDetails = alertDetailsVO;
		this.sensorDataId = sensorDataId;
		this.productId =  productId;
	}


	/**
	 * 
	 * @param notificationId
	 * @param notification
	 * @param deviceId
	 * @param containerId
	 * @param createdOn
	 */
	/*public NotificationVO(Long notificationId, String notification,String deviceId, String containerId,String createdOn,String productId) {
		this.notificationId = notificationId;
		this.notification = notification;
		this.deviceId = deviceId;
		this.containerId = containerId;
		this.createdOn = createdOn;

	}*/

	/**
	 * 
	 */
	public NotificationVO(){

	}
	/**
	 * 
	 * @param notificationId
	 * @param notification
	 * @param createdOn
	 * @param productId
	 */
	public NotificationVO(Long notificationId, String notification, String createdOn,String productId) {
		this.notificationId = notificationId;
		this.notification = notification;
		this.createdOn = createdOn;
		this.productId =productId;
	}

	/**
	 * 
	 * @param notificationId
	 * @param notification
	 * @param deviceId
	 * @param containerId
	 * @param createdOn
	 */
	public NotificationVO(long notificationId, String notification, String deviceId,String containerId, String createdOn) {
		this.notificationId = notificationId;
		this.notification = notification;
		this.deviceId = deviceId;
		this.containerId = containerId;
		this.createdOn = createdOn;
	}


	/**
	 * @return the notificationId
	 */
	public Long getNotificationId() {
		return notificationId;
	}

	/**
	 * @param notificationId the notificationId to set
	 */
	public void setNotificationId(Long notificationId) {
		this.notificationId = notificationId;
	}

	/**
	 * @return the alertDetails
	 */
	public AlertDetailsVO getAlertDetails() {
		return alertDetails;
	}

	/**
	 * @param alertDetails the alertDetails to set
	 */
	public void setAlertDetails(AlertDetailsVO alertDetails) {
		this.alertDetails = alertDetails;
	}

	/**
	 * @return the alertCode
	 */
	public Long getAlertCode() {
		if(this.getAlertDetails() != null )
			return this.getAlertDetails().getAlertCode();
		else 
			return null;
	}

	/**
	 * @param alertCode the alertCode to set
	 */
	public void setAlertCode(Long alertCode) {
		this.getAlertDetails().setAlertCode(alertCode);
	}

	/**
	 * @return the notification
	 */
	public String getNotification() {
		return notification;
	}

	/**
	 * @param notification the notification to set
	 */
	public void setNotification(String notification) {
		this.notification = notification;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the ruleId
	 */
	public Long getRuleId() {
		return ruleId;
	}

	/**
	 * @param ruleId the ruleId to set
	 */
	public void setRuleId(Long ruleId) {
		this.ruleId = ruleId;
	}

	/**
	 * @return the status
	 */
	public Long getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(Long status) {
		this.status = status;
	}

	/**
	 * @return the sensorDataId
	 */
	public Long getSensorDataId() {
		return sensorDataId;
	}

	/**
	 * @param sensorDataId the sensorDataId to set
	 */
	public void setSensorDataId(Long sensorDataId) {
		this.sensorDataId = sensorDataId;
	}

	/**
	 * @return the createdOn
	 */
	public String getCreatedOn() {
		return createdOn;
	}



	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}



	/**
	 * @param createdDate is used to save (but not display) notification created date
	 */
	public Date getCreatedDate() {
		return createdDate;
	}


	/**
	 * @param createdDate is used to save (but not display) notification created date
	 */
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}


	/**
	 * @return the containerId
	 */
	public String getContainerId() {
		return containerId;
	}

	/**
	 * @param containerId the containerId to set
	 */
	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	/**
	 * 
	 * @return product id
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * 
	 * @param sets productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}


	/**
	 * @return the replicateNotification
	 */
	public Boolean getReplicateNotification() {
		return replicateNotification;
	}


	/**
	 * @param replicateNotification the replicateNotification to set
	 */
	public void setReplicateNotification(Boolean replicateNotification) {
		this.replicateNotification = replicateNotification;
	}


	public Integer getState() {
		return state;
	}


	public void setState(Integer state) {
		this.state = state;
	}
	
	public String toLogFormat() {
		return "notification ("
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (containerId != null ? "containerId=" + containerId + ", ": "")
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (notification != null ? "notification=" + notification + ", "	: "")
				+ (sensorDataId != null ? "sensorDataId=" + sensorDataId + ", "	: "")
				+ (state != null ? "state=" + state + ", " : "")
				+ ")";
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "NotificationVO ["
				+ (notificationId != null ? "notificationId=" + notificationId+ ", " : "")
				+ (alertDetails != null ? "alertDetails=" + alertDetails + ", "	: "")
				+ (notification != null ? "notification=" + notification + ", "	: "")
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (containerId != null ? "containerId=" + containerId + ", ": "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (ruleId != null ? "ruleId=" + ruleId + ", " : "")
				+ (status != null ? "status=" + status + ", " : "")
				+ (sensorDataId != null ? "sensorDataId=" + sensorDataId + ", "	: "")
				+ (state != null ? "state=" + state + ", " : "")
				+ (createdOn != null ? "createdOn=" + createdOn : "") + "]";
	}


}
