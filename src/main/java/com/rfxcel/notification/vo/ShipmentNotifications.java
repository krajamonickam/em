package com.rfxcel.notification.vo;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ShipmentNotifications {
	
	private String id;
	private String deviceId;
	private String packageId;
	private String productId;
	private ArrayList <NotificationVO> notifications;
	private Integer state; 	
	
	/**
	 * 
	 */
	public ShipmentNotifications() {
	}
	
	/**
	 * 
	 * @param id
	 * @param notifications
	 */
	public ShipmentNotifications(String id,ArrayList<NotificationVO> notifications,String productId, String packageId, String deviceId,  Integer state) {
		this.id = id;
		this.notifications = notifications;
		this.productId = productId;
		this.packageId = packageId;
		this.deviceId = deviceId;
		this.state = state;
	}
	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	/**
	 * @return the deviceId
	 */
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	/**
	 * @return the packageId
	 */
	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	
	
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the notifications
	 */
	@JsonProperty("notifications")
	public ArrayList<NotificationVO> getNotifications() {
		return notifications;
	}
	/**
	 * @param notifications the notifications to set
	 */
	public void setNotifications(ArrayList<NotificationVO> notifications) {
		this.notifications = notifications;
	}

	/**
	 * @return the state
	 */
	public Integer getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(Integer state) {
		this.state = state;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShipmentNotifications ["
				+ (id != null ? "id=" + id + ", " : "")
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (packageId != null ? "packageId=" + packageId + ", " : "")
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (notifications != null ? "notifications=" + notifications	: "")
				+ (state != null ? "state" + state + ", " : "")
				+ "]";
	}

}
