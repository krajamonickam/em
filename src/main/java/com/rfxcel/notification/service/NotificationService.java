package com.rfxcel.notification.service;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.rfxcel.notification.dao.NotificationDAO;
import com.rfxcel.notification.util.EmailTemplateUtil;
import com.rfxcel.notification.util.NotificationConstant;
import com.rfxcel.notification.vo.AlertDetailsVO;
import com.rfxcel.notification.vo.EmailDetailsVO;
import com.rfxcel.notification.vo.NotificationReqVO;
import com.rfxcel.notification.vo.NotificationResVO;
import com.rfxcel.notification.vo.NotificationVO;
import com.rfxcel.org.service.OrganizationService;
import com.rfxcel.rule.util.RuleConstants;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.beans.ExcursionLog;
import com.rfxcel.sensor.beans.Geofence;
import com.rfxcel.sensor.dao.ExcursionLogDAO;
import com.rfxcel.sensor.dao.SearchDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.service.StationaryAlertProcessor;
import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceAttributeLogCache;
import com.rfxcel.cache.DeviceCache;


/**
 * 
 * @author Tejshree Kachare
 * @since 4 Nov 2016
 *
 */
@Path("sensor")
public class NotificationService {

	private static Logger logger = Logger.getLogger(NotificationService.class);
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static DeviceAttributeLogCache deviceAttrLogCache = DeviceAttributeLogCache.getInstance();
	private static ConfigCache configCache = ConfigCache.getInstance();
	private static SendumDAO sendumDAO = SendumDAO.getInstance();
	private static AssociationCache assoCache = AssociationCache.getInstance();
	
	/**
	 * @param notificationVO
	 */
	public static void createNotification (NotificationVO notificationVO){
		try{
			Integer orgId = notificationVO.getAlertDetails().getOrgId();
			AlertDetailsVO alertDetails;
			boolean flag = configCache.getOrgConfig("new.profile.screen", orgId)== null ? false : Boolean.valueOf(configCache.getOrgConfig("new.profile.screen", orgId));
			if(flag){
				alertDetails = NotificationDAO.getProfileAttrAlertDetails(notificationVO);
			}else{
			
				alertDetails = NotificationDAO.getAlertDetails(notificationVO.getAlertDetails());
			}
			if(alertDetails != null){
				String detailMessage = alertDetails.getAlertDetailMsg();
				String correctiveAction = alertDetails.getCorrectiveAction();
				//Replace attributes with actual values in notification
				HashMap<String, String> attributeValues = alertDetails.getAttributeValueMap();
				for(Entry <String,String> entry: attributeValues.entrySet()){
					if(entry.getValue()!= null){
						detailMessage = detailMessage.replace(entry.getKey(), entry.getValue());
						if(correctiveAction != null){
							correctiveAction = correctiveAction.replace(entry.getKey(), entry.getValue());
						}
					}else{
						detailMessage = detailMessage.replace(entry.getKey(), "-");
						if(correctiveAction != null)
							correctiveAction = correctiveAction.replace(entry.getKey(), "-");
					}
				}
				alertDetails.setCorrectiveAction(correctiveAction);
				alertDetails.setAlertDetailMsg(detailMessage);
				notificationVO.setNotification(detailMessage);
				notificationVO.setAlertDetails(alertDetails);
				NotificationDAO.saveNotification(notificationVO);

				//Trigger email, based on whether notificationId is set in or not, it will add view details hyperlink for serialization-cold chain integration done for Astellas
				triggerEmail(notificationVO);
			}else{
				logger.info("Alert details are not configured for alert "+ notificationVO.getAlertDetails());
			}
		}catch(Exception e){
			logger.error("Exception while creating notification "+ e.getMessage());
		}
	}

	/**
	 * 
	 * @param notificationVO
	 */
	public static void triggerEmail(NotificationVO notificationVO){
		AlertDetailsVO alertDetails = notificationVO.getAlertDetails();
		//Get the email ids to which the mail is to be sent
		Integer orgId = alertDetails.getOrgId();
		String toAddresses;
		boolean flag = configCache.getOrgConfig("new.profile.screen", orgId)== null ? false : Boolean.valueOf(configCache.getOrgConfig("new.profile.screen", orgId));
		if(flag){
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(notificationVO.getDeviceId(), notificationVO.getContainerId());
			toAddresses = NotificationDAO.getEmailAddressesForAttrAlert(alertDetails.getAlertCode(), profileId);
		}else{
			toAddresses = NotificationDAO.getEmailAddressesForAnAlert(alertDetails.getAlertCode(),orgId) ;
		}
		StringBuilder content  = new StringBuilder();
		Long alertcode = alertDetails.getAlertCode();
		if((toAddresses != null) && (toAddresses.length() != 0)) {
			String applicationUrl = configCache.getSystemConfig("application.url");
			if(applicationUrl != null){
				StringBuilder url = new StringBuilder(applicationUrl);
				url.append("app/dashboard/alerts");
				url.append("?notificationId=").append(notificationVO.getNotificationId());
				url.append("&fromMail=YES&toPage=alert");
				content.append("<html lang=\"en\">");
				content.append(EmailTemplateUtil.getHeadStyleCSS());
				//Create template using EmailTemplateUtil class
				HashMap<Long, String> mapping = EmailTemplateUtil.getActionReqAlertMapping();
				String imageMapping;
				Integer state = notificationVO.getState();
				// If state is STATE_RED then action req template else normal template
				
				 if(state == NotificationVO.STATE_RED  && mapping.keySet().contains(alertcode)){
					imageMapping = mapping.get(alertcode);
					content.append( EmailTemplateUtil.getActionReqEmailTemplate(alertDetails.getAlertMsg(), alertDetails.getAlertDetailMsg(), alertDetails.getCorrectiveAction(), imageMapping, url.toString()));
				}
				else{
					imageMapping = EmailTemplateUtil.getResetAttrAlertMapping().get(alertDetails.getAttributeName());
					content.append(EmailTemplateUtil.getEmailNoActionTemplate(alertDetails.getAlertMsg(), alertDetails.getAlertDetailMsg(), imageMapping, url.toString()));
				}
				content.append("</html>");		
				StringBuilder subject = new StringBuilder();
				subject.append( notificationVO.getDeviceId()).append(" - ").append(notificationVO.getProductId()).append(" : ")
				.append(alertDetails.getAlertMsg());
				ArrayList<String> imageList = new ArrayList<String>();
				imageList.add(imageMapping);
				imageList.add("btnViewDetails");
				EmailDetailsVO emailDetails = new EmailDetailsVO(toAddresses, subject.toString(), content.toString(), "Daily Report",imageList);
				new EmailAlertService().triggerEmail(emailDetails);
				logger.info("Notification sent to " + toAddresses + " " + notificationVO.toLogFormat());
			}else{
				logger.info("Notification not sent, no application URL " + notificationVO.toLogFormat());
			}
		}else {
			logger.info("Notification not sent, no email address " + notificationVO.toLogFormat());
		}
	}
	
	/**
	 * 
	 * @param attributeDataMap
	 * @return
	 */
	public static ArrayList<String> processAttributeAlerts(Map<String, String> attributeDataMap) {
		String replaceString="";
		long  alertCode = 0 ;
		Boolean raiseNotification = false;
		ArrayList<String> attributes = new ArrayList<String>();
		try{
			String deviceId = attributeDataMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
			String sensorRecordId = attributeDataMap.get("sensorRecordId");
			String containerId = attributeDataMap.get("containerId");
			String productId = attributeDataMap.get("productId");
			Integer orgId = attributeDataMap.get("orgId") == null ? null : Integer.valueOf(attributeDataMap.get("orgId"));
			HashMap<String, String> attrValueMap = new HashMap<String, String>();
			if(attributeDataMap.get(NotificationConstant.ALARM_TYPE) != null){
				if(attributeDataMap.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_TEMPERATURE)){
					alertCode = RuleConstants.ATTRIBUTE_ALERT_MAP.get(RuleConstants.SENSOR_ATTR_TEMPERATURE);
					replaceString = attributeDataMap.get(NotificationConstant.ALARM_VALUE);
					raiseNotification = true;
					attributes.add(RuleConstants.SENSOR_ATTR_TEMPERATURE);
					attrValueMap.put("<<ATTR>>",replaceString );
				}else if(attributeDataMap.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_LIGHT)){
					alertCode = RuleConstants.ATTRIBUTE_ALERT_MAP.get(RuleConstants.SENSOR_ATTR_LIGHT);
					replaceString = attributeDataMap.get(NotificationConstant.ALARM_VALUE);
					raiseNotification = true;
					attributes.add(RuleConstants.SENSOR_ATTR_LIGHT);
					attrValueMap.put("<<ATTR>>",replaceString );
				}else if(attributeDataMap.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_LOW_BATTERY)){
					alertCode = RuleConstants.ATTRIBUTE_ALERT_MAP.get(RuleConstants.SENSOR_ATTR_BATTERY);
					replaceString = attributeDataMap.get(NotificationConstant.ALARM_VALUE);
					raiseNotification = true;
					attributes.add(RuleConstants.SENSOR_ATTR_BATTERY);
					attrValueMap.put("<<ATTR>>",replaceString );
				}else if(attributeDataMap.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_PRESSURE)){
					alertCode =  RuleConstants.ATTRIBUTE_ALERT_MAP.get(RuleConstants.SENSOR_ATTR_PRESSURE); 
					replaceString = attributeDataMap.get(NotificationConstant.ALARM_VALUE);
					raiseNotification = true;
					attributes.add(RuleConstants.SENSOR_ATTR_PRESSURE);
					attrValueMap.put("<<ATTR>>",replaceString );
				}else if(attributeDataMap.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_HUMIDITY)){
					alertCode = RuleConstants.ATTRIBUTE_ALERT_MAP.get(RuleConstants.SENSOR_ATTR_HUMIDITY); 
					replaceString = attributeDataMap.get(NotificationConstant.ALARM_VALUE);
					raiseNotification = true;
					attributes.add(RuleConstants.SENSOR_ATTR_HUMIDITY);
					attrValueMap.put("<<ATTR>>",replaceString );
				}else if(attributeDataMap.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_SHOCK)){
					alertCode =  RuleConstants.ATTRIBUTE_ALERT_MAP.get(RuleConstants.SENSOR_ATTR_SHOCK);
					replaceString = attributeDataMap.get(NotificationConstant.ALARM_VALUE);
					raiseNotification = true;
					attributes.add(RuleConstants.SENSOR_ATTR_SHOCK);
					attrValueMap.put("<<ATTR>>",replaceString );
				}

				if(raiseNotification == true){

					AlertDetailsVO alertDetailsVO = new AlertDetailsVO(alertCode, attrValueMap, orgId);
					NotificationVO notVO = new NotificationVO(deviceId,containerId,productId, null, Long.valueOf(sensorRecordId), alertDetailsVO);
					String timeLong = attributeDataMap.get(RuleConstants.SENSOR_ATTR_TIME);
					if (timeLong != null) {
						notVO.setCreatedDate(new Date(Long.parseLong(timeLong)));
					}
					notVO.setState(NotificationVO.STATE_GREEN);
					NotificationService.createNotification(notVO);
				}
			}
		}catch(Exception e){
			logger.error("Error while processing alert to raise Notification  " + e.getMessage());
		}
		return attributes;
	}

	/**
	 * @param attributeDataMap
	 */
	public static void processSameLocationAlert(Map<String, String> attributeDataMap) {
		String replaceString="";
		long  alertCode = 0 ;
		HashMap<String, String> attrValueMap ;
		try{
			String deviceId = attributeDataMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
			String sensorRecordId = attributeDataMap.get("sensorRecordId");
			String containerId = attributeDataMap.get("containerId");	
			String productId = attributeDataMap.get("productId");
			Integer orgId = attributeDataMap.get("orgId") == null ? null : Integer.valueOf(attributeDataMap.get("orgId"));
			alertCode = NotificationConstant.ALERT_CODE_WITHOUT_MOTION;
			replaceString= attributeDataMap.get(NotificationConstant.TIME_DURATION);
			attrValueMap = new HashMap<String, String>();
			attrValueMap.put("<<ATTR>>",replaceString );
			AlertDetailsVO alertDetailsVO = new AlertDetailsVO(alertCode, attrValueMap, orgId);
			NotificationVO notVO = new NotificationVO(deviceId, containerId, productId, null, Long.valueOf(sensorRecordId), alertDetailsVO);
			String timeLong = attributeDataMap.get(RuleConstants.SENSOR_ATTR_TIME);
			if (timeLong != null) {
				notVO.setCreatedDate(new Date(Long.parseLong(timeLong)));
			}
			notVO.setState(NotificationVO.STATE_GREEN);
			NotificationService.createNotification(notVO);
			String industry4DemoString = ConfigCache.getInstance().getOrgConfig("system.industry4demo", orgId);
			boolean industry4Demo = (industry4DemoString == null) ? false: new Boolean(industry4DemoString).booleanValue();
			if (industry4Demo) {
				StationaryAlertProcessor processor = new StationaryAlertProcessor(orgId, productId, containerId, deviceId, alertCode);
			}
		}catch(Exception e){
			logger.error("Error while processing alert to for derived attributes: " + e.getMessage());
		}
	}

	@POST
	@Path("/getNotifications")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getNotifications(@Context HttpServletRequest httpRequest, NotificationReqVO request) {
		NotificationResVO res = new NotificationResVO();
		try {
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){				
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getSortBy()==null) {
				request.setSortBy(NotificationConstant.NOTIFICATION_SORTBY_AGE);
			}
			if (request.getSearchType()==null) {
				request.setSearchType(NotificationConstant.NOTIFICATION_SEARCHTYPE_ALL);
			}
			if (request.getStart()==null) {
				request.setStart(0); //if default page no is null then setting it to 1
			}
			if (request.getLimit()==null) {
				request.setLimit(NotificationConstant.NOTIFICATION_PAGESIZE);//if default page size is null then setting it to 100
			}
			if (request.getSearchType().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				if (request.getDeviceData()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("DeviceData is null")).build();
				}
				if (request.getDeviceData().getDeviceId()==null || request.getDeviceData().getPackageId()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("deviceId or packageId is null")).build();
				}
			}
			String user = TokenManager.getInstance().getUserForToken(httpRequest);
			request.setUser(user);
			res = NotificationDAO.getInstance().getNotifications(request);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getNotifications "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getNotifications "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getNotifications.  Request :: "+request +" Response :: "+res );
		}
	}

	@POST
	@Path("/getShipmentNotifications")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getShipmentNotifications(@Context HttpServletRequest httpRequest, NotificationReqVO request) {
		NotificationResVO res = new NotificationResVO();
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			if (request.getSortBy()==null) {
				request.setSortBy(NotificationConstant.NOTIFICATION_SORTBY_AGE);
			}
			if (request.getSearchType()==null) {
				request.setSearchType(NotificationConstant.NOTIFICATION_SEARCHTYPE_ALL);
			}
			/*	if (request.getStart()==null) {
				request.setStart(0); //if default page no is null then setting it to 1
			}
			if (request.getLimit()==null) {
				request.setLimit(NotificationConstant.NOTIFICATION_PAGESIZE);//if default page size is null then setting it to 100
			}*/
			if (request.getSearchType().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				if (request.getDeviceData()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("DeviceData is null")).build();
				}
				if (request.getDeviceData().getDeviceId()==null || request.getDeviceData().getPackageId()==null ||  request.getDeviceData().getProductId()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("deviceId, packageId or productId is null")).build();
				}
			}
			String user = TokenManager.getInstance().getUserForToken(httpRequest);
			request.setUser(user);
			
			//check the Elastic search fetch is true or false
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			
			// Temporarily adding alertDeviceLogSearch parameter to search alert from elastic search. Please remove this flag after elastic search running perfect.
			String alertDevicelogString = ConfigCache.getInstance().getSystemConfig("alert.devicelog.search.enable");
			Boolean alertDeviceLogSearch = false;
			if(alertDevicelogString != null){
				alertDeviceLogSearch = Boolean.valueOf(alertDevicelogString);
			}
			// end
			
			String responseMsg = "";
			//if fetchESData is true then it will search from elastic search else it will fetch from MySQL
			if(fetchESData || alertDeviceLogSearch){				
				res = SearchDAO.getInstance().getNotifications(request);
				if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
					responseMsg = res.getResponseMessage();
					res.setResponseMessage(responseMsg);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}				
			}else{
				res = NotificationDAO.getInstance().getShipmentNotifications(request);
			}
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getShipmentNotifications "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getShipmentNotifications "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getShipmentNotifications.  Request :: "+request +" Response :: "+res );
		}

	}


	@POST
	@Path("/getNotificationCount")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getNotificationCount(@Context HttpServletRequest httpRequest, NotificationReqVO request) {
		NotificationResVO res = new NotificationResVO();
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if(orgId!=null && orgId!=0){
				com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
				if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
				}
			}
			if (request.getSearchType()==null) {
				request.setSearchType(NotificationConstant.NOTIFICATION_SEARCHTYPE_ALL);
			}
			if (request.getSearchType().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				if (request.getDeviceData()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("DeviceData is null")).build();
				}
				if (request.getDeviceData().getDeviceId()==null || request.getDeviceData().getPackageId()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("deviceId or packageId is null")).build();
				}
			}
			
			//check the Elastic search fetch is true or false
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			String responseMsg = "";
			//if fetchESData is true then it will search from elastic search else it will fetch from MySQL
			if(fetchESData){				
				res = SearchDAO.getInstance().getNotificationsCount(request);
				if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
					responseMsg = res.getResponseMessage();
					res.setResponseMessage(responseMsg);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}				
			}else{
				res = NotificationDAO.getInstance().getNotificationsCount(request);
			}			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getNotificationsCount "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getNotificationsCount "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getNotificationsCount.  Request :: "+request +" Response :: "+res );
		}

	}

	/**
	 * 
	 * @param attributeDataMap
	 */
	public static String processGeofenceAlerts(Map<String, String> attributeDataMap){
		String replaceString="";
		long  alertCode = 0 ;
		Boolean raiseNotification = false;
		String attrName = null;
		try{
			String deviceId = attributeDataMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
			String sensorRecordId = attributeDataMap.get("sensorRecordId");
			String containerId = attributeDataMap.get("containerId");
			String productId = attributeDataMap.get("productId");
			Integer orgId = attributeDataMap.get("orgId") == null ? null : Integer.valueOf(attributeDataMap.get("orgId"));
			HashMap<String, String> attrValueMap = new HashMap<String, String>();
			if(attributeDataMap.get(NotificationConstant.GEO_FENCE_NUMBER) != null && attributeDataMap.containsKey(NotificationConstant.GEO_FENCE_NUMBER))
			{
				String geofenceEvent = attributeDataMap.get(NotificationConstant.GEO_FENCE_EVENT);
				if(geofenceEvent != null && (geofenceEvent.equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY) || geofenceEvent.equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_EXIT) )){
					if(geofenceEvent.equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY)){
						alertCode = NotificationConstant.ALERT_CODE_GEO_POINT_ENTRY;
					}else {
						alertCode = NotificationConstant.ALERT_CODE_GEO_POINT_EXIT;
					}
					String geofenceNumber = attributeDataMap.get(NotificationConstant.GEO_FENCE_NUMBER);
					Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, containerId);
					Geofence geofence = DeviceCache.getInstance().getGeofence(profileId, geofenceNumber) ;	
					if(geofence != null){
						attrValueMap.put("<<ATTR>>",geofence.getPointName());
						attrValueMap.put("<<ATTR1>>",geofence.getLatitude().toString());
						attrValueMap.put("<<ATTR2>>",geofence.getLongitude().toString());
					}
					else{
						attrValueMap.put("<<ATTR>>","-");
						attrValueMap.put("<<ATTR1>>","-");
						attrValueMap.put("<<ATTR2>>","-");
					}
					raiseNotification = true;
				}
			}else if(attributeDataMap.get(NotificationConstant.GEO_FENCE_NAME) != null && attributeDataMap.containsKey(NotificationConstant.GEO_FENCE_NAME)){
				String geofenceName = attributeDataMap.get(NotificationConstant.GEO_FENCE_NAME);
				if(attributeDataMap.get(NotificationConstant.GEO_FENCE_EVENT).equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY)){
					DeviceInfo deviceInfo = deviceAttrLogCache.getNotificationLimitLog(deviceId, RuleConstants.SENSOR_ATTR_GEOFENCE+"#"+geofenceName);
					if (deviceInfo == null){
						alertCode = NotificationConstant.ALERT_CODE_SHIPMENT_ON_ROUTE;
					}else{
					alertCode = NotificationConstant.ALERT_CODE_ROUTE_RESUMED;
					}
					replaceString = "("+attributeDataMap.get(NotificationConstant.GEO_FENCE_LATITUDE) +"," + attributeDataMap.get(NotificationConstant.GEO_FENCE_LONGITUDE)  +")" ;
					raiseNotification = true;
					if(attributeDataMap.containsKey(NotificationConstant.GEO_FENCE_ADDRESS) && !attributeDataMap.get(NotificationConstant.GEO_FENCE_ADDRESS).isEmpty()) {
						String address = attributeDataMap.get(NotificationConstant.GEO_FENCE_ADDRESS);
						replaceString = replaceString + ",(" + address + ")"; 
					}
					attrValueMap.put("<<ATTR>>",replaceString );
					cleanupAfterGeofenceDeviation(deviceId, containerId, orgId, RuleConstants.SENSOR_ATTR_GEOFENCE+"#"+geofenceName);
				}else if(attributeDataMap.get(NotificationConstant.GEO_FENCE_EVENT).equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_EXIT)){
					alertCode = NotificationConstant.ALERT_CODE_ROUTE_DEVIATION;	
					replaceString ="("+attributeDataMap.get(NotificationConstant.GEO_FENCE_LATITUDE) +"," + attributeDataMap.get(NotificationConstant.GEO_FENCE_LONGITUDE)  +")" ;
					raiseNotification = true;
					if(attributeDataMap.containsKey(NotificationConstant.GEO_FENCE_ADDRESS) && !attributeDataMap.get(NotificationConstant.GEO_FENCE_ADDRESS).isEmpty()) {
						String address = attributeDataMap.get(NotificationConstant.GEO_FENCE_ADDRESS);
						replaceString = replaceString + ",(" + address + ")"; 
					}
					attrValueMap.put("<<ATTR>>",replaceString );
					attrName = RuleConstants.SENSOR_ATTR_GEOFENCE;
					processGeofenceDeviation(deviceId, containerId, geofenceName, attrName+"#"+geofenceName);
				}
			}if(raiseNotification == true){

				AlertDetailsVO alertDetailsVO = new AlertDetailsVO(alertCode, attrValueMap, orgId);
				NotificationVO notVO = new NotificationVO(deviceId,containerId,productId, null, Long.valueOf(sensorRecordId), alertDetailsVO);
				notVO.setState(NotificationVO.STATE_GREEN);
				NotificationService.createNotification(notVO);
			}
		}catch(Exception e){
			logger.error("Error while processing alert to raise Notification  " + e.getMessage());
		}
		return attrName;
	}
	
	
	/**
	 * 
	 * @param deviceId
	 */
	public static void cleanupAfterGeofenceDeviation(String deviceId, String containerId, Integer orgId, String attrName){
		DeviceInfo deviceInfo = deviceAttrLogCache.getNotificationLimitLog(deviceId, attrName);
		if(deviceInfo != null){
			deviceInfo = new DeviceInfo();
			deviceInfo.setDeviceId(deviceId);
			deviceInfo.setDeviceLogType(1);
			deviceAttrLogCache.removeNotificationLimitLog(deviceId, attrName);
			sendumDAO.removeDeviceLog(deviceInfo,attrName);
		}
		// clear entry related to that attribute from sensor_attribute_log table
		ExcursionLog excursionLog = new ExcursionLog(deviceId, containerId, attrName, orgId);
		ExcursionLogDAO.getInstance().removeExcursionLog(excursionLog);
		assoCache.removeShipmentExcursionAttr(deviceId, containerId, attrName);
	}
	
	
	/**
	 * @param deviceId
	 */
	public static void processGeofenceDeviation(String deviceId, String containerId, String geofenceName, String attrName){
		DeviceInfo deviceInfo = deviceAttrLogCache.getNotificationLimitLog(deviceId, attrName);
			if(deviceInfo == null ) {
				deviceInfo = new DeviceInfo();
				deviceInfo.setDeviceId(deviceId);
				deviceInfo.setAttributeName(attrName);
				deviceInfo.setAttributeValue(geofenceName);
				deviceInfo.setStartTime(new DateTime());
				deviceInfo.setEndTime(new DateTime());
				deviceInfo.setDeviceLogType(1);
				deviceInfo.setNotifyCount(1);			// -- setting the notificationCount to 1
				
				sendumDAO.addDeviceLog(deviceInfo);
				deviceAttrLogCache.putNotificationLimitLog(deviceId, attrName, deviceInfo);
				assoCache.addShipmentExcursionAttr(deviceId, containerId, attrName);
				
			}else{
				deviceInfo.setEndTime(new DateTime());
				deviceInfo.setNotifyCount(deviceInfo.getNotifyCount() + 1);
				deviceAttrLogCache.putNotificationLimitLog(deviceId, attrName, deviceInfo);	// updating cache with new notify count
				sendumDAO.updateDeviceLog(deviceInfo);
			}
	}
	/**
	 * 
	 * @param attributeDataMap
	 */
	public static void processGeoPointRangeAlert(Map<String, String> attributeDataMap) {
		String deviceId = attributeDataMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
		String sensorRecordId = attributeDataMap.get("sensorRecordId");
		String containerId = attributeDataMap.get("containerId");
		String productId = attributeDataMap.get("productId");
		String latitude =  attributeDataMap.get(NotificationConstant.GEO_FENCE_LATITUDE);
		String longitude = attributeDataMap.get(NotificationConstant.GEO_FENCE_LONGITUDE);
		Integer orgId = attributeDataMap.get("orgId") == null ? null : Integer.valueOf(attributeDataMap.get("orgId"));
		String geofenceEvent = attributeDataMap.get(NotificationConstant.GEO_FENCE_EVENT);
		if(geofenceEvent!= null && (geofenceEvent.equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY) || geofenceEvent.equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_EXIT) )){
			long alertCode;
			if(geofenceEvent.equalsIgnoreCase(NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY)){
				alertCode = NotificationConstant.ALERT_CODE_GEO_POINT_ENTRY;
			}else {
				alertCode = NotificationConstant.ALERT_CODE_GEO_POINT_EXIT;
			}
			String geofenceNumber = attributeDataMap.get(NotificationConstant.GEO_FENCE_NUMBER);
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, containerId);
			Geofence geofence = DeviceCache.getInstance().getGeofence(profileId, geofenceNumber) ;	
			HashMap<String, String> attrValueMap = new HashMap<String, String>();
			if(geofence != null){
				attrValueMap.put("<<ATTR>>",geofence.getPointName());
			}
			else{
				attrValueMap.put("<<ATTR>>","-");
			}
			if (latitude != null) {
				attrValueMap.put("<<ATTR1>>", latitude);
			}
			else {
				attrValueMap.put("<<ATTR1>>","-");
			}
			if (longitude != null) {
				attrValueMap.put("<<ATTR2>>", longitude);
			}
			else {
				attrValueMap.put("<<ATTR2>>","-");
			}
			AlertDetailsVO alertDetailsVO = new AlertDetailsVO(alertCode, attrValueMap, orgId);
			NotificationVO notVO = new NotificationVO(deviceId, containerId, productId, null, Long.valueOf(sensorRecordId), alertDetailsVO);
			String timeLong = attributeDataMap.get(RuleConstants.SENSOR_ATTR_TIME);
			if (timeLong != null) {
				notVO.setCreatedDate(new Date(Long.parseLong(timeLong)));
			}
			notVO.setState(NotificationVO.STATE_GREEN);
			NotificationService.createNotification(notVO);
		}
	}
	
}
