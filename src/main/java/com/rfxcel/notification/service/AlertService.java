package com.rfxcel.notification.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.notification.dao.AlertDAO;
import com.rfxcel.notification.entity.Alert;
import com.rfxcel.notification.vo.AlertRequest;
import com.rfxcel.notification.vo.AlertResponse;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;


/**
 * 
 * @author sachin_kohale
 * @since Apr 20, 2017
 */
@Path("alert")
public class AlertService {
	private static Logger logger = Logger.getLogger(AlertService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static AlertDAO alertDAO = AlertDAO.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	
	
	@POST
	@Path("getAlertList")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAlertList(@Context HttpServletRequest httpRequest, AlertRequest request){
		AlertResponse response = new AlertResponse(simpleDateFormat.format(new Date()));	
		try{
			List<Alert> alertList = null;
			//check if request is not null
			if(request == null || request.getAlert() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert details cannot be null.")).build();
			}
			Alert alert = request.getAlert();
			Integer orgId = alert.getOrgId();
			//check if org id is not null
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			alertList = alertDAO.getAlertList(orgId);
			response.setAlertList(alertList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
						
		}catch(Exception e){
			logger.error("Exception in AlertService.getAlertList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.getAlertList  Request :: "+request +" Response :: "+response );
		}
	}
	
	@POST
	@Path("addAlert")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addAlert(@Context HttpServletRequest httpRequest, AlertRequest request){
		AlertResponse response = new AlertResponse(simpleDateFormat.format(new Date()));	
		try{
			//Check if request is not empty
			if(request == null || request.getAlert() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert details cannot be null")).build();
			}
			Alert alert = request.getAlert();				
			Long alertCode = alert.getAlertCode();
			Integer orgId = alert.getOrgId();
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			String alertMessage = alert.getAlertMsg();
			String alertDetailsMsg = alert.getAlertDetailMsg();
			//check orgid, alertCode is not null
			if( alertCode == null ){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("alerCode cannot be null.")).build();
			}
			
			//check alertMessage and alertDetailsMsg is not empty
			if(alertMessage == null || alertMessage.trim().length() == 0 || alertDetailsMsg == null || alertDetailsMsg.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("alertMessage or alertDetailsMsg cannot be null or empty.")).build();
			}
			
			//check if alert code is present for requested organization
			Boolean present = alertDAO.checkIfAlertCodeIsPresent(alertCode, orgId);
			if(present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert for the selected attribute is already present in the system for this organization.")).build();
			}
			
			alertDAO.addAlert(alert);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has added alert "+alertMessage;
			AuditLog auditLog = new AuditLog(userId, userName, null, "Alert", "Create", null, alert.toJson(),logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Alert added successfully")).build();
		}catch(Exception e){
			logger.error("Exception in AlertService.addAlert "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.addAlert  Request :: "+request +" Response :: "+response );
		}
	}
	
	
	
	@POST
	@Path("modifyAlert")
	@Consumes("application/json")
	@Produces("application/json")
	public Response modifyAlert(@Context HttpServletRequest httpRequest, AlertRequest request){
		AlertResponse response = new AlertResponse(simpleDateFormat.format(new Date()));	
		try{
			//Check if request is not empty
			if(request == null || request.getAlert() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert details cannot be null")).build();
			}
			
			//check if alertId, alertGroupId is not null
			Alert alert = request.getAlert();
			Integer orgId = alert.getOrgId();
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			Long alertId = alert.getAlertId();
			ArrayList<Long> alertGroupIds = alert.getAlertGroupIds();
			String alertMessage = alert.getAlertMsg();
			String alertDetailsMsg = alert.getAlertDetailMsg();
			if(alertId == null || alertGroupIds == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("alertId or alertGroupIds cannot be null.")).build();
			}
			
			//check alertMessage and alertDetailsMsg is not empty			
			if(alertMessage == null || alertMessage.trim().length() == 0 || alertDetailsMsg == null || alertDetailsMsg.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("alertMessage or alertDetailsMsg cannot be null or empty.")).build();
			}
			
			//check if alert is present
			Boolean present = alertDAO.checkIfAlertIsPresent(alertId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert is not configured or is already deleted.")).build();
			}
			Alert oldAlert = alertDAO.getAlertById(alertId);
			alertDAO.updateAlert(alert);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has updated alert "+oldAlert.getAlertMsg();
			AuditLog auditLog = new AuditLog(userId, userName, alertId, "Alert", "Update", oldAlert.toJson(), alert.toJson(),logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Alert modified successfully")).build();
		}catch(Exception e){
			logger.error("Exception in AlertService.modifyAlert "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.modifyAlert  Request :: "+request +" Response :: "+response );
		}
	}
	
	@POST
	@Path("deleteAlert")
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteAlert(@Context HttpServletRequest httpRequest, AlertRequest request){
		AlertResponse response = new AlertResponse(simpleDateFormat.format(new Date()));	
		try{
			//Check if request is not empty
			if(request == null || request.getAlert() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert details cannot be null")).build();
			}
			Alert alert = request.getAlert();
			Integer orgId = alert.getOrgId();
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			Long alertId = alert.getAlertId();
			//check if alert is not null
			if(alertId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("alertId cannot be null.")).build();
			}
			
			//check if alert is present
			Boolean present = alertDAO.checkIfAlertIsPresent(alertId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert is not configured or is already deleted.")).build();
			}
			Alert oldAlert = alertDAO.getAlertById(alertId);
			alertDAO.deleteAlert(alertId);
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has deleted alert "+oldAlert.getAlertMsg();
			AuditLog auditLog = new AuditLog(userId, userName, alertId, "Alert", "Delete", oldAlert.toJson(), null,logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Alert deleted successfully")).build();
		}catch(Exception e){
			logger.error("Exception in AlertService.deleteAlert "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.deleteAlert  Request :: "+request +" Response :: "+response );
		}
		
	}
	
	@POST
	@Path("getAlertDetailsByAttrName")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAlertDetailsByAttrName(@Context HttpServletRequest httpRequest, AlertRequest request){
		AlertResponse response = new AlertResponse(simpleDateFormat.format(new Date()));	
		try{
			//check if request is not null
			if(request == null || request.getAlert() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert details cannot be null.")).build();
			}
			
			Alert alert = request.getAlert();
			Integer orgId = alert.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			String attributeName = alert.getAttrName();
			
			
			if(attributeName == null || attributeName.trim().length() ==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("attrName cannot be null or empty.")).build();
			}
			
			Alert alertDetails = alertDAO.getAlertDetailsByAttrName(attributeName);
			response.setAlert(alertDetails);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
						
		}catch(Exception e){
			logger.error("Exception in AlertService.getAlertDetailsByAttrName "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.getAlertDetailsByAttrName  Request :: "+request +" Response :: "+response );
		}
	}
	
	@POST
	@Path("getDefaultAlertList")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getDefaultAlertList(@Context HttpServletRequest httpRequest, AlertRequest request){
		AlertResponse response = new AlertResponse(simpleDateFormat.format(new Date()));	
		try{
			List<Alert> alertList = null;
			//check if request is not null
			if(request == null || request.getAlert() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert details cannot be null.")).build();
			}
			Alert alert = request.getAlert();
			Integer orgId = alert.getOrgId();
			//check if org id is not null
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			alertList = alertDAO.getDefaultAlertList();
			response.setAlertList(alertList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
						
		}catch(Exception e){
			logger.error("Exception in AlertService.getDefaultAlertList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.getDefaultAlertList  Request :: "+request +" Response :: "+response );
		}
	}
}
