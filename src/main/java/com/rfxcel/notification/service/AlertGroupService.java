package com.rfxcel.notification.service;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.notification.dao.AlertGroupDAO;
import com.rfxcel.notification.entity.AlertGroup;
import com.rfxcel.notification.vo.AlertGroupRequest;
import com.rfxcel.notification.vo.AlertGroupResponse;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;


/**
 * 
 * @author Albeena
 * @Since Apr 21, 2017
 */
@Path("alertGroup")
public class AlertGroupService {

	private static Logger logger = Logger.getLogger(AlertGroupService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static AlertGroupDAO alertGroupDao = AlertGroupDAO.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	
	
	@Path("addAlertGroup")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response addAlertGroup(@Context HttpServletRequest httpRequest,AlertGroupRequest request){
		AlertGroupResponse response = new AlertGroupResponse(simpleDateFormat.format(new Date()));
		try {
			
			//Check if request is not empty
			if(request == null || request.getAlertGroup() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group information cannot be null.")).build();
			}
			AlertGroup alertGroup = request.getAlertGroup();
			Long orgId = alertGroup.getOrgId();
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId.intValue());
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			Boolean present = alertGroupDao.checkIfAlertGroupIsPresent(alertGroup);
			if(present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group "+alertGroup.getGroupName() +" is already present in system for the organization.")).build();
			}
			Long id = alertGroupDao.addAlertGroup(alertGroup);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has added alert group "+alertGroup.getGroupName();
			AuditLog auditLog = new AuditLog(userId, userName, id, "AlertGroup", "Create", null, alertGroup.toJson(),logMessage, orgId.intValue());
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Alert Group created successfully")).build();

		}catch (Exception e) {
			logger.error("Exception in AlertGroupService.addAlertGroup "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertGroupService.addAlertGroup Request :: "+request +" Response :: "+response );
		}
	}
	
	
	
	@Path("deleteAlertGroup")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteAlertGroup(@Context HttpServletRequest httpRequest,AlertGroupRequest request){
		AlertGroupResponse response = new AlertGroupResponse(simpleDateFormat.format(new Date()));
		try {
			if(request == null || request.getAlertGroup() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(	response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group Id cannot be null.")).build();
			}
			AlertGroup alertGroup = request.getAlertGroup();
			Long orgId = alertGroup.getOrgId();
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId.intValue());
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			Long alertGroupId = alertGroup.getId();
			alertGroup = alertGroupDao.getAlertGroupById(alertGroupId);
			alertGroupDao.deleteAlertGroup(alertGroupId);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has deleted alert group "+alertGroup.getGroupName();
			AuditLog auditLog = new AuditLog(userId, userName, alertGroupId, "AlertGroup", "Delete", alertGroup.toJson(), null,logMessage, orgId.intValue());
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(	response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Alert Group deleted successfully")).build();

		}catch (Exception e) {
			logger.error("Exception in AlertGroupService.deleteAlertGroup "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertGroupService.deleteAlertGroup Request :: "+request +" Response :: "+response );
		}
	}

	
	@Path("getAlertGroupList")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAlertGroupList(@Context HttpServletRequest httpRequest, AlertGroupRequest request){
		AlertGroupResponse response = new AlertGroupResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getAlertGroup() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group information cannot be null.")).build();
			}
			AlertGroup alertGroup = request.getAlertGroup();
			Long orgId = alertGroup.getOrgId();
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId.intValue());
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			List<AlertGroup> alertGroupList = null;
			alertGroupList = alertGroupDao.getAlertGroupList(alertGroup);
			response.setAlertGroupList(alertGroupList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
			
		}catch(Exception e){
			logger.error("Exception in AlertGroupService.getAlertGroupList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertGroupService.getAlertGroupList Request :: "+request +" Response :: "+response );
		}		
	}
	
	@Path("updateAlertGroup")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateAlertGroup(@Context HttpServletRequest httpRequest, AlertGroupRequest request){
		AlertGroupResponse response = new AlertGroupResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getAlertGroup() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group information cannot be null.")).build();
			}
			AlertGroup alertGroup = request.getAlertGroup();
			Long orgId = alertGroup.getOrgId();
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId.intValue());
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			Long alertGroupId = alertGroup.getId();
			if(alertGroupId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group id cannot be null.")).build();
			}
			Boolean present = alertGroupDao.checkIfActiveAlertGroupIsPresent(alertGroup);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group is not present in the system or already deleted.")).build();
			}
			AlertGroup oldAlertGroup = alertGroupDao.getAlertGroupById(alertGroupId);
			alertGroupDao.updateAlertGroup(alertGroup);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has updated alert group "+oldAlertGroup.getGroupName();
			AuditLog auditLog = new AuditLog(userId, userName, alertGroupId, "AlertGroup", "Update", oldAlertGroup.toJson(), alertGroup.toJson(),logMessage, orgId.intValue());
			auditLogDao.addAuditLog(auditLog);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Alert Group updated successfully")).build();
		
		}catch(Exception e){
			logger.error("Exception in AlertGroupService.updateAlertGroup "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertGroupService.updateAlertGroup Request :: "+request +" Response :: "+response );
		}
	}


	/**
	 * 
	 * @param orgId
	 *//*
	public static void deactivateAlertGroupsForOrg(Integer orgId) {
		try{
			alertGroupDao.deactivateAlertGroupsForOrg(orgId);
		
		}catch(Exception e){
			logger.error("Exception in AlertGroupService.deactivateAlertGroupsForOrg "+ e.getMessage());
			e.printStackTrace();
		}finally{
			diagnosticService.write("API :: AlertGroupService.deactivateAlertGroupsForOrg " );
		}
	}*/
}
