package com.rfxcel.notification.service;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;

import javax.activation.DataHandler;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.notification.util.NotificationConstant;
import com.rfxcel.notification.vo.EmailDetailsVO;


/**
 * 
 * @author Tejshree Kachare
 * @Since 10 Nov 2016
 */

public class EmailAlertService implements Runnable{

	private Thread thread = null;
	private static Logger logger = Logger.getLogger(EmailAlertService.class);
	private EmailDetailsVO emailDetails;
	private static final String trStart ="<tr>";
	private static final String trEnd ="</tr>";
	private static final String tdStart ="<td>";
	private static final String tdEnd ="</td>";
	private static final String trEmpty ="<tr><td></td></tr>";
	
	private static final ConfigCache configCache = ConfigCache.getInstance();
	public EmailAlertService(){
	}
	
	public void triggerEmail(EmailDetailsVO emailDetails){
		this.emailDetails = emailDetails;
		this.thread = new Thread(this);
		thread.start();
	}
	
	@Override
	public void run() {
		sendMail();
	}
	
	/**
	 * 
	 * @param toAddresses
	 * @param subject
	 * @param messageBody
	 * @param title
	 */
	public void sendMail(){
		try{

			String fromAddress = configCache.getSystemConfig(NotificationConstant.EMAIL_FROM_ADDRESS);
			Session session = getSession();

			//Compose the message  
			MimeMessage message = new MimeMessage(session);  
			message.setFrom(new InternetAddress(fromAddress)); 

			//Parse the comma separated values for toAddress
			message.addRecipients(Message.RecipientType.TO,  InternetAddress.parse(this.emailDetails.getToAddresses())); 
			message.setSubject(this.emailDetails.getSubject());  

			Multipart multiPart = new MimeMultipart();			
			MimeBodyPart mimeTextBodyPart = new MimeBodyPart();				

			message.setSentDate(new Date());
			mimeTextBodyPart.setContent(this.emailDetails.getMessage(), "text/html");
			multiPart.addBodyPart(mimeTextBodyPart);
			if(this.emailDetails.getImageMapping()!= null){
				addInlineImages(multiPart, this.emailDetails.getImageMapping());
			}
			message.setContent(multiPart);

			Transport.send(message);
			logger.info("Notification email sent successfully to " + 
						this.emailDetails.getToAddresses() + 
						" subject " + this.emailDetails.getSubject());
		}
		catch(AddressException ae ){
			logger.error("Address Exception while sending emails: " + ae.getMessage());
		}catch( MessagingException me){
			logger.error("Message Exception while sending emails: " + me.getMessage());
		}catch (MalformedURLException mue){
			logger.error("Malformed URL Exception while sending emails: " + mue.getMessage());
		} catch (Exception e) {
			logger.error("Exception while sending emails: " + e.getMessage());
		}
	}
	
	private static void addInlineImages(Multipart multiPart, ArrayList<String> arrayList) throws Exception{
		
		Map<String, URL> imageList = loadImages(arrayList);
        if (imageList != null && imageList.size() > 0) {
            Set<String> setImageID = imageList.keySet();
            for (String contentId : setImageID) {
                MimeBodyPart imagePart = new MimeBodyPart();
                imagePart.setHeader("Content-ID", "<" + contentId + ">");
                imagePart.setDisposition(MimeBodyPart.INLINE);
                imagePart.setFileName(contentId);
                URL url = imageList.get(contentId);
                try {
                	imagePart.setDataHandler(new DataHandler(url));
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
                multiPart.addBodyPart(imagePart);
            }
        }
	}
        
	/**
	 * <p> Load images required for an email to be triggered
	 * @param imageList
	 * @return
	 * @throws Exception
	 */
	private static Map<String, URL> loadImages(ArrayList<String> imageList) throws Exception{
		ClassLoader classLoader = EmailAlertService.class.getClassLoader();
		Map<String, URL> inlineImages = new HashMap<String, URL>();
		inlineImages.put("rfxLogo", new URL(classLoader.getResource("images/rfxcel_EM_1.png").toString()));
		if(imageList != null){
			for(String image: imageList){
				if(image!= null && image.trim().length() !=0){
					switch(image){
					case "btnConfirm":
						inlineImages.put("btnConfirm", new URL(classLoader.getResource("images/confirm-account-button@2x.png").toString()));
						break;
					case "btnReset":
						inlineImages.put("btnReset", new URL(classLoader.getResource("images/reset-account-button@2x.png").toString()));
						break;
					case "btnViewDetails":
						inlineImages.put("btnViewDetails", new URL(classLoader.getResource("images/view-details-button@2x.png").toString()));
						break;
					case "tempExcursion":
						inlineImages.put("tempExcursion", new URL(classLoader.getResource("images/red-thermo@2x.png").toString()));
						break;
					case "lightExcursion":
						inlineImages.put("lightExcursion", new URL(classLoader.getResource("images/red-light@2x.png").toString()));
						break;
					case "tiltExcursion":
						inlineImages.put("tiltExcursion", new URL(classLoader.getResource("images/red-tilt@2x.png").toString()));
						break;
					case "pressureExcursion":
						inlineImages.put("pressureExcursion", new URL(classLoader.getResource("images/red-pressure@2x.png").toString()));
						break;
					case "humidityExcursion":
						inlineImages.put("humidityExcursion", new URL(classLoader.getResource("images/red-humidity@2x.png").toString()));
						break;
					case "shockExcursion":
						inlineImages.put("shockExcursion", new URL(classLoader.getResource("images/red-shock@2x.png").toString()));
						break;
					case "batteryExcursion":
						inlineImages.put("batteryExcursion", new URL(classLoader.getResource("images/red-battery@2x.png").toString()));
						break;
					case "tempReset":
						inlineImages.put("tempReset", new URL(classLoader.getResource("images/blue-thermo@2x.png").toString()));
						break;
					case "lightReset":
						inlineImages.put("lightReset", new URL(classLoader.getResource("images/blue-light@2x.png").toString()));
						break;
					case "tiltReset":
						inlineImages.put("tiltReset", new URL(classLoader.getResource("images/blue-tilt-button@2x.png").toString()));
						break;
					case "pressureReset":	
						inlineImages.put("pressureReset", new URL(classLoader.getResource("images/blue-pressure@2x.png").toString()));
						break;
					case "humidityReset":
						inlineImages.put("humidityReset", new URL(classLoader.getResource("images/blue-humidity@2x.png").toString()));
						break;
					case "shockReset":
						inlineImages.put("shockReset", new URL(classLoader.getResource("images/blue-shock@2x.png").toString()));
						break;
					case "batteryReset":
						inlineImages.put("batteryReset", new URL(classLoader.getResource("images/blue-battery@2x.png").toString()));
						break;
					case "geoPointEntry":
						inlineImages.put("geoPointEntry", new URL(classLoader.getResource("images/blue-geo-arrival@2x.png").toString()));
						break;
					case "geoPointExit":
						inlineImages.put("geoPointExit", new URL(classLoader.getResource("images/blue-geo-departure@2x.png").toString()));
						break;
					}
				}
			}
		}
		return inlineImages;
	}
	/**
	 * 
	 * @return
	 */
	private static Session getSession(){
		Session session = null;
		try{
			String host = configCache.getSystemConfig(NotificationConstant.SMTP_HOST);  	
			String authenticate = configCache.getSystemConfig(NotificationConstant.MAIL_AITHENTICATE);
			String smtpPort = configCache.getSystemConfig(NotificationConstant.SMTP_PORT);
			//Get the session object  
			Properties props = new Properties();  
			props.put("mail.smtp.host",host);  
			props.put("mail.smtp.port", smtpPort);
			props.put("mail.smtp.auth", authenticate);  

			if(Boolean.valueOf(authenticate)){
				logger.debug("Session created with authentication information ");
				final String user = configCache.getSystemConfig(NotificationConstant.SMTP_USER);			
				final String password = configCache.getSystemConfig(NotificationConstant.SMTP_PASSWORD); 	
				session = Session.getInstance(props, new javax.mail.Authenticator() {  
					protected PasswordAuthentication getPasswordAuthentication() {  
						return new PasswordAuthentication(user, password );  
					}  
				});  
			}else{
				logger.debug("Session created without authentication information ");
				session = Session.getInstance(props);
			}
		}catch(Exception e){
			logger.error("Exception while getting session for sending mail"+ e.getMessage());
		}
		return session;
	}
	
	public static String constructLinkMessageBody(String msgBody, String title, String saluation) {
		String strStyle = includeRfxcelMailStyle(title);
		String strHeaderMessage = constructrfxcelheader(saluation);
		/*String mailNote =  " Note: 	This e-mail is intended for the use of the addressee only and may contain privileged, confidential, or proprietary 		"
			+ " information that is exempt from disclosure under law. If you have received this message in error, please inform us 		"
			+ "promptly by reply e-mail, then delete the e-mail and destroy any printed copy. Thank you." ;*/
		/*String strFooterMessage = constructrfxcelfooter();*/
		String strMessage = strStyle;
		strMessage = strStyle + "<table width='100%' cellpadding='3' cellspacing='3' height='95' border='0'><tr class='trbg'><td>"
				+ "<table width='100%' cellpadding='3' cellspacing='3' height='95' border='0' class='body_text'>";
		strMessage = strMessage + strHeaderMessage;
		strMessage = strMessage + msgBody;
		strMessage = strMessage + trStart+tdStart + msgBody + tdEnd+trEnd;
		strMessage = strMessage + trEmpty;
		strMessage = strMessage + "</table></td></tr></table>";
		strMessage = strMessage + "</body></html>";
		return strMessage;
	}
	
	private static String includeRfxcelMailStyle(String title) {
		String strStyle = "";
		strStyle = strStyle + "<html><head><title> "+ title +" </title><style type='text/css'>"
				+ ".table_text {	font-family: Verdana, Arial, Helvetica, sans-serif;;" + " 				font-size: 11px;;"
				+ "	 		color: #0D3359;;" + "				font-weight: bold;;" + "				text-decoration: none;}"
				+ ".body_text { 	font-family: Verdana, Arial, Helvetica, sans-serif;;" + "				font-size: 11px;;" + "				color: #000;}"
				+ ".trbg {	background-color: #eaf3fa;}" + "</style></head><body>";

		return strStyle;

	}
	private static String constructrfxcelheader(String saluation) {
		String strMessage = "<tr> <td class='table_text'><bold>"+ saluation+",</bold><br></td> </tr> ";
		return strMessage;
	}
	private static String constructrfxcelfooter() {
		String strMessage = "<div id='report_banner'><span class='table_text'> &nbsp; </span></div>"
			+"<br><a href='#'><img src='cid:logo' border='0'></a>"
			+ "</span></div>";
		return strMessage; 
		
	}

}
