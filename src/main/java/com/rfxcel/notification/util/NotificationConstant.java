package com.rfxcel.notification.util;

/**
 * 
 * @author Tejshree Kachare
 * Class for all the constants used in Notification module
 */
public class NotificationConstant {

	public static final String SMTP_HOST = "mail.smtphost";
	public static final String MAIL_AITHENTICATE = "mail.authenticate";
	public static final String SMTP_PORT = "mail.smtpport";
	public static final String SMTP_USER ="mail.smtp.username";
	public static final String SMTP_PASSWORD = "mail.smtp.password";
	public static final String EMAIL_FROM_ADDRESS ="mail.smtp.username";
	
	
	/* Constants added for processing Alert data*/
	public static final String GEO_FENCE_NUMBER = "geofenceNumber" ;
	public static final String GEO_FENCE_NAME = "geofenceName" ;
	public static final String GEO_FENCE_EVENT = "geofenceEvent" ;
	public static final String GEO_FENCE_POINT_NAME = "pointName" ;
	public static final String GEO_FENCE_LATITUDE = "latitude" ;
	public static final String GEO_FENCE_LONGITUDE = "longitude" ;
	public static final String GEO_FENCE_ADDRESS = "geoAddress" ;
	
	public static final String GEO_FENCE_ALARM_MSG_ENTRY = "ENTRY" ;
	public static final String GEO_FENCE_ALARM_MSG_EXIT = "EXIT" ;
	
	
	public static final long ALERT_CODE_TEMPERATURE_EXCURSION = 3042L;
	public static final long ALERT_CODE_LIGHT_EXCURSION =3043L;
	public static final long ALERT_CODE_TILT_EXCURSION =3044L;
	public static final long ALERT_CODE_PRESSURE_EXCURSION = 3045L;
	public static final long ALERT_CODE_HUMIDY_EXCURSION = 3046L;
	public static final long ALERT_CODE_CUMULATICVE_LOG_EXCURSION =3047L;
	public static final long ALERT_CODE_SHOCK_EXCURSION = 3048L;
	public static final long ALERT_CODE_BATTERY_EXCURSION = 3049L;
	public static final long ALERT_CODE_AMBIENT_TEMP_EXCURSION = 3051L;
	public static final long ALERT_CODE_ATTR_RESET = 3052L;
	public static final long ALERT_CODE_GROWTH_LOG = 3053L;
	public static final long ALERT_CODE_VIBRATION = 3054L;
	public static final long ALERT_CODE_GEO_POINT_ENTRY = 3701L;
	public static final long ALERT_CODE_GEO_POINT_EXIT = 3702L;
	public static final long ALERT_CODE_ROUTE_RESUMED = 3703L;
	public static final long ALERT_CODE_ROUTE_DEVIATION = 3704L;
	public static final long ALERT_CODE_WITHOUT_MOTION = 3705L;
	public static final long ALERT_CODE_SHIPMENT_ON_ROUTE = 3706L;
	public static final long ALERT_CODE_3050 = 3050L;

	
	public static final String ALARM_TYPE = "alarmType" ;
	

	public static String ALARM_VALUE = "alarmValue" ;
	public static String TIME_DURATION = "timeDuration" ;
	
	public static String EVENT_TYPE_SAME_LOCATION = "SAME_LOCATION" ;
	
	public static String NOTIFICATION_SEARCHTYPE_ALL = "All";
	public static String NOTIFICATION_SEARCHTYPE_CONTAINER = "Container";
	public static String NOTIFICATION_SEARCHTYPE_PARAMS = "params";
	public static final Integer NOTIFICATION_PAGESIZE = 100;
	public static final String NOTIFICATION_SORTBY_AGE = "Age";
	public static final String NOTIFICATION_SORTBY_TYPE = "Type";
	public static final String NOTIFICATION_GROUPBY_DEVICE = "Device";	
	
}
