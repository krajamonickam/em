package com.rfxcel.notification.util;

import java.util.HashMap;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.rule.util.RuleConstants;

/**
 * 
 * @author tejshree_kachare
 * @since Jul 6 2017
 */
public class EmailTemplateUtil {


	private static HashMap<Long, String> actionReqAlertMapping  =  new HashMap<Long, String>();
	private static HashMap<String, String> resetAttrAlertMapping  =  new HashMap<String, String>();
	
	public static HashMap<String, String> getResetAttrAlertMapping() {
		return resetAttrAlertMapping;
	}

	public static HashMap<Long, String> getActionReqAlertMapping() {
		return actionReqAlertMapping;
	}

	static{
		populateActionReqAlertMapping();
		populateResetAttrAlertMapping();
	}

	private static void populateActionReqAlertMapping() {
		actionReqAlertMapping.put(3042L, "tempExcursion");
		actionReqAlertMapping.put(3043L, "lightExcursion");
		actionReqAlertMapping.put(3044L, "tiltExcursion");
		actionReqAlertMapping.put(3045L,"pressureExcursion");
		actionReqAlertMapping.put(3046L, "humidityExcursion" );
		actionReqAlertMapping.put(3047L,"cumLogExcursion" );
		actionReqAlertMapping.put(3048L,"shockExcursion" );
		actionReqAlertMapping.put(3049L,"batteryExcursion");
	}

	private static void populateResetAttrAlertMapping() {
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_TEMPERATURE, "tempReset");	
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_LIGHT, "lightReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_TILT, "tiltReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_PRESSURE, "pressureReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_HUMIDITY, "humidityReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_CUMULATIVE_LOG, "cumLogReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_SHOCK, "shockReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_BATTERY, "batteryReset");
		resetAttrAlertMapping.put(RuleConstants.SENSOR_ATTR_AMBIENTTEMP, "ambientTempReset");
	}

	/**
	 * 
	 * @param alertMessage
	 * @param detailMessage
	 * @param correctiveAction
	 * @param imageMapping
	 * @param url 
	 * @return
	 */
	public static String getActionReqEmailTemplate(String alertMessage,String detailMessage, String correctiveAction, String imageMapping, String url){
		StringBuilder template = new StringBuilder();
		template.append(" <body style= \"margin:0; padding:0;\" bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
		template.append(" <table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F6F8FA\">");
		template.append(" <tr>");
		template.append(" <td align=\"center\" valign=\"top\" bgcolor=\"#F6F8FA\" style=\"background-color: #F6F8FA;\">");
		template.append(" <table border=\"0\" width=\"750\" cellpadding=\"0\" cellspacing=\"0\" class=\"container\"");
		template.append(" style=\"width:750px;max-width:750px;padding-top:29px\">");
		template.append(" <tr>");
		template.append(" <td class=\"container-padding content\" align=\"left\"");
		template.append(" style=\"padding-left:60px;padding-right:60px;padding-top:57px;padding-bottom:40px;background-color:#ffffff\">");
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
		template.append(" <tr>");
		template.append(getLogoImageDiv());
		template.append(" <td align=\"right\" valign=\"bottom\">");
		template.append(" <span style='font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;color:#CD040B;'> Action Required</span>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table><br>");
		/*template.append(" <div style='float: right; text-align: right; font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;color:#CD040B;'>");
		template.append("  Action Required");
		template.append(" </div>");*/
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
		//template.append(" style=\"padding-top:30px;padding-bottom:30px\">");
		template.append(" <tr>");
		template.append(" <td style=\"background:none; border-bottom: 1px solid #E2E3E4; height:1px; width:100%; margin:0px 0px 0px 0px;\">");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table><br><br>");
		
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"> ");
		template.append( "<tr> ");
		template.append("<td style=\"padding-left:0\">");
		
		template.append(" <div class=\"title\"");
		template.append(" style=\"font-family:Helvetica, Arial, sans-serif;font-size:22px;font-weight:normal;color:#CD040B\">");
		if(imageMapping!= null){
			template.append(" <img alt=\"Verizon Track and Trace\"");
			template.append(" src=\"cid:");
			template.append(imageMapping);
			if(imageMapping.equalsIgnoreCase("batteryExcursion")){
				template.append("\" width=\"20\"");
				template.append(" style=\"width: 100%; max-width: 20px; font-family: sans-serif; color: #ffffff; font-size: 20px; border: 0px;\"");
			}else{
				template.append("\" width=\"10\"");
				template.append(" style=\"width: 100%; max-width: 10px; font-family: sans-serif; color: #ffffff; font-size: 20px; border: 0px;\"");
			}
		}
		template.append(" border=\"0\">");
		template.append(alertMessage);
		template.append(" </div>");
		template.append(" </td></tr></table>");
		
		template.append(" <br>");
		template.append(" <div class=\"body-text\"");
		template.append(" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333\">");
		template.append(detailMessage );
		template.append(" <br><br>");
		if(correctiveAction!= null){
			template.append(correctiveAction);
			template.append(" </br></br>");
		}
		template.append(getActionButton(url, "View Details", "btnViewDetails"));
		
		template.append(" </div>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(getMailDisclaimer());
		template.append("  </table>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table>");
		template.append(" </body>");

		return template.toString();
	}

	/**
	 * @param alertMessage
	 * @param detailMessage
	 * @param imageMapping
	 * @param url
	 * @return
	 */
	public static String getEmailNoActionTemplate(String alertMessage,String detailMessage, String imageMapping, String url){
		StringBuilder template = new StringBuilder();
		template.append(" <body style=\"margin:0; padding:0;\" bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
		template.append(" <table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F6F8FA\">");
		template.append(" <tr>");
		template.append(" <td align=\"center\" valign=\"top\" bgcolor=\"#F6F8FA\" style=\"background-color: #F6F8FA;\">");
		template.append(" <table border=\"0\" width=\"750\" cellpadding=\"0\" cellspacing=\"0\" class=\"container\"");
		template.append(" style=\"width:750px;max-width:750px;padding-top:29px\">");
		template.append(" <tr>");
		template.append(" <td class=\"container-padding content\" align=\"left\"");
		template.append(" style=\"padding-left:60px;padding-right:60px;padding-top:57px;padding-bottom:40px;background-color:#ffffff\">");
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
		template.append(" <tr>");
		template.append(getLogoImageDiv());
		template.append(" <td align=\"right\" valign=\"bottom\">");
		template.append(" <span style='font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;color:#0066CC;'> No Action Required</span>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table><br>");
		/*template.append(" <div style='float: right; text-align: right; font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;color:#0066CC;'>");
		template.append(" No Action Required");
		template.append(" </div>");*/
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
		//template.append(" style=\"padding-top:30px;padding-bottom:30px\">");
		template.append(" <tr>");
		template.append(" <td style=\"background:none; border-bottom: 1px solid #E2E3E4; height:1px; width:100%; margin:0px 0px 0px 0px;\">");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table><br><br>");
		
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"> ");
		template.append( "<tr> ");
		template.append("<td style=\"padding-left:0\">");
		template.append(" <div class=\"title\"");
		template.append(" style=\"font-family:Helvetica, Arial, sans-serif;font-size:22px;font-weight:normal;color:#0066CC\">");
	
		if(imageMapping!=null){
			template.append("<img alt=\"Verizon Track and Trace\"");
			template.append(" src=\"cid:");
			template.append(imageMapping);
			if(imageMapping.equalsIgnoreCase("batteryReset")){
				template.append("\" width=\"20\"");
				template.append(" style=\"width: 100%; max-width: 20px; font-family: sans-serif; color: #ffffff; font-size: 20px; border: 0px;\"");
			}else{
				template.append("\" width=\"10\"");
				template.append(" style=\"width: 100%; max-width: 10px; font-family: sans-serif; color: #ffffff; font-size: 20px; border: 0px;\"");
			}
			template.append(" border=\"0\">");
		}
		
		template.append(alertMessage);
		template.append(" </div>");
		template.append(" </td></tr></table>");
		template.append("</br>");
		template.append("  <div class=\"body-text\"");
		template.append(" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333\">");
		template.append( detailMessage);
		template.append("</br></br>");
		template.append(getActionButton(url,"View Details","btnViewDetails"));
		
		template.append(" </div>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(getMailDisclaimer());
		template.append(" </table>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table>");
		template.append(" </body>");

		return template.toString();
	}

	
	/**
	 * 
	 * @param url
	 * @param imageId
	 * @param title
	 * @param bodyTextPart1
	 * @param bodyTextPart2
	 * @param bodyTextPart3
	 * @return
	 */
	public static String getWelcomeEmailTemplate(String url, String imageId, String title, String bodyTextPart1, String bodyTextPart2, String bodyTextPart3){
		StringBuilder template = new StringBuilder();
		template.append("<html> ");
		template.append(getHeadStyleCSS());
		template.append(" <body style=\"margin:0; padding:0;\" bgcolor=\"#F0F0F0\" leftmargin=\"0\" topmargin=\"0\" marginwidth=\"0\" marginheight=\"0\">");
		template.append(" <table border=\"0\" width=\"100%\" height=\"100%\" cellpadding=\"0\" cellspacing=\"0\" bgcolor=\"#F6F8FA\">");
		template.append(" <tr>");
		template.append(" <td align=\"center\" valign=\"top\" bgcolor=\"#F6F8FA\" style=\"background-color: #F6F8FA;\">");
		template.append(" <table border=\"0\" width=\"750\" cellpadding=\"0\" cellspacing=\"0\" class=\"container\"");
		template.append(" style=\"width:750px;max-width:750px;padding-top:29px\">");
		template.append(" <tr>");
		template.append(" <td class=\"container-padding content\" align=\"left\"");
		template.append(" style=\"padding-left:60px;padding-right:60px;padding-top:57px;padding-bottom:40px;background-color:#ffffff\">");
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
		template.append(" <tr>");
		template.append(getLogoImageDiv());
		template.append(" <td align=\"right\" valign=\"bottom\">");
		template.append(" <span style='font-family:Helvetica, Arial, sans-serif;font-size:12px;font-weight:bold;color:#0066CC;'> Welcome</span>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table></br>");
		
		template.append("<table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\">");
		//template.append(" style=\"padding-top:30px;padding-bottom:30px\">");
		template.append(" <tr>");
		template.append(" <td style=\"background:none; border-bottom: 1px solid #E2E3E4; height:1px; width:100%; margin:0px 0px 0px 0px;\">");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table><br><br>");
		
		template.append(" <table border=\"0\" width=\"100%\" cellpadding=\"0\" cellspacing=\"0\"> ");
		template.append( "<tr> ");
		template.append("<td style=\"padding-left:0\">");
		template.append(" <div class=\"title\"");
		template.append(" style=\"font-family:Helvetica, Arial, sans-serif;font-size:22px;font-weight:normal;color:#0066CC\">");
		template.append(title);
		template.append(" </div>");
		template.append(" </td></tr></table>");
		template.append(" <br>");
		template.append(" <div class=\"body-text\"");
		template.append(" style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:20px;text-align:left;color:#333333\">");
		template.append(bodyTextPart1);
		template.append(" <br><br>");
		template.append(" <span style=\"font-weight:bold\">"+bodyTextPart2+"</span>");
		
		template.append(" <br>");
		//template.append(getActionButton(url,"View Details", imageId));
		template.append(getWelcomeEmailButton(url,"View Details", imageId));
		
		template.append("<br><br>");
		template.append(bodyTextPart3);
		template.append(" <br>");
		template.append(" </div>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(getMailDisclaimer());
		template.append(" </table>");
		template.append(" </td>");
		template.append(" </tr>");
		template.append(" </table>");
		template.append(" </body>");
		template.append("</html> ");
		return template.toString();
	}

	
	/**
	 * 
	 * @return
	 */
	public static String getLogoImageDiv(){
		StringBuilder template = new StringBuilder();
		template.append(" <br>");
		template.append("  <td>");
		template.append(" <img alt=\"Verizon Track and Trace\"");
		template.append(" src=\"cid:rfxLogo\" width=\"234\"");
		template.append(" style=\"width: 100%; max-width: 234px; font-family: sans-serif; color: #ffffff; font-size: 20px; display: block; border: 0px;\"");
		template.append(" border=\"0\">");
		template.append(" </td>");
		
		
		
		return template.toString();
	}
	
	
	/**
	 * @param url
	 * @param imageMapping
	 * @return
	 */
	public static String getActionButton(String url,String altText, String imageMapping){
		StringBuilder actionButton = new StringBuilder();
		actionButton.append(" <br>");
		actionButton.append( "<a href=\""+url +"\" >");
		actionButton.append(" <img alt=\""+altText+"\"");
		//actionButton.append(" src=\"cid:"+imageMapping+"\" width=\"117\"");
		//actionButton.append(" style=\"width: 100%; max-width: 117px; font-family: sans-serif; color: #ffffff; font-size: 20px; display: block; border: 0px;\"");
		actionButton.append(" src=\"cid:"+imageMapping+"\" width=\"117\"");
		actionButton.append(" style=\"width: 117px; max-width: 117px;font-family: sans-serif; color: #ffffff; font-size: 20px; display: block; border: 0px;\"");
		actionButton.append(" border=\"0\">");
		actionButton.append(" </a>");
		return actionButton.toString();
	}
	
	public static String getWelcomeEmailButton(String url,String altText, String imageMapping){
		StringBuilder actionButton = new StringBuilder();
		actionButton.append(" <br>");
		actionButton.append( "<a href=\""+url +"\" >");
		actionButton.append(" <img alt=\""+altText+"\"");
		actionButton.append(" src=\"cid:"+imageMapping+"\" width=\"143\"");
		actionButton.append(" style=\"width: 143px; max-width: 143px; font-family: sans-serif; color: #ffffff; font-size: 20px; display: block; border: 0px;\"");
		actionButton.append(" border=\"0\">");
		actionButton.append(" </a>");
		return actionButton.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	public static String getHeadStyleCSS(){
		StringBuilder stylecss = new StringBuilder();


		stylecss.append("<head>");
		stylecss.append(" <meta http-equiv= \"Content-Type\" content=\"text/html; charset=UTF-8\">");
		stylecss.append(" <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> <!-- So that mobile will display zoomed in --> ");
		stylecss.append(" <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\"> <!-- enable media queries for windows phone 8 --> ");
		stylecss.append(" <meta name=\"format-detection\" content=\"telephone=no\"> <!-- disable auto telephone linking in iOS --> ");
		stylecss.append("<title>Verizon Track and Trace</title> ");

		stylecss.append(" <style type=\"text/css\"> ");
		stylecss.append(" body {margin: 0;padding: 0; -ms-text-size-adjust: 100%; -webkit-text-size-adjust: 100%;} ");
		stylecss.append(" table { border-spacing: 0; }");
		stylecss.append("  table td { border-collapse: collapse; }");
		stylecss.append(" .ExternalClass { width: 100%; }");
		stylecss.append(" .ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div { line-height: 100%; }");
		stylecss.append(" .ReadMsgBody {  width: 100%; background-color: #ebebeb; }");
		stylecss.append(" table { mso-table-lspace: 0pt;  mso-table-rspace: 0pt; }");
		stylecss.append(" img { -ms-interpolation-mode: bicubic;  }");
		stylecss.append(" .yshortcuts a { border-bottom: none !important; }");
		stylecss.append("  @media screen and (max-width: 599px) { .force-row, .container { width: 100% !important; max-width: 100% !important; } }");
		stylecss.append("  @media screen and (max-width: 400px) {  .container-padding {  padding-left: 12px !important; padding-right: 12px !important;  } }");
		stylecss.append(" .ios-footer a {  color: #aaaaaa !important; text-decoration: underline; }");
		stylecss.append(" </style> ");
		stylecss.append(" </head>");
		return stylecss.toString();
	}

	/**
	 * 
	 * @return
	 */
	public static String getMailDisclaimer(){
		StringBuilder disclaimer = new StringBuilder();
		disclaimer.append(" <tr>");
		disclaimer.append(" <td class=\"container-padding footer-text\" align=\"left\" "
				+ " style=\"font-family:Helvetica, Arial, sans-serif;font-size:14px;line-height:18px;color:#aaaaaa;padding-left:31px;padding-right:31px;padding-bottom:62px;\">"
			//	+ " <br><br>"
			//	+ " <a href=\"#\" style=\"color:#aaaaaa\">Unsubscribe</a> | <a href=\"#\" style=\"color:#aaaaaa\">Email Settings</a>"
				+ " <br><br> ");
				
		String emailDisclaimer = ConfigCache.getInstance().getSystemConfig("email.disclaimer");
		if(emailDisclaimer!= null){
			disclaimer.append(" <em>"+  emailDisclaimer +"</em> <br>");
		}
		disclaimer.append(" <em>Note: This e-mail is intended for the use of the addressee only and may contain privileged, confidential, or proprietary information that is exempt from disclosure under law. "
				+ " If you have received this message in error, please inform us promptly by reply e-mail, then delete the e-mail and destroy any printed copy. Thank you.</em> </td>");
		disclaimer.append(" </td>");
		disclaimer.append(" </tr>");
		return disclaimer.toString();
	}
}
