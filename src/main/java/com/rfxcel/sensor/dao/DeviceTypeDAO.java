package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.cache.DeviceCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.Attribute;
import com.rfxcel.sensor.beans.DeviceDetail;

/**
 * 
 * @author sachin_kohale
 * @since Apr 26, 2017
 */
public class DeviceTypeDAO {
	private static final Logger logger = Logger.getLogger(DeviceTypeDAO.class);
	private static DeviceTypeDAO deviceTypeDAO;
	
	public static DeviceTypeDAO getInstance(){
		if(deviceTypeDAO == null){
			deviceTypeDAO = new DeviceTypeDAO();			
		}
		return deviceTypeDAO;
	}
	
	/**
	 * <p>This method inserts device type and its attributes</p>
	 * @param deviceDetails
	 * @return
	 * @throws SQLException 
	 */
	public Integer addDeviceType(DeviceDetail deviceDetail , int orgId) throws SQLException{	
		Integer rowInsertedValue = 0;
		
		Integer defaultValue = 1;
		try{
		Object value = DAOUtility.runInTransaction(conn->{
			Integer rowInserted = -1;
				String sqlQuery = "insert into sensor_device_types(device_type, device_manufacturer, org_id) values(?, ?, ?)";
				PreparedStatement stmt = conn.prepareStatement(sqlQuery, Statement.RETURN_GENERATED_KEYS);
				String deviceType;
				String manufacturer;			
				deviceType =  deviceDetail.getDeviceType();
				manufacturer = deviceDetail.getManufacturer();
				stmt.setString(1, deviceType);
				stmt.setString(2, manufacturer);
				stmt.setInt(3, orgId);
				stmt.executeUpdate();	
				ResultSet rs = stmt.getGeneratedKeys();
				if (rs.next()) {
					rowInserted = rs.getInt(1);
				}
				rs.close();
				stmt.close(); //close the deviceType prepareStatment
				if(rowInserted > 0){
					String attrSqlQuery = "INSERT INTO sensor_attr_type(device_type_id, attr_id, attr_name, legend, excursion_duration, notify_limit, show_graph, show_map, org_id, unit,range_min,range_max, sensor_attribute,round_digits,alert_on_reset) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(attrSqlQuery);
					List<com.rfxcel.sensor.beans.Attribute> attributeList = deviceDetail.getAttributes();
					for(com.rfxcel.sensor.beans.Attribute attribute : attributeList){
						String legend = attribute.getLegend();
						Integer excursionDuration = attribute.getExcursionDuration();
						Integer notifyLimit = attribute.getNotifyLimit();
						Boolean showGraph = attribute.getShowGraph();
						Boolean showMap = attribute.getShowMap();
						String unit = attribute.getUnit();
						Integer roundOff = attribute.getRoundOffTo();
						
						stmt.setInt(1, rowInserted);
						stmt.setInt(2, attribute.getAttrId());
						stmt.setString(3, attribute.getAttrName());
						
						if(legend != null){
							stmt.setString(4, legend);
						}else{
							stmt.setNull(4, Types.NULL);
						}
						
						if(excursionDuration != null){
							stmt.setInt(5, excursionDuration);
						}else{
							stmt.setNull(5, Types.NULL);
						}
						
						if(notifyLimit != null){
							stmt.setInt(6, notifyLimit);
						}else{
							stmt.setNull(6, Types.NULL);
						}
																	
						stmt.setBoolean(7, showGraph);
						stmt.setBoolean(8, showMap);
						stmt.setInt(9, orgId);
						
						if(unit != null){
							stmt.setString(10, unit);
						}else{
							stmt.setNull(10, Types.NULL);
						}
						stmt.setDouble(11, attribute.getRangeMin());
						stmt.setDouble(12, attribute.getRangeMax());
						
						
						stmt.setBoolean(13, attribute.getIsSensorAttribute());
						
						if(roundOff != null){
							stmt.setInt(14, attribute.getRoundOffTo());
						}else{
							stmt.setNull(14, Types.NULL);
						}
						
						if(attribute.getAlertOnReset() != null){
							stmt.setInt(15, attribute.getAlertOnReset());
						}else{
							stmt.setInt(15, defaultValue);
						}
						
						
						stmt.addBatch();
					}
					stmt.executeBatch();
					stmt.close(); //closing attribute prepareStatement
				}
					return rowInserted;
		});
		rowInsertedValue = value!= null ? Integer.valueOf(value.toString()):null;
		 return rowInsertedValue;					
		}finally{
			
		}
	}

	/**
	 * <p>This method update device type and its attributes</p>
	 * @param deviceDetails
	 * @return
	 * @throws SQLException 
	 */
	public Integer updateDeviceType(DeviceDetail deviceDetail, int orgId) throws SQLException{	
		Integer rowUpdatedValue = 0;
		Integer defaultValue = 1;
		try{
			Object value = DAOUtility.runInTransaction(conn->{
				Integer rowUpdated = -1;
				String sqlQuery = "UPDATE sensor_device_types SET device_type=?, device_manufacturer=?  WHERE device_type_id=? AND org_id = ?";
				PreparedStatement stmt = conn.prepareStatement(sqlQuery);
				String deviceType;
				String manufacturer;
				deviceType =  deviceDetail.getDeviceType();
				manufacturer = deviceDetail.getManufacturer();
				stmt.setString(1, deviceType);	
				stmt.setString(2, manufacturer);
				stmt.setInt(3, deviceDetail.getDeviceTypeId());			
				stmt.setInt(4, orgId);
				rowUpdated = stmt.executeUpdate();
				stmt.close();   //closing deviceType preapareStatement
				if(rowUpdated > 0){				
					//delete existing attributes records by deviceTypeId
					String deleteAttrQuery = "DELETE FROM sensor_attr_type WHERE device_type_id = ? and scope=0";
					stmt = conn.prepareStatement(deleteAttrQuery);
					stmt.setInt(1, deviceDetail.getDeviceTypeId());
					stmt.executeUpdate();
					stmt.close();    //close stmt of deleteAttribute
					
					String attrSqlQuery = "INSERT INTO sensor_attr_type(device_type_id, attr_id, attr_name, legend, excursion_duration, notify_limit, show_graph, show_map, org_id, unit,range_min,range_max, sensor_attribute,round_digits,alert_on_reset) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(attrSqlQuery);
					List<com.rfxcel.sensor.beans.Attribute> attributeList = deviceDetail.getAttributes();
					for(com.rfxcel.sensor.beans.Attribute attribute : attributeList){
						String legend = attribute.getLegend();
						Integer excursionDuration = attribute.getExcursionDuration();
						Integer notifyLimit = attribute.getNotifyLimit();
						Boolean showGraph = attribute.getShowGraph();
						Boolean showMap = attribute.getShowMap();
						String unit = attribute.getUnit();
						Integer roundOff = attribute.getRoundOffTo();
						
						stmt.setInt(1, deviceDetail.getDeviceTypeId());
						stmt.setInt(2, attribute.getAttrId());
						stmt.setString(3, attribute.getAttrName());
						
						if(legend != null){
							stmt.setString(4, legend);
						}else{
							stmt.setNull(4, Types.NULL);
						}
						
						if(excursionDuration != null){
							stmt.setInt(5, excursionDuration);
						}else{
							stmt.setNull(5, Types.NULL);
						}
						
						if(notifyLimit != null){
							stmt.setInt(6, notifyLimit);
						}else{
							stmt.setNull(6, Types.NULL);
						}
									
						stmt.setBoolean(7, showGraph);
						stmt.setBoolean(8, showMap);
						stmt.setInt(9, orgId);
						
						if(unit != null){
							stmt.setString(10, unit);
						}else{
							stmt.setNull(10, Types.NULL);
						}
						
						stmt.setDouble(11, attribute.getRangeMin());
						stmt.setDouble(12, attribute.getRangeMax());
						stmt.setBoolean(13, attribute.getIsSensorAttribute());
						
						
						if(roundOff != null){
							stmt.setInt(14, attribute.getRoundOffTo());
						}else{
							stmt.setNull(14, Types.NULL);
						}
						
						
						if(attribute.getAlertOnReset() != null){
							stmt.setInt(15, attribute.getAlertOnReset());
						}else{
							stmt.setInt(15, defaultValue);
						}
						stmt.addBatch();
					}
					stmt.executeBatch();
					stmt.close();  //closing attribute prepareStatement			
				}
				return rowUpdated;
			});
			rowUpdatedValue = value!= null ? Integer.valueOf(value.toString()):null;
			 return rowUpdatedValue;			
		}finally{
			
		}
		
	}
	
	/**
	 * 
	 * @param deviceTypeId
	 * @throws SQLException
	 */
	public void deleteDeviceType(int deviceTypeId) throws SQLException{
		Connection conn= null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "UPDATE sensor_device_types SET active=0 WHERE device_type_id=?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, deviceTypeId);
			stmt.executeUpdate();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param deviceType
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfActiveDeviceTypeIsPresent(int deviceTypeId) throws SQLException{
		Connection conn= null;
		Boolean isPresent = false;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT EXISTS (SELECT 1 from sensor_device_types where device_type_id = ? and active = 1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, deviceTypeId);				
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count!=0){
				isPresent = true;
			}
			stmt.close();
			rs.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<DeviceDetail> getDeviceTypes(int orgId) throws SQLException {
		Connection conn= null;
		ArrayList<DeviceDetail> deviceList = new ArrayList<DeviceDetail>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "select device_type, device_type_id, device_manufacturer, active, gps_com_freq_limit, receive_com_freq_limit, send_com_freq_limit from sensor_device_types where org_id = ? AND active=1";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			DeviceDetail deviceDetail;
			while(rs.next()){
				deviceDetail = new DeviceDetail();
				deviceDetail.setDeviceType(rs.getString("device_type"));
				deviceDetail.setDeviceTypeId(rs.getInt("device_type_id"));
				deviceDetail.setManufacturer(rs.getString("device_manufacturer"));
				deviceDetail.setActive(rs.getBoolean("active"));
				deviceDetail.setGpsComFreqLimit(rs.getInt("gps_com_freq_limit"));
				deviceDetail.setReceiveComFreqLimit(rs.getInt("receive_com_freq_limit"));
				deviceDetail.setSendComFreqLimit(rs.getInt("send_com_freq_limit"));
				deviceList.add(deviceDetail);
			}
			stmt.close();
			rs.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceList;
	}
	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<DeviceDetail> getAllDeviceTypes() throws SQLException {
		Connection conn= null;
		ArrayList<DeviceDetail> deviceList = new ArrayList<DeviceDetail>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT device_type, device_type_id, device_manufacturer, org_id, active, gps_com_freq_limit, receive_com_freq_limit, send_com_freq_limit FROM sensor_device_types WHERE active=1";
			PreparedStatement stmt = conn.prepareStatement(query);			
			ResultSet rs = stmt.executeQuery();
			DeviceDetail deviceDetail;
			while(rs.next()){
				deviceDetail = new DeviceDetail();
				deviceDetail.setDeviceType(rs.getString("device_type"));
				deviceDetail.setDeviceTypeId(rs.getInt("device_type_id"));
				deviceDetail.setManufacturer(rs.getString("device_manufacturer"));
				deviceDetail.setOrgId(rs.getInt("org_id"));
				deviceDetail.setActive(rs.getBoolean("active"));
				deviceDetail.setGpsComFreqLimit(rs.getInt("gps_com_freq_limit"));
				deviceDetail.setReceiveComFreqLimit(rs.getInt("receive_com_freq_limit"));
				deviceDetail.setSendComFreqLimit(rs.getInt("send_com_freq_limit"));
				deviceList.add(deviceDetail);
			}
			stmt.close();
			rs.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceList;
	}
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Attribute> getAttributeList() throws SQLException{
		Connection conn= null;
		ArrayList<Attribute> attributeList = new ArrayList<Attribute>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT id, attr_name, is_configurable, alert_code, unit,range_min,range_max,sensor_attribute,round_digits,alert_on_reset FROM sensor_attributes WHERE is_configurable=1 order by attr_name";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Attribute attribute = new Attribute();
				attribute.setAttrId(rs.getInt("id"));
				attribute.setAttrName(rs.getString("attr_name"));
				attribute.setIsConfigurable(rs.getBoolean("is_configurable"));
				attribute.setAlertCode(rs.getLong("alert_code"));
				attribute.setUnit(rs.getString("unit"));
				attribute.setRangeMin(rs.getDouble("range_min"));
				attribute.setRangeMax(rs.getDouble("range_max"));
				attribute.setIsSensorAttribute(rs.getBoolean("sensor_attribute"));
				attribute.setRoundOffTo(rs.getString("round_digits")!= null ? Integer.parseInt(rs.getString("round_digits")):null);
				attribute.setAlertOnReset(rs.getInt("alert_on_reset"));
				attribute.setExcursion(null);
				attributeList.add(attribute);
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return attributeList;
	}
	
	/**
	 * 
	 * @param deviceDetails
	 * @return
	 * @throws SQLException
	 */
	
	public ArrayList<com.rfxcel.sensor.beans.Attribute> getAttributesByType(DeviceDetail deviceDetails, int orgId) throws SQLException
	{
		ArrayList<com.rfxcel.sensor.beans.Attribute> attributeList ;
		Connection conn= null;
		try{
			attributeList = new ArrayList<com.rfxcel.sensor.beans.Attribute>();
			String query = "";
			PreparedStatement stmt = null;
			if(orgId > 0){
				conn = DAOUtility.getInstance().getConnection();
				query = "SELECT attr_id, legend, attr_name, excursion_duration, notify_limit, show_graph, show_map, range_min,range_max, unit,sensor_attribute,round_digits,alert_on_reset from sensor_attr_type where device_type_id=? and scope=0 and org_id =? order by attr_name";
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, deviceDetails.getDeviceTypeId());
				stmt.setInt(2,orgId);
				
			} else {
				conn = DAOUtility.getInstance().getConnection();
				query = "SELECT attr_id, legend, attr_name, excursion_duration, notify_limit, show_graph, show_map, range_min,range_max, unit,sensor_attribute,round_digits,alert_on_reset from sensor_attr_type where device_type_id=? and scope=0 and org_id != 0 order by attr_name";
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, deviceDetails.getDeviceTypeId());				
			}
			ResultSet  rs = stmt.executeQuery();
			while(rs.next())
			{
				com.rfxcel.sensor.beans.Attribute attribute = new com.rfxcel.sensor.beans.Attribute();
				attribute.setAttrId(rs.getInt("attr_id"));
				attribute.setLegend(rs.getString("legend"));
				attribute.setAttrName(rs.getString("attr_name"));
				attribute.setExcursionDuration(rs.getInt("excursion_duration"));
				attribute.setNotifyLimit(rs.getInt("notify_limit"));
				attribute.setShowGraph(rs.getBoolean("show_graph"));
				attribute.setShowMap(rs.getBoolean("show_map"));
				attribute.setRangeMin(rs.getDouble("range_min"));
				attribute.setRangeMax(rs.getDouble("range_max"));
				attribute.setUnit(rs.getString("unit"));
				attribute.setIsSensorAttribute(rs.getBoolean("sensor_attribute"));
				attribute.setRoundOffTo(rs.getString("round_digits")!= null ? Integer.parseInt(rs.getString("round_digits")):null);
				attribute.setAlertOnReset(rs.getInt("alert_on_reset"));
				
				attributeList.add(attribute);
			}

			rs.close();
			stmt.close();

		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return attributeList;
	}

	/**
	 * <p> Deavtivate devices for an organization
	 * @param orgId
	 * @throws SQLException
	 */
	public void deactivateOrgDeviceTypes(Connection conn,Integer orgId) throws SQLException {
		try{
			String query = "Update sensor_device_types set active=0 where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			stmt.executeUpdate();
			stmt.close();
			ArrayList<Integer> deviceTypeIds = getDeviceTypesForOrg(orgId);
			//deleting attributes by orgId
			deleteDeviceTypeAttributesForOrg(conn, orgId);
			//removing attribute data from cache
			DeviceCache.getInstance().removeDeviceType(deviceTypeIds);
		}finally{
			
		}
	}

	
	/**
	 * <p> Get device type ids for an organization
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Integer> getDeviceTypesForOrg(Integer orgId)throws SQLException
	{
		ArrayList<Integer> deviceTypeIds = new ArrayList<Integer>();
		Connection conn= null;
		try{
			String query = "SELECT device_type_id from sensor_attr_type where org_id =?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet  rs = stmt.executeQuery();
			while(rs.next()){
				deviceTypeIds.add(rs.getInt("device_type_id"));
			}
			rs.close();
			stmt.close();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceTypeIds;
	}
	
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Attribute> getDefaultAttributeList() throws SQLException{
		ArrayList<Attribute> attributeList = new ArrayList<Attribute>();
		Connection conn= null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT id, attr_name, is_configurable, alert_code,range_min,range_max,sensor_attribute,round_digits,alert_on_reset FROM sensor_attributes WHERE is_configurable=0";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Attribute attribute = new Attribute();
				attribute.setAttrId(rs.getInt("id"));
				attribute.setAttrName(rs.getString("attr_name"));	
				attribute.setLegend(rs.getString("attr_name"));
				attribute.setRangeMin(rs.getDouble("range_min"));
				attribute.setRangeMax(rs.getDouble("range_max"));
				attribute.setIsSensorAttribute(rs.getBoolean("sensor_attribute"));
				attribute.setRoundOffTo(rs.getString("round_digits")!= null ? Integer.parseInt(rs.getString("round_digits")):null);
				attribute.setAlertOnReset(rs.getInt("alert_on_reset"));
				attributeList.add(attribute);
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return attributeList;
	}
	
	/**
	 * 
	 * @param deviceTypeId
	 * @param orgId
	 * @throws SQLException
	 */
	public void addDefaultAttributesByDeviceType(int deviceTypeId, int orgId) throws SQLException{
		Connection conn = null;	
		Integer defaultValue =1;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String attrSqlQuery = "INSERT INTO sensor_attr_type(device_type_id, attr_id, attr_name, legend, excursion_duration, notify_limit, show_graph, show_map, org_id, scope,range_min,range_max, sensor_attribute,round_digits,alert_on_reset) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(attrSqlQuery);
			List<com.rfxcel.sensor.beans.Attribute> attributeList = getDefaultAttributeList();
			for(com.rfxcel.sensor.beans.Attribute attribute : attributeList){				
				Integer excursionDuration = 0;
				Integer notifyLimit = 1;
				Integer roundOff = attribute.getRoundOffTo();
				Boolean showGraph = Boolean.FALSE;
				Boolean showMap = Boolean.FALSE;
				stmt.setInt(1, deviceTypeId);
				stmt.setInt(2, attribute.getAttrId());
				stmt.setString(3, attribute.getAttrName());					
				stmt.setString(4, attribute.getLegend());
				stmt.setInt(5, excursionDuration);
				stmt.setInt(6, notifyLimit);								
				stmt.setBoolean(7, showGraph);
				stmt.setBoolean(8, showMap);
				stmt.setInt(9, orgId);
				stmt.setInt(10, 1);
				stmt.setDouble(11, attribute.getRangeMin());
				stmt.setDouble(12, attribute.getRangeMax());
				stmt.setBoolean(13, attribute.getIsSensorAttribute());
				
				if(roundOff != null){
					stmt.setInt(14, attribute.getRoundOffTo());
				}else{
					stmt.setNull(14, Types.NULL);
				}
				if(attribute.getAlertOnReset() != null){
					stmt.setInt(15, attribute.getAlertOnReset());
				}else{
					stmt.setInt(15, defaultValue);
				}
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();  	
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param conn
	 * @param deviceTypeId
	 * @param orgId
	 * @throws SQLException
	 */
	public void addDefaultAttributesByDeviceType(Connection conn, int deviceTypeId, int orgId) throws SQLException{				
		try{
			Integer defaultValue = 1;
			String attrSqlQuery = "INSERT INTO sensor_attr_type(device_type_id, attr_id, attr_name, legend, excursion_duration, notify_limit, show_graph, show_map, org_id, scope,range_min,range_max, sensor_attribute,round_digits,alert_on_reset) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(attrSqlQuery);
			List<com.rfxcel.sensor.beans.Attribute> attributeList = getDefaultAttributeList();
			for(com.rfxcel.sensor.beans.Attribute attribute : attributeList){				
				Integer excursionDuration = 0;
				Integer notifyLimit = 1;
				Integer roundOff = attribute.getRoundOffTo();
				Boolean showGraph = Boolean.FALSE;
				Boolean showMap = Boolean.FALSE;
				stmt.setInt(1, deviceTypeId);
				stmt.setInt(2, attribute.getAttrId());
				stmt.setString(3, attribute.getAttrName());					
				stmt.setString(4, attribute.getLegend());
				stmt.setInt(5, excursionDuration);
				stmt.setInt(6, notifyLimit);								
				stmt.setBoolean(7, showGraph);
				stmt.setBoolean(8, showMap);
				stmt.setInt(9, orgId);
				stmt.setInt(10, 1);
				stmt.setDouble(11, attribute.getRangeMin());
				stmt.setDouble(12, attribute.getRangeMax());
				stmt.setBoolean(13, attribute.getIsSensorAttribute());
				
				if(roundOff != null){
					stmt.setInt(14, attribute.getRoundOffTo());
				}else{
					stmt.setNull(14, Types.NULL);
				}
				
				if(attribute.getAlertOnReset() != null){
					stmt.setInt(15, attribute.getAlertOnReset());
				}else{
					stmt.setInt(15, defaultValue);
				}
				
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();  	
		}finally{			
		}
	}
	
	/**
	 * <p>It checks if device type is present for orgId and devicetype</p>
	 * @param deviceType
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceTypeIsPresentForOrg(String deviceType, Integer orgId) throws SQLException{
		Connection conn= null;
		Boolean isPresent = false;
		try{
			conn = DAOUtility.getInstance().getConnection();			
			String query = "SELECT EXISTS (SELECT 1 from sensor_device_types where device_type = ? and org_id = ? and active = 1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceType);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count!=0){
				isPresent = true;
			}
			stmt.close();
			rs.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * <p>It checks the if same device type is present by orgId and devicetype except current device type id</p>
	 * @param deviceType
	 * @param deviceTypeId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceTypeIsPresentForOrg(String deviceType, Integer deviceTypeId, Integer orgId) throws SQLException{
		Connection conn= null;
		Boolean isPresent = false;
		try{
			conn = DAOUtility.getInstance().getConnection();			
			String query = "SELECT EXISTS (SELECT 1 from sensor_device_types where device_type = ? and device_type_id <> ? and org_id = ? and active = 1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceType);
			stmt.setInt(2, deviceTypeId);
			stmt.setInt(3, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count!=0){
				isPresent = true;
			}
			stmt.close();
			rs.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * <p>This is used to add default devicetype</p>
	 * @param orgId
	 * @return
	 * @throws SQLException 
	 */
	public void addDefaultDeviceType(Connection conn, Integer orgId) throws SQLException{
		ArrayList<DeviceDetail> devicetypeList = getDefaultDeviceTypeList(conn);	
	    ArrayList<Integer> deviceTypeIds =	addDefaultDeviceTypeList(conn, devicetypeList, orgId);
	    //adding default devicetype attributes for default devicetypes
		for(Integer deviceTypeId : deviceTypeIds){
			DeviceTypeDAO.getInstance().addDefaultAttributesByDeviceType(conn, deviceTypeId, orgId);			
		}	 		
	}
	
	/**
	 * <p>get default devicetype list</p>
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<DeviceDetail> getDefaultDeviceTypeList(Connection conn) throws SQLException{
		ArrayList<DeviceDetail> deviceList = new ArrayList<DeviceDetail>();
		try{
			String query = "select device_type, device_type_id, device_manufacturer, active, gps_com_freq_limit, receive_com_freq_limit, send_com_freq_limit from sensor_device_types where org_id = 0 AND active=1";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			DeviceDetail deviceDetail;
			while(rs.next()){
				deviceDetail = new DeviceDetail();
				deviceDetail.setDeviceType(rs.getString("device_type"));
				deviceDetail.setDeviceTypeId(rs.getInt("device_type_id"));
				deviceDetail.setManufacturer(rs.getString("device_manufacturer"));
				deviceDetail.setActive(rs.getBoolean("active"));
				deviceDetail.setGpsComFreqLimit(rs.getInt("gps_com_freq_limit"));
				deviceDetail.setReceiveComFreqLimit(rs.getInt("receive_com_freq_limit"));
				deviceDetail.setSendComFreqLimit(rs.getInt("send_com_freq_limit"));
				deviceList.add(deviceDetail);
			}
			stmt.close();
			rs.close();
			
		}finally{
			
		}
		return deviceList;
	}
	
		
	/**
	 * <p>Add default devicetype list</p>
	 * @param conn 
	 * @param deviceList,orgId
	 * @return
	 * @throws SQLException 
	 */
	public ArrayList<Integer> addDefaultDeviceTypeList(Connection conn, List<DeviceDetail> deviceList, Integer orgId) throws SQLException{
		ArrayList<Integer> deviceTypeIds = new ArrayList<Integer>();
		try{			
			String query = "insert into sensor_device_types(device_type, device_manufacturer, org_id, active, gps_com_freq_limit, receive_com_freq_limit, send_com_freq_limit) VALUES(?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(query, PreparedStatement.RETURN_GENERATED_KEYS);
			for(com.rfxcel.sensor.beans.DeviceDetail device : deviceList){				
				stmt.setString(1, device.getDeviceType());
				stmt.setString(2, device.getManufacturer());
				stmt.setInt(3, orgId);					
				stmt.setBoolean(4, device.getActive());
				stmt.setInt(5, device.getGpsComFreqLimit());
				stmt.setInt(6, device.getReceiveComFreqLimit());
				stmt.setInt(7, device.getSendComFreqLimit());
				stmt.addBatch();
			}
			stmt.executeBatch();
			ResultSet rs = stmt.getGeneratedKeys();
			while(rs.next()){
				Integer deviceTypeId = rs.getInt(1);
				deviceTypeIds.add(deviceTypeId);
			}
			rs.close();
			stmt.close(); 
		}finally{
		}
		return deviceTypeIds;
	}
	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Integer> getDeviceTypeIdsByOrgId(Integer orgId) throws SQLException{
		Connection conn= null;
		ArrayList<Integer> deviceTypeIds = new ArrayList<Integer>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT device_type_id FROM sensor_device_types WHERE org_id=? AND active=1";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Integer deviceTypeId = rs.getInt("device_type_id");
				deviceTypeIds.add(deviceTypeId);
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceTypeIds;
	}
	
	
	/**
	 * <p> get device type ids for an organization and manufacturer
	 * @param orgId
	 * @param manufacturer
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Integer> getDeviceTypeIdsByOrgId(Integer orgId, String manufacturer) throws SQLException{
		Connection conn= null;
		ArrayList<Integer> deviceTypeIds = new ArrayList<Integer>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT device_type_id FROM sensor_device_types WHERE org_id=? AND active=1 AND LOWER(device_manufacturer) = ?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			stmt.setString(2, manufacturer);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Integer deviceTypeId = rs.getInt("device_type_id");
				deviceTypeIds.add(deviceTypeId);
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceTypeIds;
	}
	
	/**
	 * 
	 * @param orgId
	 * @param deviceType
	 * @return
	 * @throws SQLException
	 */
	public String getDeviceTypeManufacturer(Connection conn, Integer orgId, String deviceType) throws SQLException{		
		String manufacturer = null;
		try{
			String sqlQuery = "SELECT device_manufacturer FROM sensor_device_types WHERE org_id=? AND device_type = ? AND active=1";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			stmt.setString(2, deviceType);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				manufacturer = rs.getString("device_manufacturer");
			}
			rs.close();
			stmt.close();
			
		}finally{
			
		}
		return manufacturer;
		
	}
	
	public String getDeviceTypeManufacturer(Integer orgId, String deviceType) throws SQLException{		
		String manufacturer = null;
		Connection conn = null;
		try{			
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT device_manufacturer FROM sensor_device_types WHERE org_id=? AND device_type = ? AND active=1";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			stmt.setString(2, deviceType);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				manufacturer = rs.getString("device_manufacturer");
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return manufacturer;
		
	}
	/**
	 * 
	 * @param conn
	 * @param deviceTypeIds
	 * @throws SQLException
	 */
	public void deleteDeviceTypeAttributesForOrg(Connection conn, Integer orgId) throws SQLException{
		String query = "DELETE FROM sensor_attr_type WHERE org_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
}
