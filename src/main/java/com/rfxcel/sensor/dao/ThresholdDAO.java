package com.rfxcel.sensor.dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.Threshold;
import com.rfxcel.sensor.util.Config;

public class ThresholdDAO {
	private static ThresholdDAO threshHoldDAO;
	
	public static ThresholdDAO getInstance(){
		if(threshHoldDAO == null){
			threshHoldDAO = new ThresholdDAO();
		}
		Config.getInstance().getProp();
		return threshHoldDAO;
	}

	
	// To save threshhold

	public String saveThreshold(List<Threshold> listThreshHold) throws SQLException
	{
		String flagValue="";
		try {
			Object value = DAOUtility.runInTransaction(conn->{
				String flag = "";
				for(int i=0;i<listThreshHold.size();i++)
				{	
					String getThreshHoldQuery = "SELECT min_value,max_value from sensor_threshold where ndc=? and attr_name=?";
					PreparedStatement stmThreshHold = conn.prepareStatement(getThreshHoldQuery);
					stmThreshHold.setString(1, listThreshHold.get(i).getNdc());
					stmThreshHold.setString(2, listThreshHold.get(i).getAttrName());
					ResultSet  rsThreshHold = stmThreshHold.executeQuery();
					if(rsThreshHold.next())
					{
						int  rs =0;
						String deleteQuery="delete from sensor_threshold where ndc=? and attr_name=?";
						PreparedStatement stmt = conn.prepareStatement(deleteQuery);
						stmt.setString(1, listThreshHold.get(i).getNdc());
						stmt.setString(2, listThreshHold.get(i).getAttrName());
						rs= stmt.executeUpdate();
						stmt.close();
						/*if(rs>0)
										{*/
						int insertStatus=0;
						String insertQuery="insert into sensor_threshold(ndc,attr_name,min_value,max_value,sensor_type)values(?,?,?,?,(select device_type_id from sensor_device_types where device_type=?))";
						PreparedStatement stmtInsert = conn.prepareStatement(insertQuery);
						stmtInsert.setString(1, listThreshHold.get(i).getNdc());
						stmtInsert.setString(2, listThreshHold.get(i).getAttrName());
						stmtInsert.setDouble(3, listThreshHold.get(i).getMin());
						stmtInsert.setDouble(4, listThreshHold.get(i).getMax());
						stmtInsert.setString(5, listThreshHold.get(i).getNdc());
						insertStatus= stmtInsert.executeUpdate();
						stmtInsert.close();

						if(insertStatus>0)
							flag="success";
						/*}*/
						
					}
					else
					{
						int insertStatus=0;
						String insertQuery="insert into sensor_threshold(ndc,attr_name,min_value,max_value,sensor_type)values(?,?,?,?,(select device_type_id from sensor_device_types where device_type=?))";
						PreparedStatement stmtInsert = conn.prepareStatement(insertQuery);
						stmtInsert.setString(1, listThreshHold.get(i).getNdc());
						stmtInsert.setString(2, listThreshHold.get(i).getAttrName());
						stmtInsert.setDouble(3, listThreshHold.get(i).getMin());
						stmtInsert.setDouble(4, listThreshHold.get(i).getMax());
						stmtInsert.setString(5, listThreshHold.get(i).getNdc());

						insertStatus= stmtInsert.executeUpdate();

						stmtInsert.close();
						if(insertStatus>0)
							flag="success";

					}
					rsThreshHold.close();
					stmThreshHold.close();
				}
				return flag;
			});
			flagValue = value!= null ? value.toString():null;
			 return flagValue;
		} finally {
		}
	}

}
