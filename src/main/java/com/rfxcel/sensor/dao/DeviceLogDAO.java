package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorRequest;

/**
 * 
 * @author sachin_kohale
 * @since May 05, 2017
 */
public class DeviceLogDAO {
	private static UserTimeZoneCache userTimeZoneCache = UserTimeZoneCache.getInstance();
	private static final Logger logger = Logger.getLogger(DeviceLogDAO.class);
	private static DeviceLogDAO deviceLogDAO;
	public static DeviceLogDAO getInstance(){
		if(deviceLogDAO == null){
			deviceLogDAO = new DeviceLogDAO();
		}
		return deviceLogDAO;
	}
	
	
	
	/**
	 * 
	 * @param deviceId
	 * @param userId
	 * @return
	 * @throws SQLException
	 * @throws ParseException 
	 */
	public ArrayList<DeviceLog> getDeviceLogs(SensorRequest sensorRequest) throws SQLException, ParseException{
		Connection conn = null;
		ArrayList<DeviceLog> deviceLogList = new ArrayList<DeviceLog>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String timeZoneOffset = userTimeZoneCache.getTimeZoneOffset(sensorRequest.getUser());
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}
			String sqlQuery = "SELECT devicelog.device_id, u.user_name, devicelog.log_msg, CONVERT_TZ(devicelog.create_time,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as create_time"
					+ " FROM device_log devicelog LEFT JOIN rfx_users u ON devicelog.user_id = u.user_id WHERE device_id =?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, sensorRequest.getDeviceLog().getDeviceId());			
			ResultSet rs = stmt.executeQuery();
			DeviceLog deviceLog = null;
			while(rs.next()){
				deviceLog = new DeviceLog(rs.getString("device_id"), rs.getString("user_name"), rs.getString("log_msg"),rs.getTimestamp("create_time"));
				deviceLogList.add(deviceLog);
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceLogList;
	}
	
	/**
	 * 
	 * @param deviceLog
	 * @throws SQLException
	 */
	public void addDeviceLog(DeviceLog deviceLog) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "INSERT INTO device_log(device_id, user_id, log_msg, create_time, org_id) VALUES(?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setString(1, deviceLog.getDeviceId());
			if( deviceLog.getUserId() == null){
				stmt.setNull(2, Types.NULL);
			}else{
			stmt.setLong(2, deviceLog.getUserId());
			}
			stmt.setString(3, deviceLog.getLogMsg());
			stmt.setString(4, Utility.getDateFormat().format(new Date()));
			stmt.setInt(5, deviceLog.getOrgId());
			stmt.executeUpdate();
			stmt.close();
		}catch(Exception e){			
			e.printStackTrace();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	public static void doAddDeviceLog(String deviceId, long userId, String logMsg, Integer orgId)
	{
		try {
			DeviceLog deviceLog = new DeviceLog();
			deviceLog.setDeviceId(deviceId);
			deviceLog.setUserId(userId);
			deviceLog.setLogMsg(logMsg);
			deviceLog.setOrgId(orgId);
			DeviceLogDAO.getInstance().addDeviceLog(deviceLog);
		} catch (SQLException e) {
			logger.error("Exception trying to addDeviceLog for device " + deviceId + ", user " + userId + ", message " + logMsg + ", org Id "+ orgId);
			e.printStackTrace();
		}
	}
	
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteDeviceLogForOrg(Connection conn, int orgId) throws SQLException {
		String query = "delete from  device_log where org_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
}
