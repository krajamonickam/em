package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.ExcursionLog;

/**
 * 
 * @author Tejshree Kachare
 * @since 26th July 2017
 *
 */
public class ExcursionLogDAO {

	private static final Logger logger = Logger.getLogger(ExcursionLogDAO.class);
	private static ExcursionLogDAO excursionLogDAO;

	public static ExcursionLogDAO getInstance(){
		if(excursionLogDAO == null){
			excursionLogDAO = new ExcursionLogDAO();
		}
		return excursionLogDAO;
	}

	/**
	 * 
	 * @param excursionLog
	 * @param sensorAttributes
	 * @throws SQLException 
	 */
	public void addExcursionLogs(ExcursionLog excursionLog, ArrayList<String> sensorAttributes) throws SQLException{
		Connection conn = null;
		try {
			PreparedStatement stmt = null;	
			conn = DAOUtility.getInstance().getConnection();
			String query = "INSERT INTO sensor_excursion_log(device_id, container_id, org_id, attribute) VALUES(?,?,?,?)";
			stmt = conn.prepareStatement(query);
			String deviceId = excursionLog.getDeviceId();
			String packageId = excursionLog.getPackageId();
			Integer orgId = excursionLog.getOrgId();
			for(String attribute : sensorAttributes){
				stmt.setString(1, deviceId);
				stmt.setString(2,packageId);
				stmt.setInt(3, orgId);
				stmt.setString(4, attribute);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close(); 
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}


	/**
	 * 
	 * @param excursionLog
	 * @param sensorAttributes
	 * @throws SQLException 
	 */
	public void removeExcursionLog(ExcursionLog excursionLog){
		Connection conn = null;
		try {
			PreparedStatement stmt = null;	
			conn = DAOUtility.getInstance().getConnection();
			String query = "DELETE FROM sensor_excursion_log where device_id=? and container_id=? and attribute=? and org_id=?";
			stmt = conn.prepareStatement(query);
			stmt.setString(1, excursionLog.getDeviceId());
			stmt.setString(2, excursionLog.getPackageId());
			stmt.setString(3, excursionLog.getAttribute());
			stmt.setInt(4, excursionLog.getOrgId());
			stmt.executeUpdate();
			stmt.close(); 
		}catch(Exception e){
			logger.error("Error while removing excursion "+e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<MultiKey, HashSet<String>> getExcursionAttr(){
		Connection conn = null;
		HashMap<MultiKey, HashSet<String>> shipmentExcursionAttr = new HashMap<MultiKey, HashSet<String>>();
		try {
			PreparedStatement stmt = null;	
			conn = DAOUtility.getInstance().getConnection();
			String query = "Select device_id, container_id, attribute from sensor_excursion_log";
			stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				MultiKey key = new MultiKey(rs.getString("device_id"), rs.getString("container_id"));
				if(shipmentExcursionAttr.containsKey(key)){
					shipmentExcursionAttr.get(key).add(rs.getString("attribute"));
				}else{
					HashSet<String> excursionAttrList = new HashSet<String>();
					excursionAttrList.add(rs.getString("attribute"));
					shipmentExcursionAttr.put(key, excursionAttrList);
				}
			}
			stmt.close(); 
		}catch(Exception e){
			logger.error("Error while fetching list of excursion attributes ");
		}finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return shipmentExcursionAttr;
	}

	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteExcursionLogForOrg(Connection conn, int orgId) throws SQLException {
		String query = "delete from  sensor_excursion_log where org_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
	
	public void deleteExcursionLogForOrgDevicePackage(Connection conn, int orgId, String deviceId, String packageId) throws SQLException {
		String query = "delete from sensor_excursion_log where org_id = ? and device_id = ? and container_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.setString(2, deviceId);
		stmt.setString(3, packageId);
		stmt.executeUpdate();
		stmt.close();
	}
}