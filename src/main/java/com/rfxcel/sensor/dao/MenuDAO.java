package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.Menu;

/**
 * 
 * @author sachin_kohale
 * @since May 15, 2017
 */
public class MenuDAO {
	private static final Logger logger = Logger.getLogger(MenuDAO.class);
	private static MenuDAO menuDAO;
	
	public static MenuDAO getInstance(){
		if(menuDAO == null){
			menuDAO = new MenuDAO();
		}
		return menuDAO;
	}
	
	/**
	 * <p>get menu list</p>
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Menu> getMenuList(int userType) throws SQLException{
		Connection conn = null;
		ArrayList<Menu> menuList = new ArrayList<Menu>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT * FROM rfx_menus where min_user_type = ?";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, userType);
			int count = 0;
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Menu menu = new Menu();
				menu.setState(rs.getString("state"));
				menu.setMenuName(rs.getString("menu_name"));
				menu.setOrder(rs.getInt("order"));
				menu.setOrgId(rs.getLong("org_id"));
				menu.setMenuCode(rs.getInt("menu_code"));
				menuList.add(menu);
				count++;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return menuList;
	}
}
