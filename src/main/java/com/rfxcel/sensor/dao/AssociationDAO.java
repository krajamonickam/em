package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;

import com.rfxcel.cache.AssociationCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.DevicePackageStatus;
import com.rfxcel.sensor.util.Utility;

public class AssociationDAO {
	private static final Logger logger = Logger.getLogger(AssociationDAO.class);
	private static AssociationDAO associationDAO;
	
	
	private static final String COMMA_DELIMITER = ",";
	private static final SendumDAO sendumDao = SendumDAO.getInstance();
	private static final ExcursionLogDAO excursionLogDAO = ExcursionLogDAO.getInstance();
	

	public static AssociationDAO getInstance() {
		if (associationDAO == null) {
			associationDAO = new AssociationDAO();
		}
		return associationDAO;
	}

	

	// To getAssociationDeviceDetails
	public List<DeviceDetail> getItemsByID(ArrayList<String> itemListDetails) throws SQLException {
		Connection conn = null;
		// Sensor sensor = new Sensor();
		List<DeviceDetail> deviceDetailList = new ArrayList<DeviceDetail>();
		try {
			for (int i = 0; i < itemListDetails.size(); i++) {// String Query =
				// "select sd.device_id,sdt.device_type,sdt.device_manufacturer
				// from sensor_devices sd,sensor_device_types sdt where
				// sd.device_type_id=sdt.device_type_id";
				String Query = "select sd.device_id,sdt.device_type,sd.device_type_id,sdt.device_manufacturer from sensor_devices sd,sensor_device_types sdt where sd.device_id like ? and sd.device_type_id=sdt.device_type_id";
				conn = DAOUtility.getInstance().getConnection();
				PreparedStatement stmt = conn.prepareStatement(Query);
				stmt.setString(1, itemListDetails.get(i) + '%');
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					DeviceDetail deviceDetail = new DeviceDetail();
					deviceDetail.setDeviceId(rs.getString("device_id"));
					deviceDetail.setDeviceType(rs.getString("device_type"));
					deviceDetail.setDeviceName(rs.getString("device_manufacturer"));
					deviceDetail.setDeviceTypeId(rs.getInt("device_type_id"));
					deviceDetailList.add(deviceDetail);
				}
				// sensor.setDeviceDetails(deviceDetailList);
				rs.close();
				stmt.close();
			}
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}

		// sensor.setItemList(itemList);
		// response.setSensor(sensor);
		return deviceDetailList;
	}

	/**
	 * <p>
	 * get productId based on prior associations of the same deviceId & packageId
	 * </p>
	 * @param deviceId
	 * @param packageId
	 * @param orgId
	 * @return productId
	 * @throws SQLException
	 */
	public String getProductIdForAssociation(String deviceId, String packageId, int orgId)
			throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT product_id from sensor_associate where parent_id =? and child_id = ? and org_id = ? order by id desc";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			stmt.setInt(3, orgId);
			ResultSet rs = stmt.executeQuery();
			String productId = null;
			if (rs.next()) {
				productId = rs.getString(1);
			}
			return(productId);
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * <p>
	 * get productId based on prior associations of the same deviceId & packageId
	 * </p>
	 * @param deviceId
	 * @param packageId
	 * @param orgId
	 * @return productId
	 * @throws SQLException
	 */
	public String getPackageIdForAssociation(String deviceId, String productId, int orgId)
			throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT parent_id from sensor_associate where  product_id = ? and child_id = ? and org_id = ? order by id desc";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, productId);
			stmt.setString(2, deviceId);
			stmt.setInt(3, orgId);
			ResultSet rs = stmt.executeQuery();
			String  packageId = null;
			if (rs.next()) {
				packageId = rs.getString(1);
			}
			return(packageId);
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	
	/**
	 * <p>
	 * check prior associations of the same productId & packageId
	 * </p>
	 * @param productId
	 * @param packageId
	 * @param orgId
	 * @return boolean
	 * @throws SQLException
	 */
	public boolean isProductAndPackagePartOfPriorShipment(String productId, String packageId, int orgId) throws SQLException {
		boolean multipleRecords = false;
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			// Check if package has active association with more than one device
			String query = "SELECT count(*) from sensor_associate WHERE parent_id = ? and product_id = ?";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, productId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int records = rs.getInt(1);
				if (records > 0) {
					multipleRecords = true;
				}
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return multipleRecords;
	}
	
	/**
	 * <p>
	 * get productId based on prior associations of the same deviceId & packageId
	 * </p>
	 * @param deviceId
	 * @param packageId
	 * @param orgId
	 * @return productId
	 * @throws SQLException
	 */
	public Integer getProfileIdForAssociation(String deviceId, String productId, String packageId, int orgId)
			throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT profile_id from sensor_associate where parent_id =? and child_id = ? and product_id = ? and org_id = ? order by id desc";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			stmt.setString(3, productId);
			stmt.setInt(4, orgId);
			ResultSet rs = stmt.executeQuery();
			Integer profileId = null;
			if (rs.next()) {
				profileId = rs.getInt(1);
			}
			return(profileId);
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	/**
	 * <p>
	 * Associate device and package and change their state
	 * </p>
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @param orgId
	 * @param groupId
	 * @param profileId
	 * @throws SQLException
	 */
	public void associateItems(String deviceId, String packageId,String productId, int orgId, long groupId, Integer profileId, String rtsItemId, String rtsSerialNumber)
			throws SQLException {
		try {
			
			DAOUtility.runInTransaction(conn -> {
				// check if record with same child and parent id is present, ie
				// device package was associated earlier, if so update the same
				// entry
				String query = "SELECT id from sensor_associate where parent_id =? and child_id = ? and org_id = ? and product_id = ?";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setString(1, packageId);
				stmt.setString(2, deviceId);
				stmt.setInt(3, orgId);
				stmt.setString(4, productId);
				ResultSet rs = stmt.executeQuery();
				int id = -1;
				while (rs.next()) {
					id = rs.getInt("id");
				}
				
				String homeLocation = null;
				if (id != -1) {// record for same device and package id exist in association table, so update the state in the same
					
					
					String updateDate  = Utility.getDateFormat().format(new Date());
					query = "UPDATE sensor_associate set relation_status=1, excursion_status =0, profile_id = ?, update_datetime=?, rts_item_id=?, rts_serial_number=? where parent_id=? and child_id=? and org_id = ? and product_id = ?";
					PreparedStatement pstmt = conn.prepareStatement(query);
					if (profileId == null) {
						pstmt.setNull(1, Types.NULL);
					} else {
						pstmt.setInt(1, profileId);
					}
					pstmt.setString(2, updateDate);
					pstmt.setString(3, rtsItemId);
					pstmt.setString(4, rtsSerialNumber);
					pstmt.setString(5, packageId);
					pstmt.setString(6, deviceId);
					pstmt.setInt(7, orgId);
					pstmt.setString(8, productId);
					pstmt.executeUpdate();
					pstmt.close();
				} else {
					query = "INSERT into sensor_associate(parent_id,child_id,create_datetime,update_datetime,relation_status,excursion_status, org_id, group_id, profile_id, product_id, rts_item_id, rts_serial_number)values(?,?,?,?,?,?,?,?,?,?,?,?)";
					PreparedStatement pstmt = conn.prepareStatement(query);
					pstmt.setString(1, packageId);
					pstmt.setString(2, deviceId);
					String createdDate  = Utility.getDateFormat().format(new Date());
					pstmt.setString(3, createdDate);
					pstmt.setString(4, createdDate);
					pstmt.setInt(5, 1); // Relationship status is associated=1
					pstmt.setInt(6, 0); // Excursion status is set to 0(Green)
					pstmt.setInt(7, orgId);
					pstmt.setLong(8, groupId);
					
					if (profileId == null) {
						pstmt.setNull(9, Types.NULL);
					} else {
						pstmt.setInt(9, profileId);
					}
					pstmt.setString(10, productId);
					pstmt.setString(11, rtsItemId);
					pstmt.setString(12, rtsSerialNumber);
					pstmt.executeUpdate();
					pstmt.close();
				}
				stmt.close();
				// Associate device and package and then change its state
				sendumDao.associateDevice(conn, deviceId);
				sendumDao.associatePackage(conn, packageId,productId, orgId);
				excursionLogDAO.deleteExcursionLogForOrgDevicePackage(conn, orgId, deviceId, packageId);
				//AssociationCache.getInstance().addShipmentHomeGeopoint(deviceId, packageId,homeLocation);
				return null;
			});
		} finally {
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @throws SQLException
	 */
	public void disAssociateItems(String deviceId, String packageId, String productId, int orgId) throws SQLException {
		try {
			DAOUtility.runInTransaction(conn -> {
				boolean recordPresent = packageHasMultipleAssociations(packageId,productId, orgId);
				// disAssociate device and package and then change its state
				sendumDao.disAssociateDevice(conn, deviceId);
												
				PreparedStatement stmt;

				if (!recordPresent) {
					sendumDao.disAssociatePackage(conn, packageId, productId, orgId);
				}
				String updateDate  = Utility.getDateFormat().format(new Date());
				String update = "UPDATE sensor_associate set relation_status=0 ,update_datetime=? where parent_id=? and child_id=? and product_id = ?";
				stmt = conn.prepareStatement(update);
				stmt.setString(1, updateDate);
				stmt.setString(2, packageId);
				stmt.setString(3, deviceId);
				stmt.setString(4, productId);
				stmt.executeUpdate();
				sendumDao.deleteTransHistory(conn, deviceId, packageId);
				sendumDao.removeBacterialGrowthLog(conn, deviceId);
				sendumDao.removeDeviceLog(conn, deviceId);
				stmt.close();
				return null;
			});
		} finally {

		}
	}

	/**
	 * 
	 * @param conn
	 * @param packageId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public boolean packageHasMultipleAssociations(String packageId,String productId, int orgId) throws SQLException {
		boolean multipleRecords = false;
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			// Check if package has active association with more than one device
			String query = "SELECT count(*) from sensor_associate WHERE parent_id = ? and product_id = ? and relation_status = 1";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, productId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				int records = rs.getInt(1);
				if (records > 1) {
					multipleRecords = true;
				}
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return multipleRecords;
	}

	/**
	 * 
	 * @param devicePackageStatus
	 * @throws SQLException
	 */
	public void updateStatus(DevicePackageStatus devicePackageStatus) throws SQLException {
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String packageId = devicePackageStatus.getPackageId();
			String deviceId = devicePackageStatus.getDeviceId();
			String productId = devicePackageStatus.getProductId();
			int status = devicePackageStatus.getStatusId();
			String updateDate  = Utility.getDateFormat().format(new Date());
			String update = "update sensor_associate set excursion_status=?, update_datetime=? where parent_id=? and child_id=? and product_id = ?";
			PreparedStatement stmt = connection.prepareStatement(update);
			stmt.setInt(1, status);
			stmt.setString(2, updateDate);
			stmt.setString(3, packageId);
			stmt.setString(4, deviceId);
			stmt.setString(5, productId);
			stmt.executeUpdate();
			AssociationCache.getInstance().putAssociationStatus(deviceId, packageId, status);

		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
	}

	/**
	 * 
	 * @return map of active shipments with deviceId as key and packageId as
	 *         value
	 */
	public HashMap<String, String> getActiveAssociations() {
		Connection conn = null;
		HashMap<String, String> associations = new HashMap<String, String>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select  parent_id, child_id from sensor_associate where relation_status = 1;";
			PreparedStatement prepStmt = conn.prepareStatement(query);
			ResultSet rs = prepStmt.executeQuery();
			while (rs.next()) {
				associations.put(rs.getString("child_id"), rs.getString("parent_id"));
			}
			rs.close();
			prepStmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting active associations  : " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return associations;
	}

	/**
	 * 
	 * @return active shipment's excursion status with deviceId-packageId
	 *         combination as key and excursion status as value
	 */
	public HashMap<MultiKey, String> getAssociationStatus() {
		Connection conn = null;
		HashMap<MultiKey, String> shipmentStatus = new HashMap<MultiKey, String>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select  parent_id, child_id,excursion_status from sensor_associate where relation_status = 1;";
			PreparedStatement prepStmt = conn.prepareStatement(query);
			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				MultiKey key = new MultiKey(rs.getString("child_id"), rs.getString("parent_id"));
				shipmentStatus.put(key, rs.getString("excursion_status"));
			}
			rs.close();
			prepStmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting association status  : " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return shipmentStatus;
	}

	/**
	 * 
	 * @param devicePackageStatus
	 * @throws SQLException
	 */
	public void updateSensorDataStatus(DevicePackageStatus devicePackageStatus) throws SQLException {
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt;
			ArrayList<String> newAttributes = devicePackageStatus.getAttributes();
			String update = "update sensor_data set excursion_status=?, excursion_attributes=? where id=?";
			stmt = connection.prepareStatement(update);
			stmt.setInt(1, devicePackageStatus.getStatusId());
			if(newAttributes != null && newAttributes.size() >0){
				StringBuilder attributes = new StringBuilder();
				for (String attribute : newAttributes) {
					attributes.append(attribute).append(COMMA_DELIMITER);
				}
				stmt.setString(2, attributes.toString());
			}else{
				stmt.setNull(2, Types.NULL);
			}
			stmt.setLong(3, devicePackageStatus.getDataRecordId());
			stmt.executeUpdate();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param pointName
	 */
	/*public void updateGeoPointHomeForAssociation(String deviceId, String containerId, String pointName) {
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt;
			String updateDate  = Utility.getDateFormat().format(new Date());
			String update = "UPDATE sensor_associate set geopoint_name=?, update_datetime = ? where parent_id=? and child_id=?";
			stmt = connection.prepareStatement(update);
			stmt.setString(1, pointName);
			stmt.setString(2, updateDate);
			stmt.setString(3, containerId);
			stmt.setString(4, deviceId);
			stmt.executeUpdate();
			stmt.close();

		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
	}*/
	/**
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	
	public HashMap<Integer, String> getShipmentsHomeGeopoint() {
		Connection conn = null;
		HashMap<Integer, String> homeLocations = new HashMap<Integer, String>();
		try
		{
			String Query = "select * from sensor_geopoints where location_type=1";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				homeLocations.put(rs.getInt("profile_id"), rs.getString("point_name"));
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e)
		{
			logger.error(e);
		}
		finally
		{
			DAOUtility.getInstance().closeConnection(conn);
		}
			return homeLocations;
	}
	
	/**
	 * 
	 * @return
	 */
	/*public HashMap<MultiKey, String> getShipmentsHomeGeopoint() {
		Connection connection = null;
		HashMap<MultiKey, String> shipmentStatus = new HashMap<MultiKey, String>();
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT parent_id, child_id, geopoint_name FROM sensor_associate WHERE geopoint_name IS NOT NULL";
			PreparedStatement prepStmt = connection.prepareStatement(query);
			ResultSet rs = prepStmt.executeQuery();

			while (rs.next()) {
				MultiKey key = new MultiKey(rs.getString("child_id"), rs.getString("parent_id"));
				shipmentStatus.put(key, rs.getString("geopoint_name"));
			}
			prepStmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting Shipment HomeGeopoint : " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
		return shipmentStatus;
	}*/

	/**
	 * 
	 * @param searchString
	 * @return
	 */
	/*public ArrayList<String> searchHomeGeopoints(String searchString) {
		ArrayList<String> result = new ArrayList<String>();
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT distinct child_id, geopoint_name FROM sensor_associate WHERE geopoint_name Like ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, searchString + "%");
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				result.add(rs.getString("geopoint_name") + "-" + rs.getString("child_id"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting Shipment HomeGeopoint : " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return result;
	}*/

	/**
	 * 
	 * @param deviceId
	 * @return
	 */
	// Tejshree :- Not referenced any where
	/*public Integer getProfileIdForDeviceId(String deviceId) {
		Integer profileId = null;
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT profile_id FROM sensor_associate WHERE child_id=? and relation_status=0 ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				profileId = rs.getInt("profile_id");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting getProfileId for DeviceId  : " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profileId;

	}*/

	/**
	 * 
	 * @return
	 */
	public Map<MultiKey, Integer> getActiveShipmentProfileMapping() {
		Connection conn = null;
		Map<MultiKey, Integer> shipmentProfileMapping = new HashMap<MultiKey, Integer>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT child_id, parent_id, profile_id FROM sensor_associate WHERE relation_status=1 ";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			MultiKey key;
			while (rs.next()) {
				key = new MultiKey(rs.getString("child_id"), rs.getString("parent_id"));
				shipmentProfileMapping.put(key, rs.getInt("profile_id"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting Shipment Profile Mapping: " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return shipmentProfileMapping;

	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @return
	 */
	public Integer getProfileIdForShipment(String deviceId, String packageId, String productId) {
		Connection conn = null;
		Integer profileId = null ;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT profile_id FROM sensor_associate WHERE child_id = ? and parent_id = ? and product_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, packageId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				profileId = rs.getInt("profile_id");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getProfileIdForShipment: " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profileId;

	}
	
	
	

	/**
	 * <p>
	 * Dis associate all the shipments for an org
	 * </p>
	 * 
	 * @param orgId
	 */
	public void DisassociateShipmentsForOrg(Connection conn, Integer orgId) throws SQLException {
		try {
			String updateDate  = Utility.getDateFormat().format(new Date());
			String update = "UPDATE sensor_associate set child_id = CONCAT(child_id,'_DELETED_' , UNIX_TIMESTAMP()), parent_id = CONCAT(parent_id,'_DELETED_' , UNIX_TIMESTAMP()),"
					+ " relation_status=1, update_datetime = ? where org_id=? ";
			PreparedStatement stmt;
			stmt = conn.prepareStatement(update);
			stmt.setString(1, updateDate);
			stmt.setInt(2, orgId);
			stmt.executeUpdate();
			stmt.close();
		} finally{
		}
	}


	/**
	 * @return
	 */
	public Map<Integer, Integer> getProfileTempFreqMapping() {
		Connection conn = null;
		Map<Integer, Integer> ProfileTempFreqMapping = new HashMap<Integer, Integer>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT profile_id, attr_freq FROM sensor_frequency where attr_freq IS NOT NULL ";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				ProfileTempFreqMapping.put(rs.getInt("profile_id"), rs.getInt("attr_freq"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting Profile TempFreq Mapping: " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return ProfileTempFreqMapping;
	}
	
	
	/**
	 * @param deviceId
	 * @param packageId
	 * @return
	 * @throws SQLException 
	 */
	public Boolean checkIfAssociationIsActive(String deviceId, String packageId, String productId) throws SQLException{
		Connection conn = null;
		Boolean active = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT relation_status FROM sensor_associate where parent_id =? and child_id = ? and product_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				active = rs.getBoolean("relation_status");
			}
			rs.close();
			stmt.close();
		}finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return active;
	
	}



	public void updateShipmentProfile(String deviceId, String packageId, Integer profileId) throws SQLException {
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String updateDate  = Utility.getDateFormat().format(new Date());
			String update = "update sensor_associate set profile_id=?, update_datetime=? where parent_id=? and child_id=?";
			PreparedStatement stmt = connection.prepareStatement(update);
			stmt.setInt(1, profileId);
			stmt.setString(2, updateDate);
			stmt.setString(3, packageId);
			stmt.setString(4, deviceId);
			stmt.executeUpdate();
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
	}

	
	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws SQLException 
	 */
	public HashMap<String, String> getAssociationForProfile(Integer profileId) throws SQLException{
		Connection connection = null;
		HashMap<String,String> association = new HashMap<String, String>();
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT parent_id,child_id FROM sensor_associate where profile_id=? and relation_status =1 ";
			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				association.put(rs.getString("child_id"), rs.getString("parent_id"));
			}
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
		return association;
	}
	
	
	/**
	 * 
	 * @param deviceTypeId
	 * @return 
	 */
	public ArrayList<String> getAssociatedDevicesWithDeviceTypeId(Integer deviceTypeId) {
		Connection conn = null;
		ArrayList<String> deviceIds = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			deviceIds = new ArrayList<String>();
			String query = "select child_id from sensor_associate sa inner join sensor_devices sd"
					+ " on sa.child_id=sd.device_id where sa. relation_status=1 and sd.device_type_id=?";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setInt(1, deviceTypeId);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					deviceIds.add(rs.getString("child_id"));
				}
				rs.close();
				stmt.close();	
		} catch (SQLException e) {
			logger.error("Error while associated devices with deviceType Id "+deviceTypeId + " : "+e.getMessage());
			e.printStackTrace();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceIds;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @return
	 * @throws SQLException
	 */
	public String getProductIdForActiveAssociation(String deviceId, String packageId) throws SQLException{
		Connection connection = null;
		String productId = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT product_id FROM sensor_associate where parent_id = ? and child_id = ? and relation_status =1 ";
			PreparedStatement stmt = connection.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				productId = rs.getString("product_id");
			}
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
		return productId;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 */
	public void updateShipmentHomeEntry(String deviceId, String packageId, String productId ){
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String updateDate  = Utility.getDateFormat().format(new Date());
			String update = "update sensor_associate set left_home=?, update_datetime=? where parent_id=? and child_id=? and product_id=? ";
			PreparedStatement stmt = connection.prepareStatement(update);
			stmt.setInt(1, 1);
			stmt.setString(2, updateDate);
			stmt.setString(3, packageId);
			stmt.setString(4, deviceId);
			stmt.setString(5, productId);
			stmt.executeUpdate();
		} catch (SQLException e) {
			logger.error("Error while updating left_home for shipment ");
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
	}
	
	/**
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @return
	 */
	public Boolean getShipmentHomeEntryState(String deviceId, String packageId, String productId ){
		Boolean shipmentLeft = false;
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String update = "SELECT left_home from sensor_associate where parent_id=? and child_id=? and product_id=? and relation_status =1 ";
			PreparedStatement stmt = connection.prepareStatement(update);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				shipmentLeft = rs.getBoolean("left_home");
			}
		} catch (SQLException e) {
			logger.error("Error while fetching getShipmentHomeEntryState " +e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
		return shipmentLeft;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @return
	 */
	public Integer getShipmentExcursionStatus(String deviceId, String packageId, String productId ){
		Integer excursionStatus = 0;
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String update = "SELECT excursion_status from sensor_associate where parent_id=? and child_id=? and product_id=? ";
			PreparedStatement stmt = connection.prepareStatement(update);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				excursionStatus = rs.getInt("excursion_status");
			}
		} catch (SQLException e) {
			logger.error("Error while fetching getShipmentExcursionStatus " +e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(connection);
		}
		return excursionStatus;
	}	
	
	/**
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfProfileIsAssociated(Integer profileId) throws SQLException {
		Connection conn = null;
		Boolean isAssociated = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT EXISTS (SELECT 1 from sensor_associate where profile_id = ? and relation_status = 1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count != 0) {
				isAssociated = true;
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isAssociated;
	}
	
}