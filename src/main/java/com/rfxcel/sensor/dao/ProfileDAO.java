package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.notification.dao.AlertDAO;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.notification.entity.Alert;
import com.rfxcel.rule.service.RuleCache;
import com.rfxcel.rule.util.RuleConstants;
import com.rfxcel.sensor.beans.Geopoint;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.ProfileRequest;

/**
 @author Vijay kumar
 */
public class ProfileDAO {
	private static ProfileDAO profileDAO;
	
	private static final Logger logger = Logger.getLogger(ProfileDAO.class);
	private static final ConfigCache cache = ConfigCache.getInstance();
	private static final AlertDAO alertDao = AlertDAO.getInstance();
	public static ProfileDAO getInstance() {
		if (profileDAO == null) {
			profileDAO = new ProfileDAO();
		}
		return profileDAO;
	}

/**
 * <p> To get all profiles </p>
 * @return
 * @throws SQLException
 */
	
	public List<Profile> getListProfiles(Profile profile)	throws SQLException {
		Connection conn = null;
		List<Profile> profileList = new ArrayList<Profile>();
		try
		{
			String Query = "select * from sensor_profile where org_id = ? and active=1";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setInt(1, profile.getOrgId());
//			stmt.setLong(2,profile.getGroupId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Profile profiles = new Profile();
				profiles.setProfileId(rs.getInt("id"));
				profiles.setName(rs.getString("name"));
				profiles.setDescription(rs.getString("description"));
				profiles.setDeviceTypeId(rs.getInt("device_type_id"));
				profiles.setOrgId(rs.getInt("org_id"));
				profiles.setGroupId(rs.getLong("group_id"));
				profiles.setActive(rs.getBoolean("active"));
				profileList.add(profiles);
			}
			rs.close();
			stmt.close();
		}
		finally
		{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profileList;

	}
	
	/**
	 * <p> To get all profiles </p>
	 * @return
	 * @throws SQLException
	 */
		
		public List<Profile> getListProfiles()	throws SQLException {
			Connection conn = null;
			List<Profile> profileList = new ArrayList<Profile>();
			try
			{
				String Query = "select * from sensor_profile where active=1";
				conn = DAOUtility.getInstance().getConnection();
				PreparedStatement stmt = conn.prepareStatement(Query);
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					Profile profile = new Profile();
					profile.setProfileId(rs.getInt("id"));
					profile.setName(rs.getString("name"));
					profile.setDescription(rs.getString("description"));
					profile.setDeviceTypeId(rs.getInt("device_type_id"));
					profile.setOrgId(rs.getInt("org_id"));
					profile.setGroupId(rs.getLong("group_id"));
					profile.setActive(rs.getBoolean("active"));
					profileList.add(profile);
				}
				rs.close();
				stmt.close();
			}
			finally
			{
				DAOUtility.getInstance().closeConnection(conn);
			}
			return profileList;

		}
	/**
	 * <p> To get profiles by deviceTypeID </p>
	 * @return
	 * @throws SQLException
	 */
		
	public List<Profile> getProfilesByDeviceType(Profile profile,String deviceType)throws SQLException {
		Connection conn = null;
		List<Profile> profileList = new ArrayList<Profile>();
		try
		{
			String Query = "select sp.id, sp.name, sp.active from sensor_profile sp JOIN sensor_device_types sd ON sp.device_type_id = sd.device_type_id"
							+ " where sd.device_type =? and sp.org_id=? and sp.active=1";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setString(1,deviceType);
			stmt.setInt(2, profile.getOrgId());
//			stmt.setLong(3, profile.getGroupId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Profile profiles = new Profile();
				profiles.setProfileId(rs.getInt("id"));
				profiles.setName(rs.getString("name"));
				profiles.setActive(rs.getBoolean("active"));
				profileList.add(profiles);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e)
		{
			logger.error("Error while getting profile by device type. Error message is"+e.getMessage());
		}
		finally
		{
			DAOUtility.getInstance().closeConnection(conn);
		}
			return profileList;
	}
	/**
	 * <p> Create Profile with inserting threshold and geopoint data</p>
	 * @return
	 * @throws SQLException
	 */		
	public Integer createProfile(Profile profile) throws SQLException {		
		Integer rowInsertedValue = 0;
		try {
			Object value = DAOUtility.runInTransaction(conn->{
				PreparedStatement stmt = null;	
				Integer profileId = -1;
				Integer orgId = profile.getOrgId();
				String profileQuery = "INSERT INTO sensor_profile(name, description, device_type_Id, org_id, group_id) VALUES(?,?,?,?,?)";
				stmt = conn.prepareStatement(profileQuery, Statement.RETURN_GENERATED_KEYS);
				stmt.setString(1, profile.getName());
				stmt.setString(2, profile.getDescription());
				stmt.setInt(3, profile.getDeviceTypeId());
				stmt.setInt(4, orgId );
				stmt.setLong(5, profile.getGroupId());
				stmt.executeUpdate();

				ResultSet rs=stmt.getGeneratedKeys();
				if (rs.next()) {
					profileId = rs.getInt(1);
				}
				rs.close();
				stmt.close();    //close stmt of profileQuery

				if (profileId > 0){
					//inserting sensor_threshold 				
					String thresholdQuery = "INSERT INTO sensor_threshold(profile_id, attr_name, min_value, max_value, alert, device_type_id, org_id) VALUES(?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(thresholdQuery);
					for(Attribute attrType : profile.getAttrType()){
						stmt.setInt(1, profileId);
						stmt.setString(2, attrType.getName());
						stmt.setDouble(3, attrType.getMinValue());
						stmt.setDouble(4, attrType.getMaxValue());
						stmt.setInt(5, attrType.getAlert());
						stmt.setInt(6, profile.getDeviceTypeId());
						stmt.setInt(7, orgId);
						stmt.addBatch();
					}
					stmt.executeBatch();
					stmt.close();    //close stmt of thresholdQuery

					//inserting sensor_geopoints
					String geopointQuery = "INSERT INTO sensor_geopoints(profile_id,geofence_number,point_name,latitude, longitude, address, radius,location_type,addr1,addr2,city,state,zip,country) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(geopointQuery);
					for(Geopoint geopoint : profile.getGeopoints()){
						stmt.setInt(1, profileId);
						stmt.setLong(2, System.nanoTime());
						stmt.setString(3, geopoint.getPointName());
						stmt.setString(4, geopoint.getLatitude());
						stmt.setString(5, geopoint.getLongitude());
						stmt.setString(6, geopoint.getAddress());
						stmt.setInt(7, geopoint.getRadius());
						stmt.setInt(8, geopoint.getLocationType());
						stmt.setString(9, geopoint.getAddr1());
						stmt.setString(10, geopoint.getAddr2());
						stmt.setString(11, geopoint.getCity());
						stmt.setString(12, geopoint.getState());
						stmt.setString(13, geopoint.getZip());
						stmt.setString(14, geopoint.getCountry());
						stmt.addBatch();					
					}
					stmt.executeBatch();
					stmt.close();    //close stmt of geopointQuery

					//inserting sensor_frequency
					String frequencyQuery = "INSERT INTO sensor_frequency(profile_id,common_freq,gps_freq,attr_freq, org_id, group_id,common_freq_unit, gps_freq_unit, attr_freq_unit) VALUES(?,?,?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(frequencyQuery);
					stmt.setInt(1, profileId);
					if (profile.getCommFrequency() == null ) {
						stmt.setNull(2, Types.NULL);
					} else {
						stmt.setInt(2, profile.getCommFrequency());
					}

					if ( profile.getGpsFrequency() == null ) {
						stmt.setNull(3, Types.NULL);
					} else {
						stmt.setInt(3, profile.getGpsFrequency());
					}

					if ( profile.getAttbFrequency() == null ) {
						stmt.setNull(4, Types.NULL);
					} else {
						stmt.setInt(4, profile.getAttbFrequency());
					}

					stmt.setInt(5, orgId);
					stmt.setLong(6, profile.getGroupId());
					stmt.setInt(7, profile.getCommFrequencyUnit());
					stmt.setInt(8, profile.getGpsFrequencyUnit());
					stmt.setInt(9, profile.getAttbFrequencyUnit());
					stmt.executeUpdate();
					stmt.close();    //close stmt of frequencyQuery
				}
				// org level config for new Profile screen is set to true
				boolean flag = cache.getOrgConfig("new.profile.screen", orgId)== null ? false : Boolean.valueOf(cache.getOrgConfig("new.profile.screen", orgId));
				if(flag){
					// insert into AlertGroup Mapping table
					Alert alert ;
					for(Attribute attrType : profile.getAttrType()){
						alert = new Alert();
						alert.setAlertGroupIds(attrType.getAlertGroupIds());
						alert.setAlertCode(attrType.getAlertCode());
						alert.setAlertDetailMsg(attrType.getAlertDetailMsg());
						alert.setAttrName(attrType.getName());
						alert.setEmailIds(attrType.getEmailIds());
						alert.setOrgId(orgId);
						alertDao.addProfileAlertGroupMapping(alert,profileId, conn);
						alertDao.addProfileAlertDetailMsg(alert,profileId, conn);
						alertDao.addProfileAlertEmailIdMapping(alert, profileId, conn);
					}
					// iterating over additional alerts configured
					for(Attribute additionalAlerts: profile.getAdditionalAlerts()){
						alert = new Alert();
						alert.setAlertGroupIds(additionalAlerts.getAlertGroupIds());
						alert.setAlertCode(additionalAlerts.getAlertCode());
						alert.setAlertDetailMsg(additionalAlerts.getAlertDetailMsg());
						alert.setAttrName(additionalAlerts.getName());
						alert.setEmailIds(additionalAlerts.getEmailIds());
						alert.setOrgId(orgId);
						alertDao.addProfileAlertGroupMapping(alert,profileId, conn);
						alertDao.addProfileAlertDetailMsg(alert,profileId, conn);
						alertDao.addProfileAlertEmailIdMapping(alert, profileId, conn);
					}
				}
				return profileId;
			});
			rowInsertedValue = value!= null ? Integer.valueOf(value.toString()):null;
			return rowInsertedValue;
		} finally {
		}
	}

	public Profile getProfileByProfileId(Integer profileId){
		Connection conn = null;
		Profile profile = new Profile();
		try
		{
			String Query = "select id, name, description from sensor_profile where id=?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				profile.setProfileId(profileId);
				profile.setName(rs.getString("name"));
				profile.setDescription(rs.getString("description"));
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("error while fetching profile details for profileId "+profileId);
			e.printStackTrace();
		}
		finally
		{
			DAOUtility.getInstance().closeConnection(conn);
		}
			return profile;
	}
	
	/**
	 * 
	 * @return Boolean value 
	 * @throws SQLException 
	 */
	public Boolean checkIfProfileNameIsPresent(String profileName, Integer orgId) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 FROM sensor_profile WHERE name = ? and active=1 and org_id=?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, profileName);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count != 0){
				isPresent = true;
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * 
	 * @param profile
	 * @return Boolean value
	 * @throws SQLException
	 */
	public Boolean updateProfile(Profile profile) throws SQLException{
		Boolean updateStatus = false;
		try {
			Object value = DAOUtility.runInTransaction(conn->{
				PreparedStatement stmt = null;
				int rowUpdated = -1;
				Boolean status = false;
				Integer orgId = profile.getOrgId();
				Integer profileId = profile.getProfileId();
				String profileQuery = "UPDATE sensor_profile SET description=?, device_type_Id=?, org_id=?, group_id=?,name=?, updated_time=NOW() WHERE id=?";
				stmt = conn.prepareStatement(profileQuery);
				stmt.setString(1, profile.getDescription());
				stmt.setInt(2, profile.getDeviceTypeId());
				stmt.setInt(3, orgId);
				stmt.setLong(4, profile.getGroupId());
				stmt.setString(5, profile.getName());
				stmt.setInt(6, profileId);
				rowUpdated = stmt.executeUpdate();			

				stmt.close();    //close stmt of profileQuery

				if(rowUpdated > 0){
					//delete existing threshold records by profileId
					String deleteThreshold = "DELETE FROM sensor_threshold WHERE profile_id = ?";
					stmt = conn.prepareStatement(deleteThreshold);
					stmt.setInt(1, profile.getProfileId());
					stmt.executeUpdate();
					stmt.close();    //close stmt of deleteThreshold

					//inserting threshold records by profileId							
					String thresholdQuery = "INSERT INTO sensor_threshold(profile_id, attr_name, min_value, max_value, alert,  device_type_id,org_id) VALUES(?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(thresholdQuery);
					for(Attribute attrType : profile.getAttrType()){
						stmt.setInt(1, profileId);
						stmt.setString(2, attrType.getName());
						stmt.setDouble(3, attrType.getMinValue());
						stmt.setDouble(4, attrType.getMaxValue());
						stmt.setInt(5, attrType.getAlert());
						stmt.setInt(6, profile.getDeviceTypeId());
						stmt.setInt(7, orgId);
						stmt.addBatch();
					}
					stmt.executeBatch();
					stmt.close();    //close stmt of thresholdQuery

					//delete existing geoppint records by profileId
					String deleteGeopoint = "DELETE FROM sensor_geopoints WHERE profile_id = ?";
					stmt = conn.prepareStatement(deleteGeopoint);
					stmt.setInt(1, profileId);
					stmt.executeUpdate();
					stmt.close();    //close stmt of deleteGeopoint

					//inserting geopoint records by profileId
					String geopointQuery = "INSERT INTO sensor_geopoints(profile_id,geofence_number,point_name,latitude, longitude, address, radius,location_type,addr1,addr2,city,state,zip,country) VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(geopointQuery);
					for(Geopoint geopoint : profile.getGeopoints()){
						stmt.setInt(1, profileId);
						stmt.setString(2, geopoint.getPointNumber()==null || geopoint.getPointNumber()==""?String.valueOf(System.nanoTime()):geopoint.getPointNumber() );
						stmt.setString(3, geopoint.getPointName());
						stmt.setString(4, geopoint.getLatitude());
						stmt.setString(5, geopoint.getLongitude());
						stmt.setString(6, geopoint.getAddress());
						stmt.setInt(7, geopoint.getRadius());
						stmt.setInt(8, geopoint.getLocationType());
						stmt.setString(9, geopoint.getAddr1());
						stmt.setString(10, geopoint.getAddr2());
						stmt.setString(11, geopoint.getCity());
						stmt.setString(12, geopoint.getState());
						stmt.setString(13, geopoint.getZip());
						stmt.setString(14, geopoint.getCountry());
						stmt.addBatch();		
					}
					stmt.executeBatch();
					stmt.close();     //close stmt of geopointQuery	

					//delete existing geoppint records by profileId
					String deleteFrequency = "DELETE FROM sensor_frequency WHERE profile_id = ?";
					stmt = conn.prepareStatement(deleteFrequency);
					stmt.setInt(1, profileId);
					stmt.executeUpdate();
					stmt.close();    //close stmt of deleteGeopoint

					//inserting sensor_frequency
					String frequencyQuery = "INSERT INTO sensor_frequency(profile_id,common_freq,gps_freq,attr_freq, org_id, group_id,common_freq_unit, gps_freq_unit, attr_freq_unit) VALUES(?,?,?,?,?,?,?,?,?)";
					stmt = conn.prepareStatement(frequencyQuery);
					stmt.setInt(1,profileId);
					if (profile.getCommFrequency() == null ) {
						stmt.setNull(2, Types.NULL);
					} else {
						stmt.setInt(2, profile.getCommFrequency());
					}
					if ( profile.getGpsFrequency() == null ) {
						stmt.setNull(3, Types.NULL);
					} else {
						stmt.setInt(3, profile.getGpsFrequency());
					}
					if ( profile.getAttbFrequency() == null ) {
						stmt.setNull(4, Types.NULL);
					} else {
						stmt.setInt(4, profile.getAttbFrequency());
					}
					stmt.setInt(5, orgId);
					stmt.setLong(6, profile.getGroupId());
					stmt.setInt(7, profile.getCommFrequencyUnit());
					stmt.setInt(8, profile.getGpsFrequencyUnit());
					stmt.setInt(9, profile.getAttbFrequencyUnit());
					stmt.executeUpdate();
					stmt.close();    //close stmt of frequencyQuery

					// org level config for new Profile screen is set to true
					boolean flag = cache.getOrgConfig("new.profile.screen", orgId)== null ? false : Boolean.valueOf(cache.getOrgConfig("new.profile.screen", orgId));
					if(flag){
						alertDao.deleteProfileAlertGroupMapping(profileId, conn);
						alertDao.deleteProfileAlertDetailMsg(profileId, conn);
						alertDao.deleteProfileAlertEmailIdMapping(profileId, conn);

						// insert into AlertGroup Mapping table
						Alert alert ;
						for(Attribute attrType : profile.getAttrType()){
							alert = new Alert();
							alert.setAlertGroupIds(attrType.getAlertGroupIds());
							alert.setAlertCode(attrType.getAlertCode());
							alert.setAlertDetailMsg(attrType.getAlertDetailMsg());
							alert.setAttrName(attrType.getName());
							alert.setEmailIds(attrType.getEmailIds());
							alert.setOrgId(orgId);
							alertDao.addProfileAlertGroupMapping(alert,profileId, conn);
							alertDao.addProfileAlertDetailMsg(alert,profileId, conn);
							alertDao.addProfileAlertEmailIdMapping(alert, profileId, conn);
						}
						// iterating over geopoint alerts configured
						for(Attribute geopointAttr: profile.getAdditionalAlerts()){
							alert = new Alert();
							alert.setAlertGroupIds(geopointAttr.getAlertGroupIds());
							alert.setAlertCode(geopointAttr.getAlertCode());
							alert.setAlertDetailMsg(geopointAttr.getAlertDetailMsg());
							alert.setAttrName(geopointAttr.getName());
							alert.setEmailIds(geopointAttr.getEmailIds());
							alert.setOrgId(orgId);
							alertDao.addProfileAlertGroupMapping(alert,profileId, conn);
							alertDao.addProfileAlertDetailMsg(alert,profileId, conn);
							alertDao.addProfileAlertEmailIdMapping(alert, profileId, conn);
						}
					}
					status = true;
				}		
				return status;
			});
			updateStatus = value!=null ? Boolean.valueOf(value.toString()):null;
			return updateStatus;
		}finally {
		}
	}
	
	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfActiveProfileIsPresent(int profileId) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT EXISTS(SELECT 1 FROM sensor_profile WHERE id = ? and active = 1 )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count != 0){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * 
	 * @param profileId
	 * @throws SQLException
	 */
	public void deleteProfile(int profileId) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_profile SET active = 0 WHERE id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, profileId);
			stmt.executeUpdate();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	/**
	 * <p> To get profiles by deviceTypeID </p>
	 * @return
	 * @throws SQLException
	 */
		
	public Profile getProfile(ProfileRequest request)throws SQLException {
		Connection conn = null;
		Profile profile = new Profile();
		try
		{
			String query = "";
			PreparedStatement stmt = null;
			Integer orgId = request.getProfile().getOrgId();
			Integer profileId = request.getProfile().getProfileId();
			boolean flag = cache.getOrgConfig("new.profile.screen", orgId)== null ? false : Boolean.valueOf(cache.getOrgConfig("new.profile.screen", orgId));
			
			if(orgId > 0){
				conn = DAOUtility.getInstance().getConnection();
				query = "select * from sensor_profile sp, sensor_frequency sf where sp.id=? and sf.profile_id=? and "
						+ "sp.org_id=? and sf.org_id=? and sp.id=sf.profile_id and active=1";
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, profileId);
				stmt.setInt(2, profileId);
				stmt.setDouble(3, orgId);
				stmt.setDouble(4, orgId);
			}else{
				conn = DAOUtility.getInstance().getConnection();
				query = "select * from sensor_profile sp, sensor_frequency sf where sp.id=? and sf.profile_id=? and "
						+ "sp.id=sf.profile_id and active=1";
				stmt = conn.prepareStatement(query);
				stmt.setInt(1, profileId);
				stmt.setInt(2, profileId);
			}
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				profile.setProfileId(rs.getInt("id"));
				profile.setName(rs.getString("name"));

				profile.setDescription(rs.getString("description"));
				profile.setDeviceTypeId(rs.getInt("device_type_id"));
				profile.setOrgId(rs.getInt("org_id"));
				profile.setGroupId(rs.getLong("group_id"));
				profile.setAttrType(getAttributesForDeviceTypes(request));
				profile.setGeopoints(getProfileGeoPoints(request));
				profile.setCommFrequency(rs.getInt("common_freq"));
				profile.setCommFrequencyUnit(rs.getInt("common_freq_unit"));
				profile.setGpsFrequency(rs.getInt("gps_freq"));
				profile.setGpsFrequencyUnit(rs.getInt("gps_freq_unit"));
				profile.setAttbFrequency(rs.getInt("attr_freq"));
				profile.setAttbFrequencyUnit(rs.getInt("attr_freq_unit"));
				
				
				if(flag){
					profile.setAdditionalAlerts(getAdditionalAlerts(profileId));
				}
			}
			rs.close();

			// get the list of attributes associated mapped to a device_type, required for thingspace integration
			query = "select attr_name from sensor_attr_type where device_type_id = ? and sensor_attribute = 1";
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, profile.getDeviceTypeId());
			ResultSet resultSet = stmt.executeQuery();
			String attrName;
			ArrayList<String> attrNames = new ArrayList<String>();
			while(resultSet.next()){
				attrName = resultSet.getString("attr_name");
				attrNames.add(attrName);
			}
			resultSet.close();
			stmt.close();
			profile.setAttributes(attrNames);
		}
		catch(Exception e)
		{
			logger.error("Error while getting profile. Error message is"+e.getMessage());
		}
		finally
		{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profile;
	}
	
	
	public List<Attribute> getAttributesForDeviceTypes(ProfileRequest request){
		{
			//----getting threshold and geo points values
			Connection conn = null;
			List<Attribute> attrTypeList = new LinkedList<Attribute>() ;
			try{
				Integer orgId = request.getProfile().getOrgId();
				Integer profileId = request.getProfile().getProfileId();

				String getAttrQuery="select attr.attr_name, attr.legend, attr.device_type_id, thr.min_value, thr.max_value, thr.alert, attr.range_min,"
						+ " attr.range_max, attr.unit from sensor_attr_type attr left join"
						+ " (select attr_name, min_value, max_value, alert, device_type_id from sensor_threshold where profile_id =? ) as "
						+ "thr on (attr.attr_name=thr.attr_name and attr.device_type_id = thr.device_type_id ) where "
						+ "attr.device_type_id =(select sp.device_type_id from sensor_profile sp where sp.id=?) and attr.org_id =? and attr.scope=0 order by attr.attr_name";
				conn = DAOUtility.getInstance().getConnection();
				PreparedStatement stmt = conn.prepareStatement(getAttrQuery);
				stmt=conn.prepareStatement(getAttrQuery);
				stmt.setInt(1, profileId);
				stmt.setInt(2, profileId);
				stmt.setInt(3, orgId);
				ResultSet rs = stmt.executeQuery();
				int deviceTypeId;
				String attrName;
				// org level config for new Profile screen is set to true
				boolean flag = cache.getOrgConfig("new.profile.screen", orgId)== null ? false : Boolean.valueOf(cache.getOrgConfig("new.profile.screen", orgId));
				while (rs.next()) {
					attrName = rs.getString("attr_name");
					Attribute attr = new Attribute();
					attr.setName(attrName);
					attr.setLegend(Utility.getCamelCaseValue(rs.getString("attr_name")));
					deviceTypeId = rs.getInt("device_type_id") ;
					attr.setDeviceTypeId(deviceTypeId);
					attr.setMinValue(rs.getObject("min_value") != null? rs.getDouble("min_value") :0);
					attr.setMaxValue(rs.getObject("max_value") != null? rs.getDouble("max_value") :0);
					attr.setAlert(rs.getInt("alert"));
					attr.setRangeMin(rs.getDouble("range_min"));
					attr.setRangeMax(rs.getDouble("range_max"));
					attr.setUnit(rs.getString("unit"));
					attr.setAlertCode(RuleConstants.ATTRIBUTE_ALERT_MAP.get(attrName));
					if(flag){
						attr.setAlertGroupIds(alertDao.getProfileAttrAlertGroupIds(attrName,profileId));
						attr.setEmailIds(alertDao.getProfileAttrAlertEmailIds(attrName,profileId));
						attr.setAlertDetailMsg(alertDao.getProfileAttrAlertMessage(attrName,profileId));
					}
					attrTypeList.add(attr);

				}
				rs.close();
				stmt.close();
			}catch(Exception e){
				logger.error("Error while getting device type adn attribute mapping. Error message is"+e.getMessage());
			}finally{
				DAOUtility.getInstance().closeConnection(conn);
			}
			return attrTypeList;
		}
	}
	
	
	public List<Geopoint> getProfileGeoPoints(ProfileRequest request)throws SQLException {
		Connection conn = null;
		List<Geopoint> geopointList = new LinkedList<Geopoint>();
		try
		{
			String Query = "select * from sensor_geopoints where profile_id=?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setInt(1, request.getProfile().getProfileId());
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Geopoint geoPoint = new Geopoint();
				geoPoint.setPointName(rs.getString("point_name"));
				geoPoint.setPointNumber(rs.getString("geofence_number"));
				geoPoint.setLatitude(rs.getString("latitude"));
				geoPoint.setLongitude(rs.getString("longitude"));
				geoPoint.setAddress(rs.getString("address"));
				geoPoint.setRadius(rs.getInt("radius"));
				geoPoint.setLocationType(rs.getInt("location_type"));
				geoPoint.setAddr1(rs.getString("addr1"));
				geoPoint.setAddr2(rs.getString("addr2"));
				geoPoint.setCity(rs.getString("city"));
				geoPoint.setState(rs.getString("state"));
				geoPoint.setZip(rs.getString("zip"));
				geoPoint.setCountry(rs.getString("country"));
				
				geopointList.add(geoPoint);
			}
			rs.close();
			stmt.close();
		}
		catch(Exception e)
		{
			logger.error("Error while getting geopoints for a profiel in method getProfileGeoPoints(). Error message is"+e.getMessage());
		}
		finally
		{
			DAOUtility.getInstance().closeConnection(conn);
		}
			return geopointList;
	}
	
	/**
	 * <p> Deactivate profiles for an org
	 * @param orgId
	 * @throws SQLException 
	 */
	public void deactivateProfilesForOrg(Connection conn,Integer orgId) throws SQLException {
			String query = "Update sensor_profile set active=0 where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			stmt.executeUpdate();
			stmt.close();
			ArrayList<Integer> profileIds = getProfilesForOrg(orgId);
			//deleting profile geopoints from table
			deleteProfileGeoPoints(conn, profileIds);
			//removing profile  geopoints from cache
			DeviceCache.getInstance().removeProfileGeoPoints(profileIds);
			AssociationCache.getInstance().removeShipmentHomeGeopoint(profileIds);
			AssociationCache.getInstance().removeProfileTempFrequency(profileIds);
			//deleting sensor thresholds from table
			deleteSensorThresholdForOrg(conn, orgId);
			//removing rules from cache
			RuleCache.getInstance().removeRules(profileIds);
			//deleting sensor frequencies from table
			deleteSensorFrequencyForOrg(conn, orgId);
	}

	
	/**
	 * <p> get list of profiles defined for an org
	 * @param orgId
	 * @return
	 * @throws SQLException 
	 */
	public ArrayList<Integer> getProfilesForOrg(Integer orgId) throws SQLException {
		Connection conn = null;
		ArrayList<Integer> profileIds = new ArrayList<Integer>();
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT id from sensor_profile where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				profileIds.add(rs.getInt("id"));
			}
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profileIds;
	}
	
	
	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Attribute> getAdditionalAlerts(Integer profileId) throws SQLException {
		Connection conn = null;
		ArrayList<Attribute> attributes = new ArrayList<Attribute>();
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT alert_detail_msg, alert_code, attr_name from profile_alert_types where alert_code IN (3702, 3701) and profile_id =? ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, profileId);
			ResultSet rs = stmt.executeQuery();
			Long alertCode;
			Attribute attribute;
			while(rs.next()){
				attribute = new Attribute();
				alertCode = rs.getLong("alert_code");
				attribute.setName(rs.getString("attr_name"));
				attribute.setAlertDetailMsg(rs.getString("alert_detail_msg"));
				attribute.setAlertCode(alertCode);
				attribute.setEmailIds(alertDao.getProfileAttrAlertEmailIds(alertCode, profileId));
				attribute.setAlertGroupIds(alertDao.getProfileAttrAlertGroupIds(alertCode, profileId));
				attributes.add(attribute);
			}
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return attributes;
	}
	
	/**
	 * 
	 * @param orgId 
	 * @return Boolean value 
	 * @throws SQLException 
	 */
	public Boolean checkProfileNameIsPresentforModify(String profileName,int profileId, Integer orgId) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 FROM sensor_profile WHERE name = ? and id != ? and active=1 and org_id =?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, profileName);
			stmt.setInt(2, profileId);
			stmt.setInt(3, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count != 0){
				isPresent = true;
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	
	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public String getProfileNameById(Integer profileId) throws SQLException{
		Connection conn = null;
		String profileName = null;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "Select name from sensor_profile where id=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				profileName = rs.getString("name");
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profileName;
	}
	/**
	 * 
	 * @param profileId
	 * @return
	 * @throws SQLException
	 */
	public Profile getProfileGeopoints(Integer profileId) throws SQLException{
		Connection conn = null;
		Profile profile = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT point_name, address, location_type ,addr1, addr2,city,state,zip,country FROM sensor_geopoints WHERE profile_id=? AND (location_type=1 OR location_type=2)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();			
			profile = new Profile();
			Geopoint geoPoint = null;
			ArrayList<Geopoint> geopoints = new ArrayList<Geopoint>();
			while(rs.next()){
				geoPoint = new Geopoint();
				geoPoint.setPointName(rs.getString("point_name"));
				geoPoint.setAddress(rs.getString("address"));
				geoPoint.setLocationType(rs.getInt("location_type"));
				
				
				geoPoint.setAddr1(rs.getString("addr1"));
				geoPoint.setAddr2(rs.getString("addr2"));
				geoPoint.setCity(rs.getString("city"));
				geoPoint.setState(rs.getString("state"));
				geoPoint.setZip(rs.getString("zip"));
				geoPoint.setCountry(rs.getString("country"));
				
				geopoints.add(geoPoint);
			}
			profile.setGeopoints(geopoints);
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profile;
	}
	
	
	/**
	 * 
	 * @param conn
	 * @param profileIds
	 * @throws SQLException
	 */
	public void deleteProfileGeoPoints(Connection conn, ArrayList<Integer> profileIds) throws SQLException {
		String query = "DELETE FROM sensor_geopoints WHERE profile_id=?";
		PreparedStatement stmt = conn.prepareStatement(query);
		for(Integer profileId : profileIds){
			stmt.setInt(1, profileId);
			stmt.addBatch();
		}
		stmt.executeBatch();
		stmt.close();
	}
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteSensorThresholdForOrg(Connection conn, Integer orgId) throws SQLException {
		String query = "DELETE FROM sensor_threshold WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteSensorFrequencyForOrg(Connection conn, Integer orgId) throws SQLException {
		String query = "DELETE FROM sensor_frequency WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
}
