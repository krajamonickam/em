package com.rfxcel.sensor.dao;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.sql.Connection;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.TreeSet;

import org.apache.http.HttpEntity;
import org.apache.http.HttpHost;
import org.apache.http.entity.ContentType;
import org.apache.http.nio.entity.NStringEntity;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.elasticsearch.client.RestClient;
import org.json.JSONArray;
import org.json.JSONObject;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.notification.dao.NotificationDAO;
import com.rfxcel.notification.util.NotificationConstant;
import com.rfxcel.notification.vo.NotificationReqVO;
import com.rfxcel.notification.vo.NotificationResVO;
import com.rfxcel.notification.vo.NotificationVO;
import com.rfxcel.notification.vo.ShipmentNotifications;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.vo.AuditLogResponse;
import com.rfxcel.rule.service.RuleCache;
import com.rfxcel.rule.vo.RuleVO;
import com.rfxcel.search.util.ElasticSearchConstants;
import com.rfxcel.sensor.beans.AttributeData;
import com.rfxcel.sensor.beans.DataPoint;
import com.rfxcel.sensor.beans.DeviceData;
import com.rfxcel.sensor.beans.DeviceInfoDetails;
import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.beans.Location;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

public class SearchDAO {
	private static final Logger logger = Logger.getLogger(SearchDAO.class);
	private static SearchDAO searchDAO;
	private final String keySeparator = "@#";

	
	//ConfigCache configCache = ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port");	
	/*private static ConfigCache configCache = ConfigCache.getInstance();	
	private int ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size") = Integer.parseInt(configCache.getSystemConfig("elasticsearch.data.fetch.size"));
	private String  ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name") = configCache.getSystemConfig("elasticsearch.index.name");
	private static final String ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name") = configCache.getSystemConfig("elasticsearch.host.name");
	private static final Integer esHttpPort = Integer.parseInt(configCache.getSystemConfig("elasticsearch.http.port"));
	
	
	private  static RuleCache ruleCache = RuleCache.getInstance();
	private static UserTimeZoneCache.getInstance() UserTimeZoneCache.getInstance() = UserTimeZoneCache.getInstance().getInstance();*/
	
	
	private static final double ATTR_VALUE_NULL = -999.00d;
	private static HashMap<String, String> paramColumnMap = new HashMap<String, String>();
	
	
	public static SearchDAO getInstance(){
		if(searchDAO == null){
			searchDAO = new SearchDAO();			
		}
		return searchDAO;
	}
	
	
	/**
	 * <p>It is used to fetch notification data from elastic search index using RestClient</p>	
	 * @param request
	 * @return
	 * @throws ParseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public NotificationResVO getNotifications(NotificationReqVO request) throws ParseException, JsonParseException, JsonMappingException, IOException{
		NotificationResVO response = new NotificationResVO();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;		
		try{
			String searchString = request.getSearchString();
			Integer orgId = request.getOrgId();	
			String deviceId = null;
			String packageId = null;
			String productId = null;
			if(request.getDeviceData() != null){
				deviceId = request.getDeviceData().getDeviceId();
				packageId = request.getDeviceData().getPackageId();
				productId = request.getDeviceData().getProductId();
			}
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_NOTIFICATION;
			
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();			
			
			//String query = "{\"query\":{\"match\":{\"deviceId\":\"" + searchString + "\"}}}";
//			String query = "{\"query\": {\"bool\": {\"must\": [{\"bool\": {\"must\": [{\"match\": {	\"orgId\": \""+orgId+"\"}}]}},"
//					+ "{\"query_string\": {\"query\": \"*"+searchString+"*\", \"fields\": [\"deviceId\", \"containerId\", \"productId\", \"notification\"]}}]}},"
//					+ "	\"sort\": {	\"createdOn\": {\"order\": \"desc\"}}}";
			/*Start Optimized query*/
			//Integer pageNumber = 0; //get input from request
			final Map<String, Object> esQuery = new LinkedHashMap<>();	
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			//esQuery.put("from", pageNumber*ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size")); //pagination i.e. displaying number of records(ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size")) per page, we can get input for pageNumber from UI
			esQuery.put("_source", new String[] {"id", "containerId", "deviceId", "alertCode", "productId", "notification", "createdOn", "state"});
			
			//if searchString is null then don't search using searchString
			if(searchString == null || searchString.trim().length() == 0){			
				if ((deviceId == null) && (packageId == null) && (productId == null)) {
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
									Collections.singletonMap("term", Collections.singletonMap("orgId", orgId)),
									Collections.singletonMap("term", Collections.singletonMap("status", 1))
							}))		
					})));
					
				}
				else {
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
									Collections.singletonMap("term", Collections.singletonMap("orgId", orgId)),
									Collections.singletonMap("term", Collections.singletonMap("status", 1)),
									Collections.singletonMap("match", Collections.singletonMap("deviceId", getQueryAnalyser(deviceId))),
									Collections.singletonMap("match", Collections.singletonMap("containerId", getQueryAnalyser(packageId))),
									Collections.singletonMap("match", Collections.singletonMap("productId", getQueryAnalyser(productId)))
							}))		
					})));
				}
			}else{
				
				Boolean hasSpecialChars = Utility.hasSpecialCharacter(searchString);
				String queryParserName = hasSpecialChars ? "simple_query_string" : "query_string";			
				if ((deviceId == null) && (packageId == null) && (productId == null)) {
					
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
									Collections.singletonMap("term", Collections.singletonMap("orgId", orgId)),
									Collections.singletonMap("term", Collections.singletonMap("status", 1))
							})),						
							
							Collections.singletonMap(queryParserName, Collections.singletonMap("query", "*"+searchString+"*"))						
					})));
					
				} else {
					
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
									Collections.singletonMap("term", Collections.singletonMap("orgId", orgId)),
									Collections.singletonMap("term", Collections.singletonMap("status", 1)),
									Collections.singletonMap("match", Collections.singletonMap("deviceId", getQueryAnalyser(deviceId))),
									Collections.singletonMap("match", Collections.singletonMap("containerId", getQueryAnalyser(packageId))),
									Collections.singletonMap("match", Collections.singletonMap("productId", getQueryAnalyser(productId)))
							})),						
							
							Collections.singletonMap(queryParserName, Collections.singletonMap("query", "*"+searchString+"*"))						
					})));
					
				}
					
				
			}
					
			//esQuery.put("sort", Collections.singletonMap("createdOn", Collections.singletonMap("order", "desc")));
			
			
			//esQuery.put("sort", Collections.singletonMap("createdOn", Collections.singletonMap("order", "desc")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("createdOn", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);		
			/*End Optimized query*/
			
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
				DAOUtility.getInstance().closeConnection(client);
				return response;
			}
			
			JSONArray searchHitJson = null;
			ObjectMapper objectMapper = new ObjectMapper();
			searchHitJson = getSearchHitsJsonArray(esResponse);
			
			LinkedHashMap<String, ArrayList<NotificationVO>> groupedNotifications = new LinkedHashMap<String, ArrayList<NotificationVO>>();
			
			String key;
			Long alertCode;
			int state;
			
			String dateFormatString = ConfigCache.getInstance().getOrgConfig("date.format", orgId);
			SimpleDateFormat configDateFormat;
			if(dateFormatString != null){
				configDateFormat =  new SimpleDateFormat(dateFormatString);
			}else{
				configDateFormat= new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			}
			
			String userLogin = request.getUser();
			String timeZoneOffset = UserTimeZoneCache.getInstance().getTimeZoneOffset(userLogin);
			String tzCode = UserTimeZoneCache.getInstance().getTimeZone(userLogin);
			
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}
			if(searchHitJson != null){
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
							
					packageId = result.get("containerId") != null ? result.get("containerId").toString() : "";
					deviceId = result.get("deviceId") != null ? result.get("deviceId").toString() : "";
					alertCode = result.get("alertCode") != null ? Long.parseLong(result.get("alertCode").toString()) : 0L;
					productId = result.get("productId") != null ? result.get("productId").toString() : "";
					state = result.get("state") != null ? Integer.parseInt(result.get("state").toString()) : 0;
					key = productId + keySeparator+ packageId+ keySeparator+ deviceId;
					String createdOn ="";
					String notification = result.get("notification")!= null ? result.get("notification").toString() : "";
					Long notId = Long.parseLong(result.get("id").toString());
					if(result.get("createdOn") != null){				
						createdOn = configDateFormat.format((esDateFormat.parse(result.get("createdOn").toString()).getTime()) + (Utility.convertOffsetToMillis(timeZoneOffset)));				
						if(tzCode!= null){
							createdOn = createdOn +" " +tzCode;
						}
					}
					
					//state = NotificationDAO.getInstance().getStateFromAlertCode(alertCode);
					NotificationVO notiVO = new NotificationVO(notId, notification, createdOn, productId);
					notiVO.setState(state);
											
					if(groupedNotifications.get(key)!= null){
						groupedNotifications.get(key).add(notiVO);
					}else{
						 ArrayList<NotificationVO> temp = new  ArrayList<NotificationVO>();
						temp.add( notiVO);
						groupedNotifications.put(key, temp);
					}				
				}
			}
			
			ArrayList<ShipmentNotifications> shipmentNotifications =  new ArrayList<ShipmentNotifications>();
			String id;
			ArrayList<NotificationVO> notifications ;
			ShipmentNotifications shipmentNot;
			
			for(Entry<String, ArrayList<NotificationVO>> entry : groupedNotifications.entrySet()){
				id = entry.getKey();
				// get shipment status from sensor_Associate table
				String[] values = id.split(keySeparator);
				productId = values[0];
				packageId = values[1];
				deviceId = values[2];
				state = AssociationDAO.getInstance().getShipmentExcursionStatus(deviceId, packageId, productId);
				notifications = entry.getValue();
				shipmentNot = new ShipmentNotifications(id, notifications,productId,packageId,deviceId,  state);
				shipmentNotifications.add(shipmentNot);
			}
				
			if(shipmentNotifications.size() == 0 && searchString != null){
				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No alerts found for the keyword \""+searchString+"\"");
			}
			response.setShipmentNotifications(shipmentNotifications);
			response.setDateTime(Utility.getSimpleDateFormat().format(new Date()));
			return response;
		}catch(Exception e){
			logger.error("Exception in SearchDAO :: getNotifications "+ e.getLocalizedMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			DAOUtility.getInstance().closeConnection(client);
			return response;
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
	}
	
	/**
	 * <p>It is used to fetch deviceLog data from elastic search index using RestClient</p>
	 * @param request
	 * @return
	 * @throws ParseException
	 * @throws IOException 
	 * @throws UnsupportedEncodingException 
	 */
	public SensorResponse getDeviceLogs(SensorRequest request) throws ParseException, UnsupportedEncodingException, IOException{
		SensorResponse response = new SensorResponse(Utility.getSimpleDateFormat().format(new Date()));
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;		
		try{
			Integer orgId = request.getDeviceLog().getOrgId();
			String timeZoneOffset = UserTimeZoneCache.getInstance().getTimeZoneOffset(request.getUser());
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}
			String searchString = request.getSearchString();		
			String documentType = ElasticSearchConstants.ES_DOCTYPE_DEVICE_LOGS;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();			
			
//			String query = "{\"query\": {\"query_string\": { \"query\": \"*"+searchString+"*\",\"fields\": [\"deviceId\", \"logMsg\"]}},"
//					+ "\"sort\": { \"createTime\": { \"order\": \"desc\" }}}";
			/*Start Optimized query*/
			final Map<String, Object> esQuery = new LinkedHashMap<>();	
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			//esQuery.put("from", 10); //pagination for 10 records per page, we can get input from UI
			esQuery.put("_source", new String[] {"id", "deviceId", "userName", "logMsg", "createTime", "orgId"});
			//if searchString is null then don't search using searchString			
			if(searchString == null || searchString.trim().length() == 0){			
				esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
						Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
								Collections.singletonMap("term", Collections.singletonMap("orgId", orgId))
						}))	
						
				})));
			}else{
				Boolean hasSpecialChars = Utility.hasSpecialCharacter(searchString);
				String queryParserName = hasSpecialChars ? "simple_query_string" : "query_string";	
				esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
						Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
								Collections.singletonMap("term", Collections.singletonMap("orgId", orgId))
						})),				
						Collections.singletonMap(queryParserName, Collections.singletonMap("query", "*"+searchString+"*"))		
						
				})));
			}
						
			//esQuery.put("sort", Collections.singletonMap("createTime", Collections.singletonMap("order", "desc")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("createTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);		
			/*End Optimized query*/
			
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				e.printStackTrace();
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
				DAOUtility.getInstance().closeConnection(client);
				return response;
			}
		
			JSONArray searchHitJson = null;
			ObjectMapper objectMapper = new ObjectMapper();
			searchHitJson = getSearchHitsJsonArray(esResponse);			
			
			DeviceLog deviceLog = null;
			ArrayList<DeviceLog> deviceLogList = new ArrayList<DeviceLog>();
			String deviceId;
			String userName;
			String logMsg;		
			
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					deviceId = result.get("deviceId") != null ? result.get("deviceId").toString() : "";
					userName = result.get("userName") != null ? result.get("userName").toString() : "";
					logMsg = result.get("logMsg") != null ? result.get("logMsg").toString() : "";
					java.sql.Timestamp createdTime = null;
					if (result.get("createTime") != null) {
						String createdTimeString = Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("createTime").toString()).getTime())
										+ (Utility.convertOffsetToMillis(timeZoneOffset)));
						createdTime = java.sql.Timestamp.valueOf(createdTimeString);
					}
					deviceLog = new DeviceLog(deviceId, userName, logMsg, createdTime);
					deviceLogList.add(deviceLog);
				}
			}
			if(deviceLogList.size() == 0 && searchString != null){
				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No device logs found for the keyword \""+searchString+"\"");
			}
			response.setDeviceLogList(deviceLogList);				
			return response;
		}catch(Exception e){
			logger.error("Exception in SearchDAO :: getDeviceLogs "+ e.getLocalizedMessage());	
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			DAOUtility.getInstance().closeConnection(client);
			return response;
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		
	}
	
	/**
	 * <p>It is used to fetch auditLog data from elastic search index using RestClient</p>
	 * @param request
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 * @throws ParseException
	 */
	public AuditLogResponse getAuditLogs(SensorRequest request) throws org.apache.http.ParseException, IOException, ParseException{
		AuditLogResponse response = new AuditLogResponse(Utility.getSimpleDateFormat().format(new Date()));
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String timeZoneOffset = UserTimeZoneCache.getInstance().getTimeZoneOffset(request.getUser());
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}
			//Integer pageNumber = 0;  //get input from request
			Integer orgId = request.getOrgId();
			String searchString = request.getSearchString();
			String documentType = ElasticSearchConstants.ES_DOCTYPE_AUDIT_LOGS;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();	
			final Map<String, Object> esQuery = new LinkedHashMap<>();	
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			//esQuery.put("from", pageNumber*ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size")); //pagination i.e. displaying number of records(ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size")) per page, we can get input for pageNumber from UI
			esQuery.put("_source", new String[] {"id", "userId", "userName", "entityType", "entityId", "action", "oldValue", "newValue", "description", "orgId", "createTime"});
			//if searchString is null then don't search using searchString
			if(searchString == null || searchString.trim().length() == 0){			
				esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
						Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
								Collections.singletonMap("term", Collections.singletonMap("orgId", orgId))
						}))	
						
				})));
			}else{			
				Boolean hasSpecialChars = Utility.hasSpecialCharacter(searchString);
				String queryParserName = hasSpecialChars ? "simple_query_string" : "query_string";	
				esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
						Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
								Collections.singletonMap("term", Collections.singletonMap("orgId", orgId))
						})),				
						Collections.singletonMap(queryParserName, Collections.singletonMap("query", "*"+searchString+"*"))		
						
				})));
			}		
			
			//esQuery.put("sort", Collections.singletonMap("createTime", Collections.singletonMap("order", "desc")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("createTime", getSortByValue("date"))});
			
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){			
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
				DAOUtility.getInstance().closeConnection(client);
				return response;
			}
		
			JSONArray searchHitJson = null;
			ObjectMapper objectMapper = new ObjectMapper();
			searchHitJson = getSearchHitsJsonArray(esResponse);
			
			AuditLog auditLog = null;
			ArrayList<AuditLog> auditLogs = new ArrayList<AuditLog>();
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});				
					Long userId = result.get("userId") != null ? Long.parseLong(result.get("userId").toString()) : 0L;
					String userName = result.get("userName") != null ? result.get("userName").toString() : "";
					String entityType = result.get("entityType") != null ? result.get("entityType").toString() : "";
					Long entityId = result.get("entityId") != null ? Long.parseLong(result.get("entityId").toString()) : 0L;
					String action = result.get("action") != null ? result.get("action").toString() : "";
					String oldValue = result.get("oldValue") != null ? result.get("oldValue").toString() : "";
					String newValue = result.get("newValue") != null ? result.get("newValue").toString() : "";
					String description = result.get("description") != null ? result.get("description").toString() : "";
					String createdTime = null;
					if (result.get("createTime") != null) {
						createdTime = Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("createTime").toString()).getTime())
								+ (Utility.convertOffsetToMillis(timeZoneOffset)));										
					}
					auditLog = new AuditLog(userId, userName, entityId, entityType, action, oldValue, newValue, description, orgId);
					auditLog.setCreatedOn(createdTime);
					auditLogs.add(auditLog);
				}
			}
			if(auditLogs.size() == 0 && searchString != null){
				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No audit logs found for the keyword \""+searchString+"\"");
			}
			response.setAuditLogs(auditLogs);		
			return response;
		}catch(Exception e){
			logger.error("Exception in SearchDAO :: getAuditLogs "+ e.getLocalizedMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			DAOUtility.getInstance().closeConnection(client);
			return response;
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
	}
	
	/**
	 * <p>It is used to fetch current/pending/completed shipment list from elastic search index using RestClient</p>
	 * @param request
	 * @return
	 * @throws ParseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public SensorResponse getShipmentList(SensorRequest request) throws ParseException, JsonParseException, JsonMappingException, IOException{
		SensorResponse response = new SensorResponse(Utility.getSimpleDateFormat().format(new Date()));
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{			
			Integer orgId = request.getOrgId();
			String searchType = request.getSearchType();
			String searchString = request.getSearchString();
			boolean showMap = request.isShowMap();
			String sensorAssociateDocType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_ASSOCIATION;			
			
			String timeZoneOffset = UserTimeZoneCache.getInstance().getTimeZoneOffset(request.getUser());
			if (timeZoneOffset == null) {
				timeZoneOffset = "+00:00";
			}
			//search shipmentList by any keyword i.e. org_id, deviceId and containerId or productId
			DeviceData deviceData = null;
			if(SensorConstant.SEARCH_TYPE_CURRENT_SHIPMENTS.equalsIgnoreCase(searchType)){ //current shipment	
				deviceData = new DeviceData();
				deviceData.setRelationStatus(1);				
				//deviceData.setSensorDataExist(1);
				request.setDeviceData(deviceData);	
				
			}else if(SensorConstant.SEARCH_TYPE_PENDING_SHIPMENTS.equalsIgnoreCase(searchType)){ //pending shipment	
				deviceData = new DeviceData();
				deviceData.setRelationStatus(1);
				//deviceData.setSensorDataExist(0);
				request.setDeviceData(deviceData);			
				
			}else if(SensorConstant.SEARCH_TYPE_COMPLETED_SHIPMENTS.equalsIgnoreCase(searchType)){ //completed shipment
				deviceData = new DeviceData();
				deviceData.setRelationStatus(0);
				//deviceData.setSensorDataExist(null);
				request.setDeviceData(deviceData);		
				
			}
			
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();	
			final Map<String, Object> esQuery = new LinkedHashMap<>();	
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			esQuery.put("_source", new String[] {"id", "containerId", "deviceId", "productId", "excursionStatus", "addFavorite", "createDateTime", "updateDateTime"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
							Collections.singletonMap("term", Collections.singletonMap("orgId", orgId)),
							Collections.singletonMap("term", Collections.singletonMap("relationStatus", deviceData.getRelationStatus()))
							
							//,deviceData.getSensorDataExist() != null ? Collections.singletonMap("term", Collections.singletonMap("sensorDataExist", deviceData.getSensorDataExist())) : Collections.singletonMap(null, null)
					})),				
					Collections.singletonMap("query_string", Collections.singletonMap("query", "*"+searchString+"*"))		
			})));					
			
			//esQuery.put("sort", Collections.singletonMap("addFavorite", Collections.singletonMap("order", "desc")));
			
			esQuery.put("sort",new Map[] { Collections.singletonMap("addFavorite", getSortByValue("int"))});
			
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+sensorAssociateDocType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){			
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+sensorAssociateDocType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
				DAOUtility.getInstance().closeConnection(client);
				return response;
			}
		
			JSONArray searchHitJson = null;
			ObjectMapper objectMapper = new ObjectMapper();
			searchHitJson = getSearchHitsJsonArray(esResponse);	
			
			ArrayList<DeviceInfoDetails> shipmentList = new ArrayList<DeviceInfoDetails>();
			com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
			Sensor sensor = new Sensor();
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					String containerId = result.get("containerId") != null ? result.get("containerId").toString() : "";
					String deviceId = result.get("deviceId") != null ? result.get("deviceId").toString() : "";
					String productId = result.get("productId") != null ?result.get("productId").toString() : "";
					Integer excursionStatus = result.get("excursionStatus") != null ? Integer.parseInt(result.get("excursionStatus").toString()) : 0;
					Integer addFavorite = result.get("addFavorite") != null ? Integer.parseInt(result.get("addFavorite").toString()) : 0;
					DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails(deviceId, containerId, productId, excursionStatus, addFavorite);
					String createDateTime = "";					
					try{						
						createDateTime = result.get("createDateTime") != null ? result.get("createDateTime").toString() : "";
						if (createDateTime != null && !"".equals(createDateTime)) {
							createDateTime = Utility.getSimpleDateFormat().format((esDateFormat.parse(createDateTime).getTime()) + (Utility.convertOffsetToMillis(timeZoneOffset)));										
						}	
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Error parsing createDateTime::"+e.getLocalizedMessage());
					}								
					
					deviceInfoDetails.setCreatedOn(createDateTime);
					deviceInfoDetails.setUpdatedOn(getLastStatusTime(deviceId, containerId, productId));			
					if(showMap){
						map = getMapDataForShipment(deviceId, containerId, productId, map);
					}
					shipmentList.add(deviceInfoDetails);
				}
			}
			
			if(shipmentList.size() == 0 && searchString != null){				
				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No shipments found for the keyword \""+searchString+"\"");
			}
			sensor.setShipmentList(shipmentList);										
			sensor.setMap(map);
			response.setSensor(sensor);
			return response;
		}catch(Exception e){
			logger.error("Exception in SearchDAO :: getShipmentList "+ e.getLocalizedMessage());	
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			DAOUtility.getInstance().closeConnection(client);
			return response;
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
	}
	
	/**
	 * <p>It is used to fetch last status time from elastic search index using RestClient</p>
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 * @throws ParseException
	 */
	public Long getLastStatusTime(String deviceId, String containerId, String productId) throws org.apache.http.ParseException, IOException, ParseException{
		Long lastUpdatedTime = null;
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String sensorDataDocType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;	
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();	
			//get last updated time
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", 1);
			esQuery.put("_source", new String[] {"statusTime"});
			//here i am using match instead of term because match query is analyzed, and every fields are analyzed by default
			//we should use match when searching natural language strings
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId))
					}))	
					
			})));
			///esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response updatedTimeResponse = null;
				
			try{
				updatedTimeResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+sensorDataDocType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+sensorDataDocType+" and error details "+e.getLocalizedMessage());			
			}
			
			JSONArray searchHitJson = null;
			ObjectMapper objectMapper = new ObjectMapper();
			searchHitJson = getSearchHitsJsonArray(updatedTimeResponse);
			
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});				
					String statusTime = null;
					try{					
						statusTime = result.get("statusTime") != null ? result.get("statusTime").toString() : "";
						if(statusTime != null && !"".equals(statusTime)){
							lastUpdatedTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(statusTime).getTime())));
						}
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Error parsing statusTime::"+e.getLocalizedMessage());					
					}
					
				}
			}
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getLastStatusTime "+ e.getLocalizedMessage());	
			e.printStackTrace();
			
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		return lastUpdatedTime;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 * @throws ParseException
	 */
	public String getLastStatusTimeInESFormat(String deviceId, String containerId, String productId) throws org.apache.http.ParseException, IOException, ParseException{
		String lastUpdatedTime = null;
		RestClient client = null;
		try{
			String sensorDataDocType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;	
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();	
			//get last updated time
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", 1);
			esQuery.put("_source", new String[] {"statusTime"});
			//here i am using match instead of term because match query is analyzed, and every fields are analyzed by default
			//we should use match when searching natural language strings
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId))
					}))	
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response updatedTimeResponse = null;
				
			try{
				updatedTimeResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+sensorDataDocType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+sensorDataDocType+" and error details "+e.getLocalizedMessage());			
			}
			
			JSONArray searchHitJson = null;
			ObjectMapper objectMapper = new ObjectMapper();
			searchHitJson = getSearchHitsJsonArray(updatedTimeResponse);
			
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					
					if(result.get("statusTime") != null){
						lastUpdatedTime = result.get("statusTime").toString();
					}
				}
			}
			
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getLastStatusTimeInESFormat "+ e.getLocalizedMessage());	
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		return lastUpdatedTime;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 * @throws ParseException
	 */
	public com.rfxcel.sensor.beans.Map getMapDataForShipment(String deviceId, String containerId, String productId, com.rfxcel.sensor.beans.Map map) throws org.apache.http.ParseException, IOException, ParseException{		
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String sensorDataDocType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;	
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			//get last updated time
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", 1);
			esQuery.put("_source", new String[] {"latitude", "longitude", "excursionStatus", "statusTime"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId))
					}))	
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
				
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+sensorDataDocType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+sensorDataDocType+" and error details "+e.getLocalizedMessage());		
			}
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});				
					Long dateTime = 0L;				
					String statusTime = null;
					try{					
						statusTime = result.get("statusTime") != null ? result.get("statusTime").toString() : "";
						if(statusTime != null && !"".equals(statusTime)){
							dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(statusTime).getTime())));
						}	
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Error parsing statusTime::"+e.getLocalizedMessage());					
					}
					
					Location location  = new Location(deviceId, containerId, productId);
					location.setDateTime(dateTime);
					String latitude = result.get("latitude") != null ? result.get("latitude").toString() : "";
					String longitude = result.get("longitude") != null ? result.get("longitude").toString() : "";
					Integer excursionStatus = result.get("excursionStatus") != null ? Integer.parseInt(result.get("excursionStatus").toString()) : 0;
					location.setLat(latitude);
					location.setLang(longitude);
					location.setPairStatus(excursionStatus);
					map.getLocations().add(location);
				}
			}
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getMapDataForShipment "+ e.getLocalizedMessage());	
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		return map;		
	}
	
	/**
	 * <p>It is used to fetch total notification count from elastic search index using RestClient</p>
	 * @param request
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	public NotificationResVO getNotificationsCount(NotificationReqVO request) throws org.apache.http.ParseException, IOException{
		NotificationResVO response = new NotificationResVO();
		RestClient client = null;
		try{
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_NOTIFICATION;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			String searchType = request.getSearchType();
			String containerId = null;
			String deviceId = null;
			String productId = null;
			
			final Map<String, Object> esQuery = new LinkedHashMap<>();		
			
			if (searchType.equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_CONTAINER)) {
				containerId = request.getDeviceData().getPackageId();
				deviceId = request.getDeviceData().getDeviceId();
				productId = request.getDeviceData().getProductId();
				if ((deviceId == null) && (containerId == null) && (productId == null)) {
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
									Collections.singletonMap("term", Collections.singletonMap("orgId", request.getOrgId())),
									Collections.singletonMap("term", Collections.singletonMap("status", 1))
							}))	
					})));
				}
				else {
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
									Collections.singletonMap("term", Collections.singletonMap("orgId", request.getOrgId())),
									Collections.singletonMap("match", Collections.singletonMap("deviceId", getQueryAnalyser(deviceId))),
									Collections.singletonMap("match", Collections.singletonMap("containerId", getQueryAnalyser(containerId))),
									Collections.singletonMap("match", Collections.singletonMap("productId", getQueryAnalyser(productId))),
									Collections.singletonMap("term", Collections.singletonMap("status", 1))
							}))	
							
					})));
				}
			}else{
				esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
						Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{
								Collections.singletonMap("term", Collections.singletonMap("orgId", request.getOrgId()))							
						}))	
						
				})));
			}
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_count", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
				return response;	
			}		
			
			Integer notificationCount = 0;			
			notificationCount = getSearchHitCount(esResponse);	
			response.setCount(notificationCount);
			response.setDateTime(Utility.getSimpleDateFormat().format(new Date()));
			return response;
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getNotificationsCount "+ e.getLocalizedMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			return response;
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
	}
	
	/**
	 * <p>It is used to get Map data from elastic search index using RestClient</p>
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 * @throws org.apache.http.ParseException 
	 * @throws IOException 
	 */
	public SensorResponse getMapDataByDeviceNPackageId(SensorRequest request) throws ParseException, org.apache.http.ParseException, IOException {
		SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try {			
			DeviceData deviceData = request.getDeviceData();
			String deviceId = deviceData.getDeviceId();
			String packageId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			Long startDate = deviceData.getStartDate();
			
			response.setPackageId(packageId);
			response.setDeviceId(deviceId);
			Utility.getDateFormat().setTimeZone(TimeZone.getTimeZone("UTC"));
			Sensor sensor = new Sensor();
				
			// If startDate is not specified, get all the data points
			com.rfxcel.sensor.beans.Map map;
			if (startDate == null) {
				map = getAllMapData(deviceId, packageId, productId);
			}// Get data points after start date
			else {
				map = getMapDataFromStartDate(deviceId, packageId, productId, startDate);
			}
			ArrayList<Location> locations = map.getLocations();
			// If there are datapoints, get the last timestamp from that
			if (locations != null && locations.size() > 0) {
				Long lastTimeStamp = locations.get(locations.size() - 1).getDateTime();
				map.setLastTimeStamp(lastTimeStamp);
			}// else get the status time stamp
			else if (deviceData.getStartDate() != null) {
				map.setLastTimeStamp(deviceData.getStartDate());
			}
			sensor.setMap(map);
			response.setSensor(sensor);
		}catch(Exception e){
			
			logger.error("Exception in SearchDAO :: getMapDataByDeviceNPackageId "+ e.getLocalizedMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			return response;
		}
		
		return response;		
	}
	
	/**
	 * 	
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 * @throws ParseException
	 */
	public com.rfxcel.sensor.beans.Map getAllMapData(String deviceId, String packageId, String productId) throws org.apache.http.ParseException, IOException, ParseException {
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();			
			String value = ConfigCache.getInstance().getSystemConfig("sensor.location.decimal.limit"); // get the precision value from config, if not present setting it to 3
			value = (value == null) ? "3" : value;
			int precision;
			try {
				precision = Integer.valueOf(value);
			} catch (NumberFormatException nfe) {
				precision = 3;
			}
			// This is stored in DB just for demo purpose
			setZoomFactorForShipment(deviceId, packageId, productId, map);
			final Map<String, Object> esQuery = new LinkedHashMap<>();	
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			esQuery.put("_source", new String[] {"id", "longitude", "latitude", "statusTime", "excursionStatus"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", packageId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId))							
					}))	
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "ASC")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
			}		
			String prevLat = "";
			String prevLang = "";
			String currentLat;
			String currentLang;
			Location location;
			Long dateTime;
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					Long id = result.get("id") != null ? Long.parseLong(result.get("id").toString()) : 0L;
					currentLat = result.get("latitude") != null ? result.get("latitude").toString() : "";
					currentLang = result.get("longitude") != null ? result.get("longitude").toString() : "";
					dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
					int excursionStatus = result.get("excursionStatus") != null ? Integer.parseInt(result.get("excursionStatus").toString()) : 0;
					// Check if we need to add this data point
					Boolean skip = SendumDAO.getInstance().skipDataPoints(precision, currentLat, currentLang, prevLat, prevLang);
					if (!skip) {
						location = new Location(id, dateTime, currentLat, currentLang, excursionStatus);
						map.getLocations().add(location);
					}else{
						location = map.getLocations().get(map.getLocations().size() -1);
						// if we are skipping the data point get the latest information at that lat and lang value
						location.setId(id);
						location.setDateTime(dateTime);
						location.setLang(currentLang);
						location.setLat(currentLat);
						location.setPairStatus(excursionStatus);									
					}
					prevLang = currentLang;
					prevLat = currentLat;
					
				}
			}
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getAllMapData "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		return map;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @param map
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	public void setZoomFactorForShipment(String deviceId, String packageId, String productId, com.rfxcel.sensor.beans.Map map) throws org.apache.http.ParseException, IOException {
		RestClient client = null;
		try{
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_ASSOCIATION;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			Integer zoomFactor = null;
			String centerLatitude = null;
			String centerLongitude = null;
			
			final Map<String, Object> esQuery = new LinkedHashMap<>();	
			esQuery.put("size", 1);
			esQuery.put("_source", new String[] {"zoomFactor", "centerLatitude", "centerLongitude"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", packageId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId))							
					}))	
					
			})));
		
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());			
			}		
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});	
					zoomFactor = result.get("zoomFactor") != null ? Integer.parseInt(result.get("zoomFactor").toString()) : 0;
					centerLatitude = result.get("centerLatitude") != null ? result.get("centerLatitude").toString() : "";
					centerLongitude = result.get("centerLongitude") != null ? result.get("centerLongitude").toString() : "";
				}
			}
			map.setZoomFactor(zoomFactor);
			map.setCenterLatLong(centerLatitude, centerLongitude);
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: setZoomFactorForShipment "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @param startDate
	 * @return
	 * @throws ParseException
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	private com.rfxcel.sensor.beans.Map getMapDataFromStartDate(String deviceId, String packageId, String productId, Long startDate) throws ParseException, org.apache.http.ParseException, IOException {
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();			
			String value = ConfigCache.getInstance().getSystemConfig("sensor.location.decimal.limit"); // get the precision value from config, if not present  setting it to 3
			value = (value == null) ? "3" : value;
			int precision;
			try {
				precision = Integer.valueOf(value);
			} catch (NumberFormatException nfe) {
				precision = 3;
			}
			Long UTCDateTime = Utility.getSimpleDateFormat().parse(Utility.getDateFormat().format(new Date(startDate))).getTime();		
			String sDate = esDateFormat.format(UTCDateTime)+30;  //appending 30 min, so SDate will be like 2017-09-23T07:05:15.000+0530, it will convert sDate to exact record in elastic search format, and it will fetch the records from that sDate
			
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			esQuery.put("_source", new String[] {"id", "longitude", "latitude", "statusTime", "excursionStatus"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", packageId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId)),
							Collections.singletonMap("range",
									Collections.singletonMap("statusTime", Collections.singletonMap("gt", sDate)))
					}))
					
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "ASC")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
			}		
			String prevLat = "";
			String prevLang = "";
			String currentLat;
			String currentLang;
			Location location;
			long dateTime = 0L;
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int i = 0; i < searchHitJson.length(); i++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(i);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					Long id = result.get("id") != null ? Long.parseLong(result.get("id").toString()) : 0L;				
					currentLat = result.get("latitude") != null ? result.get("latitude").toString() : "";
					currentLang = result.get("longitude") != null ? result.get("longitude").toString() : "";
					if(result.get("statusTime") != null){
						dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
					}
					
					int excursionStatus = result.get("excursionStatus") != null ? Integer.parseInt(result.get("excursionStatus").toString()) : 0;
					// Check if we need to add this data point
					Boolean skip = SendumDAO.getInstance().skipDataPoints(precision, currentLat, currentLang, prevLat, prevLang);
					if (!skip) {
						location = new Location(id, dateTime, currentLat, currentLang, excursionStatus);
						map.getLocations().add(location);
					}else{
						location = map.getLocations().get(map.getLocations().size() -1);
						// if we are skipping the data point get the latest information at that lat and lang value
						location.setId(id);
						location.setDateTime(dateTime);
						location.setLang(currentLang);
						location.setLat(currentLat);
						location.setPairStatus(excursionStatus);
					}
					prevLang = currentLang;
					prevLat = currentLat;
					
				}
			}
			// This is stored in DB just for demo purpose
			setZoomFactorForShipment(deviceId, packageId, productId, map);
			
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getMapDataFromStartDate "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		return map;
	}
	
	/**
	 * <p>It is used to fetch attribute List from elastic search index using RestClient</p>
	 * @param sensorRequest
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	public SensorResponse getAttributeList(SensorRequest sensorRequest)	throws SQLException, ParseException, org.apache.http.ParseException, IOException {
		SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try {			
			Sensor sensor = new Sensor();
			DeviceData devicedata = sensorRequest.getDeviceData();
			String deviceId = devicedata.getDeviceId();
			String packageId = devicedata.getPackageId();
			String productId = devicedata.getProductId();
			Boolean showAll = devicedata.getShowAll() == null ? false : devicedata.getShowAll();
			Boolean showMapAttribute = devicedata.getShowMapAttribute() == null ? false : devicedata.getShowMapAttribute();
			Long id = devicedata.getId();
			response.setDeviceId(deviceId);
			response.setPackageId(packageId);
			response.setProductId(productId);			
			ArrayList<com.rfxcel.sensor.beans.Attribute> attrList = SendumDAO.getInstance().getAttributeDefinition(deviceId, showAll, showMapAttribute);
			paramColumnMap = Utility.populateParamColumnMap();
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			if (attrList.size() > 0) {
				// need column name instead of attribute name to map to actual table columns
				StringBuffer columns = new StringBuffer();
				for (int i = 0; i < attrList.size(); i++) {
					if (paramColumnMap.get(attrList.get(i).getAttrName()) != null) {
						if (i != 0) {
							if(columns.length() != 0){
								columns.append(",");
							}							
						}
						//due to elastic search mapping of column name of sensorData in logstash, renaming column name
						String columnName = paramColumnMap.get(attrList.get(i).getAttrName());
						if(columnName.equals("battery_rem")){
							columnName = "battery";
						}else if(columnName.equals("growth_log")){
							columnName = "growthLog";
						}else if(columnName.equals("cumulative_log")){
							columnName = "cumulativeLog";
						}
						columns.append(columnName);
					}
				}
				// if id is null, fetch latest values of attributes
				String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;				
				final Map<String, Object> esQuery = new LinkedHashMap<>();
				esQuery.put("size", 1);
				esQuery.put("_source", new String[] {"statusTime", "excursionStatus", "temperature", "tilt", "pressure", "humidity", "light", "growthLog", "cumulativeLog", "battery", "shock", "vibration", "latitude", "longitude"});
				
				if (id == null) {
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
									Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
									Collections.singletonMap("match", Collections.singletonMap("containerId", packageId)),
									Collections.singletonMap("match", Collections.singletonMap("productId", productId))									
							}))						
							
					})));
					//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));	
					esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
				}// Else fetch value corresponding to a given id
				else {
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
									Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
									Collections.singletonMap("match", Collections.singletonMap("containerId", packageId)),
									Collections.singletonMap("term", Collections.singletonMap("id", id))									
							}))						
							
					})));					
				}	
				
				final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
				org.elasticsearch.client.Response esResponse = null;
				try{
					esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
				}catch(Exception e){
					DAOUtility.getInstance().closeConnection(client);
					logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());						
				}		
				String attrName;
				Double value;
				com.rfxcel.sensor.beans.Attribute attribute;
				Integer roundOffTo;
				
				//ArrayList<String> excursionAttr = new ArrayList<String>();
				Long statusTime = 1L;
				
				JSONArray searchHitJson = null;
				searchHitJson = getSearchHitsJsonArray(esResponse);
				ObjectMapper objectMapper = new ObjectMapper();
				if (searchHitJson != null) {
					for (int i = 0; i < searchHitJson.length(); i++) {
						JSONObject jsonObj = searchHitJson.getJSONObject(i);
						String sourceObj = jsonObj.getJSONObject("_source").toString();
						Map<String, Object> result = new HashMap<String, Object>();
						// convert JSON string to Map
						result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
						if(result.get("statusTime") != null){
							statusTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
						}						
						sensor.setStatusTime(statusTime);
						/*String excursionAttrString = result.get("excursionAttributes") != null ? result.get("excursionAttributes").toString() : null;
						if(excursionAttrString != null && excursionAttrString.trim().length() != 0){
							excursionAttr.addAll(Arrays.asList(excursionAttrString.toLowerCase().split(",")));
						}*/
						for (int j = 0; j < attrList.size(); j++) {
							attribute = attrList.get(j);
							attrName = attribute.getAttrName();
							String paramCoulumnName = paramColumnMap.get(attrName);		
							if (paramCoulumnName != null) {
								//due to elastic search mapping of column name of sensorData in logstash, renaming column name						
								if(paramCoulumnName.equals("battery_rem")){
									paramCoulumnName = "battery";
								}else if(paramCoulumnName.equals("growth_log")){
									paramCoulumnName = "growthLog";
								}else if(paramCoulumnName.equals("cumulative_log")){
									paramCoulumnName = "cumulativeLog";
								}
								
								value = result.get(paramCoulumnName) != null ? Double.parseDouble(result.get(paramCoulumnName).toString()) : 0L;
								
								/*if (excursionAttr.contains(attrName.toLowerCase())) {
									attribute.setExcursion(true);
								}*/
								// If value is null, checking if it is because of null value or it is actually 0. If it is 0 then setting it to 0
								if (value == 0) {
									boolean wasNull = result == null ? true : false;
									if (wasNull) {
										attribute.setAttrValue(ATTR_VALUE_NULL);
									} else {
										roundOffTo = attribute.getRoundOffTo();
										if (roundOffTo != null) {
											value = Utility.round(value, roundOffTo);
										}
										attribute.setAttrValue(value);
									}
								} else {
									roundOffTo = attribute.getRoundOffTo();
									if (roundOffTo != null) {
										value = Utility.round(value, roundOffTo);
									}
									attribute.setAttrValue(value);
								}
								
								RuleVO rule = RuleCache.getInstance().getRule(deviceId, packageId, productId,attrName);
								if(rule!= null && rule.getMinValue()!= null)
									attribute.setMin(rule.getMinValue().floatValue());
								if(rule!= null && rule.getMaxValue()!= null)
									attribute.setMax(rule.getMaxValue().floatValue());
							}
						}
					}
				}
				
				attrList = getAttributeList(deviceId, packageId, productId, attrList);			
				
			}			
			sensor.setAttributeList(attrList);
			response.setSensor(sensor);
			return response;
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getAttributeList "+ e.getLocalizedMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			return response;
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @param attrList
	 * @return
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private ArrayList<com.rfxcel.sensor.beans.Attribute> getAttributeList(String deviceId, String packageId, String productId, ArrayList<com.rfxcel.sensor.beans.Attribute> attrList) throws JsonParseException, JsonMappingException, IOException{
		RestClient client = null;
		try{
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			for (int i = 0; i < attrList.size(); i++) {
				com.rfxcel.sensor.beans.Attribute attribute = attrList.get(i);			
				double attrValue = 0.0;
				Double value;
				Integer roundOffTo;
				paramColumnMap = Utility.populateParamColumnMap();
				if(attribute.getAttrValue() != null){
					attrValue = attribute.getAttrValue();
				}
				if (attrValue == ATTR_VALUE_NULL) {
					String colName = paramColumnMap.get(attrList.get(i).getAttrName());
					String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;					
					final Map<String, Object> esQuery = new LinkedHashMap<>();
					esQuery.put("size", 1);
					esQuery.put("_source", new String[] {colName});
					esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
							Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
									Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
									Collections.singletonMap("match", Collections.singletonMap("containerId", packageId)),
									Collections.singletonMap("match", Collections.singletonMap("productId", productId)),
									Collections.singletonMap("exists", Collections.singletonMap("field", colName))	
							}))						
							
					})));
					//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));
					esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
					final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
					org.elasticsearch.client.Response esResponse = null;
					try{
						esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
					}catch(Exception e){
						DAOUtility.getInstance().closeConnection(client);
						logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
						//e.printStackTrace();
					}	
					JSONArray searchHitJson = null;
					searchHitJson = getSearchHitsJsonArray(esResponse);
					ObjectMapper objectMapper = new ObjectMapper();
					if (searchHitJson != null) {
						for (int j = 0; j < searchHitJson.length(); j++) {
							JSONObject jsonObj = searchHitJson.getJSONObject(j);
							String sourceObj = jsonObj.getJSONObject("_source").toString();
							Map<String, Object> result = new HashMap<String, Object>();
							// convert JSON string to Map
							result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
							value = result.get(colName) != null ? Double.parseDouble(result.get(colName).toString()) : 0L;
							if (value == 0) {
								boolean wasNull = result == null ? true : false;
								if (!wasNull) {
									roundOffTo = attribute.getRoundOffTo();
									if (roundOffTo != null) {
										value = Utility.round(value, roundOffTo);
									}
									attribute.setAttrValue(value);
								}
							} else {
								roundOffTo = attribute.getRoundOffTo();
								if (roundOffTo != null) {
									value = Utility.round(value, roundOffTo);
								}
								attribute.setAttrValue(value);
							}
						}
					}				
					
				}
			}
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getAttributeList "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		return attrList;
	}
	
	/**
	 * <p>It is used to fetch map attribute data from elastic search index using RestClient</p>
	 * @param sensorRequest
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public SensorResponse getAttributeData(SensorRequest sensorRequest) throws SQLException, ParseException {
		Connection conn = null;
		SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try {			
			String timeZoneOffset = UserTimeZoneCache.getInstance().getTimeZoneOffset(sensorRequest.getUser());
			if (timeZoneOffset == null) {
				timeZoneOffset = "+00:00";
			}
			
			DeviceData deviceData = sensorRequest.getDeviceData();
			String deviceId = deviceData.getDeviceId();
			String containerId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			Long startDate =  deviceData.getStartDate();
			Long endDate = deviceData.getEndDate();
			response.setDeviceId(deviceId);
			response.setPackageId(containerId);

			Sensor sensor = new Sensor();
			ArrayList<AttributeData> attributesData = new ArrayList<AttributeData>();
			conn = DAOUtility.getInstance().getConnection();

			Float duration = deviceData.getDuration();
			List<DataPoint> dataPoints;
			AttributeData attrData;
			HashMap<String, RuleVO> thresholds = RuleCache.getInstance().getAttributeThreshold(deviceId, containerId, productId);
		
			ArrayList<com.rfxcel.sensor.beans.Attribute> attrList = deviceData.getAttrType();
			// get attribute names by calling gwetAttribute Names method
			HashMap<String, com.rfxcel.sensor.beans.Attribute> attrMap = SendumDAO.getInstance().getAttributeMap(conn, deviceId);
			RuleVO ruleVO;
			paramColumnMap = Utility.populateParamColumnMap();
			if ((attrList == null || attrList.size() == 0)) {
				Comparator<Entry<String, com.rfxcel.sensor.beans.Attribute>> comp = new Comparator<Entry<String, com.rfxcel.sensor.beans.Attribute>>() {
					@Override
					public int compare(Entry<String, com.rfxcel.sensor.beans.Attribute> o1, Entry<String, com.rfxcel.sensor.beans.Attribute> o2) {
						if (o1.getValue().getAttrId() < o2.getValue().getAttrId())
							return (-1);
						if (o1.getValue().getAttrId() > o2.getValue().getAttrId())
							return (1);
						return 0;
					}
				};
				TreeSet<Entry<String, com.rfxcel.sensor.beans.Attribute>> tree = new TreeSet<Entry<String, com.rfxcel.sensor.beans.Attribute>>(comp);
				tree.addAll(attrMap.entrySet());
				ArrayList<Entry<String, com.rfxcel.sensor.beans.Attribute>> list = new ArrayList<Entry<String, com.rfxcel.sensor.beans.Attribute>>(tree);
				String attrName;
				String columnName;
				Double minValue;
				Double maxValue;
				for (Entry<String, com.rfxcel.sensor.beans.Attribute> entry : list) {
					com.rfxcel.sensor.beans.Attribute attr = entry.getValue();
					attrName = attr.getAttrName();
					columnName = paramColumnMap.get(attrName);// checking if corresponding column is present
					
					minValue = null;
					maxValue = null;
					if (columnName != null) {
						//due to elastic search mapping of column name of sensorData in logstash, renaming column name
						if(columnName.equals("battery_rem")){
							columnName = "battery";
						}else if(columnName.equals("growth_log")){
							columnName = "growthLog";
						}else if(columnName.equals("cumulative_log")){
							columnName = "cumulativeLog";
						}
						if(startDate != null && endDate!= null){
							dataPoints = getDataForStartEndDate(deviceId, containerId,productId, startDate, endDate, columnName);
						}
						else if (duration != null) {
							dataPoints = getResult4Slider(deviceId, containerId,productId, duration, columnName); // get data point for duration specified
						} else {
							dataPoints = getAllDataForAttribute(deviceId, containerId, productId, columnName); // get all the data points
						}
						boolean alert = false;
						if (thresholds != null && thresholds.get(attrName) != null) {
							ruleVO = thresholds.get(attrName);
							minValue = ruleVO.getMinValue() == null ? null : ruleVO.getMinValue();
							maxValue = ruleVO.getMaxValue() == null ? null : ruleVO.getMaxValue();
							if (ruleVO.getAlert() == RuleVO.ALERT_ON) {
								alert = true;
							}
						}
						attrData = new AttributeData(attr.getLegend(), attrName, dataPoints, minValue, maxValue, attr.getUnit(), alert);
						if (dataPoints != null && dataPoints.size() > 0) {
							DataPoint lastDataPoint = dataPoints.get(dataPoints.size() - 1);
							Long lastTimeStamp = lastDataPoint.getDateTime();
							attrData.setLastDateTime(lastTimeStamp);
						}
						attributesData.add(attrData);
					}
				}
			} else {
				String attrName;
				String columnName;
				Double minValue;
				Double maxValue;
				String legend;
				String unit;
				for (com.rfxcel.sensor.beans.Attribute attr : attrList) {
					attrName = attr.getAttrName();
					columnName = paramColumnMap.get(attrName);// checking if corresponding column is present
					minValue = null;
					maxValue = null;
					if (columnName != null) {
						//due to elastic search mapping of column name of sensorData in logstash, renaming column name
						if(columnName.equals("battery_rem")){
							columnName = "battery";
						}else if(columnName.equals("growth_log")){
							columnName = "growthLog";
						}else if(columnName.equals("cumulative_log")){
							columnName = "cumulativeLog";
						}
						dataPoints = getResult4StartDate(deviceId, containerId, productId, attr.getStartDateTime(), columnName); // get all datapoints
						boolean alert = false;
						if (thresholds != null && thresholds.get(attrName) != null) {
							ruleVO = thresholds.get(attrName);
							minValue = ruleVO.getMinValue() == null ? null : ruleVO.getMinValue();
							maxValue = ruleVO.getMaxValue() == null ? null : ruleVO.getMaxValue();
							if (ruleVO.getAlert() == RuleVO.ALERT_ON) {
								alert = true;
							}
						}
						legend = attrMap.get(attrName) == null ? attrName : attrMap.get(attrName).getLegend();
						unit = attrMap.get(attrName).getUnit();
						attrData = new AttributeData(legend, attrName, dataPoints, minValue, maxValue, unit, alert);

						if (dataPoints != null && dataPoints.size() > 0) {
							DataPoint lastDataPoint = dataPoints.get(dataPoints.size() - 1);
							Long lastTimeStamp = lastDataPoint.getDateTime();
							attrData.setLastDateTime(lastTimeStamp);
						} else {
							attrData.setLastDateTime(attr.getStartDateTime());
						}
						attributesData.add(attrData);
					}
				}
			}

			sensor.setAttributes(attributesData);
			response.setSensor(sensor);
			return response;
		} catch(Exception e){
			logger.error("Exception in SearchDAO :: getAttributeData "+ e.getLocalizedMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Elastic search server is not running.");
			return response;
		} finally {		
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @param startDate
	 * @param endDate
	 * @param column
	 * @param offset
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private List<DataPoint> getDataForStartEndDate(String deviceId, String containerId, String productId, Long startDate, Long endDate, String column) throws ParseException, JsonParseException, JsonMappingException, IOException {
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String sDate = null;
			String eDate = null;
			if(startDate != null){
				Long startDateTimeUTC = Utility.getSimpleDateFormat().parse(Utility.getDateFormat().format(new Date(startDate))).getTime();
				sDate = esDateFormat.format(startDateTimeUTC)+30; //appending 30 min, so sDate will be like 2017-09-23T00:00:00.000+0530, it will convert sDate to exact record in elastic search format, and it will fetch the records from that sDate
			}
			if(endDate != null){
				Long endDateTimeUTC = Utility.getSimpleDateFormat().parse(Utility.getDateFormat().format(new Date(endDate))).getTime();
				eDate = esDateFormat.format(endDateTimeUTC)+30; //appending 30 min, so eDate will be like 2017-09-23T23:59:59.000+0530, it will convert edate to exact record in elastic search format, and it will fetch the records up to that eDate
			}	
			
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			esQuery.put("_source", new String[] {column, "statusTime"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId)),
							Collections.singletonMap("exists", Collections.singletonMap("field", column)),
							Collections.singletonMap("range",
									Collections.singletonMap("statusTime", Collections.singletonMap("gte", sDate))), 
							Collections.singletonMap("range",
									Collections.singletonMap("statusTime",Collections.singletonMap("lte", eDate)))
					}))						
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "ASC")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
			}
			
			DataPoint dataPoint;
			Double value;
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int j = 0; j < searchHitJson.length(); j++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(j);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					value = result.get(column) != null ? Double.parseDouble(result.get(column).toString()) : 0L;
					value = Utility.round(value, 3);
					dataPoint = new DataPoint();
					Long dateTime = 0L;
					if(result.get("statusTime") != null){
						dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
						dataPoint.setDateTime(dateTime);
					}
					dataPoint.setDataPoint(value);
					dataPoints.add(dataPoint);// data is already in ascending format
				}
			}		
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getDataForStartEndDate "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		return dataPoints;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @param timespan
	 * @param column
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	private List<DataPoint> getResult4Slider(String deviceId, String containerId, String productId, Float timespan, String column) throws ParseException, org.apache.http.ParseException, IOException {
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String maxDate = null;
			maxDate = getLastStatusTimeInESFormat(deviceId, containerId, productId);
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			if(maxDate != null){
				int timeSpanInMin = (int) (timespan * 60);
				//calculate the minDate by substract the timespan from maxDate
				Long maxDateTime = esDateFormat.parse(maxDate).getTime();
				Long minDateTime = (long) (maxDateTime - (timeSpanInMin * 60 * 1000));
				String minDate = esDateFormat.format(minDateTime)+30; //appending 30 min so it will convert minDate to exact record in elastic search format, and it will fetch the records from that minDate
				
				String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;				
				final Map<String, Object> esQuery = new LinkedHashMap<>();
				esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
				esQuery.put("_source", new String[] {column, "statusTime"});
				esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
						Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
								Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
								Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),
								Collections.singletonMap("match", Collections.singletonMap("productId", productId)),
								Collections.singletonMap("exists", Collections.singletonMap("field", column)),
								Collections.singletonMap("range",
										Collections.singletonMap("statusTime", Collections.singletonMap("gte", minDate))),
								Collections.singletonMap("range",
												Collections.singletonMap("statusTime", Collections.singletonMap("lte", maxDate)))
						}))						
						
				})));
				//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));
				esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
				final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
				org.elasticsearch.client.Response esResponse = null;
				try{
					esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
				}catch(Exception e){
					DAOUtility.getInstance().closeConnection(client);
					logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
					//e.printStackTrace();
				}
				DataPoint dataPoint;
				Double value;
				JSONArray searchHitJson = null;
				searchHitJson = getSearchHitsJsonArray(esResponse);
				ObjectMapper objectMapper = new ObjectMapper();
				if (searchHitJson != null) {
					for (int j = 0; j < searchHitJson.length(); j++) {
						JSONObject jsonObj = searchHitJson.getJSONObject(j);
						String sourceObj = jsonObj.getJSONObject("_source").toString();
						Map<String, Object> result = new HashMap<String, Object>();
						// convert JSON string to Map
						result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
						value = result.get(column) != null ? Double.parseDouble(result.get(column).toString()) : 0L;
						value = Utility.round(value, 3);
						dataPoint = new DataPoint();
						Long dateTime = 0L;
						if(result.get("statusTime") != null){
							dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
							dataPoint.setDateTime(dateTime);
						}
						dataPoint.setDataPoint(value);
						dataPoints.add(0, dataPoint);// arranging data in ascending order
					}
				}
			}		
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getResult4Slider "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		return dataPoints;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @param column
	 * @return
	 * @throws ParseException
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	private List<DataPoint> getAllDataForAttribute(String deviceId, String containerId, String productId, String column) throws ParseException, org.apache.http.ParseException, IOException {
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			esQuery.put("_source", new String[] {column, "statusTime"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),
							Collections.singletonMap("match", Collections.singletonMap("productId", productId)),
							Collections.singletonMap("exists", Collections.singletonMap("field", column))						
					}))						
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "desc")));	
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
			}
			DataPoint dataPoint;
			Double value;
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int j = 0; j < searchHitJson.length(); j++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(j);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					value = result.get(column) != null ? Double.parseDouble(result.get(column).toString()) : 0L;
					value = Utility.round(value, 3);
					dataPoint = new DataPoint();
					Long dateTime = 0L;
					if(result.get("statusTime") != null){
						dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
						dataPoint.setDateTime(dateTime);
					}
					dataPoint.setDataPoint(value);
					dataPoints.add(0, dataPoint);// arranging data in ascending order
				}
			}	
			
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getAllDataForAttribute "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		
		return dataPoints;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param containerId
	 * @param productId
	 * @param startDate
	 * @param column
	 * @return
	 * @throws ParseException
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	private List<DataPoint> getResult4StartDate(String deviceId, String containerId, String productId, Long startDate, String column) throws ParseException, JsonParseException, JsonMappingException, IOException {
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
		RestClient client = null;
		try{
			String sDate = null;
			if(startDate != null){
				Long startDateTimeUTC = Utility.getSimpleDateFormat().parse(Utility.getDateFormat().format(new Date(startDate))).getTime();
				sDate = esDateFormat.format(startDateTimeUTC)+30;  //appending 30 min so it will convert sDate to exact record in elastic search format, and it will fetch the records from that sDate
			}
			
			String documentType = ElasticSearchConstants.ES_DOCTYPE_SENSOR_DATA;
			Integer esHttpPort = Integer.parseInt(ConfigCache.getInstance().getSystemConfig("elasticsearch.http.port"));
			client = RestClient.builder(new HttpHost(ConfigCache.getInstance().getSystemConfig("elasticsearch.host.name"), esHttpPort)).build();
			final Map<String, Object> esQuery = new LinkedHashMap<>();
			esQuery.put("size", ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch.size"));
			esQuery.put("_source", new String[] {column, "statusTime"});
			esQuery.put("query", Collections.singletonMap("bool", Collections.singletonMap("must", new Map[] {
					Collections.singletonMap("bool", Collections.singletonMap("must", new Map[]{							
							Collections.singletonMap("match", Collections.singletonMap("deviceId", deviceId)),
							Collections.singletonMap("match", Collections.singletonMap("containerId", containerId)),						
							Collections.singletonMap("exists", Collections.singletonMap("field", column)),
							Collections.singletonMap("range",
									Collections.singletonMap("statusTime", Collections.singletonMap("gt", sDate)))
					}))						
					
			})));
			//esQuery.put("sort", Collections.singletonMap("statusTime", Collections.singletonMap("order", "ASC")));
			esQuery.put("sort",new Map[] { Collections.singletonMap("statusTime", getSortByValue("date"))});
			final HttpEntity payload = new NStringEntity(JSONObject.valueToString(esQuery), ContentType.APPLICATION_JSON);
			org.elasticsearch.client.Response esResponse = null;
			try{
				esResponse = client.performRequest("GET", "/"+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+"/"+documentType+"/_search", new Hashtable<>(), payload);
			}catch(Exception e){
				DAOUtility.getInstance().closeConnection(client);
				logger.error("Error while establishing connection to fetch data from index "+ ConfigCache.getInstance().getSystemConfig("elasticsearch.index.name")+" and document type "+documentType+" and error details "+e.getLocalizedMessage());
				//e.printStackTrace();
			}
			
			
			DataPoint dataPoint;
			Double value;
			JSONArray searchHitJson = null;
			searchHitJson = getSearchHitsJsonArray(esResponse);
			ObjectMapper objectMapper = new ObjectMapper();
			if (searchHitJson != null) {
				for (int j = 0; j < searchHitJson.length(); j++) {
					JSONObject jsonObj = searchHitJson.getJSONObject(j);
					String sourceObj = jsonObj.getJSONObject("_source").toString();
					Map<String, Object> result = new HashMap<String, Object>();
					// convert JSON string to Map
					result = objectMapper.readValue(sourceObj, new TypeReference<Map<String, Object>>() {});
					value = result.get(column) != null ? Double.parseDouble(result.get(column).toString()) : 0L;
					value = Utility.round(value, 3);
					dataPoint = new DataPoint();
					Long dateTime = 0L;
					if(result.get("statusTime") != null){
						dateTime = Utility.getUTCDate(Utility.getSimpleDateFormat().format((esDateFormat.parse(result.get("statusTime").toString()).getTime())));
						dataPoint.setDateTime(dateTime);
					}
					dataPoint.setDataPoint(value);				
					dataPoints.add(0, dataPoint);// arranging data in ascending order
				}
			}		
		}catch(Exception e){
			DAOUtility.getInstance().closeConnection(client);
			logger.error("Exception in SearchDAO :: getResult4StartDate "+ e.getLocalizedMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(client);
		}
		return dataPoints;
	}
	
	/**
	 * 
	 * @param esResponse
	 * @return
	 * @throws org.apache.http.ParseException
	 * @throws IOException
	 */
	public JSONArray getSearchHitsJsonArray(org.elasticsearch.client.Response esResponse) throws org.apache.http.ParseException, IOException{
		HttpEntity entity = esResponse.getEntity();
		JSONArray searchHitJson = null;		
		if (entity != null) {
			String responseString = EntityUtils.toString(entity);
			// parsing JSON
			JSONObject result = new JSONObject(responseString); //convert string to json object
			JSONObject searchHit = result.getJSONObject("hits");
			if(searchHit.has("hits")){
				searchHitJson = searchHit.getJSONArray("hits");
			}
		}
		return searchHitJson;
	}
		
	/**
	 * 
	 * @param responseString
	 * @return
	 * @throws IOException 
	 * @throws org.apache.http.ParseException 
	 */
	public Integer getSearchHitCount(org.elasticsearch.client.Response esResponse) throws org.apache.http.ParseException, IOException{
		HttpEntity entity = esResponse.getEntity();
		Integer searchHitCount = 0;		
		if (entity != null) {
			String responseString = EntityUtils.toString(entity);
			// parsing JSON
			JSONObject result = new JSONObject(responseString); //convert string to json object		
			if(result.has("count")){
				searchHitCount = Integer.parseInt(result.get("count").toString());
			}
		}
		return searchHitCount;
	}

	private static Map getSortByValue(String fieldType){
		Map<String, String> map = new HashMap<String, String>();
		map.put("order", "desc");
		map.put("unmapped_type", fieldType);
		return map;
	}
	
	private static Map getQueryAnalyser(String valueString){
		Map<String, String> map = new HashMap<String, String>();
		map.put("query", valueString);
		map.put("operator", "and");
		return map;
	}
	
}
