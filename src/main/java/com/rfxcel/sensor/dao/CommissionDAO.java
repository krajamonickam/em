package com.rfxcel.sensor.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.Product;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorResponse;

public class CommissionDAO {
	Logger logger = Logger.getLogger(CommissionDAO.class);
	private static CommissionDAO commissionDAO;
	
	private static DeviceCache deviceCache = DeviceCache.getInstance(); 
	
	public static CommissionDAO getInstance(){
		if(commissionDAO == null){
			commissionDAO = new CommissionDAO();
		}
		return commissionDAO;
	}

	CommissionDAO(){
		//	Utility.getDateFormat().setTimeZone(TimeZone.getTimeZone("UTC"));
	}

	// For commission Screen
	// To get Device Details
	public List<DeviceDetail> getItemsByName(List<String> itemListDetails) throws SQLException {
		 Connection conn = null;
		List<DeviceDetail> deviceDetailList = new ArrayList<DeviceDetail>();
		try{
			for (int i = 0; i < itemListDetails.size(); i++) {
				String Query = "select * from sensor_device_types where device_manufacturer like ?";
				conn = DAOUtility.getInstance().getConnection();
				PreparedStatement stmt = conn.prepareStatement(Query);
				stmt.setString(1, itemListDetails.get(i) + '%');
				ResultSet rs = stmt.executeQuery();
				while (rs.next()) {
					DeviceDetail deviceDetail = new DeviceDetail();
					deviceDetail.setDeviceTypeId(rs.getInt("device_type_id"));
					deviceDetail.setDeviceType(rs.getString("device_type"));
					deviceDetail.setDeviceName(rs.getString("device_manufacturer"));
					deviceDetailList.add(deviceDetail);
				}
				rs.close();
				stmt.close();
			}
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		
		return deviceDetailList;
	}

	/**
	 * 
	 * @param deviceDetails
	 * @throws SQLException
	 */
	public SensorResponse commissionDevices(List<DeviceDetail> deviceDetails, int orgId, long groupId){
		Connection conn = null;
		SensorResponse res = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "insert into sensor_devices(device_id, device_type_id, active, state, org_id, group_id, device_key, mdn, activation_status)"
					+ "values(?,(SELECT device_type_id FROM sensor_device_types where device_type=? and active=1 and org_id=?),?,1,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			String deviceId;
			String deviceType;
			String deviceKey;
			String mdn;
			Boolean active;
			int activationStatus;
			for(DeviceDetail deviceDetail : deviceDetails){
				deviceId =  deviceDetail.getDeviceId();
				deviceType = deviceDetail.getDeviceType();
				deviceKey = deviceDetail.getDeviceKey();
				mdn = deviceDetail.getMdn();	
				if(deviceDetail.getActivationState()!= null && deviceDetail.getActivationState()==0){
					activationStatus = 0;
					active = false;		//activation status is pending so active is set to false;
				}else{
					activationStatus = 1;// activation status is completed, so keeping active flag value as is.
					active = deviceDetail.getActive();
					if(active == null){
						active = true;
					}
				}
				stmt.setString(1,deviceId );
				stmt.setString(2, deviceType);
				stmt.setInt(3, orgId);
				stmt.setBoolean(4, active);
				stmt.setInt(5, orgId);
				stmt.setLong(6, groupId);
				stmt.setString(7, deviceKey);
				stmt.setString(8, mdn);
				stmt.setInt(9, activationStatus);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
		} catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage("Error occured while commissioning device(s) "+ e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}

	/**
	 * 
	 * @param newDeviceDetails
	 * @param decommissionedDevices
	 * @return
	 */
	public SensorResponse commissionDevices(List<DeviceDetail> newDeviceDetails, ArrayList<DeviceDetail> decommissionedDevices, int orgId, long groupId) {
		SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
		String respMsg = "";
		if(newDeviceDetails!= null && newDeviceDetails.size()!=0){
			response = commissionDevices(newDeviceDetails, orgId, groupId);
			if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
				respMsg = response.getResponseMessage();
			}else{
				Integer deviceTypeId ;
				for(DeviceDetail deviceDetails : newDeviceDetails){
					//Adding deviceId - type mapping to cache
					String deviceId = deviceDetails.getDeviceId();
					deviceTypeId = SendumDAO.getInstance().getDeviceTypeIdForADeviceId(deviceId);
					deviceCache.addDeviceIdTypeMapping(deviceId,deviceTypeId);
					deviceCache.putDeviceIdBatteryValueMapping(deviceId, (long) SensorConstant.BATTERY_DEFAULT_VALUE);
					deviceCache.addDeviceOrgMapping(deviceId, orgId);
				}
			}
		}
		if(decommissionedDevices!= null && decommissionedDevices.size()!=0){
			response = commissionDecommissionedDevices(decommissionedDevices, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
				respMsg = respMsg + response.getResponseMessage();
			}
		}
		if(respMsg.length()>0){
			response.setResponseMessage(respMsg);
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
		}else{
			response.setResponseMessage("Device(s) commissioned successfully");
			response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
		}
		return response;
	}

	/**
	 * 
	 * @param decommissionedDevices
	 * @return
	 */
	private SensorResponse commissionDecommissionedDevices(ArrayList<DeviceDetail> decommissionedDevices, int orgId) {
		Connection conn = null;
		SensorResponse res = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET state = 1, active=? and activation_status = ? WHERE device_id=? and org_id = ? ";
			PreparedStatement stmt = conn.prepareStatement(query);
		    
		    String deviceId;		
			Boolean active;
			Integer activationStatus = 1;
			for(DeviceDetail deviceDetail : decommissionedDevices){
				deviceId = deviceDetail.getDeviceId();				
				if(deviceDetail.getActivationState()!= null && deviceDetail.getActivationState()==0){
					activationStatus = 0;
					active = false;		//activation status is pending so active is set to false;
				}else{
					activationStatus = 1;// activation status is completed, so keeping active flag value as is.
					active = deviceDetail.getActive();
					if(active == null){
						active = true;
					}
				}
				stmt.setBoolean(1, active); 
				stmt.setInt(2, activationStatus);
				stmt.setString(3, deviceId);
				stmt.setInt(4, orgId);
				stmt.addBatch();				
			}
			stmt.executeBatch();
			stmt.close();
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
		} catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage("Error occured while commissioning of decommissioned device(s) "+ e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}

	/**
	 * @param products
	 * @param orgId 
	 * @throws SQLException
	 */
	public SensorResponse commissionProducts(ArrayList<Product> products, int orgId, long groupId){
		Connection conn = null;
		SensorResponse res = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "INSERT INTO sensor_package (package_id, product_id, state, org_id, group_id) values (?,?,?,?, ?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			String packageId;
			String productId;
			for(Product product : products){
				packageId = product.getPackageId(); 
				productId = product.getProductId();
				stmt.setString(1,packageId );
				stmt.setString(2, productId);
				stmt.setInt(3, 1);
				stmt.setInt(4, orgId);
				stmt.setLong(5, groupId);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
		}catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage("Error occured while commissioning package(s) "+ e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}

	/**
	 * @param decommissionedProducts
	 * @param orgId 
	 * @return
	 */
	public SensorResponse commissionDecommissionedProducts(ArrayList<Product> decommissionedProducts, int orgId) {
		Connection conn = null;
		SensorResponse res = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try{
			StringBuilder packageIdList  = new StringBuilder();
			String list;
			for( Product product : decommissionedProducts){
				packageIdList.append(product.getPackageId()).append(",");
			}
			list = packageIdList.substring(0, packageIdList.length()-1);
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_package SET state = 1 WHERE package_id in (?) and org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, list ); 
			stmt.setInt(2, orgId);
			stmt.executeUpdate();
			stmt.close();
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
		} catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage("Error occured while commissioning of decommissioned package(s) "+ e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}

	/**
	 * 
	 * @param orgId 
	 * @param newDeviceDetails
	 * @param decommissionedDevices
	 * @return
	 */
	public SensorResponse commissionProducts(ArrayList<Product> newProducts, ArrayList<Product> decommissionedProducts, int orgId, long groupId) {
		SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
		String respMsg = "";
		if(newProducts!= null && newProducts.size()!=0){
			response = commissionProducts(newProducts, orgId, groupId);
			if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
				respMsg = response.getResponseMessage();
			}else{
				//add commissioned package's mapping to cache, deCommissioned package mapping is already retained in cache
				String packageId, productId;
				for(Product product : newProducts){
					packageId = product.getPackageId();
					productId = product.getProductId();
				}
			}
		}
		if(decommissionedProducts!= null && decommissionedProducts.size()!=0){
			response = commissionDecommissionedProducts(decommissionedProducts, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
				respMsg = respMsg + response.getResponseMessage();
			}
		}
		if(respMsg.length()>0){
			response.setResponseMessage(respMsg);
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
		}else{
			response.setResponseMessage("Package(s) commissioned successfully");
			response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
		}
		return response;
	}

	/**
	 * @return
	 * @throws SQLException
	 */
	public HashMap<Integer, HashMap<String, String>> getPackProductMapping(){
		Connection conn = null;
		HashMap<Integer, HashMap<String, String>> mapping = new HashMap<Integer, HashMap<String, String>>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "Select package_id, product_id, org_id  from sensor_package ";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			Integer orgId;
			while(rs.next()){
				orgId = rs.getInt("org_id");
				if(mapping.get(orgId) != null){
					mapping.get(orgId).put(rs.getString("package_id"), rs.getString("product_id"));
				}else{
					HashMap<String, String> map = new HashMap<String, String>();
					map.put(rs.getString("package_id"), rs.getString("product_id"));
					mapping.put(orgId, map);
				}
			}
			rs.close();
			stmt.close();
		}catch(SQLException e){
			logger.error("Error while fetching package-product Mapping per organization"+e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return mapping;
	}


	/**
	 * 
	 * @return
	 */
	public HashMap<String, Integer> getDeviceIdTypeMapping(){
		Connection conn = null;
		HashMap<String, Integer> mapping = new HashMap<String, Integer>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "Select device_id, device_type_id from sensor_devices";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				mapping.put(rs.getString("device_id"), rs.getInt("device_type_id"));
			}
			rs.close();
			stmt.close();
		}catch(SQLException e){
			logger.error("Error while fetching device id-type Mapping"+e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return mapping;
	}
	/**
	 * 
	 * @return
	 */
	public HashMap<String, Long> getDeviceIdBatteryValueMapping(){
		Connection conn = null;
		HashMap<String, Long> mapping = new HashMap<String, Long>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "Select device_id, battery_value from sensor_devices";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				String deviceId = rs.getString("device_id");
				Long batteryValue = (Long)rs.getObject("battery_value");
				mapping.put(deviceId, batteryValue);
			}
			rs.close();
			stmt.close();			
		}catch(SQLException e){
			logger.error("Error while fetching device id-battery value mapping "+e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return mapping;
	}
	
	/**
	 * 
	 * @return
	 */
	public HashMap<String, Integer> getDeviceOrgMapping(){
		Connection conn = null;
		HashMap<String, Integer> mapping = new HashMap<String, Integer>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT device_id, org_id FROM sensor_devices";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				String deviceId = rs.getString("device_id");
				Integer orgId = rs.getInt("org_id");
				mapping.put(deviceId, orgId);
			}
			rs.close();
			stmt.close();
			
		}catch(Exception e){
			logger.error("Error while fetching device id-org Id mapping "+e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return mapping;
	}

	
}
