package com.rfxcel.sensor.dao;

import java.sql.CallableStatement;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TimeZone;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceAttributeLogCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.notification.dao.AlertDAO;
import com.rfxcel.notification.dao.AlertGroupDAO;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.org.dao.ConfigDAO;
import com.rfxcel.org.dao.UserDAO;
import com.rfxcel.rule.service.RuleCache;
import com.rfxcel.rule.util.RuleConstants;
import com.rfxcel.rule.vo.RuleVO;
import com.rfxcel.sensor.beans.AttributeData;
import com.rfxcel.sensor.beans.DataPoint;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.beans.DeviceData;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.DeviceFields;
import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.beans.DeviceInfoDetails;
import com.rfxcel.sensor.beans.Geofence;
import com.rfxcel.sensor.beans.Location;
import com.rfxcel.sensor.beans.ProductDetail;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.beans.ShipDetails;
import com.rfxcel.sensor.beans.ShipmentData;
import com.rfxcel.sensor.beans.ShipmentLocation;
import com.rfxcel.sensor.control.VerizonDeviceDeactivator;
import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

public class SendumDAO {
	private static final double ATTR_VALUE_NULL = -999.00d;
	private static final Logger logger = Logger.getLogger(SendumDAO.class);
	private static SendumDAO sendumDAO;
	DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
	private static HashMap<String, String> paramColumnMap = new HashMap<String, String>();
	private static UserTimeZoneCache userTimeZoneCache = UserTimeZoneCache.getInstance();
	private  static RuleCache ruleCache = RuleCache.getInstance();
	private static ConfigCache configCache = ConfigCache.getInstance();
	private static AssociationCache assoCache = AssociationCache.getInstance();
	private static DeviceCache deviceCache = DeviceCache.getInstance() ;
	private static DeviceAttributeLogCache attrLogcache = DeviceAttributeLogCache.getInstance();
	
	public static SendumDAO getInstance() {
		if (sendumDAO == null) {
			sendumDAO = new SendumDAO();
			//populateParamColumnMap();
		}
		return sendumDAO;
	}
	
	/**
	 * Adds data to sensor_data table
	 * 
	 * @param sensorData
	 */
	public long addSensorData(Map<String, String> sensorData) {
		long id = -1;
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = null;
			String sqlQurySensorData = "INSERT INTO sensor_data( device_id, container_id, temperature, pressure, humidity, light, tilt, battery_rem, growth_log, cumulative_log, latitude, longitude,"
					+ " position_error, fix_type, fix_valid ,temp_probe, status_time, capacity_rem, voltage, cycle_count, signal_strength, cell_sys_id, ambient_temp,is_alert, product_id, shock, vibration) "
					+ "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) "
					+ " ON DUPLICATE KEY UPDATE temperature  = IFNULL(?,temperature), battery_rem = IFNULL(?,battery_rem), "
					+ "latitude = IFNULL(?,latitude), longitude = IFNULL(?,longitude), tilt = IFNULL(?,tilt), shock = IFNULL(?,shock), "
					+ "vibration = IFNULL(?,vibration),light = IFNULL(?,light),pressure = IFNULL(?,pressure),humidity = IFNULL(?,humidity)";
			stmt = conn.prepareStatement(sqlQurySensorData,	Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, sensorData.get("deviceIdentifier"));
			stmt.setString(2, sensorData.get("containerId"));
			stmt.setString(3, sensorData.get("temperature"));
			stmt.setString(4, sensorData.get("pressure"));
			stmt.setString(5, sensorData.get("humidity"));
			stmt.setString(6, sensorData.get("light"));
			stmt.setString(7, sensorData.get("tilt"));
			stmt.setString(8, sensorData.get("battery"));
			stmt.setString(9, sensorData.get("growthLog"));
			stmt.setString(10, sensorData.get("cumulativeLog"));
			stmt.setString(11, sensorData.get("latitude"));
			stmt.setString(12, sensorData.get("longitude"));
			stmt.setString(13, sensorData.get("position_error"));
			stmt.setString(14, sensorData.get("fixType"));
			stmt.setString(15, sensorData.get("valid"));
			stmt.setString(16, sensorData.get("temperatureProbe"));

			String statusTimeStamp = sensorData.get("statusTimeStamp");
			String alarmtime = sensorData.get("alarmTimeStamp");

			if (statusTimeStamp == null && alarmtime == null) {
				stmt.setString(17, null);
			} else {
				if (statusTimeStamp != null) {
					//long a = Utility.getDateFormat().parse(statusTimeStamp).getTime();
					long a = Utility.getUTCDate(statusTimeStamp);
					sensorData.put(RuleConstants.SENSOR_ATTR_TIME, Long.toString(a));
					//stmt.setTimestamp(17, new java.sql.Timestamp(a));
					stmt.setString(17,statusTimeStamp );
				} else if (alarmtime != null) {
					//long a = Utility.getDateFormat().parse(alarmtime).getTime();
					long a = Utility.getUTCDate(alarmtime);
					sensorData.put(RuleConstants.SENSOR_ATTR_TIME, Long.toString(a));
					stmt.setTimestamp(17, new java.sql.Timestamp(a));

				}
			}

			stmt.setString(18, sensorData.get("capacityRemaining"));
			stmt.setString(19, sensorData.get("voltage"));
			stmt.setString(20, sensorData.get("cycleCount"));
			stmt.setString(21, sensorData.get("signal_strength"));
			stmt.setString(22, sensorData.get("sid"));
			stmt.setString(23, sensorData.get("ambient_temp"));
			stmt.setBoolean(24, false); // -- to indicate it is sensor data record
			stmt.setString(25, sensorData.get("productId"));
			if(sensorData.get("shock") == null || sensorData.get("shock").isEmpty()){
			//stmt.setString(26, "0");	// -- If shock is not present setting it to 0 and not null
			stmt.setString(26, null);	
			}else{
				stmt.setString(26,sensorData.get("shock"));
			}
			stmt.setString(27, sensorData.get("vibration"));
			// Merge data if they are received at the same time
			if (sensorData.get("temperature") == null || sensorData.get("temperature").isEmpty()) {
				stmt.setString(28, null);
			} else {
				stmt.setString(28, sensorData.get("temperature"));
			}

			if (sensorData.get("battery") == null || sensorData.get("battery").isEmpty()) {
				stmt.setString(29, null);
			} else {
				stmt.setString(29, sensorData.get("battery"));
			}

			if (sensorData.get("latitude") == null || sensorData.get("latitude").isEmpty()) {
				stmt.setString(30, null);
			} else {
				stmt.setString(30, sensorData.get("latitude"));
			}

			if (sensorData.get("longitude") == null || sensorData.get("longitude").isEmpty()) {
				stmt.setString(31, null);
			} else {
				stmt.setString(31, sensorData.get("longitude"));
			}
			

			//
			if (sensorData.get("tilt") == null || sensorData.get("tilt").isEmpty()) {
				stmt.setString(32, null);
			} else {
				stmt.setString(32, sensorData.get("tilt"));
			}
			
			if (sensorData.get("shock") == null || sensorData.get("shock").isEmpty()) {
				stmt.setString(33, null);
			} else {
				stmt.setString(33, sensorData.get("shock"));
			}
			
			if (sensorData.get("vibration") == null || sensorData.get("vibration").isEmpty()) {
				stmt.setString(34, null);
			} else {
				stmt.setString(34, sensorData.get("vibration"));
			}
			
			if (sensorData.get("light") == null || sensorData.get("light").isEmpty()) {
				stmt.setString(35, null);
			} else {
				stmt.setString(35, sensorData.get("light"));
			}
			if (sensorData.get("pressure") == null || sensorData.get("pressure").isEmpty()) {
				stmt.setString(36, null);
			} else {
				stmt.setString(36, sensorData.get("pressure"));
			}
			if (sensorData.get("humidity") == null || sensorData.get("humidity").isEmpty()) {
				stmt.setString(37, null);
			} else {
				stmt.setString(37, sensorData.get("humidity"));
			}
			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getLong(1);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while inserting sensor data " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return id;
	}

	/**
	 * <p>
	 * Adds sensor alert to sensor_data table
	 * </p>
	 * 
	 * @param map
	 * @return
	 */
	public long addSensorAlert(Map<String, String> sensorData) {
		long id = -1;
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();

			String alarmType = sensorData.get("alarmType");
			if (alarmType != null && alarmType.toLowerCase().contains(RuleConstants.SENSOR_ATTR_TEMPERATURE)) {
				id = addAttributeSensorAlert(conn, sensorData, "temperature");
			} else if (alarmType != null && alarmType.toLowerCase().contains(RuleConstants.SENSOR_ATTR_LIGHT)) {
				id = addAttributeSensorAlert(conn, sensorData, "light");
			} else if (alarmType != null && alarmType.toLowerCase().contains(RuleConstants.SENSOR_ATTR_LOW_BATTERY)) {
				id = addAttributeSensorAlert(conn, sensorData, "battery_rem");
			} else if (alarmType != null && alarmType.toLowerCase().contains(RuleConstants.SENSOR_ATTR_LOW_BATTERY_VOLTAGE)) {
				id = addAttributeSensorAlert(conn, sensorData, "voltage");
			} else if (alarmType != null && alarmType.toLowerCase().contains(RuleConstants.SENSOR_ATTR_SHOCK)) {
				id = addAttributeSensorAlert(conn, sensorData, "shock");
			} else {
				id = addSensorAlert(conn, sensorData);
			}

		} catch (Exception e) {
			logger.error("Error while inserting sensor alert in sensor_data table "+ e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return id;
	}

	/**
	 * 
	 * @param conn
	 * @param sensorData
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private long addSensorAlert(Connection conn, Map<String, String> sensorData) throws SQLException, ParseException {
		long id = -1;
		PreparedStatement stmt = null;
		String sqlQurySensorAlert = "INSERT INTO sensor_data( device_id,container_id,alarm_type, alarm_value ,status_time,latitude,"
				+ " longitude,geo_fence_no, geo_fence_event, geo_fence_name, is_alert, product_id) values"
				+ " (?,?,?,?,?,?,?,?,?,?,?,?)";

		stmt = conn.prepareStatement(sqlQurySensorAlert, Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, sensorData.get("deviceIdentifier"));
		stmt.setString(2, sensorData.get("containerId"));
		stmt.setString(3, sensorData.get("alarmType"));
		stmt.setString(4, sensorData.get("alarmValue"));

		String alarmtime = sensorData.get("alarmTimeStamp");
		if (alarmtime == null) {
			stmt.setString(5, null);
		} else {
			//long a = Utility.getDateFormat().parse(alarmtime).getTime();
			long a = Utility.getUTCDate(alarmtime);
			sensorData.put(RuleConstants.SENSOR_ATTR_TIME, Long.toString(a));
			stmt.setString(5, alarmtime);
		}
		stmt.setString(6, sensorData.get("latitude"));
		stmt.setString(7, sensorData.get("longitude"));
		stmt.setString(8, sensorData.get("geofenceNumber"));
		stmt.setString(9, sensorData.get("geofenceEvent"));
		stmt.setString(10, sensorData.get("geofenceName"));
		stmt.setBoolean(11, true); // -- set flag to indicate its sensor alert
		stmt.setString(12, sensorData.get("productId"));
		stmt.executeUpdate();
		ResultSet rs = stmt.getGeneratedKeys();
		if (rs.next()) {
			id = rs.getLong(1);
		}
		rs.close();
		stmt.close();
		return id;
	}

	/**
	 * 
	 * @param conn
	 * @param sensorData
	 * @param column : column name in which alarm value is to be stored
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private long addAttributeSensorAlert(Connection conn, Map<String, String> sensorData, String column) throws SQLException, ParseException {
		long id = -1;
		PreparedStatement stmt = null;

		String sqlQurySensorAlert = "INSERT INTO sensor_data( device_id,container_id,"+ column + ", alarm_type, alarm_value ,status_time,latitude, longitude, is_alert, product_id) values(?,?,?,?,?,?,?,?,?,?)";
		stmt = conn.prepareStatement(sqlQurySensorAlert, Statement.RETURN_GENERATED_KEYS);
		stmt.setString(1, sensorData.get("deviceIdentifier"));
		stmt.setString(2, sensorData.get("containerId"));
		if (column != null && column.toLowerCase().contains(RuleConstants.SENSOR_ATTR_SHOCK)) {
			double result = Double.parseDouble(sensorData.get("alarmValue")) / 10000;
			DecimalFormat df = new DecimalFormat("#.#");
			stmt.setString(3, df.format(result));
			sensorData.put("shock", df.format(result));
		} else {
			stmt.setString(3, sensorData.get("alarmValue"));
		}
		
		stmt.setString(4, sensorData.get("alarmType"));
		stmt.setString(5, sensorData.get("alarmValue"));

		String alarmtime = sensorData.get("alarmTimeStamp");
		if (alarmtime == null) {
			stmt.setString(6, null);
		} else {
			//long a = Utility.getDateFormat().parse(alarmtime).getTime();
			long a = Utility.getUTCDate(alarmtime);
			stmt.setTimestamp(6, new java.sql.Timestamp(a));
		}
		stmt.setString(7, sensorData.get("latitude"));
		stmt.setString(8, sensorData.get("longitude"));
		stmt.setBoolean(9, true); // -- set flag to indicate its sensor alert
		stmt.setString(10, sensorData.get("productId"));
		stmt.executeUpdate();
		ResultSet rs = stmt.getGeneratedKeys();
		if (rs.next()) {
			id = rs.getLong(1);
		}
		rs.close();
		stmt.close();
		return id;
	}

	/**
	 * Creating a mapping between input attributes and table columns
	 */
	/*
	private static void populateParamColumnMap() {
		paramColumnMap.put("temperature", "temperature");
		paramColumnMap.put("tilt", "tilt");
		paramColumnMap.put("pressure", "pressure");
		paramColumnMap.put("humidity", "humidity");
		paramColumnMap.put("light", "light");
		paramColumnMap.put("growthLog", "growth_log");
		paramColumnMap.put("cumulativeLog", "cumulative_log");
		paramColumnMap.put("battery", "battery_rem");
		paramColumnMap.put("latitude", "latitude");
		paramColumnMap.put("longitude", "longitude");
		paramColumnMap.put("shock", "shock"); 
		paramColumnMap.put("vibration", "vibration"); 
		
	}
	*/

	/**
	 * <p>
	 * It returns a list of all attributes of a device
	 * </p>
	 * 
	 * @param sensorRequest
	 * @return SensorResponse
	 * @throws SQLException
	 * @throws ParseException 
	 */
	public SensorResponse getAttributeList(SensorRequest sensorRequest)	throws SQLException, ParseException {
		Connection conn = null;
		try {

			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			Sensor sensor = new Sensor();
			DeviceData devicedata = sensorRequest.getDeviceData();
			String deviceId = devicedata.getDeviceId();
			String packageId = devicedata.getPackageId();
			String productId = devicedata.getProductId();
			Boolean showAll = devicedata.getShowAll() == null ? false : devicedata.getShowAll();
			Boolean showMapAttribute = devicedata.getShowMapAttribute() == null ? false : devicedata.getShowMapAttribute();
			Long id = devicedata.getId();
			response.setDeviceId(deviceId);
			response.setPackageId(packageId);
			response.setProductId(productId);
			conn = DAOUtility.getInstance().getConnection();
			ArrayList<com.rfxcel.sensor.beans.Attribute> attrList = getAttributeDefinition(deviceId, showAll, showMapAttribute);
			paramColumnMap = Utility.populateParamColumnMap();
			if (attrList.size() > 0) {
				// need column name instead of attribute name to map to actual table columns
				StringBuffer columns = new StringBuffer();
				for (int i = 0; i < attrList.size(); i++) {
					if (paramColumnMap.get(attrList.get(i).getAttrName()) != null) {
						if (i != 0) {
							if(columns.length() != 0){
								columns.append(",");
							}
							
						}
						columns.append(paramColumnMap.get(attrList.get(i).getAttrName()));
					}
				}
				// if id is null, fetch latest values of attributes
				PreparedStatement stmt;
				String query;
				if (id == null) {
					query = "SELECT "+ columns.toString() + ", status_time, excursion_status from sensor_data where device_id = ? and container_id = ? and product_id = ? ORDER BY status_time DESC LIMIT 1";
					stmt = conn.prepareStatement(query);
					stmt.setString(1, deviceId);
					stmt.setString(2, packageId);
					stmt.setString(3, productId);
				}// Else fetch value corresponding to a given id
				else {
					query = "SELECT "+ columns.toString()+ ", status_time, excursion_status from sensor_data where device_id = ? and container_id = ? and id = ?";
					stmt = conn.prepareStatement(query);
					stmt.setString(1, deviceId);
					stmt.setString(2, packageId);
					stmt.setLong(3, id);
				}
				String attrName;
				Double value;
				com.rfxcel.sensor.beans.Attribute attribute;
				Integer roundOffTo;
				ResultSet rsAttrValue = stmt.executeQuery();
				Long statusTime = 1L;
				while (rsAttrValue.next()) {					
					if(rsAttrValue.getString("status_time") != null){
						statusTime = Utility.getUTCDate(rsAttrValue.getString("status_time"));
						sensor.setStatusTime(statusTime);
					}
					
					for (int i = 0; i < attrList.size(); i++) {
						attribute = attrList.get(i);
						attrName = attribute.getAttrName();
						if (paramColumnMap.get(attrName) != null) {
							value = rsAttrValue.getDouble(paramColumnMap.get(attrName));
							
							// If value is null, checking if it is because of null value or it is actually 0. If it is 0 then setting it to 0
							if (value == 0) {
								boolean wasNull = rsAttrValue.wasNull();
								if (wasNull) {
									attribute.setAttrValue(ATTR_VALUE_NULL);
								} else {
									roundOffTo = attribute.getRoundOffTo();
									if (roundOffTo != null) {
										value = Utility.round(value, roundOffTo);
									}
									attribute.setAttrValue(value);

								}
							} else {
								roundOffTo = attribute.getRoundOffTo();
								if (roundOffTo != null) {
									value = Utility.round(value, roundOffTo);
								}
								attribute.setAttrValue(value);
							}
							RuleVO rule = ruleCache.getRule(deviceId, packageId,productId, attrName);
							if(rule!= null && rule.getMinValue()!= null)
								attribute.setMin(rule.getMinValue().floatValue());
							if(rule!= null && rule.getMaxValue()!= null)
								attribute.setMax(rule.getMaxValue().floatValue());
							if(rule != null && rule.getAlert() !=null)
								attribute.setAlert(rule.getAlert() == 1? true : false);
						}
					}

				}
				rsAttrValue.close();
				stmt.close();
				for (int i = 0; i < attrList.size(); i++) {
					attribute = attrList.get(i);
					attrName = attribute.getAttrName();
					double attrValue = 0.0;
					if(attribute.getAttrValue() != null){
						attrValue = attribute.getAttrValue();
					}
					if (attrValue == ATTR_VALUE_NULL) {
						String colName = paramColumnMap.get(attrList.get(i).getAttrName());
						query = "SELECT " + colName  + " from sensor_data where device_id = ? and container_id = ? and product_id = ? and " + colName + " is NOT NULL ORDER BY status_time DESC LIMIT 1";
						logger.debug("Query(" + deviceId + "," + packageId + ") = " + query);
						stmt = conn.prepareStatement(query);
						stmt.setString(1, deviceId);
						stmt.setString(2, packageId);
						stmt.setString(3, productId);
						rsAttrValue = stmt.executeQuery();
						while (rsAttrValue.next()) {
							value = rsAttrValue.getDouble(colName);
							if (value == 0) {
								boolean wasNull = rsAttrValue.wasNull();
								if (!wasNull) {
									roundOffTo = attribute.getRoundOffTo();
									if (roundOffTo != null) {
										value = Utility.round(value, roundOffTo);
									}
									attribute.setAttrValue(value);
								}
							} else {
								roundOffTo = attribute.getRoundOffTo();
								if (roundOffTo != null) {
									value = Utility.round(value, roundOffTo);
								}
								attribute.setAttrValue(value);
							}
						}
					}
				}
			}			
			sensor.setAttributeList(attrList);
			response.setSensor(sensor);
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	public HashMap<String, com.rfxcel.sensor.beans.Attribute> getAttributeMap(Connection conn, String deviceId) throws SQLException {
		HashMap<String, com.rfxcel.sensor.beans.Attribute> attrList = new HashMap<String, com.rfxcel.sensor.beans.Attribute>();
		String searchQuery = "SELECT attr_id,attr_name,legend, unit from sensor_attr_type where show_graph=1 and device_type_id = (SELECT device_type_id from sensor_devices where device_id = ?)";
		PreparedStatement stmt = conn.prepareStatement(searchQuery);
		stmt.setString(1, deviceId);
		ResultSet rs = stmt.executeQuery();
		while (rs.next()) {
			com.rfxcel.sensor.beans.Attribute attr = new com.rfxcel.sensor.beans.Attribute();
			attr.setAttrId(Integer.parseInt(rs.getString("attr_id")));
			attr.setAttrName(rs.getString("attr_name"));
			attr.setLegend(rs.getString("legend"));
			attr.setUnit(rs.getString("unit"));
			attrList.put(rs.getString("attr_name"), attr);
		}
		rs.close();
		stmt.close();
		return attrList;
	}

	/**
	 * <p>
	 * this method would return attribute list based on show_graph value
	 * </p>
	 * 
	 * @param conn
	 * @param deviceId
	 * @param showGraph
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<com.rfxcel.sensor.beans.Attribute> getAttributeDefinition(String deviceId, Boolean showAll, Boolean showMapAttribute) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			ArrayList<com.rfxcel.sensor.beans.Attribute> attrList = new ArrayList<com.rfxcel.sensor.beans.Attribute>();
			String searchQuery;
			PreparedStatement stmt;
			if (showAll) {
				//fetching all attributes added for device type
				searchQuery = "SELECT attr_id,attr_name,legend, round_digits, unit from sensor_attr_type where scope=0 and device_type_id = (SELECT device_type_id from sensor_devices where device_id = ?)";
				stmt = conn.prepareStatement(searchQuery);
				stmt.setString(1, deviceId);
			} else {
				//fetching attributes with show_graph=1 added for device type
				searchQuery = "SELECT attr_id,attr_name,legend,round_digits, unit from sensor_attr_type where show_graph=1 and device_type_id = (SELECT device_type_id from sensor_devices where device_id = ?)";
				stmt = conn.prepareStatement(searchQuery);
				stmt.setString(1, deviceId);
			}

			//fetching attributes with show_map=1 added for device type
			if(showMapAttribute){
				searchQuery = "SELECT attr_id,attr_name,legend,round_digits, unit from sensor_attr_type where show_map=1 and device_type_id = (SELECT device_type_id from sensor_devices where device_id = ?)";
				stmt = conn.prepareStatement(searchQuery);
				stmt.setString(1, deviceId);
			}
			
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				com.rfxcel.sensor.beans.Attribute attr = new com.rfxcel.sensor.beans.Attribute();
				attr.setAttrId(Integer.parseInt(rs.getString("attr_id")));
				attr.setAttrName(rs.getString("attr_name"));
				attr.setLegend(rs.getString("legend"));
				int roundDigit = rs.getInt("round_digits");
				if (roundDigit >= 0) {
					boolean wasNull = rs.wasNull();
					if(wasNull){
						attr.setRoundOffTo(null);	
					}else{
						attr.setRoundOffTo(roundDigit);
					}
				}
				
				attr.setUnit(rs.getString("unit"));

				attrList.add(attr);
			}
			rs.close();
			stmt.close();
			return attrList;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * <p>
	 * returns list of devices starting with input devise Id's
	 * </p>
	 * 
	 * @param sensorRequest
	 * @return SensorResponse containing data for graph
	 * @throws SQLException
	 * @throws ParseException
	 */
	public SensorResponse getAttributeData(SensorRequest sensorRequest) throws SQLException, ParseException {
		Connection conn = null;
		try {
			String timeZoneOffset = userTimeZoneCache.getTimeZoneOffset(sensorRequest.getUser());
			if (timeZoneOffset == null) {
				timeZoneOffset = "+00:00";
			}
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			DeviceData deviceData = sensorRequest.getDeviceData();
			String deviceId = deviceData.getDeviceId();
			String containerId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			Long startDate =  deviceData.getStartDate();
			Long endDate = deviceData.getEndDate();
			response.setDeviceId(deviceId);
			response.setPackageId(containerId);

			Sensor sensor = new Sensor();
			ArrayList<AttributeData> attributesData = new ArrayList<AttributeData>();
			conn = DAOUtility.getInstance().getConnection();

			Float duration = deviceData.getDuration();
			List<DataPoint> dataPoints;
			AttributeData attrData;
			HashMap<String, RuleVO> thresholds = RuleCache.getInstance().getAttributeThreshold(deviceId, containerId, productId);
			//ArrayList<com.rfxcel.sensor.beans.Attribute> attrList = deviceData.getAttributeType();
			ArrayList<com.rfxcel.sensor.beans.Attribute> attrList = deviceData.getAttrType();
			// get attribute names by calling gwetAttribute Names method
			HashMap<String, com.rfxcel.sensor.beans.Attribute> attrMap = getAttributeMap(conn, deviceId);
			paramColumnMap = Utility.populateParamColumnMap();
			RuleVO ruleVO;
			if ((attrList == null || attrList.size() == 0)) {
				Comparator<Entry<String, com.rfxcel.sensor.beans.Attribute>> comp = new Comparator<Entry<String, com.rfxcel.sensor.beans.Attribute>>() {
					@Override
					public int compare(Entry<String, com.rfxcel.sensor.beans.Attribute> o1, Entry<String, com.rfxcel.sensor.beans.Attribute> o2) {
						if (o1.getValue().getAttrId() < o2.getValue().getAttrId())
							return (-1);
						if (o1.getValue().getAttrId() > o2.getValue().getAttrId())
							return (1);
						return 0;
					}
				};
				TreeSet<Entry<String, com.rfxcel.sensor.beans.Attribute>> tree = new TreeSet<Entry<String, com.rfxcel.sensor.beans.Attribute>>(comp);
				tree.addAll(attrMap.entrySet());
				ArrayList<Entry<String, com.rfxcel.sensor.beans.Attribute>> list = new ArrayList<Entry<String, com.rfxcel.sensor.beans.Attribute>>(tree);
				String attrName;
				String columnName;
				Double minValue;
				Double maxValue;
				for (Entry<String, com.rfxcel.sensor.beans.Attribute> entry : list) {
					com.rfxcel.sensor.beans.Attribute attr = entry.getValue();
					attrName = attr.getAttrName();
					columnName = paramColumnMap.get(attrName);// checking if corresponding column is present
					minValue = null;
					maxValue = null;
					if (columnName != null) {
						if(startDate != null && endDate!= null){
							dataPoints = getDataForStartEndDate(conn, deviceId, containerId,productId, startDate, endDate, columnName, timeZoneOffset);
						}
						else if (duration != null) {
							dataPoints = getResult4Slider(conn, deviceId, containerId,productId, duration, columnName, timeZoneOffset); // get data point for duration specified
						} else {
							dataPoints = getAllDataForAttribute(conn, deviceId, containerId,productId, columnName, timeZoneOffset); // get all the data points
						}
						boolean alert = false;
						if (thresholds != null && thresholds.get(attrName) != null) {
							ruleVO = thresholds.get(attrName);
							minValue = ruleVO.getMinValue() == null ? null : ruleVO.getMinValue();
							maxValue = ruleVO.getMaxValue() == null ? null : ruleVO.getMaxValue();
							if (ruleVO.getAlert() == RuleVO.ALERT_ON) {
								alert = true;
							}
						}
						attrData = new AttributeData(attr.getLegend(), attrName, dataPoints, minValue, maxValue, attr.getUnit(), alert);
						attrData = getRangeValues(attrData, deviceId);
						if (dataPoints != null && dataPoints.size() > 0) {
							DataPoint lastDataPoint = dataPoints.get(dataPoints.size() - 1);
							Long lastTimeStamp = lastDataPoint.getDateTime();
							attrData.setLastDateTime(lastTimeStamp);
						}
						attributesData.add(attrData);
					}
				}
				if(startDate != null && endDate!= null){
					sensor.setStartDate(startDate);
					sensor.setEndDate(endDate);
				}else if (duration != null) {
					int timeSpanInMin = (int) (duration * 60);
					String query ="SELECT MAX(status_time) as endDate ,DATE_SUB(MAX(status_time), INTERVAL ? Minute) as startDate from sensor_data where device_id =? AND container_id =? and product_id =?";
					PreparedStatement stmt = conn.prepareStatement(query);
					stmt.setInt(1,timeSpanInMin);
					stmt.setString(2, deviceId);
					stmt.setString(3, containerId);
					stmt.setString(4, productId);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()){
						if(rs.getString("startDate") != null)
							sensor.setStartDate(Utility.getUTCDate(rs.getString("startDate")));
						if(rs.getString("endDate") != null)
							sensor.setEndDate(Utility.getUTCDate(rs.getString("endDate")));
					}
				} else {
					String query ="SELECT MAX(status_time) as endDate ,MIN(status_time) as startDate from sensor_data where device_id =? AND container_id =? and product_id =?";
					PreparedStatement stmt = conn.prepareStatement(query);
					stmt.setString(1, deviceId);
					stmt.setString(2, containerId);
					stmt.setString(3, productId);
					ResultSet rs = stmt.executeQuery();
					while(rs.next()){
						if(rs.getString("startDate") != null)
							sensor.setStartDate(Utility.getUTCDate(rs.getString("startDate")));
						if(rs.getString("endDate") != null)
							sensor.setEndDate(Utility.getUTCDate(rs.getString("endDate")));
					}
				}
			} else {
				String attrName;
				String columnName;
				Double minValue;
				Double maxValue;
				String legend;
				String unit;
				for (com.rfxcel.sensor.beans.Attribute attr : attrList) {
					attrName = attr.getAttrName();
					columnName = paramColumnMap.get(attrName);// checking if corresponding column is present
					minValue = null;
					maxValue = null;
					if (columnName != null) {
						Long startDateTime = attr.getStartDateTime();
						if(startDateTime != null){// get incremental data points
							dataPoints = getResult4StartDate(conn, deviceId, containerId, startDateTime, columnName, timeZoneOffset); 
						}// get data points within given duration
						else if(startDate != null && endDate!= null){
							dataPoints = getDataForStartEndDate(conn, deviceId, containerId,productId, startDate, endDate, columnName, timeZoneOffset);
						}
						else if (duration != null) {
							dataPoints = getResult4Slider(conn, deviceId, containerId,productId, duration, columnName, timeZoneOffset); // get data point for duration specified
						} else {
							dataPoints = getAllDataForAttribute(conn, deviceId, containerId,productId, columnName, timeZoneOffset); // get all the data points
						}
						boolean alert = false;
						if (thresholds != null && thresholds.get(attrName) != null) {
							ruleVO = thresholds.get(attrName);
							minValue = ruleVO.getMinValue() == null ? null : ruleVO.getMinValue();
							maxValue = ruleVO.getMaxValue() == null ? null : ruleVO.getMaxValue();
							if (ruleVO.getAlert() == RuleVO.ALERT_ON) {
								alert = true;
							}
						}
						legend = attrMap.get(attrName) == null ? attrName : attrMap.get(attrName).getLegend();
						unit = attrMap.get(attrName).getUnit();
						attrData = new AttributeData(legend, attrName, dataPoints, minValue, maxValue, unit, alert);

						if (dataPoints != null && dataPoints.size() > 0) {
							DataPoint lastDataPoint = dataPoints.get(dataPoints.size() - 1);
							Long lastTimeStamp = lastDataPoint.getDateTime();
							attrData.setLastDateTime(lastTimeStamp);
						} else {
							attrData.setLastDateTime(attr.getStartDateTime());
						}
						attributesData.add(attrData);
					}
				}
			}

			sensor.setAttributes(attributesData);
			response.setSensor(sensor);
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}


	private AttributeData getRangeValues(AttributeData attrData, String deviceId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			Integer deviceTypeId = deviceCache.getDeviceTypeId(deviceId);
			if(deviceTypeId!= null){
				String query ="SELECT range_min, range_max from sensor_attr_type where device_type_id=? and attr_name = ?";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setInt(1, deviceTypeId);
				stmt.setString(2, attrData.getAttrName());
				ResultSet rs = stmt.executeQuery();
				while(rs.next()){
					attrData.setRangeMin(rs.getDouble("range_min"));
					attrData.setRangeMax(rs.getDouble("range_max"));
				}
			}
		}finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return attrData;
	}

	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param containerId
	 * @param startDate
	 * @param column
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private List<DataPoint> getResult4StartDate(Connection conn, String deviceId, String containerId, Long startDate, String column, String offset) throws SQLException, ParseException {

		String sDate = startDate != null ? Utility.getUTCDateString(startDate) : null;

		String query = "SELECT "+ column+ ",status_time FROM sensor_data where device_id = ? AND container_id = ? AND "
				+ column + " is NOT NULL AND (DATE_FORMAT(status_time,'%Y-%m-%d %T') > ?) order by status_time";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, deviceId);
		stmt.setString(2, containerId);
		stmt.setString(3, sDate);
		ResultSet rs = stmt.executeQuery();
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		DataPoint dataPoint;
		Double value;
		while (rs.next()) {
			value = rs.getDouble(column);
			value = Utility.round(value, 3);
			dataPoint = new DataPoint();
			if (rs.getString("status_time") != null) {
				
				dataPoint.setDateTime(Utility.getUTCDate(rs.getString("status_time")));
			} else {
				dataPoint.setDateTime(0l);
			}
			dataPoint.setDataPoint(value);
			dataPoints.add(dataPoint);// data is already in ascending format
		}
		rs.close();
		stmt.close();
		return dataPoints;
	}
	
	
	
	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param containerId
	 * @param productId 
	 * @param startDate
	 * @param endDate
	 * @param column
	 * @param offset
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private List<DataPoint> getDataForStartEndDate(Connection conn, String deviceId, String containerId, String productId, Long startDate, Long endDate,String column, String offset) throws SQLException, ParseException {
		
		String sDate = startDate != null ? Utility.getUTCDateString(startDate) : null;
		String eDate = endDate != null ? Utility.getUTCDateString(endDate) : null;
	
		String query = "SELECT "+ column+ ", status_time  FROM sensor_data where device_id = ? AND container_id = ? AND product_id = ? AND "
				+ column + " is NOT NULL AND (DATE_FORMAT(status_time,'%Y-%m-%d %T') BETWEEN ? and ?) order by status_time";
		
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, deviceId);
		stmt.setString(2, containerId);
		stmt.setString(3, productId);
		stmt.setString(4, sDate);
		stmt.setString(5, eDate);
		ResultSet rs = stmt.executeQuery();
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		DataPoint dataPoint;
		Double value;
		while (rs.next()) {
			value = rs.getDouble(column);
			value = Utility.round(value, 3);
			dataPoint = new DataPoint();
			if (rs.getString("status_time") != null) {
				dataPoint.setDateTime(Utility.getUTCDate(rs.getString("status_time")));
				
			} else {
				dataPoint.setDateTime(0l);
			}
			dataPoint.setDataPoint(value);
			dataPoints.add(dataPoint);// data is already in ascending format
		}
		rs.close();
		stmt.close();
		return dataPoints;
	}
	

	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param productId 
	 * @param dpCount
	 * @param column
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private List<DataPoint> getResult4Slider(Connection conn, String deviceId, 	String containerId, String productId, Float timespan, String column, String offset) throws SQLException, ParseException {
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		PreparedStatement stmt;
		ResultSet rs;
		String maxDate = null;
		String query = "SELECT MAX(status_time) from sensor_data where device_id = ? AND container_id = ? and product_id = ? ";
		stmt = conn.prepareStatement(query);
		stmt.setString(1, deviceId);
		stmt.setString(2, containerId);
		stmt.setString(3, productId);
		rs = stmt.executeQuery();
		while (rs.next()) {
			maxDate = rs.getString(1);
		}
		if (maxDate != null) {
			int timeSpanInMin = (int) (timespan * 60);
			
			query = "SELECT "+ column + ", status_time FROM sensor_data where device_id = ? AND container_id = ? AND product_id = ? AND "
					+ column + " is NOT NULL AND (DATE_FORMAT(status_time,'%Y-%m-%d %T') BETWEEN ( DATE_SUB(?, INTERVAL ? Minute)) and ? ) order by status_time DESC";

			
			stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, containerId);
			stmt.setString(3, productId);
			stmt.setString(4, maxDate);
			stmt.setInt(5, timeSpanInMin);
			stmt.setString(6, maxDate);
			rs = stmt.executeQuery();

			DataPoint dataPoint;
			Double value;
			while (rs.next()) {
				value = rs.getDouble(column);
				value = Utility.round(value, 3);
				dataPoint = new DataPoint();
				if (rs.getString("status_time") != null) {
				
					dataPoint.setDateTime(Utility.getUTCDate(rs.getString("status_time")));
				} else {
					dataPoint.setDateTime(0l);
				}
				dataPoint.setDataPoint(value);
				dataPoints.add(0, dataPoint);// arranging data in ascending order
			}
		}
		rs.close();
		stmt.close();
		return dataPoints;
	}

	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param containerId
	 * @param column
	 * @param offset
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private List<DataPoint> getAllDataForAttribute(Connection conn, String deviceId, String containerId,String productId, String column, String offset) throws SQLException, ParseException {
		List<DataPoint> dataPoints = new ArrayList<DataPoint>();
		PreparedStatement stmt;
		ResultSet rs;
		String query = "SELECT " + column + ", status_time FROM sensor_data where device_id = ? AND container_id = ? AND product_id = ? AND "
				+ column + " is NOT NULL order by status_time DESC";
		
		stmt = conn.prepareStatement(query);
		stmt.setString(1, deviceId);
		stmt.setString(2, containerId);
		stmt.setString(3, productId);
		rs = stmt.executeQuery();

		DataPoint dataPoint;
		Double value;
		while (rs.next()) {
			value = rs.getDouble(column);
			value = Utility.round(value, 3);
			dataPoint = new DataPoint();
			if (rs.getString("status_time") != null) {
				
				dataPoint.setDateTime(Utility.getUTCDate(rs.getString("status_time")));
			} else {
				dataPoint.setDateTime(0l);
			}
			dataPoint.setDataPoint(value);
			dataPoints.add(0, dataPoint);// arranging data in ascending order
		}
		rs.close();
		stmt.close();
		return dataPoints;
	}

	/**
	 * This is used to get device logs for same location devices.
	 * 
	 * @return
	 */
	public ConcurrentHashMap<String, DeviceInfo> getDeviceLogForLatitude() {
		Connection conn = null;
		ConcurrentHashMap<String, DeviceInfo> sameLocationdeviceNotificationLog = new ConcurrentHashMap<String, DeviceInfo>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("SELECT log_id, device_id, start_time, end_time as end_time, device_log_type, notify_count FROM sensor_device_log t1 WHERE t1.end_time = (SELECT MAX(t2.end_time) FROM sensor_device_log t2 WHERE t2.device_id = t1.device_id and t2.device_log_type=t1.device_log_type) and t1.device_log_type=2");

			while (rs.next()) {
				DeviceInfo deviceInfo = new DeviceInfo();
				String deviceId = rs.getString("device_id");
				deviceInfo.setId(rs.getLong("log_id"));
				deviceInfo.setDeviceId(deviceId);
				deviceInfo.setDeviceLogType(rs.getInt("device_log_type"));
				deviceInfo.setNotifyCount(rs.getInt("notify_count"));
				deviceInfo.setStartTime(formatter.parseDateTime(Utility.getDateFormat().format(Utility.getDateFormat().parse(rs.getString("start_time")))));
				deviceInfo.setEndTime(formatter.parseDateTime(Utility.getDateFormat().format(Utility.getDateFormat().parse(rs.getString("end_time")))));
				sameLocationdeviceNotificationLog.put(deviceId, deviceInfo);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("error while getting device log " + e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return sameLocationdeviceNotificationLog;
	}

	/**
	 * 
	 * This is used to get the latest log value. whenever we calculate bacterial
	 * growth log we get cumulative log start to end.
	 * 
	 * @return
	 * */
	public HashMap<String, DeviceInfo> getDeviceBactLog() {
		DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		Connection conn = null;
		HashMap<String, DeviceInfo> DeviceBactLog = new HashMap<String, DeviceInfo>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			Statement stmt = conn.createStatement();
			
			/* Below query was taking time so added max for both the start and  end time and grouping by deviceId
			  ResultSet rs = stmt.executeQuery(  "select device_id, start_time, end_time from sensor_bacterial_growth_log as sbgl "
			  + "where end_time = (select max(end_time) from sensor_bacterial_growth_log l1 where sbgl.device_id = l1.device_id)" );*/
			ResultSet rs = stmt.executeQuery("select device_id, max(start_time) as start_time, max(end_time) as end_time  from sensor_bacterial_growth_log group by device_id");
			while (rs.next()) {
				DeviceInfo deviceInfo = new DeviceInfo();
				deviceInfo.setDeviceId(rs.getString("device_id"));
				deviceInfo.setStart(formatter.parseDateTime(Utility.getDateFormat().format(Utility.getDateFormat().parse(rs.getString("start_time")))).getMillis());
				deviceInfo.setEnd(formatter.parseDateTime(Utility.getDateFormat().format(Utility.getDateFormat().parse(rs.getString("end_time")))).getMillis());
				DeviceBactLog.put(rs.getString("device_id"), deviceInfo);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return DeviceBactLog;
	}

	/**
	 * Inserts device attribute(Eg. Temperature) log for its deviations
	 * 
	 * @param deviceInfo
	 */
	public DeviceInfo addDeviceLog(DeviceInfo deviceInfo) {
		Connection conn = null;
		long id = -1;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String sqlQury = "INSERT INTO sensor_device_log(device_id, attribute_name, attribute_value, start_time, end_time,device_log_type,notify_count) values (?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQury, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1, deviceInfo.getDeviceId());
			stmt.setString(2, deviceInfo.getAttributeName());
			stmt.setString(3, deviceInfo.getAttributeValue());

			Timestamp timeStampStart = Utility.jodaToSQLTimestamp(deviceInfo.getStartTime().toLocalDateTime());
			Timestamp timeStampEnd = Utility.jodaToSQLTimestamp(deviceInfo.getEndTime().toLocalDateTime());
			stmt.setString(4, Utility.getDateFormat().format(Utility.getDateFormat().parse(timeStampStart.toString())));
			stmt.setString(5, Utility.getDateFormat().format(Utility.getDateFormat().parse(timeStampEnd.toString())));
			stmt.setInt(6, deviceInfo.getDeviceLogType());
			stmt.setInt(7, deviceInfo.getNotifyCount());

			stmt.executeUpdate();
			ResultSet rs = stmt.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getLong(1);
			}
			deviceInfo.setId(id);
			rs.close();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceInfo;
	}

	/**
	 * 
	 * @param deviceInfo
	 */
	public void updateDeviceLog(DeviceInfo deviceInfo){
		Connection conn = null;	
		try {
			Timestamp timeStampEnd = Utility.jodaToSQLTimestamp(deviceInfo.getEndTime().toLocalDateTime());
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_device_log SET notify_count = ?, end_time = ? WHERE log_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, deviceInfo.getNotifyCount());
			stmt.setString(2, Utility.getDateFormat().format(Utility.getDateFormat().parse(timeStampEnd.toString())));
			stmt.setLong(3, deviceInfo.getId());
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * Deletes device attribute(Eg. Temperature) log when it comes back to
	 * normal
	 * 
	 * @param deviceInfo
	 */
	public void removeDeviceLog(DeviceInfo deviceInfo, String attrName) {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String sqlQury = "delete from sensor_device_log where device_id=? and device_log_type =? and attribute_name = ?";
			PreparedStatement stmt = conn.prepareStatement(sqlQury);
			stmt.setString(1, deviceInfo.getDeviceId());
			stmt.setInt(2, deviceInfo.getDeviceLogType());
			stmt.setString(3, attrName);
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	
	/**
	 * @param conn
	 * @param deviceId
	 * @throws SQLException 
	 */
	public void removeDeviceLog(Connection conn, String deviceId) throws SQLException {
		try {
			String sqlQury = "delete from sensor_device_log where device_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQury);
			stmt.setString(1, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
		}
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<Integer, HashMap<String, Attribute>> getAttributesForDeviceTypes() {
		Connection conn = null;
		HashMap<Integer, HashMap<String, Attribute>> deviceAttrMap = new HashMap<Integer, HashMap<String, Attribute>>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery("select attr_name, scope, factor, excursion_duration, legend, multiply_factor, device_type_id,"
							+ " round_digits, notify_limit,alert_on_reset from sensor_attr_type");
			int deviceTypeId;
			String attrName;
			HashMap<String, Attribute> attrMap;
			while (rs.next()) {
				attrName = rs.getString("attr_name");
				Attribute attr = new Attribute();
				attr.setName(attrName);
				attr.setScope(rs.getObject("scope") != null ? rs.getInt("scope") : 0);
				attr.setFactor(rs.getObject("factor") != null ? rs.getDouble("factor") : 0);
				attr.setLegend(rs.getString("legend"));
				attr.setExcursionDuration(rs.getObject("excursion_duration") != null ? rs.getInt("excursion_duration") : 0);
				attr.setMultiplyFactor(rs.getFloat("multiply_factor"));
				deviceTypeId = rs.getInt("device_type_id");
				attr.setDeviceTypeId(deviceTypeId);
				attr.setNotifyLimit(rs.getInt("notify_limit"));
				attr.setRoundOffTo(rs.getObject("round_digits") != null ? rs.getInt("round_digits") : null);
				attr.setAlertOnReset(rs.getInt("alert_on_reset"));
				// Add it to the deviceTypeId map
				if (deviceAttrMap.get(deviceTypeId) == null) {
					attrMap = new HashMap<String, Attribute>();
					attrMap.put(attrName, attr);
					deviceAttrMap.put(deviceTypeId, attrMap);
				} else {
					deviceAttrMap.get(deviceTypeId).put(attrName, attr);
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while getting device type adn attribute mapping. Error message is "	+ e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceAttrMap;

	}

	
	/**
	 * 
	 * @param deviceTypeId
	 * @return
	 */
	public  HashMap<String, Attribute> getAttributesForDeviceType(Integer deviceTypeId) {
		Connection conn = null;
		HashMap<String, Attribute> deviceAttrMap = new HashMap<String, Attribute>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select attr_name, scope, factor, excursion_duration, legend, multiply_factor, device_type_id,"
					+ " round_digits, notify_limit,alert_on_reset from sensor_attr_type where device_type_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, deviceTypeId);
			ResultSet rs = stmt.executeQuery();
			String attrName;
			while (rs.next()) {
				attrName = rs.getString("attr_name");
				Attribute attr = new Attribute();
				attr.setName(attrName);
				attr.setScope(rs.getObject("scope") != null ? rs.getInt("scope") : 0);
				attr.setFactor(rs.getObject("factor") != null ? rs.getDouble("factor") : 0);
				attr.setLegend(rs.getString("legend"));
				attr.setExcursionDuration(rs.getObject("excursion_duration") != null ? rs.getInt("excursion_duration") : 0);
				attr.setMultiplyFactor(rs.getFloat("multiply_factor"));
				deviceTypeId = rs.getInt("device_type_id");
				attr.setDeviceTypeId(deviceTypeId);
				attr.setNotifyLimit(rs.getInt("notify_limit"));
				attr.setRoundOffTo(rs.getObject("round_digits") != null ? rs.getInt("round_digits") : null);
				attr.setAlertOnReset(rs.getInt("alert_on_reset"));

				deviceAttrMap.put(attrName, attr);
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while getting device type adn attribute mapping. Error message is "	+ e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceAttrMap;
	}
	

	/**
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceIsPresent(String deviceId, int orgId) throws SQLException {
		Connection conn = null;
		Boolean isPresent = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT count(*) from sensor_devices WHERE device_id = ? and org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count != 0) {
				isPresent = true;
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * <p> to check if the device with given id is configured in multi-tenant application </p>
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceIsPresent(String deviceId) throws SQLException {
		Connection conn = null;
		Boolean isPresent = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			// String query = SELECT EXISTS(SELECT 1 from sensor_devices WHERE device_id = ? )
			String query = "SELECT count(*) from sensor_devices WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count != 0) {
				isPresent = true;
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}

	/**
	 * 
	 * @param deviceId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceIsActive(String deviceId) throws SQLException {
		Connection conn = null;
		Boolean active = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT active from sensor_devices WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				active = rs.getBoolean("active");
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return active;
	}

	/**
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public long getDeviceGroupId(String deviceId, long orgId) throws SQLException {
		Connection conn = null;
		long groupId = -1;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT group_id from sensor_devices WHERE device_id = ? and org_id = ? ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setLong(2, orgId);
			ResultSet rs = stmt.executeQuery();

			while (rs.next()) {
				groupId = rs.getLong("group_id");
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return groupId;
	}

	/**
	 * @param packageId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public long getPackageGroupId(String packageId, long orgId) throws SQLException {
		Connection conn = null;
		long groupId = -1;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT group_id from sensor_package WHERE package_id = ? and org_id = ? ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setLong(2, orgId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				groupId = rs.getLong("group_id");
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return groupId;
	}

	/**
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public void resetData() throws Exception {
		Connection conn = null;
		CallableStatement stmt;
		try {
			logger.info("*** CALL resetData() ***");
			conn = DAOUtility.getInstance().getConnection();
			stmt = conn.prepareCall("{call resetData()}");
			stmt.executeQuery();
			stmt.close();

			/*
			 * ClassLoader classLoader =
			 * Thread.currentThread().getContextClassLoader(); InputStream
			 * inputStream = classLoader.getResourceAsStream("resetData.sql");
			 * StringWriter stringWriter = new StringWriter();
			 * IOUtils.copy(inputStream, stringWriter);
			 * 
			 * conn = DAOUtility.getInstance().getConnection(); Statement stat =
			 * conn.createStatement(); stat.addBatch(stringWriter.toString());
			 * stat.executeBatch();
			 */

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}

	}

	/**
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public void resetOrg(int orgId) throws Exception {
		try{
			DAOUtility.runInTransaction(conn->{
				SendumDAO.getInstance().deactivateDevicesForOrg(conn,orgId);
				logger.info("*** Calling resetOrg(" + orgId + ")...");

				PreparedStatement stmt = conn.prepareCall("{call resetOrg(" + orgId + ")}");
				stmt.executeQuery();
				stmt.close();
				 
				return null;
			});
		}finally{
		}
	}
	/*
	 * private boolean isComment(String line) { if ((line != null) &&
	 * (line.length() > 0)) return (line.charAt(0) == '#' || line.charAt(0) ==
	 * '-' || line.charAt(0) == '�' || line.charAt(0) == '/'); return false; }
	 * 
	 * private boolean checkStatementEnds(String s) { return (s.indexOf(";") !=
	 * -1); }
	 */

	/**
	 * returns fields for a device status
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public SensorResponse getDeviceStatus(SensorRequest request) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			SensorResponse res = new SensorResponse(Utility.getDateFormat().format(new Date()));
			// res.setDeviceId(request.getDeviceData().getDeviceId());
			String deviceId = request.getDeviceData().getDeviceId();
			res.setDeviceId(deviceId);

			if (request.getDeviceData().getFields() != null) {
				Sensor sensor = new Sensor();
				ArrayList<String> inputFields = request.getDeviceData().getFields();
				DeviceFields fields = null;
				String queryLastFieldVal = "SELECT battery_rem,latitude,longitude,temperature from sensor_data where device_id = ? ORDER BY status_time DESC LIMIT 1";
				PreparedStatement stmtFieldValue = conn.prepareStatement(queryLastFieldVal);
				stmtFieldValue.setString(1, deviceId);
				ResultSet rsFieldValue = stmtFieldValue.executeQuery();
				while (rsFieldValue.next()) {
					fields = new DeviceFields();
					if (inputFields != null && inputFields.size() > 0) {
						if (inputFields.contains("batteryStatus")) {
							fields.setBatteryStatus(rsFieldValue.getString("battery_rem"));
						}
						if (inputFields.contains("latitude")) {
							fields.setLatitude(rsFieldValue.getString("latitude"));
						}
						if (inputFields.contains("longitude")) {
							fields.setLongitude(rsFieldValue.getString("longitude"));
						}
						if (inputFields.contains("temperature")) {
							fields.setTemperature(rsFieldValue.getDouble("temperature"));
						}
						if (inputFields.contains("onlineStatus")) {
							fields.setOnlineStatus("true"); // TODO remove with appropriate value
						}
					} else {
						fields.setBatteryStatus(rsFieldValue.getString("battery_rem"));
						fields.setLatitude(rsFieldValue.getString("latitude"));
						fields.setLongitude(rsFieldValue.getString("longitude"));
						fields.setTemperature(rsFieldValue.getDouble("temperature"));
						fields.setOnlineStatus("true"); // TODO remove with appropriate value
					}
				}
				rsFieldValue.close();
				stmtFieldValue.close();

				boolean isAssociated = false;
				String searchQuery = "SELECT relation_status from sensor_associate where child_id = ?";
				PreparedStatement stmt = conn.prepareStatement(searchQuery);
				stmt.setString(1, deviceId);
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) {
					if (rs.getInt("relation_status") == 1) {
						isAssociated = true;
					} else {
						isAssociated = false;
					}
				}
				rs.close();
				stmt.close();
				if (inputFields != null && inputFields.size() > 0) {
					if (inputFields.contains("isAssociated")) {
						if (fields == null)
							fields = new DeviceFields();
						fields.setAssociated(isAssociated);
					}
				} else {
					if (fields != null)
						fields.setAssociated(isAssociated);
				}

				sensor.setFields(fields);
				res.setSensor(sensor);
			} else {
				String searchQuery = "SELECT state from sensor_devices where device_id = ?";
				PreparedStatement stmt = conn.prepareStatement(searchQuery);
				stmt.setString(1, deviceId);
				ResultSet rs = stmt.executeQuery();
				if (rs.next()) {
					if (rs.getInt("state") == 1) {
						res.setResponseMessage("DeviceId is in commissioned status");
					}
					if (rs.getInt("state") == 2) {
						res.setResponseMessage("DeviceId is in associated status");
					}
					if (rs.getInt("state") == 3) {
						res.setResponseMessage("DeviceId is in disassociated status");
					}
					if (rs.getInt("state") == 4) {
						res.setResponseMessage("DeviceId is in decommissioned status");
					}
				} else {
					res.setResponseMessage("DeviceId(" + deviceId+ ") not found/commissioned ");
				}
				rs.close();
				stmt.close();
			}
			return res;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param orgId
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfPackageIsPresent(String packageId, String productId, int orgId)	throws SQLException {
		Connection conn = null;
		Boolean isPresent = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT count(*) from sensor_package WHERE package_id = ? and org_id = ? and product_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setInt(2, orgId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count != 0) {
				isPresent = true;
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}

	/**
	 * returns device attached to a package
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public SensorResponse getPackageStatus(SensorRequest request) throws SQLException {
		Connection conn = null;
		SensorResponse res = new SensorResponse(Utility.getDateFormat().format(new Date()));
		try {
			conn = DAOUtility.getInstance().getConnection();
			String packageId = request.getDeviceData().getPackageId();
			String searchQuery = "SELECT child_id ,relation_status from sensor_associate where parent_id = ? ";
			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setString(1, packageId);
			ResultSet rs = stmt.executeQuery();
			if (rs.next()) {
				res.setDeviceId(rs.getString("child_id"));
				res.setResponseStatus(rs.getString("relation_status"));
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return res;
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public SensorResponse getShipmentList(SensorRequest request) throws SQLException, ParseException {
		Connection conn = null;
		try {
			String searchType = request.getSearchType();
			boolean showMap = request.isShowMap();
			int orgId = request.getOrgId();
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			String timeZoneOffset = userTimeZoneCache.getTimeZoneOffset(request.getUser());
			if (timeZoneOffset == null) {
				timeZoneOffset = "+00:00";
			}
			conn = DAOUtility.getInstance().getConnection();

			Sensor sensor = null;
			if (SensorConstant.SEARCH_TYPE_CURRENT_SHIPMENTS.equalsIgnoreCase(searchType)) {
				sensor = getCurrentShipments(conn, showMap, timeZoneOffset, orgId);
			} else if (SensorConstant.SEARCH_TYPE_PENDING_SHIPMENTS.equalsIgnoreCase(searchType)) {
				sensor = getPendingShipments(conn, timeZoneOffset, orgId);
			} else if (SensorConstant.SEARCH_TYPE_COMPLETED_SHIPMENTS.equalsIgnoreCase(searchType)) {
				sensor = getCompletedShipments(conn, showMap, timeZoneOffset, orgId);
			}
			response.setSensor(sensor);
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * <p>
	 * Current shipments are shipments for which atleast one data point is
	 * present in sensor_data table and association status is 1
	 * 
	 * @param connection
	 * @param showMap
	 * @param timeZoneOffset
	 * @param groupId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private static Sensor getCurrentShipments(Connection connection, boolean showMap, String timeZoneOffset, int orgId) throws SQLException, ParseException {
		Sensor sensor = new Sensor();
		ArrayList<DeviceInfoDetails> shipmentList = new ArrayList<DeviceInfoDetails>();
		
		/*String searchShipments = "SELECT a.child_id, a.parent_id, a.product_id, a.excursion_status,a.add_favorite, CONVERT_TZ(a.create_datetime,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as create_datetime from sensor_associate a WHERE EXISTS"
				+ " (SELECT 1 FROM sensor_data s WHERE s.device_id = a.child_id and s.container_id = a.parent_id and s.product_id = a.product_id LIMIT 1)"
				+ " and a.relation_status=1 and a.org_id = ? ORDER BY add_favorite DESC";*/
		
		String searchShipments = "SELECT a.child_id, a.parent_id, a.product_id, a.rts_item_id, a.rts_serial_number, a.excursion_status,a.add_favorite, CONVERT_TZ(a.create_datetime,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as create_datetime from sensor_associate a"
								+ " where a.relation_status=1 and a.org_id = ? ORDER BY add_favorite DESC";
		
		
		PreparedStatement stmt = connection.prepareStatement(searchShipments);
		stmt.setInt(1, orgId);
		ResultSet rs = stmt.executeQuery();
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		String productId;
		String lastUpdatedTimeQuery;
		while (rs.next()) {
			String deviceId = rs.getString("child_id");
			String containerId = rs.getString("parent_id");
			productId = rs.getString("product_id");
			String rtsItemId = rs.getString("rts_item_id");
			String rtsSerialNumber = rs.getString("rts_serial_number");
		
			// Get the last update time for data
			DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails(deviceId, containerId, productId, rs.getInt("excursion_status"), rs.getInt("add_favorite"));
			deviceInfoDetails.setRtsItemId(rtsItemId);
			deviceInfoDetails.setRtsSerialNumber(rtsSerialNumber);
			String createdOn = "";
			if (rs.getString("create_datetime") != null) {
				createdOn = Utility.getSimpleDateFormat().format(rs.getTimestamp("create_datetime"));
				
			}
			deviceInfoDetails.setCreatedOn(createdOn);
			lastUpdatedTimeQuery = "select status_time from sensor_data where device_id =? and container_id =? and product_id = ? order by status_time desc limit 1";
			PreparedStatement pstsmt = connection.prepareStatement(lastUpdatedTimeQuery);
			pstsmt.setString(1, deviceId);
			pstsmt.setString(2, containerId);
			pstsmt.setString(3, productId);
			ResultSet rSet = pstsmt.executeQuery();
			Long statTime = null;
			while (rSet.next()) {
				statTime = Utility.getUTCDate(rSet.getString("status_time"));
			}
			deviceInfoDetails.setUpdatedOn(statTime);
			rSet.close();
			pstsmt.close();
			if (showMap) {
			
				String searchData = "SELECT device_id, container_id, longitude, latitude, status_time, excursion_status FROM sensor_data"
						+ " where device_id = ? AND container_id=? AND product_id=? AND longitude is NOT NULL AND latitude is NOT NULL order by status_time DESC LIMIT 1";
				PreparedStatement stmtdata = connection.prepareStatement(searchData);
				stmtdata.setString(1, deviceId);
				stmtdata.setString(2, containerId);
				stmtdata.setString(3, productId);
				ResultSet rsdata = stmtdata.executeQuery();
				Location location = new Location(deviceId, containerId, productId);
				while (rsdata.next()) {
					long datetime = -1L;
					if (rsdata.getString("status_time") != null) {
						datetime = Utility.getUTCDate(rsdata.getString("status_time"));
					}
					location.setDateTime(datetime);
					location.setLat(rsdata.getString("latitude"));
					location.setLang(rsdata.getString("longitude"));
					location.setPairStatus(rsdata.getInt("excursion_status"));
				}
				
				// do not add locations to the response if lat and lang values are empty
				if(location.getLat() != null &&  location.getLang() != null) {
					map.getLocations().add(location);	
				}
				
				rsdata.close();
				stmtdata.close();
			}
			shipmentList.add(deviceInfoDetails);
		}
		sensor.setShipmentList(shipmentList);
		sensor.setMap(map);
		rs.close();
		stmt.close();
		return sensor;
	}

	/**
	 * <p>
	 * Get list of pending shipments, pending shipments are shipments for which
	 * no data point in present in sensor_data table and association status is 1
	 * 
	 * @param connection
	 * @param timeZoneOffset
	 * @param groupId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private static Sensor getPendingShipments(Connection connection, String timeZoneOffset, int orgId) throws SQLException, ParseException {
		Sensor sensor = new Sensor();
		ArrayList<DeviceInfoDetails> shipmentList = new ArrayList<DeviceInfoDetails>();
		
		String searchShipments = "SELECT a.child_id, a.parent_id, a.product_id, a.excursion_status,a.add_favorite, CONVERT_TZ(a.create_datetime,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as create_datetime from sensor_associate a  WHERE NOT EXISTS "
				+ " (SELECT 1 FROM sensor_data s WHERE s.device_id = a.child_id and s.container_id = a.parent_id and s.product_id = a.product_id LIMIT 1) "
				+ " and a.relation_status=1 and a.org_id = ? ORDER BY add_favorite DESC";
		PreparedStatement stmt = connection.prepareStatement(searchShipments);
		stmt.setInt(1, orgId);
		ResultSet rs = stmt.executeQuery();
		String productId = "";
		while (rs.next()) {
			String deviceId = rs.getString("child_id");
			String containerId = rs.getString("parent_id");
			productId = rs.getString("product_id");		
			String createdOn = "";
			if (rs.getString("create_datetime") != null) {
				createdOn = Utility.getSimpleDateFormatDateStr(rs.getTimestamp("create_datetime"));
			}
			DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails(deviceId, containerId, productId, rs.getInt("excursion_status"), rs.getInt("add_favorite"));
			deviceInfoDetails.setCreatedOn(createdOn);
			deviceInfoDetails.setUpdatedOn(1l);// Null values are omitted from response so setting it to empty string
			shipmentList.add(deviceInfoDetails);
		}
		sensor.setShipmentList(shipmentList);
		rs.close();
		stmt.close();
		return sensor;
	}

	/**
	 * 
	 */
	/**
	 * @param connection
	 * @param timeZoneOffset
	 * @param groupId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private static Sensor getCompletedShipments(Connection connection, boolean showMap,String timeZoneOffset, int orgId) throws SQLException, ParseException {
		Sensor sensor = new Sensor();
		ArrayList<DeviceInfoDetails> shipmentList = new ArrayList<DeviceInfoDetails>();
		String searchShipments = "SELECT child_id, parent_id, product_id, excursion_status,add_favorite, CONVERT_TZ(create_datetime,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as create_datetime from sensor_associate where relation_status = 0 and org_id = ? ORDER BY add_favorite DESC";
		PreparedStatement stmt = connection.prepareStatement(searchShipments);
		stmt.setInt(1, orgId);
		ResultSet rs = stmt.executeQuery();
		String productId;
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		while (rs.next()) {
			String deviceId = rs.getString("child_id");
			String containerId = rs.getString("parent_id");
			productId = rs.getString("product_id");
			
			DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails(deviceId, containerId, productId, rs.getInt("excursion_status"), rs.getInt("add_favorite"));
			String createdOn = "";
			if (rs.getString("create_datetime") != null) {
				createdOn = Utility.getSimpleDateFormatDateStr(rs.getTimestamp("create_datetime"));
			}
			deviceInfoDetails.setCreatedOn(createdOn);

			// setting last updated time
			String lastUpdatedTimeQuery = "select status_time from sensor_data where device_id =? and container_id =? AND product_id =? order by status_time desc limit 1";
			PreparedStatement pstsmt = connection.prepareStatement(lastUpdatedTimeQuery);
			pstsmt.setString(1, deviceId);
			pstsmt.setString(2, containerId);
			pstsmt.setString(3, productId);
			ResultSet rSet = pstsmt.executeQuery();
			Long statTime = null;
			while (rSet.next()) {
				statTime = Utility.getUTCDate(rSet.getString("status_time"));
			}

			deviceInfoDetails.setUpdatedOn(statTime);
			shipmentList.add(deviceInfoDetails);
			rSet.close();
			pstsmt.close();
			if (showMap) {
				
				String searchData = "SELECT longitude, latitude, status_time, excursion_status FROM sensor_data"
						+ " where device_id = ? AND container_id=? AND product_id =? AND longitude is NOT NULL AND latitude is NOT NULL order by status_time DESC LIMIT 1";
				PreparedStatement stmtdata = connection.prepareStatement(searchData);
				stmtdata.setString(1, deviceId);
				stmtdata.setString(2, containerId);
				stmtdata.setString(3, productId);
				ResultSet rsdata = stmtdata.executeQuery();
				Location location = new Location(deviceId, containerId, productId);
				while (rsdata.next()) {
					long datetime = -1L;
					if (rsdata.getString("status_time") != null) {
						datetime = Utility.getUTCDate(rsdata.getString("status_time"));
					}
					location.setDateTime(datetime);
					location.setLat(rsdata.getString("latitude"));
					location.setLang(rsdata.getString("longitude"));
					location.setPairStatus(rsdata.getInt("excursion_status"));
				}
				// do not add locations to the response if lat and lang values are empty
				if(location.getLat() != null &&  location.getLang() != null) {
					map.getLocations().add(location);	
				}

				
				
				rsdata.close();
				stmtdata.close();
			}
		}
		sensor.setShipmentList(shipmentList);
		sensor.setMap(map);
		rs.close();
		stmt.close();
		return sensor;
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public SensorResponse getMapDataByDeviceNPackageId(SensorRequest request) throws SQLException, ParseException {
		Connection conn = null;
		try {
			DeviceData deviceData = request.getDeviceData();
			String deviceId = deviceData.getDeviceId();
			String packageId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			Long startDate = deviceData.getStartDate();
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			response.setPackageId(packageId);
			response.setDeviceId(deviceId);
			Utility.getDateFormat().setTimeZone(TimeZone.getTimeZone("UTC"));
			Sensor sensor = new Sensor();
			conn = DAOUtility.getInstance().getConnection();
			// If startDate is not specified, get all the data points
			com.rfxcel.sensor.beans.Map map;
			if (startDate == null) {
				map = getAllMapData(conn, deviceId, packageId, productId);
			}// Get data points after start date
			else {
				map = getMapDataFromStartDate(conn, deviceId, packageId, productId, startDate);
			}
			ArrayList<Location> locations = map.getLocations();
			// If there are datapoints, get the last timestamp from that
			if (locations != null && locations.size() > 0) {
				Long lastTimeStamp = locations.get(locations.size() - 1).getDateTime();
				map.setLastTimeStamp(lastTimeStamp);
			}// else get the status time stamp
			else if (deviceData.getStartDate() != null) {
				map.setLastTimeStamp(deviceData.getStartDate());
			}
			sensor.setMap(map);
			response.setSensor(sensor);
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param packageId
	 * @param productId 
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public com.rfxcel.sensor.beans.Map getAllMapData(Connection conn, String deviceId, String packageId, String productId) throws SQLException, ParseException {
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		String value = configCache.getSystemConfig("sensor.location.decimal.limit"); // get the precision value from config, if not present setting it to 3
		value = (value == null) ? "3" : value;
		int precision;
		try {
			precision = Integer.valueOf(value);
		} catch (NumberFormatException nfe) {
			precision = 3;
		}
		// This is stored in DB just for demo purpose
		setZoomFactorForShipment(deviceId, packageId, map);

		String searchData = "SELECT id,longitude,latitude,status_time, excursion_status FROM sensor_data where device_id = ? and container_id = ? and product_id = ? "
				+ " AND longitude is NOT NULL AND latitude is NOT NULL ORDER BY status_time";
		PreparedStatement stmtdata = conn.prepareStatement(searchData);
		stmtdata.setString(1, deviceId);
		stmtdata.setString(2, packageId);
		stmtdata.setString(3, productId);
		ResultSet rsdata = stmtdata.executeQuery();
		String prevLat = "";
		String prevLang = "";
		String currentLat;
		String currentLang;
		Location location;
		Long dateTime;
		while (rsdata.next()) {
			currentLat = rsdata.getString("latitude");
			currentLang = rsdata.getString("longitude");
			dateTime = Utility.getUTCDate(rsdata.getString("status_time"));
			int excursion = rsdata.getInt("excursion_status");
			// Check if we need to add this data point
			Boolean skip = skipDataPoints(precision, currentLat, currentLang, prevLat, prevLang);
			if (!skip) {
				location = new Location(rsdata.getLong("id"), dateTime, currentLat, currentLang,excursion);
				map.getLocations().add(location);
			}else{
				location = map.getLocations().get(map.getLocations().size() -1);
				// if we are skipping the data point get the latest information at that lat and lang value
				location.setDateTime(dateTime);
				location.setId(rsdata.getLong("id"));
				location.setLat(currentLat);
				location.setLang(currentLang);
				location.setPairStatus(excursion);
			}
			prevLang = currentLang;
			prevLat = currentLat;
		}
		rsdata.close();
		stmtdata.close();
		return map;
	}

	/**
	 * 
	 * @param precision
	 * @param currentLat
	 * @param currentLang
	 * @param prevLat
	 * @param prevLang
	 * @return
	 */
	public Boolean skipDataPoints(int precision, String currentLat,  String currentLang, String prevLat, String prevLang) {
		// If lat and lang values are same for both then dont add data point, else add the data point to response
		Boolean skip = false;
		Boolean latMatch = false;
		Boolean langMatch = false;
		precision = precision + 1; // Zero based index value
		try {
			// Check if current and previous lat values are equal
			if (currentLat != null && currentLat != "" && prevLat != null && prevLat != "") {
				if (currentLat.substring(0, currentLat.indexOf(".") + precision).equals(prevLat.substring(0, prevLat.indexOf(".")+ precision))) {
					latMatch = true;
				}
			}
			// Check if current and previous lang values are equal
			if (currentLang != null && currentLang != "" && prevLang != null && prevLang != "") {
				if (currentLang.substring(0, currentLang.indexOf(".") + precision).equals(prevLang.substring(0, prevLang.indexOf(".")+ precision))) {
					langMatch = true;
				}
			}
			// If both are matching skip the record
			if (latMatch && langMatch) {
				skip = true;
			}
		} catch (StringIndexOutOfBoundsException e) {

		} catch (Exception e) {

		}
		// logger.info("Skipping :: "+ skip);
		return skip;
	}

	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param packageId
	 * @param productId 
	 * @param startDate
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	private com.rfxcel.sensor.beans.Map getMapDataFromStartDate(Connection conn, String deviceId, String packageId, String productId, Long startDate) throws SQLException, ParseException {
		String value = configCache.getSystemConfig("sensor.location.decimal.limit"); // get the precision value from config, if not present  setting it to 3
		value = (value == null) ? "3" : value;
		int precision;
		try {
			precision = Integer.valueOf(value);
		} catch (NumberFormatException nfe) {
			precision = 3;
		}
		/*
		 * 
		 * String sDate = Long.toString(startDate/1000);
		   String query = "SELECT id,longitude,latitude,status_time, excursion_status FROM sensor_data where device_id = ? and container_id = ? and product_id = ? " 
				+ " AND longitude is NOT NULL AND latitude is NOT NULL AND UNIX_TIMESTAMP(status_time) > ? ORDER BY status_time";
		 */
		String sDate = Utility.getDateFormat().format(new Date(startDate));
		String query = "SELECT id,latitude,longitude,status_time, excursion_status FROM sensor_data where device_id = ? AND container_id = ? AND product_id = ? "
				+ " AND longitude is NOT NULL AND latitude is NOT NULL AND (DATE_FORMAT(status_time,'%Y-%m-%d %T') > ?) order by status_time";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt = conn.prepareStatement(query);
		stmt.setString(1, deviceId);
		stmt.setString(2, packageId);
		stmt.setString(3, productId);
		stmt.setString(4, sDate);
		ResultSet rs = stmt.executeQuery();
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		String prevLat = "";
		String prevLang = "";
		String currentLat;
		String currentLang;
		Location location;
		long dateTime;
		while (rs.next()) {
			currentLat = rs.getString("latitude");
			currentLang = rs.getString("longitude");
			//dateTime = Utility.getDateFormat().parse(rs.getString("status_time")).getTime();
			dateTime = Utility.getUTCDate(rs.getString("status_time"));
			// Check if we need to add this data point
			Boolean skip = skipDataPoints(precision, currentLat, currentLang, prevLat, prevLang);
			if (!skip) {
				location = new Location(rs.getLong("id"), dateTime, currentLat, currentLang, rs.getInt("excursion_status"));
				map.getLocations().add(location);
			}
			else{
				location = map.getLocations().get(map.getLocations().size() -1);
				// if we are skipping the data point get the latest information at that lat and lang value
				location.setDateTime(dateTime);
				location.setId(rs.getLong("id"));
				location.setLat(currentLat);
				location.setLang(currentLang);
				location.setPairStatus(rs.getInt("excursion_status"));
			}
			prevLang = currentLang;
			prevLat = currentLat;
		}
		rs.close();
		stmt.close();
		// This is stored in DB just for demo purpose
		setZoomFactorForShipment(deviceId, packageId, map);
		return map;
	}

	/**
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 * @throws ParseException
	 */
	public SensorResponse getShipmentDetails(SensorRequest request) throws SQLException, ParseException {
		Connection conn = null;
		try {
			String deviceId = request.getDeviceData().getDeviceId();
			String packageId = request.getDeviceData().getPackageId();
			int orgId = request.getOrgId();

			String shipmentStatus = request.getDeviceData().getShipmentStatus();
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			response.setPackageId(packageId);
			// String deviceId = null;
			String productId = null;
			String searchData;
			Utility.getDateFormat().setTimeZone(TimeZone.getTimeZone("UTC"));
			Sensor sensor = new Sensor();
			ShipmentData shipmentdata = new ShipmentData();
			ShipDetails shipTo = null;
			ShipDetails shipFrom = null;
			PreparedStatement stmtdata = null;
			conn = DAOUtility.getInstance().getConnection();
			// 1- active/current shipment, 1 - pending shipments, 2 - completed shipments
			// 1- active and pending transactions, 0 - inactive/completed shipment transactions
			// TODO , Need to convert into multiple methods latter on
			if (deviceId != null && packageId != null) {
				searchData = "SELECT id, product_id, device_id, addr_type,location,addr_1, addr_2, city, state, zip, country, create_datetime FROM trans_history"
						+ " where container_id = ? and device_id=? and is_active=? and org_id = ? ";
				stmtdata = conn.prepareStatement(searchData);
				stmtdata.setString(1, packageId);
				stmtdata.setString(2, deviceId);
				if (shipmentStatus != null && shipmentStatus.equalsIgnoreCase("3")) {
					stmtdata.setInt(3, 0);
				} else {
					stmtdata.setInt(3, 1);
				}
				stmtdata.setInt(4, orgId);
				//stmtdata.setLong(5, groupId);
			} else if (deviceId == null) {
				searchData = "SELECT id, product_id, device_id, addr_type,location,addr_1, addr_2, city, state, zip, country, create_datetime FROM trans_history"
						+ " where container_id = ? and is_active=1 and org_id = ? ";
				stmtdata = conn.prepareStatement(searchData);
				stmtdata.setString(1, packageId);
				stmtdata.setInt(2, orgId);
				//stmtdata.setLong(3, groupId);
			}

			ResultSet rsdata = stmtdata.executeQuery();
			while (rsdata.next()) {
				int addr_type = rsdata.getInt("addr_type");
				if (addr_type == 201) {
					shipFrom = new ShipDetails(rsdata.getLong("id"), rsdata.getString("location"), rsdata.getString("addr_1"), rsdata.getString("addr_2"), rsdata.getString("city"),
							rsdata.getString("state"), rsdata.getString("country"), rsdata.getString("zip"), Utility.getUTCDate(rsdata.getString("create_datetime")));
					deviceId = rsdata.getString("device_id");
					productId = rsdata.getString("product_id");
				} else if (addr_type == 202) {
					shipTo = new ShipDetails(rsdata.getLong("id"), rsdata.getString("location"), rsdata.getString("addr_1"), rsdata.getString("addr_2"), rsdata.getString("city"),
							rsdata.getString("state"), rsdata.getString("country"),	rsdata.getString("zip"), Utility.getUTCDate(rsdata.getString("create_datetime")));
				}
			}
			rsdata.close();
			stmtdata.close();
			shipmentdata.setProductId(productId);
			if (shipTo == null && shipFrom == null) {
				sensor.setShipmentData(null);
			} else {
				shipmentdata.setShipTo(shipTo);
				shipmentdata.setShipFrom(shipFrom);
			}

			// get device code and latest shipment location information
			SensorRequest sensorRequest = new SensorRequest();
			DeviceData deviceData = new DeviceData();
			deviceData.setDeviceId(deviceId);
			deviceData.setPackageId(packageId);
			deviceData.setShowAll(Boolean.TRUE);
			sensorRequest.setDeviceData(deviceData);
			// If device Id value is null, that means no record is present in trans_history corresponding to packageId
			if (deviceId != null) {
				PreparedStatement stmt;
				String query = "SELECT latitude, longitude from sensor_data where device_id = ? and container_id = ? "
						+ "AND longitude is NOT NULL AND latitude is NOT NULL ORDER BY status_time DESC LIMIT 1";
				stmt = conn.prepareStatement(query);
				stmt.setString(1, deviceId);
				stmt.setString(2, packageId);
				ResultSet rs = stmt.executeQuery();
				Double lat, lang;
				while(rs.next()){
					lat = rs.getDouble("latitude");
					lang = rs.getDouble("longitude");
					shipmentdata.setLat(lat);
					shipmentdata.setLang(lang);
				}
				shipmentdata.setDeviceCode(getDeviceTypeById(deviceId));
				Profile profile = getProfileById(deviceId, packageId);
				if(profile != null){
					shipmentdata.setDeviceProfile(profile.getProfileId().toString());
					shipmentdata.setProfileName(profile.getName());
				}
				
			}
			sensor.setShipmentData(shipmentdata);
			response.setDeviceId(deviceId);
			response.setSensor(sensor);
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	public String getDeviceTypeById(String deviceId) throws SQLException {
		Connection conn = null;
		try {
			String Query = "select sd.device_id,sdt.device_type,sd.device_type_id,sdt.device_manufacturer from sensor_devices sd,sensor_device_types sdt where sd.device_type_id=sdt.device_type_id and sd.device_id =  ?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				return rs.getString("device_type");
			}
			rs.close();
			stmt.close();
			return null;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	public Profile getProfileById(String deviceId, String packageId) throws SQLException {
		Connection conn = null;
		Profile profile = null;
		try {
//			String Query = "select sd.profile_id,sdt.id,sdt.name from sensor_associate sd,sensor_profile sdt where sd.profile_id=sdt.id and sd.parent_id = ? and sd.child_id = ? and sd.relation_status=1";
			String query = "SELECT sa.profile_id, sp.id, sp.name FROM sensor_associate sa, sensor_profile sp"
					+ " WHERE sa.profile_id = sp.id AND sa.parent_id = ? AND sa.child_id = ?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				profile = new Profile();
				profile.setProfileId(rs.getInt("id"));
				profile.setName(rs.getString("name"));
			}
			rs.close();
			stmt.close();			
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return profile;
	}

	/**
	 * 
	 * @param productDetails
	 * @param orgId
	 * @param groupId
	 * @throws SQLException
	 */
	public void addShipmentDetails(ProductDetail productDetails, int orgId, long userId, long groupId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			ShipmentLocation shipFrom = productDetails.getShipFrom();
			addTransHistory(conn, shipFrom, 201, productDetails, orgId, userId, groupId);
			ShipmentLocation shipTo = productDetails.getShipTo();
			addTransHistory(conn, shipTo, 202, productDetails, orgId, userId, groupId);
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param conn
	 * @param loc
	 * @param addrType
	 * @param orgId
	 * @param packageId
	 * @param deviceId
	 * @throws SQLException
	 */
	private void addTransHistory(Connection conn, ShipmentLocation loc, int addrType, ProductDetail productDetail, int orgId, long userId, long groupId) throws SQLException {
		try {
			String query = "Insert into trans_history (container_Id,product_id, device_id, org_id, addr_type,location,addr_1, addr_2, city, zip, state, country, create_datetime,update_datetime,user_id, group_id) values(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, productDetail.getPackageId());
			stmt.setString(2, productDetail.getProductId());
			stmt.setString(3, productDetail.getDeviceId());
			stmt.setInt(4, orgId);
			stmt.setInt(5, addrType);
			stmt.setString(6, loc.getLocationName());
			stmt.setString(7, loc.getAddress1());
			stmt.setString(8, loc.getAddress2());
			stmt.setString(9, loc.getCity());
			stmt.setString(10, loc.getPostalCode());
			stmt.setString(11, loc.getState());
			stmt.setString(12, loc.getCountry());
			stmt.setTimestamp(13, new java.sql.Timestamp(System.currentTimeMillis()));
			stmt.setTimestamp(14, new java.sql.Timestamp(System.currentTimeMillis()));
			stmt.setLong(15, userId);
			stmt.setLong(16, groupId);
			stmt.executeUpdate();
			stmt.close();
		} finally {

		}
	}
	
	/**
	 * 
	 * @param productDetails
	 * @param orgId
	 * @param userId
	 * @param groupId
	 * @throws SQLException
	 */
	public void updateShipmentDetails(ProductDetail productDetails, int orgId, long userId, long groupId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			ShipmentLocation shipFrom = productDetails.getShipFrom();
			updateTransHistory(conn, shipFrom, 201, productDetails, orgId, userId, groupId);
			ShipmentLocation shipTo = productDetails.getShipTo();
			updateTransHistory(conn, shipTo, 202, productDetails, orgId, userId, groupId);
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param conn
	 * @param loc
	 * @param addrType
	 * @param productDetail
	 * @param orgId
	 * @param userId
	 * @param groupId
	 * @throws SQLException
	 */
	private void updateTransHistory(Connection conn, ShipmentLocation loc, int addrType, ProductDetail productDetail, int orgId, long userId, long groupId) throws SQLException {
		try {
			String query = "UPDATE trans_history SET container_Id=?, product_id=?, device_id=?, org_id=?, addr_type=?, location=?, addr_1=?, addr_2=?, city=?, zip=?, state=?, country=?,"
					+ " update_datetime=?, user_id=?, group_id=? WHERE id=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, productDetail.getPackageId());
			stmt.setString(2, productDetail.getProductId());
			stmt.setString(3, productDetail.getDeviceId());
			stmt.setInt(4, orgId);
			stmt.setInt(5, addrType);
			if(loc.getLocationName() == null){
				stmt.setNull(6, Types.NULL);
			}else{
				stmt.setString(6, loc.getLocationName());
			}
			if(loc.getAddress1() == null){
				stmt.setNull(7, Types.NULL);
			}else{
				stmt.setString(7, loc.getAddress1());
			}
			if(loc.getAddress2() == null){
				stmt.setNull(8, Types.NULL);
			}else{
				stmt.setString(8, loc.getAddress2());
			}
			if(loc.getCity() == null){
				stmt.setNull(9, Types.NULL);
			}else{
				stmt.setString(9, loc.getCity());
			}
			if(loc.getPostalCode() == null){
				stmt.setNull(10, Types.NULL);
			}else{
				stmt.setString(10, loc.getPostalCode());
			}
			if(loc.getState() == null){
				stmt.setNull(11, Types.NULL);
			}else{
				stmt.setString(11, loc.getState());
			}
			if(loc.getCountry() == null){
				stmt.setNull(12, Types.NULL);
			}else{
				stmt.setString(12, loc.getCountry());
			}			
			stmt.setTimestamp(13, new java.sql.Timestamp(System.currentTimeMillis()));
			stmt.setLong(14, userId);
			stmt.setLong(15, groupId);
			if(loc.getLocationId() == null){
				stmt.setNull(16, Types.NULL);
			}else{
			stmt.setLong(16, loc.getLocationId());
			}
			stmt.executeUpdate();
			stmt.close();
			
		}finally{
			
		}
	}

	/* It has not use of rfx_ent_locations in sensor
	public SensorResponse getLocationByName(SensorRequest request) throws SQLException {
		Connection conn = null;
		try {
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			String Query = "select * from rfx_ent_locations where loc_name like ?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setString(1, "%" + request.getLocationRequest().getName()+ "%");
			ResultSet rs = stmt.executeQuery();
			LocationData locationData = new LocationData();
			while (rs.next()) {
				ShipmentLocation shipmentLocation = new ShipmentLocation();
				shipmentLocation.setLocationId(rs.getString("ext_facility_id"));
				shipmentLocation.setLocationName(rs.getString("loc_name"));
				locationData.getLocations().add(shipmentLocation);
				// return rs.getString("device_type");locationData
			}
			response.setLocationData(locationData);
			rs.close();
			stmt.close();
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	*/

	/* It has not use of rfx_ent_locations in sensor
	public SensorResponse getAddressById(SensorRequest request) throws SQLException {
		Connection conn = null;
		try {
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			String Query = "select * from rfx_ent_locations where ext_facility_id = ?";
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement(Query);
			stmt.setString(1, request.getLocationRequest().getId());
			ResultSet rs = stmt.executeQuery();
			LocationData locationData = new LocationData();
			// enterprise_id, location_id, loc_name, loc_addr_line_1,
			// loc_addr_line_2, loc_country, loc_zip, loc_phone, loc_fax,
			// loc_email, loc_type_id, loc_manager, loc_stat_id, state_name,
			// loc_city_id, store_id, ext_facility_id, IS_EXT_UNIT,
			// created_time, modified_time, location_code, loc_lat_lng, loc_tz,
			// loc_extn_code, company_prefix_len
			while (rs.next()) {
				ShipmentLocation shipmentLocation = new ShipmentLocation();
				shipmentLocation.setLocationId(rs.getString("ext_facility_id"));
				shipmentLocation.setLocationName(rs.getString("loc_name"));
				shipmentLocation.setAddress1(rs.getString("loc_addr_line_1"));
				shipmentLocation.setAddress2(rs.getString("loc_addr_line_2"));
				shipmentLocation.setCity(rs.getString("loc_city_id"));
				shipmentLocation.setState(rs.getString("loc_stat_id"));
				shipmentLocation.setCountry(rs.getString("loc_country"));
				shipmentLocation.setPostalCode(rs.getString("loc_zip"));

				locationData.getLocations().add(shipmentLocation);
				// return rs.getString("device_type");locationData
			}
			response.setLocationData(locationData);
			rs.close();
			stmt.close();
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	*/

	/*public SensorResponse getSensorConfig(SensorRequest request) throws Exception {
		Connection conn = null;
		try {
			String query;
			PreparedStatement stmt = null;
			SensorResponse response = new SensorResponse(Utility.getDateFormat().format(new Date()));
			conn = DAOUtility.getInstance().getConnection();
			if (request.getSearchType() != null && request.getSearchType().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_PARAMS)) {
				StringBuilder strBuilder = new StringBuilder();
				for (int i = 0; i < request.getConfigData().getParams().size(); i++) {
					strBuilder.append("?,");
				}
				query = "select * from sensor_config where param_name in ("+ strBuilder.deleteCharAt(strBuilder.length() - 1).toString() + ")";
				stmt = conn.prepareStatement(query);
				int index = 1;
				for (String value : request.getConfigData().getParams()) {
					stmt.setString(index++, value);
				}
			} else {
				query = "select * from sensor_config ";
				stmt = conn.prepareStatement(query);

			}
			ResultSet rs = stmt.executeQuery();
			Sensor sensor = new Sensor();
			HashMap<String, String> configMap = new HashMap<>();
			while (rs.next()) {
				configMap.put(rs.getString("param_name"), rs.getString("param_value"));
			}
			sensor.setConfigMap(configMap);
			response.setSensor(sensor);
			rs.close();
			stmt.close();
			return response;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}*/

	/**
	 * <p>
	 * check if device is in associated state
	 * </p>
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceIsAssociated(String deviceId, int orgId) throws SQLException {
		Boolean isAssociated = false;
		try {
			int state = getDeviceState(deviceId, orgId);
			if (state == SensorConstant.DEVICE_STATE_ASSOCIATED) {
				isAssociated = true;
			}
		} finally {
		}
		return isAssociated;
	}

	/**
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceIsCommissioned(String deviceId, int orgId) throws SQLException {
		Boolean isCommissioned = false;
		int state = getDeviceState(deviceId, orgId);
		if (state == SensorConstant.DEVICE_STATE_COMMISSION) {
			isCommissioned = true;
		}
		return isCommissioned;
	}

	/**
	 * 
	 * @param deviceId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceIsDeCommissioned(String deviceId, int orgId) throws SQLException {
		Boolean isDeCommissioned = false;
		int state = getDeviceState(deviceId, orgId);
		if (state == SensorConstant.DEVICE_STATE_DECOMMISSIONED) {
			isDeCommissioned = true;
		}
		return isDeCommissioned;
	}

	/**
	 * 
	 * @param deviceId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public int getDeviceState(String deviceId, int orgId) throws SQLException {
		Connection conn = null;
		int state = 0;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT state from sensor_devices WHERE device_id = ? and org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				state = rs.getInt("state");
			}
			rs.close();
			stmt.close();
			return state;
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * <p>
	 * check if device is ready to be associated
	 * </p>
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean isDeviceReadyToBeAssociated(String deviceId, int orgId) throws SQLException {
		Boolean ready = false;
		try {
			int state = getDeviceState(deviceId, orgId);
			if (state == SensorConstant.DEVICE_STATE_COMMISSION || state == SensorConstant.DEVICE_STATE_DISASSOICATED) {
				ready = true;
			}
		} finally {
		}
		return ready;
	}

	/**
	 * <p>
	 * check if package is ready to be associated
	 * </p>
	 * 
	 * @param packageId
	 * @return
	 * @throws SQLException
	 */
	public Boolean isPackageReadyToBeAssociated(String packageId,String productId, int orgId) throws SQLException {
		Boolean ready = false;
		try {
			int state = getPackageState(packageId, productId, orgId);
			if (state == SensorConstant.DEVICE_STATE_COMMISSION || state == SensorConstant.DEVICE_STATE_DISASSOICATED || state == SensorConstant.DEVICE_STATE_ASSOCIATED) {
				ready = true;
			}
		} finally {
		}
		return ready;
	}

	/**
	 * <p>
	 * check if package is in associated state
	 * </p>
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfPackageIsAssociated(String packageId, String productId, int orgId) throws SQLException {
		Boolean isAssociated = false;
		try {
			int state = getPackageState(packageId, productId, orgId);
			if (state == SensorConstant.DEVICE_STATE_ASSOCIATED) {
				isAssociated = true;
			}
		} finally {
		}
		return isAssociated;
	}

	/**
	 * <p>
	 * check if package is in commissioned state
	 * </p>
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfPackageIsCommissioned(String packageId,String productId, int orgId) throws SQLException {
		Boolean isCommissioned = false;
		try {
			int state = getPackageState(packageId, productId, orgId);
			if (state == SensorConstant.DEVICE_STATE_COMMISSION) {
				isCommissioned = true;
			}
		} finally {
		}
		return isCommissioned;
	}

	/**
	 * 
	 * @param packageId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfPackageIsDeCommissioned(String packageId, String productId, int orgId) throws SQLException {
		Boolean isDeCommissioned = false;
		int state = getPackageState(packageId,productId, orgId);
		if (state == SensorConstant.DEVICE_STATE_DECOMMISSIONED) {
			isDeCommissioned = true;
		}
		return isDeCommissioned;
	}

	/**
	 * 
	 * @param packageId
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public int getPackageState(String packageId, String productId, int orgId) throws SQLException {
		Connection conn = null;
		int state = -1; // Not present in system
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT state from sensor_package WHERE package_id = ? and org_id = ? and product_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setInt(2, orgId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				state = rs.getInt("state");
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return state;

	}

	/**
	 * <p>
	 * decommission a device
	 * </p>
	 * 
	 * @param deviceId
	 * @throws SQLException
	 */
	public void decommissionDevice(String deviceId) throws SQLException {
		updateDeviceStateDecommission(deviceId, SensorConstant.DEVICE_STATE_DECOMMISSIONED);
	}

	/**
	 * <p>
	 * Associate Device
	 * </p>
	 * @param conn 
	 * 
	 * @param deviceId
	 * @throws SQLException
	 */
	public void associateDevice(Connection conn, String deviceId) throws SQLException {
		updateDeviceState(conn, deviceId, SensorConstant.DEVICE_STATE_ASSOCIATED);
	}

	/**
	 * Disassociate Device, used in case of transaction management is to be applied
	 * @param connection
	 * @param deviceId
	 * @throws SQLException
	 */
	public void disAssociateDevice(Connection connection, String deviceId) throws SQLException {
		updateDeviceState(connection, deviceId, SensorConstant.DEVICE_STATE_DISASSOICATED);
	}
	
	/**
	 *  Disassociate Device
	 * @param deviceId
	 * @throws SQLException
	 */
	public void disAssociateDevice(String deviceId) throws SQLException {
		updateDeviceState( deviceId, SensorConstant.DEVICE_STATE_DISASSOICATED);
	}

	/**
	 * 
	 * @param deviceId
	 * @param state
	 * @throws SQLException
	 */
	private void updateDeviceState(String deviceId, int state) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET state = ? WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, state);
			stmt.setString(2, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	/**
	 * 
	 * @param orgId
	 * @param state
	 * @throws SQLException
	 */
	public void updateDeviceStateOrg(int orgId, int state) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "Update sensor_devices set active=?, device_id = CONCAT(device_id,'_DELETED_' , UNIX_TIMESTAMP()) where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt = conn.prepareStatement(query);
			stmt.setInt(1, state);
			stmt.setInt(2, orgId);
			stmt.executeUpdate();
			stmt.close();
			
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	
	
	/**
	 * <p> Connection object is passed in. 
	 * This method is to be used in case we need transaction management
	 * @param conn
	 * @param deviceId
	 * @param state
	 * @throws SQLException
	 */
	private void updateDeviceState(Connection conn, String deviceId, int state) throws SQLException {
		try {
			String query = "UPDATE sensor_devices SET state = ? WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, state);
			stmt.setString(2, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param state
	 * @throws SQLException
	 */
	private void updateDeviceStateDecommission(String deviceId, int state) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET state = ? WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, state);
			stmt.setString(2, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	public void updateDevice(Device device, Integer orgId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET device_key = ?, mdn = ?, active=? WHERE device_id = ? AND org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, device.getDeviceKey());
			stmt.setString(2, device.getMdn());
			stmt.setBoolean(3, device.getActive());
			stmt.setString(4, device.getDeviceId());
			stmt.setInt(5, orgId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * <p>
	 * decommission a package
	 * </p>
	 * 
	 * @param deviceId
	 * @throws SQLException
	 */
	public void decommissionPackage(String packageId, int orgId) throws SQLException {
		updatePackageState(packageId, orgId, SensorConstant.DEVICE_STATE_DECOMMISSIONED);
	}

	/**
	 * <p>
	 * Change state of package to associate
	 * </p>
	 * @param conn 
	 * 
	 * @param packageId
	 * @throws SQLException
	 */
	public void associatePackage(Connection conn, String packageId, String productId, int orgId) throws SQLException {
		updatePackageState(conn, packageId, productId, orgId, SensorConstant.DEVICE_STATE_ASSOCIATED);
	}

	/**
	 * <p>
	 * Change state of package to disassociate
	 * </p>
	 * @param conn 
	 * 
	 * @param packageId
	 * @param productId 
	 * @throws SQLException
	 */
	public void disAssociatePackage(Connection conn, String packageId, String productId, int orgId) throws SQLException {
		updatePackageState(conn, packageId,productId, orgId, SensorConstant.DEVICE_STATE_DISASSOICATED);
	}

	
	public void disAssociatePackage( String packageId, int orgId) throws SQLException {
		updatePackageState( packageId, orgId, SensorConstant.DEVICE_STATE_DISASSOICATED);
	}
	/**
	 * <p> This method is to be used when we need  transaction management implemented
	 * @param conn
	 * @param packageId
	 * @param orgId
	 * @param state
	 * @throws SQLException
	 */
	private void updatePackageState(Connection conn, String packageId, String productId, int orgId, int state) throws SQLException {
		try {
			String query = "UPDATE sensor_package SET state =? WHERE package_id = ? and org_id = ? and product_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, state);
			stmt.setString(2, packageId);
			stmt.setInt(3, orgId);
			stmt.setString(4, productId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
		}
	}
	
	/**
	 * 
	 * @param packageId
	 * @param state
	 * @throws SQLException
	 */
	private void updatePackageState(String packageId, int orgId, int state) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_package SET state =? WHERE package_id = ? and org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, state);
			stmt.setString(2, packageId);
			stmt.setInt(3, orgId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}


	public void activateTransHistory(String deviceId, String productId, String packageId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE trans_history SET is_active = 1 WHERE device_id = ? AND container_id = ? AND product_id=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, packageId);
			stmt.setString(3, productId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	/**
	 * 
	 * @param conn
	 * @param deviceId
	 * @param packageId
	 * @throws SQLException
	 */
	public void deleteTransHistory(Connection conn, String deviceId, String packageId) throws SQLException {
		try {
			String query = "UPDATE trans_history SET is_active = 0 WHERE device_id = ? AND container_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, packageId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
		}
	}

	/**
	 * Update sensor associate table to set device-package pair as favorite
	 * 
	 * @param deviceId
	 * @param logId
	 */
	public void addToFavourite(SensorRequest request) {
		Connection conn = null;
		try {
			DeviceData deviceData = request.getDeviceData();
			conn = DAOUtility.getInstance().getConnection();
			PreparedStatement stmt = conn.prepareStatement("update sensor_associate set add_favorite=? ,user_id =? where parent_id=? and child_id=? and product_id = ? ");
			stmt.setString(1, request.getAction() != null ? request.getAction()	: "0");
			stmt.setString(2, request.getUser());
			stmt.setString(3, deviceData.getPackageId());
			stmt.setString(4, deviceData.getDeviceId());
			stmt.setString(5, deviceData.getProductId());
			stmt.executeUpdate();
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param deviceType
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfDeviceTypeIsPresent(String deviceType) throws SQLException {
		Connection conn = null;
		Boolean isPresent = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			// String query =  "SELECT count(*) from sensor_device_types where device_type = ? and org_id = ?";
			String query = "SELECT EXISTS (SELECT 1 from sensor_device_types where device_type = ?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceType);
			// stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count != 0) {
				isPresent = true;
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}

	/**
	 * 
	 * @param deviceTypeId
	 * @return
	 * @throws SQLException
	 */
/*	public Boolean checkIfDeviceTypeIdIsPresent(String deviceTypeId) throws SQLException {
		Connection conn = null;
		Boolean isPresent = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT count(*) from sensor_device_types where device_type_id = ? ";
			// String query = "SELECT EXISTS (SELECT 1 from sensor_device_types where device_type = ? )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceTypeId);
			// stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count != 0) {
				isPresent = true;
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}*/

	/**
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Integer getDeviceTypeIdForADeviceId(String deviceId) {
		Connection conn = null;
		Integer deviceTypeId = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select device_type_id from sensor_devices where device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				deviceTypeId = rs.getInt("device_type_id");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while fetching deviceTypeId for deviceId : "+ deviceId + " " + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceTypeId;
	}

	/**
	 * 
	 * @param deviceId
	 * @param productId
	 * @return
	 */
	public void setZoomFactorForShipment(String deviceId, String packageId, com.rfxcel.sensor.beans.Map map) {
		Connection conn = null;
		Integer zoomFactor = null;
		String centerLatitude = null;
		String centerLongitude = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select zoom_factor, center_latitude, center_longitude from sensor_associate where parent_id =? and child_id =? ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, packageId);
			stmt.setString(2, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				zoomFactor = rs.getInt("zoom_factor");
				centerLatitude = rs.getString("center_latitude");
				centerLongitude = rs.getString("center_longitude");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while fetching zoom Factor for deviceId : "	+ deviceId + " " + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		map.setZoomFactor(zoomFactor);
		map.setCenterLatLong(centerLatitude, centerLongitude);
	}

	/**
	 * 
	 * @param deviceId
	 * @return
	 */
	public com.rfxcel.sensor.util.Sensor getSensorDataByLastStatusTime(String deviceId) {
		Connection conn = null;
		com.rfxcel.sensor.util.Sensor sensor = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT device_id, temperature, latitude, longitude, status_time FROM sensor_data WHERE device_id=? AND latitude IS NOT NULL AND longitude IS NOT NULL ORDER BY status_time DESC LIMIT 1";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				sensor = new com.rfxcel.sensor.util.Sensor();
				sensor.setDeviceIdentifier(rs.getString("device_id"));
				sensor.setTemperature(rs.getString("temperature"));
				sensor.setLatitude(rs.getString("latitude"));
				sensor.setLongitude(rs.getString("longitude"));
				sensor.setStatusTimeStamp(rs.getString("status_time"));
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while fetching sensor data for deviceId : "	+ deviceId + " " + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return sensor;

	}

	/**
	 * 
	 * @param deviceLogType
	 * @return
	 */
	public ConcurrentHashMap<String, ConcurrentHashMap<String, DeviceInfo>> getLattestDeviceAttributeLog(int deviceLogType) {
		Connection conn = null;
		ConcurrentHashMap<String, ConcurrentHashMap<String, DeviceInfo>> deviceAttrNotificationLimitLog = new ConcurrentHashMap<String, ConcurrentHashMap<String, DeviceInfo>>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT log_id, attribute_name, attribute_value, device_id, start_time, end_time, device_log_type, notify_count"
					+ " FROM sensor_device_log t1 WHERE "
					/*+ "t1.end_time IN (SELECT MAX(t2.end_time) FROM sensor_device_log t2 WHERE t2.device_id = t1.device_id "
					+ "and t2.device_log_type=t1.device_log_type group by attribute_name) and"*/
					+ " t1.device_log_type= ?";

			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, deviceLogType);
			ResultSet rs = stmt.executeQuery();
			String deviceId;
			String attrName;
			String attrValue;
			DateTime startTime, endTime;
			Long id;
			int notifyCount;
			while (rs.next()) {
				deviceId = rs.getString("device_id");
				attrName = rs.getString("attribute_name");
				attrValue = rs.getString("attribute_value");
				id = rs.getLong("log_id");
				startTime = formatter.parseDateTime(Utility.getDateFormat().format(Utility.getDateFormat().parse(rs.getString("start_time"))));
				endTime = formatter.parseDateTime(Utility.getDateFormat().format(Utility.getDateFormat().parse(rs.getString("end_time"))));
				notifyCount = rs.getInt("notify_count");
				DeviceInfo deviceInfo = new DeviceInfo(id,attrName, attrValue,	startTime, endTime, deviceId, notifyCount);
				if (deviceAttrNotificationLimitLog.containsKey(deviceId)) {
					deviceAttrNotificationLimitLog.get(deviceId).put(attrName, deviceInfo);
				} else {
					ConcurrentHashMap<String, DeviceInfo> attrMap = new ConcurrentHashMap<String, DeviceInfo>();
					attrMap.put(attrName, deviceInfo);
					deviceAttrNotificationLimitLog.put(deviceId, attrMap);
				}
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getLattestDeviceAttributeLog during start up "+ e.getMessage());
		} catch (ParseException e) {
			logger.error("Error while getLattestDeviceAttributeLog during start up "+ e.getMessage());
			e.printStackTrace();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceAttrNotificationLimitLog;
	}

	/**
	 * get associatedSensorData by latest status timestamp
	 * 
	 * @return
	 */
	public HashMap<String, com.rfxcel.sensor.util.Sensor> getAssociatedSensorData() {
		Connection conn = null;
		ResultSet rs = null;
		HashMap<String, com.rfxcel.sensor.util.Sensor> sensorData = new HashMap<String, com.rfxcel.sensor.util.Sensor>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT child_id AS device_id FROM sensor_associate WHERE relation_status=1";
			Statement stmt = conn.createStatement();
			rs = stmt.executeQuery(query);
			while (rs.next()) {
				String deviceId = rs.getString("device_id");
				com.rfxcel.sensor.util.Sensor sensor = getSensorDataByLastStatusTime(deviceId);
				if (sensor != null) {
					sensorData.put(deviceId, sensor);
				}
			}
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while getAssociatedSensorData during fetching "+ e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return sensorData;
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<Integer, HashMap<String, Geofence>> getProfileIdGeoPoints() {
		HashMap<Integer, HashMap<String, Geofence>> deviceGeofence = new HashMap<Integer, HashMap<String, Geofence>>();
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select id,profile_id, point_name, geofence_number, latitude, longitude, radius,location_type from sensor_geopoints";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			String pointName, geofenceNumber;
			Double latitude, longitude;
			Integer radius;
			Integer profileId;
			Long id;
			while (rs.next()) {
				id = rs.getLong("id");
				profileId = rs.getInt("profile_id");
				pointName = rs.getString("point_name");
				geofenceNumber = rs.getString("geofence_number");
				latitude = rs.getDouble("latitude");
				longitude = rs.getDouble("longitude");
				radius = rs.getObject("radius") != null ? rs.getInt("radius") : null;
				Geofence geofence = new Geofence(id, geofenceNumber, pointName, latitude, longitude, radius,rs.getInt("location_type"));
				HashMap<String, Geofence> geofenceMap = deviceGeofence.get(profileId);
				if (geofenceMap == null) {
					geofenceMap = new HashMap<String, Geofence>();
					geofenceMap.put(geofenceNumber, geofence);
					deviceGeofence.put(profileId, geofenceMap);
				} else {
					geofenceMap.put(geofenceNumber, geofence);
				}
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while loading getGeoPoints information "+ e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceGeofence;
	}

	/**
	 * 
	 * @return
	 */
	public HashMap<String, Geofence> getGeoPointsForProfile(Integer profileId) {
		HashMap<String, Geofence> geofenceMap = new HashMap<String, Geofence>();
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "select id,point_name, geofence_number, latitude, longitude,radius,location_type from sensor_geopoints where profile_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, profileId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				Geofence geofence = new Geofence(rs.getLong("id"), rs.getString("geofence_number"), rs.getString("point_name"), rs.getDouble("latitude"),
						rs.getDouble("longitude"), rs.getObject("radius") != null ? rs.getInt("radius") : null,rs.getInt("location_type"));
				geofenceMap.put(rs.getString("geofence_number"), geofence);
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while loading getGeoPoints information for deviceTypeId "+ profileId + " " + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return geofenceMap;
	}

	/**
	 * 
	 * @param conn 
	 * @param deviceId
	 */
	public void removeBacterialGrowthLog(Connection conn, String deviceId) throws SQLException{
		try {
			String sqlQury = "delete from sensor_bacterial_growth_log where device_id=? ";
			PreparedStatement stmt = conn.prepareStatement(sqlQury);
			stmt.setString(1, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
		}
	}

	

	/**
	 * <p>
	 * Deactivate Devices for an org
	 * 
	 * @param orgId
	 * @throws SQLException
	 */
	public void deactivateDevicesForOrg(Connection conn,Integer orgId) throws SQLException {
			
			ArrayList<String> deviceIds = getDevicesForOrg(orgId);
			logger.info("*** Cleaning internal device cache for org " + orgId);
			deviceCache.removeDeviceLastLocation(deviceIds);
			deviceCache.removeDeviceGeopointState(deviceIds);
			deviceCache.removeDeviceIdTypeMapping(deviceIds);
			deviceCache.removeDeviceIdBatteryValueMapping(deviceIds);
			deviceCache.removeDeviceOrgMapping(deviceIds);
			attrLogcache.removeNotificationLimitLog(deviceIds);
			attrLogcache.removeExcursionTimeLog(deviceIds);
			attrLogcache.removeSameLocationNotificationLimitLog(deviceIds);
			attrLogcache.removeDeviceBacterialGrowthLog(deviceIds);
			String packageId;
			for(String deviceId : deviceIds){
				packageId = assoCache.getContainerId(deviceId);
				assoCache.removeAssociationStatus(deviceId, packageId);
				assoCache.removeActiveShipmentProfileMapping(deviceId, packageId);
				assoCache.removeShipmentExcursion(deviceId, packageId);
				//assoCache.removeShipmentHomeGeopoint(deviceId, packageId);
				assoCache.removeAssociation(deviceId);
				sendumDAO.disAssociatePackage(packageId, orgId);
				sendumDAO.disAssociateDevice(deviceId);
				
				String deviceType = sendumDAO.getDeviceTypeById(deviceId);
				String deviceManufacturer = DeviceTypeDAO.getInstance().getDeviceTypeManufacturer(orgId, deviceType);	
				if(deviceManufacturer.equalsIgnoreCase(SensorConstant.SENSOR_VERIZON)){
					logger.info("*** Deactivating " + deviceManufacturer + " device " + deviceId + " for org " + orgId);
					Device device = sendumDAO.getDeviceById(deviceId, orgId);
					String key = device.getDeviceKey();
					if ((key != null) && (!key.equalsIgnoreCase(SensorConstant.DEVICE_KEY_DEMO))) {
						VerizonDeviceDeactivator.getInstance().deActivateDevice(device);
					}
				}
				else
				{
					logger.info("*** Deleting " + deviceManufacturer + " device " + deviceId + " for org " + orgId);
				}
				sendumDAO.deleteDevice(deviceId, orgId);
			}
			sendumDAO.updateDeviceStateOrg(orgId,0);
	}

	/**
	 * <p>
	 * Get deviceIds for an organization
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> getDevicesForOrg(Integer orgId) throws SQLException {
		Connection conn = null;
		ArrayList<String> deviceIds = new ArrayList<String>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT device_id from sensor_devices where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				deviceIds.add(rs.getString("device_id"));
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceIds;
	}

	/**
	 * <p>
	 * Get deviceIds for an organization
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public List<Device> getDevices(SensorRequest request) throws SQLException {
		Connection conn = null;
		List<Device> devices = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT sd.device_id,sdt.device_type,sd.device_type_id,sd.active,sd.org_id,sd.state,"
					+ "sd.activation_status, sd.group_id,sd.battery_value,sd.demo_flag,sd.device_key, sd.mdn from sensor_devices sd "
					+ "LEFT JOIN sensor_device_types sdt ON sd.device_type_id = sdt.device_type_id where sd.org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setInt(1, request.getDevice().getOrgId());
			ResultSet rs = stmt.executeQuery();
			devices = new ArrayList<Device>();
			Device device = null;
			while (rs.next()) {
				device = new Device(rs.getString("device_id"), rs.getInt("device_type_id"), rs.getBoolean("active"), rs.getInt("state"), rs.getInt("org_id"),
						rs.getLong("group_id"), rs.getLong("battery_value"), rs.getInt("demo_flag"), rs.getString("device_type"), rs.getString("device_key"), rs.getString("mdn"));
				device.setActivationStatus(rs.getInt("activation_status"));
				devices.add(device);
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return devices;
	}
	
	/**
	 * <p>
	 * Get device for an organization,Group and DeviceId
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Device getDeviceById(String deviceId,Integer orgId) throws SQLException {
		Connection conn = null;
		String query = null;
		PreparedStatement stmt = null;
		Device device = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			if (deviceId != null && orgId != null ) {
				query = "SELECT sd.device_id,sd.device_type_id,sd.active,sd.org_id,sd.state,sd.group_id,sd.battery_value,sd.demo_flag,sd.device_key, sd.mdn from sensor_devices sd"
						+ " where sd.device_id = ? AND sd.org_id = ?";
						//+ " AND sd.group_id = ? "
						//+ " AND sd.active = 1";
				stmt = conn.prepareStatement(query);
				stmt.setString(1, deviceId);
				stmt.setInt(2, orgId);
				//stmt.setLong(3, groupId);
			} 
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				String deviceType = null;
				device = new Device(rs.getString("device_id"), rs.getInt("device_type_id"), rs.getBoolean("active"), 
						rs.getInt("state"), rs.getInt("org_id"), rs.getLong("group_id"), rs.getLong("battery_value"), rs.getInt("demo_flag"), deviceType,
						rs.getString("device_key"), rs.getString("mdn"));
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return device;
	}

	/**
	 * <p>
	 * Get device for an organization,Group and DeviceKey
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Device getDeviceByKey(String deviceKey) throws SQLException 
	{
		Connection conn = null;
		String query = null;
		PreparedStatement stmt = null;
		Device device = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			if (deviceKey != null ) {
				query = "SELECT sd.device_id,sd.device_type_id,sd.active,sd.org_id,sd.state,sd.group_id,sd.battery_value,sd.demo_flag,sd.device_key, sd.mdn from sensor_devices sd"
						+ " where sd.device_key = ?";
				stmt = conn.prepareStatement(query);
				stmt.setString(1, deviceKey);
			} 
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				String deviceType = null;
				device = new Device(rs.getString("device_id"), rs.getInt("device_type_id"), rs.getBoolean("active"), 
						rs.getInt("state"), rs.getInt("org_id"), rs.getLong("group_id"), rs.getLong("battery_value"), rs.getInt("demo_flag"), deviceType,
						rs.getString("device_key"), rs.getString("mdn"));
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return device;
	}
	
	
	/**
	 * 
	 * @param searchString
	 * @param orgId
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<DeviceDetail> searchDevices(String searchType, String searchString, Integer orgId) throws SQLException {
		Connection conn = null;
		ArrayList<DeviceDetail> result = new ArrayList<DeviceDetail>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String searchQuery;
			if (searchType.equals("1")) { // seachType=1 for commissioned devices
				searchQuery = "SELECT sd.device_id, sd.active, sd.device_type_id, sdt.device_type, sd.activation_status FROM sensor_devices sd inner join sensor_device_types sdt "
						+ "on sd.device_type_id = sdt.device_type_id WHERE device_id LIKE ? and state in(1,2,3) and sd.org_id = ?";
			} else {
				searchQuery = "SELECT sd.device_id, sd.active, sd.device_type_id, sdt.device_type, sd.activation_status FROM sensor_devices sd inner join sensor_device_types sdt "
						+ "on sd.device_type_id = sdt.device_type_id WHERE device_id LIKE ? and sd.org_id = ?";
			}

			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setString(1, "%" + searchString + "%");
			stmt.setInt(2, orgId);
			//stmt.setLong(3, groupId);
			ResultSet rs = stmt.executeQuery();
			DeviceDetail deviceDetail;
			while (rs.next()) {
				deviceDetail = new DeviceDetail();
				deviceDetail.setDeviceId(rs.getString("device_id"));
				deviceDetail.setDeviceTypeId(rs.getInt("device_type_id"));
				deviceDetail.setDeviceType(rs.getString("device_type"));
				deviceDetail.setActive(rs.getBoolean("active"));
				deviceDetail.setActivationState(rs.getInt("activation_status"));
				result.add(deviceDetail);
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return result;
	}

	
	/**
	 * 
	 * @param deviceId
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteDevice(String deviceId, Integer orgId) throws SQLException{
		DAOUtility.runInTransaction(conn->{
			// hard delete deviceId from sensor_device 
			String query = "delete from sensor_devices where device_id = ? and org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setInt(2, orgId);
			stmt.executeUpdate();
			stmt.close();
			// delete association mapping for completed devices
			deleteCompletedAssociations(conn,deviceId, orgId);
			return null;
		});
	}
	
	
	/**
	 * @param conn
	 * @param deviceId
	 * @param orgId
	 */
	public void deleteCompletedAssociations(Connection conn,String deviceId, Integer orgId) throws SQLException{
		String query = "delete from sensor_associate where child_id = ? and org_id = ? and relation_status = 0";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setString(1, deviceId);
		stmt.setInt(2, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
	
	/**
	 * <p>It is use to call resetK4Data stored procedure.</p>
	 * @throws Exception
	 */
	public void resetK4Data() throws Exception {
		Connection conn = null;
		CallableStatement stmt;
		try {
			logger.info("*** CALL resetK4Data() ***");
			conn = DAOUtility.getInstance().getConnection();
			stmt = conn.prepareCall("{call resetK4Data()}");
			stmt.executeQuery();
			stmt.close();			

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}

	}	

	/**
	 * <p>It is use to call resetK1K3Data stored procedure.</p>
	 * @throws Exception
	 */
	public void resetK1K3Data() throws Exception {
		Connection conn = null;
		CallableStatement stmt;
		try {
			logger.info("*** CALL resetK1K3Data() ***");
			conn = DAOUtility.getInstance().getConnection();
			stmt = conn.prepareCall("{call resetK1K3Data()}");
			stmt.executeQuery();
			stmt.close();			

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}

	}
	
	/**
	 * <p>de-activate the device</p>
	 * @param conn
	 * @param deviceId
	 * @param orgId
	 * @throws SQLException
	 */
	public void deactivateDevice(String deviceId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET active = 0 WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	/**
	 * <p>de-activate the device</p>
	 * @param deviceId
	 * @param orgId
	 * @throws SQLException
	 *//*
	public void deactivateDevice(String deviceId, int orgId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET active = 0 WHERE device_id = ? AND org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setInt(2, orgId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	*/
		
	/**
	 * @param deviceId
	 * @throws SQLException
	 */
	public void activateDevice(String deviceId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET active = 1 WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * @param deviceId
	 * @throws SQLException
	 */
	public void changeActivationStateOfDevice(String deviceId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET active = 1, activation_status = 1 WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param deviceKey
	 * @throws SQLException
	 */
	public void updateDeviceKey(String deviceId, String deviceKey) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE sensor_devices SET device_key = ? WHERE device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceKey);
			stmt.setString(2, deviceId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param orgId
	 * @param deviceManufacturer
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<String> getKirsenDeviceForOrg(Integer orgId, String deviceManufacturer) throws SQLException {
		Connection conn = null;
		ArrayList<String> deviceIds = new ArrayList<String>();
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT device_id from sensor_devices where org_id = ? and device_type_id IN (select device_type_id from sensor_device_types where org_id = ? and device_manufacturer=? )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, orgId);
			stmt.setLong(2, orgId);
			stmt.setString(3, deviceManufacturer);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				deviceIds.add(rs.getString("device_id"));
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return deviceIds;
	}
	
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deletePackagesForOrg(Connection conn, Integer orgId) throws SQLException {
		String query = "Update sensor_package set package_id = CONCAT(package_id,'_DELETED_' , UNIX_TIMESTAMP()) where org_id = ?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
	
	/**
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Integer getActivationStatus(String deviceId) throws SQLException {
		Connection conn = null;
		Integer activationStatus = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT activation_status from sensor_devices where device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				activationStatus = rs.getInt("activation_status");	
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return activationStatus;
	}
	
	/**
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public Integer getActiveStatus(String deviceId) throws SQLException {
		Connection conn = null;
		Integer active = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT active from sensor_devices where device_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			ResultSet rs = stmt.executeQuery();
			while (rs.next()) {
				active = rs.getInt("active");	
			}
			rs.close();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return active;
	}
	
	/**
	 * <p>It is used to fetch sensor data for EMS requirement</p>
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @return
	 */
	public ArrayList<com.rfxcel.sensor.util.Sensor> getSensorData(String deviceId, String packageId, String productId) {
		//TODO - UI is sending rtsItemId & rtsSerialNumber instead of packageId and productId (fix UI)
		
		String packId = null;
		String prodId = null;
		Connection conn = null;
		ArrayList<com.rfxcel.sensor.util.Sensor> sensorDataList = new ArrayList<com.rfxcel.sensor.util.Sensor>();		
		try {
			conn = DAOUtility.getInstance().getConnection();
			
			String fquery = "SELECT parent_id, product_id FROM sensor_associate"
					+ " WHERE child_id=? AND rts_serial_number=? and rts_item_id=?";
			
			PreparedStatement fstmt = conn.prepareStatement(fquery);
			fstmt.setString(1, deviceId);
			fstmt.setString(2, packageId);
			fstmt.setString(3, productId);
			
			ResultSet frs = fstmt.executeQuery();
			
			while (frs.next()) {
				packId = frs.getString("parent_id");
				prodId = frs.getString("product_id");
			}
					
			frs.close();
			fstmt.close();
			
			logger.info("fetching sensor data for deviceId : "	+ deviceId + ", " + packageId + ", " + productId + ", " + packId + ", " + prodId);
			
			String query = "SELECT device_id, temperature, pressure, humidity, light, tilt, battery_rem AS battery, growth_log, cumulative_log, latitude, longitude,"
					+ " status_time, voltage, excursion_status, excursion_attributes, vibration, shock FROM sensor_data"
					+ " WHERE device_id=? AND container_id=? AND product_id=? order by status_time desc LIMIT 10";
    		PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, packId);
			stmt.setString(3, prodId);
			ResultSet rs = stmt.executeQuery();
			com.rfxcel.sensor.util.Sensor sensor = null;
			while (rs.next()) {
				sensor = new com.rfxcel.sensor.util.Sensor();
				sensor.setDeviceIdentifier(rs.getString("device_id"));
				sensor.setTemperature(rs.getString("temperature"));
				sensor.setPressure(rs.getString("pressure"));
				sensor.setHumidity(rs.getString("humidity"));
				sensor.setLight(rs.getString("light"));
				sensor.setTilt(rs.getString("tilt"));
				sensor.setBattery(rs.getString("battery"));
				sensor.setGrowthLog(rs.getString("growth_log"));
				sensor.setCumulativeLog(rs.getString("cumulative_log"));
				sensor.setLatitude(rs.getString("latitude"));
				sensor.setLongitude(rs.getString("longitude"));
				sensor.setStatusTimeStamp(rs.getString("status_time"));
				sensor.setVoltage(rs.getString("voltage"));
				sensor.setExcursionStatus(rs.getString("excursion_status"));
				sensor.setExcursionAttributes(rs.getString("excursion_attributes"));
				sensor.setVibration(rs.getString("vibration"));
				sensor.setShock(rs.getString("shock"));
				sensorDataList.add(sensor);
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while fetching sensor data for deviceId : "	+ deviceId + ", packageId : "+packageId+", productId : "+productId+" and error details is" + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return sensorDataList;

	}
}