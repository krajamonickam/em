package com.rfxcel.sensor.transmitter;

import java.util.Map;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.util.Config;

public class RESTTransmitter extends Transmitter {
	final static Logger logger = Logger.getLogger(RESTTransmitter.class);
	private RestRequester restRequester = null;

	public RESTTransmitter(String url) {
		super(url);
		logger.info("NEW RESTTransmitter(" + url + ")...");
		Properties prop = Config.getInstance().getProp();
		restRequester = new RestRequester(prop.getProperty("user"), prop.getProperty("password"));
	}

	@Override
	public void doTransmit(String url, String payload) {
		Map<String, String> map = Utility.convertJsonToMap(payload);
		if (map == null) return;
		try
		{
			String deviceID = map.get("deviceIdentifier");
			logger.debug("Sending request for " + deviceID + " to " + url);
			Timer timer = new Timer();
			restRequester.doPost(url, payload);
			logger.info("Sent REST request in " + timer.getSeconds() + "s for " + deviceID + " to " + url);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	 public String doGet(String url) { 
		 return restRequester.doGet(url);
	 }

}
