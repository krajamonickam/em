package com.rfxcel.sensor.transmitter;

import java.util.concurrent.LinkedBlockingQueue;

import com.rfxcel.sensor.beans.Entity;


public abstract class Transmitter extends Entity implements Runnable {
	private Thread thread = null;
	private LinkedBlockingQueue<String> queue = new LinkedBlockingQueue<String>();

	public Transmitter(String id) {
		super(id);
		thread = new Thread(this);
		thread.start();
	}

	public String getUrl() {
		return id;
	}

	public LinkedBlockingQueue<String> getQueue() {
		return queue;
	}

	public void run() {
		if (queue == null)
			return;
		String payload = null;
		try {
			while ((payload = queue.take()) != null) {
				this.doTransmit(this.getUrl(), payload);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public abstract void doTransmit(String url, String payload);

	public String toString() {
		StringBuffer sb = new StringBuffer();
		sb.append("Queue: " + this.queue.size() + " URL: " + this.getUrl() + " ");
		return (sb.toString());
	}

}
