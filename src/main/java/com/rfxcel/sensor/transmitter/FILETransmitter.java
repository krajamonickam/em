package com.rfxcel.sensor.transmitter;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;

public class FILETransmitter extends Transmitter {
	final static Logger logger = Logger.getLogger(FILETransmitter.class);
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private String directory = null;

	public FILETransmitter(String url) {
		super(url);
		logger.info("NEW FileTransmitter(" + url + ")...");
		directory = url;
	}

	@Override
	public void doTransmit(String url, String payload) {
		Map<String, String> map = Utility.convertJsonToMap(payload);
		if (map == null) return;
		try
		{
			String deviceID = map.get("deviceIdentifier");
			if (deviceID == null) {
				deviceID = "99000512000707";
			}
			logger.debug("Writing request for " + deviceID + " to " + url);
			Timer timer = new Timer();
			String fileName = createFile(deviceID);
			PrintWriter pw = new PrintWriter(new FileWriter(fileName, true));
			pw.println(timeFormat.format(new Date()) + " " + payload.replace('\n', ' ').replace('\r', ' '));
			pw.flush();
			pw.close();
			logger.debug("Wrote request in " + timer.getSeconds() + "s for " + deviceID + " to " + fileName);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
		
	}
	
	public String createFile(String deviceid)
	{
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdir()) {
				logger.info("Directory " + directory + " does not exist!");
				return(null);
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return(null);
		}
		File subDir = new File(directory, deviceid);
		if (!subDir.exists()) {
			if (!subDir.mkdir()) {
				logger.info("Directory " + subDir.getAbsolutePath() + " does not exist!");
				return(null);
			}
		}
		if (!subDir.isDirectory()) {
			logger.info("Directory " + subDir.getAbsolutePath() + " not a directory!");
			return(null);
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(subDir, deviceid + "-" + suffix + ".log");
		return(file.getAbsolutePath());
	}

}
