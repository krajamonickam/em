package com.rfxcel.sensor.transmitter;

import java.util.Map;

import org.apache.log4j.Logger;

import com.rfxcel.notification.service.NotificationService;
import com.rfxcel.rule.service.RuleEvaluationService;
import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.dao.SendumDAO;

public class SOAPTransmitter extends Transmitter {
	final static Logger logger = Logger.getLogger(SOAPTransmitter.class);

	public SOAPTransmitter(String url) {
		super(url);
		logger.info("NEW SOAPTransmitter(" + url + ")...");
	}

	@Override
	public void doTransmit(String url, String payload) {
		Map<String, String> map = Utility.stringToMap(payload);
		if (map == null) return;
		try
		{
			String deviceID = map.get("deviceIdentifier");
			Timer timer = new Timer();
			/*Utility.callProcessMessage(url, map);*/
			long recordId ;
			if (map.containsKey("alarmTimeStamp")) {
				recordId = SendumDAO.getInstance().addSensorAlert(map);
				map.put("sensorRecordId", String.valueOf(recordId));
				NotificationService.processAttributeAlerts(map);
			}else {
				recordId = SendumDAO.getInstance().addSensorData(map);
				map.put("sensorRecordId", String.valueOf(recordId));
				RuleEvaluationService.evaluateRules(map);
				NotificationService.processSameLocationAlert(map);
			}
			logger.info("Sent SOAP request in " + timer.getSeconds() + "s for " + deviceID + " to " + url);
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
}