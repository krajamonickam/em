package com.rfxcel.sensor.sendum.receiver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.rfxcel.sensor.service.SensorProcessJSON;
import com.rfxcel.sensor.util.RfxcelException;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.cache.ConfigCache;

@WebServlet("/sendum")
public class Sendum extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(Sendum.class);
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

	private static ConfigCache configCache = ConfigCache.getInstance();


	public Sendum() {
		super();
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws ServletException, IOException {
		resp.setContentType("text/html");
		OutputStreamWriter ow = new OutputStreamWriter(resp.getOutputStream());
		PrintWriter pw = new PrintWriter(ow);
		pw.println("<html><head><meta http-equiv=\"refresh\" content=\"10\"></head><body>");
		pw.println("<br/>Sendum Receiver is up and Running!<br/>");
		pw.println("******************************<br/>");
		pw.println("</body></html>");
		pw.flush();
		pw.close();
	}

	@Override
	/**
	 * this is the servlet that accepts the https post request from the calling service.
	 * performs the basic authentication
	 * accepts the json and calls the callProcessMessage method for further processing.
	 */
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Sendum Sensor message received");
		
		response.setContentType("text/html");
		final String authorization = request.getHeader("Authorization");
		String requestType = request.getMethod();
		if (authorization != null && authorization.startsWith("Basic") && "POST".equals(requestType)) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			String credentials[] = Utility.decrypytUserNamePwd(base64Credentials);
			String user = configCache.getSystemConfig("sendum.user") ;
			String password = configCache.getSystemConfig("sendum.password");
			if (user.equals(credentials[0].trim()) && password.equals(credentials[1].trim())) {
				InputStream inputStream = request.getInputStream();
				StringBuilder stringBuilder = new StringBuilder();
				BufferedReader bufferedReader = null;
				String payload = null;
				try {
					if (inputStream != null) {
						bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
						char[] charBuffer = new char[128];
						int bytesRead = -1;
						while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
							stringBuilder.append(charBuffer, 0, bytesRead);
						}
					} else {
						stringBuilder.append("");
					}
				} catch (IOException ex) {
					logger.error(ex.getMessage());
				} finally {
					if (bufferedReader != null) {
						try {
							bufferedReader.close();
						} catch (IOException ex) {
							logger.error(ex.getMessage());
						}
					}
				}
				payload = stringBuilder.toString();
				if (!(payload.equals(""))) {
					Map<String, String> map = Utility.convertJsonToMap(payload);
					map.put("deviceType", SensorConstant.SENSOR_SENDUM);
					ObjectMapper mapperObj = new ObjectMapper();
					payload = mapperObj.writeValueAsString(map);   
					SensorProcessJSON.getInstance().doQueue(payload);
					
					//writing sendum message in to log file						
					try{
						if(map != null){
							String deviceId = map.get("deviceIdentifier");
							Timer timer = new Timer();
							String fileName = createFile(deviceId);
							PrintWriter pw = new PrintWriter(new FileWriter(fileName, true));
							pw.println(timeFormat.format(new Date()) + " " + payload.replace('\n', ' ').replace('\r', ' ').replace('\t', ' '));
							pw.flush();
							pw.close();
							logger.debug("Wrote request in " + timer.getSeconds() + "s for " + deviceId + " to " + fileName);								
						}							
					}catch (Exception e){							
						logger.error("Error logging data to log file "+ e.getMessage());
						logger.error("Message is : "+ payload);
					}
					
					
				} else {
					logger.error("HTTP Payload is empty !!");
				}
			} else {
				logger.error("Authentication Failed !!");
				response.setContentType("application/json");
				response.setStatus(401);
				String responseMsg =bindErrorMessage(401,"Invalid user credentials");
				response.getWriter().write(responseMsg);

			}
		}else{
			logger.error("Authentication Failed !!");
			response.setContentType("application/json");
			response.setStatus(401);
			String responseMsg =bindErrorMessage(401,"Unauthorized user");
			response.getWriter().write(responseMsg);
		}
	}
	
	public String bindErrorMessage(int errorCode, String message) {
		ObjectMapper mapper = new ObjectMapper();
		RfxcelException rfxcelException = new RfxcelException();
		rfxcelException.setStatusCode(errorCode);
		rfxcelException.setMessage(message);
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(rfxcelException);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return jsonString;
	}
	
	
	public static String createFile(String deviceid)
	{
		String directory = ConfigCache.getInstance().getSystemConfig(SensorConstant.LOG_FILE_LOC);
		if(directory == null){
			return null;
		}
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdir()) {
				logger.info("Directory " + directory + " does not exist!");
				return null;
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return null;
		}
		File subDir = new File(directory, deviceid);
		if (!subDir.exists()) {
			if (!subDir.mkdir()) {
				logger.info("Directory " + subDir.getAbsolutePath() + " does not exist!");
				return null;
			}
		}
		if (!subDir.isDirectory()) {
			logger.info("Directory " + subDir.getAbsolutePath() + " not a directory!");
			return null;
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(subDir, deviceid + "-" + suffix + ".log");
		return(file.getAbsolutePath());
	}

}
