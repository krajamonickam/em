package com.rfxcel.sensor.util;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.log4j.Logger;

public class Config {
	final static Logger logger = Logger.getLogger(Config.class);
	public Properties prop = new Properties();
	public static Config instance = null;
	public static int SENSOR_LOCATION_TIME_LIMIT = 0;
	public static int SENSOR_LOCATION_ALERT_LIMIT = 0;
	public static int SENSOR_LIMITE_ATTRIBUTE = 0;
	public static int SENSOR_LOCATION_DECIMAL_LIMIT = 4;
	public static int SENSOR_LOCATION_DECIMAL_FACTOR = 1;
	
	public static int SYSTEM_TIME_FLAG = 0;
	public static int SYSTEM_TIME_FLAG_DEFALUT = 0;
	public static int SYSTEM_TIME_FLAG_CUSTOM = 1;
	
	public static int THING_POLL_TIME = 5;  // default 5 seconds
	public static long FILTER_TIME  = -1;       				// time interval for filtering
	public static int FILTER_TILT = Integer.MAX_VALUE;          // don't filter if change in tilt 
	public static int FILTER_LIGHT = Integer.MAX_VALUE;         // don't filter if change in light 
	public static int FILTER_TEMPERATURE = Integer.MAX_VALUE;   // don't filter if change in temp 
	
	public static Config getInstance()
	{
		if (instance == null) {
			instance = new Config();
		}
		return(instance);
	}
	
	public Config()
	{
		this.loadConfigProp();
		//MessagingService.checkWSDLLocation();
	}
	

	public Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		this.prop = prop;
	}

	
	public void loadConfigProp()
	{
		String resourceName = "config.properties"; 
		ClassLoader loader = Thread.currentThread().getContextClassLoader();
		try {
			InputStream resourceStream = loader.getResourceAsStream(resourceName);
		    prop.load(resourceStream);
		    logger.debug("loaded config properties from file: " + resourceName);  
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			logger.error("Error while Reading the data from objectevent.properties  ! ", e);
		}
		
	}
}
