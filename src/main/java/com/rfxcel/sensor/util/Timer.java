package com.rfxcel.sensor.util;

import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;
import org.joda.time.Period;
import org.joda.time.Seconds;
import org.joda.time.format.PeriodFormatter;
import org.joda.time.format.PeriodFormatterBuilder;

public class Timer {
	private long startTime = 0L;
	private long stopTime = 0L;

	public Timer() {
		startTime = new Date().getTime();
	}

	public long getTime() {
		stopTime = new Date().getTime();
		return (stopTime - startTime);
	}

	public String getSeconds() {
		double time = (double) getTime() / 1000d;
		time = Math.round(time * 100d) / 100d;
		return (Double.toString(time));
	}

	public int getDifferenceInMinute(Date startDate) {		
		int minute = 0;
		long milliSeconds = new Date().getTime() - startDate.getTime();
		long seconds = milliSeconds / 1000;
		minute = (int) seconds / 60;		
		return minute;
	}

	public int getDifferenceInMinuteOfTwoDates(Date startDate, Date endDate) {		
		int minute = 0;
		long milliSeconds = endDate.getTime() - startDate.getTime();
		long seconds = milliSeconds / 1000;
		minute = (int) seconds / 60;		
		return minute;
	}

	public static int printDifferenceTwoDates(DateTime startDate, DateTime endDate) {

		Seconds seconds =  Seconds.secondsBetween(startDate,endDate);
		return seconds.getSeconds();
	}
	public void printDifference(Date startDate) {

		long different = new Date().getTime() - startDate.getTime();
		long secondsInMilli = 1000;
		long minutesInMilli = secondsInMilli * 60;
		long hoursInMilli = minutesInMilli * 60;
		long daysInMilli = hoursInMilli * 24;

		long elapsedDays = different / daysInMilli;
		different = different % daysInMilli;

		long elapsedHours = different / hoursInMilli;
		different = different % hoursInMilli;

		long elapsedMinutes = different / minutesInMilli;
		different = different % minutesInMilli;

		long elapsedSeconds = different / secondsInMilli;

		System.out.printf("%d days, %d hours, %d minutes, %d seconds%n",
				elapsedDays, elapsedHours, elapsedMinutes, elapsedSeconds);
	}

	public static ExcursionTime getDifferenceTime(DateTime dateTime1,DateTime dateTime2) {
		ExcursionTime excursionTime = new ExcursionTime();

		Period period = new Period(dateTime1, dateTime2);

		PeriodFormatter formatter = new PeriodFormatterBuilder()
		.appendYears() .appendSuffix(" year(s)")
		.appendMonths().appendSuffix(" month(s)")
		.appendWeeks().appendSuffix(" week(s)")
		.appendDays().appendSuffix(" day(s)")
		.appendHours().appendSuffix(" hour(s)")
		.appendMinutes().appendSuffix(" minute(s)")
		.appendSeconds().appendSuffix(" second(s)")
		.printZeroNever().toFormatter();


		excursionTime.setMinutes(period.getMinutes());
		excursionTime.setElapsedTime(formatter.print(period));


		return excursionTime;
	}

	/*
	 * public long getDifferenceTime(long milliSec1, long milliSec2) {
	 * 
	 * long timeDifInMilliSec; if (milliSec1 >= milliSec2) { timeDifInMilliSec =
	 * milliSec1 - milliSec2; } else { timeDifInMilliSec = milliSec2 -
	 * milliSec1; }
	 * 
	 * long timeDifSeconds = timeDifInMilliSec / 1000; long timeDifMinutes =
	 * timeDifInMilliSec / (60 * 1000); long timeDifHours = timeDifInMilliSec /
	 * (60 * 60 * 1000); long timeDifDays = timeDifInMilliSec / (24 * 60 * 60 *
	 * 1000);
	 * 
	 * return timeDifSeconds; }
	 */

	public static void main(String[] args) throws ParseException,
	InterruptedException {
		double lat = 89.25639;
		System.out.println(new Date().getTime());
		System.out.println(Math.round(lat * 100));

		Timer timer = new Timer();
		String string = "Jan 07, 2014 9:15:12";
		DateFormat formatter = new SimpleDateFormat("MMM dd, yyyy hh:mm:ss");
		Date date = formatter.parse(string);

		int seconds = timer.getDifferenceInMinute(date);

		int milliseconds = seconds * 1000;

		String a = String.format(
				"%d days,%d hours,%d min, %d sec",
				TimeUnit.MILLISECONDS.toDays(milliseconds),
				TimeUnit.MILLISECONDS.toHours(milliseconds),
				TimeUnit.MILLISECONDS.toMinutes(milliseconds),
				TimeUnit.MILLISECONDS.toSeconds(milliseconds)
				- TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS
						.toMinutes(milliseconds)));
		System.out.println(a);

		DateTime time1 = new DateTime();
		// Thread.sleep(70000);

		DateTime time2 = new DateTime();

		System.out.println(time1.getChronology());
		System.out.println(jodaToSQLTimestamp(time2.toLocalDateTime()));

		getDifferenceTime(time1, time2);

		ConcurrentHashMap<String, String> deviceInfo = new ConcurrentHashMap<String, String>();
		deviceInfo.put("12", "tet");
		deviceInfo.put("12", "tet2");
		deviceInfo.put("13", "tet");
		System.out.println(deviceInfo);

	}

	public static Timestamp jodaToSQLTimestamp(LocalDateTime localDateTime) {
		return new Timestamp(localDateTime.toDateTime().getMillis());
	}

}
