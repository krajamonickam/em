/**
 * @ COMPANY              : RFXCEL
 * @ PROJECT              : Sendum - Verizon
 * @ AUTHOR               : JEYASEELAN   
 * @ DESCRIPTION          : 
 *
 * @ MODIFICATION HISTORY:
 **********************************************************************************
 * DATE			*		NAME                *       			CHANGES           *
 **********************************************************************************
 * June 14, 2016*		JEYASEELAN A		* CREATED                             *
 * 				*							*                                     *
 *				*							*									  *
 *				*							*									  *
 **********************************************************************************
 */
package com.rfxcel.sensor.util;

import java.util.ArrayList;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author JEYASEELAN
 * 
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Attribute {
	
	private String name;
	private Integer scope;
	private double factor;
	private String legend;
	private double minValue;
	private double maxValue;
	private int excursionDuration;
	private float multiplyFactor;
	private long deviceTypeId;
	private int notifyLimit;
	private Integer roundOffTo;
	private Boolean showGraph;
	private Double rangeMin;
	private Double rangeMax;
	private String unit;
	private Integer alert;
	private String alertDetailMsg;
	private ArrayList<Long> alertGroupIds;
	private ArrayList<String> emailIds;
	private Long alertCode;
	
	private Integer alertOnReset;
	
	/**
	 * @return the excursionDuration
	 */
	public int getExcursionDuration() {
		return excursionDuration;
	}

	/**
	 * @param excursionDuration
	 *            the excursionDuration to set
	 */
	public void setExcursionDuration(int excursionDuration) {
		this.excursionDuration = excursionDuration;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the scope
	 */
	public Integer getScope() {
		return scope;
	}

	/**
	 * @param scope
	 *            the scope to set
	 */
	public void setScope(Integer scope) {
		this.scope = scope;
	}

	/**
	 * @return the factor
	 */
	public double getFactor() {
		return factor;
	}

	/**
	 * @param factor
	 *            the factor to set
	 */
	public void setFactor(double factor) {
		this.factor = factor;
	}

	/**
	 * @return the legend
	 */
	public String getLegend() {
		return legend;
	}

	/**
	 * @param legend
	 *            the legend to set
	 */
	public void setLegend(String legend) {
		this.legend = legend;
	}

	/**
	 * @return the minValue
	 */
	public double getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue
	 *            the minValue to set
	 */
	public void setMinValue(double minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	public double getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue
	 *            the maxValue to set
	 */
	public void setMaxValue(double maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * 
	 * @return multiplyFactor
	 */
	public float getMultiplyFactor() {
		return multiplyFactor;
	}

	/**
	 * 
	 * @param multiplyFactor
	 *            the multiply value to be used to multiply
	 */
	public void setMultiplyFactor(float multiplyFactor) {
		this.multiplyFactor = multiplyFactor;
	}


	/**
	 * @return the deviceTypeId
	 */
	public long getDeviceTypeId() {
		return deviceTypeId;
	}

	/**
	 * @param deviceTypeId the deviceTypeId to set
	 */
	public void setDeviceTypeId(long deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}

	public int getNotifyLimit() {
		return notifyLimit;
	}

	public void setNotifyLimit(int notifyLimit) {
		this.notifyLimit = notifyLimit;
	}

	/**
	 * @return the roundOffTo
	 */
	public Integer getRoundOffTo() {
		return roundOffTo;
	}

	/**
	 * @param roundOffTo the roundOffTo to set
	 */
	public void setRoundOffTo(Integer roundOffTo) {
		this.roundOffTo = roundOffTo;
	}

	/**
	 * 
	 * @return
	 */
	public Boolean getShowGraph() {
		return showGraph;
	}

	/**
	 * 
	 * @param showGraph
	 */
	public void setShowGraph(Boolean showGraph) {
		this.showGraph = showGraph;
	}

	/**
	 * 
	 * @return
	 */
	public Double getRangeMin() {
		return rangeMin;
	}

	/**
	 * 
	 * @param rangeMin
	 */
	public void setRangeMin(Double rangeMin) {
		this.rangeMin = rangeMin;
	}

	/**
	 * 
	 * @return
	 */
	public Double getRangeMax() {
		return rangeMax;
	}

	/**
	 * 
	 * @param rangeMax
	 */
	public void setRangeMax(Double rangeMax) {
		this.rangeMax = rangeMax;
	}

	/**
	 * 
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * 
	 * @param unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * 
	 * @return the alert
	 */
	public Integer getAlert() {
		return alert;
	}

	/**
	 * 
	 * @param alert the alert to set
	 */
	public void setAlert(Integer alert) {
		this.alert = alert;
	}

	/**
	 * @return the alertDetailMsg
	 */
	public String getAlertDetailMsg() {
		return alertDetailMsg;
	}

	/**
	 * @param alertDetailMsg the alertDetailMsg to set
	 */
	public void setAlertDetailMsg(String alertDetailMsg) {
		this.alertDetailMsg = alertDetailMsg;
	}

	/**
	 * @return the alertGroupIds
	 */
	public ArrayList<Long> getAlertGroupIds() {
		return alertGroupIds;
	}

	/**
	 * @param alertGroupIds the alertGroupIds to set
	 */
	public void setAlertGroupIds(ArrayList<Long> alertGroupIds) {
		this.alertGroupIds = alertGroupIds;
	}
	
	/**
	 * @return the emailIds
	 */
	public ArrayList<String> getEmailIds() {
		return emailIds;
	}

	/**
	 * @param emailIds the emailIds to set
	 */
	public void setEmailIds(ArrayList<String> emailIds) {
		this.emailIds = emailIds;
	}

	/**
	 * @return the alertCode
	 */
	public Long getAlertCode() {
		return alertCode;
	}

	/**
	 * @param alertCode the alertCode to set
	 */
	public void setAlertCode(Long alertCode) {
		this.alertCode = alertCode;
	}
	
	/**
	 * 
	 * @return the alert on reset flag
	 */
	public Integer getAlertOnReset() {
		return alertOnReset;
	}
	/**
	 * 
	 * @param alertOnReset flag to set
	 */
	public void setAlertOnReset(Integer alertOnReset) {
		this.alertOnReset = alertOnReset;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Attribute [" + (name != null ? "name=" + name + ", " : "")
				+ (scope != null ? "scope=" + scope + ", " : "") 
				+ "factor=" + factor + ", "
				+ (legend != null ? "legend=" + legend + ", " : "") 
				+ "minValue=" + minValue 
				+ ", maxValue=" + maxValue
				+ ", excursionDuration=" + excursionDuration 
				+ ", multiplyFactor=" + multiplyFactor 
				+ ", deviceTypeId="	+ deviceTypeId 
				+ ", notifyLimit=" + notifyLimit + ", "
				+ (roundOffTo != null ? "roundOffTo=" + roundOffTo + ", " : "")
				+ (showGraph != null ? "showGraph=" + showGraph + ", " : "")
				+ (rangeMin != null ? "rangeMin=" + rangeMin + ", " : "")
				+ (rangeMax != null ? "rangeMax=" + rangeMax + ", " : "") 
				+ (unit != null ? "unit=" + unit + ", " : "")
				+ (alertOnReset != null ? "alertOnReset=" + alertOnReset + ", " : "")
				+ (alert != null ? "alert=" + alert + ", " : "")+ "]";
	}

}
