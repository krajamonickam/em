package com.rfxcel.sensor.util;

import java.io.File;
import java.net.URL;
import java.util.Properties;

import org.apache.log4j.Logger;

public class JBossPropLoader {
	final static Logger logger = Logger.getLogger(JBossPropLoader.class);

	public JBossPropLoader() {
	}

	public static Properties getProperties(String propertiesFileName)
	{
		Properties jbossProp = new Properties();

		try {
			String dir = System.getProperty("jboss.server.config.url");
			if (dir == null) {
				dir = "file:///./";
			}
			String path = dir + propertiesFileName;  
			URL url = new URL(path);  
			  
			if(new File(url.toURI()).exists()) {  
			    jbossProp.load(url.openStream());  
			    logger.debug("loaded application properties from file: " + path);  
			} else {  
			    jbossProp.load(JBossPropLoader.class.getResourceAsStream("/" + propertiesFileName));  
			    logger.debug("loaded application properties: /"+propertiesFileName);  
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(jbossProp);
	}
}
