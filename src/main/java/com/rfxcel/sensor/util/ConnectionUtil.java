package com.rfxcel.sensor.util;

import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class ConnectionUtil {

	private static ConnectionUtil singelton;
	private final PoolingHttpClientConnectionManager connManager;
	private ConnectionUtil(){
		connManager = new PoolingHttpClientConnectionManager(); 
		    connManager.setMaxTotal(100); 
		    connManager.setDefaultMaxPerRoute(90); 
		    
	}

	/**
	 * @return
	 */
	public static ConnectionUtil getInstance(){
		if(singelton ==  null){
			synchronized (ConnectionUtil.class) {
				singelton = new ConnectionUtil();
			}
		}
		return singelton;
	}
	
	/**
	 * @return
	 */
	public PoolingHttpClientConnectionManager getConnectionManager(){
		return connManager;
	}
}
