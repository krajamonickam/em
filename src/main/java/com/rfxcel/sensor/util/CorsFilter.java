package com.rfxcel.sensor.util;
import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletResponse;


public class CorsFilter implements Filter {

	public void doFilter(ServletRequest req, ServletResponse res, FilterChain chain)
            throws IOException, ServletException {
    	
		//To allowing cross domain access 
        ((HttpServletResponse) res).addHeader("Access-Control-Allow-Origin", "*");
        ((HttpServletResponse) res).addHeader("Access-Control-Allow-Methods", "PUT, GET, POST, DELETE");
        ((HttpServletResponse) res).addHeader("Access-Control-Max-Age", "-1");
        ((HttpServletResponse) res).addHeader("Cache-Control", "no-store");
        ((HttpServletResponse) res).addHeader("Pragma", "no-cache");
        ((HttpServletResponse) res).addDateHeader("Expires", 0); // Proxies.
        ((HttpServletResponse) res).addHeader("Access-Control-Allow-Headers", "x-requested-with, Origin, Content-Type, Accept, access-control-allow-headers,Cache-Control,authToken");
        ((HttpServletResponse) res).addHeader("Access-Control-Allow-Credentials", "true");
        
        chain.doFilter(req, res);
    
    }

    public void destroy() {
    }

	public void init(FilterConfig arg0) throws ServletException {
		// TODO Auto-generated method stub
		
	}
    
   
}
