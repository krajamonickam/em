package com.rfxcel.sensor.util;

import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.rfxcel.sensor.queclink.receiver.QueclinkServer;
import com.rfxcel.sensor.thingspace.receiver.PollService;
import com.rfxcel.auth2.dao.AuthDao;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;

/**
 * Servlet implementation class InitializationServlet
 */
public class InitializationServlet extends HttpServlet {
    
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	// static initializer
	{
		org.apache.log4j.PropertyConfigurator.configureAndWatch("log4j.properties");
		ConfigCache.getInstance().populateOrgConfig();				
		ConfigCache.getInstance().populateSystemConfig();
		
		//sendumDAO.addDeviceFromAssociate();

		//sendumDAO.getDeviceLog();
		//sendumDAO.getMaxDeviceBactLog();
		
		DeviceCache.getInstance();
		//sendumDAO.getDeviceNotifyLog();
		
		AssociationCache.getInstance();
		
		/* PollDweetThread dweetThread = new PollDweetThread();
		 dweetThread.poll();*/
	
		//Start Queclink server
		QueclinkServer.getInstance();
		
		//Starting thingspace polling server
		//PollService.getInstance();
	}
	
	
    /**
     * @see HttpServlet#HttpServlet()
     */
    public InitializationServlet() {
        super();
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		resp.setContentType("text/html");
		OutputStreamWriter ow = new OutputStreamWriter(resp.getOutputStream());
		PrintWriter pw = new PrintWriter(ow);
		Object refresh = req.getParameter("refresh");
		Integer refreshFreq = refresh== null ? 60 : Integer.valueOf(refresh.toString());
		pw.println("<html><head><meta http-equiv=\"refresh\" content=\""+refreshFreq+"\"></head><body>");
		pw.println("<br/>Federation Status:OFF<br/>");
		pw.println("</body></html>");
		pw.flush();
		pw.close();
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	}
}
