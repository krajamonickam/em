package com.rfxcel.sensor.util;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class Sensor {

	public Sensor() {
	}

	protected String deviceIdentifier;
	protected String temperature;
	protected String latitude;
	protected String longitude;
	protected String statusTimeStamp;
	protected String deviceType;
	protected String alarmTimeStamp;
	protected String geofenceNumber;
	protected String geofenceEvent;
	protected String hepe;
	protected String fixType;
	protected String battery;
	protected String tilt;
	protected String shock;
	protected String vibration;
	protected String light;
	protected String pressure;
	protected String humidity;
	public String getGrowthLog() {
		return growthLog;
	}

	public void setGrowthLog(String growthLog) {
		this.growthLog = growthLog;
	}

	public String getCumulativeLog() {
		return cumulativeLog;
	}

	public void setCumulativeLog(String cumulativeLog) {
		this.cumulativeLog = cumulativeLog;
	}

	public String getVoltage() {
		return voltage;
	}

	public void setVoltage(String voltage) {
		this.voltage = voltage;
	}

	public String getExcursionStatus() {
		return excursionStatus;
	}

	public void setExcursionStatus(String excursionStatus) {
		this.excursionStatus = excursionStatus;
	}

	public String getExcursionAttributes() {
		return excursionAttributes;
	}

	public void setExcursionAttributes(String excursionAttributes) {
		this.excursionAttributes = excursionAttributes;
	}

	protected String growthLog;
	protected String cumulativeLog;
	protected String voltage;
	protected String excursionStatus;
	protected String excursionAttributes;
	/**
	 * @return the deviceIdentifier
	 */
	public String getDeviceIdentifier() {
		return deviceIdentifier;
	}
	
	/**
	 * @param deviceIdentifier the deviceIdentifier to set
	 */
	public void setDeviceIdentifier(String deviceIdentifier) {
		this.deviceIdentifier = deviceIdentifier;
	}
	
	/**
	 * @return the temperature
	 */
	public String getTemperature() {
		return temperature;
	}
	
	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(String temperature) {
		this.temperature = temperature;
	}
	
	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}
	
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	
	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}
	
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	
	/**
	 * @return the statusTimeStamp
	 */
	public String getStatusTimeStamp() {
		return statusTimeStamp;
	}
	
	/**
	 * @param statusTimeStamp the statusTimeStamp to set
	 */
	public void setStatusTimeStamp(String statusTimeStamp) {
		this.statusTimeStamp = statusTimeStamp;
	}
	
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}
	
	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	
	/**
	 * @return the alarmTimeStamp
	 */
	public String getAlarmTimeStamp() {
		return alarmTimeStamp;
	}
	
	/**
	 * @param alarmTimeStamp the alarmTimeStamp to set
	 */
	public void setAlarmTimeStamp(String alarmTimeStamp) {
		this.alarmTimeStamp = alarmTimeStamp;
	}
	
	/**
	 * @return the geofenceNumber
	 */
	public String getGeofenceNumber() {
		return geofenceNumber;
	}
	
	/**
	 * @param geofenceNumber the geofenceNumber to set
	 */
	public void setGeofenceNumber(String geofenceNumber) {
		this.geofenceNumber = geofenceNumber;
	}
	
	/**
	 * @return the geofenceEvent
	 */
	public String getGeofenceEvent() {
		return geofenceEvent;
	}
	
	/**
	 * @param geofenceEvent the geofenceEvent to set
	 */
	public void setGeofenceEvent(String geofenceEvent) {
		this.geofenceEvent = geofenceEvent;
	}
	
	/**
	 * @return the hepe
	 */
	public String getHepe() {
		return hepe;
	}
	
	/**
	 * @param hepe the hepe to set
	 */
	public void setHepe(String hepe) {
		this.hepe = hepe;
	}

	/**
	 * @return the fixType
	 */
	public String getFixType() {
		return fixType;
	}

	/**
	 * @param fixType the fixType to set
	 */
	public void setFixType(String fixType) {
		this.fixType = fixType;
	}
	
	/**
	 * @return the battery
	 */
	public String getBattery() {
		return battery;
	}

	/**
	 * @param battery the battery to set
	 */
	public void setBattery(String battery) {
		this.battery = battery;
	}

	/**
	 * @return the tilt
	 */
	public String getTilt() {
		return tilt;
	}

	/**
	 * @param tilt the tilt to set
	 */
	public void setTilt(String tilt) {
		this.tilt = tilt;
	}

	/**
	 * @return the shock
	 */
	public String getShock() {
		return shock;
	}

	/**
	 * @param shock the shock to set
	 */
	public void setShock(String shock) {
		this.shock = shock;
	}

	/**
	 * @return the vibration
	 */
	public String getVibration() {
		return vibration;
	}

	/**
	 * @param vibration the vibration to set
	 */
	public void setVibration(String vibration) {
		this.vibration = vibration;
	}

	/**
	 * @return the light
	 */
	public String getLight() {
		return light;
	}

	/**
	 * @param light the light to set
	 */
	public void setLight(String light) {
		this.light = light;
	}

	/**
	 * @return the pressure
	 */
	public String getPressure() {
		return pressure;
	}

	/**
	 * @param pressure the pressure to set
	 */
	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	/**
	 * @return the humidity
	 */
	public String getHumidity() {
		return humidity;
	}

	/**
	 * @param humidity the humidity to set
	 */
	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}
	
	
}
