package com.rfxcel.sensor.util;

import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.math.BigDecimal;
import java.math.MathContext;
import java.math.RoundingMode;

public class CustomShellfishLogs {
	public static final MathContext mc = new MathContext(10, RoundingMode.HALF_UP);
	public static final BigDecimal factor = new BigDecimal("0.01122", mc);
	public static final BigDecimal subtrahend = new BigDecimal("0.4689", mc);
	public static final BigDecimal minPerHour = new BigDecimal("60", mc);
	public static final BigDecimal tempFifty = new BigDecimal("50", mc);
	public static final int power = 2;

	public CustomShellfishLogs() {
		
	}

	public static void main(String[] args) {
		if (args.length != 1) {
			return;
		}
		try {
			String separator = ",";
			PrintWriter pw = new PrintWriter(new FileWriter(args[0]));
			pw.println("Oyster Temperature (degrees F)" + separator + "Growth Rate (logs/hr)" + separator + "Growth Rate (logs/min)");
			for (int f = 50; f <= 100; f++) {
				double temp = (double)f;
				double lph  = getLogsPerHour(temp);
				BigDecimal lpm  = getLogsPerMinute(temp);
				pw.println(temp + separator + lph + separator + lpm);
			}
			pw.flush();
			pw.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static double getLogsPerHour(double fahrenheit)
	{
		if (fahrenheit < 50d) return(0d);
		BigDecimal temp = new BigDecimal(fahrenheit, mc);
		BigDecimal logs = temp.multiply(factor).subtract(subtrahend).pow(power);
		return(logs.doubleValue());
	}
	
	public static BigDecimal getLogsPerMinute(double fahrenheit)
	{
		if (fahrenheit < 50d) return(new BigDecimal(0));
		BigDecimal temp = new BigDecimal(fahrenheit, mc);
		BigDecimal logs = temp.multiply(factor).subtract(subtrahend).pow(power).divide(minPerHour);
		return(logs);
	}

}
