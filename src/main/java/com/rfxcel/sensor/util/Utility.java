package com.rfxcel.sensor.util;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringReader;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.TimeZone;
import java.util.TreeMap;
import java.util.concurrent.ConcurrentHashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.codec.binary.Base64;
import org.apache.commons.collections.keyvalue.MultiKey;
import org.apache.log4j.Logger;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.type.TypeReference;
import org.joda.time.DateTime;
import org.joda.time.LocalDateTime;

import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.beans.DevicePackageStatus;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;

public class Utility {
	final static Logger logger = Logger.getLogger(Utility.class);
	private static HashMap<String, String> paramColumnMap = new HashMap<String, String>();
	
	public static int getInteger(String value, int defaultInt)
	{
		if ((value == null) || (value.trim().length() == 0)) {
			return(defaultInt);
		}
		try
		{
			int valueInt = Integer.parseInt(value);
			return(valueInt);
		}
		catch(Exception ex)
		{
			return(defaultInt);
		}
	}
	
	public static double getDouble(String value, double defaultDbl)
	{
		if ((value == null) || (value.trim().length() == 0)) {
			return(defaultDbl);
		}
		try
		{
			double valueDbl = Double.parseDouble(value);
			return(valueDbl);
		}
		catch(Exception ex)
		{
			return(defaultDbl);
		}
	}

	public static double round(double x, int num)
	{
		double factor = Math.pow(10, num); 
		x = x * factor;
		long n = Math.round(x);
		double result = ((double) n)/factor;
		return(result);
	}

	/**
	 * decrypts the credentials returned from the authorization header.
	 * 
	 * @param encodedString
	 * @return
	 */
	public static String[] decrypytUserNamePwd(final String encodedString) {
		final byte[] decrypytedBytes = Base64.decodeBase64(encodedString
				.getBytes());
		final String userNamePwdPair = new String(decrypytedBytes);
		final String[] credentials = userNamePwdPair.split(":", 2);
		return credentials;
	}

	/**
	 * converts the json string into the map.
	 * 
	 * @param payload
	 * @return
	 */
	public static Map<String, String> convertJsonToMap(String payload) {
		Map<String, String> map = new HashMap<String, String>();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			mapperObj.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			Map<String, Object> objMap = mapperObj.readValue(payload,new TypeReference<HashMap<String, Object>>() {});
			for (String key : objMap.keySet()) {
				Object obj = objMap.get(key);
				map.put(key, obj.toString());
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}
	
	/**
	 * 
	 * @param payload
	 * @return
	 */
	public static Map<String, Object> convertNestedJsonToMap(String payload) {
		Map<String, Object> map = new HashMap<String, Object>();
		ObjectMapper mapperObj = new ObjectMapper();
		try {
			mapperObj.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			map = mapperObj.readValue(payload,new TypeReference<HashMap<String, Object>>() {});
		} catch (IOException e) {
			e.printStackTrace();
		}
		return map;
	}

	public static Map<String, String> stringToMap(String mapText) {
		try {
			Properties props = new Properties();
			props.load(new StringReader(mapText.substring(1,
					mapText.length() - 1).replace(", ", "\n")));
			Map<String, String> map = new HashMap<String, String>();
			for (Map.Entry<Object, Object> e : props.entrySet()) {
				map.put((String) e.getKey(), (String) e.getValue());
			}
			return (map);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return (null);
	}
	
	public static void stringToFile(String text, File file) 
	{
		try
		{
			PrintWriter pw = new PrintWriter(new FileWriter(file));
			pw.print(text);
			pw.flush();
			pw.close();
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	public static Map<String, String> convertStringOutMap(String mapText) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			Map<String, String> srcMap = mapper.readValue(mapText, new TypeReference<Map<String, String>>(){});
			Map<String, String> outMap = new HashMap<String, String>();
			String deviceIdentifer = srcMap.get("deviceIdentifier");

			if(deviceIdentifer == null){
				Map<String, String> newMap = new TreeMap<String, String>(String.CASE_INSENSITIVE_ORDER);
				newMap.putAll(srcMap);
				for(String attrName : DeviceCache.getInstance().getAtttributeNameList(deviceIdentifer)){
					if(newMap.containsKey(attrName)){
						outMap.put(attrName, newMap.get(attrName));
					}
				}
			}
			return (outMap);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return (null);
	}

	public static Timestamp jodaToSQLTimestamp(LocalDateTime localDateTime) {
		return new Timestamp(localDateTime.toDateTime().getMillis());
	}

	public static ExcursionTime calculateExcursion (DeviceInfo deviceInfo) {
		return Timer.getDifferenceTime(deviceInfo.getStartTime(),deviceInfo.getEndTime());
	}

	public static DeviceInfo updateDeviceLog(ConcurrentHashMap<String, DeviceInfo> cMap,String attributeName,double attributeValue,String deviceId) {
		DeviceInfo deviceInfo = cMap.get(deviceId);
		deviceInfo.setAttributeName(attributeName);
		deviceInfo.setAttributeValue(String.valueOf(attributeValue));
		deviceInfo.setEndTime(new DateTime());
		cMap.remove(deviceId);
		cMap.put(deviceId, deviceInfo);
		return deviceInfo;
	}
	
	public static String convertEPOCToDate(String epocTime){
		String formatedDate = null;
		try{
			Long time = new Long(epocTime);
			//Since the data is coming in EPOC Seconds and date object takes number of milliseconds since the standard time, multiplying it by 1000
			Date date = new Date(time*1000);	 
			DateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			format.setTimeZone(TimeZone.getTimeZone("UTC"));
			formatedDate = format.format(date);
		}
		catch(Exception e){
			logger.error("Exception while formating EPOC date for Kirsen Sensor "+ e.getMessage());
		}
		return formatedDate;
	}
	
	/**
	 * 
	 * @param originalFormatString
	 * @param newFormatString
	 * @param dateValue
	 * @return
	 */
	public static String convertDateFormat(String originalFormatString, String newFormatString, String dateValue){
		String formatedDate = null;
		try{
			DateFormat originalFormat = new SimpleDateFormat(originalFormatString);
			DateFormat newFormat = new SimpleDateFormat(newFormatString);
			Date date = originalFormat.parse(dateValue);
			formatedDate = newFormat.format(date);
		}
		catch(Exception e){
			logger.error("Exception while converting date from format "+ originalFormatString + " to "+newFormatString +" "+ e.getMessage());
		}
		return formatedDate;
	}

	public static void updateShipmentStatus(DevicePackageStatus devicePackageStatus) {
		try {
			// If there is excursion, set status to red
			if( (devicePackageStatus.isNofityExcursion() || devicePackageStatus.isDerivedExcursion())){ 
				devicePackageStatus.setStatusId(SensorConstant.DEVICE_PACKAGE_STATE_RED);
				
			}else{
				// if statusId is not 2 then check if there are some attributes which have not come down to normal value.
				HashSet<String> attrList = AssociationCache.getInstance().getShipmentExcursionAttr(devicePackageStatus.getDeviceId(), devicePackageStatus.getPackageId());
				if(attrList!= null && attrList.size() >0){
					devicePackageStatus.setStatusId(SensorConstant.DEVICE_PACKAGE_STATE_RED);
				}
				else if(getLastStatusInfo(devicePackageStatus) != 0){
						devicePackageStatus.setStatusId(SensorConstant.DEVICE_PACKAGE_STATE_YELLOW);
					}
				}
			AssociationDAO.getInstance().updateStatus(devicePackageStatus);
			AssociationDAO.getInstance().updateSensorDataStatus(devicePackageStatus);
		} catch (NumberFormatException e) {
			logger.error("Exception while updating shipment status: Method : updateShipmentStatus : "+ e.getMessage());
		} catch (SQLException e) {
			logger.error("Exception while updating shipment status: Method : updateShipmentStatus : "+ e.getMessage());
			
		}
	}
	
	private static int getLastStatusInfo(DevicePackageStatus devicePackageStatus){
		MultiKey key = new MultiKey(devicePackageStatus.getDeviceId(),devicePackageStatus.getPackageId());
		if(AssociationCache.getInstance().getShipmentStatus(key)!= null){
			return Integer.parseInt(AssociationCache.getInstance().getShipmentStatus(key));
		}
		return 0;
	}
	
	
	public static void startDataSimulator() throws Exception {
		String path = ConfigCache.getInstance().getSystemConfig("data.simulator.path");
		Runtime.getRuntime().exec("cmd /c start " + path);

	}
	
	/**
	 * 
	 * @param lat1
	 * @param lng1
	 * @param lat2
	 * @param lng2
	 * @return distance between geopoint location(lat1, lng1) and device location(lat2, lng2) in meter
	 */
	public static double getDistance(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(lat2-lat1);
        double dLng = Math.toRadians(lng2-lng1);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                   Math.cos(Math.toRadians(lat1)) * Math.cos(Math.toRadians(lat2)) *
                   Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = (double) (earthRadius * c);

        return dist;
    }
	/**
	 * 
	 * @param str
	 * @return the string in human readable format
	 */
	public static String getCamelCaseValue(String str) {
		str = str.replaceAll(String.format("%s|%s|%s", "(?<=[A-Z])(?=[A-Z][a-z])", "(?<=[^A-Z])(?=[A-Z])",
				"(?<=[A-Za-z])(?=[^A-Za-z])"), " ");

		StringBuffer stringbf = new StringBuffer();
		Matcher m = Pattern.compile("([a-z])([a-z]*)", Pattern.CASE_INSENSITIVE).matcher(str);

		while (m.find()) {
			m.appendReplacement(stringbf, m.group(1).toUpperCase() + m.group(2).toLowerCase());
		}

		return m.appendTail(stringbf).toString();
	}

	public static synchronized SimpleDateFormat getDateFormat(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		return simpleDateFormat;
	}
	public static synchronized SimpleDateFormat getSimpleDateFormat(){
		SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		
		return simpleDateFormat;
	}
	/**
	 * <p>It is used to convert string date in UTC time stamp.</p>
	 * @param date
	 * @return
	 * @throws ParseException
	 */
	public static Long getUTCDate(String date) throws ParseException{
		return getDateFormat().parse(date).getTime();
	}
	/**
	 * <p>It is used to convert time stamp date in UTC string format.</p>
	 * @param date
	 * @return
	 */
	public static String getUTCDateString(Long date){
		return Utility.getDateFormat().format(new Date(date));
	}
	/**
	 * <p>It is used to convert string date in SimpleDateFormat string format.</p>
	 * @param date
	 * @return
	 */
	public static String getSimpleDateFormatDateStr(Date date){
		return getSimpleDateFormat().format(date);
	}
	
	public static long convertOffsetToMillis(String offset){		
		String[] tokens = offset.split(":");
		int minutesToMs = Integer.parseInt(tokens[1]) * 60000;
		int hoursToMs = Integer.parseInt(tokens[0]) * 3600000;
		long total = minutesToMs + hoursToMs;		
		return total;		
	}
	
	/**
	 * Creating a mapping between input attributes and table columns
	 */
	public static HashMap<String, String> populateParamColumnMap() {
		paramColumnMap.put("temperature", "temperature");
		paramColumnMap.put("tilt", "tilt");
		paramColumnMap.put("pressure", "pressure");
		paramColumnMap.put("humidity", "humidity");
		paramColumnMap.put("light", "light");
		paramColumnMap.put("growthLog", "growth_log");
		paramColumnMap.put("cumulativeLog", "cumulative_log");
		paramColumnMap.put("battery", "battery_rem");
		paramColumnMap.put("latitude", "latitude");
		paramColumnMap.put("longitude", "longitude");
		paramColumnMap.put("shock", "shock"); 
		paramColumnMap.put("vibration", "vibration"); 
		return paramColumnMap;
		
	}
	/**
	 * <p>It is used to check the string contains special character</p>
	 * @param inputString
	 * @return
	 */
	public static Boolean hasSpecialCharacter(String inputString){
		Pattern special = Pattern.compile ("[!@#$%&*^:/()+=|<>?{}\\[\\]~-]");
		Matcher hasSpecialChars = special.matcher(inputString);
		return hasSpecialChars.find();
	}
	
	public static double c2F(double c) {
		double f = ((c * 9d)/5d) + 32d;
		return(f);
	}

	public static double f2C(double f) {
		double c = ((f - 32d) * 5d)/9d;
		return(c);
	}
	
	public static double mapF2C(double f, int orgId) {
		String format = ConfigCache.getInstance().getOrgConfig("sensor.temperature.format", orgId);
		if ((format == null) || (SensorConstant.TEMPERATURE_FORMAT_FAHRENHEIT.equalsIgnoreCase(format))){
			return(f2C(f));
		}else{
			return(f);
		}
	}
	
	public static String formatDate(String srcFormat,String desFormat,String dateStr)
	{
		 
		 try {
			DateFormat srcDf = new SimpleDateFormat(srcFormat);
			Date date = srcDf.parse(dateStr);
			DateFormat destDf = new SimpleDateFormat(desFormat);
			dateStr = destDf.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return dateStr;
	}
}
