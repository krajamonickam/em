/**
 * @ COMPANY              : RFXCEL
 * @ PROJECT              : Sensor Receiver
 * @ AUTHOR               : JEYASEELAN   
 * @ DESCRIPTION          : 
 *
 * @ MODIFICATION HISTORY:
 **********************************************************************************
 * DATE			*		NAME                *       			CHANGES           *
 **********************************************************************************
 * Jun 1, 2016	*		JEYASEELAN A		* CREATED                             *
 * 				*							*                                     *
 *				*							*									  *
 *				*							*									  *
 **********************************************************************************
 */
package com.rfxcel.sensor.util;


public class SensorConstant {
	//public static String EVENT_TYPE_SENSOR 			= "SENSOR";
	//public static String EVENT_TYPE_ALARM 			= "ALARM";
	//public static String EVENT_TYPE_SAME_LOCATION 	= "SAME_LOCATION";
	//public static String JBOSS_PROPERTY_FILE_GEO_FENCING 	= "geofence.properties";
	//public static String JBOSS_PROPERTY_FILE_THERMAL_CAMERA 	= "dweet.properties";
	
	public static String TEMPERATURE_FORMAT_CELSIUS 	= "C";
	public static String TEMPERATURE_FORMAT_FAHRENHEIT 	= "F";
	
	//public static String BACTERIAL_GROWTH_LOG 		= "GROWTHLOG";
	//public static String BACTERIAL_CUMULATIVE_LOG 	= "CUMULATIVELOG";
	
	//0-Actice, 1-Inactive, 2-Min Value for Duration, 3-Max Value for Duration
	//public static int ATTRIBUTE_EXCURSION_DURATION_NO 					= 0;
	//public static int ATTRIBUTE_EXCURSION_DURATION_MINIMUM 				= 1;
	//public static int ATTRIBUTE_EXCURSION_DURATION_MAXIMUM 				= 2;
	//public static int ATTRIBUTE_EXCURSION_DURATION_MINIMUM_AND_MAXIMUM 	= 3;
	
	//public static String ATTRIBUTE_NAME_TEMPERATURE 		= "temperature";
	public static String ATTRIBUTE_NAME_LIGHT 				= "light";
	public static String ATTRIBUTE_NAME_CAPACITY_REM 		= "capacityRemaining";
	public static String ATTRIBUTE_NAME_CAPACITY_FULL 		= "capacityFull";
	public static String ATTRIBUTE_NAME_BATTERY 		= "battery";
	//public static String ATTRIBUTE_NAME_NOTIFY_LIMIT 		= "notifyLimit";
	
	public static double BATTERY_DEFAULT_VALUE			= 3760;
	//public static double BATTERY_DEFAULT_EXTENDED_VALUE			= 10000;
	
	//public static final String SENSOR_SENDUM = "sendum";
	public static final String SENSOR_QUECLINK = "queclink";
	public static final String SENSOR_KIRSEN = "kirsen";
	public static final String SENSOR_SENDUM = "sendum";
	public static final String SENSOR_BOSCH = "bosch";
	public static final String SENSOR_VERIZON ="verizon";
	public static final String DEVICE_KEY_DEMO = "demo";
	public static final int RESPONSECODE_SUCCESS = 200;
	public static final int RESPONSECODE_ERROR = 500;
	public static final int RESPONSECODE_AUTHENTICATION_FAILED = 403;
	public static final String RESP_STAT_SUCCESS = "success";
	public static final String RESP_STAT_ERROR = "error";
	
	public static final int DEVICE_STATE_COMMISSION = 1;
	public static final int DEVICE_STATE_ASSOCIATED = 2;
	public static final int DEVICE_STATE_DISASSOICATED = 3;
	public static final int DEVICE_STATE_DECOMMISSIONED = 4;
	
	public static final int DEVICE_PACKAGE_STATE_GREEN = 0;
	public static final int DEVICE_PACKAGE_STATE_YELLOW = 1;
	public static final int DEVICE_PACKAGE_STATE_RED = 2;
	
	public static final int DEVICE_ENTERED_GEOPOINT = 1;
	public static final int DEVICE_EXITED_GEOPOINT = 0;
	
	
	public static final String SEARCH_TYPE_CURRENT_SHIPMENTS ="1";
	public static final String SEARCH_TYPE_PENDING_SHIPMENTS ="2";
	public static final String SEARCH_TYPE_COMPLETED_SHIPMENTS ="3";
	public static final String SEARCH_TYPE_ALL ="ALL";
	public static final String TIME_ZONE_OFFSET = "+00:00";
	
	public static final String AUTHENTICATE_REQUEST = "sensor.authenticate.request";
	
	public static final String DERIVED_ATTR_SAME_LOC = "sameLocation";
	//public static final String DERIVED_ATTR_EXCURSION_TIME = "excursionTime";
	public static final String LOG_FILE_LOC ="sendum.logFolder";
	
	public static final String AUTH_TOKEN_KEY= "rts.application.auth";
	
}

