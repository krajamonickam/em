package com.rfxcel.sensor.control;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.Response;


public class SendumController implements Runnable
{
	private static final Logger logger = Logger.getLogger(SendumController.class);
	public static SendumController instance = null;
	private ConfigCache configCache = ConfigCache.getInstance();
	private LinkedBlockingQueue<SendumRequest> queue = new LinkedBlockingQueue<SendumRequest>();
	private RESTClient http = null;
	private Thread thread = null;
	private String controlURL  = null;
	private String controlUser = null;
	private String controlPass = null;
	private String configURL  = null;
	private String configLogin  = null;
	private String configPassword  = null;
	
	public static class SendumRequest
	{
		public String deviceID;
		public String deviceKey;
		public SendumCmdMsg cmd;
		public Profile profile;

		public SendumRequest(String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq) {
			this.deviceID = deviceID;
			this.deviceKey = deviceKey;
			this.cmd = new SendumCmdMsg(commFreq, gpsFreq, tempFreq);
		}

		public String getDeviceID() {
			return deviceID;
		}
		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}
		public String getDeviceKey() {
			return deviceKey;
		}
		public void setDeviceKey(String deviceKey) {
			this.deviceKey = deviceKey;
		}
		public SendumCmdMsg getCmd() {
			return cmd;
		}
		public void setCmd(SendumCmdMsg cmd) {
			this.cmd = cmd;
		}
		
		public Profile getProfile() {
			return profile;
		}
		public void setProfile(Profile profile) {
			this.profile = profile;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class SendumCmdMsg
	{
		public String uplinterval;
//		public String autointerval;
		public String statusinterval;
		public String sendinterval;
//		public String alarmcheckinterval;
//		public String locationcheckinterval;
//		public String gpsjamming;
//		public String gpsonextra;
//		public String nomotionsleep;
//		public String shock;
//		public String accelconfig;
//		public String motionaccelsettings;
//		public String lowtemperature;
//		public String hightemperature;
//		public String lowbatterycap;
//		public String lowbatteryvoltage;
//		public String lowtempprobe;
//		public String hightempprobe;
//		public String lowpressure;
//		public String highpressure;
//		public String lowhumidity;
//		public String highhumidity;
//		public String hightilt;
//		public String highlight;
		
		public SendumCmdMsg() {
		}

		public SendumCmdMsg(int commFreq, int gpsFreq, int tempFreq) {
			super();
			this.sendinterval = (commFreq > 0) ? new Integer(commFreq).toString() : "STOP";
			this.uplinterval = (gpsFreq > 0) ? new Integer(gpsFreq).toString() : "STOP";
			this.statusinterval = (tempFreq > 0) ? new Integer(tempFreq).toString() : "STOP";
		}

		public String getUplinterval() {
			return uplinterval;
		}

		public void setUplinterval(String uplinterval) {
			this.uplinterval = uplinterval;
		}

		public String getStatusinterval() {
			return statusinterval;
		}

		public void setStatusinterval(String statusinterval) {
			this.statusinterval = statusinterval;
		}

		public String getSendinterval() {
			return sendinterval;
		}

		public void setSendinterval(String sendinterval) {
			this.sendinterval = sendinterval;
		}

		public static SendumCmdMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				SendumCmdMsg cmd = mapper.readValue(text, SendumCmdMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class SendumURLMsg
	{
		public String deviceidentifier;
		public String url;
		
		public SendumURLMsg() {
		}
		public SendumURLMsg(String deviceidentifier, String url) {
			super();
			this.deviceidentifier = deviceidentifier;
			this.url = url;
		}
		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
		public String getDeviceidentifier() {
			return deviceidentifier;
		}
		public void setDeviceidentifier(String deviceidentifier) {
			this.deviceidentifier = deviceidentifier;
		}
		public static SendumURLMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				SendumURLMsg cmd = mapper.readValue(text, SendumURLMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class SendumURLGetMsg
	{
		public String posturl;
		
		public SendumURLGetMsg() {
		}
		public SendumURLGetMsg(String deviceidentifier, String url) {
			super();
			this.posturl = url;
		}
		public String getPosturl() {
			return posturl;
		}
		public void setPosturl(String posturl) {
			this.posturl = posturl;
		}
		public static SendumURLGetMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				SendumURLGetMsg cmd = mapper.readValue(text, SendumURLGetMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	public static void main(String[] args) {
		try {
			SendumController c = SendumController.getInstance();
			c.setupDevice(0, "99000512004189", "4305519628", 180, 360, 180, null);
		} catch (Exception e) {
		}
	}
	
	public static SendumController getInstance()
	{
		if (instance == null) {
			instance = new SendumController();
		}
		return(instance);
	}
	
	public SendumController()
	{
		controlURL  = configCache.getSystemConfig("sendum.controller.url");
		controlUser = configCache.getSystemConfig("sendum.controller.userid");
		controlPass = configCache.getSystemConfig("sendum.controller.password");
		configURL   = configCache.getSystemConfig("sendum.config.url");
		configLogin = configCache.getSystemConfig("sendum.user");
		configPassword = configCache.getSystemConfig("sendum.password");
		if (controlURL == null) {
			logger.error("SendumController - system config invalid value for sendum.controller.url");
			controlURL = "https://restapi.sendum.com/<verb>";
			controlUser = "arunrao@rfxcel.com";
			controlPass = "sendum";
		}
		if (configURL == null) {
			logger.error("SendumController - system config invalid value for sendum.config.url");
			configURL = "https://www.verizonitt.net/sendum";
			configURL = "https://ittbeta.track-n-trace.net/sendum";
		}
		if (configLogin == null) {
			logger.error("SendumController - system config invalid value for sendum.user");
			configLogin = "sendum";
		}
		if (configPassword == null) {
			logger.error("SendumController - system config invalid value for sendum.password");
			configPassword = "Sendum123";
		}
		http = new RESTClient(controlUser, controlPass);
		logger.info("SendumController - initialized with account = " + controlUser + " for URL " + controlURL);
	}
	
	public void setupDevice(int orgID, String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq, Profile profile)
	{
		synchronized(this) {
			if (thread == null) {
				logger.info("SendumController - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		SendumRequest req = new SendumRequest(deviceID, deviceKey, commFreq, gpsFreq, tempFreq);
		req.setProfile(profile);
		this.queue.add(req);
		logger.info("SendumController - queuing request " + req.toString());
	}
	
	private void doSetupDevice(String deviceID, String deviceKey, String commFreq, String gpsFreq, String tempFreq, Profile profile)
	{
		changeURL(deviceID);
		changeSettings(deviceID, commFreq, gpsFreq, tempFreq, profile);
	}
	
	public void changeURL(String deviceID)
	{
		String url = controlURL.replace("<verb>", "rfxcelurl");
		url = url.replace("<deviceKey>", deviceID);
		
		String url2 = url + "?deviceidentifier=" + deviceID;
		logger.info("SendumController.changeURL - doSetupDevice url " + url2);
		String text = http.doGet(url2);
		logger.info("SendumController.changeURL - doSetupDevice doGet() " + text);
		SendumURLGetMsg preSetting = SendumURLGetMsg.fromJson(text);
		logger.info("SendumController.changeURL - doSetupDevice preSetting " + preSetting.toJson());

		SendumURLMsg newSetting = new SendumURLMsg(deviceID, configURL);
		String content = newSetting.toJson();
		http.doPut(url, content);
		logger.info("SendumController.changeURL - doSetupDevice PUT " + url + " content " + content);
		
		logger.info("SendumController.changeURL - doSetupDevice url " + url2);
		String text2 = http.doGet(url2);
		logger.info("SendumController.changeURL - doSetupDevice doGet() " + text2);
		SendumURLGetMsg postSetting = SendumURLGetMsg.fromJson(text2);
		logger.info("SendumController.changeURL - doSetupDevice postSetting " + postSetting.toJson());
	}
	
	public void changeSettings(String deviceID,  String commFreq, String gpsFreq, String tempFreq, Profile profile)
	{
		String getURL = controlURL.replace("<verb>", "deviceconfig1");
		String putURL = controlURL.replace("<verb>", "configdevice1");
		
		getURL = getURL + "?deviceidentifier=" + deviceID;
		logger.info("SendumController.changeSettings - doSetupDevice url " + getURL);
		String text = http.doGet(getURL);
		logger.info("SendumController.changeSettings - doSetupDevice doGet() " + text);
		SendumCmdMsg preSetting = SendumCmdMsg.fromJson(text);
		logger.info("SendumController.changeSettings - doSetupDevice preSetting " + preSetting.toJson());
		
		String shock = null;
		for (Attribute a: profile.getAttrType()) {
			String attrName = a.getName();
			double min = a.getMinValue();
			double max = a.getMaxValue();
			double rangeMin = a.getRangeMin();
			double rangeMax = a.getRangeMax();
			logger.info("Sendum Profile: " + attrName + " : " + min + ", " + max + ", " + rangeMin + ", " + rangeMax);
			if (attrName.equals("shock")) {
				int k = (int)max * 1000;
				shock = k + ",5";
			}
		}
		
		String json = "{" + 
				"\"uplinterval\" : \"" + gpsFreq + "\"," + 
				"\"statusinterval\" : \"" + tempFreq + "\"," + 
				"\"sendinterval\" : \"" + commFreq + "\"," + 
				"\"shock_mg\" : \"" + shock + "\"," + 
				"\"accelconfig\" : \"SHOCK\"" + 
				"}";
		
		logger.info("Sendum Profile Configuration: " + json);

		/*
		putURL = putURL + "?deviceidentifier=" + deviceID + "&"
				+ "uplinterval=" + gpsFreq + "&"
				+ "statusinterval=" + tempFreq + "&"
				+ "sendinterval=" + commFreq + "&"
				;
				*/
		
		putURL = putURL + "?deviceidentifier=" + deviceID;
		http.doPut(putURL, json);
		logger.info("SendumController.changeSettings - doSetupDevice PUT " + putURL);
		
		logger.info("SendumController.changeSettings - doSetupDevice url " + getURL);
		String text2 = http.doGet(getURL);
		logger.info("SendumController.changeSettings - doSetupDevice doGet() " + text2);
		SendumCmdMsg postSetting = SendumCmdMsg.fromJson(text2);
		logger.info("SendumController.changeSettings - doSetupDevice postSetting " + postSetting.toJson());
	}
	

	@Override
	public void run() {
		try
		{
			SendumRequest req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("SendumCmdMsg - dequeuing request " + req.toString());
				this.doSetupDevice(req.getDeviceID(), 
						req.getDeviceKey(), 
						req.getCmd().getSendinterval(),
						req.getCmd().getUplinterval(),
						req.getCmd().getStatusinterval(), req.getProfile());
			}
		}
		catch(Exception e)
		{
			logger.info("SendumCmdMsg thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}

	/**
	 * 
	 * @param userId 
	 * @param device
	 * @return
	 */
	public com.rfxcel.sensor.vo.Response locateDevice(long userId, Device device) {
		com.rfxcel.sensor.vo.Response resp = new com.rfxcel.sensor.vo.Response();
		try
		{
			String deviceID = device.getDeviceId();
			String postURL = controlURL.replace("<verb>", "locatenow");
			postURL = postURL + "?deviceidentifier=" + deviceID;
			logger.info("SendumController.locateDevice - POST url " + postURL);
			http.doPost(postURL, "{}");
			resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Locate Now command sent successfully to the device");
			return(resp);
		}
		catch(Exception ex)
		{
			logger.equals("Exception: " + ex.getMessage());
		}
		resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Locate device feature is not available for devices of type Sendum.");
		return(resp);
	}
}
