package com.rfxcel.sensor.control;

import java.util.ArrayList;
import java.util.HashMap;


import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.integration.service.DeviceAssociateSaveRequest;
import com.rfxcel.integration.service.DeviceAssociateSaveWrapperRequest;
import com.rfxcel.integration.service.DeviceDisassociateSaveRequest;
import com.rfxcel.integration.service.DeviceDisassociateSaveWrapperRequest;
import com.rfxcel.integration.service.ProductSearchResponse;
import com.rfxcel.integration.service.ReceiveSaveRequest;
import com.rfxcel.integration.service.ShipSaveRequest;
import com.rfxcel.integration.service.SiteList;
import com.rfxcel.integration.service.SiteListResponse;
import com.rfxcel.integration.service.TraceItem;
import com.rfxcel.integration.service.TraceItemRequest;
import com.rfxcel.integration.service.TraceItemResponse;
import com.rfxcel.integration.service.TraceItemWrapperRequest;
import com.rfxcel.integration.service.TraceLocation;
import com.rfxcel.integration.service.TraceLocationResponse;
import com.rfxcel.sensor.beans.DeviceInfoDetails;
import com.rfxcel.sensor.beans.Location;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.control.ItemController.ItemMapLocation;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorResponse;

public class EventController {
	private static final Logger logger = Logger.getLogger(EventController.class);
	//private static String authToken = "3d831cefe05d937e3a0428fc5387d48a";
	public static EventController instance = null;
	private RESTClient http = null;
	private ObjectMapper mapper = new ObjectMapper();

	public EventController() {
		http = new RESTClient("", "");
	}

	public static EventController getInstance() {
		if (instance == null) {
			instance = new EventController();
		}
		return instance;
	}
	public static class ItemData {
		public int allowedQty;
		public int quantity;
		public int mfrTpExtIdQlfr;
		public long mfrLocId;
		public int mfrLocVerId;
		public int mfrLocExtIdQlfr;
		public long entId;
		public int hasChild;
		public String containerId;
		public String productId;
		public int getAllowedQty() {
			return allowedQty;
		}
		public void setAllowedQty(int allowedQty) {
			this.allowedQty = allowedQty;
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
		public int getMfrTpExtIdQlfr() {
			return mfrTpExtIdQlfr;
		}
		public void setMfrTpExtIdQlfr(int mfrTpExtIdQlfr) {
			this.mfrTpExtIdQlfr = mfrTpExtIdQlfr;
		}
		public long getMfrLocId() {
			return mfrLocId;
		}
		public void setMfrLocId(long mfrLocId) {
			this.mfrLocId = mfrLocId;
		}
		public int getMfrLocVerId() {
			return mfrLocVerId;
		}
		public void setMfrLocVerId(int mfrLocVerId) {
			this.mfrLocVerId = mfrLocVerId;
		}
		public int getMfrLocExtIdQlfr() {
			return mfrLocExtIdQlfr;
		}
		public void setMfrLocExtIdQlfr(int mfrLocExtIdQlfr) {
			this.mfrLocExtIdQlfr = mfrLocExtIdQlfr;
		}
		public long getEntId() {
			return entId;
		}
		public void setEntId(long entId) {
			this.entId = entId;
		}
		public int getHasChild() {
			return hasChild;
		}
		public void setHasChild(int hasChild) {
			this.hasChild = hasChild;
		}
		public String getContainerId() {
			return containerId;
		}
		public void setContainerId(String containerId) {
			this.containerId = containerId;
		}
		public String getProductId() {
			return productId;
		}
		public void setProductId(String productId) {
			this.productId = productId;
		}
		
	}
	public static class BizTransaction {

		public String bizTransTypeValue;
		public String bizTransType;
		public String bizTransTypeGroup;
		public String bizTransId;
		public String bizTransDate;
		public String bizTransHasLines;
		public String bizTransExtId;
		public String bizTransGlobalId;
		public String getBizTransTypeValue() {
			return bizTransTypeValue;
		}
		public void setBizTransTypeValue(String bizTransTypeValue) {
			this.bizTransTypeValue = bizTransTypeValue;
		}
		public String getBizTransType() {
			return bizTransType;
		}
		public void setBizTransType(String bizTransType) {
			this.bizTransType = bizTransType;
		}
		public String getBizTransTypeGroup() {
			return bizTransTypeGroup;
		}
		public void setBizTransTypeGroup(String bizTransTypeGroup) {
			this.bizTransTypeGroup = bizTransTypeGroup;
		}
		public String getBizTransId() {
			return bizTransId;
		}
		public void setBizTransId(String bizTransId) {
			this.bizTransId = bizTransId;
		}
		public String getBizTransDate() {
			return bizTransDate;
		}
		public void setBizTransDate(String bizTransDate) {
			this.bizTransDate = bizTransDate;
		}
		public String getBizTransHasLines() {
			return bizTransHasLines;
		}
		public void setBizTransHasLines(String bizTransHasLines) {
			this.bizTransHasLines = bizTransHasLines;
		}
		public String getBizTransExtId() {
			return bizTransExtId;
		}
		public void setBizTransExtId(String bizTransExtId) {
			this.bizTransExtId = bizTransExtId;
		}
		public String getBizTransGlobalId() {
			return bizTransGlobalId;
		}
		public void setBizTransGlobalId(String bizTransGlobalId) {
			this.bizTransGlobalId = bizTransGlobalId;
		}
	    
	}
	public static class EventsData{
		public int bizEventType;
		public String eventStatusId;	
		public long eventId;		
		public String eventDateTime;
		public int eventCause;
		public String eventType;
		public long eventLoc;		
		public String eventTradingPartner;
		public String occuredAt;
		public long docId;
		public long parentEventId;
		public BizTransaction bizTransactions;
		public long eventTypeId;
		public String eventStatus;
		public ItemData parent = new ItemData();
		public String receiverTpExtId;
		public String senderTpId;
		public String senderLocName;
		public String receiverTpId;
		public String receiverLocName;
		public String eventExtId;
		public String latitude;
		public String longitude;
		public String eventLocationId;
		public String receiver;
		public String bizTransId;
		public String bizTransDate;
		public String disposition;
		public String sender;
		public String eventLocationName;
		public String bizTransType;
		
		
		public String getEventLocationId() {
			return eventLocationId;
		}
		public void setEventLocationId(String eventLocationId) {
			this.eventLocationId = eventLocationId;
		}
		public String getReceiver() {
			return receiver;
		}
		public void setReceiver(String receiver) {
			this.receiver = receiver;
		}
		public String getBizTransId() {
			return bizTransId;
		}
		public void setBizTransId(String bizTransId) {
			this.bizTransId = bizTransId;
		}
		public String getBizTransDate() {
			return bizTransDate;
		}
		public void setBizTransDate(String bizTransDate) {
			this.bizTransDate = bizTransDate;
		}
		public String getDisposition() {
			return disposition;
		}
		public void setDisposition(String disposition) {
			this.disposition = disposition;
		}
		public String getSender() {
			return sender;
		}
		public void setSender(String sender) {
			this.sender = sender;
		}
		public String getEventLocationName() {
			return eventLocationName;
		}
		public void setEventLocationName(String eventLocationName) {
			this.eventLocationName = eventLocationName;
		}
		public String getBizTransType() {
			return bizTransType;
		}
		public void setBizTransType(String bizTransType) {
			this.bizTransType = bizTransType;
		}
		public String getReceiverTpId() {
			return receiverTpId;
		}
		public void setReceiverTpId(String receiverTpId) {
			this.receiverTpId = receiverTpId;
		}
		public String getReceiverLocName() {
			return receiverLocName;
		}
		public void setReceiverLocName(String receiverLocName) {
			this.receiverLocName = receiverLocName;
		}
		public String getEventExtId() {
			return eventExtId;
		}
		public void setEventExtId(String eventExtId) {
			this.eventExtId = eventExtId;
		}
		public String getSenderTpId() {
			return senderTpId;
		}
		public void setSenderTpId(String senderTpId) {
			this.senderTpId = senderTpId;
		}
		public String getSenderLocName() {
			return senderLocName;
		}
		public void setSenderLocName(String senderLocName) {
			this.senderLocName = senderLocName;
		}
		public String getReceiverTpExtId() {
			return receiverTpExtId;
		}
		public void setReceiverTpExtId(String receiverTpExtId) {
			this.receiverTpExtId = receiverTpExtId;
		}
		public ItemData getParent() {
			return parent;
		}
		public void setParent(ItemData parent) {
			this.parent = parent;
		}
		public int getBizEventType() {
			return bizEventType;
		}
		public void setBizEventType(int bizEventType) {
			this.bizEventType = bizEventType;
		}
		public String getEventStatusId() {
			return eventStatusId;
		}
		public void setEventStatusId(String eventStatusId) {
			this.eventStatusId = eventStatusId;
		}
		public long getEventId() {
			return eventId;
		}
		public void setEventId(long eventId) {
			this.eventId = eventId;
		}
		public String getEventDateTime() {
			return eventDateTime;
		}
		public void setEventDateTime(String eventDateTime) {
			this.eventDateTime = eventDateTime;
		}
		public int getEventCause() {
			return eventCause;
		}
		public void setEventCause(int eventCause) {
			this.eventCause = eventCause;
		}
		public String getEventType() {
			return eventType;
		}
		public void setEventType(String eventType) {
			this.eventType = eventType;
		}
		public long getEventLoc() {
			return eventLoc;
		}
		public void setEventLoc(long eventLoc) {
			this.eventLoc = eventLoc;
		}
		public String getEventTradingPartner() {
			return eventTradingPartner;
		}
		public void setEventTradingPartner(String eventTradingPartner) {
			this.eventTradingPartner = eventTradingPartner;
		}
		public String getOccuredAt() {
			return occuredAt;
		}
		public void setOccuredAt(String occuredAt) {
			this.occuredAt = occuredAt;
		}
		public long getDocId() {
			return docId;
		}
		public void setDocId(long docId) {
			this.docId = docId;
		}
		public long getParentEventId() {
			return parentEventId;
		}
		public void setParentEventId(long parentEventId) {
			this.parentEventId = parentEventId;
		}
		public BizTransaction getBizTransactions() {
			return bizTransactions;
		}
		public void setBizTransactions(BizTransaction bizTransactions) {
			this.bizTransactions = bizTransactions;
		}
		public long getEventTypeId() {
			return eventTypeId;
		}
		public void setEventTypeId(long eventTypeId) {
			this.eventTypeId = eventTypeId;
		}
		public String getEventStatus() {
			return eventStatus;
		}
		public void setEventStatus(String eventStatus) {
			this.eventStatus = eventStatus;
		}
		public String getLatitude() {
			return latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getLongitude() {
			return longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
	}

	public SensorResponse getEventList(int orgId) {
		SensorResponse response = new SensorResponse();
		Sensor sensor = new Sensor();
		ArrayList<DeviceInfoDetails> shipmentList = new ArrayList<DeviceInfoDetails>();
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		try {			
			String bodyText = "{\"eventIdCond\":\"0\",\"eventIdValue\":\"\",\"eventType\":\"6\",\"eventLocationIdCond\":\"\",\"eventLocationIdValue\":\"\",\"eventLocation\":\"\",\"eventDateCond\":\"0\",\"eventDateValue\":\"\",\"eventDateValue1\":\"\",\"eventCause\":\"\",\"senderLocIdCond\":\"0\",\"senderLocIdValue\":\"\",\"senderNameValue\":\"\",\"receiverLocIdCond\":\"0\",\"receiverLocIdValue\":\"\",\"receiverNameValue\":\"\",\"bizTransIdCond\":\"\",\"bizTransIdValue\":\"\",\"eventStatus\":\"\",\"attributeNameValue\":\"\",\"attributeValueCond\":\"0\",\"attributeValue\":\"\",\"itemTypeValue\":\"\",\"itemTypeIdCond\":\"\",\"itemTypeIdValue\":\"\",\"traceIdCond\":\"0\",\"traceIdValue\":\"\",\"itemNameCond\":\"0\",\"itemNameValue\":\"\",\"lotIdCond\":\"0\",\"lotIdValue\":\"\",\"mfrNameValue\":\"\",\"dispositionValue\":\"\",\"limit\":500,\"offset\":1}";

			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventListURL = rtsAppUrl + "event/getSearchEventList";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getEventListURL, "Authorization", authToken, bodyText);
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving event list data.");
					return response;
				}

				JSONObject jsonObject = new JSONObject(jsonText);
				ObjectMapper mapper = new ObjectMapper();
			
				if(jsonObject.has("data")){
					JSONObject dataObject = jsonObject.getJSONObject("data");
					JSONArray pageItems = dataObject.getJSONArray("pageItems");
					Object[] events = mapper.readValue(pageItems.toString(), Object[].class);
					for(Object event : events){
						HashMap<String, String> eventMap =  (HashMap<String, String>) event;
						EventsData eventsData = (EventsData) mapper.convertValue(eventMap, EventsData.class);
						DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails();
						String productId = String.valueOf(eventsData.getEventId());
						String packageId = eventsData.getEventType();
						String eventStatus=(eventsData.getEventStatusId().equals("1"))? "0":eventsData.getEventStatusId();
						deviceInfoDetails.setProductId(productId);
						deviceInfoDetails.setPackageId(packageId);
						deviceInfoDetails.setPairStatus(Integer.valueOf(eventStatus));
						shipmentList.add(deviceInfoDetails);
						Location location = new Location(null, packageId, productId);
						location.setLat(eventsData.getLatitude());
						location.setLang(eventsData.getLongitude());
						// do not add locations to the response if lat and lang values are empty
						if(location.getLat() != null &&  location.getLang() != null) {
							map.getLocations().add(location);	
						}
					}
				}
			}
			sensor.setShipmentList(shipmentList);
			//set map to sensor
			sensor.setMap(map);
			response.setSensor(sensor);
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventController :: getEventList " + e.getMessage());
		}

		return response;
	}

	public String getParentItemByEventId(String eventId, int orgId) {
		SensorResponse response = new SensorResponse();
		String traceItemId = null;
		ItemData data=new ItemData();
		try {			
			String bodyText = "{\"eventId\":\""+eventId+"\"}\r\n";

			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventListURL = rtsAppUrl + "event/item/getParentTraceItemByEventId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getEventListURL, "Authorization", authToken, bodyText);
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving Item Data.");
					return response.getResponseStatus();
				}
				JSONObject jsonObject = new JSONObject(jsonText);
				if(jsonObject.has("data")){
					JSONObject dataObject = jsonObject.getJSONObject("data");
					JSONArray pageItems = dataObject.getJSONArray("pageItems");
					traceItemId = pageItems.getJSONObject(0).getString("traceEntId");
					data.setEntId(Long.valueOf(traceItemId));
					data.setProductId(pageItems.getJSONObject(0).getString("itemId"));
					data.setContainerId(pageItems.getJSONObject(0).getString("traceExtId"));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventController :: getParentTraceId " + e.getMessage());
		}
		response.setItemData(data);
		return traceItemId;
	}

	public String getDeviceByTraceEntId(String traceEntId, int orgId) {
		SensorResponse response = new SensorResponse();
		String traceEntExtId = null;
		try {			
			String bodyText = "{\"traceEntId\":\""+traceEntId+"\"}\r\n";

			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventListURL = rtsAppUrl + "event/item/getDeviceByTraceEntId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getEventListURL, "Authorization", authToken, bodyText);
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving traceItem Id data.");
					return response.getResponseStatus();
				}

				JSONObject jsonObject = new JSONObject(jsonText);
				if(jsonObject.has("data")){
					traceEntExtId = jsonObject.getString("data");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventController :: getParentTraceId " + e.getMessage());
		}
		return traceEntExtId;
	}
	
	public String getDeviceByEventId(String eventId, int orgId) {
		SensorResponse response = new SensorResponse();
		String traceEntExtId = null;
		try {			
			String bodyText = "{\"eventId\":\""+eventId+"\"}\r\n";

			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventListURL = rtsAppUrl + "event/item/getDeviceByEventId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getEventListURL, "Authorization", authToken, bodyText);
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving getTraceEntExtId data.");
					return response.getResponseStatus();
				}

				JSONObject jsonObject = new JSONObject(jsonText);
				if(jsonObject.has("data")){
					traceEntExtId = jsonObject.getString("data");
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventController :: getTraceEntExtId " + e.getMessage());
		}
		return traceEntExtId;
	}
	
	public SensorResponse getMapItemLocations(String traceItemId, String eventId, int orgId) {
		logger.info("EventController :: getMapItemLocations(traceItemId=" + traceItemId + ", eventId=" + eventId + ")");
		SensorResponse response = new SensorResponse();
		Sensor sensor = new Sensor();
		try{
			com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
			String value = ConfigCache.getInstance().getSystemConfig("sensor.location.decimal.limit"); // get the precision value from config, if not present setting it to 3
			value = (value == null) ? "3" : value;
			int precision;
			try {
				precision = Integer.valueOf(value);
			} catch (NumberFormatException nfe) {
				precision = 3;
			}
			String bodyText = "{\"traceItemId\":\""+traceItemId+"\"}\r\n";
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getMapItemLocationsURL = rtsAppUrl + "item/getClusterMapItemLocations";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getMapItemLocationsURL, "Authorization", authToken, bodyText);
			
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving item location data.");
					return response;
				}
				
				JSONObject jsonObject = new JSONObject(jsonText);
				ObjectMapper mapper = new ObjectMapper();
				Location location;
				Long dateTime;
				String currentLat;
				String currentLang;
				String prevLat = "";
				String prevLang = "";
				if(jsonObject.has("listData")){					
					JSONArray locationList = jsonObject.getJSONArray("listData");
					Object[] locations = mapper.readValue(locationList.toString(), Object[].class);
					for(Object locationObj : locations){
						HashMap<String, String> locationMap =  (HashMap<String, String>) locationObj;
						ItemMapLocation itemMapLocation = (ItemMapLocation) mapper.convertValue(locationMap, ItemMapLocation.class);
						if (!itemMapLocation.getEventId().equals(eventId)) continue;
						dateTime = Utility.getUTCDate(itemMapLocation.getEventDate());
						currentLat = itemMapLocation.getLat();
						currentLang = itemMapLocation.getLng();
						int pairStatus = itemMapLocation.getPairStatus();
						String address = itemMapLocation.getAddress();
						// Check if we need to add this data point						
						Boolean skip = SendumDAO.getInstance().skipDataPoints(precision, currentLat, currentLang, prevLat, prevLang);
						if (!skip) {
							location = new Location();
							location.setLat(currentLat);
							location.setLang(currentLang);
							location.setDateTime(dateTime);
							location.setPairStatus((pairStatus==1)?0:pairStatus);
							location.setAddress(address);
							// do not add locations to the response if lat and lang values are empty
							if(location.getLat() != null && location.getLang() != null){
								map.getLocations().add(location);	
							}							
						}else{
							location = map.getLocations().get(map.getLocations().size() -1);
							// if we are skipping the data point get the latest information at that lat and lang value							
							location.setLat(currentLat);
							location.setLang(currentLang);
							location.setDateTime(dateTime);
							location.setPairStatus((pairStatus==1)?0:pairStatus);
							location.setAddress(address);							
						}
						prevLang = currentLang;
						prevLat = currentLat;
																	
					}
				}
			}
			
			// If there are datapoints, get the last timestamp from that
			ArrayList<Location> locations = map.getLocations();
			if (locations != null && locations.size() > 0) {
				Long lastTimeStamp = locations.get(locations.size() - 1).getDateTime();
				map.setLastTimeStamp(lastTimeStamp);
			}
			sensor.setMap(map);
			response.setSensor(sensor);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in ItemController :: getMapItemLocations " + e.getMessage());
		}
		return response;
	}
	
	public boolean createDeviceCommission(int orgId, String deviceId, String deviceType)
	{
		logger.info("createDeviceCommission " + orgId + ", " + deviceId + ", " + deviceType);
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			
			String prodNameValue = null;
			if (deviceType.contains("Sendum")) {
				prodNameValue = "Sendum Device";
			} else if (deviceType.contains("Kirsen")) {
				prodNameValue = "Kirsen Device";
			} else if (deviceType.contains("Bosch")) {
				prodNameValue = "Bosch Device";
			}
			
			//Get Product
			
			String productSearchPayload = "{\"requestObject\":{\"prodCodeCondition\":\"0\",\"prodCodeValue\":\"\",\"prodIdCondition\":\"0\",\"prodIdValue\":\"\",\"prodNameCondition\":\"2\",\"prodNameValue\":\"" + prodNameValue + "\",\"gtinCondition\":\"0\",\"gtinValue\":\"\",\"mfrCondition\":\"0\",\"mfrValue\":\"\",\"orgCondition\":\"0\",\"orgValue\":\"\",\"brandCondition\":\"0\",\"brandValue\":\"\",\"sortColum\":\"\",\"orderBy\":\"\",\"limit\":\"75\",\"offset\":0}}";
			
			ProductSearchResponse productSearchResponse = null;
			String presponse = null;
			try {
				presponse = http.doRTSPost(rtsAppUrl+"products/getProductList", "Authorization", authToken, productSearchPayload);	 
			
			} catch (Exception e) {
				logger.error("exception in get product: " + e.getMessage());
				
			}
			productSearchResponse = mapper.readValue(presponse, ProductSearchResponse.class);
			String productCode = productSearchResponse.getData().getPageItems()[0].getGtin();
			String productName = productSearchResponse.getData().getPageItems()[0].getProductName();
			
			
			//Get Site
			
			SiteListResponse siteListResponse = null;
			String response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"common/getChildSites", "Authorization", authToken, "{}");	 
			
			} catch (Exception e) {
				logger.error("exception in getSites: " + e.getMessage());
				
			}
			siteListResponse = mapper.readValue(response, SiteListResponse.class);
			SiteList site = siteListResponse.getListData()[0];
			
	
			//Device Commission
			DeviceAssociateSaveWrapperRequest deviceAssociateSaveWrapperRequest = new DeviceAssociateSaveWrapperRequest();
			DeviceAssociateSaveRequest deviceAssociateSaveRequest = new DeviceAssociateSaveRequest();
			deviceAssociateSaveRequest.setSiteId(site.getSiteId());
			
			
			TraceItem[] childEntityList = new TraceItem[1];
			TraceItem traceItem = new TraceItem();
			traceItem.setProductId(Long.parseLong(productCode));
			traceItem.setProductName(productName);
			traceItem.setQuantity(1);
			traceItem.setProductCode(productCode);
			traceItem.setEntExtId("");
			traceItem.setManufacturerId(0);
			traceItem.setDrugId(productCode);
			traceItem.setItemType("27");
			traceItem.setSerialNumber(deviceId);
			traceItem.setEntityId(0);
			traceItem.setParentEntityId(0);
			traceItem.setQuantityName("1 Unit(s)");
			traceItem.setRootEntId(0);
			traceItem.setDispositionId(13);
			traceItem.setDispositionValue("Allocated");
			traceItem.setMfrLocExtId(site.getSiteId());
			traceItem.setItemTypeValue("Device");
			traceItem.setRootItemType("100");
			traceItem.setLotId("");
			traceItem.setExpiryDate("");
			childEntityList[0] = traceItem;
			deviceAssociateSaveRequest.setChildEntityList(childEntityList);
			deviceAssociateSaveRequest.setParentEntity(new TraceItem());
			deviceAssociateSaveRequest.setEventType("4");
			
			deviceAssociateSaveWrapperRequest.setRequestObject(deviceAssociateSaveRequest);
			String endpointUrl = rtsAppUrl+"scan/saveAggregationCommission";
			
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, mapper.writeValueAsString(deviceAssociateSaveWrapperRequest));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: createDeviceCommission " + e.getMessage());
			return false;
		}
	}
	
	public boolean createDeviceAssociation(int orgId, String rtsItemId, String rtsSerialNumber, String deviceId)
	{
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			
			String[] gtin = rtsItemId.split("-");
			String itemSerialNumber = ConfigCache.getInstance().getSystemConfig("rts.application.sscc") + gtin[0] + "." + rtsSerialNumber;
			
			
			//Get Site
			
			SiteListResponse siteListResponse = null;
			String response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"common/getChildSites", "Authorization", authToken, "{}");	 
			
			} catch (Exception e) {
				logger.error("exception in getSites: " + e.getMessage());
				
			}
			siteListResponse = mapper.readValue(response, SiteListResponse.class);
			SiteList site = siteListResponse.getListData()[0];
			
			//Get Trace Item for Pallet
			
			TraceItemWrapperRequest traceItemWrapperRequest = new TraceItemWrapperRequest();
			TraceItemRequest traceItemRequest = new TraceItemRequest();
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(itemSerialNumber);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			TraceItemResponse traceItemResponse = null;
			
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem parentEntity = traceItemResponse.getListData()[0];
			
			//Get Trace Item for Device
			
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(deviceId);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			traceItemResponse = null;
			response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem[] childEntityList = traceItemResponse.getListData();
			
			//Create Device Association
			DeviceAssociateSaveWrapperRequest deviceAssociateSaveWrapperRequest = new DeviceAssociateSaveWrapperRequest();
			DeviceAssociateSaveRequest deviceAssociateSaveRequest = new DeviceAssociateSaveRequest();
			deviceAssociateSaveRequest.setSiteId(site.getSiteId());
			
			deviceAssociateSaveRequest.setChildEntityList(childEntityList);
			deviceAssociateSaveRequest.setParentEntity(parentEntity);
			deviceAssociateSaveRequest.setEventType("41");
			
			deviceAssociateSaveWrapperRequest.setRequestObject(deviceAssociateSaveRequest);
			String endpointUrl = rtsAppUrl+"scan/saveDeviceAssociation";
			
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, mapper.writeValueAsString(deviceAssociateSaveWrapperRequest));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: createDeviceAssociation " + e.getMessage());
			return false;
		}
	}
	
	
	public boolean performDeviceDisAssociation(int orgId, String deviceId)
	{
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			
			//Get Site
			
			SiteListResponse siteListResponse = null;
			String response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"common/getChildSites", "Authorization", authToken, "{}");	 
			
			} catch (Exception e) {
				logger.error("exception in getSites: " + e.getMessage());
				
			}
			siteListResponse = mapper.readValue(response, SiteListResponse.class);
			SiteList site = siteListResponse.getListData()[0];
			
			
			TraceItemWrapperRequest traceItemWrapperRequest = new TraceItemWrapperRequest();
			TraceItemRequest traceItemRequest = new TraceItemRequest();
			TraceItemResponse traceItemResponse = null;
			
			//Get Trace Item for Device
			
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(deviceId);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			traceItemResponse = null;
			response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem[] childEntityList = traceItemResponse.getListData();
			
			//Perform Device DisAssociation
			DeviceDisassociateSaveWrapperRequest deviceDisassociateSaveWrapperRequest = new DeviceDisassociateSaveWrapperRequest();
			DeviceDisassociateSaveRequest deviceDisassociateSaveRequest = new DeviceDisassociateSaveRequest();
			deviceDisassociateSaveRequest.setSiteId(site.getSiteId());
			
			deviceDisassociateSaveRequest.setIsChildren(false);
			deviceDisassociateSaveRequest.setIsParent(true);
			deviceDisassociateSaveRequest.setEntityList(childEntityList);
			
			
			deviceDisassociateSaveWrapperRequest.setRequestObject(deviceDisassociateSaveRequest);
			String endpointUrl = rtsAppUrl+"scan/saveDisAssociate";
			
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, mapper.writeValueAsString(deviceDisassociateSaveWrapperRequest));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: performDeviceDisAssociation " + e.getMessage());
			return false;
		}
	}

	public boolean createShipEvent(Integer orgId, String productId, String packageId, String deviceId) {
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			
			//Get Ship Location
			
			TraceLocationResponse traceLocationResponse = null;
			String response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"common/getDefaultShipFromAndBillFrom", "Authorization", authToken, "{}");	 
			
			} catch (Exception e) {
				logger.error("exception in getSites: " + e.getMessage());
				
			}
			traceLocationResponse = mapper.readValue(response, TraceLocationResponse.class);
			TraceLocation location = traceLocationResponse.getListData()[0];
			
			
			TraceItemWrapperRequest traceItemWrapperRequest = new TraceItemWrapperRequest();
			TraceItemRequest traceItemRequest = new TraceItemRequest();
			TraceItemResponse traceItemResponse = null;
			
			//Get Trace Item for Pallet
			
			String[] gtin = productId.split("-");
			String itemSerialNumber = ConfigCache.getInstance().getSystemConfig("rts.application.sscc") + gtin[0] + "." + packageId;
			
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(itemSerialNumber);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			traceItemResponse = null;
			response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem[] entityList = traceItemResponse.getListData();
			entityList[0].setExpiryDate("12/31/2020");
			
			//Create Ship Event
			ShipSaveRequest saveShipRequest = new ShipSaveRequest();
			saveShipRequest.setDispositionId("3");
		 	saveShipRequest.setDispositionValue("In Transit");
		 	saveShipRequest.setPageFrom("6");
		 	saveShipRequest.setEventId("");
		 	
		 	saveShipRequest.setBillFrom(location);
			saveShipRequest.setBillTo(location);
			
		 	saveShipRequest.setShipFrom(location);
		 	saveShipRequest.setShipTo(location);
		 	
		 	saveShipRequest.setShippingItemList(entityList);
		 	
		 	
		 	saveShipRequest.setTransNumber(String.valueOf(Math.random()));
		 	saveShipRequest.setTransBizOrgGLN(1234567891011L);
		 	saveShipRequest.setTransBizOrgName("Astellas Norman");
		 	saveShipRequest.setTransType("1");
		 	saveShipRequest.setTransBizVersionId(25533L);
		 	saveShipRequest.setShipDate("6-22-2019");
		 	saveShipRequest.setTransactionDate("6-22-2019");
		 	saveShipRequest.setCarrierPartyId(1234567891011L);
		 	saveShipRequest.setCarrierPartyOrgName("Astellas Norman");
	
			String endpointUrl = rtsAppUrl+"shipReceive/saveShip";
			
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, mapper.writeValueAsString(saveShipRequest));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: createShipEvent " + e.getMessage());
			return false;
		}
		
	}

	public boolean createReceiveEvent(Integer orgId, String productId, String packageId, String deviceId) {
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			
			//Get Ship Location
			
			TraceLocationResponse traceLocationResponse = null;
			String response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"common/getDefaultShipFromAndBillFrom", "Authorization", authToken, "{}");	 
			
			} catch (Exception e) {
				logger.error("exception in getSites: " + e.getMessage());
				
			}
			traceLocationResponse = mapper.readValue(response, TraceLocationResponse.class);
			TraceLocation location = traceLocationResponse.getListData()[0];
			
			
			TraceItemWrapperRequest traceItemWrapperRequest = new TraceItemWrapperRequest();
			TraceItemRequest traceItemRequest = new TraceItemRequest();
			TraceItemResponse traceItemResponse = null;
			
			//Get Trace Item for Pallet
			
			String[] gtin = productId.split("-");
			String itemSerialNumber = ConfigCache.getInstance().getSystemConfig("rts.application.sscc") + gtin[0] + "." + packageId;
			
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(itemSerialNumber);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			traceItemResponse = null;
			response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem[] entityList = traceItemResponse.getListData();
			entityList[0].setExpiryDate("12/31/2020");
			
			//Create Receive Event
			ReceiveSaveRequest saveReceiveRequest = new ReceiveSaveRequest();
			saveReceiveRequest.setDispositionId("6");
			saveReceiveRequest.setDispositionValue("Received");
			saveReceiveRequest.setPageFrom("7");
			saveReceiveRequest.setEventId("");
		 	
			saveReceiveRequest.setBillFrom(location);
			saveReceiveRequest.setBillAt(location);
			
			saveReceiveRequest.setShipFrom(location);
			saveReceiveRequest.setReceiveAt(location);
		 	
			saveReceiveRequest.setShippingItemList(entityList);
		 	
		 	
			saveReceiveRequest.setTransNumber(String.valueOf(Math.random()));
			saveReceiveRequest.setTransBizOrgGLN(1234567891011L);
			saveReceiveRequest.setTransBizOrgName("Astellas Norman");
			saveReceiveRequest.setTransType("1");
			saveReceiveRequest.setTransBizVersionId(25533L);
			saveReceiveRequest.setShipDate("6-22-2019");
			saveReceiveRequest.setTransactionDate("6-22-2019");
			saveReceiveRequest.setCarrierPartyId(1234567891011L);
			saveReceiveRequest.setCarrierPartyOrgName("Astellas Norman");
	
			String endpointUrl = rtsAppUrl+"shipReceive/saveReceive";
			
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, mapper.writeValueAsString(saveReceiveRequest));
			return true;
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: createReceiveEvent " + e.getMessage());
			return false;
		}
		
	}
	
}
