package com.rfxcel.sensor.control;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.kirsen.message.KirsenSensor;
import com.rfxcel.sensor.service.SensorProcessJSON;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.Response;
import com.rfxcel.sensor.vo.SensorResponse;


public class KirsenController implements Runnable
{
	private static final boolean debug = false;
	private static final Logger logger = Logger.getLogger(KirsenController.class);
	public static KirsenController instance = null;
	private ConfigCache configCache = ConfigCache.getInstance();
	private LinkedBlockingQueue<KirsenRequest> queue = new LinkedBlockingQueue<KirsenRequest>();
	private RESTClient http = null;
	private Thread thread = null;
	private String controlURL  = null;
	private String controlUser = null;
	private String controlPass = null;
	private String configURL  = null;
	private String configLogin  = null;
	private String configPassword  = null;
	private String shard  = null;
	
	public static class KirsenRequest
	{
		public String deviceID;
		public String deviceKey;
		public KirsenURLMsg url;
		public KirsenCmdMsg cmd;

		public KirsenRequest(String deviceID, String deviceKey, KirsenURLMsg url, KirsenCmdMsg cmd) {
			this.deviceID = deviceID;
			this.deviceKey = deviceKey;
			this.url = url;
			this.cmd = cmd;
		}

		public String getDeviceID() {
			return deviceID;
		}
		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}
		public String getDeviceKey() {
			return deviceKey;
		}
		public void setDeviceKey(String deviceKey) {
			this.deviceKey = deviceKey;
		}
		public KirsenURLMsg getUrl() {
			return url;
		}
		public void setUrl(KirsenURLMsg url) {
			this.url = url;
		}
		public KirsenCmdMsg getCmd() {
			return cmd;
		}
		public void setCmd(KirsenCmdMsg cmd) {
			this.cmd = cmd;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class KirsenCmdMsg
	{
		@JsonProperty
		public Double temp_min;
		@JsonProperty
		public Double temp_max;
		@JsonProperty
		public String temp_mode;
		@JsonProperty
		public Double tilt_max;
		@JsonProperty
		public String tilt_mode;
		@JsonProperty
		public Double vibration_max;
		@JsonProperty
		public String vibration_mode;
		@JsonProperty
		public Double shock_max;
		@JsonProperty
		public String shock_mode;
		@JsonProperty
		public Integer communication_period;
		@JsonProperty
		public Integer gps_period;
		@JsonProperty
		public Integer gps_timeout;
		@JsonProperty
		public Integer temperature_period;
		@JsonProperty
		public Integer accel_period;
		@JsonProperty
		public String hardware;
		@JsonProperty
		public String firmware;
		@JsonProperty
		public String command;
		@JsonProperty
		public String next_comm_time;
		@JsonProperty
		public Boolean demo_mode;
		@JsonProperty
		public Boolean forced_mode;
		@JsonProperty
		public Boolean train_mode;

		public KirsenCmdMsg() {
		}
		
		public Double getTemp_min() {
			return temp_min;
		}
		public void setTemp_min(Double temp_min) {
			this.temp_min = temp_min;
		}
		public Double getTemp_max() {
			return temp_max;
		}
		public void setTemp_max(Double temp_max) {
			this.temp_max = temp_max;
		}
		public String getTemp_mode() {
			return temp_mode;
		}
		public void setTemp_mode(String temp_mode) {
			this.temp_mode = temp_mode;
		}
		public Double getTilt_max() {
			return tilt_max;
		}
		public void setTilt_max(Double tilt_max) {
			this.tilt_max = tilt_max;
		}
		public String getTilt_mode() {
			return tilt_mode;
		}
		public void setTilt_mode(String tilt_mode) {
			this.tilt_mode = tilt_mode;
		}
		public Double getVibration_max() {
			return vibration_max;
		}
		public void setVibration_max(Double vibration_max) {
			this.vibration_max = vibration_max;
		}
		public String getVibration_mode() {
			return vibration_mode;
		}
		public void setVibration_mode(String vibration_mode) {
			this.vibration_mode = vibration_mode;
		}
		public Double getShock_max() {
			return shock_max;
		}
		public void setShock_max(Double shock_max) {
			this.shock_max = shock_max;
		}
		public String getShock_mode() {
			return shock_mode;
		}
		public void setShock_mode(String shock_mode) {
			this.shock_mode = shock_mode;
		}
		public Integer getCommunication_period() {
			return communication_period;
		}
		public void setCommunication_period(Integer communication_period) {
			this.communication_period = communication_period;
		}
		public Integer getGps_period() {
			return gps_period;
		}
		public void setGps_period(Integer gps_period) {
			this.gps_period = gps_period;
		}
		public Integer getGps_timeout() {
			return gps_timeout;
		}
		public void setGps_timeout(Integer gps_timeout) {
			this.gps_timeout = gps_timeout;
		}
		public Integer getTemperature_period() {
			return temperature_period;
		}
		public void setTemperature_period(Integer temperature_period) {
			this.temperature_period = temperature_period;
		}
		public Integer getAccel_period() {
			return accel_period;
		}
		public void setAccel_period(Integer accel_period) {
			this.accel_period = accel_period;
		}
		public String getHardware() {
			return hardware;
		}
		public void setHardware(String hardware) {
			this.hardware = hardware;
		}
		public String getFirmware() {
			return firmware;
		}
		public void setFirmware(String firmware) {
			this.firmware = firmware;
		}
		public String getCommand() {
			return command;
		}
		public void setCommand(String command) {
			this.command = command;
		}
		public String getNext_comm_time() {
			return next_comm_time;
		}
		public void setNext_comm_time(String next_comm_time) {
			this.next_comm_time = next_comm_time;
		}
		public Boolean getDemo_mode() {
			return demo_mode;
		}
		public void setDemo_mode(Boolean demo_mode) {
			this.demo_mode = demo_mode;
		}
		public Boolean getForced_mode() {
			return forced_mode;
		}
		public void setForced_mode(Boolean forced_mode) {
			this.forced_mode = forced_mode;
		}
		public Boolean getTrain_mode() {
			return train_mode;
		}
		public void setTrain_mode(Boolean train_mode) {
			this.train_mode = train_mode;
		}

		public static KirsenCmdMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				KirsenCmdMsg cmd = mapper.readValue(text, KirsenCmdMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class KirsenURLMsg
	{
		@JsonProperty
		public String url;
		@JsonProperty
		public String login;
		@JsonProperty
		public String password;
		@JsonProperty
		public String description;
		@JsonProperty
		public Integer shard;
		@JsonProperty
		public Boolean active;
		
		@JsonProperty
		public Boolean cell_location;
		
		public KirsenURLMsg() {
		}

		public String getUrl() {
			return url;
		}
		public void setUrl(String url) {
			this.url = url;
		}
		public String getLogin() {
			return login;
		}
		public void setLogin(String login) {
			this.login = login;
		}
		public String getPassword() {
			return password;
		}
		public void setPassword(String password) {
			this.password = password;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Integer getShard() {
			return shard;
		}
		public void setShard(Integer shard) {
			this.shard = shard;
		}

		public Boolean getActive() {
			return active;
		}
		public void setActive(Boolean active) {
			this.active = active;
		}
		public Boolean getCell_location() {
			return cell_location;
		}

		public void setCell_location(Boolean cell_location) {
			this.cell_location = cell_location;
		}

		public static KirsenURLMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				KirsenURLMsg cmd = mapper.readValue(text, KirsenURLMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	public static void main(String[] args) {
		try {
			KirsenController c = KirsenController.getInstance();
			Device d = new Device();
			d.setDeviceId("358502061655695");
			d.setDeviceKey("1830");
			Profile p = new Profile();
			p.setAttbFrequency(180);
			p.setCommFrequency(60);
			p.setGpsFrequency(240);
			Attribute a = new Attribute();
			a.setName("temperature");
			a.setMaxValue(10);
			a.setMaxValue(50);
			a.setRangeMin(0d);
			a.setRangeMax(100d);
			List<Attribute> attrType = new ArrayList<Attribute>();
			attrType.add(a);
			p.setAttrType(attrType );
			c.setupProfile(0, 0, d, p);
		} catch (Exception e) {
		}
	}
	
	public static KirsenController getInstance()
	{
		if (instance == null) {
			instance = new KirsenController();
		}
		return(instance);
	}

	/*
	private static String reformatConfigTemplate(String text)
	{
		text = text.replaceAll("@", "\"");
		return(text);
	}

	public String getKirsenConfiguration(String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq)
	{
		String text = new String(configTemplate);
		text = text.replace("<profile.gps.freq>", new Integer(gpsFreq).toString());
		text = text.replace("<profile.attr.freq>",  new Integer(tempFreq).toString());
		text = text.replace("<profile.comm.freq>",  new Integer(commFreq).toString());
		text = text.replace("<deviceid>",  deviceID);
		text = text.replace("<devicekey>",  deviceKey);
		logger.info("KirsenController - device " + deviceID + " config:\n" + text);
		return(text);
	}
	*/
	
	public KirsenController()
	{
		shard = ConfigCache.getInstance().getSystemConfig("application.shard");
		controlURL  = configCache.getSystemConfig("kirsen.controller.url");
		controlUser = configCache.getSystemConfig("kirsen.controller.userid");
		controlPass = configCache.getSystemConfig("kirsen.controller.password");
		configURL   = configCache.getSystemConfig("kirsen.config.url");
		configLogin = configCache.getSystemConfig("kirsen.user");
		configPassword = configCache.getSystemConfig("kirsen.password");
		if (shard == null) {
			shard = "0";
		}
		if (controlURL == null) {
			logger.error("KirsenController - system config invalid value for kirsen.controller.url");
			controlURL = "https://control.kirsenglobalsecurity.com/api/v1/<verb>/<deviceKey>/";
			controlUser = "Rfxcel";
			controlPass = "da2head";
		}
	
		if (configURL == null) {
			logger.error("KirsenController - system config invalid value for kirsen.config.url");
			configURL = "https://www.verizonitt.net/kirsen";
		}
		if (configLogin == null) {
			logger.error("KirsenController - system config invalid value for kirsen.user");
			//configLogin = "kirsen";
		}
		if (configPassword == null) {
			logger.error("KirsenController - system config invalid value for kirsen.password");
			//configPassword = "kirsen123";
		}
		http = new RESTClient(controlUser, controlPass);
		logger.info("KirsenController - initialized with account = " + controlUser + " for URL " + controlURL);
	}
	
	
	
	public KirsenURLMsg getKirsenURLMsg(int orgID, long userId, Device d, Profile p)
	{
		String kirsenLogin = configLogin;
		String kirsenPassword = configPassword;
		if ((kirsenLogin == null) || (kirsenPassword == null)) {
			kirsenLogin = configCache.getOrgConfig("kirsen.user", orgID);
			kirsenPassword = configCache.getOrgConfig("kirsen.password", orgID);
		}
		KirsenURLMsg c = new KirsenURLMsg();
		c.setUrl(configURL);
		c.setLogin(kirsenLogin);
		c.setPassword(kirsenPassword);
		c.setDescription("URL Autoconfigure " + new Date().toString());
		c.setShard(Integer.parseInt(shard));
		c.setActive(true);
		
		if(configCache.getOrgConfig("kirsen.enablecell", orgID)!= null){
			String cellLocationStr = configCache.getOrgConfig("kirsen.enablecell", orgID);
			Boolean cellLocation = Boolean.valueOf(cellLocationStr);
			if (cellLocation == null) {
				cellLocation = false;
			}
			c.setCell_location(cellLocation);
		}
		return(c);
	}
	
	public KirsenCmdMsg getKirsenCmdMsg(int orgID, long userId, Device d, Profile p)
	{
		KirsenCmdMsg c = new KirsenCmdMsg();
		String command = ConfigCache.getInstance().getOrgConfig("kirsen.config.command", orgID);
		if (command == null) {
			c.setCommand("none");
		}
		else {
			c.setCommand(command);
		}		
		String demoMode = ConfigCache.getInstance().getOrgConfig("kirsen.config.demomode", orgID);
		if (demoMode == null) {
			c.setDemo_mode(false);
		}
		else {
			c.setDemo_mode(new Boolean(demoMode));
		}		
		c.setForced_mode(false);
		c.setTrain_mode(false);
		c.setCommunication_period(p.getCommFrequency());
		c.setTemperature_period(p.getAttbFrequency());
		c.setAccel_period(p.getAttbFrequency());
		c.setGps_period(p.getGpsFrequency());
		String gpsPeriod = ConfigCache.getInstance().getOrgConfig("kirsen.config.gpsperiod", orgID);
		if (gpsPeriod == null) {
			c.setGps_timeout(5);
		}
		else {
			c.setGps_timeout(new Integer(gpsPeriod));
		}
		for (Attribute a: p.getAttrType()) {
			String attrName = a.getName();
			double min = a.getMinValue();
			double max = a.getMaxValue();
			double rangeMin = a.getRangeMin();
			double rangeMax = a.getRangeMax();
			if (!Double.isNaN(min) && !Double.isNaN(max) && (min >= max)) {
				min = Double.NaN;
				max = Double.NaN;
			}
			if (!Double.isNaN(min) && !Double.isNaN(rangeMin) && (min <= rangeMin)) {
				min = Double.NaN;
			}
			if (!Double.isNaN(max) && !Double.isNaN(rangeMax) && (max >= rangeMax)) {
				max = Double.NaN;
			}
			if (attrName.toLowerCase().startsWith("temperature")) {
				if (Double.isNaN(min) && Double.isNaN(max)) {
					c.setTemp_mode("record");
					c.setTemp_min(rangeMin);
					c.setTemp_max(rangeMax);
				}
				else {
					c.setTemp_mode("transfer");
					if (!Double.isNaN(min)) {
						double minC = Utility.round(Utility.mapF2C(min,orgID), 1);
						c.setTemp_min(minC);
					}
					if (!Double.isNaN(max)) {
						double maxC = Utility.round(Utility.mapF2C(max,orgID), 1);
						c.setTemp_max(maxC);
					}
				}
			}
			else if (attrName.toLowerCase().startsWith("tilt")) {
				if (Double.isNaN(max)) {
					c.setTilt_mode("record");
					c.setTilt_max(rangeMax);
				}
				else {
					c.setTilt_mode("transfer");
					if (!Double.isNaN(max)) {
						c.setTilt_max(max);
					}
				}
			}
			else if (attrName.toLowerCase().startsWith("shock")) {
				if (Double.isNaN(max)) {
					c.setShock_mode("record");
					c.setShock_max(rangeMax);
				}
				else {
					c.setShock_mode("transfer");
					if (!Double.isNaN(max)) {
						c.setShock_max(max);
					}
				}
			}
			else if (attrName.toLowerCase().startsWith("vibration")) {
				if (Double.isNaN(max)) {
					c.setVibration_mode("record");
					c.setVibration_max(rangeMax);
				}
				else {
					c.setVibration_mode("transfer");
					if (!Double.isNaN(max)) {
						c.setVibration_max(max);
					}
				}
			}
			else {
				logger.info("KirsenController - skipping attribute " + attrName);
			}
		}
		return(c);
	}

	public void setupProfile(int orgID, long userId, Device d, Profile p)
	{
		if (!debug){
			synchronized(this) {
				if (thread == null) {
					logger.info("SMSService - starting new thread....");
					thread = new Thread(this);
					thread.start();
				}
			}
		}
		KirsenURLMsg urlMsg = getKirsenURLMsg(orgID, userId, d, p);
		KirsenCmdMsg cmdMsg = getKirsenCmdMsg(orgID, userId, d, p);
		KirsenRequest req = new KirsenRequest(d.getDeviceId(), d.getDeviceKey(), urlMsg, cmdMsg);
		if (!debug) {
			this.queue.add(req);
			logger.info("KirsenController - queuing request " + req.toString());
		}
		else {
			doSetupDevice(d.getDeviceId(), d.getDeviceKey(), urlMsg, cmdMsg);
		}
	}
	
	private void doSetupDevice(String deviceID, String deviceKey, KirsenURLMsg urlMsg, KirsenCmdMsg cmdMsg)
	{
		boolean urlOK  = changeURL(deviceID, deviceKey, urlMsg);
		boolean freqOK = changeSettings(deviceID, deviceKey, cmdMsg);
		Integer orgId = DeviceCache.getInstance().getDeviceOrgId(deviceID);
		if (urlOK && freqOK) {
			DeviceLogDAO.doAddDeviceLog(deviceID, 1L, 
					"Profile settings applied successfully: deviceID=" + deviceID + 
					",DeviceKey="+ deviceKey +
					"commFreq="+cmdMsg.getCommunication_period() + 
					"gpsFreq="+cmdMsg.getGps_period() + 
					"tempFreq=" +cmdMsg.getTemperature_period(), 
					orgId);	
		}
		else {
			DeviceLogDAO.doAddDeviceLog(deviceID, 1L, 
					"Failed to apply profile settings to device(" + deviceID+ ")", 
					orgId);
		}
	}
	
	public boolean changeURL(String deviceID, String deviceKey, KirsenURLMsg urlMsg)
	{
		String url = controlURL.replace("<verb>", "endpoints");
		url = url.replace("<deviceKey>", deviceKey);
		
		String text = http.doGet(url);
		KirsenURLMsg preSetting = KirsenURLMsg.fromJson(text);
		logger.info("KirsenController.changeURL - doSetupDevice preSetting " + text);

		String content = urlMsg.toJson();
		http.doPut(url, content);
		logger.info("KirsenController.changeURL - doSetupDevice PUT " + url + " content " + content);
		
		String text2 = http.doGet(url);
		KirsenURLMsg postSetting = KirsenURLMsg.fromJson(text2);
		logger.info("KirsenController.changeURL - doSetupDevice postSetting " + text2);
		if ((postSetting.getUrl() != null) && (postSetting.getUrl().equals(configURL))) {
			return(true);
		}
		logger.error("KirsenController.changeURL - postSetting.URL=" + postSetting.getUrl() + " <> config.URL="+configURL);
		return(false);
	}
	
	public boolean changeSettings(String deviceID, String deviceKey, KirsenCmdMsg cmdMsg)
	{
		String url = controlURL.replace("<verb>", "options");
		url = url.replace("<deviceKey>", deviceKey);
		
		String text = http.doGet(url);
		KirsenCmdMsg preSetting = KirsenCmdMsg.fromJson(text);
		logger.info("KirsenController.changeSettings - doSetupDevice preSetting " + text);

		String content = cmdMsg.toJson();
		http.doPut(url, content);
		logger.info("KirsenController.changeSettings - doSetupDevice PUT " + url + " content " + content);
		
		String text2 = http.doGet(url);
		KirsenCmdMsg postSetting = KirsenCmdMsg.fromJson(text2);
		logger.info("KirsenController.changeSettings - doSetupDevice postSetting " + text2);
		if ((postSetting.getGps_period().equals(cmdMsg.getGps_period())) &&
				(postSetting.getTemperature_period().equals(cmdMsg.getTemperature_period())) &&
				(postSetting.getCommunication_period().equals(cmdMsg.getCommunication_period()))) {
			return(true);
		}
		logger.error("KirsenController.changeURL - postSetting.values=(" + 
				postSetting.getGps_period() + "," +
				postSetting.getTemperature_period() + "," +
				postSetting.getCommunication_period() + ")" +
				" <> config.values=("+ 
				cmdMsg.getGps_period() + "," + 
				cmdMsg.getTemperature_period() + "," + 
				cmdMsg.getCommunication_period() + ")");
		return(false);
	}
	

	@Override
	public void run() {
		try
		{
			KirsenRequest req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("KirsenCmdMsg - dequeuing request " + req.toString());
				if ((req.getDeviceKey() != null) && (req.getDeviceKey().equalsIgnoreCase(SensorConstant.DEVICE_KEY_DEMO))) {
					logger.info("KirsenCmdMsg - skipping demo device " + req.getDeviceID());
					continue;
				}
				this.doSetupDevice(req.getDeviceID(), 
						req.getDeviceKey(), 
						req.getUrl(),
						req.getCmd());
			}
		}
		catch(Exception e)
		{
			logger.info("KirsenCmdMsg thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}

	
	/**
	 * @param userId 
	 * @param device
	 * @return 
	 */
	public Response locateDevice(long userId, Device device) {
		com.rfxcel.sensor.vo.Response resp = new com.rfxcel.sensor.vo.Response();
		String url = controlURL.replace("<verb>", "locations");
		url = url.replace("<deviceKey>", device.getDeviceKey() == null ? "":device.getDeviceKey());
		String json = http.doGet(url);
		if(json != null){
			logger.info("KirsenController - locateDevice GET " + url + " content " + json);
			//Convert json string to map
			Map<String, String> kirsenDataMap = Utility.convertJsonToMap(json);
			if(kirsenDataMap.containsKey("error")){
				resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("The device you are trying to locate could not be found , Please try again later!");
			}else{
				//create instance of KirsenSensor from map data
				KirsenSensor kirsenSensor = new KirsenSensor(kirsenDataMap);
				String kirsenData = kirsenSensor.toJson();
				SensorProcessJSON.getInstance().doQueue(kirsenData);
				
				resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Locate Now command sent successfully to the device");
			}
		}else{
			logger.info("KirsenController - locateDevice GET " + url + " No response received ");
			resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No response received for locate Device Request");
		}
		return resp;
	}
	
	
	/**
	 * @return
	 */
	public SensorResponse getDeviceKeys() {
		SensorResponse resp = new SensorResponse();
		HashMap<Object, Object> deviceKey = new HashMap<Object, Object>();
		try {
			String url = controlURL.replace("<verb>", "devices");
			url = url.replace("<deviceKey>",  "");
			if(url.charAt(url.length()-1) =='/'){
				url = url.substring(0, url.length()-2);
			}
			String json = http.doGet(url);
			if(json != null && json.trim().length() != 0){
				logger.info("KirsenController - getDeviceKeys GET " + url + " content " + json);
				if(json.contains("error")){
					resp.setResponseCode(333);
					resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Error while retriving device keys for Kirsen device(s)." );
					return resp;
				}
				ObjectMapper mapper = new ObjectMapper();
				Object[] devices = mapper.readValue(json, Object[].class);
				for(Object device : devices){
					HashMap<String, Object> deviceDetails =(HashMap<String, Object>) device;
					deviceKey.put(deviceDetails.get("imei"), deviceDetails.get("id"));
				}
				resp.setDeviceKeyMap(deviceKey);
				resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Get device key command sent successfully to the device(s)");
			}else{
				logger.info("KirsenController - getDeviceKeys GET " + url + " No response received ");
				resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No response received for getDeviceKey Request");
			}
			return resp;
		}catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in Kirsencontroller :: getDeviceKeys "+ e.getMessage());
		}
		return resp;
	}
	
}
