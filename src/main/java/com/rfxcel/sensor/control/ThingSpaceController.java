package com.rfxcel.sensor.control;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.thingspace.util.ThingSpaceConstants;
import com.rfxcel.sensor.thingspace.util.ThingSpaceUtils;
import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.util.SensorConstant;


public class ThingSpaceController implements Runnable
{
	private static final Logger logger = Logger.getLogger(ThingSpaceController.class);
	public static ThingSpaceController instance = null;
	private ConfigCache configCache = ConfigCache.getInstance();
	private LinkedBlockingQueue<ThingSpaceRequest> queue = new LinkedBlockingQueue<ThingSpaceRequest>();
	private RESTClient http = null;
	private Thread thread = null;
	private String controlURL  = null;
	private String configURL  = null;
	
	private static final int ALARMTYPE_DISABLED = 0;
	private static final int ALARMTYPE_VALUECHANGED = 1;
	private static final int ALARMTYPE_EXCEEDTHRESHOLD = 2;
	private static final int ALARMTYPE_BELOWTHRESHOLD = 3;
	private static final int ALARMTYPE_OUTOFRANGE = 4;
	
	private static HashMap<String, String> attributeToAlarmType = new HashMap<String, String>()
	{{
	     put("battery", "battAlarm");
	     put("temperature", "tempAlarm");
	     put("humidity", "humiAlarm");
	     put("pressure", "presAlarm");
	     put("light", "liteAlarm");
	     put("accelerometer", "acclAlarm");
	     put("rssi", "rssiAlarm");
	}};
	
	
	public static void main(String[] args)
	{
		ThingSpaceCmdAlarm alarm = new ThingSpaceCmdAlarm(4, 20, 100);
		System.out.println(alarm.toAlarmJson("temperature"));
	}
	
	public static class ThingSpaceRequest
	{
		public int orgID;
		public String deviceID;
		public String deviceKey;
		public ThingSpaceCmdMsg cmd;
		public ArrayList<String> attributes; 
		public Profile profile;
		
		public ThingSpaceRequest(int orgID, String deviceID, String deviceKey, int commFreq, int gpsFreq, int attrFreq, ArrayList<String> attributes) {
			this.orgID = orgID;
			this.deviceID = deviceID;
			this.deviceKey = deviceKey;
			this.cmd = new ThingSpaceCmdMsg(commFreq, gpsFreq, attrFreq);
			this.attributes = attributes;
		}

		public int getOrgID() {
			return orgID;
		}
		public void setOrgID(int orgID) {
			this.orgID = orgID;
		}
		public String getDeviceID() {
			return deviceID;
		}
		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}
		public String getDeviceKey() {
			return deviceKey;
		}
		public void setDeviceKey(String deviceKey) {
			this.deviceKey = deviceKey;
		}
		public ThingSpaceCmdMsg getCmd() {
			return cmd;
		}
		public void setCmd(ThingSpaceCmdMsg cmd) {
			this.cmd = cmd;
		}
		public ArrayList<String> getAttributes() {
			return attributes;
		}
		public void setAttributes(ArrayList<String> attributes) {
			this.attributes = attributes;
		}

		public Profile getProfile() {
			return profile;
		}
		public void setProfile(Profile profile) {
			this.profile = profile;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class ThingSpaceCmdMsg
	{
		public int gpsFrequency;
		public int attrFrequency;
		public int commFrequency;
		
		public ThingSpaceCmdMsg() {
		}

		public ThingSpaceCmdMsg(int commFreq, int gpsFreq, int attrFreq) {
		this.gpsFrequency =  gpsFreq;
		this.commFrequency = commFreq;
		this.attrFrequency = attrFreq;
		}
		public int getGpsFrequency() {
			return gpsFrequency;
		}

		public void setGpsFrequency(int gpsFrequency) {
			this.gpsFrequency = gpsFrequency;
		}

		public int getAttrFrequency() {
			return attrFrequency;
		}

		public void setAttrFrequency(int attrFrequency) {
			this.attrFrequency = attrFrequency;
		}

		public int getCommFrequency() {
			return commFrequency;
		}

		public void setCommFrequency(int commFrequency) {
			this.commFrequency = commFrequency;
		}

		public String toJson(int monitorPeriod, int reportPeriod) {
			StringBuilder sb = new StringBuilder("{");
			sb.append("\"deviceConfig\"");
			sb.append(":{\"<<attribute>>\":{");
			sb.append("\"monitorPeriod\":"+ monitorPeriod+",");
			sb.append("\"opMode\":1,");
			sb.append("\"reportOffset\":0,");
			sb.append("\"reportPeriod\":"+reportPeriod+",");
			sb.append("\"reportType\":2}}");
			sb.append("}");
			return sb.toString();
		}
		
		public String toAttributeJson(int monitorPeriod, int reportPeriod) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"<<attribute>>\":{");
			sb.append("\"monitorPeriod\":"+ monitorPeriod+",");
			sb.append("\"opMode\":1,");
			sb.append("\"reportOffset\":0,");
			sb.append("\"reportPeriod\":"+reportPeriod+",");
			sb.append("\"reportType\":2");
			sb.append("},");
			return sb.toString();
		}
		
		public String toAttributeJson() {
			StringBuilder sb = new StringBuilder();
			sb.append("\"<<attribute>>\":{");
			sb.append("\"monitorPeriod\":0,");
			sb.append("\"opMode\":0,");
			sb.append("\"reportOffset\":0,");
			sb.append("\"reportPeriod\":0,");
			sb.append("\"reportType\":0");
			sb.append("},");
			return sb.toString();
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class ThingSpaceCmdAlarm
	{
		public int alarmType;
		public double threshold;
		public double thresholdRange;
		public ThingSpaceCmdAlarm() {
		}
		public ThingSpaceCmdAlarm(int alarmType, double threshold, double thresholdRange) {
			super();
			this.alarmType = alarmType;
			this.threshold = threshold;
			this.thresholdRange = thresholdRange;
		}
		public int getAlarmType() {
			return alarmType;
		}
		public void setAlarmType(int alarmType) {
			this.alarmType = alarmType;
		}
		public double getThreshold() {
			return threshold;
		}
		public void setThreshold(double threshold) {
			this.threshold = threshold;
		}
		public double getThresholdRange() {
			return thresholdRange;
		}
		public void setThresholdRange(double thresholdRange) {
			this.thresholdRange = thresholdRange;
		}
		public String toAlarmJson(String attribute) {
			StringBuilder sb = new StringBuilder();
			sb.append("\"<<alarmName>>\":{");
			sb.append("\"alarmType\":"+ getAlarmType() +",");
			sb.append("\"threshold\":" + getThreshold() + ",");
			sb.append("\"thresholdRange\":" + getThresholdRange() +"");
			sb.append("},");
			String alarmName = attributeToAlarmType.get(attribute);
			if (alarmName == null) {
				logger.error("ThingSpaceCmdAlarm - unable to map attribute " + attribute + " for " + sb.toString());
				return(null);
			}
			return sb.toString().replaceAll("<<alarmName>>", alarmName);
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class ThingSpaceURLMsg
	{
		public String address;
		public String addressscheme;
		public String kind;
		public String version;
		
		public ThingSpaceURLMsg() {
		}

		public ThingSpaceURLMsg(String address, String addressscheme,
				String kind, String version) {
			super();
			this.address = address;
			this.addressscheme = addressscheme;
			this.kind = kind;
			this.version = version;
		}

		public String getAddress() {
			return address;
		}

		public void setAddress(String address) {
			this.address = address;
		}

		public String getAddressscheme() {
			return addressscheme;
		}

		public void setAddressscheme(String addressscheme) {
			this.addressscheme = addressscheme;
		}

		public String getKind() {
			return kind;
		}

		public void setKind(String kind) {
			this.kind = kind;
		}

		public String getVersion() {
			return version;
		}

		public void setVersion(String version) {
			this.version = version;
		}

		public static ThingSpaceURLMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				ThingSpaceURLMsg cmd = mapper.readValue(text, ThingSpaceURLMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	public static ThingSpaceController getInstance(){
		if (instance == null) {
			instance = new ThingSpaceController();
		}
		return(instance);
	}

	public ThingSpaceController()
	{
		controlURL  = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		configURL   = configCache.getSystemConfig("kirsen.config.url").replace("/kirsen","/thingspace");
		if (controlURL == null) {
			logger.error("ThingSpaceController - system config invalid value for thingspace.integration.url");
			controlURL = "https://core.thingspace.verizon.com/api/v2/devices";
		}
		if (configURL == null) {
			logger.error("ThingSpaceController - system config invalid value for kirsen.config.url");
			configURL = "https://ittdemo00.track-n-trace.net/thingspace";
		}
		http = new RESTClient(null, null);
		logger.info("ThingSpaceController - initialized with URL " + controlURL);
	}
	
	
	public static double getTemperaureInCelcius(int orgId, double tempInFahrenheit)
	{
		double tempInCelcius;
		String format = ConfigCache.getInstance().getOrgConfig("sensor.temperature.format", orgId);
		if (SensorConstant.TEMPERATURE_FORMAT_CELSIUS.equalsIgnoreCase(format)){
			tempInCelcius = tempInFahrenheit;
		}else{
			tempInCelcius = ((tempInFahrenheit - 32d) * 5d) / 9d;
		}
		return(tempInCelcius);
	}
	
	
	public void setupDevice(int orgID, String deviceID, String deviceKey, Integer commFreq, Integer gpsFreq, Integer attrFreq, ArrayList<String> attributes, Profile profile)
	{
		synchronized(this) {
			if (thread == null) {
				logger.info("SMSService - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		ThingSpaceRequest req = new ThingSpaceRequest(orgID, deviceID, deviceKey,commFreq, gpsFreq, attrFreq, attributes);
		req.setProfile(profile);
		this.queue.add(req);
		logger.info("ThingSpaceController - queuing request " + req.toString());
	}
	
		
	private void doSetupDevice(int orgId, String deviceId, String deviceKey, ThingSpaceCmdMsg cmd, ArrayList<String> attributes, Profile profile)
	{
		 //changeSettings(orgId, deviceId, deviceKey, cmd, attributes);
		changeUpdatedSettings(orgId, deviceId, deviceKey, cmd, attributes, profile);
	}
	
	private static void sleep(long millis)
	{
		try { Thread.sleep(millis); } catch(Exception e) {}
	}

	
	private void changeSettings(int orgId, String deviceId, String deviceKey, ThingSpaceCmdMsg cmd,ArrayList<String> attributes) {
		String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
		if (authToken == null) {
			authToken = "NDY3OTgyY2YtOTdjNi00MTdhLWE0MGItMDE2ZThlZmJmNzY3";
		}
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		String cmdURL = url + "/"+ deviceKey + "/actions/set";
		String preCommand = "{\"deviceConfig\":{\"device\":{\"opMode\":2,\"ledMode\":1,\"airplaneMode\":0,\"commSynchPeriod\":0,\"bufferMode\":0,\"checkFota\":1}}}";
		logger.info("ThingSpaceController.changeSettings - Command to bring the device up to normal mode "+ preCommand);
		http.doPost(cmdURL, "Authorization", authToken, preCommand);
		sleep(200L);
		
		String command ;	
		attributes.add("location");// since location is not added to profile_thresholds as an attribute
		for(String attribute : attributes){
			// for shock we need to pass accelerometer as the attribute name in command
			if(ThingSpaceConstants.ATTRIBUTE_SHOCK.equals(attribute)) {
				attribute = "accelerometer";
			} else if(ThingSpaceConstants.ATTRIBUTE_TILT.equals(attribute)) {
				attribute = "gyro";
			} else if(ThingSpaceConstants.ATTRIBUTE_VIBRATION.equals(attribute)) {
				continue;
			}
			if(attribute.equalsIgnoreCase("location")){
				command = cmd.toJson(cmd.getAttrFrequency(), cmd.getGpsFrequency());
			}else{
				command = cmd.toJson(cmd.getAttrFrequency(), cmd.getCommFrequency());
			}
			command = command.replace("<<attribute>>", attribute);
			logger.info("ThingSpaceController.changeSettings - doSetupDevice POST URL " + cmdURL);
			logger.info("ThingSpaceController.changeSettings - doSetupDevice POST content " + command);
			http.doPost(cmdURL, "Authorization", authToken, command);
			sleep(200L);
		}
	}
	
	private static void addAttribute(ArrayList<String> attributes, String newAttribute)
	{
		for (String attribute : attributes)
		{
			if (attribute.trim().equals(newAttribute.trim())) return;
		}
		attributes.add(newAttribute.trim());
	}
	
	private void changeUpdatedSettings(int orgId, String deviceId, String deviceKey, ThingSpaceCmdMsg cmd,ArrayList<String> attributes, Profile profile) {
		String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
		if (authToken == null) {
			authToken = "MjJmNzRjZGUtMmVjZS00ZWIyLWJiZWUtNzZhYjhiZjYyMzQz";
		}
		
		// if �Send GPS Location Every� and �Send Sensor Data Every� are both > 5 minutes, then 
		// set commSynchPeriod to the lower of the 2 parameters (in seconds)
		// else set commSynchPeriod = 0
		int commSynchPeriod = 0;
		if (cmd.getGpsFrequency() >= 300 && cmd.getCommFrequency() >= 300) {
			commSynchPeriod = Math.min(cmd.getGpsFrequency(), cmd.getCommFrequency());
		}
		
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		String cmdURL = url + "/"+ deviceKey + "/actions/set";
		String preCommand = "{\"deviceConfig\":{\"device\":{\"opMode\":2,\"ledMode\":1,\"airplaneMode\":0,\"commSynchPeriod\":" + commSynchPeriod + ",\"bufferMode\":0}}}";
		logger.info("ThingSpaceController.changeSettings - Command to bring the device up to normal mode "+ preCommand);
		ThingSpaceUtils.logMessage("ThingSpaceController.changeSettings - Command to bring the device up to normal mode request = "+ preCommand);
		String response = http.doPost(cmdURL, "Authorization", authToken, preCommand);
		ThingSpaceUtils.logMessage("ThingSpaceController.changeSettings - Command to bring the device up to normal mode - response = " + response);
		sleep(200L);
		
		addAttribute(attributes, "location");// since location is not added to profile_thresholds as an attribute
		addAttribute(attributes, "rfSignal");
		addAttribute(attributes, ThingSpaceConstants.ATTRIBUTE_SHOCK);
		addAttribute(attributes, ThingSpaceConstants.ATTRIBUTE_TILT);
		String attributeCommand = null;
		String uuid = UUID.randomUUID().toString();
		StringBuilder command = new StringBuilder("{");
		command.append("\"deviceConfig\": {");
		for(String attribute : attributes){
			// for shock we need to pass accelerometer as the attribute name in command
			if(ThingSpaceConstants.ATTRIBUTE_SHOCK.equals(attribute)) {
				attribute = "accelerometer";
			} else if(ThingSpaceConstants.ATTRIBUTE_TILT.equals(attribute)) {
				attribute = "gyro";
			} else if(ThingSpaceConstants.ATTRIBUTE_VIBRATION.equals(attribute)) {
				continue;
			}
			if(attribute.equalsIgnoreCase("location") || attribute.equalsIgnoreCase("rfSignal")){
				attributeCommand = cmd.toAttributeJson(cmd.getGpsFrequency(), cmd.getGpsFrequency());
			}else if(attribute.equalsIgnoreCase("accelerometer") || attribute.equalsIgnoreCase("gyro")){
				attributeCommand = cmd.toAttributeJson();
			}else{
				attributeCommand = cmd.toAttributeJson(cmd.getAttrFrequency(), cmd.getCommFrequency());
			}
			attributeCommand = attributeCommand.replace("<<attribute>>", attribute);
			command.append(attributeCommand);
		}
		for (Attribute a: profile.getAttrType()) {
			String attrName = a.getName();
			double min = a.getMinValue();
			double max = a.getMaxValue();
			double rangeMin = a.getRangeMin();
			double rangeMax = a.getRangeMax();
			int alarmType = ALARMTYPE_DISABLED;
			double threshold = 0.0;
			double thresholdRange = 0.0;
			if (!Double.isNaN(min) && !Double.isNaN(max) && (min >= max)) {
				min = Double.NaN;
				max = Double.NaN;
			}
			if (!Double.isNaN(min) && !Double.isNaN(rangeMin) && (min <= rangeMin)) {
				min = Double.NaN;
				alarmType = ALARMTYPE_EXCEEDTHRESHOLD;
				if (a.getName().equals("temperature")) {
					max = getTemperaureInCelcius(orgId, max);
				}
				threshold = new BigDecimal(max).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(); //(int) Math.round(max);
				thresholdRange = 0.0;
			}
			if (!Double.isNaN(max) && !Double.isNaN(rangeMax) && (max >= rangeMax)) {
				max = Double.NaN;
				alarmType = ALARMTYPE_BELOWTHRESHOLD;
				if (a.getName().equals("temperature")) {
					min = getTemperaureInCelcius(orgId, min);
				}
				threshold = new BigDecimal(min).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(); //(int) Math.round(min);
				thresholdRange = 0.0;
			}
			if (Double.isNaN(min) && Double.isNaN(max)) {
				alarmType = ALARMTYPE_DISABLED;
				threshold = new BigDecimal(rangeMin).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(); //(int) rangeMin;
				thresholdRange = threshold;
			}
			if (!Double.isNaN(min) && !Double.isNaN(max)) {
				alarmType = ALARMTYPE_OUTOFRANGE;
				if (a.getName().equals("temperature")) {
					min = getTemperaureInCelcius(orgId, min);
					max = getTemperaureInCelcius(orgId, max);
				}
				threshold = new BigDecimal(min).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(); //(int) Math.round(min);
				thresholdRange = new BigDecimal(max).setScale(1, BigDecimal.ROUND_HALF_UP).doubleValue(); //(int) Math.round(max);
			}
			ThingSpaceCmdAlarm alarm = new ThingSpaceCmdAlarm(alarmType, threshold, thresholdRange);
			String lineToAdd = alarm.toAlarmJson(attrName);
			if (lineToAdd == null || lineToAdd.trim().length() == 0) continue;
			command.append(lineToAdd);
		}
		String temp = command.toString().substring(0, command.toString().length() - 1); //remove the last comma added in the above for loop
		command.setLength(0);
		command.append(temp).append("}}");
		logger.info("ThingSpaceController.changeSettings - doSetupDevice url = " + cmdURL + " POST request = " + command.toString());
		ThingSpaceUtils.logMessage("ThingSpaceController.changeSettings - doSetupDevice url = " + cmdURL + " POST request = " + command.toString());
		response = http.doPost(cmdURL, "Authorization", authToken, command.toString());
		if (response == null) {
			ThingSpaceUtils.logMessage("ThingSpaceController.changeSettings - doSetupDevice POST failed to get response");
		}
		ThingSpaceUtils.logMessage("ThingSpaceController.changeSettings - doSetupDevice POST response = " + response);
		//sleep(200L);
	}
	
	@Override
	public void run() {
		try	{
			ThingSpaceRequest req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("ThingSpaceController - dequeuing request " + req.toString());
				if ((req.getDeviceKey() != null) && (req.getDeviceKey().equalsIgnoreCase(SensorConstant.DEVICE_KEY_DEMO))) {
					logger.info("ThingSpaceController - skipping demo device " + req.getDeviceID());
					continue;
				}
				this.doSetupDevice(	req.getOrgID(),req.getDeviceID(),req.getDeviceKey(),req.getCmd(),req.getAttributes(), req.getProfile());
			}
		}
		catch(Exception e){
			logger.info("ThingSpaceController thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}
}
