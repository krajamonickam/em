package com.rfxcel.sensor.control;

import java.io.IOException; 

import org.apache.http.HttpEntity; 
import org.apache.http.HttpHost; 
import org.apache.http.HttpResponse; 
import org.apache.http.auth.AuthScheme; 
import org.apache.http.auth.AuthScope; 
import org.apache.http.auth.UsernamePasswordCredentials; 
import org.apache.http.client.AuthCache;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.CredentialsProvider; 
import org.apache.http.client.HttpResponseException; 
import org.apache.http.client.ResponseHandler; 
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet; 
import org.apache.http.client.methods.HttpPost; 
import org.apache.http.client.methods.HttpPut; 
import org.apache.http.client.protocol.HttpClientContext; 
import org.apache.http.entity.StringEntity; 
import org.apache.http.impl.auth.BasicScheme; 
import org.apache.http.impl.client.BasicCredentialsProvider; 
import org.apache.http.impl.client.BasicResponseHandler; 
import org.apache.http.impl.client.CloseableHttpClient; 
import org.apache.http.impl.client.HttpClients; 
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager; 
import org.apache.http.util.EntityUtils; 
import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.control.RESTClient.SynchronousResponseHandler;
import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;

 
public final class RESTClient { 
    private static final Logger logger = Logger.getLogger(RESTClient.class);
    private static final long TIMEOUT = 60000L; //one minute
    private final CloseableHttpClient client; 
    private final HttpClientContext context; 
    
    public RESTClient(String username, String password) { 
        final PoolingHttpClientConnectionManager connManager = new PoolingHttpClientConnectionManager(); 
        connManager.setMaxTotal(200); 
        connManager.setDefaultMaxPerRoute(20); 
 
        RequestConfig requestConfig = RequestConfig.custom().setSocketTimeout(30000).build(); 
        CredentialsProvider credsProvider = new BasicCredentialsProvider(); 
        if(username != null){
        	credsProvider.setCredentials(new AuthScope(null, -1), new UsernamePasswordCredentials(username, password)); 
        }
        client = HttpClients.custom().setConnectionManager(connManager) 
                                     .setDefaultRequestConfig(requestConfig) 
                                     .setDefaultCredentialsProvider(credsProvider) 
                                     .build(); 
        context = HttpClientContext.create(); 
        context.setAuthCache(new SingleAuthCache(new BasicScheme())); 
    } 
 
 
    public String doGet(String url) { 
        logger.debug(url); 
        try { 
            final HttpGet httpget = new HttpGet(url); 
            httpget.setHeader("Accept", "application/json"); 
 
            final ResponseHandler<String> responseHandler = new BasicResponseHandler(); 
            return client.execute(httpget, responseHandler, context); 
        } 
        catch (HttpResponseException e) { 
        	return("{ \"error\" : " + e.getStatusCode() + " }");
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
    
    
    /**
     * <p> Use this method to set additional header
     * @param url
     * @param headerName
     * @param headerValue
     * @return
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    public String doGet(String url, String headerName, String headerValue) throws ClientProtocolException, IOException { 
        logger.info("GET " + url + " " + headerName + "=" + headerValue); 
    	try { 
    		final HttpGet httpget = new HttpGet(url); 
    		httpget.setHeader("Accept", "application/json"); 
    		httpget.setHeader(headerName, headerValue);	
    		final ResponseHandler<String> responseHandler = new BasicResponseHandler(); 
    		return client.execute(httpget, responseHandler, context); 
    	} 
    	finally{
    	}
    } 
 
    public void doPost(String url, String content) { 
        logger.debug(url); 
        try { 
            final HttpPost post = new HttpPost(url); 
            post.setEntity(new StringEntity(content)); 
            if (content != null) client.execute(post, new ConsumingResponseHandler(), context); 
        } 
        catch (Exception e) { 
            logger.error(url, e); 
        } 
    } 
    
    public String doPost(String url, String headerName, String headerValue, String content) { 
        logger.info("POST " + url + " " + headerName + "=" + headerValue + " data: " + content); 
        String result = null;
    	Timer t = new Timer();
        try { 
            final HttpPost post = new HttpPost(url); 
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept", "application/json");
            post.setHeader(headerName, headerValue);
            post.setEntity(new StringEntity(content)); 
            if (content != null) {
            	SynchronousResponseHandler handler = new SynchronousResponseHandler();
            	result = client.execute(post, handler, context);
            	String text = handler.getText(); 
                logger.info("POST " + url + " successful in " + t.getSeconds() + "s " + ((text != null)?"with data: " + text:"")); 
            	return(text);
            }
        } 
        catch (Exception e) { 
            logger.info("POST " + url + " exception in " + t.getSeconds() + "s with data:" + e.getMessage()); 
            logger.error(url, e); 
        }
		return result; 
    } 
    
    public String doDelete(String url, String headerName, String headerValue) { 
        logger.info("DELETE " + url + " " + headerName + "=" + headerValue); 
        String result = null;
        try { 
            final HttpDelete httpDelete = new HttpDelete(url);
            httpDelete.setHeader("Content-Type", "application/json");
            httpDelete.setHeader("Accept", "text/plain");
            httpDelete.setHeader(headerName, headerValue);
            final ResponseHandler<String> responseHandler = new BasicResponseHandler(); 
    		result = client.execute(httpDelete, responseHandler, context); 
            
        } 
        catch (Exception e) { 
            logger.error(url, e); 
        }
		return result; 
    } 
 
 
    public void doPut(String url, String content) { 
        logger.debug(url); 
        try { 
            final HttpPut put = new HttpPut(url); 
            put.setHeader("Content-Type", "application/json"); 
            put.setHeader("Accept", "application/json");
            if (content != null) put.setEntity(new StringEntity(content)); 
             
            client.execute(put, new ConsumingResponseHandler(), context); 
        } 
        catch (Exception e) { 
            logger.error(url, e); 
        } 
    } 
    
    public String doRTSPost(String url, String headerName, String headerValue, String content) throws Exception { 
        logger.info("POST " + url + " " + headerName + "=" + headerValue + " data: " + content); 
        String result = null;
    	Timer t = new Timer();
        try { 
            final HttpPost post = new HttpPost(url); 
            post.setHeader("Content-Type", "application/json");
            post.setHeader("Accept", "application/json");
            post.setHeader(headerName, headerValue);
            post.setEntity(new StringEntity(content)); 
            if (content != null) {
            	SynchronousResponseHandler handler = new SynchronousResponseHandler();
            	result = client.execute(post, handler, context);
            	String text = handler.getText(); 
                logger.info("POST " + url + " successful in " + t.getSeconds() + "s " + ((text != null)?"with data: " + text:"")); 
            	return(text);
            }
        } 
        catch (Exception e) { 
            logger.info("POST " + url + " exception in " + t.getSeconds() + "s with data:" + e.getMessage()); 
            logger.error(url, e); 
            throw e;
        }
		return result; 
    } 
 
     public void shutdown() { 
        try { 
            client.close(); 
        } 
        catch (Exception e) { 
            logger.error("error shutting down", e); 
        } 
    } 
     
    public static final class SingleAuthCache implements AuthCache { 
        private AuthScheme authScheme; 
        public SingleAuthCache(AuthScheme authScheme) { this.authScheme = authScheme; } 
        public void put(HttpHost host, AuthScheme authScheme) { this.authScheme = authScheme; }  
        public AuthScheme get(HttpHost host) { return this.authScheme; }  
        public void remove(HttpHost host) { return; }  
        public void clear() { return; } 
    } 
     
    public static final class ConsumingResponseHandler implements ResponseHandler<String> { 
        public String handleResponse(HttpResponse response) { 
            final HttpEntity entity = response.getEntity(); 
            final int statusCode = response.getStatusLine().getStatusCode(); 
            try { 
                if (statusCode >= 300 && statusCode != 302) { 
                    logger.error("Failed to PUT/POST\n" + EntityUtils.toString(entity)); 
                } 
                EntityUtils.consume(entity); 
            } catch (IOException e) { 
                logger.error("Failed to consume response entity"); 
            } 
            return ""; 
        } 
    } 
    
    public static final class SynchronousResponseHandler implements ResponseHandler<String> { 
    	public String text = null;
        public String handleResponse(HttpResponse response) { 
            final HttpEntity entity = response.getEntity(); 
            final int statusCode = response.getStatusLine().getStatusCode(); 
            try { 
                if (statusCode >= 300 && statusCode != 302) { 
                    logger.error("Failed to PUT/POST\n" + EntityUtils.toString(entity)); 
                } 
                if (entity != null) {
                    setText(EntityUtils.toString(entity, "UTF-8"));
                }
                else {
                    setText(null);
                }
            } catch (IOException e) { 
                setText(null);
                logger.error("Failed to consume response entity"); 
            } 
            return(text); 
        }
		public String getText() {
			synchronized(this) {
				if (text != null) return(text);
				try {
					this.wait(TIMEOUT);
				} catch (InterruptedException e) {
					return(null);
				}
			}
			return(text);
		}
		public void setText(String text) {
			synchronized(this) {
                logger.debug("setText " + ((text != null)?"data: " + text:"")); 
				this.text = text;
	            this.notifyAll();
			}
		} 
        
    } 
}
