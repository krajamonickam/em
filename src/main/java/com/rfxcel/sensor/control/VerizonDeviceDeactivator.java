package com.rfxcel.sensor.control;

import java.io.IOException;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.client.ClientProtocolException;
import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.thingspace.util.ThingSpaceUtils;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class VerizonDeviceDeactivator implements Runnable{
	
	
	private static Logger logger = Logger.getLogger(VerizonDeviceDeactivator.class);
	private static VerizonDeviceDeactivator instance = null;
	private Thread thread = null;
	private LinkedBlockingQueue<Device> queue = new LinkedBlockingQueue<Device>();
	
	private VerizonDeviceDeactivator(){}

	public static VerizonDeviceDeactivator getInstance(){
		synchronized (VerizonDeviceActivator.class) {
			if(instance == null){
				instance = new VerizonDeviceDeactivator();
			}
		}
		return instance;
	}
	
	public void deActivateDevice(Device device){
		synchronized(this) {
			if (thread == null) {
				logger.info("Verizon Device Deactivator - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		this.queue.add(device);
	}
	

	@Override
	public void run() {
		try	{
			Device req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("Verizon Device Deactivator - dequeuing request " + req.toString());
				this.deActivateVerizonDevice(req.getOrgId(),req.getDeviceKey());
				this.deRegisterVerizonDevice(req.getOrgId(), req.getDeviceId());
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.info("Verizon Device Activator thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}

	private boolean deActivateVerizonDevice(Integer orgId, String key){	
		String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		url = url + "/" + key + "/actions/deactivate";
		RESTClient httpClient = new RESTClient(null, null);
		String response = httpClient.doPost(url, "Authorization", authToken, "");
		if (response == null) {
			logger.error("Failed to deactivate deviceKey " + key + " url " + url + " token " + authToken);
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.deActivateVerizonDevice - failed to deactivate deviceKey " + key + " url " + url + " token " + authToken);
			return(false);
		}
		ThingSpaceUtils.logMessage("VerizonDeviceActivator.deActivateVerizonDevice - url = " + url + " response = " + response);
		JSONObject obj = new JSONObject(response);
		if (obj.optString("error", null) != null) {
			String errorMsg = obj.getString("error_description");
			logger.error("Failed to deactivate deviceKey " + key + " url " + url + " token " + authToken + " error " + errorMsg);
			return(false);
		}
		return(true);
	}
	
	private String deRegisterVerizonDevice(Integer orgId, String deviceID){	
		String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		String key = this.findVerizonDevice(orgId, deviceID);
		if (key == null) return(null);
		url = url +"/"+ key;
		RESTClient httpClient = new RESTClient(null, null);
		String response =  httpClient.doDelete(url, "Authorization", authToken);
		if (response == null) {
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.deRegisterVerizonDevice - failed to get response url " + url + " token " + authToken);
			return(null);
		}
		ThingSpaceUtils.logMessage("VerizonDeviceActivator.deRegisterVerizonDevice - url = " + url + " response = " + response);
		return response;
	}	
	
	private String findVerizonDevice(Integer orgId, String deviceID){
		try {
			String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
			if (authToken == null) {
				logger.error("Failed to get authToken for deviceID " + deviceID);
				return(null);
			}
			String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
			RESTClient httpClient = new RESTClient(null, null);
			String response = httpClient.doGet(url, "Authorization", authToken);
			if (response == null) {
				logger.error("Failed to get response from ThingSpace for url " + url + " token " + authToken);
				ThingSpaceUtils.logMessage("VerizonDeviceActivator.findVerizonDevice - failed to get response url " + url + " token " + authToken);
				return(null);
			}
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.findVerizonDevice - url = " + url + " response = " + response);
			JSONArray array = new JSONArray(response);
			for (int i = 0; i < array.length(); i++) {
				String text = array.get(i).toString();
				JSONObject o = new JSONObject(text);
				String id = o.getString("id");
				String imei = o.optString("imei");
				if ((imei != null) && (imei.equalsIgnoreCase(deviceID))) {
					return(id);
				}
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(null);
	}	
}
