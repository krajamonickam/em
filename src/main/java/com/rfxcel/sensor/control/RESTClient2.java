package com.rfxcel.sensor.control;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

import okhttp3.Authenticator;
import okhttp3.Credentials;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.Route;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;
 
public final class RESTClient2 { 
    private static final Logger logger = Logger.getLogger(RESTClient2.class);
	private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");
    private static final long TIMEOUT = 600000L; //ten minutes
    private final OkHttpClient client; 
    
    public RESTClient2() { 
        client = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .build();
    } 
    
    public RESTClient2(String username, String password) { 
        client = new OkHttpClient.Builder()
                .connectTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .writeTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .readTimeout(TIMEOUT, TimeUnit.MILLISECONDS)
                .authenticator(new Authenticator() {
                  @Override public Request authenticate(Route route, Response response) throws IOException {
                    if (response.request().header("Authorization") != null) {
                      return null; // Give up, we've already attempted to authenticate.
                    }

                    System.out.println("Authenticating for response: " + response);
                    System.out.println("Challenges: " + response.challenges());
                    String credential = Credentials.basic(username, password);
                    return response.request().newBuilder()
                        .header("Authorization", credential)
                        .build();
                  }
                })
                .build();
    } 
    
    public static void main(String[] args)
    {
    	//String url = "https://core.thingspace.verizon.com/oauth2/token";
    	//String authToken = "YmY2MjI2MTYtNzE3Zi00ZjExLTk2NTEtODI1ZmM0MjBmMjBj";
    	String url = "https://jsonplaceholder.typicode.com/posts";
    	RESTClient2 httpClient = new RESTClient2();
    	String response = httpClient.doPost(url, "Content-type", "application/json; charset=UTF-8", "{\"title\":\"foo\",\"body\":\"bar\",\"userId\":1}");
    	System.out.println("response="  + response);
    	
    }
 
 
    public String doGet(String url) { 
        logger.info("GET " + url); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
    
    
    /**
     * <p> Use this method to set additional header
     * @param url
     * @param headerName
     * @param headerValue
     * @return
     * @throws IOException 
     * @throws ClientProtocolException 
     */
    public String doGet(String url, String headerName, String headerValue)  { 
        logger.info("GET " + url + " " + headerName + "=" + headerValue); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.header(headerName, headerValue)
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
 
    public String doPost(String url, String content) { 
        logger.info("POST " + url + " " + " data: " + content); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.post(RequestBody.create(JSON, content))
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
    
    public String doPost(String url, String headerName, String headerValue, String content) { 
        logger.info("POST " + url + " " + headerName + "=" + headerValue + " data: " + content); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.header(headerName, headerValue)
        			.post(RequestBody.create(JSON, content))
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
    
    public String doDelete(String url, String headerName, String headerValue) { 
        logger.info("DELETE " + url + " " + headerName + "=" + headerValue); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.header(headerName, headerValue)
        			.delete()
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
 
 
    public String doPut(String url, String content) { 
        logger.info("PUT " + url + " " + " data: " + content); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.put(RequestBody.create(JSON, content))
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
 
    public String doPut(String url, String headerName, String headerValue, String content) { 
        logger.info("PUT " + url + " " + headerName + "=" + headerValue + " data: " + content); 
        try { 
        	Request request = new Request.Builder()
        			.url(url)
        			.header(headerName, headerValue)
        			.put(RequestBody.create(JSON, content))
        			.build();
            try (Response response = client.newCall(request).execute()) {
                if (!response.isSuccessful()) {
                	return("{ \"error\" : " + response + " }");
                }
                return(response.body().string());
              }
        } 
        catch (Exception e) { 
            logger.error(url, e); 
            return("{ \"error\" : \"" + e.getMessage() + "\" }");
        } 
    } 
 
     public void shutdown() { 
        try { 
        	client.dispatcher().executorService().shutdown();
        	client.connectionPool().evictAll();
        } 
        catch (Exception e) { 
            logger.error("error shutting down", e); 
        } 
    } 
     
}
