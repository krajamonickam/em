package com.rfxcel.sensor.control;

import java.io.File;
import java.io.StringWriter;
import java.util.HashMap;
import java.util.concurrent.LinkedBlockingQueue;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.messaging.SMSService;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;

@Path("sensor")
public class QueclinkController implements Runnable
{
	private static final Logger logger = Logger.getLogger(QueclinkController.class);
	private static QueclinkController instance = null;
	private static ConfigCache configCache = ConfigCache.getInstance();
	private static LinkedBlockingQueue<QueclinkRequest> queue = new LinkedBlockingQueue<QueclinkRequest>();
	private static HashMap<String, String> deviceConfig = new HashMap<String, String>();
	private static Thread thread = null;
	private static String controlURL  = null;
	private static String controlPort = null;
	private static String configURL  = null;
	private static String configTemplate = null;
	private static String configCommand = null;
	private static String configDir = null;
	private static String locateNowCmd = null;
	
	public static class QueclinkRequest
	{
		public String deviceID;
		public String deviceKey;
		public String deviceCmd;

		public QueclinkRequest(String deviceID, String deviceKey, String deviceCmd) {
			this.deviceID = deviceID;
			this.deviceKey = deviceKey;
			this.deviceCmd = deviceCmd;
		}

		public String getDeviceID() {
			return deviceID;
		}
		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}
		public String getDeviceKey() {
			return deviceKey;
		}
		public void setDeviceKey(String deviceKey) {
			this.deviceKey = deviceKey;
		}
		public String getDeviceCmd() {
			return deviceCmd;
		}
		public void setDeviceCmd(String deviceCmd) {
			this.deviceCmd = deviceCmd;
		}
	}
	

	public static void main(String[] args) {
		try {
			QueclinkController c = QueclinkController.getInstance();
			c.setupDevice(0, "A1000043D214F4", "+19252661993", 30, 30, 30);
		} catch (Exception e) {
		}
	}
	
	public static QueclinkController getInstance()
	{
		if (instance == null) {
			instance = new QueclinkController();
		}
		return(instance);
	}
	
	public QueclinkController()
	{
		if (instance != null) {
			logger.info("QueclinkController - already initialized with URL " + controlURL + " Port " + controlPort);
			return;
		}
		controlURL  = configCache.getSystemConfig("queclink.controller.url");
		controlPort = configCache.getSystemConfig("queclink.controller.port");
		configURL   = configCache.getSystemConfig("queclink.config.url");
		configTemplate   = configCache.getSystemConfig("queclink.config.template");
		configCommand    = configCache.getSystemConfig("queclink.config.cmd");
		configDir	     = configCache.getSystemConfig("queclink.config.dir");
		locateNowCmd 	 = configCache.getSystemConfig("queclink.config.locate.cmd"); 
		
		if (controlURL == null) {
			logger.error("QueclinkController - system config invalid value for queclink.controller.url");
			controlURL = "www.verizonitt.net";
		}
		if (controlPort == null) {
			logger.error("QueclinkController - system config invalid value for queclink.controller.port");
			controlPort = "6880";
		}
		if (configURL == null) {
			logger.error("QueclinkController - system config invalid value for queclink.config.url");
			configURL = "52.8.38.100/";
		}
		if (configTemplate == null) {
			logger.error("QueclinkController - system config invalid value for queclink.config.template");
			configTemplate = "AT+GTRTO=gl300vc,4,,,,,,FFFF$\r\n" +
							"AT+GTSRI=gl300vc,2,,2,<queclink.device.url>,<queclink.device.port>,0.0.0.0,0,,15,0,0,,,,FFFF$" +
							"AT+GTFKS=gl300vc,1,1,3,1,1,3,3,10,3,3,FFFF$" + 
							"AT+GTCFG=gl300vc,gl300vc,GL300VC,0,0.0,1,5,1F,,,823,0,1,0,60,0,0,20491231235959,1,0,,FFFF$" + 
							"AT+GTTMA=gl300vc,+,0,0,0,,0,,,,FFFF$" + 
							"AT+GTFRI=gl300vc,1,0,,,0000,0000,<profile.gps.freq>,<profile.gps.freq>,9999,9999,,1000,1000,0,5,50,5,0,0,FFFF$" +
							"AT+GTTEM=gl300vc,0,0,60,10,900,,,,,,,FFFF$" + 
							"AT+GTTPR=gl300vc,1,<profile.attr.freq>,<profile.comm.count>,1,,,,,FFFF$" + 
							"AT+GTDAT=gl300vc,0,,Device <deviceid> setup completed,1,,,,FFFF$";
		}
		configTemplate = reformatConfigTemplate(configTemplate);
		
		if (configCommand == null) {
			logger.error("QueclinkController - system config invalid value for queclink.config.cmd");
			configCommand = "AT+GTUPC=gl300vc,3,6,0,1,0,<queclink.config.url>,1,,,,FFFF$";
		}
		
		if(locateNowCmd == null){
			logger.error("QueclinkController - system config invalid value for queclink.config.locate.cmd");
			configCommand = "AT+GTRTO=gl300vc,1,,,,,,FFFF$";
		}
		
		
		if (configDir == null) {
			logger.error("QueclinkController - system config invalid value for queclink.config.dir");
			configDir = "C:\\Program Files\\Apache2.2\\htdocs";
		}
		
		File dir  = new File(configDir);
		File file = new File(dir, "device.ini");
		Utility.stringToFile(configTemplate, file);
		
		logger.info("QueclinkController - initialized with URL " + controlURL + " Port " + controlPort);
	}
	
	private static String reformatConfigTemplate(String text)
	{
		text = text.replaceAll("\r", "");
		text = text.replaceAll("\n", "");
		String[] lines = text.split("\\$");
		StringWriter sw = new StringWriter();
		for (int i = 0; i < lines.length; i++) {
			sw.append(lines[i] + "$\r\n");
		}
		return(sw.toString());
	}
	
	@GET
	@Path("/deviceconfig/{id}")
	@Consumes("application/json")
	@Produces("application/text")
	public Response getDeviceConfig(@PathParam("id") String id)
	{
		String deviceID = id;
		String text = deviceConfig.get(deviceID);
		if (text == null) {
			text = "AT+GTDAT=gl300vc,0,,Device " + deviceID + " config not found,1,,,,FFFF$";
		}
		logger.info("QueclinkController - GET " + deviceID + " config:\n" + text);
		return Response.status(200).entity(text).build();
	}
	
	public String getQueclinkConfiguration(String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq)
	{
		String text = new String(configTemplate);
		text = text.replace("<queclink.device.url>", controlURL);
		text = text.replace("<queclink.device.port>", controlPort);
		text = text.replace("<profile.gps.freq>", new Integer(gpsFreq).toString());
		text = text.replace("<profile.attr.freq>",  new Integer(tempFreq).toString());
		int commCount = (commFreq/tempFreq);
		text = text.replace("<profile.comm.count>",  new Integer(commCount).toString());
		text = text.replace("<deviceid>",  deviceID);
		text = text.replace("<devicekey>",  deviceKey);
		logger.info("QueclinkController - device " + deviceID + " config:\n" + text);
		return(text);
	}
	
	public String getQueclinkCommand(String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq)
	{
		String text = new String(configCommand);
		text = text.replace("<queclink.config.url>", configURL);
		text = text.replace("<deviceid>",  deviceID);
		text = text.replace("<devicekey>",  deviceKey);
		logger.info("QueclinkController - device " + deviceID + " command:\n" + text);
		return(text);
	}
	
	public void setupDevice(int orgID, String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq)
	{
		synchronized(this) {
			if (thread == null) {
				logger.info("SMSService - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		String configText = getQueclinkConfiguration(deviceID, deviceKey, commFreq, gpsFreq, tempFreq);
		File dir  = new File(configDir);
		File file = new File(dir, deviceID + ".ini");
		Utility.stringToFile(configText, file);
		logger.info("QueclinkController - saved content to file " + file.getAbsolutePath());
		deviceConfig.put(deviceID, configText);
		String configCmd  = getQueclinkCommand(deviceID, deviceKey, commFreq, gpsFreq, tempFreq);
		QueclinkRequest req = new QueclinkRequest(deviceID, deviceKey, configCmd);
		this.queue.add(req);
		logger.info("QueclinkController - queuing request " + req.toString());
	}
	
	private void doSetupDevice(String deviceID, String deviceKey, String deviceCmd)
	{
		Integer orgId = DeviceCache.getInstance().getDeviceOrgId(deviceID);
		for (int i = 0; i < 3; i++) {
			SMSService sms = SMSService.getInstance();
			logger.info("QueclinkController - sending SMS " + deviceKey +  " for device " + deviceID + " content " + deviceCmd);			
			if (sms.doSend(deviceKey, deviceCmd)) {
				DeviceLogDAO.doAddDeviceLog(deviceID, 1L, "QueclinkController - sent SMS " + deviceKey +  " for device " + deviceID + " content " + deviceCmd, orgId);
				logger.info("QueclinkController - sent SMS " + deviceKey +  " for device " + deviceID + " content " + deviceCmd + "org Id " + orgId);
				return;
			}
			else {
				DeviceLogDAO.doAddDeviceLog(deviceID, 1L, "Failed to send profile settings to device("+ deviceID +"), retrying...", orgId);
			}
		}
		DeviceLogDAO.doAddDeviceLog(deviceID, 0L, "Failed to send profile settings to device("+ deviceID +"), giving up!", orgId);
	}


	@Override
	public void run() {
		try
		{
			QueclinkRequest req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("QueclinkCmdMsg - dequeuing request " + req.toString());
				this.doSetupDevice(req.getDeviceID(), 
						req.getDeviceKey(), 
						req.getDeviceCmd());
			}
		}
		catch(Exception e)
		{
			logger.info("QueclinkCmdMsg thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}

	/**
	 * 
	 * @param userId 
	 * @param device
	 * @return
	 */
	public com.rfxcel.sensor.vo.Response locateDevice(long userId, Device device) {
		com.rfxcel.sensor.vo.Response resp = new com.rfxcel.sensor.vo.Response();
		
		String deviceId = device.getDeviceId();
		String deviceKey = device.getDeviceKey();
		Integer orgId = DeviceCache.getInstance().getDeviceOrgId(deviceId);
		for (int i = 0; i < 3; i++) {
			SMSService sms = SMSService.getInstance();
			logger.info("QueclinkController - Locate Now SMS " + deviceKey +  " for device " + deviceId + " content " + locateNowCmd);
			if (sms.doSend(deviceKey, locateNowCmd)) {
				DeviceLogDAO.doAddDeviceLog(deviceId, 1L, "QueclinkController - sent SMS " + deviceKey +  " for device " + deviceId + " content " + locateNowCmd, orgId);
				logger.info("QueclinkController - Locate Now sent SMS " + deviceKey +  " for device " + deviceId + " content " + locateNowCmd + " org Id " + orgId );
				resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Locate Now command sent successfully to the device");
				return resp;
			}
			else {
				DeviceLogDAO.doAddDeviceLog(deviceId, 1L, "Failed to send Locate Now cmd to device("+ deviceId +"), retrying...", orgId);
			}
		}
		DeviceLogDAO.doAddDeviceLog(deviceId, 0L, "Failed to send Locate Now cmd to device("+ deviceId +"), giving up!", orgId);
		resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Unable to send Locate Now command to device");
		return resp;
	
	}
}
