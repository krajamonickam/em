package com.rfxcel.sensor.control;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.notification.dao.AlertDAO;
import com.rfxcel.notification.dao.NotificationDAO;
import com.rfxcel.notification.vo.NotificationReqVO;
import com.rfxcel.notification.vo.NotificationResVO;
import com.rfxcel.notification.vo.NotificationVO;
import com.rfxcel.notification.vo.RTSNotificationVO;
import com.rfxcel.notification.vo.ShipmentNotifications;
import com.rfxcel.sensor.beans.DeviceInfoDetails;
import com.rfxcel.sensor.beans.Location;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

public class ItemController {
	private static final Logger logger = Logger.getLogger(ItemController.class);
	//private static String authToken = "3d831cefe05d937e3a0428fc5387d48a";
	public static ItemController instance = null;
	
	private RESTClient http = null;
	public ItemController() {
		http = new RESTClient("", "");
	}
	
	public static ItemController getInstance() {
		if (instance == null) {
			instance = new ItemController();
		}
		return instance;
	}
	
	public static class ItemListData {
		public String traceEntId;
		public String traceExtId;
		
		public String traceEntExtId;
		public String itemId;
		public String itemType;
		public String itemName;
		public String itemLocation;
		public String disposition;
		public String lotId;		
		public Integer quantity;
		public String mfrName;
		public String receivedDate;		
		public int hasChild;
		public String latitude;
		public String longitude;
		public String expiryDate;
		private String dispositionColour;
		
		public String getTraceExtId() {
			return traceExtId;
		}
		public void setTraceExtId(String traceExtId) {
			this.traceExtId = traceExtId;
		}
		public String getExpiryDate() {
			return expiryDate;
		}
		public void setExpiryDate(String expiryDate) {
			this.expiryDate = expiryDate;
		}
		public String getTraceEntId() {
			return traceEntId;
		}
		public void setTraceEntId(String traceEntId) {
			this.traceEntId = traceEntId;
		}
		public String getTraceEntExtId() {
			return traceEntExtId;
		}
		public void setTraceEntExtId(String traceEntExtId) {
			this.traceEntExtId = traceEntExtId;
		}
		public String getItemId() {
			return itemId;
		}
		public void setItemId(String itemId) {
			this.itemId = itemId;
		}
		public String getItemType() {
			return itemType;
		}
		public void setItemType(String itemType) {
			this.itemType = itemType;
		}
		public String getItemName() {
			return itemName;
		}
		public void setItemName(String itemName) {
			this.itemName = itemName;
		}
		public String getItemLocation() {
			return itemLocation;
		}
		public void setItemLocation(String itemLocation) {
			this.itemLocation = itemLocation;
		}
		public String getDisposition() {
			return disposition;
		}
		public void setDisposition(String disposition) {
			this.disposition = disposition;
		}
		public String getLotId() {
			return lotId;
		}
		public void setLotId(String lotId) {
			this.lotId = lotId;
		}
		
		public Integer getQuantity() {
			return quantity;
		}
		public void setQuantity(Integer quantity) {
			this.quantity = quantity;
		}
		public String getMfrName() {
			return mfrName;
		}
		public void setMfrName(String mfrName) {
			this.mfrName = mfrName;
		}
		public String getReceivedDate() {
			return receivedDate;
		}
		public void setReceivedDate(String receivedDate) {
			this.receivedDate = receivedDate;
		}
		public int getHasChild() {
			return hasChild;
		}
		public void setHasChild(int hasChild) {
			this.hasChild = hasChild;
		}
		
		public String getLatitude() {
			return latitude;
		}
		public void setLatitude(String latitude) {
			this.latitude = latitude;
		}
		public String getLongitude() {
			return longitude;
		}
		public void setLongitude(String longitude) {
			this.longitude = longitude;
		}
		public String getDispositionColour() {
			return dispositionColour;
		}
		public void setDispositionColour(String dispositionColour) {
			this.dispositionColour = dispositionColour;
		}
	}
	
	public static class ItemMapLocation {
		private String eventId;
		public String lat;
		public String lng;
		public String title;
		public String description;
		public Integer pairStatus;
		public String address;
		public String eventDate;
		public String locationName;
		public String expiryDate;
		public String getEventId() {
			return eventId;
		}
		public void setEventId(String eventId) {
			this.eventId = eventId;
		}
		public String getLat() {
			return lat;
		}
		public void setLat(String lat) {
			this.lat = lat;
		}
		public String getLng() {
			return lng;
		}
		public void setLng(String lng) {
			this.lng = lng;
		}
		public String getTitle() {
			return title;
		}
		public void setTitle(String title) {
			this.title = title;
		}
		public String getDescription() {
			return description;
		}
		public void setDescription(String description) {
			this.description = description;
		}
		public Integer getPairStatus() {
			return pairStatus;
		}
		public void setPairStatus(Integer pairStatus) {
			this.pairStatus = pairStatus;
		}
		public String getAddress() {
			return address;
		}
		public void setAddress(String address) {
			this.address = address;
		}
		public String getEventDate() {
			return eventDate;
		}
		public void setEventDate(String eventDate) {
			this.eventDate = eventDate;
		}
		public String getLocationName() {
			return locationName;
		}
		public void setLocationName(String locationName) {
			this.locationName = locationName;
		}
		public String getExpiryDate() {
			return expiryDate;
		}
		public void setExpiryDate(String expiryDate) {
			this.expiryDate = expiryDate;
		}
	}
	
	
	public SensorResponse getItemList(int orgId) {
		SensorResponse response = new SensorResponse();
		Sensor sensor = new Sensor();
		ArrayList<DeviceInfoDetails> shipmentList = new ArrayList<DeviceInfoDetails>();
		com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
		try {			
			String bodyText = "{\r\n\t\"requestObject\": {\r\n\t\t\"traceIdCondition\": \"0\",\r\n\t\t\"traceIdValue\": \"\",\r\n\t\t\"itemTypeCondition\": \"0\",\r\n\t\t\"itemTypeValue\": \"1\",\r\n\t\t\"itemLocationCondition\": \"0\",\r\n\t\t\"itemLocationValue\": \"\",\r\n\t\t\"lotIdCondition\": \"0\",\r\n\t\t\"lotIdValue\": \"\",\r\n\t\t\"mfrNameCondition\": \"0\",\r\n\t\t\"mfrNameValue\": \"\",\r\n\t\t\"itemIdCondition\": \"\",\r\n\t\t\"itemIdValue\": \"\",\r\n\t\t\"itemNameCondition\": \"0\",\r\n\t\t\"itemNameValue\": \"\",\r\n\t\t\"dispositionCondition\": \"0\",\r\n\t\t\"dispositionValue\": \"\",\r\n\t\t\"expDateCondition\": \"0\",\r\n\t\t\"expDateValue1\": \"\",\r\n\t\t\"expDateValue2\": \"\",\r\n\t\t\"limit\": 500,\r\n\t\t\"offset\": 0\r\n\t}\r\n}";
			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getItemListURL = rtsAppUrl + "item/getItemList";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getItemListURL, "Authorization", authToken, bodyText);
			
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving event list data.");
					return response;
				}
				
				JSONObject jsonObject = new JSONObject(jsonText);
				ObjectMapper mapper = new ObjectMapper();
				if(jsonObject.has("data")){
					JSONObject dataObject = jsonObject.getJSONObject("data");
					JSONArray pageItems = dataObject.getJSONArray("pageItems");
					Object[] items = mapper.readValue(pageItems.toString(), Object[].class);
					for(Object item : items){
						HashMap<String, String> itemMap =  (HashMap<String, String>) item;
						ItemListData itemListData = (ItemListData) mapper.convertValue(itemMap, ItemListData.class);
						DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails();
						
						String packageId = itemListData.getTraceEntExtId().substring(itemListData.getTraceEntExtId().lastIndexOf(".") + 1); //urn:epc:id:sscc:0364896.5000033500 traceId - 5000033500 //itemListData.getItemName();
						String productId = String.valueOf(itemListData.getItemId());
						String deviceId = itemListData.getLotId(); //itemListData.getTraceEntId();
						String traceId = itemListData.getTraceEntId(); 
						deviceInfoDetails.setProductId(productId);
						deviceInfoDetails.setPackageId(packageId);
						deviceInfoDetails.setTraceId(traceId);
						deviceInfoDetails.setDeviceId(deviceId);
						shipmentList.add(deviceInfoDetails);
						Location location = new Location(deviceId, packageId, productId);
						location.setLat(itemListData.getLatitude());
						location.setLang(itemListData.getLongitude());
						// do not add locations to the response if lat and lang values are empty
						if(location.getLat() != null &&  location.getLang() != null) {
							map.getLocations().add(location);	
						}
					}
				}		
			}
			sensor.setShipmentList(shipmentList);
			//set map to sensor
			sensor.setMap(map);
			response.setSensor(sensor);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in ItemController :: getItemList " + e.getMessage());
		}
		return response;
	}
	
	public SensorResponse getMapItemLocations(String traceItemId, int orgId) {
		SensorResponse response = new SensorResponse();
		Sensor sensor = new Sensor();
		try{
			com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
			String value = ConfigCache.getInstance().getSystemConfig("sensor.location.decimal.limit"); // get the precision value from config, if not present setting it to 3
			value = (value == null) ? "3" : value;
			int precision;
			try {
				precision = Integer.valueOf(value);
			} catch (NumberFormatException nfe) {
				precision = 3;
			}
			String bodyText = "{\"traceItemId\":\""+traceItemId+"\"}\r\n";
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getMapItemLocationsURL = rtsAppUrl + "item/getClusterMapItemLocations";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getMapItemLocationsURL, "Authorization", authToken, bodyText);
			
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving item location data.");
					return response;
				}
				
				JSONObject jsonObject = new JSONObject(jsonText);
				ObjectMapper mapper = new ObjectMapper();
				Location location;
				Long dateTime;
				String currentLat;
				String currentLang;
				String prevLat = "";
				String prevLang = "";
				if(jsonObject.has("listData")){					
					JSONArray locationList = jsonObject.getJSONArray("listData");
					Object[] locations = mapper.readValue(locationList.toString(), Object[].class);
					for(Object locationObj : locations){
						HashMap<String, String> locationMap =  (HashMap<String, String>) locationObj;
						ItemMapLocation itemMapLocation = (ItemMapLocation) mapper.convertValue(locationMap, ItemMapLocation.class);
						dateTime = Utility.getUTCDate(itemMapLocation.getEventDate());
						currentLat = itemMapLocation.getLat();
						currentLang = itemMapLocation.getLng();
						int pairStatus = itemMapLocation.getPairStatus();
						String address = itemMapLocation.getAddress();
						// Check if we need to add this data point						
						Boolean skip = SendumDAO.getInstance().skipDataPoints(precision, currentLat, currentLang, prevLat, prevLang);
						if (!skip) {
							location = new Location();
							location.setLat(currentLat);
							location.setLang(currentLang);
							location.setDateTime(dateTime);
							location.setPairStatus((pairStatus==1)?0:pairStatus);
							location.setAddress(address);
							// do not add locations to the response if lat and lang values are empty
							if(location.getLat() != null && location.getLang() != null){
								map.getLocations().add(location);	
							}							
						}else{
							location = map.getLocations().get(map.getLocations().size() -1);
							// if we are skipping the data point get the latest information at that lat and lang value							
							location.setLat(currentLat);
							location.setLang(currentLang);
							location.setDateTime(dateTime);
							location.setPairStatus((pairStatus==1)?0:pairStatus);
							location.setAddress(address);							
						}
						prevLang = currentLang;
						prevLat = currentLat;
																	
					}
				}
			}
			
			// If there are datapoints, get the last timestamp from that
			ArrayList<Location> locations = map.getLocations();
			if (locations != null && locations.size() > 0) {
				Long lastTimeStamp = locations.get(locations.size() - 1).getDateTime();
				map.setLastTimeStamp(lastTimeStamp);
			}
			sensor.setMap(map);
			response.setSensor(sensor);
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in ItemController :: getMapItemLocations " + e.getMessage());
		}
		return response;
	}
	
	
	public SensorResponse getMergeAlerts(SensorRequest sensorRequest) {
		SensorResponse response = new SensorResponse();
		Sensor sensor = new Sensor();
		try{
			List<NotificationVO> rtsNotifications=new ArrayList<>();
			NotificationReqVO notificationRequest=new NotificationReqVO();
			notificationRequest.setOrgId(sensorRequest.getOrgId());
			notificationRequest.setSearchType("All");
			notificationRequest.setGroupBy("device");
			notificationRequest.setSortBy("Age");
			NotificationResVO notificationResVOa=NotificationDAO.getInstance().getShipmentNotifications(notificationRequest);
			
			String bodyText = "{\"requestObject\":{\"subjectCondition\":\"\",\"subjectValue\":\"\",\"messageCondition\":\"\",\"messageValue\":\"\",\"trackingIdCondition\":\"\",\"trackingIdValue\":\"\",\"statusCondition\":\"0\",\"statusValue\":\"0\",\"raisedDateCondition\":\"\",\"raisedDateValue1\":\"\",\"raisedDateValue2\":\"\",\"notificationIdCondition\":\"\",\"notificationIdValue\":\"\",\"eventExtIdCondition\":\"\",\"eventExtIdValue\":\"\",\"limit\":50,\"offset\":1}}";
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", sensorRequest.getOrgId());
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String notificationsURL = rtsAppUrl + "notification/getAlerts";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", sensorRequest.getOrgId());
			String jsonText = http.doPost(notificationsURL, "Authorization", authToken, bodyText);
			
			if (jsonText != null && jsonText.trim().length() != 0) {
				JSONObject jsonObject = new JSONObject(jsonText);
				ObjectMapper mapper = new ObjectMapper();
				Location location;
				Long dateTime;
				
				if (jsonObject.getString("responseStatus").equals("error")) {
					response.setResponseCode(SensorConstant.RESPONSECODE_ERROR);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("Error while retriving Notifications from rts.");
					return response;
				}
				
				if(jsonObject.has("data")){
					JSONObject dataObject = jsonObject.getJSONObject("data");
					JSONArray pageItems = dataObject.getJSONArray("pageItems");
					Object[] items = mapper.readValue(pageItems.toString(), Object[].class);
					
 					for(Object item : items){
 						HashMap<String, Object> itemMap =  (HashMap<String, Object>) item;
 						RTSNotificationVO rtsNotificationVO = (RTSNotificationVO) mapper.convertValue(itemMap, RTSNotificationVO.class);
 						NotificationVO notificationVO=new NotificationVO();
 						notificationVO.setNotificationId(rtsNotificationVO.getNotificationId());
 						notificationVO.setNotification(rtsNotificationVO.getMessage());
 						notificationVO.setCreatedOn(Utility.formatDate("MM/dd/yyyy hh:mm:ss", "yyyy-MM-dd hh:mm:ss", rtsNotificationVO.getCreateDate()));
 						notificationVO.setLatitude(rtsNotificationVO.getLatitude());
 						notificationVO.setLongitude(rtsNotificationVO.getLongitude());
 						// RTS sends status = true if is closed and status = false if it is open
 						if(rtsNotificationVO.isStatus()){
 							notificationVO.setState(0);
 						}else{
 							notificationVO.setState(2);
 						}
 						rtsNotifications.add(notificationVO);
					}
 					if(!notificationResVOa.getShipmentNotifications().isEmpty() && notificationResVOa.getShipmentNotifications().size()>0)
 					{
 						for(ShipmentNotifications notifications:notificationResVOa.getShipmentNotifications())
 						{
 							rtsNotifications.addAll(notifications.getNotifications());
 						}
 						Collections.sort(rtsNotifications,new SortByDate());
 					}
				}
			}
			ArrayList<DeviceInfoDetails> shippmentList=new ArrayList<>();
			com.rfxcel.sensor.beans.Map map = new com.rfxcel.sensor.beans.Map();
			for(NotificationVO alertvo: rtsNotifications)
			{
				DeviceInfoDetails detail=new DeviceInfoDetails();
				String productId = alertvo.getNotification();				
				String latitude = alertvo.getLatitude();
				String longitude = alertvo.getLongitude();
				String createdOn = alertvo.getCreatedOn();
				String utcCreatedOn = createdOn != null ? String.valueOf(Utility.getUTCDate(createdOn)) : "0";
				Integer state = alertvo.getState();
				detail.setProductId(productId);
				detail.setCreatedOn(utcCreatedOn);
				detail.setLatitude(latitude);
				detail.setLongitude(longitude);
				detail.setPairStatus(state);
				shippmentList.add(detail);				
			}
			NotificationVO alertvo = rtsNotifications.get(0);
			//adding map data to show lat, lon
			Location location  = new Location();
			location.setProductId(alertvo.getNotification());
			location.setDateTime(Utility.getUTCDate(alertvo.getCreatedOn()));
			location.setLat(alertvo.getLatitude());
			location.setLang(alertvo.getLongitude());
			location.setPairStatus(alertvo.getState());
			map.getLocations().add(location);
			
			sensor.setShipmentList(shippmentList);
			sensor.setMap(map);
			response.setSensor(sensor);
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in ItemController :: getMergeAlerts " + e.getMessage());
		}
		return response;
	}
		
	private static final class SortByDate implements Comparator<NotificationVO>
	{
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
	    public int compare(NotificationVO a, NotificationVO b)
	    {
	    	  Date d1 = null;
	          Date d2 = null;
	          try {
	              d1 = sdf.parse(a.getCreatedOn());
	              d2 = sdf.parse(b.getCreatedOn());
	          } catch (ParseException e) {
	              // TODO Auto-generated catch block
	              e.printStackTrace();
	          }
	          return (d1.getTime() > d2.getTime() ? -1 : 1);
	    }
	}


	public String getItemTraceId(Integer orgId, String productId, String packageId) {
		String traceId = null;
		try {
			logger.info("getItemTraceId " + orgId + ", " + productId +", " + packageId);
			
			String[] gtin = productId.split("-");
			String sscc = ConfigCache.getInstance().getSystemConfig("rts.application.sscc") + gtin[0] + "." + packageId;
			//String bodyText = "{\r\n\t\"requestObject\": {\r\n\t\t\"traceIdCondition\": \"0\",\r\n\t\t\"traceIdValue\": \"\",\r\n\t\t\"itemTypeCondition\": \"0\",\r\n\t\t\"itemTypeValue\": \"\",\r\n\t\t\"itemLocationCondition\": \"0\",\r\n\t\t\"itemLocationValue\": \"\",\r\n\t\t\"lotIdCondition\": \"0\",\r\n\t\t\"lotIdValue\": \"\",\r\n\t\t\"mfrNameCondition\": \"0\",\r\n\t\t\"mfrNameValue\": \"\",\r\n\t\t\"itemIdCondition\": \"\",\r\n\t\t\"itemIdValue\": \"\",\r\n\t\t\"itemNameCondition\": \"0\",\r\n\t\t\"itemNameValue\": \"\",\r\n\t\t\"dispositionCondition\": \"0\",\r\n\t\t\"dispositionValue\": \"\",\r\n\t\t\"expDateCondition\": \"0\",\r\n\t\t\"expDateValue1\": \"\",\r\n\t\t\"expDateValue2\": \"\",\r\n\t\t\"limit\": 500,\r\n\t\t\"offset\": 0\r\n\t}\r\n}";
			String bodyText = "{\"requestObject\":{\"traceIdCondition\":\"0\",\"traceIdValue\":\"" + sscc + "\",\"itemTypeCondition\":\"0\",\"itemTypeValue\":\"\",\"itemLocationCondition\":\"0\",\"itemLocationValue\":\"\",\"lotIdCondition\":\"0\",\"lotIdValue\":\"\",\"mfrNameCondition\":\"0\",\"mfrNameValue\":\"\",\"itemIdCondition\":\"\",\"itemIdValue\":\"\",\"itemNameCondition\":\"0\",\"itemNameValue\":\"\",\"gTinCondition\":\"0\",\"gTinValue\":\"\",\"dispositionCondition\":\"0\",\"dispositionValue\":\"\",\"expDateCondition\":\"0\",\"expDateValue1\":\"\",\"expDateValue2\":\"\",\"limit\":\"75\",\"offset\":0}}";
			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getItemListURL = rtsAppUrl + "item/getItemList";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId);
			String jsonText = http.doPost(getItemListURL, "Authorization", authToken, bodyText);
			
			if (jsonText != null && jsonText.trim().length() != 0) {
				if (jsonText.contains("error")) {
					
					return null;
				}
				
				JSONObject jsonObject = new JSONObject(jsonText);
				ObjectMapper mapper = new ObjectMapper();
				if(jsonObject.has("data")){
					JSONObject dataObject = jsonObject.getJSONObject("data");
					JSONArray pageItems = dataObject.getJSONArray("pageItems");
					Object[] items = mapper.readValue(pageItems.toString(), Object[].class);
					for(Object item : items){
						HashMap<String, String> itemMap =  (HashMap<String, String>) item;
						ItemListData itemListData = (ItemListData) mapper.convertValue(itemMap, ItemListData.class);
						
						/*DeviceInfoDetails deviceInfoDetails = new DeviceInfoDetails();
						String serialId = itemListData.getTraceEntExtId().substring(itemListData.getTraceEntExtId().lastIndexOf(".") + 1); //urn:epc:id:sscc:0364896.5000033500 traceId - 5000033500 //itemListData.getItemName();
						String productId = String.valueOf(itemListData.getItemId());
						String deviceId = itemListData.getLotId(); //itemListData.getTraceEntId();
						*/
						traceId = itemListData.getTraceEntId();
						logger.info("getItemTraceId " + traceId);
						
						/*
						deviceInfoDetails.setProductId(productId);
						deviceInfoDetails.setPackageId(packageId);
						deviceInfoDetails.setTraceId(traceId);
						deviceInfoDetails.setDeviceId(deviceId);
						
						Location location = new Location(deviceId, packageId, productId);
						location.setLat(itemListData.getLatitude());
						location.setLang(itemListData.getLongitude());
						// do not add locations to the response if lat and lang values are empty
						if(location.getLat() != null &&  location.getLang() != null) {
							//map.getLocations().add(location);	
						}
						*/
					}
				}		
			}
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error in ItemController :: getItemTraceId " + e.getMessage());
		}
		return traceId;
	}
}
