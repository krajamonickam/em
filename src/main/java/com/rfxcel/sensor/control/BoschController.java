package com.rfxcel.sensor.control;

import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.codehaus.jackson.annotate.JsonIgnoreProperties;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.Response;


public class BoschController implements Runnable
{
	private static final Logger logger = Logger.getLogger(BoschController.class);
	public static BoschController instance = null;
	private ConfigCache configCache = ConfigCache.getInstance();
	private LinkedBlockingQueue<BoschRequest> queue = new LinkedBlockingQueue<BoschRequest>();
	private RESTClient http = null;
	private Thread thread = null;
	private String controlURL  = null;
	private String controlUser = null;
	private String controlPass = null;
	private String configURL  = null;
	private String configLogin  = null;
	private String configPassword  = null;
	
	public static class BoschRequest
	{
		public String deviceID;
		public String deviceKey;
		public BoschCmdMsg cmd;

		public BoschRequest(String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq) {
			this.deviceID = deviceID;
			this.deviceKey = deviceKey;
			this.cmd = new BoschCmdMsg(commFreq, gpsFreq, tempFreq);
		}

		public String getDeviceID() {
			return deviceID;
		}
		public void setDeviceID(String deviceID) {
			this.deviceID = deviceID;
		}
		public String getDeviceKey() {
			return deviceKey;
		}
		public void setDeviceKey(String deviceKey) {
			this.deviceKey = deviceKey;
		}
		public BoschCmdMsg getCmd() {
			return cmd;
		}
		public void setCmd(BoschCmdMsg cmd) {
			this.cmd = cmd;
		}
	}
	
	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class BoschCmdMsg
	{
		public String uplinterval;
//		public String autointerval;
		public String statusinterval;
		public String sendinterval;
//		public String alarmcheckinterval;
//		public String locationcheckinterval;
//		public String gpsjamming;
//		public String gpsonextra;
//		public String nomotionsleep;
//		public String shock;
//		public String accelconfig;
//		public String motionaccelsettings;
//		public String lowtemperature;
//		public String hightemperature;
//		public String lowbatterycap;
//		public String lowbatteryvoltage;
//		public String lowtempprobe;
//		public String hightempprobe;
//		public String lowpressure;
//		public String highpressure;
//		public String lowhumidity;
//		public String highhumidity;
//		public String hightilt;
//		public String highlight;
		
		public BoschCmdMsg() {
		}

		public BoschCmdMsg(int commFreq, int gpsFreq, int tempFreq) {
			super();
			this.sendinterval = (commFreq > 0) ? new Integer(commFreq).toString() : "STOP";
			this.uplinterval = (gpsFreq > 0) ? new Integer(gpsFreq).toString() : "STOP";
			this.statusinterval = (tempFreq > 0) ? new Integer(tempFreq).toString() : "STOP";
		}

		public String getUplinterval() {
			return uplinterval;
		}

		public void setUplinterval(String uplinterval) {
			this.uplinterval = uplinterval;
		}

		public String getStatusinterval() {
			return statusinterval;
		}

		public void setStatusinterval(String statusinterval) {
			this.statusinterval = statusinterval;
		}

		public String getSendinterval() {
			return sendinterval;
		}

		public void setSendinterval(String sendinterval) {
			this.sendinterval = sendinterval;
		}

		public static BoschCmdMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				BoschCmdMsg cmd = mapper.readValue(text, BoschCmdMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class SendumURLMsg
	{
		public String deviceidentifier;
		public String url;
		
		public SendumURLMsg() {
		}
		public SendumURLMsg(String deviceidentifier, String url) {
			super();
			this.deviceidentifier = deviceidentifier;
			this.url = url;
		}
		public String getUrl() {
			return url;
		}

		public void setUrl(String url) {
			this.url = url;
		}
		public String getDeviceidentifier() {
			return deviceidentifier;
		}
		public void setDeviceidentifier(String deviceidentifier) {
			this.deviceidentifier = deviceidentifier;
		}
		public static SendumURLMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				SendumURLMsg cmd = mapper.readValue(text, SendumURLMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	@JsonIgnoreProperties(ignoreUnknown = true)
	public static class SendumURLGetMsg
	{
		public String posturl;
		
		public SendumURLGetMsg() {
		}
		public SendumURLGetMsg(String deviceidentifier, String url) {
			super();
			this.posturl = url;
		}
		public String getPosturl() {
			return posturl;
		}
		public void setPosturl(String posturl) {
			this.posturl = posturl;
		}
		public static SendumURLGetMsg fromJson(String text) {
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				SendumURLGetMsg cmd = mapper.readValue(text, SendumURLGetMsg.class);
				return(cmd);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return(null);
		}
		public String toJson() {
			String jsonInString = null;
			try {
				ObjectMapper mapper = new ObjectMapper();
				mapper.setSerializationInclusion(Inclusion.NON_NULL);
				jsonInString = mapper.writeValueAsString(this);
			} catch (Exception e) {
				e.printStackTrace();
			} 
			return jsonInString;
		}
	}

	public static void main(String[] args) {
		try {
			BoschController c = BoschController.getInstance();
			c.setupDevice(0, "99000512004189", "4305519628", 180, 360, 180);
		} catch (Exception e) {
		}
	}
	
	public static BoschController getInstance()
	{
		if (instance == null) {
			instance = new BoschController();
		}
		return(instance);
	}
	
	public BoschController()
	{
		controlURL  = configCache.getSystemConfig("sendum.controller.url");
		controlUser = configCache.getSystemConfig("sendum.controller.userid");
		controlPass = configCache.getSystemConfig("sendum.controller.password");
		configURL   = configCache.getSystemConfig("sendum.config.url");
		configLogin = configCache.getSystemConfig("sendum.user");
		configPassword = configCache.getSystemConfig("sendum.password");
		if (controlURL == null) {
			logger.error("BoschController - system config invalid value for sendum.controller.url");
			controlURL = "https://restapi.sendum.com/<verb>";
			controlUser = "arunrao@rfxcel.com";
			controlPass = "sendum";
		}
		if (configURL == null) {
			logger.error("BoschController - system config invalid value for sendum.config.url");
			configURL = "https://www.verizonitt.net/sendum";
			configURL = "https://ittbeta.track-n-trace.net/sendum";
		}
		if (configLogin == null) {
			logger.error("BoschController - system config invalid value for sendum.user");
			configLogin = "sendum";
		}
		if (configPassword == null) {
			logger.error("BoschController - system config invalid value for sendum.password");
			configPassword = "Sendum123";
		}
		http = new RESTClient(controlUser, controlPass);
		logger.info("BoschController - initialized with account = " + controlUser + " for URL " + controlURL);
	}
	
	public void setupDevice(int orgID, String deviceID, String deviceKey, int commFreq, int gpsFreq, int tempFreq)
	{
		synchronized(this) {
			if (thread == null) {
				logger.info("BoschController - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		BoschRequest req = new BoschRequest(deviceID, deviceKey, commFreq, gpsFreq, tempFreq);
		this.queue.add(req);
		logger.info("BoschController - queuing request " + req.toString());
	}
	
	private void doSetupDevice(String deviceID, String deviceKey, String commFreq, String gpsFreq, String tempFreq)
	{
		changeURL(deviceID);
		changeSettings(deviceID, commFreq, gpsFreq, tempFreq);
	}
	
	public void changeURL(String deviceID)
	{
		logger.info("BoschController - change URL");
	}
	
	public void changeSettings(String deviceID,  String commFreq, String gpsFreq, String tempFreq)
	{
		logger.info("BoschController - change settings");
	}
	

	@Override
	public void run() {
		try
		{
			BoschRequest req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("BoschCmdMsg - dequeuing request " + req.toString());
				this.doSetupDevice(req.getDeviceID(), 
						req.getDeviceKey(), 
						req.getCmd().getSendinterval(),
						req.getCmd().getUplinterval(),
						req.getCmd().getStatusinterval());
			}
		}
		catch(Exception e)
		{
			logger.info("BoschCmdMsg thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}

	/**
	 * 
	 * @param userId 
	 * @param device
	 * @return
	 */
	public com.rfxcel.sensor.vo.Response locateDevice(long userId, Device device) {
		com.rfxcel.sensor.vo.Response resp = new com.rfxcel.sensor.vo.Response();
		try
		{
			String deviceID = device.getDeviceId();
			String postURL = controlURL.replace("<verb>", "locatenow");
			postURL = postURL + "?deviceidentifier=" + deviceID;
			logger.info("BoschController.locateDevice - POST url " + postURL);
			http.doPost(postURL, "{}");
			resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Locate Now command sent successfully to the device");
			return(resp);
		}
		catch(Exception ex)
		{
			logger.equals("Exception: " + ex.getMessage());
		}
		resp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Locate device feature is not available for devices of type Sendum.");
		return(resp);
	}
}
