package com.rfxcel.sensor.control;

import java.sql.SQLException;
import java.util.ArrayList;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.dao.DeviceTypeDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.Response;

public class DeviceController {
	private static final Logger logger = Logger.getLogger(DeviceController.class);
	private static DeviceController instance = null;
	
	public static DeviceController getInstance()
	{
		if (instance == null) {
			instance = new DeviceController();
		}
		return(instance);
	}
	
	public DeviceController()
	{
	}

	public void applyDeviceProfile(int orgID,long userId, Device d, Profile p)
	{
		String manufacturer = getDeviceManufacturer(orgID, d);
		if (manufacturer == null) {
			logger.info("DeviceController:applyDeviceProfile() - unable to get manufacturer for device " + d.getDeviceId() + " type " + d.getDeviceType() +  " org " + d.getOrgId());
			DeviceLogDAO.doAddDeviceLog(d.getDeviceId(), userId, "Please contact support - unable to get manufacturer for deviceType " + d.getDeviceType(), d.getOrgId());
			return;
		}
		manufacturer = manufacturer.toLowerCase();
		if (manufacturer.startsWith("kirsen")) {
			KirsenController k = KirsenController.getInstance();
			//k.setupDevice(orgID, d.getDeviceId(), d.getDeviceKey(), p.getCommFrequency(), p.getGpsFrequency(), p.getAttbFrequency());
			k.setupProfile(orgID, userId, d, p);
			return;
		}
		if (manufacturer.startsWith("queclink")) {
			QueclinkController q = QueclinkController.getInstance();
			q.setupDevice(orgID, d.getDeviceId(), d.getDeviceKey(), p.getCommFrequency(), p.getGpsFrequency(), p.getAttbFrequency());
			return;
		}
		if (manufacturer.startsWith("sendum")) {
			SendumController s = SendumController.getInstance();
			s.setupDevice(orgID, d.getDeviceId(), d.getDeviceKey(), p.getCommFrequency(), p.getGpsFrequency(), p.getAttbFrequency(), p);
			return;
		}
		if (manufacturer.startsWith("verizon")) {
			ThingSpaceController s = ThingSpaceController.getInstance();
			s.setupDevice(orgID, d.getDeviceId(), d.getDeviceKey(),p.getCommFrequency(), p.getGpsFrequency(), p.getAttbFrequency(), p.getAttributes(), p);
			return;
		}
		if (manufacturer.startsWith("bosch")) {
			BoschController s = BoschController.getInstance();
			s.setupDevice(orgID, d.getDeviceId(), d.getDeviceKey(), p.getCommFrequency(), p.getGpsFrequency(), p.getAttbFrequency());
			return;
		}
		logger.info("DeviceController - unknown manufacturer " + manufacturer + " for device " + d.getDeviceId() + " type " + d.getDeviceType() +  " org " + d.getOrgId());
	}
	
	public String getDeviceManufacturer(int orgID, Device device)
	{
		DeviceTypeDAO dao = DeviceTypeDAO.getInstance();
		try {
			ArrayList<DeviceDetail> dd = dao.getDeviceTypes(orgID);
			for (DeviceDetail devType : dd) {
				if (devType.getDeviceTypeId().intValue() == device.getDeviceTypeId().intValue()) {
					return(devType.getManufacturer());
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(null);
	}
	
	
	/**
	 * 
	 * @param orgId
	 * @param device
	 * @return
	 */
	public Response locateDevice(int orgId, long userId, Device device){
		Response resp = new Response();
		String manufacturer = getDeviceManufacturer(orgId, device);
		if (manufacturer == null) {
			logger.info("DeviceController: locateDevice() - unable to get manufacturer for device " + device.getDeviceId() + " type " + device.getDeviceType() +  " org " + orgId);
			DeviceLogDAO.doAddDeviceLog(device.getDeviceId(), userId, "Please contact support - unable to get manufacturer for deviceType " + device.getDeviceType(), orgId);
			resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(" unable to get manufacturer for device "+device.getDeviceId());
			return resp;
		}
		manufacturer = manufacturer.toLowerCase();
		if (manufacturer.startsWith("kirsen")) {
			KirsenController k = KirsenController.getInstance();
			return k.locateDevice(userId, device);
		}
		if (manufacturer.startsWith("queclink")) {
			QueclinkController q = QueclinkController.getInstance();
			return q.locateDevice(userId, device);
		}
		if (manufacturer.startsWith("sendum")) {
			SendumController s = SendumController.getInstance();
			return s.locateDevice(userId, device);
		}
		logger.info("DeviceController - unknown manufacturer " + manufacturer + " for device " + device.getDeviceId() + " type " + device.getDeviceType() +  " org " + device.getOrgId());
		return resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Unknown manufacturer " + manufacturer + " for device " + device.getDeviceId());
	}
		
	
}
