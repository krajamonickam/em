package com.rfxcel.sensor.control;


import java.math.BigInteger;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;
import org.json.JSONArray;
import org.json.JSONObject;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.thingspace.util.ThingSpaceUtils;
import com.rfxcel.sensor.util.Utility;

/**
 * @author Tejshree Kachare
 */
public class VerizonDeviceActivator implements Runnable{
	
	private static Logger logger = Logger.getLogger(VerizonDeviceActivator.class);
	private static VerizonDeviceActivator instance = null;
	private Thread thread = null;
	private LinkedBlockingQueue<DeviceDetail> queue = new LinkedBlockingQueue<DeviceDetail>();
	
	private VerizonDeviceActivator(){}

	public static VerizonDeviceActivator getInstance(){
		synchronized (VerizonDeviceActivator.class) {
			if(instance == null){
				instance = new VerizonDeviceActivator();
			}
		}
		return instance;
	}

	@Override
	public void run() {
		try	{
			DeviceDetail req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("Verizon Device Activator - dequeuing request " + req.toString());
				Integer orgId = req.getOrgId();
				String key = null;
				String deviceId = req.getDeviceId();
				key = registerVerizonDevice(orgId, req);
				Boolean deviceAlreadyRegistered = false;
				if (key == null) {
					deviceAlreadyRegistered = true;
					key = findVerizonDevice(orgId, deviceId);
				}
				if (key != null) {
					req.setDeviceKey(key);
					req.setOrgId(orgId);
					// update device with device key 
					SendumDAO.getInstance().updateDeviceKey(deviceId, key);
					if(!deviceAlreadyRegistered){
						this.activateVerizonDevice(	req.getOrgId(),req.getDeviceId(),req.getDeviceKey());
					}
					// update device active state and activation status
					SendumDAO.getInstance().changeActivationStateOfDevice(req.getDeviceId());
				}
			}
		}
		catch(Exception e){
			e.printStackTrace();
			logger.info("Verizon Device Activator thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}

	
	public void activateDevice(DeviceDetail deviceDetail){
		synchronized(this) {
			if (thread == null) {
				logger.info("Verizon Device Activator - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		this.queue.add(deviceDetail);
	}
	
	
	private boolean activateVerizonDevice(Integer orgId, String deviceId, String key){
		String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
		if (authToken == null) {
			logger.error("Failed to get authToken for deviceID " + deviceId);
			return(false);
		}
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		url = url + "/" + key + "/actions/activate";
		String preCommand = "{\"force\":true}";
		RESTClient httpClient = new RESTClient(null, null);
		String response = httpClient.doPost(url, "Authorization", authToken, preCommand);
		if (response == null) {
			logger.error("Failed to activate deviceKey " + key + " url " + url + " token " + authToken);
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.activateVerizonDevice - Failed to activate deviceKey " + key + " url " + url + " token " + authToken);			
			return(false);
		}
		JSONObject obj = new JSONObject(response);
		if (obj.optString("error", null) != null) {
			String errorMsg = obj.getString("error_description");
			logger.error("Failed to activate deviceKey " + key + " url " + url + " token " + authToken + " error " + errorMsg);
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.activateVerizonDevice - Failed to activate deviceKey " + key + " url " + url + " token " + authToken + " error " + errorMsg);			
			return(false);
		}
		ThingSpaceUtils.logMessage("VerizonDeviceActivator.activateVerizonDevice - url = " + url + " response = " + response);			
		return(true);
	}	
	
	/**
	 * 
	 * @param orgId
	 * @param deviceDetail
	 * @return
	 */
	private String registerVerizonDevice(Integer orgId, DeviceDetail deviceDetail){
		String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
		if (authToken == null) {
			logger.error("Failed to get authToken for deviceID " + deviceDetail.getDeviceId());
			return(null);
		}
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		String deviceType = ConfigCache.getInstance().getSystemConfig("thingspace.device.type");
		String tsProviderID = ConfigCache.getInstance().getSystemConfig("thingspace.device.provider");
		RESTClient httpClient = new RESTClient(null, null);
		JSONObject obj = new JSONObject();
		BigInteger imei = new BigInteger(deviceDetail.getDeviceId());
		obj.put("kind", deviceType);
		obj.put("version", "1.0");
		obj.put("name", "ITT Device");
		obj.put("imei", imei);				   
		obj.put("qrcode", deviceDetail.getDeviceId());				   
		obj.put("providerid", tsProviderID);				   
		String jsonMsg = obj.toString();
		String response = httpClient.doPost(url, "Authorization", authToken, jsonMsg);
		if (response == null) {
			logger.error("Failed to get deviceKey from ThingSpace for url " + url + " token " + authToken + " request " + jsonMsg);
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.registerVerizonDevice - Failed to register device url " + url + " token " + authToken + " request " + jsonMsg);			
			return(null);
		}
		ThingSpaceUtils.logMessage("VerizonDeviceActivator.registerVerizonDevice - url = " + url + " request = " + jsonMsg + " response = " + response);			
		Map<String, String> map = Utility.convertJsonToMap(response);
		return(map.get("id"));
	}	


	/**
	 * 
	 * @param orgId
	 * @param deviceID
	 * @return
	 */
	private String findVerizonDevice(Integer orgId, String deviceID){
		try {
			String authToken = ConfigCache.getInstance().getOrgConfigUncached("thingspace.auth.token", orgId);
			if (authToken == null) {
				logger.error("Failed to get authToken for deviceID " + deviceID);
				return(null);
			}
			String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
			RESTClient httpClient = new RESTClient(null, null);
			String response = httpClient.doGet(url, "Authorization", authToken);
			if (response == null) {
				logger.error("Failed to get response from ThingSpace for url " + url + " token " + authToken);
				ThingSpaceUtils.logMessage("VerizonDeviceActivator.findVerizonDevice - Failed to get response from ThingSpace for url " + url + " token " + authToken);
				return(null);
			}
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.findVerizonDevice - url = " + url + " response = " + response);			
			JSONArray array = new JSONArray(response);
			for (int i = 0; i < array.length(); i++) {
				String text = array.get(i).toString();
				JSONObject o = new JSONObject(text);
				String id = o.getString("id");
				String imei = o.optString("imei");
				if ((imei != null) && (imei.equalsIgnoreCase(deviceID))) {
					return(id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Failed to findVerizonDevice orgId " + orgId + " device ID " + deviceID + " error " + e.getMessage());
			ThingSpaceUtils.logMessage("VerizonDeviceActivator.findVerizonDevice - failed orgId " + orgId + " device ID " + deviceID + " error " + e.getMessage());
		}
		return(null);
	}
}
