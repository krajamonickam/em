package com.rfxcel.sensor.queclink.receiver;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.queclink.message.FixedInfoReportMessage;
import com.rfxcel.sensor.queclink.message.GPSData;
import com.rfxcel.sensor.queclink.message.HeartbeatAckMessage;
import com.rfxcel.sensor.queclink.message.QueclinkMessage;
import com.rfxcel.sensor.queclink.message.QueclinkMessageFactory;
import com.rfxcel.sensor.queclink.message.QueclinkSensor;
import com.rfxcel.sensor.queclink.message.SOSMessage;
import com.rfxcel.sensor.queclink.message.ServerHeartbeatAckMessage;
import com.rfxcel.sensor.queclink.message.TemperatureReportMessage;
import com.rfxcel.sensor.queclink.util.QueclinkUtility;
import com.rfxcel.sensor.service.SensorProcessJSON;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;

public class QueclinkMsgProcessor implements Runnable {

	private static final Logger logger = Logger.getLogger(QueclinkMsgProcessor.class);
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static final SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private Thread thread = null;
	private Socket clientSocket;
	private final static AssociationCache assoCache = AssociationCache.getInstance();
	private static DeviceCache deviceCache = DeviceCache.getInstance();
	private final static SendumDAO sendumDao = SendumDAO.getInstance();
	/**
	 * 
	 * @param clientSocket
	 */
	public QueclinkMsgProcessor(Socket clientSocket){
		this.thread = new Thread(this);
		this.clientSocket = clientSocket;
		thread.start();
	}

	/**
	 * REad the data on socket and write it to log file and send it to the queue
	 * @param clientSocket
	 */
	public  void run() {
		PrintWriter pw = null;
		try {
			StringBuffer data = new StringBuffer(80);
			InputStreamReader inputStream  = new InputStreamReader(clientSocket.getInputStream());
			DataOutputStream os = new DataOutputStream(clientSocket.getOutputStream());

			try{
				String fileName = createFile();
				if(fileName == null){
					logger.error("Please check log file configurations for Queclink sensor");
				}else{
					pw = new PrintWriter(new FileWriter(fileName, true));// true to append data to same file
				}
			} catch(Exception e){
				logger.error("Exception while writing response to log file "+e.getMessage());
			}

			int c;
			char ch;
			int counter = 0;
			QueclinkMessage msg = null;
			for(; ; ){
				c = inputStream.read();
				if (c >= 0) {
					ch = (char) c;
					data.append(ch);	
					if (ch == '$') {
						try{
							msg = QueclinkMessageFactory.getInstance(data.toString());
						} catch(IllegalArgumentException e){
							logger.error("Exception while processing queclink response : " +e.getMessage());
							if(pw != null){
								pw.println(timeFormat.format(new Date()) + " :: Exception while processing queclink response " +e.getMessage());
							}
						}
						//If it is heartbeat ack, server needs to send back acknowledgement for the same
						try {
							if(msg != null){
								if(msg instanceof HeartbeatAckMessage){
									HeartbeatAckMessage hbtMsg = (HeartbeatAckMessage) msg;
									ServerHeartbeatAckMessage serverHbtMsg = new ServerHeartbeatAckMessage(hbtMsg.getProtocolVersion(), hbtMsg.getCountNumber());
									String json = 	serverHbtMsg.toJson();
									os.write(json.getBytes());
								}else if(msg instanceof TemperatureReportMessage){
									processTempRecordMessage(msg);
								}else if(msg instanceof FixedInfoReportMessage ){
									processGTFRIMessage(msg);
								}else if( msg instanceof SOSMessage){
									processSOSMessage(msg);
								}
								else{
									String sensorJson = msg.toSensorJson();
									SensorProcessJSON.getInstance().doQueue(sensorJson);
								}
							}
						} catch (Exception e) {
							logger.info(timeFormat.format(new Date()) + " EXCEPTION:  " + data);
							e.printStackTrace();
						}
						if(pw != null){
							pw.println(timeFormat.format(new Date()) + " ::  " + data);
						}
						logger.info(timeFormat.format(new Date()) + " ::  " + data);
						//	data.delete(0, data.length());
						data = new StringBuffer(80);
					}
				}
				else {
					try {
						Thread.sleep(1000L);
					} catch (Exception ex) {}
					counter++;
					if ((counter % 10) == 0) {	//Waiting for 10 sec, to see if additional input is received
						logger.info("Slept for " + counter + " seconds..."); 
						break; //breaking out of the infinite for loop
					}
				}
			}
		} catch (Exception e) {
			logger.error("Exception while processing queclink response : "+e.getMessage());
		}
		finally {
			try {
				logger.info("Closing socket connection");
				clientSocket.close();
				if(	pw != null){
					pw.flush();
					pw.close();
				}
			}
			catch (IOException e){/*close failed*/}
		}
	}

	/**
	 * GTFRI can contain multiple gps segments.
	 * So we need to create multiple QueclinkSensor data objects and put them to Queue for processing 
	 * @param msg
	 * @throws Exception
	 */
	private static void processGTFRIMessage(QueclinkMessage msg) throws Exception{
		FixedInfoReportMessage f = (FixedInfoReportMessage) msg;
		Date utcTime = simpleDateFormat.parse(QueclinkUtility.getDateFromUTC(f.getSendTime()));
		ArrayList<GPSData> gps = f.getGPS();
		int count = gps.size();
		String deviceId = f.getUniqueId();
		Boolean active = sendumDao.checkIfDeviceIsActive(deviceId);
		if(!active){
			logger.error("** processGTFRIMessage:: Device "+deviceId+" is not active *** " + deviceId);
			return;
		}
		String battery = f.getBatteryPercentage();
		QueclinkSensor sensor;
		for(int i = 0; i < count ; i++){
			GPSData g = gps.get(i);
			if (g!= null)
			{
				//process each temperature point
				sensor = new QueclinkSensor();
				String longitude = g.getLongitude();
				String latitude = g.getLatitude();
				sensor.setDeviceIdentifier(deviceId);
				sensor.setLatitude(latitude);
				sensor.setLongitude(longitude);
				Date gpsTime = simpleDateFormat.parse(QueclinkUtility.getDateFromUTC(g.getGpsUTCTimes()));
				sensor.setStatusTimeStamp(simpleDateFormat.format(gpsTime));
				if ((battery != null) && (battery.trim().length() > 0)) {
					sensor.setBattery(battery);					
				}
				String sensorJson = sensor.toJson();
				SensorProcessJSON.getInstance().doQueue(sensorJson);
			}
		}
	}
	
	/**
	 * 
	 * @param msg
	 * @throws Exception
	 */
	private static void processSOSMessage(QueclinkMessage msg) throws Exception{
		SOSMessage f = (SOSMessage) msg;
		ArrayList<GPSData> gps = f.getGPS();
		int count = gps.size();
		String deviceId = f.getUniqueId();
		Boolean active = sendumDao.checkIfDeviceIsActive(deviceId);
		
		if(!active){
			sendumDao.activateDevice(deviceId);
			logger.info("** processSOSMessage:: Activating device "+deviceId+" *** " + deviceId);
			Integer orgId = deviceCache.getDeviceOrgId(deviceId);
			DeviceLog deviceLog = new DeviceLog();
			deviceLog.setDeviceId(deviceId);
			deviceLog.setUserId(null);
			deviceLog.setOrgId(orgId);
			deviceLog.setLogMsg("GTSOS message received for device "+deviceId +". Activated the device. Content of message is: "+ f.toString() );
			DeviceLogDAO.getInstance().addDeviceLog(deviceLog);
			
		}
		String battery = f.getBatteryPercentage();
		QueclinkSensor sensor;
		for(int i = 0; i < count ; i++){
			GPSData g = gps.get(i);
			if (g!= null)
			{
				//process each temperature point
				sensor = new QueclinkSensor();
				String longitude = g.getLongitude();
				String latitude = g.getLatitude();
				sensor.setDeviceIdentifier(deviceId);
				sensor.setLatitude(latitude);
				sensor.setLongitude(longitude);
				Date gpsTime = simpleDateFormat.parse(QueclinkUtility.getDateFromUTC(g.getGpsUTCTimes()));
				sensor.setStatusTimeStamp(simpleDateFormat.format(gpsTime));
				if ((battery != null) && (battery.trim().length() > 0)) {
					sensor.setBattery(battery);					
				}
				String sensorJson = sensor.toJson();
				SensorProcessJSON.getInstance().doQueue(sensorJson);
			}
		}
	}
	
	
	
	/**
	 * TemperatureReportMessage contains multiple temperature reading and one GPS reading.
	 * So we need to create multiple QueclinkSensor data objects and put them to Queue for processing 
	 * @param msg
	 * @throws Exception
	 */
	private static void processTempRecordMessage(QueclinkMessage msg) throws Exception{
		TemperatureReportMessage trMsg = (TemperatureReportMessage) msg;
		int timeIntervalInSec = 60;

		Date utcTime = simpleDateFormat.parse(QueclinkUtility.getDateFromUTC(trMsg.getSendTime()));
		ArrayList<String> temperaturePoints = trMsg.getTemperaturePoints();
		int count = temperaturePoints.size();
		String longitude = trMsg.getLongitude();
		String latitude = trMsg.getLatitude();
		String deviceId = trMsg.getUniqueId();
		
		Boolean active = sendumDao.checkIfDeviceIsActive(deviceId);
		if(!active){
			logger.error("** processTempRecordMessage:: Device "+deviceId+" is not active *** " + deviceId);
			return;
		}
		
		String packageId = assoCache.getContainerId(deviceId);
		Integer profileId = assoCache.getActiveShipmentProfileId(deviceId, packageId);
		if(profileId != null && assoCache.getProfileTempFrequency(profileId) != null){
			timeIntervalInSec = assoCache.getProfileTempFrequency(profileId);
		}else{ // -- if attribute frequency is not configured for profile, use the config property defined in config table 
			String timeInterValProp = ConfigCache.getInstance().getSystemConfig("queclink.temp.report.interval.in.sec");
			if(timeInterValProp != null){
				timeIntervalInSec = Integer.valueOf(timeInterValProp);
			}
		}
		String temp;
		QueclinkSensor sensor;
		for(int i = 0; i < count ; ){
			temp = temperaturePoints.get(i);
			//If temp is not null or empty, process the record, else increment the counter. If its the last data point. then process lat and lang information
			if(temp!= null && temp.trim().length()!=0){
				//process each temperature point
				sensor = new QueclinkSensor();
				sensor.setTemperature(temp);
				sensor.setDeviceIdentifier(deviceId);

				//calculate the time to be set in
				Calendar cal = Calendar.getInstance();
				cal.setTime(utcTime);
				cal.add(Calendar.SECOND, - (timeIntervalInSec *(count- i)));
				Date statusDate = cal.getTime();
				sensor.setStatusTimeStamp(simpleDateFormat.format(statusDate));
				//increment the counter
				i++ ;
				// If its the last temperature point, insert latitude and longitude values to sensor
				if(i == count){
					//set lat and lang values as well since this is the last record
					if(latitude!= null && latitude.trim().length()!=0){
						sensor.setLatitude(latitude);
					}
					if(longitude!=null  && longitude.trim().length()!=0){
						sensor.setLongitude(longitude);
					}
				}
				String battery = trMsg.getBatteryPercent();
				if ((battery != null) && (battery.trim().length() > 0)) {
					sensor.setBattery(battery);					
				}
				String sensorJson = sensor.toJson();
				SensorProcessJSON.getInstance().doQueue(sensorJson);
			}else{
				i++;
				if(i == count && ((latitude!= null && latitude.trim().length()!=0) || (longitude!=null  && longitude.trim().length()!=0))){
					sensor = new QueclinkSensor();
					sensor.setDeviceIdentifier(deviceId);
					//set lat and lang values as well since this is the last record
					if(latitude!= null && latitude.trim().length()!=0){
						sensor.setLatitude(latitude);
					}
					sensor.setStatusTimeStamp(simpleDateFormat.format(utcTime));
					if(longitude!=null  && longitude.trim().length()!=0){
						sensor.setLongitude(longitude);
					}
					String sensorJson = sensor.toJson();
					SensorProcessJSON.getInstance().doQueue(sensorJson);
				}
			}
		}
	}


	/**
	 * Create directory to store the incoming data, if not already present
	 * @return
	 */
	private static String createFile()
	{
		String directory = ConfigCache.getInstance().getSystemConfig("queclink.logFolder");
		String fileName = ConfigCache.getInstance().getSystemConfig("queclink.logFile");
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				logger.info("Directory " + directory + " does not exist!");
				return(null);
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return(null);
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(dir, fileName+"-"+suffix + ".log");
		return(file.getAbsolutePath());
	}
}
