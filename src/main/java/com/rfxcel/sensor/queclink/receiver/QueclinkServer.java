package com.rfxcel.sensor.queclink.receiver;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;

/**
 * @author tejshree_kachare
 * This class is used to receive sensor data from Queclink terminal
 */

public class QueclinkServer implements Runnable{

	private static final Logger logger = Logger.getLogger(QueclinkServer.class);
	private static QueclinkServer instance =null;
	private Thread thread = null;
	private static Properties prop = new Properties();

	public static Properties getProp() {
		return prop;
	}

	public void setProp(Properties prop) {
		QueclinkServer.prop = prop;
	}

	private QueclinkServer() {
		this.thread = new Thread(this);
		this.thread.start();
	}

	public static QueclinkServer getInstance(){
		synchronized (QueclinkServer.class) {
			if(instance == null){
				instance = new QueclinkServer();
			}
		}
		return instance;
	}

	/**
	 * Start the Queclink server
	 */
	public void run () 
	{
		logger.info("Starting TCP server ");
		ServerSocket serverSocket = null;
		try{ 
			String port = ConfigCache.getInstance().getSystemConfig("queclink.tcpport");
			int serverPort = Integer.valueOf(port); 
			serverSocket = new ServerSocket(serverPort);
			logger.info("TCP Server is Listening...");

			while(true) { 
				Socket clientSocket = serverSocket.accept(); 
				logger.info("Received new connection");
				QueclinkMsgProcessor processor = new QueclinkMsgProcessor(clientSocket);
			} 
		} catch( NumberFormatException nfe){
			logger.error("Invalid TCP port number, Please check the properties file :"+ nfe.getMessage());
		}
		catch(IOException e) {
			logger.error("IOException while creating Server Socket : "+e.getMessage());
		} 
		finally{
			if(serverSocket != null) 
				try {
					serverSocket.close();
				} 
			catch (IOException e) {/*close failed*/}

		}
	}
}


