package com.rfxcel.sensor.queclink.message;


/**
 * @author Tejshree Kachare
 * @since 12-Oct-2016
 *
 */
public abstract class QueclinkMessage {

	protected String messageType; 
	/**
	 * Needs to be implemented 
	 * @return
	 */
	/*public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonInString = mapper.writeValueAsString(this);
		} 
		catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		// TODO Auto-generated method stub
		return jsonInString;
	}*/
	
	public abstract String toSensorJson();
}
