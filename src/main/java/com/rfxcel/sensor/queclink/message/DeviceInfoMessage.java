package com.rfxcel.sensor.queclink.message;

import com.rfxcel.sensor.queclink.util.QueclinkUtility;


/**
 * 
 * @author tejshree_kachare
 * @since 12 Oct 2016
 * <p>Device Information report Ack Message</p>
 * <p> String message "+RESP:GTINF" </p>

 */


public class DeviceInfoMessage extends QueclinkMessage {


	// number of parameters present in this message
	private static final int parameterCount = 25;


	public DeviceInfoMessage(String message){
		String [] parameters = message.split(",");
		if(parameters.length != parameterCount){
			throw new IllegalArgumentException("Number of parameters expected in +RESP:GTINF message is "+parameterCount );
		}
		this.protocolVersion = parameters[1];
		this.uniqueId = parameters[2];
		this.deviceName = parameters[3];
		this.state = parameters[4];
		this.iccid = parameters[5];
		this.csqRSSI = parameters[6];
		this.csqBER = parameters[7];
		this.externalPowerSupply = parameters[8];
		this.mileage = parameters[9];
		//parameter 10th is reserved
		this.batteryVoltage = parameters[11];
		this.charging = parameters[12];
		this.ledOn = parameters[13];
		this.gpsOnNeed = parameters[14];
		//15 and 16 are reserved
		this.lastGpsFixUtcTime = parameters[17];
		this.batteryPercentage = parameters[18];
		//19 is reserved
		this.temperature = parameters[20];
		//21 and 22 are reserved
		this.sendTime = parameters[23];
		this.countNumber = parameters[24]; //TODO remove the tail character
	}

	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String state;
	private String iccid;
	private String csqRSSI;
	private String csqBER;
	private String externalPowerSupply;
	private String mileage;
	private String reserved;
	private String batteryVoltage;
	private String charging;
	private String ledOn;
	private String gpsOnNeed;
	private String lastGpsFixUtcTime;
	private String batteryPercentage;
	private String temperature;
	private String sendTime;
	private String countNumber;


	/**
	 * @return the parametercount
	 */
	public static int getParametercount() {
		return parameterCount;
	}
	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}
	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @return the iccid
	 */
	public String getIccid() {
		return iccid;
	}
	/**
	 * @return the csqRSSI
	 */
	public String getCsqRSSI() {
		return csqRSSI;
	}
	/**
	 * @return the csqBER
	 */
	public String getCsqBER() {
		return csqBER;
	}
	/**
	 * @return the externalPowerSupply
	 */
	public String getExternalPowerSupply() {
		return externalPowerSupply;
	}
	/**
	 * @return the mileage
	 */
	public String getMileage() {
		return mileage;
	}
	/**
	 * @return the reserved
	 */
	public String getReserved() {
		return reserved;
	}
	/**
	 * @return the batteryVoltage
	 */
	public String getBatteryVoltage() {
		return batteryVoltage;
	}
	/**
	 * @return the charging
	 */
	public String getCharging() {
		return charging;
	}
	/**
	 * @return the ledOn
	 */
	public String getLedOn() {
		return ledOn;
	}
	/**
	 * @return the gpsOnNeed
	 */
	public String getGpsOnNeed() {
		return gpsOnNeed;
	}
	/**
	 * @return the lastGpsFixUtcTime
	 */
	public String getLastGpsFixUtcTime() {
		return lastGpsFixUtcTime;
	}
	/**
	 * @return the batteryPercentage
	 */
	public String getBatteryPercentage() {
		return batteryPercentage;
	}
	/**
	 * @return the temperature
	 */
	public String getTemperature() {
		return temperature;
	}
	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}
	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}


	@Override
	public String toSensorJson() {
		String jsonInString = null;
		QueclinkSensor sensor = new QueclinkSensor();
		//sensor.setDeviceName(this.getDeviceName());
		sensor.setDeviceIdentifier(this.getUniqueId());
		sensor.setTemperature(this.getTemperature());
		sensor.setStatusTimeStamp(QueclinkUtility.getDateFromUTC(this.getSendTime()));
		sensor.setBattery(this.getBatteryPercentage());
		jsonInString = sensor.toJson();

		return jsonInString;
	}
}
