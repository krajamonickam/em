package com.rfxcel.sensor.queclink.message;

import java.util.ArrayList;


/**
 * 
 * @author tejshree Kachare
 *
 */

public class TemperatureReportMessage extends QueclinkMessage{

	// minimum number of parameters present in this message
	private static final int parameterCount = 24;
	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String reportNumber;
	private ArrayList<String> temperaturePoints = new ArrayList<String>();
	private String gpsLocation;
	private String gpsAccuracy;
	private String speed;
	private String azimuth;
	private String altitude;
	private String longitude;
	private String latitude;
	private String gpsUTCTimes;
	private String mcc;
	private String mnc;
	private String sid;
	private String bid;
	private String batteryPercent;
	private String sendTime;
	private String countNumber;

	/**
	 * Since the number of parameters are not fixed for FixedInfoReport, will not initialize object with parameters after ioStatus, as additional enabled fields will be added after it.
	 * @param message
	 */
	public TemperatureReportMessage(String message){
		String [] parameters = message.split(",");
		if(parameters.length < parameterCount){
			throw new IllegalArgumentException("Number of parameters expected in +RESP:GTTPR or +BUFF:GTTPR is greater than or equal to "+parameterCount );
		}
		this.protocolVersion = parameters[1];
		this.uniqueId = parameters[2];
		this.deviceName = parameters[3];
		this.reportNumber = parameters[4];
		if(this.reportNumber==null || this.reportNumber.trim().length()==0){
			throw new IllegalArgumentException("reportNumber in +RESP:GTTPR or +BUFF:GTTPR cannot be null or empty");
		}
		int count = Integer.valueOf(this.reportNumber);
		for(int i =0; i< count; i++){
			temperaturePoints.add(parameters[5+i]);
		}
		this.gpsLocation = parameters[5+count];
		this.gpsAccuracy = parameters[6+count];
		this.speed = parameters[7+count];
		this.azimuth = parameters[8+count];
		this.altitude = parameters[9+count];
		this.longitude = parameters[10+count];
		this.latitude = parameters[11+count];
		this.gpsUTCTimes = parameters[12+count];
		if(this.gpsUTCTimes == null || this.gpsUTCTimes.trim().length()==0){
			throw new IllegalArgumentException("gpsUTCTimes in +RESP:GTTPR or +BUFF:GTTPR cannot be null or empty");
		}
		this.mcc = parameters[13+count];
		this.mnc = parameters[14+count];
		this.sid = parameters[15+count];
		this.bid = parameters[16+count];
		this.batteryPercent = parameters[17+count];
		this.sendTime = parameters[21+count];
		this.countNumber = parameters[22+count];
	}

	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @param protocolVersion the protocolVersion to set
	 */
	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}

	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}

	/**
	 * @param uniqueId the uniqueId to set
	 */
	public void setUniqueId(String uniqueId) {
		this.uniqueId = uniqueId;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @param deviceName the deviceName to set
	 */
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}

	/**
	 * @return the reportNumber
	 */
	public String getReportNumber() {
		return reportNumber;
	}

	/**
	 * @param reportNumber the reportNumber to set
	 */
	public void setReportNumber(String reportNumber) {
		this.reportNumber = reportNumber;
	}

	/**
	 * @return the temperaturePoints
	 */
	public ArrayList<String> getTemperaturePoints() {
		return temperaturePoints;
	}

	/**
	 * @param temperaturePoints the temperaturePoints to set
	 */
	public void setTemperaturePoints(ArrayList<String> temperaturePoints) {
		this.temperaturePoints = temperaturePoints;
	}

	/**
	 * @return the gpsLocation
	 */
	public String getGpsLocation() {
		return gpsLocation;
	}

	/**
	 * @param gpsLocation the gpsLocation to set
	 */
	public void setGpsLocation(String gpsLocation) {
		this.gpsLocation = gpsLocation;
	}

	/**
	 * @return the gpsAccuracy
	 */
	public String getGpsAccuracy() {
		return gpsAccuracy;
	}

	/**
	 * @param gpsAccuracy the gpsAccuracy to set
	 */
	public void setGpsAccuracy(String gpsAccuracy) {
		this.gpsAccuracy = gpsAccuracy;
	}

	/**
	 * @return the speed
	 */
	public String getSpeed() {
		return speed;
	}

	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(String speed) {
		this.speed = speed;
	}

	/**
	 * @return the azimuth
	 */
	public String getAzimuth() {
		return azimuth;
	}

	/**
	 * @param azimuth the azimuth to set
	 */
	public void setAzimuth(String azimuth) {
		this.azimuth = azimuth;
	}

	/**
	 * @return the altitude
	 */
	public String getAltitude() {
		return altitude;
	}

	/**
	 * @param altitude the altitude to set
	 */
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	/**
	 * @return the gpsUTCTimes
	 */
	public String getGpsUTCTimes() {
		return gpsUTCTimes;
	}

	/**
	 * @param gpsUTCTimes the gpsUTCTimes to set
	 */
	public void setGpsUTCTimes(String gpsUTCTimes) {
		this.gpsUTCTimes = gpsUTCTimes;
	}

	/**
	 * @return the mcc
	 */
	public String getMcc() {
		return mcc;
	}

	/**
	 * @param mcc the mcc to set
	 */
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}

	/**
	 * @return the mnc
	 */
	public String getMnc() {
		return mnc;
	}

	/**
	 * @param mnc the mnc to set
	 */
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}

	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}

	/**
	 * @param sid the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}

	/**
	 * @return the bid
	 */
	public String getBid() {
		return bid;
	}

	/**
	 * @param bid the bid to set
	 */
	public void setBid(String bid) {
		this.bid = bid;
	}

	/**
	 * @return the battery percent value
	 */
	public String getBatteryPercent() {
		return batteryPercent;
	}

	/**
	 * @param batteryPercent the battery percent value to set
	 */
	public void setBatteryPercent(String batteryPercent) {
		this.batteryPercent = batteryPercent;
	}

	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @param sendTime the sendTime to set
	 */
	public void setSendTime(String sendTime) {
		this.sendTime = sendTime;
	}

	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}

	/**
	 * @param countNumber the countNumber to set
	 */
	public void setCountNumber(String countNumber) {
		this.countNumber = countNumber;
	}

	/**
	 * @return the parametercount
	 */
	public static int getParameterCount() {
		return parameterCount;
	}

	@Override
	public String toSensorJson() {
		return null;
	}

}
