package com.rfxcel.sensor.queclink.message;


public class GPSData
{
	private String gpsAccuracy;
	private String speed;
	private String azimuth;
	private String altitude;
	private String longitude;
	private String latitude;
	private String gpsUTCTimes;
	private String mcc;
	private String mnc;
	private String sid;
	private String bid;
	private String odoMileage;
	/**
	 * @return the gpsAccuracy
	 */
	public String getGpsAccuracy() {
		return gpsAccuracy;
	}
	/**
	 * @param gpsAccuracy the gpsAccuracy to set
	 */
	public void setGpsAccuracy(String gpsAccuracy) {
		this.gpsAccuracy = gpsAccuracy;
	}
	/**
	 * @return the speed
	 */
	public String getSpeed() {
		return speed;
	}
	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(String speed) {
		this.speed = speed;
	}
	/**
	 * @return the azimuth
	 */
	public String getAzimuth() {
		return azimuth;
	}
	/**
	 * @param azimuth the azimuth to set
	 */
	public void setAzimuth(String azimuth) {
		this.azimuth = azimuth;
	}
	/**
	 * @return the altitude
	 */
	public String getAltitude() {
		return altitude;
	}
	/**
	 * @param altitude the altitude to set
	 */
	public void setAltitude(String altitude) {
		this.altitude = altitude;
	}
	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the gpsUTCTimes
	 */
	public String getGpsUTCTimes() {
		return gpsUTCTimes;
	}
	/**
	 * @param gpsUTCTimes the gpsUTCTimes to set
	 */
	public void setGpsUTCTimes(String gpsUTCTimes) {
		this.gpsUTCTimes = gpsUTCTimes;
	}
	/**
	 * @return the mcc
	 */
	public String getMcc() {
		return mcc;
	}
	/**
	 * @param mcc the mcc to set
	 */
	public void setMcc(String mcc) {
		this.mcc = mcc;
	}
	/**
	 * @return the mnc
	 */
	public String getMnc() {
		return mnc;
	}
	/**
	 * @param mnc the mnc to set
	 */
	public void setMnc(String mnc) {
		this.mnc = mnc;
	}
	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}
	/**
	 * @param sid the sid to set
	 */
	public void setSid(String sid) {
		this.sid = sid;
	}
	/**
	 * @return the bid
	 */
	public String getBid() {
		return bid;
	}
	/**
	 * @param bid the bid to set
	 */
	public void setBid(String bid) {
		this.bid = bid;
	}
	/**
	 * @return the odoMileage
	 */
	public String getOdoMileage() {
		return odoMileage;
	}
	/**
	 * @param odoMileage the odoMileage to set
	 */
	public void setOdoMileage(String odoMileage) {
		this.odoMileage = odoMileage;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "GPSData [" + (gpsAccuracy != null ? "gpsAccuracy=" + gpsAccuracy + ", " : "")
				+ (speed != null ? "speed=" + speed + ", " : "") + (azimuth != null ? "azimuth=" + azimuth + ", " : "")
				+ (altitude != null ? "altitude=" + altitude + ", " : "")
				+ (longitude != null ? "longitude=" + longitude + ", " : "")
				+ (latitude != null ? "latitude=" + latitude + ", " : "")
				+ (gpsUTCTimes != null ? "gpsUTCTimes=" + gpsUTCTimes + ", " : "")
				+ (mcc != null ? "mcc=" + mcc + ", " : "") + (mnc != null ? "mnc=" + mnc + ", " : "")
				+ (sid != null ? "sid=" + sid + ", " : "") + (bid != null ? "bid=" + bid + ", " : "")
				+ (odoMileage != null ? "odoMileage=" + odoMileage : "") + "]";
	}
	
	
	
}