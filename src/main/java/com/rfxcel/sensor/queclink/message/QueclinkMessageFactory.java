package com.rfxcel.sensor.queclink.message;

import com.rfxcel.sensor.queclink.util.QueclinkConstant;

/**
 * @author Tejshree Kachare
 * @since 12 Oct 2016
 */

public class QueclinkMessageFactory {

	private QueclinkMessageFactory() {
		// TODO Auto-generated constructor stub
	}
	public static QueclinkMessage getInstance(String message){

		if(message == null || message.trim().length() == 0 ){
			return null;
		}	//Removing the tailing character
		if(message.contains(QueclinkConstant.REPORT_FIXED_INFORMATION)){//"+RESP:GTFRI"
			return new FixedInfoReportMessage(message);
		}
		else if(message.contains(QueclinkConstant.REPORT_DEVICE_INFORMATION)){//"+RESP:GTINF"
			return new DeviceInfoMessage(message);
		} 
		else if(message.contains(QueclinkConstant.EVENT_REPORT_MOTION_STATE)){//"+RESP:GTSTT"
			return new MotionStateMessage(message);
		}
		else if (message.contains(QueclinkConstant.EVENT_REPORT_TEMP_ALARM)){//"+RESP:GTTEM"
			return new TempAlarmMessage(message);
		}//Acknowledgement message for both are same
		else if (message.contains(QueclinkConstant.ACK_BEARER_SETTING) || message.contains(QueclinkConstant.ACK_SERVER_REGISTRATION)){//"+ACK:GTBSI" "+ACK:GTSRI"
			return new AckMessage(message);
		}
		else if(message.contains(QueclinkConstant.ACK_HEARTBEAT)) {//"+ACK:GTSRI"
			return new HeartbeatAckMessage(message);
		}
		else if(message.contains(QueclinkConstant.REPORT_TEMPERATURE_RESPONSE )) {//+RESP:GTTPR  "+BUFF:GTTPR";
			return new TemperatureReportMessage(message);
		}
		else if(message.contains(QueclinkConstant.CURRENT_POSITION_RESPONSE )) {//+RESP:GTRTL ";
			return new CurrentPositionMessage(message);
		} 
		else if(message.contains(QueclinkConstant.SOS_MESSAGE )) {//+RESP:GTRTL ";
			return new SOSMessage(message);
		}
		else{
			return null;
		}
	}
}
