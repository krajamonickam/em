package com.rfxcel.sensor.queclink.message;

import com.rfxcel.sensor.queclink.util.QueclinkUtility;


/**
 * @author Tejshree Kachare
 *<p> Location report information</p>
 *<p>
 *	Incoming message string	"+RESP:GTRTL "
 *</p>
 */
public class CurrentPositionMessage extends QueclinkMessage {

	// number of parameters present in this message
	private static final int parameterCount = 22;

	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String reportId;
	private String reportType;
	private String number;
	private String gpsAccuracy;
	private String speed;
	private String azimuth;
	private String altitude;
	private String longitude;
	private String latitude;
	private String gpsUTCTimes;
	private String mcc;
	private String mnc;
	private String sid;
	private String bid;
	private String odoMileage;
	private String batteryPercentage;
	private String ioStatus;
	private String sendTime;
	private String countNumber;

	/**
	 * Since the number of parameters are not fixed for FixedInfoReport, will not initialize object with parameters after ioStatus, as additional enabled fields will be added after it.
	 * @param message
	 */
	public CurrentPositionMessage(String message){
		String [] parameters = message.split(",");
		if(parameters.length < parameterCount){
			throw new IllegalArgumentException("Number of parameters expected in +RESP:GTRTL is greater than or equal to "+parameterCount );
		}
		this.protocolVersion = parameters[1];
		this.uniqueId = parameters[2];
		this.deviceName = parameters[3];
		this.reportId = parameters[4];
		this.reportType = parameters[5];
		this.number = parameters[6];
		this.gpsAccuracy = parameters[7];
		this.speed = parameters[8];
		this.azimuth = parameters[9];
		this.altitude = parameters[10];
		this.longitude = parameters[11];
		this.latitude = parameters[12];
		this.gpsUTCTimes = parameters[13];
		this.mcc = parameters[14];
		this.mnc = parameters[15];
		this.sid = parameters[16];
		this.bid = parameters[17];
		this.odoMileage = parameters[18];
		this.batteryPercentage = parameters[19];
		this.ioStatus= parameters[20];

	}

	/**
	 * @return the parametercount
	 */
	public static int getParameterCount() {
		return parameterCount;
	}

	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @return the reportId
	 */
	public String getReportId() {
		return reportId;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}

	/**
	 * @return the gpsAccuracy
	 */
	public String getGpsAccuracy() {
		return gpsAccuracy;
	}

	/**
	 * @return the speed
	 */
	public String getSpeed() {
		return speed;
	}

	/**
	 * @return the azimuth
	 */
	public String getAzimuth() {
		return azimuth;
	}

	/**
	 * @return the altitude
	 */
	public String getAltitude() {
		return altitude;
	}

	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}

	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}

	/**
	 * @return the gpsUTCTimes
	 */
	public String getGpsUTCTimes() {
		return gpsUTCTimes;
	}

	/**
	 * @return the mcc
	 */
	public String getMcc() {
		return mcc;
	}

	/**
	 * @return the mnc
	 */
	public String getMnc() {
		return mnc;
	}

	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}

	/**
	 * @return the bid
	 */
	public String getBid() {
		return bid;
	}

	/**
	 * @return the odoMileage
	 */
	public String getOdoMileage() {
		return odoMileage;
	}

	/**
	 * @return the batteryPercentage
	 */
	public String getBatteryPercentage() {
		return batteryPercentage;
	}

	/**
	 * @return the ioStatus
	 */
	public String getIoStatus() {
		return ioStatus;
	}

	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}



	@Override
	public String toSensorJson() {
		String jsonInString = null;
		QueclinkSensor sensor = new QueclinkSensor();
		//sensor.setDeviceName(this.deviceName);
		sensor.setDeviceIdentifier(this.getUniqueId());
		sensor.setLatitude(this.getLatitude());
		sensor.setLongitude(this.getLongitude());
		sensor.setStatusTimeStamp(QueclinkUtility.getDateFromUTC(this.getGpsUTCTimes()));
		sensor.setBattery(this.getBatteryPercentage());
		jsonInString = sensor.toJson();

		return jsonInString;
	}


}
