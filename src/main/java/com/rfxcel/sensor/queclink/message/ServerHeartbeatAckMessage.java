package com.rfxcel.sensor.queclink.message;

import java.io.IOException;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;

public class ServerHeartbeatAckMessage {


	private String protocolVersion;
	private String countNumber;


	public ServerHeartbeatAckMessage() {
		// TODO Auto-generated constructor stub
	}

	public ServerHeartbeatAckMessage(String protocolVersion, String countNumber) {
		this.protocolVersion = protocolVersion;
		this.countNumber = countNumber;
	}



	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @param protocolVersion the protocolVersion to set
	 */
	public void setProtocolVersion(String protocolVersion) {
		this.protocolVersion = protocolVersion;
	}
	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}
	/**
	 * @param countNumber the countNumber to set
	 */
	public void setCountNumber(String countNumber) {
		this.countNumber = countNumber;
	}
	
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			jsonInString = mapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (JsonMappingException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		return jsonInString;

	}
}
