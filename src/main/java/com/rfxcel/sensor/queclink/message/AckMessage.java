package com.rfxcel.sensor.queclink.message;


/**
 * 
 * @author Tejshree Kachare
 * <p> Generic Acknowledgement message </p>
 *
 */
public class AckMessage extends QueclinkMessage {

	// number of parameters present in this message
	private static final int parameterCount = 6;

	public AckMessage(String message) {

		String [] parameters = message.split(",");
		if(parameters.length != parameterCount){
			throw new IllegalArgumentException("Number of parameters expected in Acknowledgement message is "+parameterCount);
		}

		this.protocolVersion = parameters[1];
		this.uniqueId = parameters[2];
		this.deviceName = parameters[3];
		this.serialNumber = parameters[4];
		this.sendTime =  parameters[5];
		this.countNumber =parameters[6];// TODO remove tail character
	}

	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String serialNumber;
	private String sendTime;
	private String countNumber;


	/**
	 * @return the parametercount
	 */
	public static int getParametercount() {
		return parameterCount;
	}

	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @return the serialNumber
	 */
	public String getSerialNumber() {
		return serialNumber;
	}

	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}

	@Override
	public String toSensorJson() {
		String jsonInString = null;
		
		QueclinkSensor sensor = new QueclinkSensor();
		//sensor.setDeviceName(this.getDeviceName());
		sensor.setDeviceIdentifier(this.getUniqueId());
		
		jsonInString = sensor.toJson();

		return jsonInString;
	}
}
