package com.rfxcel.sensor.queclink.message;


/**
 * 
 * @author Tejshree Kachare
 *<p> Heartbeat message "+ACK:GTHBD" </p>
 *
 */
public class HeartbeatAckMessage extends QueclinkMessage {

	// number of parameters present in this message
	private static final int parameterCount = 6;

	public HeartbeatAckMessage(String respMessage) {
		String [] parameters = respMessage.split(",");
		if(parameters.length != parameterCount){
			throw new IllegalArgumentException( "Number of parameters expected in ACK:GTHBD message is "+parameterCount );
		}
		this.protocolVersion = parameters[1];
		this.uniqueId = parameters[2];
		this.deviceName = parameters[3];
		this.sendTime = parameters[4];
		this.countNumber = parameters[5];// TODO remove tail character
	}

	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String sendTime;
	private String countNumber;

	/**
	 * @return the parametercount
	 */
	public static int getParametercount() {
		return parameterCount;
	}
	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}
	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}
	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}


	@Override
	public String toSensorJson() {
		String jsonInString = null;
		QueclinkSensor sensor = new QueclinkSensor();
		//sensor.setDeviceName(this.getDeviceName());
		sensor.setDeviceIdentifier(this.getUniqueId());

		jsonInString = sensor.toJson();

		return jsonInString;
	}
}
