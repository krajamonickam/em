package com.rfxcel.sensor.queclink.message;

import java.util.ArrayList;




/**
 * @author tejshree_kachare
 *<p> Fixed report information</p>
 *<p>
 *	Incoming message string	"+RESP:GTFRI"
 *</p>
 */
public class FixedInfoReportMessage extends QueclinkMessage {

	// number of parameters present in this message
	private static final int parameterCount = 22;

	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String reportId;
	private String reportType;
	private String number;
	private ArrayList<GPSData> gps = new ArrayList<GPSData>();
	private String batteryPercentage;
	private String ioStatus;
	private String sendTime;
	private String countNumber;

	
	
	
	
	/**
	 * Since the number of parameters are not fixed for FixedInfoReport, will not initialize object with parameters after ioStatus, as additional enabled fields will be added after it.
	 * @param message
	 */
	public FixedInfoReportMessage(String message){
		String [] parameters = message.split(",");
		if(parameters.length < parameterCount){
			throw new IllegalArgumentException("Number of parameters expected in +RESP:GTFRI is greater than or equal to "+parameterCount );
		}
		int index = 1;
		this.protocolVersion = parameters[index++];
		this.uniqueId = parameters[index++];
		this.deviceName = parameters[index++];
		this.reportId = parameters[index++];
		this.reportType = parameters[index++];
		this.number = parameters[index++];
		if(this.number==null || this.number.trim().length()==0){
			throw new IllegalArgumentException("reportNumber in +RESP:GTFRI or +BUFF:GTFRI cannot be null or empty");
		}
		int count = Integer.valueOf(this.number);
		for(int i =0; i < count; i++){
			GPSData g = new GPSData();
			g.setGpsAccuracy(parameters[index++]);
			g.setSpeed(parameters[index++]);
			g.setAzimuth(parameters[index++]);
			g.setAltitude(parameters[index++]);
			g.setLongitude(parameters[index++]);
			g.setLatitude(parameters[index++]);
			g.setGpsUTCTimes(parameters[index++]);
			g.setMcc(parameters[index++]);
			g.setMnc( parameters[index++]);
			g.setSid( parameters[index++]);
			g.setBid( parameters[index++]);
			g.setOdoMileage (parameters[index++]);
			this.gps.add(g);
		}
		this.batteryPercentage = parameters[index++];
		// this.ioStatus = parameters[index+3];
		this.sendTime = parameters[index++];
		this.ioStatus= parameters[index++];
	}
	
	/**
	 * @return the parametercount
	 */
	public static int getParameterCount() {
		return parameterCount;
	}

	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}

	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}

	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}

	/**
	 * @return the reportId
	 */
	public String getReportId() {
		return reportId;
	}

	/**
	 * @return the reportType
	 */
	public String getReportType() {
		return reportType;
	}

	/**
	 * @return the number
	 */
	public String getNumber() {
		return number;
	}


	/**
	 * @return the GPSData arraylist
	 */
	public ArrayList<GPSData> getGPS() {
		return gps;
	}

	/**
	 * @set the GPSData arraylist
	 */
	public void setGPS(ArrayList<GPSData> gps) {
		this.gps = gps;
	}


	/**
	 * @return the batteryPercentage
	 */
	public String getBatteryPercentage() {
		return batteryPercentage;
	}

	/**
	 * @return the ioStatus
	 */
	public String getIoStatus() {
		return ioStatus;
	}

	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}

	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}


	
	@Override
	public String toSensorJson() {
		return null;
	}
	

}
