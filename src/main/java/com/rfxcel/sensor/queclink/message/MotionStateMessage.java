package com.rfxcel.sensor.queclink.message;

import com.rfxcel.sensor.queclink.util.QueclinkUtility;


/**
 * 
 * @author tejshree_kachare
 * Device motion state indication
 * <p> Message String "+RESP:GTSTT" </p>
 *
 */
public class MotionStateMessage extends QueclinkMessage {

	// number of parameters present in this message
	private static final int parameterCount = 19;


	public MotionStateMessage(String message){
		String [] parameters = message.split(",");
		if(parameters.length != parameterCount){
			throw new IllegalArgumentException( "Number of parameters expected in +RESP:GTSTT message is "+parameterCount  );
		}
		this.protocolVersion = parameters[1];
		this.uniqueId = parameters[2];
		this.deviceName = parameters[3];
		this.state = parameters[4];
		this.gpsAccuracy = parameters[5];
		this.speed = parameters[6];
		this.azimuth = parameters[7];
		this.altitude = parameters[8];
		this.lastLongitude = parameters[9];
		this.lastLatitude = parameters[10];
		this.gpsUtcTime = parameters[11];
		this.mcc = parameters[12];
		this.mnc = parameters[13];
		this.sid = parameters[14];
		this.bid = parameters[15];
		this.odoMileage = parameters[16];
		this.sendTime = parameters[17];
		this.countNumber = parameters[18]; //TODO remove the tail character
	}

	private String protocolVersion;
	private String uniqueId;
	private String deviceName;
	private String state;
	private String gpsAccuracy;
	private String speed;
	private String azimuth;
	private String altitude;
	private String lastLongitude;
	private String lastLatitude;
	private String gpsUtcTime;
	private String mcc;
	private String mnc;
	private String sid;
	private String bid;
	private String odoMileage;
	private String sendTime;
	private String countNumber;


	/**
	 * @return the parametercount
	 */
	public static int getParametercount() {
		return parameterCount;
	}
	/**
	 * @return the protocolVersion
	 */
	public String getProtocolVersion() {
		return protocolVersion;
	}
	/**
	 * @return the uniqueId
	 */
	public String getUniqueId() {
		return uniqueId;
	}
	/**
	 * @return the deviceName
	 */
	public String getDeviceName() {
		return deviceName;
	}
	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}
	/**
	 * @return the gpsAccuracy
	 */
	public String getGpsAccuracy() {
		return gpsAccuracy;
	}
	/**
	 * @return the speed
	 */
	public String getSpeed() {
		return speed;
	}
	/**
	 * @return the azimuth
	 */
	public String getAzimuth() {
		return azimuth;
	}
	/**
	 * @return the altitude
	 */
	public String getAltitude() {
		return altitude;
	}
	/**
	 * @return the lastLongitude
	 */
	public String getLastLongitude() {
		return lastLongitude;
	}
	/**
	 * @return the lastLatitude
	 */
	public String getLastLatitude() {
		return lastLatitude;
	}
	/**
	 * @return the gpsUtcTime
	 */
	public String getGpsUtcTime() {
		return gpsUtcTime;
	}
	/**
	 * @return the mcc
	 */
	public String getMcc() {
		return mcc;
	}
	/**
	 * @return the mnc
	 */
	public String getMnc() {
		return mnc;
	}
	/**
	 * @return the sid
	 */
	public String getSid() {
		return sid;
	}
	/**
	 * @return the bid
	 */
	public String getBid() {
		return bid;
	}
	/**
	 * @return the odoMileage
	 */
	public String getOdoMileage() {
		return odoMileage;
	}
	/**
	 * @return the sendTime
	 */
	public String getSendTime() {
		return sendTime;
	}
	/**
	 * @return the countNumber
	 */
	public String getCountNumber() {
		return countNumber;
	}


	@Override
	public String toSensorJson() {
		String jsonInString = null;
			QueclinkSensor sensor = new QueclinkSensor();
			//sensor.setDeviceName(this.getDeviceName());
			sensor.setDeviceIdentifier(this.getUniqueId());
			sensor.setLatitude((this.getLastLatitude()));
			sensor.setLongitude(this.getLastLongitude());
			sensor.setStatusTimeStamp(QueclinkUtility.getDateFromUTC(this.getGpsUtcTime()));
			
			jsonInString = sensor.toJson();
		
		return jsonInString;
	}
}
