package com.rfxcel.sensor.queclink.util;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;


public class QueclinkUtility {

	public static String getDateFromUTC(String utcTime) 
	{
		String date = null;
		try {
			Date dt =null;
			DateFormat df=new SimpleDateFormat("yyyyMMddHHmmss");
			DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
			dt=df.parse(utcTime);
			date = df1.format(dt);

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return date;
	}
}
