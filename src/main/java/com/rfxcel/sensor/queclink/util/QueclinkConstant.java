package com.rfxcel.sensor.queclink.util;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class QueclinkConstant {
	
	public static final String REPORT_FIXED_INFORMATION = ":GTFRI";
	public static final String REPORT_DEVICE_INFORMATION = ":GTINF";
	public static final String REPORT_TEMPERATURE_RESPONSE = ":GTTPR";
	public static final String EVENT_REPORT_MOTION_STATE = ":GTSTT";
	public static final String EVENT_REPORT_TEMP_ALARM = ":GTTEM";
	public static final String ACK_BEARER_SETTING = "+ACK:GTBSI";
	public static final String ACK_SERVER_REGISTRATION = "+ACK:GTSRI";
	public static final String ACK_HEARTBEAT = "+ACK:GTHBD";
	public static final String SERVER_ACK_HEARTBEAT = "+ACK:GTHBD";
	public static final String CURRENT_POSITION_RESPONSE = ":GTRTL";
	public static final String SOS_MESSAGE = ":GTSOS";
}
