package com.rfxcel.sensor.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.org.service.OrganizationService;
import com.rfxcel.sensor.beans.Attribute;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.dao.DeviceTypeDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

/**
 * 
 * @author sachin_kohale
 * @since Apr 26, 2017
 */
@Path("deviceType")
public class DeviceTypeService {

	private static Logger logger = Logger.getLogger(DeviceTypeService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static DeviceTypeDAO deviceTypeDAO = DeviceTypeDAO.getInstance();

	/**
	 * 
	 * @param httpRequest
	 * @param sensorRequest
	 * @return
	 */
	@POST
	@Path("/addDeviceType")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addDeviceType(@Context HttpServletRequest httpRequest, SensorRequest sensorRequest){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = sensorRequest.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			DeviceDetail deviceDetail = sensorRequest.getDeviceDetail();
			if(deviceDetail == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Please provide device type details")).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			String deviceType;
			Boolean isPresent;			
			deviceType = deviceDetail.getDeviceType();			
			if(deviceType == null || deviceType.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type cannot be null or empty")).build();
			}
			//check if active deviceType is not present for that requested orgId
			isPresent = deviceTypeDAO.checkIfDeviceTypeIsPresentForOrg(deviceType, orgId);
			if(isPresent){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type "+deviceType+" is already present in the system for requested organization.")).build();
			}

			Integer deviceTypeId = deviceTypeDAO.addDeviceType(deviceDetail, orgId);			

			if(deviceTypeId > 0){	
				//adding default attributes
				deviceTypeDAO.addDefaultAttributesByDeviceType(deviceTypeId, orgId);

				//populating DeviceTypeAttrCache
				DeviceCache.getInstance().addDeviceTypeAttrbute(deviceTypeId);

				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("DeviceType added successfully")).build();			
			}else{
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Error occured while adding devicetype")).build();			
			}

		} catch (Exception e) {
			logger.error("Exception in add device type "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: addDeviceType  Request :: "+sensorRequest +" Response :: "+response );
		}
	}

	@POST
	@Path("/updateDeviceType")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateDeviceType(@Context HttpServletRequest httpRequest, SensorRequest sensorRequest){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = sensorRequest.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			DeviceDetail deviceDetail = sensorRequest.getDeviceDetail();
			if(deviceDetail == null || deviceDetail.getDeviceTypeId() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Please provide device type details")).build();
			}

			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			Integer deviceTypeId = deviceDetail.getDeviceTypeId();
			//check if active deviceType not present
			Boolean present = deviceTypeDAO.checkIfActiveDeviceTypeIsPresent(deviceTypeId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("DeviceType is not configured or is already deleted.")).build();
			}

			String deviceType;				
			deviceType = deviceDetail.getDeviceType();			
			if(deviceType == null || deviceType.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type cannot be null or empty")).build();
			}

			Boolean isPresent = deviceTypeDAO.checkIfDeviceTypeIsPresentForOrg(deviceType, deviceTypeId, orgId);
			if(isPresent){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type "+deviceType+" is already present in the system for requested organization.")).build();
			}

			Integer rowupdated = deviceTypeDAO.updateDeviceType(deviceDetail, orgId);			
			if(rowupdated > 0){
				//Adding data to cache
				DeviceCache.getInstance().addDeviceTypeAttrbute(deviceTypeId);				
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("DeviceType udpdated successfully")).build();			
			}else{
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Error occured while updating devicetype")).build();			
			}

		}catch(Exception e){
			logger.error("Exception in update device type "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: updateDeviceType  Request :: "+sensorRequest +" Response :: "+response );
		}
	}

	@POST
	@Path("/deleteDeviceType")
	@Produces("application/json")
	@Consumes("application/json")
	public Response deleteDeviceType(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			DeviceDetail deviceDetail = request.getDeviceDetail();
			if(deviceDetail == null || deviceDetail.getDeviceTypeId() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("deviceTypeId cannot be null")).build();
			}

			Integer deviceTypeId = deviceDetail.getDeviceTypeId();
			//check if active deviceType not present
			Boolean present = deviceTypeDAO.checkIfActiveDeviceTypeIsPresent(deviceTypeId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("DeviceType is not configured or is already deleted.")).build();
			}
			DeviceCache.getInstance().removeDeviceType(deviceTypeId);
			deviceTypeDAO.deleteDeviceType(deviceTypeId);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("DeviceType deleted successfully")).build();			
		}catch(Exception e){
			logger.error("Exception in deleteDeviceType "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: deleteDeviceType.  Request :: "+request +" Response :: "+response );
		}
	}

	// NOTE : API is updated for multitenancy	
	@POST
	@Path("/getDeviceTypes")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getDeviceTypes(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			//Currently request only sends SearchType 
			if(request.getSearchType() == null || !request.getSearchType().equalsIgnoreCase("All")){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Search Type is null or incorrect value is provided")).build();
			}
			ArrayList<DeviceDetail> deviceList = null;
			if(orgId > 0){
				deviceList = deviceTypeDAO.getDeviceTypes(orgId);  //for normal user
			}else{
				deviceList = deviceTypeDAO.getAllDeviceTypes();   //for system user
			}

			Sensor sensor = new Sensor();
			sensor.setDeviceList(deviceList);
			res.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getDeviceTypes "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getDeviceTypes "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getDeviceTypes.  Request :: "+request +" Response :: "+res );
		}
	}

	@POST
	@Path("/getAttributeList")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getAttributeList(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			ArrayList<Attribute> attributeList = deviceTypeDAO.getAttributeList();
			Sensor sensor = new Sensor();
			sensor.setAttributeList(attributeList);
			response.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch(Exception e){
			logger.error("Exception in getAttributeList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: getAttributeList.  Request :: "+request +" Response :: "+response );
		}
	}

	// NOTE : API is updated for multitenancy
	@POST
	@Path("/getAttributesByDeviceType")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAttributesByType(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			Sensor sensor=new Sensor();
			ArrayList<Attribute> attributeList ;
			attributeList = deviceTypeDAO.getAttributesByType(request.getDeviceDetail(), request.getOrgId());
			sensor.setAttributeList(attributeList);

			res.setSensor(sensor);
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in getAttributesByType "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getAttributesByType.  Request :: "+request +" Response :: "+res );
		}
	}

	/**
	 *  <p> Deactivate all deviceTypes and its dependent entities for an organization
	 * @param orgId
	 *//*
	public static void deleteDeviceTypeForOrg(Integer orgId){
		try{
			deviceTypeDAO.deactivateOrgDeviceTypes(orgId);

		}catch(Exception e){
			logger.error("Exception in DeviceTypeService.deleteDeviceTypeForOrg "+ e.getMessage());
			e.printStackTrace();
		}finally{
			diagnosticService.write("API :: DeviceTypeService.deleteDeviceTypeForOrg " );
		}
	}*/


}
