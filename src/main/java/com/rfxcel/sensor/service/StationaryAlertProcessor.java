package com.rfxcel.sensor.service;

import org.apache.log4j.Logger;

import com.rfxcel.messaging.SMSService;
import com.rfxcel.messaging.VoiceService;
import com.rfxcel.notification.dao.NotificationDAO;

public class StationaryAlertProcessor implements Runnable{

	private static final Logger logger = Logger.getLogger(StationaryAlertProcessor.class);
	private Thread thread = null;
	private Integer orgId = null;
	private String deviceId = null;
	private String containerId = null;	
	private String productId = null;
	private Long alertCode = null;
	
	public StationaryAlertProcessor(Integer orgId, String productId, String containerId, String deviceId, Long alertCode) {
		this.orgId = orgId;
		this.productId = productId;
		this.containerId = containerId;
		this.deviceId = deviceId;
		this.alertCode = alertCode;
		this.thread = new Thread(this);
		SensorServiceDemo.setAlertsStarted();	// setting alerts finished flag to false when we start with stationary alert
		thread.start();
	}


	public void sleep(int seconds) {
		try {
			Thread.sleep(seconds*1000L);// 10 secs
		} catch (Exception ex) {}
		logger.info("StationaryAlertProcessor - sleeping " + seconds + "s...");
	}
	
	public void nextStep(String text, int delay) {
		logger.info("StationaryAlertProcessor - alert[" + SensorServiceDemo.alerts.alertsIndex + "] = " + text);
		SensorServiceDemo.addMonitorAlerts(text);
		sleep(delay);
	}
	
	@Override
	public void run() {
		logger.info("StationaryAlertProcessor - Stationary alert processor started");
		nextStep("", 3);
		nextStep("Detected stationary shipment...", 3);
		nextStep("Monitoring temperature...", 3);
		nextStep("Retrieving temperature data", 3);
		nextStep("", 3);
		nextStep("Gathering weather data", 3);
		nextStep("", 3);
		nextStep("Shipment is at risk of overheating!", 3);
		
		String[] phoneList = getPhoneList();
		String coreMsg = "Shipment " + productId + "-" + containerId + " stalled, at risk of overheating.";
		String message = "ACTION NEEDED: " + coreMsg;
		sendSMSMessages(phoneList,  message);
		nextStep((phoneList == null)?"No list for sending SMS alert" : "Sending SMS alerts", 3);

		message = coreMsg + " Your immediate action is requested.";
		sendVoiceMessages(phoneList,  message);
		nextStep((phoneList == null)?"No list for sending Voice alert" : "Sending Voice alerts", 3);
		
		nextStep("Monitoring complete...", 30);
		SensorServiceDemo.setAlertsFinished();
		logger.info("StationaryAlertProcessor - Stationary alert processor ends");
	}
	
	public String[] getPhoneList() {
		String phones = NotificationDAO.getPhoneForAlertCode(this.alertCode, this.orgId);
		if ((phones == null) || (phones.trim().length() == 0)) {
			logger.info("StationaryAlertProcessor - sendSMSMessages - no phone numbers for alert " + alertCode + " and org " + orgId);
			return(null);
		}
		String[] phoneList = phones.split(",");
		return(phoneList);
	}
	
	public void sendSMSMessages(String[] phoneList, String message) {
		if (phoneList == null) return;
		SMSService sms = SMSService.getInstance();
		for (String phone : phoneList) {
			if (sms.doSend(phone, message)) {
				logger.info("StationaryAlertProcessor - SMS message sent to " + phone + " message " + message);
			}
			else {
				logger.error("StationaryAlertProcessor - Error sending SMS message to " + phone + " message " + message);
			}
		}
	}

	public void sendVoiceMessages(String[] phoneList, String message) {
		if (phoneList == null) return;
		VoiceService sms = VoiceService.getInstance();
		for (String phone : phoneList) {
			sms.send(phone, message);
			logger.info("StationaryAlertProcessor - SMS message sent to " + phone + " message " + message);
		}
	}

	public Integer getOrgId() {
		return orgId;
	}

	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public String getContainerId() {
		return containerId;
	}

	public void setContainerId(String containerId) {
		this.containerId = containerId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	
}