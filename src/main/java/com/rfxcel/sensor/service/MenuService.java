package com.rfxcel.sensor.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.sensor.beans.Menu;
import com.rfxcel.sensor.dao.MenuDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.MenuRequest;
import com.rfxcel.sensor.vo.MenuResponse;


/**
 * 
 * @author sachin_kohale
 * @since May 15, 2017
 */
@Path("menu")
public class MenuService {
	private static Logger logger = Logger.getLogger(MenuService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	
	@POST
	@Path("/getMenuList")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getMenuList(@Context HttpServletRequest httpRequest, MenuRequest request) throws SQLException {
		MenuResponse response = new MenuResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){				
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			int userType = 0;
			String token = null;
			String user = null;
			Integer userTypeInt = null;
			if (TokenManager.getInstance().getAuthenticate())
			{
				token = TokenManager.getInstance().getToken(httpRequest);
				if (token != null) {
					user = TokenManager.getInstance().getUser(token);
				}
				if (user != null) {
					userTypeInt = TokenManager.getInstance().getUserType(token);
				}
				if (userTypeInt != null) {
					userType = userTypeInt.intValue();
				}
			}
			ArrayList<Menu> menuList = MenuDAO.getInstance().getMenuList(userType);
			response.setMenuList(menuList);
			response.setResponseCode(SensorConstant.RESPONSECODE_SUCCESS);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch(Exception e){
			logger.error("Exception in getMenuList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: getMenuList.  Request :: "+request +" Response :: "+response );
		}
	}
}
