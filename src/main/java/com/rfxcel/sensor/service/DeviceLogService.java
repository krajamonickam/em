package com.rfxcel.sensor.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.dao.SearchDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

@Path("deviceLog")
public class DeviceLogService {
	private static Logger logger = Logger.getLogger(DeviceLogService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private DeviceLogDAO deviceLogDAO = DeviceLogDAO.getInstance();
	
	@POST
	@Path("/getDeviceLogs")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getDeviceLogs(@Context HttpServletRequest httpRequest, SensorRequest sensorRequest){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			DeviceLog deviceLog = sensorRequest.getDeviceLog();
			if(deviceLog == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Please provide device log details")).build();
			}
			
			Integer orgId = deviceLog.getOrgId();
			//check if orgId is valid
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
								
			String user = TokenManager.getInstance().getUserForToken(httpRequest);
			sensorRequest.setUser(user);
			
			//check the Elastic search fetch is true or false
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			
			// Temporarily adding alertDeviceLogSearch parameter to search devicelog from elastic search. Please remove this flag after elastic search running perfect.
			String alertDevicelogString = ConfigCache.getInstance().getSystemConfig("alert.devicelog.search.enable");
			Boolean alertDeviceLogSearch = false;
			if(alertDevicelogString != null){
				alertDeviceLogSearch = Boolean.valueOf(alertDevicelogString);
			}
			// end
					
			String responseMsg = "";
			if(fetchESData || alertDeviceLogSearch){
				response = SearchDAO.getInstance().getDeviceLogs(sensorRequest);
				if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
					responseMsg = response.getResponseMessage();
					response.setResponseMessage(responseMsg);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response).build();
				}									
				
			}else{
				//check if deviceId is empty or null
				String deviceId = deviceLog.getDeviceId();			
				if(deviceId == null || deviceId.trim().length() == 0 ){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("deviceId cannot be null or empty")).build();
				}
				ArrayList<DeviceLog> deviceLogList = deviceLogDAO.getDeviceLogs(sensorRequest);
				response.setDeviceLogList(deviceLogList);
			}			
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
			
		}catch(Exception e){
			logger.error("Exception in getDeviceLogs "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: getDeviceLogs  Request :: "+sensorRequest +" Response :: "+response );
		}
	}
}
