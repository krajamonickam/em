package com.rfxcel.sensor.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.beans.MonitorAlerts;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

@Path("sensor")
public class SensorServiceDemo {
	private static Logger logger = Logger.getLogger(SensorServiceDemo.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	public static MonitorAlerts alerts = new MonitorAlerts();
	
	@POST
	@Path("/getMonitorAlerts")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getMonitorAlerts(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			res.setMonitorAlerts(alerts);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getAttrInfo "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getMonitorAlerts  Request :: "+request +" Response :: "+res );
		}

	}
	
	/**
	 * @param alert
	 */
	public static void addMonitorAlerts(String alert){
		alerts.alertsMsg.add(alert);
		alerts.alertsIndex++;
	}
	/**
	 * @param alertsFinished
	 */
	public static void setAlertsFinished() {
		alerts.alertsFinished = true;
	}
	/**
	 * @param alertsStarted
	 */
	public static void setAlertsStarted() {
		alerts = new MonitorAlerts();
		alerts.alertsMsg =  new ArrayList<String>();
		alerts.alertsIndex = 0;
		alerts.alertsFinished = false;
	}
}
