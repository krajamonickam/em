package com.rfxcel.sensor.service;

import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.TimeZone;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.http.HttpResponse;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.fasterxml.jackson.annotation.JsonInclude.Include;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceAttributeLogCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.notification.service.NotificationService;
import com.rfxcel.notification.util.NotificationConstant;
import com.rfxcel.rule.service.RuleEvaluationService;
import com.rfxcel.rule.util.RuleConstants;
import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.beans.DevicePackageStatus;
import com.rfxcel.sensor.beans.ExcursionLog;
import com.rfxcel.sensor.beans.Geofence;
import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.dao.ExcursionLogDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.util.Config;
import com.rfxcel.sensor.util.ConnectionUtil;
import com.rfxcel.sensor.util.Sensor;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Timer;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.BactGrowthRequest;
import com.rfxcel.sensor.vo.BactGrowthResponse;

public class SensorProcessJSON implements Runnable {
	private static final Logger logger = Logger.getLogger(SensorProcessJSON.class);
	SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	DecimalFormat df = new DecimalFormat("#0.##########");
	private static SensorProcessJSON instance = null;
	private LinkedBlockingQueue<SensorMessage> queue = new LinkedBlockingQueue<SensorMessage>();
	private Thread thread = null;
	private static SendumDAO sendumDAO = SendumDAO.getInstance();
	private static long filterSendTime = 0L;
	private static Map<String, String> filterLastMap = null;
	private static DeviceAttributeLogCache deviceAttributeLogCache = DeviceAttributeLogCache.getInstance();
	private static DeviceCache deviceCache = DeviceCache.getInstance();
	private static ConfigCache configCache = ConfigCache.getInstance();
	private static AssociationCache associationCache = AssociationCache.getInstance();

	public static class SensorMessage {
		public String message;
		public boolean authenticated;
		public String reqUser;
		public String reqPassword;
		public SensorMessage(String message, boolean authenticated, String reqUser, String reqPassword) {
			this.message = message;
			this.authenticated = authenticated;
			this.reqUser = reqUser;
			this.reqPassword = reqPassword;
		}
	}

	public static SensorProcessJSON getInstance()
	{
		if (instance == null) {
			instance = new SensorProcessJSON();
			sendumDAO = SendumDAO.getInstance();
		}
		return(instance);
	}

	protected SensorProcessJSON() {
		simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC"));
		thread = new Thread(this);
		thread.start();
	}

	public LinkedBlockingQueue<SensorMessage> getQueue() {
		return queue;
	}

	public void run() {
		if (queue == null)
			return;
		SensorMessage message = null;
		try {
			while ((message = queue.take()) != null) {
				try {
					process(message.message, message.authenticated, message.reqUser, message.reqPassword);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	public void doQueue(String kirsenData) {
		doQueue(kirsenData, true, null, null);
	}

	public void doQueue(String payload, boolean authenticated, String reqUser, String reqPassword)
	{
		logger.info("Processing message " + ((reqUser == null) ? "" : " with credentials:" + reqUser) + " "+ payload);
		getQueue().add(new SensorMessage(payload, authenticated, reqUser, reqPassword));
	}

	public boolean isValidMap(Map<String, String> map)
	{
		String deviceIdentifer = map.get("deviceIdentifier");
		if (deviceIdentifer == null) {
			return(false);
		}
		String tilt = map.get("tilt");
		if (tilt == null) {
			return(false);
		}
		String light = map.get("light");
		if (light == null) {
			return(false);
		}
		String temperature = map.get("temperature");
		if (temperature == null) {
			return(false);
		}
		return(true);
	}

	public int getPercentChange(String attribute, Map<String, String> map, Map<String, String> map0, int factor)
	{
		String curr  = map.get(attribute);
		if (curr == null) return(0);
		double currVal  = Double.valueOf(curr) * factor;
		String last  = map0.get(attribute);
		if (last == null) return(Integer.MAX_VALUE);
		double lastVal  = Double.valueOf(last) * factor;
		int change   = (int) Math.abs(currVal - lastVal);
		return(change);
	}


	public boolean checkFilter(Map<String, String> map, String deviceID)
	{
		boolean doSendEvent = true;
		try
		{
			if (Config.FILTER_TIME > 0L) {
				if ((filterSendTime <= 0L) || (filterLastMap == null)) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (no last send time or map)");
					return(doSendEvent);
				}
				long currTime = new Date().getTime();
				long interval = currTime - filterSendTime;
				if (interval > Config.FILTER_TIME) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (interval " + interval + ")");
					return(doSendEvent);
				}
				if (!isValidMap(map)) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (no temp/tilt/light in map)");
					return(doSendEvent);
				}
				doSendEvent = false;
				int x = 0;
				if ((x = getPercentChange("tilt", map, filterLastMap, 1)) > Config.FILTER_TILT) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (TILT: " + x + ")");
					doSendEvent = true;
					return(doSendEvent);
				}
				if ((x = getPercentChange("light", map, filterLastMap, 1)) > Config.FILTER_LIGHT) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (LIGHT: " + x + ")");
					doSendEvent = true;
					return(doSendEvent);
				}
				if ((x = getPercentChange("temperature", map, filterLastMap, 1)) > Config.FILTER_TEMPERATURE) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (TEMP: " + x + ")");
					doSendEvent = true;
					return(doSendEvent);
				}
				if ((x = getPercentChange("latitude", map, filterLastMap, Config.SENSOR_LOCATION_DECIMAL_FACTOR)) > 0) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (LATITUDE: " + x + ")");
					doSendEvent = true;
					return(doSendEvent);
				}
				if ((x = getPercentChange("longitude", map, filterLastMap, Config.SENSOR_LOCATION_DECIMAL_FACTOR)) > 0) {
					logger.info("++ Filtered IN message from device ID: "+ deviceID + " (LONGITUDE: " + x + ")");
					doSendEvent = true;
					return(doSendEvent);
				}
			}
			return(doSendEvent);
		}
		finally
		{
			if (doSendEvent || (filterSendTime <= 0L)) {
				filterSendTime = new Date().getTime();
			}
			if (isValidMap(map)) {
				filterLastMap = map;
			}
		}
	}
	
	private boolean checkCredentials(String deviceId, String reqUser, String reqPassword, int orgId) {
		String user = configCache.getOrgConfig("kirsen.user", orgId);
		String password = configCache.getOrgConfig("kirsen.password", orgId);
		if ((user == null) || (password == null)) {
			logger.error("Kirsen authentication *** NOT SET FOR ORG " + orgId + " ***");
			return(false);
		}
		if (user.equals(reqUser) && password.equals(reqPassword)) {
			return(true);
		}
		return(false);
	}
	

	public void process(String payload, boolean authenticated, String reqUser, String reqPassword) {
		try{
			Map<String, String> map = new HashMap<String, String>();
			logger.debug("Convert Json message to Map object.");

			if(payload.contains("configCompleteTimeStamp")){
				logger.info("** SKIPPNG DEVICE CONFIG MESSAGE *** ");
				return;
			}else if(payload.contains("geofenceNumber") || payload.contains("geofenceName") || payload.contains("alarmType") ){
				map = Utility.convertJsonToMap(payload);
			}else if (Config.SENSOR_LIMITE_ATTRIBUTE == 1){
				map = Utility.convertStringOutMap(payload);	
			}else {
				map = Utility.convertJsonToMap(payload);	
			}

			String deviceId = map.get("deviceIdentifier");
			if (deviceId == null) {
				deviceId = map.get("tdlUniqueId"); //Bosch
				if (deviceId == null) { 
					logger.info("** NO DEVICE ID FOUND IN THE INCOMING MESSAGE *** ");
					logger.info("payload: " + payload);
					return;
				} else {
					map.put("deviceIdentifier", deviceId);
				}
			}
			Boolean active = sendumDAO.checkIfDeviceIsActive(deviceId);
			if(!active){
				logger.info("** Device is not active *** " + deviceId);
				return;
			}
			Integer deviceTypeId = DeviceCache.getInstance().getDeviceTypeId(deviceId);
			if(deviceTypeId == null){
				logger.info("NO DEVICE MAPPED WITH THE DEVICE ID: " + deviceId);
				return;
			}

			if (!checkFilter(map, deviceId)) {
				logger.info("++ Filtered out message from device ID: " + deviceId);
				return;
			}

			//Remove invalid attribute from map
			removeInvalidAttributes(map);

			Integer orgId = deviceCache.getDeviceOrgId(deviceId);
			if (orgId == null){
				logger.error("Device "+ deviceId +" not mapped to any org");
				return;
			}
			
			if (!authenticated && (reqUser != null) && (reqPassword != null)) {
				if (!checkCredentials(deviceId, reqUser, reqPassword, orgId)) {
					logger.error("Kirsen authentication failed for org " + orgId + " (" + reqUser + "," + reqPassword + "), ignoring message");
					return;
				}
			}

			String containerId = AssociationCache.getInstance().getContainerId(deviceId);
			if (containerId == null) {
				logger.info("Device is not associated with any package: "+deviceId);
				return;
			}

			map.put("orgId", orgId.toString());
			map.put("containerId",containerId); // containerId ~ packageId
			String productId = AssociationDAO.getInstance().getProductIdForActiveAssociation(deviceId, containerId);
			map.put("productId", productId);
			map.put("deviceTypeId", deviceTypeId.toString());

			String deviceType = map.get("deviceType");
			if (map.containsKey("temperature")) {
				String celciusString = map.get("temperature");
				double temperature = Double.valueOf(celciusString);
				//For queclink server the deviceType value is present in map and is set to SensorConstant.SENSOR_QUECLINK
				if (deviceType!= null && ( deviceType.equalsIgnoreCase(SensorConstant.SENSOR_QUECLINK) || deviceType.equalsIgnoreCase(SensorConstant.SENSOR_KIRSEN) || deviceType.equalsIgnoreCase(SensorConstant.SENSOR_VERIZON))){
					temperature = celciusToFahrenheit(celciusString, orgId);
				}else if(deviceType.equalsIgnoreCase(SensorConstant.SENSOR_SENDUM)){
					// For sendum device following conversion is to be done
					temperature = deciCelciusToFahrenheit(celciusString, orgId);
				}
				map.put("temperature", new Double(temperature).toString());

				addBacterialGrowthLog(map);
			}
			if (map.containsKey("orientation") && !map.containsKey("tilt")) {
				String orientation = map.get("orientation");
				int tilt = calculateTilt(orientation);
				map.put("tilt", new Integer(tilt).toString());
			}
			if (map.containsKey("alarmType")) {
				String alarmType = map.get("alarmType");
				if(alarmType.toUpperCase().contains("TEMPERATURE"))
				{
					String alarmValue = "";
					alarmValue = map.get("alarmValue");
					double temperature = deciCelciusToFahrenheit(alarmValue, orgId);
					if (temperature != -999) {
						map.remove("alarmValue");
						map.put("alarmValue", new Double(temperature).toString());
					}
				}
			}
			calculateLightValue (map);

			//Skipping battery calculation as the value sent is already calculated value
			if(deviceType != null && !deviceType.equalsIgnoreCase(SensorConstant.SENSOR_KIRSEN)){
				calculateBatteryValue(map);
			}
			if (map.containsKey("payload")) { //Bosch
				String boschPayload = map.get("payload");
				String[] pload = boschPayload.split(",");
				for (String s : pload) {
					String[] inner = s.split("=");
					map.put(inner[0].trim(), inner[1].trim());
				}
				map.put("temperature", map.get("temperature_external"));
			}

			try {
				postForTransmission(map);

			} catch (Exception e) {
				logger.error("Exception " + e.getMessage() + " processing payload " + payload);
				e.printStackTrace();
			}
		}catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * If the value of Attribute is -999 then remove the key from map
	 * @param map
	 */

	private void removeInvalidAttributes(Map<String, String> map) {
		for(Iterator<Map.Entry<String, String>> it = map.entrySet().iterator(); it.hasNext(); ) {
			Map.Entry<String, String> entry = it.next();
			if(entry.getValue().equals("-999")) {
				it.remove();
			}
		}
	}

	private void postForTransmission(Map<String, String> map) 
	{
		try
		{
			String deviceId = map.get("deviceIdentifier");
			String packageId = map.get("containerId");
			String productId = map.get("productId");
			Integer orgId = Integer.valueOf(map.get("orgId"));
			Timer timer = new Timer();
			long recordId ;
			boolean notifyExcursion  = false;	

			DevicePackageStatus devicePackageStatus = new DevicePackageStatus();
			devicePackageStatus.setPackageId(packageId);
			devicePackageStatus.setDeviceId(deviceId);
			devicePackageStatus.setProductId(productId);
			if (map.containsKey("alarmTimeStamp")) {
				recordId = SendumDAO.getInstance().addSensorAlert(map);
				map.put("sensorRecordId", String.valueOf(recordId));
				ArrayList<String> excursionAttributes  = new ArrayList<String>();
				String attrName = NotificationService.processGeofenceAlerts(map);
				if(attrName != null){
					excursionAttributes.add(attrName);
				}
				if(map.get(NotificationConstant.ALARM_TYPE).toLowerCase().contains(RuleConstants.SENSOR_ATTR_SHOCK)) {
					excursionAttributes = RuleEvaluationService.evaluateRules(map);
				} else {
					excursionAttributes.addAll(NotificationService.processAttributeAlerts(map));
				}
				if(excursionAttributes.size() > 0){
					devicePackageStatus.setNofityExcursion(true);
					HashSet<String> attrSet = null;
					if(associationCache.getShipmentExcursionAttr(deviceId, packageId) != null)
					{
						attrSet = new HashSet<String>();
						attrSet.addAll(associationCache.getShipmentExcursionAttr(deviceId, packageId));
					}
					//get the new excursion attributes ie attributes for which there is no entry in excursion log table
					ArrayList<String> latestExcursionAttr = new ArrayList<String>();
					latestExcursionAttr.addAll(excursionAttributes);
					if(attrSet != null && attrSet.size() > 0){
						latestExcursionAttr.removeAll(attrSet);
					}
					if(latestExcursionAttr.size() > 0){
						ExcursionLog excursionLog = new ExcursionLog(deviceId, packageId, orgId);
						ExcursionLogDAO.getInstance().addExcursionLogs(excursionLog, latestExcursionAttr);
					}
				}else{
					devicePackageStatus.setNofityExcursion(false);
				}
				devicePackageStatus.setDataRecordId(recordId);
				devicePackageStatus.setAttributes(excursionAttributes);
				Utility.updateShipmentStatus(devicePackageStatus);

			}else {
				recordId = SendumDAO.getInstance().addSensorData(map);
				if(recordId != -1) {
					map.put("sensorRecordId", String.valueOf(recordId));
					//Get the list of excursion attributes till now
					HashSet<String> attrSet = null;
					if(associationCache.getShipmentExcursionAttr(deviceId, packageId) != null)
					{
						attrSet = new HashSet<String>();
						attrSet.addAll(associationCache.getShipmentExcursionAttr(deviceId, packageId));
					}
					//Get the list of excursion attributes for current data point
					ArrayList<String> excursionAttributes = RuleEvaluationService.evaluateRules(map);
					if(excursionAttributes!= null && excursionAttributes.size() >0){
						notifyExcursion = true;
						//get the new excursion attributes ie attributes for which there is no entry in excursion log table
						ArrayList<String> latestExcursionAttr = new ArrayList<String>();
						latestExcursionAttr.addAll(excursionAttributes);
						if(attrSet != null && attrSet.size() > 0){
							latestExcursionAttr.removeAll(attrSet);
						}
						ExcursionLog excursionLog = new ExcursionLog(deviceId, packageId, orgId);
						ExcursionLogDAO.getInstance().addExcursionLogs(excursionLog, latestExcursionAttr);
					}
					//generating same location device alert	if device is at same location for equal and more than (configured)5 min and notify count is less than or equals to 2 (configured)		
					if(sameLocationAlert(map)){
						NotificationService.processSameLocationAlert(map);
					}
					// geo point range alert
					processGeoPointRangeAlert(map);

					// update status of device-shipment pair
					devicePackageStatus.setNofityExcursion(notifyExcursion);
					devicePackageStatus.setDerivedExcursion(false);
					devicePackageStatus.setDataRecordId(recordId);
					devicePackageStatus.setAttributes(excursionAttributes);
					Utility.updateShipmentStatus(devicePackageStatus);
				}



			}
			logger.debug("Device data persisted in database in " + timer.getSeconds() + "s for " + deviceId );
		}
		catch(Exception ex)
		{
			ex.printStackTrace();
		}
	}

	/**
	 * @param map
	 * @throws Exception 
	 */
	private void processGeoPointRangeAlert(Map<String, String> attributeDataMap) throws Exception {
		// get current lat and lang value
		String currentLatString = attributeDataMap.get("latitude");
		String currentLangString = attributeDataMap.get("longitude");
		if(currentLatString != null && currentLatString.trim().length() != 0 && currentLangString != null && currentLangString.trim().length() != 0){
			// get the device Id
			String deviceId = attributeDataMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
			String containerId = attributeDataMap.get("containerId");
			String productId = attributeDataMap.get("productId");
			Double currentLat = Double.valueOf(currentLatString);
			Double currentLang = Double.valueOf(currentLangString);
			Double geoPointLat , geoPointLang;
			double distance;
			Integer radius;
			String geofenceNumber;
			Integer state;
			// get all the geo-points defined for this device
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, containerId);
			Collection<Geofence> geopoints = deviceCache.getGeopoints(profileId);
			if(geopoints != null){
				for(Geofence geopoint : geopoints){
					radius = geopoint.getRadius();
					if(radius != null){
						geoPointLat = geopoint.getLatitude();
						geoPointLang = geopoint.getLongitude();
						geofenceNumber = geopoint.getGeofenceNumber();
						// get device's state at a geopoint
						state = deviceCache.getGeoPointState(deviceId, geofenceNumber);
						// calculate the radius
						distance = Utility.getDistance(geoPointLat, geoPointLang, currentLat, currentLang);
						// if within the radius and if currentInOrOut is not equals lastInOut - raise the notification
						if(distance <= radius){
							//Check if device has entered within the radius of geopoint for the first time and raise ENTRY alert
							if(state == null || state != SensorConstant.DEVICE_ENTERED_GEOPOINT){

								String homeGeopointName = associationCache.getShipmentHomeGeopoint(profileId);
								if(homeGeopointName != null &&  !homeGeopointName.isEmpty() 
										&& geopoint.getPointName()!=null && !geopoint.getPointName().isEmpty() && homeGeopointName.equalsIgnoreCase(geopoint.getPointName())){
									Boolean leftHome = AssociationDAO.getInstance().getShipmentHomeEntryState(deviceId, containerId, productId);
									//check if shipment has ever left home location before
									if(leftHome){
										//If yes then raise geo point alert
										logger.info("Device " + deviceId + " within " + distance + " of " + geopoint.getPointName() + " - raising notification");
										attributeDataMap.put(NotificationConstant.GEO_FENCE_EVENT, NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY);
										attributeDataMap.put(NotificationConstant.GEO_FENCE_NUMBER, geofenceNumber);
										NotificationService.processGeoPointRangeAlert(attributeDataMap);
									}else{
										//If not, then this is the first time, so skip entry geo point alert for home location
										logger.info("Device " + deviceId + " within " + distance + " of " + geopoint.getPointName() + " - Not raising notification since it is landing at the home location for first time");
										AssociationDAO.getInstance().updateShipmentHomeEntry(deviceId, containerId, productId);
									}

								} else{
									logger.info("Device " + deviceId + " within " + distance + " of " + geopoint.getPointName() + " - raising notification");
									// raise notification
									attributeDataMap.put(NotificationConstant.GEO_FENCE_EVENT, NotificationConstant.GEO_FENCE_ALARM_MSG_ENTRY);
									attributeDataMap.put(NotificationConstant.GEO_FENCE_NUMBER, geofenceNumber);
									NotificationService.processGeoPointRangeAlert(attributeDataMap);
								}
								// Setting value to 1 to indicate that device has already processed for entry within the radius of geo point
								deviceCache.setGeopointState(deviceId, geofenceNumber, SensorConstant.DEVICE_ENTERED_GEOPOINT);
							}
						}else{
							// check if the device is moving out of geopoint radius, if so raise EXIT type of alert
							if(state!= null && state == SensorConstant.DEVICE_ENTERED_GEOPOINT){// checking it with 1 to identify if it had entered within the radius earlier
								logger.info("Device " + deviceId + " outside " + distance + " of " + geopoint.getPointName() + " - raising notification");
								// raise notification
								attributeDataMap.put(NotificationConstant.GEO_FENCE_EVENT, NotificationConstant.GEO_FENCE_ALARM_MSG_EXIT);
								attributeDataMap.put(NotificationConstant.GEO_FENCE_NUMBER, geofenceNumber);
								NotificationService.processGeoPointRangeAlert(attributeDataMap);
								// Setting value to 0 to indicate that device has exited from the radius of geo point
								deviceCache.setGeopointState(deviceId, geofenceNumber, SensorConstant.DEVICE_EXITED_GEOPOINT);
							}
						}
					}
				}
			}
		}
	}

	private int calculateTilt(String orientation) {
		int xAxis;
		int yAxis;
		int zAxis;
		double x2; // X squared (floating point for intermediate calculation)
		double y2; // Y squared (floating point for intermediate calculation)
		double resultant; // Resultant magnitude of X and Y vectors (floating
		// point for intermediate calculation)
		double rawtilt; // Pre-normalized value of tilt (before adjusting for
		// arctan range)
		int intTilt;

		String s[] = orientation.split(",");
		xAxis = Integer.parseInt(s[0]);
		yAxis = Integer.parseInt(s[1]);
		zAxis = Integer.parseInt(s[2]);

		x2 = (double) (xAxis * xAxis);
		y2 = (double) (yAxis * yAxis);
		resultant = Math.sqrt(x2 + y2);
		if (zAxis == 0) // To avoid division by 0, use 90 degrees for tilt if z
			// axis component of orientation is 0
		{
			rawtilt = 90;
		} else {
			rawtilt = Math.atan(resultant / zAxis);
			rawtilt = rawtilt * (360 / 2 / 3.1416); // Convert from radians to
			// degrees
		}
		intTilt = (int) rawtilt; // Get integral number of degrees
		if (zAxis < 0) {
			intTilt = intTilt + 180; // Normalize final tilt � if Z < 0, then
			// add 180 deg to allow for range of
			// arctan
		}
		return intTilt;
	}


	// converts sendum temp value which is C * 10 to F
	private double deciCelciusToFahrenheit(String deciCelciusString, int orgId) {
		double temperature = 0;
		try
		{
			double tempinCel = Double.parseDouble(deciCelciusString);
			String format = ConfigCache.getInstance().getOrgConfig("sensor.temperature.format", orgId);
			if(SensorConstant.TEMPERATURE_FORMAT_CELSIUS.equalsIgnoreCase(format)){
				temperature = tempinCel / 10d;
			}else{
				tempinCel = tempinCel / 10d;
				temperature = ((tempinCel * 9d) / 5.0d) + 32d;
			}
		}
		catch(Exception ex)
		{
			logger.error("deciCelciusToFahrenheit failed to parse " + deciCelciusString);		
			return(-999);
		}
		return temperature;
	}

	// converts celcius to fahrenheit
	private double celciusToFahrenheit(String celciusString, int orgId) {
		double temperature = 0;
		double tempinCel = 0;
		try {
			tempinCel = Double.parseDouble(celciusString);
			String format = ConfigCache.getInstance().getOrgConfig("sensor.temperature.format", orgId);
			if (SensorConstant.TEMPERATURE_FORMAT_CELSIUS.equalsIgnoreCase(format)){
				temperature = tempinCel;
			}else{
				temperature = ((tempinCel * 9d) / 5.0d) + 32d;
			}
			logger.debug("Ambient Temperature - celciusToFahrenheit " + celciusString  + " -> " + temperature + format);		
		} catch (Exception e) {
			logger.error("celciusToFahrenheit failed to parse " + celciusString);		
			return(Double.NaN);
		}
		return temperature;
	}


	/**
	 * 
	 * @param map
	 */
	/*private void addBacterialGrowthLog(Map<String, String> map){
		String deviceId = map.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
		Attribute attribute = deviceCache.getAttribute(deviceId, "growthLog");
		// check if we need to calculate Bacterial Growth Log, if attribute is not null that means we have configured cumulativeLog for this device
		if(attribute != null){

			DateTime  startTime;
			DateTime  endTime;
			String temp = (String) map.get("temperature");
			BigDecimal growthLog = CustomShellfishLogs.getLogsPerMinute(Double.parseDouble(temp));

			// log per sec ,divide by 60
			//growthLog = growthLog / 60;
			growthLog = growthLog.divide(new BigDecimal(60));

			// Getting the given statusTimeStamp field and converting it joda dateTime
			DateTime statusTimeStamp;
			if(Config.SYSTEM_TIME_FLAG == Config.SYSTEM_TIME_FLAG_CUSTOM){
				String timeStamp = map.get("statusTimeStamp");
				DateTimeFormatter formatter = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
				statusTimeStamp = formatter.parseDateTime(timeStamp);
			}else{
				statusTimeStamp = new DateTime();
			}
			DeviceInfo deviceInfo = deviceAttributeLogCache.getDeviceBacterialLog(deviceId);
			if (deviceInfo != null) {
				startTime =  deviceInfo.getEndTime();	
				endTime = statusTimeStamp;
				deviceInfo.setDeviceId(deviceId);
				deviceInfo.setStartTime(startTime);
				deviceInfo.setEndTime(endTime);
				deviceAttributeLogCache.putDeviceBacterialLog(deviceId, deviceInfo);
			}else{
				deviceInfo =new DeviceInfo();
				startTime = statusTimeStamp;
				deviceInfo.setDeviceId(deviceId);
				deviceInfo.setStartTime(startTime);
				deviceInfo.setEndTime(startTime);
				deviceAttributeLogCache.putDeviceBacterialLog(deviceId, deviceInfo);
			}

			//calculate duration in seconds
			int duration = Timer.printDifferenceTwoDates(deviceInfo.getStartTime(),deviceInfo.getEndTime());

			// growth log = logPerSec * duration in secs
			//growthLog = growthLog * duration;
			growthLog = growthLog.multiply(new BigDecimal(duration));

			String strGrowthLog = df.format(growthLog);

			sendumDAO.addBacterialGrowthLog(deviceId, temp, strGrowthLog,deviceInfo);
			String cumulativeLog = sendumDAO.getBacterialGrowthSummary(deviceId);
			map.put("cumulativeLog", cumulativeLog);
			map.put("growthLog", strGrowthLog);
		}
	}*/


	/**
	 * @param map
	 */
	private void addBacterialGrowthLog(Map<String, String> map){
		CloseableHttpClient httpClient = null;
		try{
			String deviceId = map.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
			Attribute attribute = deviceCache.getAttribute(deviceId, "growthLog");

			// check if we need to calculate Bacterial Growth Log, if attribute is not null that means we have configured cumulativeLog for this device
			if(attribute != null){
				DeviceInfo deviceInfo = deviceAttributeLogCache.getDeviceBacterialLog(deviceId);
				BactGrowthRequest bgBean = new BactGrowthRequest();
				bgBean.setAttribute(attribute);
				bgBean.setDeviceInfo(deviceInfo);
				bgBean.setDataMap(map);

				// make web service call
				PoolingHttpClientConnectionManager connManager = ConnectionUtil.getInstance().getConnectionManager();
				httpClient = HttpClients.custom().setConnectionManager(connManager).build();	
				int CONNECTION_TIMEOUT_MS = 7 * 1000; // Timeout in millis. 7 sec, default value
				if(configCache.getSystemConfig("bact.growth.module.connection.timeout") != null){
					CONNECTION_TIMEOUT_MS = Integer.valueOf(configCache.getSystemConfig("bact.growth.module.connection.timeout")) *1000;
				}
				RequestConfig requestConfig = RequestConfig.custom().setConnectionRequestTimeout(CONNECTION_TIMEOUT_MS)
						.setConnectTimeout(CONNECTION_TIMEOUT_MS).setSocketTimeout(CONNECTION_TIMEOUT_MS).build();
				
				String url = configCache.getSystemConfig("bact.growth.module.url");
				HttpPost httpPost = new HttpPost(url);
				httpPost.setConfig(requestConfig);
				httpPost.setHeader("Content-type", "application/json");

				ObjectMapper mapper = new ObjectMapper();
				String jsonInString = mapper.writeValueAsString(bgBean);
				mapper.setSerializationInclusion(Include.NON_NULL);
				StringEntity stringEntity = new StringEntity(jsonInString);
				httpPost.setEntity(stringEntity);

				HttpResponse response = httpClient.execute(httpPost);

				if(response.getStatusLine().getStatusCode() == 200){
					String json = EntityUtils.toString(response.getEntity());
					BactGrowthResponse resp = mapper.readValue(json, BactGrowthResponse.class);
					if(SensorConstant.RESP_STAT_SUCCESS.equals(resp.getResponseStatus())){
						//save updated deviceInfo in cache
						deviceAttributeLogCache.putDeviceBacterialLog(deviceId, resp.getDeviceInfo());
						if(resp.getDataMap() != null){
							map.put("cumulativeLog",resp.getDataMap().get("cumulativeLog"));
							map.put("growthLog",resp.getDataMap().get("growthLog"));
						}
					}
					else{
						logger.error("Error while processing bacterial growth:: " + resp.getResponseMessage());
					}
				}else{
					String msg = EntityUtils.toString(response.getEntity());
					logger.error("Error in connecting to Bacterial Growth calculation Module" +msg);
				}
			}
		}
		catch(Exception e){
			logger.error("Error while calculating bacterial Growth log:: "+e.getMessage());
			e.printStackTrace();
		}
	}

	/**
	 * @author SACHINKOHALE
	 * Checking sensor message whether it is received from same location based on the configured time.
	 * if the message is received more than the configured time from same location based on boundary, send alert message.
	 * @param device
	 * @param map
	 * @return
	 */	
	public boolean sameLocationAlert(Map<String, String> map){
		boolean isSameLocation = false;
		try{		

			DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

			String deviceId = map.get("deviceIdentifier");		
			Sensor sensor = deviceCache.getDeviceLastLocation(deviceId);				

			if(sensor != null){
				//get excursion_duration for latitude and multiply_factor i.e. radius
				com.rfxcel.sensor.util.Attribute attribute = deviceCache.getAttribute(deviceId, "latitude");
				double radius = attribute.getMultiplyFactor();
				//if radius > 0 and excursion_duration > 0 then alert will be ON
				if(attribute != null && attribute.getExcursionDuration() > 0 && radius > 0.0){
					double lon = 0d;
					double lat = 0d;	
					double sensorLastLat = 0d;
					double sensorLastLang = 0d;

					if(map.containsKey("latitude")){
						lat = Double.parseDouble((String)map.get("latitude"));
					}
					if(map.containsKey("longitude")){
						lon = Double.parseDouble((String)map.get("longitude"));
					}

					sensorLastLat = Double.parseDouble(sensor.getLatitude());
					sensorLastLang = Double.parseDouble(sensor.getLongitude());	

					double distance = 0;
					//getDistance between previous lat, lon and request message lat, lon
					distance = Utility.getDistance(sensorLastLat, sensorLastLang, lat, lon);					

					//If at distance between lat, lons is less than radius then check for how long it is there at boundary area 
					if(distance <= radius){
						Timer timer = new Timer();
						int minutes = 0;						
						minutes = timer.getDifferenceInMinuteOfTwoDates(dateFormat.parse(sensor.getStatusTimeStamp()), dateFormat.parse(map.get("statusTimeStamp")));

						//check if this time is greater than excursion time configured
						if(minutes >= attribute.getExcursionDuration()){
							//check the notification limit for same location alert
							boolean raiseNotification = calculateSameLocationNotifyLimit(map);
							if(raiseNotification){
								map.put("timeDuration", minutes+"");
								isSameLocation = true;
							}
						}		

					}else if(map.containsKey("latitude") && map.containsKey("longitude")){
						//distance between lat, lons is greater than radius so clear device Logs if any and insert new lat, lang and timestamp in sensor cache
						DeviceInfo newDeviceInfo = new DeviceInfo();
						newDeviceInfo.setDeviceId(deviceId);
						newDeviceInfo.setDeviceLogType(2);
						deviceAttributeLogCache.removeSameLocationNotificationLimitLog(deviceId);
						sendumDAO.removeDeviceLog(newDeviceInfo,"latitude");	

						//insert into sensor location cache for a device
						Sensor newSensor = new Sensor();
						newSensor.setLatitude((String)map.get("latitude"));
						newSensor.setLongitude((String)map.get("longitude"));
						newSensor.setDeviceIdentifier(deviceId);

						//Change it to server timezone based date as comparisions are on server time zone formats	
						newSensor.setStatusTimeStamp(map.get("statusTimeStamp")); 

						deviceCache.putDeviceLastLocation(deviceId, newSensor);							

					}
				}
			}else if(map.containsKey("latitude") && map.containsKey("longitude")){
				//insert into sensor location cache for a device
				Sensor sensorInfo = new Sensor();
				sensorInfo.setLatitude((String)map.get("latitude"));
				sensorInfo.setLongitude((String)map.get("longitude"));
				sensorInfo.setDeviceIdentifier(deviceId);			

				//Change it to server timezone based date as comparisons are on server time zone formats	
				sensorInfo.setStatusTimeStamp(map.get("statusTimeStamp")); 						

				deviceCache.putDeviceLastLocation(deviceId, sensorInfo);		
			}
		}catch(Exception e){
			logger.error("Error while processing same location alert "+e.getMessage());
		}

		return isSameLocation;
	}

	/**
	 * This method will multiply the light value by a number (configured) and replace the original value with multiplied value
	 * @param device
	 * @param map
	 */
	private void calculateLightValue(Map<String, String> map){
		DecimalFormat df = new DecimalFormat("#0.##");
		double lightVal = 0.0;
		if(map.containsKey(SensorConstant.ATTRIBUTE_NAME_LIGHT)) {			
			if(deviceCache.getAttribute(map.get("deviceIdentifier"), SensorConstant.ATTRIBUTE_NAME_LIGHT) != null){				
				lightVal = Double.parseDouble(map.get(SensorConstant.ATTRIBUTE_NAME_LIGHT)) * deviceCache.getAttribute(map.get("deviceIdentifier"), SensorConstant.ATTRIBUTE_NAME_LIGHT).getMultiplyFactor();
			}else{
				lightVal = Double.parseDouble(map.get(SensorConstant.ATTRIBUTE_NAME_LIGHT));
			}

			String roundVal = df.format(lightVal);
			map.remove(SensorConstant.ATTRIBUTE_NAME_LIGHT);
			map.put(SensorConstant.ATTRIBUTE_NAME_LIGHT,roundVal);

		}
	}	
	/**
	 * This method will calculate battery value using capacityRemaining and capacityFull attributes(Eg. round((capacityRemaining/capacityFull) * 100))
	 * @param map
	 */
	private void calculateBatteryValue(Map<String, String> map){
		String deviceId = map.get("deviceIdentifier");
		DecimalFormat df = new DecimalFormat("#0.##");
		if(map.containsKey(SensorConstant.ATTRIBUTE_NAME_CAPACITY_REM)) {
			double lc ;

			if(deviceCache.getBatteryValue(deviceId) == null){
				lc = SensorConstant.BATTERY_DEFAULT_VALUE;					
			}else{
				lc = deviceCache.getBatteryValue(deviceId);
			}				

			double batteryVal = Double.parseDouble(map.get(SensorConstant.ATTRIBUTE_NAME_CAPACITY_REM))/lc  * 100;
			String roundVal = df.format(batteryVal);
			map.put(SensorConstant.ATTRIBUTE_NAME_BATTERY,roundVal);

		}
	}	

	/**
	 * This method will calculate the numbers of notifications to be created in rTS for same location devices
	 * 
	 * @param map
	 * @return
	 */
	private boolean calculateSameLocationNotifyLimit(Map<String, String> map){
		boolean raiseNotification = false;
		String deviceId = map.get("deviceIdentifier");
		com.rfxcel.sensor.util.Attribute attribute = deviceCache.getAttribute(deviceId, "latitude");

		if(attribute!= null && attribute.getNotifyLimit() > 0 ){
			DeviceInfo newDeviceInfo = deviceAttributeLogCache.getSameLocationLimitLog(deviceId);
			if(newDeviceInfo == null ) {
				newDeviceInfo = new DeviceInfo();
				newDeviceInfo.setDeviceId(deviceId);
				newDeviceInfo.setAttributeName("latitude");				
				newDeviceInfo.setStartTime(new DateTime());
				newDeviceInfo.setEndTime(new DateTime());
				newDeviceInfo.setDeviceLogType(2);
				newDeviceInfo.setNotifyCount(1);
				deviceAttributeLogCache.putSameLocationLimitLog(deviceId, newDeviceInfo);
				sendumDAO.addDeviceLog(newDeviceInfo);
			}else{
				newDeviceInfo.setNotifyCount(newDeviceInfo.getNotifyCount() + 1);
				newDeviceInfo.setEndTime(new DateTime());
				newDeviceInfo.setAttributeName("latitude");	
				deviceAttributeLogCache.putSameLocationLimitLog(deviceId, newDeviceInfo);
				sendumDAO.updateDeviceLog(newDeviceInfo);
			}
			//If notification count reaches the max number configured for an attribute ,skip creating notification
			if(newDeviceInfo.getNotifyCount() <= attribute.getNotifyLimit()){
				raiseNotification = true;
			}			
		}
		return raiseNotification;
	}

}
