package com.rfxcel.sensor.service;

import java.io.InputStream;
import java.math.BigInteger;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.MultivaluedMap;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.jboss.resteasy.plugins.providers.multipart.InputPart;
import org.jboss.resteasy.plugins.providers.multipart.MultipartFormDataInput;
import org.json.JSONArray;
import org.json.JSONObject;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceAttributeLogCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.integration.dao.SensorDAO;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.dao.OrganizationDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.entity.Organization;
import com.rfxcel.org.service.OrganizationService;
import com.rfxcel.org.service.UserService;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.beans.DeviceData;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.beans.Product;
import com.rfxcel.sensor.beans.ProductDetail;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.control.DeviceController;
import com.rfxcel.sensor.control.EventController;
import com.rfxcel.sensor.control.ItemController;
import com.rfxcel.sensor.control.KirsenController;
import com.rfxcel.sensor.control.RESTClient;
import com.rfxcel.sensor.control.VerizonDeviceActivator;
import com.rfxcel.sensor.control.VerizonDeviceDeactivator;
import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.dao.CommissionDAO;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.dao.DeviceTypeDAO;
import com.rfxcel.sensor.dao.ProfileDAO;
import com.rfxcel.sensor.dao.SearchDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.ProfileRequest;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

/**
 * 
 *@author ashish_kumar1
 * @since 2016-10-10
 *
 */
@Path("sensor")
public class SensorService {	
	private static Logger logger = Logger.getLogger(SensorService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String VALIDATION_TYPE_DEVICE = "device";
	private static final String VALIDATION_TYPE_PACKAGE = "package";
	private static final String VALIDATION_TYPE_DEVICE_PACKAGE = "device-package";

	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static SendumDAO sendumDAO = SendumDAO.getInstance();
	private static AssociationCache assoCache = AssociationCache.getInstance();
	private static DeviceCache deviceCache = DeviceCache.getInstance() ;
	private static DeviceAttributeLogCache attrLogcache = DeviceAttributeLogCache.getInstance();
	private static CommissionDAO commissionDAO = CommissionDAO.getInstance();
	private static AssociationDAO associationDAO = AssociationDAO.getInstance();
	private static ProfileDAO profileDAO = ProfileDAO.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	
	/**
	 * Default constructor
	 */
	public SensorService() {
		//simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); -- check if we need to set response time as a UTC value 
	}

	/**
	 * <p>
	 * This service returns the data points for an attribute .
	 * URI to call this service
	 * 'http://serverIP:port-number/root_name/sensor/getAttrData'
	 * </p>
	 * @param request It contains itemId,attributeType,startDate and endDate 
	 * @return The Response containing dataPoints based on attributeType
	 */
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/getAttrData")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAttrData(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			DeviceData deviceData= request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}

			String user = TokenManager.getInstance().getUserForToken(httpRequest);
			request.setUser(user);
			
			//check the Elastic search fetch is true or false
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			fetchESData = false; //Arun: disable getting attribute data from elasticsearch for now
			
			String responseMsg = "";
			if(fetchESData){
				res = SearchDAO.getInstance().getAttributeData(request);
				if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
					responseMsg = res.getResponseMessage();
					res.setResponseMessage(responseMsg);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
			}else{
				res = sendumDAO.getAttributeData(request);
			}		

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getAttrData "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getAttrData  Request :: "+request +" Response :: "+res );
		}
	}

	/**
	 * <p>
	 * This service returns data for map (an array of latitude an longitude).
	 * URI to call this service
	 * 'http://serverIP:port-number/root_name/sensor/getMapData'
	 * </p>
	 * @param request It contains itemId,startDate and endDate 
	 * @return Response containing locations array with latitude and longitude wrapped in map object
	 */
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/getMapData")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getMapDataByDeviceNPackageId(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			DeviceData deviceData = request.getDeviceData();
			//check rtsDataFetch flag for EMS demo			
			Boolean rtsDataFetch = false;
			if(request.getRtsDataFetch() != null){
				rtsDataFetch = request.getRtsDataFetch();
			}
			
			if(!rtsDataFetch){
				res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE, orgId);
				if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
				
				//check the Elastic search fetch is true or false
				String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
				Boolean fetchESData = false;
				if(fetchESDataString != null){
					fetchESData = Boolean.valueOf(fetchESDataString);
				}
				
				String responseMsg = "";
				fetchESData = false;
				if (fetchESData) {
				res = SearchDAO.getInstance().getMapDataByDeviceNPackageId(request);
					if (SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())) {
						responseMsg = res.getResponseMessage();
						res.setResponseMessage(responseMsg);
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
						return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
					}
				} else {
					res = sendumDAO.getMapDataByDeviceNPackageId(request);
				}
			}else{
				String searchType = request.getSearchType();
				// EMS demo - to show eventList, itemList
				if(searchType == null || searchType.trim().length()==0 ){
					res.setResponseMessage("Please provide proper search type.");
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
				//4-event-map2, 5-item-map2
				if(searchType.equalsIgnoreCase("4")){
					String eventId = deviceData.getProductId();					
					String traceItemId = EventController.getInstance().getParentItemByEventId(eventId, orgId);
					//String deviceId = EventController.getInstance().getDeviceByEventId(eventId);
					res = EventController.getInstance().getMapItemLocations(traceItemId, eventId, orgId);
					//Connection conn = DAOUtility.getInstance().getConnection();
					//com.rfxcel.sensor.beans.Map map = SendumDAO.getInstance().getAllMapData(conn, deviceId, deviceData.getProductId(), deviceData.getProductId());
				}else if(searchType.equalsIgnoreCase("5")){
					// get item-map2 data
					String traceItemId = deviceData.getDeviceId();
					res = ItemController.getInstance().getMapItemLocations(traceItemId, orgId);
				}
				
			}
					
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getMapData "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getMapData "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getMapData  Request :: "+request +" Response :: "+res );
		}

	}

	/**
	 * <p>
	 * This service returns a list of attributes for a device.
	 * URI to call this service
	 * 'http://serverIP:port-number/root_name/sensor/getAttrInfo'
	 * </p>
	 * @param request It contains itemId
	 * @return Response containing array of attributes for device
	 */
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/getAttrInfo")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAttributeInfo(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}

			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			DeviceData deviceData = request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}

			//check the Elastic search fetch is true or false
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			fetchESData = false; //Arun: disabling elasticsearch for attribute info
			String responseMsg = "";
			if(fetchESData){
				res = SearchDAO.getInstance().getAttributeList(request);
				if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
					responseMsg = res.getResponseMessage();
					res.setResponseMessage(responseMsg);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
			}else{
				res = sendumDAO.getAttributeList(request);
			}
			//retrieve the traceId from rTS
			//res.setTraceId(ItemController.getInstance().getItemTraceId(orgId, request.getDeviceData().getProductId(), request.getDeviceData().getPackageId()));
			if (request.getDeviceData().getRtsItemId() != null && request.getDeviceData().getRtsSerialNumber() != null) {
				//retrieve the traceId from rTS
				res.setTraceId(ItemController.getInstance().getItemTraceId(orgId, request.getDeviceData().getRtsItemId(), request.getDeviceData().getRtsSerialNumber()));
			}else{
				res.setTraceId(ItemController.getInstance().getItemTraceId(orgId, request.getDeviceData().getProductId(), request.getDeviceData().getPackageId()));

			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		} catch (SQLException e) {
			logger.error("Exception in getAttrInfo "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getAttrInfo "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getAttrInfo  Request :: "+request +" Response :: "+res );
		}

	}
	
	//get device list by name
	/*@POST
	@Path("/getDeviceByName")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getItemsByName(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			Sensor sensor=new Sensor();
			List<DeviceDetail> deviceDetailList = new ArrayList<DeviceDetail>();
			deviceDetailList=CommissionDAO.getInstance().getItemsByName(request.getDeviceList());
			sensor.setDeviceList(deviceDetailList);
			if(deviceDetailList.size()<1){
				res.setResponseMessage("Records not found");
			}
			res.setSensor(sensor);
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in getDeviceByName "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getDeviceByName  Request :: "+request +" Response :: "+res );
		}
	}
*/

	// NOTE : API is updated for multitenancy
	@POST
	@Path("/commission")
	@Consumes("application/json")
	@Produces("application/json")
	public Response commissionDevices(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = request.getOrgId();
			Boolean authenticate = getAuthenticate();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest,orgId);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			List<DeviceDetail> deviceDetails = request.getItemListDetails();
			if(deviceDetails == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Please provide device details")).build();
			}
			
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			// Group ID is fetched from cache so not validating it
			Long groupId = TokenManager.getInstance().getGroupId(httpRequest);
			
			Boolean isPresent;
			String deviceType = null;
			String deviceId = null;
			Boolean deviceIsPresent;
			Boolean deviceIsDecommissioned;
			ArrayList<DeviceDetail> decommissionedDevices = new ArrayList<DeviceDetail>();
			ArrayList<DeviceDetail> verizonDevices = new ArrayList<DeviceDetail>();
			ArrayList<DeviceDetail> kirsenDevices = new ArrayList<DeviceDetail>();
			for(DeviceDetail deviceDetail :deviceDetails){
				deviceType = deviceDetail.getDeviceType();
				if(deviceType == null){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type cannot be null")).build();
				}
				isPresent = sendumDAO.checkIfDeviceTypeIsPresent(deviceType);
				if(!isPresent){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type "+deviceType +" is not configured in system")).build();
				}
				deviceId = deviceDetail.getDeviceId();
				if(deviceId== null || deviceId.trim().length() == 0){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id cannot be null or empty")).build();
				}
				deviceId = deviceId.trim();
				//Check if device id is present
				deviceIsPresent = sendumDAO.checkIfDeviceIsPresent(deviceId);
				if(deviceIsPresent){
					//check if it is in deCommissioned state for a given organization
					deviceIsDecommissioned = sendumDAO.checkIfDeviceIsDeCommissioned(deviceId, orgId);
					if(!deviceIsDecommissioned){
						Integer deviceOrgId = deviceCache.getDeviceOrgId(deviceId);
						Organization organization = OrganizationDAO.getInstance().getOrganizationById(deviceOrgId);
						return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device ID "+deviceId +" is already commissioned in organization " + (organization!=null ? organization.getName():""))).build();
					}else{
						decommissionedDevices.add(deviceDetail);
					}
				}
				//check if device is of manufacturer Verizon
				String deviceManufacturer = DeviceTypeDAO.getInstance().getDeviceTypeManufacturer(orgId, deviceType);				
				String deviceKey = deviceDetail.getDeviceKey();
				if(deviceManufacturer.equalsIgnoreCase(SensorConstant.SENSOR_VERIZON)){
					deviceDetail.setActivationState(0);//activation status 0 ~ pending
					if ((deviceKey != null) && deviceKey.equalsIgnoreCase(SensorConstant.DEVICE_KEY_DEMO)) {
						deviceDetail.setActivationState(1);//activation status 1 ~ active
					}
					verizonDevices.add(deviceDetail);					
				}
				// If device type is Kirsen and device key is null then add it to the list so that deviceKey is updated after commission
				if(deviceManufacturer.equalsIgnoreCase(SensorConstant.SENSOR_KIRSEN) && (deviceKey == null || deviceKey.trim().length() == 0 )){
					kirsenDevices.add(deviceDetail);
				}
			}
			//adding verizon devices
			if(verizonDevices.size() > 0){
				//deviceDetails.removeAll(verizonDevices);
				for(DeviceDetail deviceDetail : verizonDevices){
					String deviceKey = deviceDetail.getDeviceKey();
					if ((deviceKey != null) && deviceKey.equalsIgnoreCase(SensorConstant.DEVICE_KEY_DEMO)) continue;
				    //adding to thingspace url
				    addVerizonDevice(orgId, deviceDetail);
			
				}
			}
			deviceDetails.removeAll(decommissionedDevices);// remove devices with state deCommission
			res = commissionDAO.commissionDevices(deviceDetails, decommissionedDevices, orgId, groupId);
			
			//commission device in rTS
			EventController.getInstance().createDeviceCommission(orgId, deviceId, deviceType);
			
			//Audit log related code
			
			if (res.getResponseStatus().equalsIgnoreCase(SensorConstant.RESP_STAT_SUCCESS)) {
				String userName = tokenManager.getUserForToken(httpRequest);
				Long userId = tokenManager.getUserId(httpRequest);
				AuditLog auditLog = new AuditLog(userId, userName, null, "Device", "Create", null, request.getItemListDetails().toString(), "User "+userName +" has commissioned device(s)", orgId);
				auditLogDao.addAuditLog(auditLog);
			}
			
			// update kirsen devices where deviceKey is missing
			if(kirsenDevices.size() > 0){
				SensorResponse updateResponse = updateKirsenDeviceKeys(orgId, kirsenDevices);
				if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(updateResponse.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(updateResponse).build();
				}
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in commission Devices "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: commission  Request :: "+request +" Response :: "+res );
		}
	}



	//get device list by ID
	/*@POST
	@Path("/getDeviceByID")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getItemsByID(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			List<DeviceDetail> deviceDetailList = new ArrayList<DeviceDetail>();
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			Sensor sensor=new Sensor();
			deviceDetailList=AssociationDAO.getInstance().getItemsByID(request.getDeviceList());
			sensor.setDeviceList(deviceDetailList);

			if(deviceDetailList.size()< 1){
				res.setResponseStatus("No Records Found");
			}
			res.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in getDeviceByID "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getDeviceByID.  Request :: "+request +" Response :: "+res );
		}
	}*/


	// NOTE : API is updated for multitenancy
	@POST
	@Path("/associate")
	@Consumes("application/json")
	@Produces("application/json")
	public Response associateItems(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		DeviceData deviceData;
		try {
			Boolean authenticate = getAuthenticate();
			Integer orgId = request.getOrgId();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			// GroupId is fetched from cache so not validating it
			Long groupId = TokenManager.getInstance().getGroupId(httpRequest);
			/*resp = UserService.validateGroup(groupId);
			if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
*/			
			deviceData = request.getDeviceData();
			if (deviceData == null) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Data is missing");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			String deviceId = deviceData.getDeviceId();
			String packageId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			
			if ((productId == null || productId.trim().length()==0) && (packageId == null || packageId.trim().length()==0)) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("at least one of product Id or package Id must be provided");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			
			if (productId == null || productId.trim().length()==0) {
				productId = AssociationDAO.getInstance().getProductIdForAssociation(deviceId, packageId, orgId);
				if (productId == null) {
					logger.error("unable to get productId for deviceId=" + deviceId + " packageId=" + packageId + " orgId=" + orgId);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("unable to get productId successfully");
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
				deviceData.setProductId(productId);
			}
			
			if (packageId == null || packageId.trim().length()==0) {
				packageId = AssociationDAO.getInstance().getPackageIdForAssociation(deviceId, productId, orgId);
				if (packageId == null) {
					logger.error("unable to get packageId for deviceId=" + deviceId + " productId=" + productId + " orgId=" + orgId);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("unable to get packageId successfully");
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
				deviceData.setPackageId(packageId);
			}

			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			//check if productId and packageId is part of prior shipment
			boolean isPart = AssociationDAO.getInstance().isProductAndPackagePartOfPriorShipment(productId, packageId, orgId);
			if (isPart) {
				logger.error("productId and packageId is part of prior shipment, productId=" + productId + " packageId=" + packageId + " orgId=" + orgId);
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("A shipment with the combination of product and serial number has already been created.");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			//check if device is commissioned or disassociated
			Boolean isReady = sendumDAO.isDeviceReadyToBeAssociated(deviceId, orgId);
			if(!isReady){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id " +deviceId +" is not in commissioned or disassociated state")).build();
			}

			isReady = sendumDAO.isPackageReadyToBeAssociated(packageId, productId, orgId);
			if(!isReady){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package Id " +packageId +" is not in commissioned or disassociated state")).build();
			}
			Integer profileId = deviceData.getProfileId();
			if ((profileId == null) || (profileId.intValue() < 0)) {
				profileId = AssociationDAO.getInstance().getProfileIdForAssociation(deviceId, productId, packageId, orgId);
				if (profileId != null) {
					deviceData.setProfileId(profileId);
				}
			}
			//Associate device and package
			AssociationDAO.getInstance().associateItems(deviceId,packageId,productId, orgId, groupId, profileId, deviceData.getRtsItemId(), deviceData.getRtsSerialNumber());
			
			//Populate cache after association
			AssociationCache.getInstance().addAssociation(deviceId, packageId);
			AssociationCache.getInstance().addAssociationStatus(deviceId, packageId);
			AssociationCache.getInstance().removeShipmentExcursion(deviceId, packageId);
			
			if(profileId != null){
				AssociationCache.getInstance().addActiveShipmentProfileId(deviceId, packageId, profileId);
			}
			
			if (request.getProductDetail() == null) {
				sendumDAO.activateTransHistory(deviceId, productId, packageId);
			}
			
			String userName = tokenManager.getUserForToken(httpRequest);
			Long userId = tokenManager.getUserId(httpRequest);
			String logMsg = "This shipment < product Id = "+productId+", package Id = "+ packageId+" > has been associated with deviceId < "+deviceId +" >";
			AuditLog auditLog = new AuditLog(userId, userName, null, "Shipment", "Create", null, deviceData.toJson(),logMsg, orgId);
			auditLogDao.addAuditLog(auditLog);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device "+deviceId +" is successfully associated to package Id "+packageId)).build();
		} catch (Exception e) {
			logger.error("Exception in associate "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: associate.  Request :: "+request +" Response :: "+res );
		}
	}

	//end close notifications
	//End Sensor Notification Process		
/*	@POST
	@Path("/getDeviceStatus")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getDeviceStatus(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Boolean authenticate = getAuthenticate();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			Integer orgId = request.getOrgId();
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			DeviceData deviceData = request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}

			res = SendumDAO.getInstance().getDeviceStatus(request);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		} catch (SQLException e) {
			logger.error("Exception in getDeviceStatus "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getDeviceStatus "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getDeviceStatus.  Request :: "+request +" Response :: "+res );
		}
	}*/


	@POST
	@Path("/disassociate")
	@Consumes("application/json")
	@Produces("application/json")
	public Response disAssociateItems(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			Boolean authenticate = getAuthenticate();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			DeviceData deviceData = request.getDeviceData();
			if (deviceData == null) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Data is missing");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}	
			
			String deviceId = deviceData.getDeviceId();
			String packageId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			
			if ((productId == null || productId.trim().length()==0) && (packageId == null || packageId.trim().length()==0)) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("at least one of product Id or package Id must be provided");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			if (productId == null || productId.trim().length()==0) {
				productId = AssociationDAO.getInstance().getProductIdForAssociation(deviceId, packageId, orgId);
				if (productId == null) {
					logger.error("unable to get productId for deviceId=" + deviceId + " packageId=" + packageId + " orgId=" + orgId);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("unable to get productId successfully");
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
				deviceData.setProductId(productId);
			}
			
			if (packageId == null || packageId.trim().length()==0) {
				packageId = AssociationDAO.getInstance().getPackageIdForAssociation(deviceId, productId, orgId);
				if (packageId == null) {
					logger.error("unable to get packageId for deviceId=" + deviceId + " productId=" + productId + " orgId=" + orgId);
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("unable to get packageId successfully");
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
				}
				deviceData.setPackageId(packageId);
			}
		
			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			//check if device is associated
			Boolean associated = sendumDAO.checkIfDeviceIsAssociated(deviceId, orgId);
			if(!associated){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id " +deviceId +" is not in Associated State")).build();
			}
			//Check if package is associated
			associated = sendumDAO.checkIfPackageIsAssociated(packageId, productId, orgId);
			if(!associated){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package Id " +packageId +" for product "+ productId+" is not in Associated")).build();
			}
			//DisAssociate device and package
			associationDAO.disAssociateItems(deviceId,packageId,productId, orgId);
			
			
			//create receive event in rTS
			//EventController.getInstance().createReceiveEvent(orgId, productId, packageId, deviceId);
			
			//dis-associate device from product in rTS
			EventController.getInstance().performDeviceDisAssociation(orgId, deviceId);

			//Populate cache after disAssociation
			assoCache.removeAssociation(deviceId);	
			assoCache.removeAssociationStatus(deviceId, packageId);
			assoCache.removeActiveShipmentProfileMapping(deviceId, packageId);
			assoCache.removeShipmentExcursion(deviceId, packageId);
			// removing bacterial log records from DB and cache
			attrLogcache.removeDeviceBacterialGrowthLog(deviceId);
			attrLogcache.removeNotificationLimitLog(deviceId);
			attrLogcache.removeExcursionTimeLog(deviceId);
			deviceCache.resetGeopointsState(deviceId);
			
			String userName = tokenManager.getUserForToken(httpRequest);
			Long userId = tokenManager.getUserId(httpRequest);
			AuditLog auditLog = new AuditLog(userId, userName, null, "Shipment", "Delete", null, deviceData.toJson(), "User "+userName +" has disassociate < "+deviceId +" ,"+packageId +" >", orgId);
			auditLogDao.addAuditLog(auditLog);
			
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device "+deviceId +" is successfully disassociated from the package "+packageId);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();

		} catch (Exception e) {
			logger.error("Exception in disassociate "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: disassociate.  Request :: "+request +" Response :: "+res );
		}
	}


	
	/*@POST
	@Path("/addThreshHold")
	@Consumes("application/json")
	@Produces("application/json")
	public Response saveThreshHold(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			String flag=ThresholdDAO.getInstance().saveThreshold(request.getListThreshHold());
			if(flag.equalsIgnoreCase(SensorConstant.RESP_STAT_SUCCESS))
			{
				res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
				RuleCache.getInstance().populate();
			}
			else
			{
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Error occured while saving threshold rule");
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in addThreshHold "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: addThreshHold.  Request :: "+request +" Response :: "+res );
		}
	}*/

	/**
	 * <p>
	 * Get status of package, it will return device Id if device is associated with the given package.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/getPackageStatus'
	 * </p>
	 * @param request It contains packageId
	 * @return Response deviceId
	 */
	/*@POST
	@Path("/getPackageStatus")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getPackageStatus(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Boolean authenticate = getAuthenticate();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			
			Integer orgId = request.getOrgId();
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			DeviceData deviceData = request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			res = SendumDAO.getInstance().getPackageStatus(request);
			if (res.getDeviceId()==null) {
				res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("PackageId is not associated with any device");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}if (res.getResponseStatus()!= null && res.getResponseStatus().equalsIgnoreCase("0")) {
				res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("PackageId is disassociated from the device");
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();

		}catch (SQLException e) {
			logger.error("Exception in getPackageStatus "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getPackageStatus "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getPackageStatus.  Request :: "+request +" Response :: "+res );
		}
	}*/
	
	// NOTE : API is updated for multitenancy
	/**
	 * <p>
	 * List of Active Shipments.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/getShipmentList'
	 * </p>
	 * @param request
	 * @return Response latest MapData and shipment list(deviceId-packageId)
	 */
	@POST
	@Path("/getShipmentList")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getShipmentList(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			Long groupId = request.getGroupId();
			if(groupId!=0){
				resp = UserService.validateGroup(groupId);
				if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
				}
			}
			
			// EMS demo -  searchType = 1 - deviceList/currentShipment
			String searchType = request.getSearchType();	// if search type is not specified , get current shipments
			//check rtsDataFetch flag for EMS demo			
			Boolean rtsDataFetch = false;
			if(request.getRtsDataFetch() != null){
				rtsDataFetch = request.getRtsDataFetch();
			}
			
			if(!rtsDataFetch){
				if(searchType == null || searchType.trim().length()==0 ){
					request.setSearchType(SensorConstant.SEARCH_TYPE_CURRENT_SHIPMENTS);
					searchType = SensorConstant.SEARCH_TYPE_CURRENT_SHIPMENTS;
				}
				if(! searchType.equalsIgnoreCase(SensorConstant.SEARCH_TYPE_COMPLETED_SHIPMENTS)
						&& !searchType.equalsIgnoreCase(SensorConstant.SEARCH_TYPE_CURRENT_SHIPMENTS)
						&& !searchType.equalsIgnoreCase(SensorConstant.SEARCH_TYPE_PENDING_SHIPMENTS)){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Please provide proper Search Type")).build();
				}
				String user = TokenManager.getInstance().getUserForToken(httpRequest);
				request.setUser(user);
				String searchString = request.getSearchString();
				
				//check the Elastic search fetch is true or false
				String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
				Boolean fetchESData = false;
				if(fetchESDataString != null){
					fetchESData = Boolean.valueOf(fetchESDataString);
				}
				
				
				String responseMsg = "";
				if(fetchESData && (searchString != null && searchString.trim().length() != 0)){
					res = SearchDAO.getInstance().getShipmentList(request);
					if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
						responseMsg = res.getResponseMessage();
						res.setResponseMessage(responseMsg);
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
						return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
					}
				}else{
					res = sendumDAO.getShipmentList(request);
				}
			}else{
				// EMS demo - to show eventList, itemList
				if(searchType == null || searchType.trim().length()==0 ){
					searchType = "4"; //event list
				}
				//4-eventList, 5-itemList
				if(searchType.equalsIgnoreCase("4")){
					res = EventController.getInstance().getEventList(orgId);
				}else if(searchType.equalsIgnoreCase("5")){
					res = ItemController.getInstance().getItemList(orgId);
				}else if(searchType.equalsIgnoreCase("6")){
					res = ItemController.getInstance().getMergeAlerts(request);
				}
			}
						
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getShipmentList "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getShipmentList "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getShipmentList.  Request :: "+request +" Response :: "+res );
		}

	}

	/**
	 * <p>
	 * Gets details of an active shipment.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/getShipmentDetails'
	 * </p>
	 * @param request
	 * @return Response - Active shipment details of a given deviceId-packageId combination
	 */
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/getShipmentDetails")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getShipmentDetails(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getDeviceData()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("DeviceData is null or empty")).build();
			}	
			if (request.getDeviceData().getPackageId()==null /*|| request.getDeviceData().getDeviceId()==nul*/) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("PackageId is null")).build();
			}	

			if(orgId!= null && orgId!=0){
				com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
				if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
				}
			}

			Long groupId = request.getGroupId();
			com.rfxcel.sensor.vo.Response resp = UserService.validateGroup(groupId);
			if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			res = sendumDAO.getShipmentDetails(request);
			if (res.getSensor()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("PackageId not found")).build();
			}
			if (request.getDeviceData().getRtsItemId() != null && request.getDeviceData().getRtsSerialNumber() != null) {
				//retrieve the traceId from rTS
				res.setTraceId(ItemController.getInstance().getItemTraceId(orgId, request.getDeviceData().getRtsItemId(), request.getDeviceData().getRtsSerialNumber()));
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getShipmentDetails "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getShipmentDetails "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getShipmentDetails.  Request :: "+request +" Response :: "+res );
		}
	}
	
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/addShipment")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addShipment(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse addProductResp = new SensorResponse(simpleDateFormat.format(new Date()));
		Response response;
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			// GRoupId is fetched from cache so not validating it
			Long groupId = TokenManager.getInstance().getGroupId(httpRequest);
			/*resp = UserService.validateGroup(groupId);
			if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}*/
			Long userId = TokenManager.getInstance().getUserId(httpRequest);
			ProductDetail productDetail = request.getProductDetail();
			if (productDetail == null || productDetail.getPackageId()== null || productDetail.getDeviceId() == null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Product Detail must contain deviceId and packageId")).build();
			}
			
			if (productDetail.getRtsItemId() != null && productDetail.getRtsSerialNumber() != null) {
				
				String traceId = ItemController.getInstance().getItemTraceId(orgId, productDetail.getRtsItemId(), productDetail.getRtsSerialNumber());
				if (traceId == null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("rTS Item Id & Serial Number are invalid")).build();
				}
			
			}

			String deviceId = productDetail.getDeviceId();
			String packageId = productDetail.getPackageId();
			String productId = productDetail.getProductId();
			//check if package is present
			Boolean packageIsPresent = sendumDAO.checkIfPackageIsPresent(packageId,productId, orgId);
			//If package is present check if it is in deCommissioned state,  if so throw exception
			if(packageIsPresent){
				Boolean flag = sendumDAO.checkIfPackageIsDeCommissioned(packageId, productId, orgId);
				if(flag){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package Id " +packageId +" for product "+ productId +" is in Decommissioned state")).build();
				}
			}
			//check if device Id is present
			Boolean devicePresent = sendumDAO.checkIfDeviceIsPresent(deviceId, orgId);
			//If device is present check if it is associated or deCommissioned state, if so throw exception
			if(devicePresent){
				Boolean flag = sendumDAO.checkIfDeviceIsAssociated(deviceId, orgId);
				if(flag){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id " +deviceId +" is already in associated state")).build();
				}
				flag = sendumDAO.checkIfDeviceIsDeCommissioned(deviceId, orgId);
				if(flag){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id " +deviceId +" is in Decommissioned state")).build();
				}
			}
			if(!packageIsPresent){
				ArrayList<Product> productList = new ArrayList<Product>();
				Product product = new Product();
				product.setPackageId(packageId);
				product.setProductId(productDetail.getProductId());
				productList.add(product);
				request.getProductDetail().setProductList(productList);
				// call commissionProduct service
				response = commissionPackage(httpRequest, request);			
				com.rfxcel.sensor.vo.Response commResp = (com.rfxcel.sensor.vo.Response)response.getEntity();
				if(SensorConstant.RESP_STAT_ERROR.equals(commResp.getResponseStatus())){	// if package is not commissioned successfully
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(commResp.getResponseMessage())).build();
				}
			}

			if(!devicePresent){
				//if not present, commission it
				String deviceType =  productDetail.getDeviceType();
				Boolean deviceIsActive = productDetail.getDeviceIsActive();
				DeviceDetail deviceDetail = new DeviceDetail();
				deviceDetail.setDeviceId(deviceId);
				deviceDetail.setDeviceType(deviceType);
				deviceDetail.setActive(deviceIsActive);
				ArrayList<DeviceDetail> itemListDetails = new ArrayList<DeviceDetail>();
				itemListDetails.add(deviceDetail);
				request.setItemListDetails(itemListDetails);
				response = commissionDevices(httpRequest,request);
				SensorResponse commResp = (SensorResponse)response.getEntity();
				if(SensorConstant.RESP_STAT_ERROR.equals(commResp.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(commResp).build();
				}
			}
			//associate it with product
			DeviceData deviceData = new DeviceData(deviceId,packageId);
			deviceData.setProfileId(productDetail.getProfileId());
			deviceData.setProductId(productId);
			deviceData.setRtsItemId(productDetail.getRtsItemId());
			deviceData.setRtsSerialNumber(productDetail.getRtsSerialNumber());
			request.setDeviceData(deviceData);
			response = associateItems(httpRequest, request);
			SensorResponse assocResp = (SensorResponse)response.getEntity();
			if(SensorConstant.RESP_STAT_ERROR.equals(assocResp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage(assocResp.getResponseMessage())).build();
			}
		
			//add entry to trans_history table;
			sendumDAO.addShipmentDetails(productDetail, orgId, userId, groupId );
			
			if (productDetail.getRtsItemId() != null && productDetail.getRtsSerialNumber() != null) {
				//	associate device with product in rTS
				EventController.getInstance().createDeviceAssociation(orgId, productDetail.getRtsItemId(), productDetail.getRtsSerialNumber(), deviceId);
			}
			
			//create shipment in rTS
			//EventController.getInstance().createShipEvent(orgId, productId, packageId, deviceId);
			
			Profile profileInfo = applyDeviceProfile(deviceId, packageId, productDetail.getProfileId(), userId, orgId, groupId);
			//invoke addDeviceLog api			
			String logMsg = "This shipment <"+deviceId+", "+productDetail.getProductId()+"> has been associated with profile <"+productDetail.getProfileId()+" - "+profileInfo.getName()+">.";
			DeviceLog deviceLog = new DeviceLog();
			deviceLog.setDeviceId(deviceId);
			deviceLog.setUserId(userId);
			deviceLog.setLogMsg(logMsg);
			deviceLog.setOrgId(orgId);
			DeviceLogDAO.getInstance().addDeviceLog(deviceLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(addProductResp.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).
					setResponseMessage("Shipment has been successfully added.")).build();
		}catch (SQLException e) {
			logger.error("Exception in addShipment "+ e.getMessage());
			e.printStackTrace();
			addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(addProductResp).build();
		}catch (Exception e) {
			logger.error("Exception in addShipment "+ e.getMessage());
			e.printStackTrace();
			addProductResp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(addProductResp).build();
		}finally{
			diagnosticService.write("API :: addShipment.  Request :: "+request +" Response :: "+addProductResp );
		}
	}
	
	@POST
	@Path("/updateShipment")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateShipment(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			// GroupId is fetched from cache so not validating it 
			Long groupId = TokenManager.getInstance().getGroupId(httpRequest);
			/*resp = UserService.validateGroup(groupId);
			if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}*/
			ProductDetail productDetail = request.getProductDetail();
			if (productDetail == null || productDetail.getPackageId()== null || productDetail.getDeviceId() == null || productDetail.getProductId() == null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Product Detail must contain deviceId, packageId and ProductId")).build();
			}
			if (productDetail.getRtsItemId() != null && productDetail.getRtsSerialNumber() != null) {
				
				String traceId = ItemController.getInstance().getItemTraceId(orgId, productDetail.getRtsItemId(), productDetail.getRtsSerialNumber());
				if (traceId == null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("rTS Item Id & Serial Number are invalid")).build();
				}
			
			}
			String deviceId = productDetail.getDeviceId();
			String packageId = productDetail.getPackageId();
			String productId = productDetail.getProductId();
			// check if it is an active association
			Boolean active = associationDAO.checkIfAssociationIsActive(deviceId, packageId, productId);
			if(!active){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Device Id "+deviceId +" packageId "+ packageId+" are not associated")).build();
			}
			Integer profileId = productDetail.getProfileId();
			active = profileDAO.checkIfActiveProfileIsPresent(profileId);
			if(!active){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Profile is not active. Please check configurations")).build();
			}
			Integer oldProfileId = assoCache.getActiveShipmentProfileId(deviceId, packageId);
			String oldProfileName = profileDAO.getProfileNameById(oldProfileId);
			String newProfileName = profileDAO.getProfileNameById(profileId);
			//log message to deviceLog table mentioning old profile mapping and new profile mapping
			Long userId = TokenManager.getInstance().getUserId(httpRequest);
			// update shipment profile 
			associationDAO.updateShipmentProfile(deviceId, packageId, profileId);
			
			//update entry to trans_history table;
			sendumDAO.updateShipmentDetails(productDetail, orgId, userId, groupId );
			
			if (productDetail.getRtsItemId() != null && productDetail.getRtsSerialNumber() != null) {
				AssociationDAO.getInstance().associateItems(deviceId,packageId,productId, orgId, groupId, profileId, productDetail.getRtsItemId(), productDetail.getRtsSerialNumber());
				//	associate device with product in rTS
				EventController.getInstance().createDeviceAssociation(orgId, productDetail.getRtsItemId(), productDetail.getRtsSerialNumber(), deviceId);
			}
			
			applyDeviceProfile(deviceId, packageId, profileId, userId, orgId, groupId);
			// make an entry in device Log table
			String logMessage = "Shipment <"+deviceId +" , " +packageId +"> Profile Updated. Old Profile: "+ oldProfileName +". New Profile: "+newProfileName;
			DeviceLogDAO.doAddDeviceLog(deviceId, userId, logMessage, orgId);
			// update cache
			assoCache.addActiveShipmentProfileId(deviceId, packageId, profileId);
			
			String userName = tokenManager.getUserForToken(httpRequest);
			AuditLog auditLog = new AuditLog(userId, userName, null, "Shipment", "Update", null, productDetail.toJson(),logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).
					setResponseMessage("Shipment updated successfully.")).build();
		}catch (SQLException e) {
			logger.error("Exception in updateShipment "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in updateShipment "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: updateShipment.  Request :: "+request +" Response :: "+res );
		}
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param profileId
	 * @param userId
	 * @param orgId
	 * @param groupId
	 * @return 
	 * @throws SQLException 
	 */
	public static Profile applyDeviceProfile(String deviceId, String packageId, Integer profileId, Long userId, Integer orgId, Long groupId) throws SQLException{
		ProfileRequest profileRequest = new ProfileRequest();			
		Profile profile = new Profile();
		profile.setProfileId(profileId);
		profile.setOrgId(orgId);
		profile.setGroupId(groupId);
		profileRequest.setProfile(profile);
		Profile profileInfo = ProfileDAO.getInstance().getProfile(profileRequest);
		if(profileInfo != null && deviceId != null){
			Device device = sendumDAO.getDeviceById(deviceId, orgId);
			if(device!= null && device.getDeviceKey()!= null && !device.getDeviceKey().isEmpty() && !device.getDeviceKey().trim().isEmpty()){
				DeviceController.getInstance().applyDeviceProfile(orgId,userId, device, profileInfo);
			}
			else{
				logger.error("Device Key should not be empty");
			}
		}
		return profileInfo;
	}
	
	
	/**
	 * <p>
	 * Gets the list of locations/facilities from database.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/findLoctionName'
	 * </p>
	 * @param request
	 * @return Response - List of locations/facilities matched to the given criteria
	 */
/*	@POST
	@Path("/findLoctionName")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getLocationByName(@Context HttpServletRequest httpRequest, SensorRequest request)  {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getLocationRequest()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("locationData is null or empty")).build();
			}	
			if (request.getLocationRequest().getName()==null ) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("name is null")).build();
			}	
			res = SendumDAO.getInstance().getLocationByName(request);
			if (res.getLocationData()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("packageId not found")).build();
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in addProduct "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in addProduct "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: addProduct.  Request :: "+request +" Response :: "+res );
		}
	}*/
	
	
	/**
	 * <p>
	 * Gets the address details of a location/facility.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/findAddress'
	 * </p>
	 * @param request
	 * @return Response - Address information of location/facility matched to the given location ID
	 */
	/*@POST
	@Path("/findAddress")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAddressById(@Context HttpServletRequest httpRequest, SensorRequest request)  {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getLocationRequest()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("locationData is null or empty")).build();
			}	
			if (request.getLocationRequest().getId()==null ) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("id is null")).build();
			}	
			res = SendumDAO.getInstance().getAddressById(request);
			if (res.getLocationData()==null || res.getLocationData().getLocations().size() == 0) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Address detials not found for the given Id.")).build();
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in findAddress "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in findAddress "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: findAddress.  Request :: "+request +" Response :: "+res );
		}
	}*/
	

	/**
	 * <p>
	 * Gets the address details of a location/facility.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/findAddress'
	 * </p>
	 * @param request
	 * @return Response - Address information of location/facility matched to the given location ID
	 */
	/*@POST
	@Path("/listConfig")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getSensorConfig(@Context HttpServletRequest httpRequest, SensorRequest request)  {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getSearchType().equalsIgnoreCase(NotificationConstant.NOTIFICATION_SEARCHTYPE_PARAMS)) {
				if (request.getConfigData()==null) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("configData is null")).build();
				}
				if (request.getConfigData()!= null && request.getConfigData().getParams()==null ) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
							.setResponseMessage("params are missing in the request")).build();
				}	
			}
			res = SendumDAO.getInstance().getSensorConfig(request);
			if (res.getSensor()==null || res.getSensor().getConfigMap().size() == 0) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("No config parameters found in the system.")).build();
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in listConfig "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in listConfig "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: listConfig.  Request :: "+request +" Response :: "+res );
		}
	}*/

	
	
	@POST
	@Path("/decommissionDevice")
	@Consumes("application/json")
	@Produces("application/json")
	public Response deCommissionDevice(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Boolean authenticate = getAuthenticate();
			Integer orgId = request.getOrgId();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			DeviceData deviceData = request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			String deviceId = deviceData.getDeviceId();
			//check if device is not associated
			Boolean associated = sendumDAO.checkIfDeviceIsAssociated(deviceId, orgId);
			if(associated){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Device " +deviceId +" is in associated state, Please disassociate it before decommissioning.")).build();
			}
			//Decommission a device
			sendumDAO.decommissionDevice(deviceId);				
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)
					.setResponseMessage("Device Id " +deviceId +" is decommissioned successfully.")).build();
		}catch (SQLException e) {
			logger.error("Exception in decommissionDevice "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in decommissionDevice "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: decommissionDevice.  Request :: "+request +" Response :: "+res );
		}

	}

	@POST
	@Path("/deCommissionPackage")
	@Consumes("application/json")
	@Produces("application/json")
	public Response deCommissionPackage(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Boolean authenticate = getAuthenticate();
			Integer orgId = request.getOrgId();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			DeviceData deviceData = request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			String packageId = deviceData.getPackageId();
			String productId = deviceData.getProductId();
			//check if package is associated
			Boolean associated = sendumDAO.checkIfPackageIsAssociated(packageId,productId, orgId);
			if(associated){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("PackageId " +packageId +" for product "+productId+" is in associated state, Please disassociate it before decommissioning.")).build();
			}
			//Decommission package
			sendumDAO.decommissionPackage(packageId, orgId);
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("PackageId " +packageId +" is decommissioned successfully.");
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		}catch (SQLException e) {
			logger.error("Exception in deCommissionPackage "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in deCommissionPackage "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: deCommissionPackage.  Request :: "+request +" Response :: "+res );
		}

	}

	
	/*@POST
	@Path("/getProducts")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getProducts(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			//Currently request only sends SearchType 
			if(request.getSearchType() == null || !request.getSearchType().equalsIgnoreCase("All")){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Search Type is null or incorrect value is provided")).build();
			}
			ArrayList<Product> products = SendumDAO.getInstance().getProductsInfo();
			ProductDetail productDetail = new ProductDetail();
			productDetail.setProductList(products);
			res.setProductDetail(productDetail);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getProducts "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getProducts "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getProducts.  Request :: "+request +" Response :: "+res );
		}
	}*/

	
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/commissionPackage")
	@Consumes("application/json")
	@Produces ("application/json")
	public Response commissionPackage(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Boolean authenticate = getAuthenticate();
			Integer orgId = request.getOrgId();
			if(authenticate){
				boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
				if(!validToken){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
				}
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			Long groupId = TokenManager.getInstance().getGroupId(httpRequest);
			ProductDetail productDetail = request.getProductDetail();
			if(productDetail == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("ProductDetail is null or empty")).build();
			}
			ArrayList<Product> productList = productDetail.getProductList();
			if(productList == null ||productList.size() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Product List is null or empty")).build();
			}
			
			String productId ;
			String packageId;
			Boolean packageIsPresent;
			Boolean packageIsDecommissioned;
			ArrayList<Product> decommissionedPackages = new ArrayList<Product>();
			for(Product product : productList){
				productId = product.getProductId();
				if(productId == null || productId.trim().length()==0){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("ProductId cannot be null or empty ")).build();
				}
				packageId = product.getPackageId();
				if(packageId == null || packageId.trim().length()==0){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("PackageId cannot be null or empty ")).build();
				}
				packageId = packageId.trim();
				packageIsPresent = sendumDAO.checkIfPackageIsPresent(packageId, productId, orgId);
				if(packageIsPresent){
					packageIsDecommissioned = sendumDAO.checkIfPackageIsDeCommissioned(packageId, productId,  orgId);
					if(!packageIsDecommissioned){
						return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package ID "+packageId +" is already commissioned " )).build();
					}else{
						decommissionedPackages.add(product);
					}
				}
			}
			productList.removeAll(decommissionedPackages);// remove packages with state deCommission
			res = CommissionDAO.getInstance().commissionProducts(productList, decommissionedPackages, orgId, groupId);
			
			String userName = tokenManager.getUserForToken(httpRequest);
			Long userId = tokenManager.getUserId(httpRequest);
			AuditLog auditLog = new AuditLog(userId, userName, null, "Package", "Create", null, request.getProductDetail().toJson(), "User "+userName +" has commissioned products", orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		}catch (SQLException e) {
			logger.error("Exception in commissionPackage "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in commissionPackage "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: commissionPackage.  Request :: "+request +" Response :: "+res );
		}
	}
	
	
	@POST
	@Path("/favourite")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addFavourite(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			DeviceData deviceData = request.getDeviceData();
			res = validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			sendumDAO.addToFavourite(request);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Add to Favourite successful")).build();
		}catch (SQLException e) {
			logger.error("Exception in addFavourite "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in addFavourite "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: addFavourite.  Request :: "+request +" Response :: "+res );
		}
	}


	/**
	 * <p> method to validate deviceData, deviceId and or packageId
	 * @param deviceData
	 * @param type
	 * @return
	 * @throws SQLException
	 */
	public static SensorResponse validateDeviceData(DeviceData deviceData, String type, int orgId)throws SQLException{
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		if (deviceData == null) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Data is missing");
			return res;
		}
		
		if(VALIDATION_TYPE_DEVICE.equals(type) || VALIDATION_TYPE_DEVICE_PACKAGE.equals(type)){
			String deviceId = deviceData.getDeviceId();
			if (deviceId == null || deviceId.trim().length()==0) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id is null or empty");
				return res;
			}
			//Check if device Id is present in system
			Boolean deviceIsPresent = sendumDAO.checkIfDeviceIsPresent(deviceId, orgId);
			if(!deviceIsPresent){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id "+ deviceId+" is not configured in system");
				return res;
			}
		}
	
		if(VALIDATION_TYPE_PACKAGE.equals(type) || VALIDATION_TYPE_DEVICE_PACKAGE.equals(type)){
			String packageId = deviceData.getPackageId();
			if ( packageId == null || packageId.trim().length()==0) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package Id is null or empty");
				return res;
			}
			String productId = deviceData.getProductId();
			if ( productId == null || productId.trim().length()==0) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("product Id is null or empty");
				return res;
			}
			//Check if the packageId is present in system
			Boolean packageIsPresent = sendumDAO.checkIfPackageIsPresent(packageId,productId, orgId);
			if(!packageIsPresent){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package Id "+ packageId+" for product "+productId +" is not configured in system");
				return res;
			}
		}
		return res;
	}
	
	/**
	 * 
	 * @return
	 */
	private static Boolean getAuthenticate(){
		Boolean authenticate = false;
		String authenticateProp;
		try {
			authenticateProp =ConfigCache.getInstance().getSystemConfig(SensorConstant.AUTHENTICATE_REQUEST);
			if(authenticateProp == null){		// if property is not configured, setting authenticate to false
				authenticate = false;
			}else{
				authenticate = Boolean.valueOf(authenticateProp);
			}
		} catch (Exception e) {
			logger.error("Error while getting autheticate property");
		}
		return authenticate;
	}

	
	/**
	 * <p> Deactivate devices for an org
	 * @param orgId
	 *//*
	public static void deactivateDevicesForOrg(Integer orgId) {
		try{
			sendumDAO.deactivateDevicesForOrg(orgId);
			ArrayList<String> deviceIds = sendumDAO.getDevicesForOrg(orgId);
			deviceCache.removeDeviceLastLocation(deviceIds);
			deviceCache.removeDeviceGeopointState(deviceIds);
			deviceCache.removeDeviceIdTypeMapping(deviceIds);
			deviceCache.removeDeviceIdBatteryValueMapping(deviceIds);
			deviceCache.removeDeviceOrgMapping(deviceIds);
			attrLogcache.removeNotificationLimitLog(deviceIds);
			attrLogcache.removeExcursionTimeLog(deviceIds);
			attrLogcache.removeSameLocationNotificationLimitLog(deviceIds);
			attrLogcache.removeDeviceBacterialGrowthLog(deviceIds);
			String packageId;
			for(String deviceId : deviceIds){
				packageId = assoCache.getContainerId(deviceId);
				assoCache.removeAssociationStatus(deviceId, packageId);
				assoCache.removeShipmentProfileMapping(deviceId, packageId);
				assoCache.removeShipmentHomeGeopoint(deviceId, packageId);
				assoCache.removeAssociation(deviceId);
				sendumDAO.disAssociatePackage(packageId, orgId);
				sendumDAO.disAssociateDevice(deviceId);
			}
			// update associate table
			AssociationDAO.getInstance().DisassociateShipmentsForOrg(orgId);
			//change status of associated packages and devices to disassociate  
			
		}catch(Exception e){
			logger.error("Exception in SensorService.deactivateDevicesForOrg "+ e.getMessage());
			e.printStackTrace();
		}finally{
			diagnosticService.write("API :: SensorService.deactivateDevicesForOrg " );
		}
	}*/
	
	@POST
	@Path("/getDeviceList")
	@Produces("application/json")
	@Consumes("application/json")
	public Response getDeviceList(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			if(request== null || request.getDevice()== null || request.getDevice().getOrgId() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("device or OrgId cannot be null.")).build();
			}
			Integer orgId = request.getDevice().getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			List<Device> devices = null;
			devices =  sendumDAO.getDevices(request);
			Sensor sensor = new Sensor();
			sensor.setDevices(devices);
			res.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getDeviceList "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getDeviceList "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getDeviceList.  Request :: "+request +" Response :: "+res );
		}
	}
	
	@POST
	@Path("/getDevices")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getDevices(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			Long groupId = request.getGroupId();
			resp = UserService.validateGroup(groupId);
			if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			String searchType = request.getSearchType();
			String searchString  = request.getSearchString();
			ArrayList<DeviceDetail> deviceList = null;
			if(searchType == null || searchType.trim().length() ==0 ){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Please provide proper searchType for selecting Devices")).build();
			}
			if(searchString == null || searchString.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Please provide proper searchString")).build();
			}
			
			deviceList = sendumDAO.searchDevices(searchType, searchString, orgId);
			
			Sensor sensor = new Sensor();
			sensor.setDeviceList(deviceList);
			res.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getDevices "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: SensorService.getDevices.  Request :: "+request +" Response :: "+res );
		}
	}
	
	
	/**
	 * <p>
	 * Gets details of an active shipment.
	 * URI to call this service
	 * EndPoint : '/rest/sensor/getShipmentDetails'
	 * </p>
	 * @param request
	 * @return Response - Active shipment details of a given deviceId-packageId combination
	 */
	// NOTE : API is updated for multitenancy
	@POST
	@Path("/getPackageId")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getPackageId(@Context HttpServletRequest httpRequest, SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getDeviceData()==null || request.getDeviceData().getDeviceId()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("DeviceData is null or empty")).build();
			}	
			
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}

			res.setPackageId(SensorDAO.getInstance().getActiveAssociatedPackage(request.getDeviceData().getDeviceId()));
			res.setProductId(SensorDAO.getInstance().getActiveAssociatedProduct(request.getDeviceData().getDeviceId()));
			if (res.getPackageId()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("PackageId not found")).build();
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (SQLException e) {
			logger.error("Exception in getPackageId "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}catch (Exception e) {
			logger.error("Exception in getPackageId "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getPackageId.  Request :: "+request +" Response :: "+res );
		}
	}
	
	
	@POST
	@Path("updateDevice")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateDevice(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			//Check if request is not empty
			if(request == null || request.getDevice() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device data cannot be null.")).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			Long groupId = request.getGroupId();
			resp = UserService.validateGroup(groupId);
			if (SensorConstant.RESP_STAT_ERROR.equals(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			Device device = request.getDevice();
			sendumDAO.updateDevice(device, orgId);
			String deviceManufacturer = DeviceTypeDAO.getInstance().getDeviceTypeManufacturer(orgId, device.getDeviceType());		
			if(deviceManufacturer.equalsIgnoreCase(SensorConstant.SENSOR_KIRSEN) && (device.getDeviceKey()== null || device.getDeviceKey().trim().length()==0)){
				SensorResponse updateResponse = updateKirsenDeviceKey(orgId,device.getDeviceId());
				if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(updateResponse.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(updateResponse).build();
				}
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device updated successfully")).build();
		}catch(Exception e){
			logger.error("Exception in SensorService.updateDevice "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SensorService.updateDevice Request :: "+request +" Response :: "+response );
		}
	}
	
	@POST
	@Path("deleteDevice")
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteDevice(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			//Check if request is not empty
			if(request == null || request.getDeviceData() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device data cannot be null.")).build();
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			DeviceData deviceData = request.getDeviceData();
			response = validateDeviceData(deviceData,VALIDATION_TYPE_DEVICE, orgId);
			if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response).build();
			}
			String deviceId = deviceData.getDeviceId();
			Boolean associated = sendumDAO.checkIfDeviceIsAssociated(deviceId, orgId);
			if(associated){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Device " +deviceId +" is in associated state, Please disassociate it before deleting.")).build();
			}
			
			//check if device is of manufacturer Verizon
			String deviceType = sendumDAO.getDeviceTypeById(deviceId);
			String deviceManufacturer = DeviceTypeDAO.getInstance().getDeviceTypeManufacturer(orgId, deviceType);	
			//For verizon devices checking if activation status is pending, if it is pending restricting delete operation
			if(deviceManufacturer.equalsIgnoreCase(SensorConstant.SENSOR_VERIZON)){
				Integer activeStatus = sendumDAO.getActiveStatus(deviceId);
				if(activeStatus!= null && activeStatus!=0){
					Integer activationStatus = sendumDAO.getActivationStatus(deviceId);
					if(activationStatus!= null && activationStatus==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Device " +deviceId +" is pending activation, please wait for a while before deleting.")).build();
					}
				}
			}
			if(deviceManufacturer.equalsIgnoreCase(SensorConstant.SENSOR_VERIZON)){
				deleteVerizonDevice(orgId, deviceId);				
			}
			sendumDAO.deleteDevice(deviceId, orgId);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device deleted successfully")).build();
		}catch(Exception e){
			logger.error("Exception in SensorService.deleteDevice "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SensorService.deleteDevice Request :: "+request +" Response :: "+response );
		}
	}
	
	@POST
	@Path("locateDevice")
	@Consumes("application/json")
	@Produces("application/json")
	public Response locateDevice(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if(request == null || request.getDeviceDetail() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device detail cannot be null.")).build();
			}
			
			DeviceDetail deviceDetail =  request.getDeviceDetail();
			String deviceId = deviceDetail.getDeviceId();
			if(deviceId == null || deviceId.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Id cannot be null or empty.")).build();
			}

			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			long userId = request.getUserId();
			Device device = sendumDAO.getDeviceById(deviceId, orgId);
			resp = DeviceController.getInstance().locateDevice(orgId, userId, device);

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
		}catch(Exception e){
			logger.error("Exception in SensorService.locateDevice "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SensorService.locateDevice Request :: "+request +" Response :: "+response );
		}
	}
	
	/**
	 * 
	 * @param orgId
	 * @param jsonMsg
	 * @return
	 */
	private void addVerizonDevice(Integer orgId, DeviceDetail deviceDetail){
		try {
			deviceDetail.setOrgId(orgId);
			VerizonDeviceActivator.getInstance().activateDevice(deviceDetail);
		} catch (Exception e) {
			logger.error("Error in addVerizonDevice "+ e.getMessage());
			e.printStackTrace();
		}
	}
	
	/*private String registerVerizonDevice(Integer orgId, DeviceDetail deviceDetail){
		String authToken = ConfigCache.getInstance().getOrgConfig("thingspace.auth.token", orgId);
		if (authToken == null) {
			logger.error("Failed to get authToken for deviceID " + deviceDetail.getDeviceId());
			return(null);
		}
		String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
		String deviceType = ConfigCache.getInstance().getSystemConfig("thingspace.device.type");
		String tsProviderID = ConfigCache.getInstance().getSystemConfig("thingspace.device.provider");
		RESTClient httpClient = new RESTClient(null, null);
		JSONObject obj = new JSONObject();
		BigInteger imei = new BigInteger(deviceDetail.getDeviceId());
	    obj.put("kind", deviceType);
	    obj.put("version", "1.0");
	    obj.put("name", "ITT Device");
	    obj.put("imei", imei);				   
	    obj.put("qrcode", deviceDetail.getDeviceId());				   
	    obj.put("providerid", tsProviderID);				   
	    String jsonMsg = obj.toString();
		String response = httpClient.doPost(url, "Authorization", authToken, jsonMsg);
		if (response == null) {
			logger.error("Failed to get deviceKey from ThingSpace for url " + url + " token " + authToken + " request " + jsonMsg);
			return(null);
		}
		Map<String, String> map = Utility.convertJsonToMap(response);
		return(map.get("id"));
	}	
	
	*//**
	 * 
	 * @param orgId
	 * @param deviceId
	 * @return ThingSpace key
	 *//*

	private String findVerizonDevice(Integer orgId, String deviceID){
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("thingspace.auth.token", orgId);
			if (authToken == null) {
				logger.error("Failed to get authToken for deviceID " + deviceID);
				return(null);
			}
			String url = ConfigCache.getInstance().getSystemConfig("thingspace.integration.url");
			RESTClient httpClient = new RESTClient(null, null);
			String response = httpClient.doGet(url, "Authorization", authToken);
			if (response == null) {
				logger.error("Failed to get response from ThingSpace for url " + url + " token " + authToken);
				return(null);
			}
			JSONArray array = new JSONArray(response);
			for (int i = 0; i < array.length(); i++) {
				String text = array.get(i).toString();
				JSONObject o = new JSONObject(text);
				String id = o.getString("id");
				String imei = o.optString("imei");
				if ((imei != null) && (imei.equalsIgnoreCase(deviceID))) {
					return(id);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return(null);
	}	*/
	
	/**
	 * 
	 * @param orgId
	 * @param deviceId
	 * @return
	 */
	private String deleteVerizonDevice(Integer orgId, String deviceID){
		Device device;
		try {
			device = sendumDAO.getDeviceById(deviceID, orgId);
			String key = device.getDeviceKey();
			if ((key == null) || (key.equalsIgnoreCase(SensorConstant.DEVICE_KEY_DEMO))) return(null);
			VerizonDeviceDeactivator.getInstance().deActivateDevice(device);
			return(key);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(null);
	}
	
	
	@POST
	@Path("updateKirsenDeviceKeys")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateKirsenDeviceKeys(@Context HttpServletRequest httpRequest, SensorRequest request){
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			response = updateKirsenDeviceKeys(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(response.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device keys updated successfully for Kirsen devices")).build();
		}catch(Exception e){
			logger.error("Exception in SensorService.updateKirsenDeviceKeys "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SensorService.updateKirsenDeviceKeys Request :: "+request +" Response :: "+response );
		}
	}

	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	private SensorResponse updateKirsenDeviceKeys(Integer orgId) throws SQLException {
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			KirsenController controller = KirsenController.getInstance();
			SensorResponse resp = controller.getDeviceKeys();
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus()))
			{
				return resp;
			}else{
				HashMap<Object, Object> deviceKeyMap = resp.getDeviceKeyMap();
				Object deviceKey;
				ArrayList<String> deviceIds = sendumDAO.getKirsenDeviceForOrg(orgId, "Kirsen");
				for(String deviceId: deviceIds){
					deviceKey = deviceKeyMap.get(deviceId);
					//Update device if deviceKey is retrieved from kirsen 
					if(deviceKey != null){
					sendumDAO.updateDeviceKey(deviceId, deviceKey.toString());
					}
				}
				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device keys updated successfully for Kirsen devices");
			}
		}finally{
		}
		return response;
	}
	
	/**
	 * @param orgId
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	private SensorResponse updateKirsenDeviceKeys(Integer orgId, ArrayList<DeviceDetail> deviceDetails) throws SQLException {
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			KirsenController controller = KirsenController.getInstance();
			SensorResponse resp = controller.getDeviceKeys();
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return resp;
			}else{
				HashMap<Object, Object> deviceKeyMap = resp.getDeviceKeyMap();
				if(deviceKeyMap != null){
					String deviceId;
					for(DeviceDetail deviceDetail: deviceDetails){
						deviceId = deviceDetail.getDeviceId();
						Object deviceKey = deviceKeyMap.get(deviceId);
						//Update device if deviceKey is retrieved from kirsen 
						if(deviceKey != null){
							sendumDAO.updateDeviceKey(deviceId, deviceKey.toString());
						} else {
							sendumDAO.deactivateDevice(deviceId);
						}
					}
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device key updated successfully for commissioned Devices ");
				}
			}
		}finally{
		}
		return response;
	}
	
	
	/**
	 * @param orgId
	 * @param deviceId
	 * @return 
	 * @throws SQLException
	 */
	private SensorResponse updateKirsenDeviceKey(Integer orgId, String deviceId) throws SQLException {
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try{
			KirsenController controller = KirsenController.getInstance();
			SensorResponse resp = controller.getDeviceKeys();
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus()))
			{
				return resp;
			}else{
				HashMap<Object, Object> deviceKeyMap = resp.getDeviceKeyMap();
				Object deviceKey = deviceKeyMap.get(deviceId);
					//Update device if deviceKey is retrieved from kirsen 
					if(deviceKey != null){
						sendumDAO.updateDeviceKey(deviceId, deviceKey.toString());
					}
				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device key updated successfully for device "+deviceId);
			}
		}finally{
		}
		return response;
	}
	
	
	@POST
    @Path("/importDevices")
    @Consumes(MediaType.MULTIPART_FORM_DATA)
	public Response importDevices(@Context HttpServletRequest httpRequest, MultipartFormDataInput multipartFormDataInput){
		Response commResponse;
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
        MultivaluedMap<String, String> multivaluedMap = null;
        try{

        	Map<String, List<InputPart>> params = multipartFormDataInput.getFormDataMap();
        	List<InputPart> inputParts = params.get("file");
        	String deviceType = null;
        	Integer orgId = null;
        	if(params.get("deviceType")!= null &&  params.get("deviceType").get(0)!= null)
        		deviceType = params.get("deviceType").get(0).getBodyAsString();
        	if(params.get("orgId")!= null &&  params.get("orgId").get(0)!= null)
        		orgId = Integer.valueOf(params.get("orgId").get(0).getBodyAsString());
        	if(orgId == null || orgId < 0){
        		return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
        				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
        	}
        	if(deviceType == null){
        		return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
        				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("deviceType cannot be null")).build();
        	}
        	boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
        	if(!validToken){
        		return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
        	}
        	for (InputPart inputPart : inputParts) {
        		// convert the uploaded file to inputstream
        		InputStream inputStream = inputPart.getBody(InputStream.class, null);
        		multivaluedMap = inputPart.getHeaders();
        		String fileExtn = getFileExtension(multivaluedMap);
        		Workbook workbook = null;
        		
        		if("unknown".equalsIgnoreCase(fileExtn)){
        			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
        					.setResponseMessage("Unknow file extension. Please import file with xls or xlsx extension")).build();
        		}
        	/*	// Using XSSF for xlsx format, for xls use HSSF
        		if("xlsx".equalsIgnoreCase(fileExtn))
        			workbook = new XSSFWorkbook(inputStream);
        		else if("xls".equalsIgnoreCase(fileExtn))
        			workbook = new HSSFWorkbook(inputStream);*/
        		workbook = WorkbookFactory.create(inputStream);
        		if(workbook!= null){ 
        			Sheet sheet = workbook.getSheetAt(0);
        			Iterator<Row> rowIterator = sheet.iterator();
        			List<DeviceDetail> itemListDetails = new ArrayList<DeviceDetail>();
        			DeviceDetail deviceDetail;
        			//iterating over each row
        			int rowIndex = 0;
        			while (rowIterator.hasNext()) {
        				// skipping the header row
        				if(rowIndex == 0){
        					rowIndex ++;
        					//boolean valid = getColumnNumberColumnMapping( rowIterator.next(),headerColumnMapping);
        					boolean valid = validateHeader(rowIterator.next());
        					if(!valid){
        						return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
        								.setResponseMessage("Headers deviceId, DeviceKey or DeviceMdn are not present in uploaded excel")).build();
        					}
        					continue;
        				}
        				deviceDetail = new DeviceDetail();
        				deviceDetail.setDeviceType(deviceType);
        				deviceDetail.setActive(true);
        				Row row = rowIterator.next();
        				Iterator<Cell> cellIterator = row.cellIterator();
        				//Iterating over each cell (column wise)  in a particular row.
        				while (cellIterator.hasNext()) {
        					Cell cell = cellIterator.next();
        					//Cell with index 1 contains marks in deviceID
        					if (cell.getColumnIndex() == 0) {
        						deviceDetail.setDeviceId(getCellValue(cell));
        					}
        					//Cell with index 2 contains marks in device key
        					else if (cell.getColumnIndex() == 1) {
        						deviceDetail.setDeviceKey(getCellValue(cell));
        					}
        					//Cell with index 3 contains marks in device MDN
        					else if (cell.getColumnIndex() == 2) {
        						deviceDetail.setMdn(getCellValue(cell));
        					}
        				}
        				itemListDetails.add(deviceDetail);
        				rowIndex ++;
        			}
        			if(itemListDetails.size() >0){
        				SensorRequest request = new SensorRequest();
        				request.setOrgId(orgId);
        				request.setItemListDetails(itemListDetails);
        				commResponse = commissionDevices(httpRequest,request);
        				SensorResponse commResp = (SensorResponse)commResponse.getEntity();
        				if(SensorConstant.RESP_STAT_ERROR.equals(commResp.getResponseStatus())){
        					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(commResp).build();
        				}
        			}else{
        				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
        						response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No device data present to import")).build();
        			}
        		}else{
        			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
        					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("No sheet present in imported Excel")).build();
        		}
        	}
        	return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
    				response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Devices imported successfully")).build();
        }catch(Exception e){
			logger.error("Exception in SensorService.importDevices "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SensorService.importDevices Request :: " +" Response :: "+response );
		}
	}
	
	/**
	 * 
	 * @param row
	 * @return
	 */
	private boolean validateHeader(Row row) {
		Iterator<Cell> cellIterator = row.cellIterator();
		//Iterating over each cell (column wise)  in a particular row.
		boolean isId = false, isKey = false, isMdn = false;
		String value;
		while (cellIterator.hasNext()) {
			Cell cell = cellIterator.next();
			if (cell.getColumnIndex() == 0) {
				value = getCellValue(cell);
				if("deviceId".equalsIgnoreCase(value)){
					isId = true;
				}
			}
			//Cell with index 2 contains marks in device key
			else if (cell.getColumnIndex() == 1) {
				value = getCellValue(cell);
				if("deviceKey".equalsIgnoreCase(value)){
					isKey = true;
				}
			}
			//Cell with index 3 contains marks in device MDN
			else if (cell.getColumnIndex() == 2) {
				value = getCellValue(cell);
				if("deviceMDN".equalsIgnoreCase(value)){
					isMdn = true;
				}
			}
		}
		if(isId && isKey && isMdn)
			return true;
		else
			return false;
	}

	/**
	 * 
	 * @param cell
	 * @return
	 */
	private String getCellValue(Cell cell){
		String value = null;
		if (Cell.CELL_TYPE_STRING == cell.getCellType()) {
			value = cell.getStringCellValue();
			//The Cell Containing numeric value will contain marks
		} else if (Cell.CELL_TYPE_NUMERIC == cell.getCellType()) {
			value = String.valueOf(new Double(cell.getNumericCellValue()).intValue());
		}
		return value;
	}
	
	/**
	 * 
	 * @param header
	 * @return
	 */
	private String getFileExtension(MultivaluedMap<String, String> header) {
		String[] contentDisposition = header.getFirst("Content-Disposition").split(";");
		for (String filename : contentDisposition) {
			if ((filename.trim().startsWith("filename"))) {
				String[] name = filename.split("=");
				String finalFileName = name[1].trim().replaceAll("\"", "");
				if(finalFileName != null && finalFileName.lastIndexOf('.')!=-1 ){
					String extn = finalFileName.substring(finalFileName.lastIndexOf('.')+1, finalFileName.length());
					if(extn!= null && (extn.equalsIgnoreCase("xls")||extn.equalsIgnoreCase("xlsx"))){
						return extn;
					} else{
						return  "unknown";
					}
				}
			}
		}
		return "unknown";
	}
}