package com.rfxcel.sensor.service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;


/**
 * 
 * @author Arun Rao
 * @since 2017-01-17
 *
 */
@Path("sensor")
public class MobileAPI {
	private static Logger logger = Logger.getLogger(SensorService.class);
	private static DiagnosticService diagnosticService = DiagnosticService
			.getInstance();
	private static HashMap<String, String> systemCache = null;

	public static class MobileRequest {
		public String systemCode = null;

		public String getSystemCode() {
			return this.systemCode;
		}

		public void setSystemCode(String systemCode) {
			this.systemCode = systemCode;
		}
	}

	public static class MobileResponse {
		public String responseStatus = null;
		public String responseMessage = null;
		public String systemCode = null;
		public String systemURL = null;
		public HashMap<String, String> endpointURLs = new HashMap<String, String>();

		public String getSystemCode() {
			return this.systemCode;
		}

		public void setSystemCode(String systemCode) {
			this.systemCode = systemCode;
		}

		public String getResponseStatus() {
			return this.responseStatus;
		}

		public MobileResponse setResponseStatus(String responseStatus) {
			this.responseStatus = responseStatus;
			return this;
		}

		public String getResponseMessage() {
			return this.responseMessage;
		}

		public void setResponseMessage(String responseMessage) {
			this.responseMessage = responseMessage;
		}

		public String getSystemURL() {
			return this.systemURL;
		}

		public void setSystemURL(String systemURL) {
			this.systemURL = systemURL;
		}

		public HashMap<String, String> getEndpointURLs() {
			return this.endpointURLs;
		}

		public void addEndpointURLs(String key, String value) {
			this.endpointURLs.put(key, value);
		}
	}

	@GET
	@Path("/api/v1")
	@Produces({ "application/json" })
	public Response getApiV1(@QueryParam("code") String systemCode) {
		MobileResponse res = new MobileResponse();
		try {
			res = getEndpoints(systemCode);
			return Response.status(200).entity(res.setResponseStatus("success")).build();
		} catch (Exception e) {
			this.logger.error("Exception in getApiV1 " + e.getMessage());
			e.printStackTrace();
			res.setResponseStatus("error").setResponseMessage(
					e.getLocalizedMessage());
			return Response.status(500).entity(res).build();
		} finally {
			diagnosticService.write("API :: getApiV1  Request :: "
					+ systemCode + " Response :: " + res);
		}
	}
	
	private void addEndpointURL(MobileResponse res, String url, String endpoint)
	{
		this.addEndpointURL(res, url, endpoint, null);
	}

	private void addEndpointURL(MobileResponse res, String url, String endpoint, String extra)
	{
		String path = url + "/rest/sensor/" + endpoint;
		if (extra != null) {
			path = path + "/" + extra;
		}
		res.addEndpointURLs(endpoint, path);
	}

	
	public static void updateSystemCache() 
	{
		systemCache = new HashMap<String, String>();
		Connection connection = null;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT  system_code, system_url FROM sensor_systems";
			PreparedStatement prepStmt = connection.prepareStatement(query);
			ResultSet rs = prepStmt.executeQuery();
			while(rs.next()){
				String code = rs.getString("system_code");
				String url = rs.getString("system_url");
				systemCache.put(code,  url);
			}
			rs.close();
			prepStmt.close();
	
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(" Error updating system cache: " + e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(connection);
		}
	}
	
	private MobileResponse getEndpoints(String systemCode) {
		MobileResponse res = new MobileResponse();
		res.setSystemCode(systemCode);
		String url = null;
		if (systemCache == null) {
			updateSystemCache();
		}
		url = systemCache.get(systemCode);
		if (url == null) {
			res.setResponseMessage("Unknown code " + systemCode);
			return(res);
		}
		res.setSystemURL(url);
		addEndpointURL(res, url, "addProduct");
		addEndpointURL(res, url, "addThreshHold");
		addEndpointURL(res, url, "api", "v1");
		addEndpointURL(res, url, "associate");
		addEndpointURL(res, url, "changeNotificationStatus");
		addEndpointURL(res, url, "commission");
		addEndpointURL(res, url, "commissionPackage");
		addEndpointURL(res, url, "decommissionDevice");
		addEndpointURL(res, url, "deCommissionPackage");
		addEndpointURL(res, url, "disassociate");
		addEndpointURL(res, url, "favourite");
		addEndpointURL(res, url, "findAddress");
		addEndpointURL(res, url, "findLoctionName");
		addEndpointURL(res, url, "getShipmentList");
		addEndpointURL(res, url, "getAttrData");
		addEndpointURL(res, url, "getAttributesByType");
		addEndpointURL(res, url, "getAttrInfo");
		addEndpointURL(res, url, "getDeviceByID");
		addEndpointURL(res, url, "getDeviceByName");
		addEndpointURL(res, url, "getDeviceStatus");
		addEndpointURL(res, url, "getDeviceTypes");
		addEndpointURL(res, url, "getMapData");
		addEndpointURL(res, url, "getNotificationCount");
		addEndpointURL(res, url, "getNotifications");
		addEndpointURL(res, url, "getPackageStatus");
		addEndpointURL(res, url, "getProducts");
		addEndpointURL(res, url, "getShipmentDetails");
		addEndpointURL(res, url, "getShipmentNotifications");
		addEndpointURL(res, url, "getShipmentReport");
		addEndpointURL(res, url, "listConfig");
		addEndpointURL(res, url, "resetData");
		addEndpointURL(res, url, "startSimulator");
		addEndpointURL(res, url, "user/forgetPassword");
		addEndpointURL(res, url, "user/login");
		addEndpointURL(res, url, "user/logout");
		addEndpointURL(res, url, "user/resetPassword");
		return res;
	}

}
