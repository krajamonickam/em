package com.rfxcel.sensor.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.service.OrganizationService;
import com.rfxcel.rule.service.RuleCache;
import com.rfxcel.sensor.beans.Profile;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.dao.ProfileDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.ProfileRequest;
import com.rfxcel.sensor.vo.ProfileResponse;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;

/**
 * 
 *@author Vijay kumar
 * 
 *
 */
@Path("profile")
public class ProfileService {	
	private static Logger logger = Logger.getLogger(ProfileService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static ProfileDAO profileDao = ProfileDAO.getInstance();
	private static AssociationDAO assoDao = AssociationDAO.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	/**
	 * Default constructor
	 */
	public ProfileService() {
		//simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); -- check if we need to set response time as a UTC value 
	}
	//get profiles by device ID
	@POST
	@Path("/getProfilesByDeviceType")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getProfilesByDeviceType(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse res = new ProfileResponse(simpleDateFormat.format(new Date()));
		try {
			//Check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}
			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();
			if(orgId == null || profile.getGroupId() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId and GroupId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			List<Profile> profileList =null;
			profileList =profileDao.getProfilesByDeviceType(profile,request.getDeviceTypeId());
			res.setProfileList(profileList);
			res.setResponseStatus("success");

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in getProfilesByDeviceType "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getProfilesByDeviceType.  Request :: "+request +" Response :: "+res );
		}

	}


	@POST
	@Path("/getProfiles")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getProfiles(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse res = new ProfileResponse(simpleDateFormat.format(new Date()));
		try {
			//Check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}
			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();
			if(orgId == null ){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			if(profile.getGroupId() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("GroupId cannot be null.")).build();
			}
			List<Profile> profileList =null;
			if(profile.getOrgId() == 0){
				profileList = profileDao.getListProfiles();
			}else{
				profileList =profileDao.getListProfiles(profile);
			}
			res.setProfileList(profileList);
			res.setResponseStatus("success");

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in getProfiles "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getProfiles.  Request :: "+request +" Response :: "+res );
		}

	}

	//Create profile
	@POST
	@Path("createProfile")
	@Consumes("application/json")
	@Produces("application/json")
	public Response createProfile(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse response = new ProfileResponse(simpleDateFormat.format(new Date()));
		try {
			//check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}

			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			String profileName = profile.getName();
			if(profileName == null || profileName.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile name cannot be null or empty.")).build();
			}

			//check if name is present
			Boolean present = profileDao.checkIfProfileNameIsPresent(profileName, orgId);
			if(present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile name "+profileName+" is already present for organization.")).build();
			}

			//check orgId, groupId and deviceTypeId is not null
			Long groupId = profile.getGroupId();
			Integer deviceTypeId = profile.getDeviceTypeId();
			if(orgId == null || orgId <= 0 || groupId == null || groupId<=0 || deviceTypeId == null || deviceTypeId<=0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId or GroupId or DeviceTypeId cannot be null.")).build();
			}

			Integer profileId = profileDao.createProfile(profile);
			if(profileId > 0){
				//adding geopoints into devicecache
				DeviceCache.getInstance().addGeoPoints(profileId);
				AssociationCache.getInstance().populateShipmentsHomeGeopoint();
				RuleCache.getInstance().populate();
				Integer attrFreq = profile.getAttbFrequency();
				if(attrFreq != null){
					AssociationCache.getInstance().addProfileTempFrequency(profileId,attrFreq );
				}
				String userName = tokenManager.getUserForToken(httpRequest);
				Long userId = tokenManager.getUserId(httpRequest);
				AuditLog auditLog = new AuditLog(userId, userName, new Long(profileId), "Profile", "Create", null, profile.toJson(), "User "+userName +" has created profile "+profileName,orgId);
				auditLogDao.addAuditLog(auditLog);
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Profile created successfully")).build();
			}else{
				return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)).build();
			}				

		} catch (Exception e) {
			logger.error("Exception in ProfileService.createProfile "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: ProfileService.createProfile Request :: "+request +" Response :: "+response );
		}

	}

	//Update profile
	@POST
	@Path("updateProfile")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateProfile(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse response = new ProfileResponse(simpleDateFormat.format(new Date()));
		try{

			//check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}

			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}

			Integer profileId = profile.getProfileId();
			if(profileId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile Id cannot be null.")).build();
			}

			//check active profile is present or not
			Boolean present = profileDao.checkIfActiveProfileIsPresent(profileId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile is not configured or is already deleted.")).build();
			}

			//check if name is present
			String profileName = profile.getName();
			Boolean presentName = profileDao.checkProfileNameIsPresentforModify(profileName,profileId, orgId);
			if(presentName){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile name "+profileName+" is already present in the system.")).build();
			}

			//check orgId, groupId and deviceTypeId is not null
			Long groupId = profile.getGroupId();
			Integer deviceTypeId = profile.getDeviceTypeId();
			if(orgId == null || orgId <= 0  || groupId == null || groupId <= 0 || deviceTypeId == null || deviceTypeId <= 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId or GroupId or DeviceTypeId cannot be null.")).build();
			}
			Profile oldprofile = profileDao.getProfileByProfileId(profileId);
			//updateProfile
			Boolean updateStatus = profileDao.updateProfile(profile);
			if(updateStatus){
				// update devices with new Profile
				applyDeviceProfile(profileId, orgId);
				
				//adding geopoints into device cache
				DeviceCache.getInstance().addGeoPoints(profileId);
				AssociationCache.getInstance().populateShipmentsHomeGeopoint();
				RuleCache.getInstance().populate();
				Integer attrFreq = profile.getAttbFrequency();
				if(attrFreq != null){
					AssociationCache.getInstance().addProfileTempFrequency(profileId,attrFreq);
				}
				
				String userName = tokenManager.getUserForToken(httpRequest);
				Long userId = tokenManager.getUserId(httpRequest);
				
				AuditLog auditLog = new AuditLog(userId, userName, new Long(profileId), "Profile", "Update", oldprofile.toJson(), profile.toJson(), "User "+userName +" has updated profile "+oldprofile.getName(), orgId);
				auditLogDao.addAuditLog(auditLog);
				
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Profile updated successfully")).build();
			}else{
				return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)).build();
			}				

		}catch(Exception e){
			logger.error("Exception in ProfileService.updateProfile "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: ProfileService.updateProfile Request :: "+request +" Response :: "+response );
		}
	}

	//Delete profile
	@POST
	@Path("deleteProfile")
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteProfile(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse response = new ProfileResponse(simpleDateFormat.format(new Date()));
		try{
			//check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}

			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			Integer profileId = profile.getProfileId();
			if(profileId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile Id cannot be null.")).build();
			}

			//check if active profile is present
			Boolean present = profileDao.checkIfActiveProfileIsPresent(profileId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile is not configured or is already deleted.")).build();
			}
			present = assoDao.checkIfProfileIsAssociated(profileId);
			if(present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("This profile has been associated to the shipment(s), Please disassociate the profile from the shipment(s) before deleting")).build();
			}
			//delete the profile
			profileDao.deleteProfile(profileId);
			DeviceCache.getInstance().removeProfileGeoPoints(profileId);
			AssociationCache.getInstance().removeShipmentHomeGeopoint(profileId);
			RuleCache.getInstance().removeRules(profileId);
			AssociationCache.getInstance().removeProfileTempFrequency(profileId);
			
			String userName = tokenManager.getUserForToken(httpRequest);
			Long userId = tokenManager.getUserId(httpRequest);
			profile = profileDao.getProfileByProfileId(profileId);
			AuditLog auditLog = new AuditLog(userId, userName, new Long(profileId), "Profile", "Delete", null, profile.toJson(), "User "+userName +" has deleted profile "+profile.getName(), orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Profile deleted successfully")).build();

		}catch(Exception e){
			logger.error("Exception in ProfileService.deleteProfile "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: ProfileService.deleteProfile Request :: "+request +" Response :: "+response );
		}
	}

	//get profiles by device ID
	@POST
	@Path("/getProfile")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getProfile(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse res = new ProfileResponse(simpleDateFormat.format(new Date()));
		try {
			//check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}

			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();			
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if(orgId!= null && orgId!=0){
				com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
				if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
				}
			}

			Boolean present = profileDao.checkIfActiveProfileIsPresent(request.getProfile().getProfileId());
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile is not configured or is already deleted.")).build();
			}
			res.setProfile(profileDao.getProfile(request));
			res.setResponseStatus("success");

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			logger.error("Exception in getProfile "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: getProfile.  Request :: "+request +" Response :: "+res );
		}
	}


	//get default geolocation radius value & format
	@POST
	@Path("/getGeolocationRadiusAndFormat")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getRadiusValue(@Context HttpServletRequest httpRequest, SensorRequest request) throws SQLException {
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		Sensor sensor = new Sensor();
		HashMap<String, String> configMap = new HashMap<>();
		try {
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			if (request.getConfigData()==null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("configData is null")).build();
			}
			if (request.getConfigData()!= null && request.getConfigData().getParams()==null ) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("params are missing in the request")).build();
			}
			String geoLocationRadiusValue = ConfigCache.getInstance().getSystemConfig(request.getConfigData().getParams().get(0));
			if(geoLocationRadiusValue == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Geo location radius value is not configured")).build();
			}
			configMap.put(request.getConfigData().getParams().get(0), geoLocationRadiusValue);
			String geoLocationRadiusFormat = ConfigCache.getInstance().getSystemConfig(request.getConfigData().getParams().get(1));
			if(geoLocationRadiusFormat == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR)
						.setResponseMessage("Geo location radius format is not configured")).build();
			}
			configMap.put(request.getConfigData().getParams().get(1), geoLocationRadiusFormat);
			sensor.setConfigMap(configMap);
			response.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response).build();
		} catch (Exception e) {
			logger.error("Exception in getRadiusValue "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: getRadiusValue.  Request :: "+request +" Response :: "+response);
		}
	}

	/**
	 * <p> Deactivate profiles for a deviceTypeId
	 * @param deviceTypeId
	 *//*
	public static void deactivateProfilesForOrg(Integer orgId) {
		try{
			profileDao.deactivateProfilesForOrg(orgId);
			ArrayList<Integer> profileIds = profileDao.getProfilesForOrg(orgId);
			DeviceCache.getInstance().removeProfileGeoPoints(profileIds);
			RuleCache.getInstance().removeRules(profileIds);
		}catch(Exception e){
			logger.error("Exception in ProfileService.deactivateProfilesForOrg "+ e.getMessage());
			e.printStackTrace();
		}finally{
			diagnosticService.write("API :: ProfileService.deactivateProfilesForOrg " );
		}
	}*/

	
	/**
	 * 
	 * @param profileId
	 * @param orgId
	 */
	public void applyDeviceProfile(Integer profileId, Integer orgId){
		try {
			HashMap<String, String> associations = assoDao.getAssociationForProfile(profileId);
			for(String deviceId : associations.keySet()){
				String packageId = associations.get(deviceId);
				//Currently setting userId as 0 for system
				//Also groupId is set to null
				try{
					SensorService.applyDeviceProfile(deviceId, packageId, profileId, 0L, orgId, null);
				}catch(Exception e){
					logger.error("Error while updating device Profile for device "+deviceId +" . Error is " +e.getMessage());
				}
			}
		} catch (SQLException e) {
			logger.error("Error while updating device profile ");
			e.printStackTrace();
		}
	}
	
	//get profile Geopoints i.e. home and destination geopoint
	@POST
	@Path("/getProfileGeopoints")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getProfileGeopoints(@Context HttpServletRequest httpRequest, ProfileRequest request) throws SQLException {
		ProfileResponse response = new ProfileResponse(simpleDateFormat.format(new Date()));
		try{
			//check if request is not empty
			if(request == null || request.getProfile() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile information cannot be null.")).build();
			}
			//check if profileId is not null
			Profile profile = request.getProfile();
			Integer orgId = profile.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			Integer profileId = profile.getProfileId();
			if(profileId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile Id cannot be null.")).build();
			}
			//check if profile is active
			Boolean present = profileDao.checkIfActiveProfileIsPresent(profileId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Profile is not configured or is already deleted.")).build();
			}
			//get ProfileGeopoint
			response.setProfile(profileDao.getProfileGeopoints(profileId));
			
			response.setResponseStatus("success");
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response).build();
			
		}catch (Exception e) {
			logger.error("Exception in getProfileGeopoints "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: getProfileGeopoints.  Request :: "+request +" Response :: "+response );
		}
	}
		
			
	
	
}