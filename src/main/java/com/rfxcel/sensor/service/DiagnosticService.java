package com.rfxcel.sensor.service;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;

/**
 * 
 * @author tejshree_kachare
 *
 */
public class DiagnosticService {

	private static final Logger logger = Logger.getLogger(DiagnosticService.class);
	private static DiagnosticService instance =null;
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss.SSS");
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");//will consider server timezone for logging data
	private static Properties prop = new Properties();


	public static DiagnosticService getInstance(){
		synchronized (DiagnosticService.class) {
			if(instance == null){
				instance = new DiagnosticService();
			}
		}
		return instance;
	}

	/**
	 * @return the prop
	 */
	public Properties getProp() {
		return prop;
	}

	public void write(String log){
		PrintWriter pw = null;
		try {
			
			String fileName = createFile();
			if(fileName == null){
				logger.error("Please check diagnostic.logFolder in the properties file");
			}else{
				pw = new PrintWriter(new FileWriter(fileName, true));// true to append data to same file
				pw.println(timeFormat.format(new Date()) + " ::  " + log);
				pw.flush();
			}
		} catch(Exception e){
			logger.error("Exception while writing to log file "+e.getMessage());
		}
		finally {
				if(	pw != null){
					pw.flush();
					pw.close();
				}
		}
	}
	

	/**
	 * Create directory to store the incoming data, if not already present
	 * @return
	 */
	private  String createFile()
	{
		String directory = ConfigCache.getInstance().getSystemConfig("diagnostic.logFolder");
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				logger.info("Directory " + directory + " does not exist!");
				return(null);
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return(null);
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(dir, suffix + ".log");
		return(file.getAbsolutePath());
	}
}
