package com.rfxcel.sensor.thingspace.util;

import java.io.File;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;

public class ThingSpaceUtils {
	private static final Logger logger = Logger.getLogger(ThingSpaceUtils.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static ConfigCache configCache = ConfigCache.getInstance();
	
	public static void logMessage(String payload)
	{
		logMessage("sensor", payload);
	}
	
	public static void logMessage(String deviceId, String payload)
	{
		try{
			String fileName = createFile(deviceId);
			PrintWriter pw = new PrintWriter(new FileWriter(fileName, true));
			pw.println(timeFormat.format(new Date()) + " " + payload.replace('\n', ' ').replace('\r', ' ').replace('\t', ' '));
			pw.flush();
			pw.close();
		}catch (Exception e){							
			logger.error("Error logging data to log file "+ e.getMessage());
			logger.error("Message is : "+ payload);
		}
		
	}
	public static String createFile(String deviceid)
	{
		String directory = configCache.getSystemConfig("thingspace.logFolder");
		if(directory == null){
			return null;
		}
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				logger.info("Directory " + directory + " does not exist!");
				return null;
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return null;
		}
		File subDir = new File(directory, deviceid);
		if (!subDir.exists()) {
			if (!subDir.mkdirs()) {
				logger.info("Directory " + subDir.getAbsolutePath() + " does not exist!");
				return null;
			}
		}
		if (!subDir.isDirectory()) {
			logger.info("Directory " + subDir.getAbsolutePath() + " not a directory!");
			return null;
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(subDir, deviceid + "-" + suffix + ".log");
		return(file.getAbsolutePath());
	}

}
