package com.rfxcel.sensor.thingspace.util;


/**
 * 
 * @author Tejshree Kachare
 * @since 17th Aug 2017
 */

public class ThingSpaceConstants {

	public static final String ATTRIBUTE_ID = "id" ;
	public static final String ATTRIBUTE_LOCATION = "location";
	public static final String ATTRIBUTE_LATITUDE = "latitude";	
	public static final String ATTRIBUTE_LONGITUDE = "longitude" ;
	public static final String ATTRIBUTE_DATETIME =  "createdon";	
	public static final String ATTRIBUTE_TEMPERATURE = "temperature" ;	
	public static final String ATTRIBUTE_BATTERY = "battery" ;	
	public static final String ATTRIBUTE_LIGHT = "light" ;	
	public static final String ATTRIBUTE_TILT = "tilt" ;	
	public static final String ATTRIBUTE_PRESSURE = "pressure" ;	
	public static final String ATTRIBUTE_HUMIDITY = "humidity" ;
	public static final String ATTRIBUTE_SHOCK = "shock";
	public static final String ATTRIBUTE_VIBRATION = "vibration" ;
	public static final String ATTRIBUTE_FIELDS = "fields";
	public static final String ATTRIBUTE_DEVICEALARM = "deviceAlarm";
	public static final String ATTRIBUTE_SENSORREADING = "sensorReading";
	
	public static final String ATTRIBUTE_ACCELERATION = "acceleration";
	public static final String ATTRIBUTE_ACCELERATION_X = "x";
	public static final String ATTRIBUTE_ACCELERATION_Y = "y";
	public static final String ATTRIBUTE_ACCELERATION_Z = "z";
	
}
