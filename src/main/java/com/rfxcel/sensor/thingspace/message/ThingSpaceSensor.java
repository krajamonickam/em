package com.rfxcel.sensor.thingspace.message;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.thingspace.util.ThingSpaceConstants;
import com.rfxcel.sensor.util.Sensor;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;

/**
 * 
 * @author Tejshree Kachare
 * @since 17 Aug 2017
 *
 */
public class ThingSpaceSensor extends Sensor{

	private static final String INCOMING_DATE_FORMAT = "yyyy-MM-dd'T'HH:mm:ss'Z'";
	private static final String CONVERSION_FORMAT = "yyyy-MM-dd HH:mm:ss";
	private static ConfigCache configCache = ConfigCache.getInstance();
	public ThingSpaceSensor() {
		this.deviceType = SensorConstant.SENSOR_VERIZON;
	}

	/**
	 * since each sensor data has different attribute names, mapping those  in constructor to a standard value
	 * @param kirsenData
	 */
	public ThingSpaceSensor(Map<String, Object> data, String deviceID) {
		this.deviceType = SensorConstant.SENSOR_VERIZON;
		Object identifier = data.get(ThingSpaceConstants.ATTRIBUTE_ID);
		if( identifier== null || identifier==""){
			throw new IllegalArgumentException("Device Identifier field cannot be null or empty");
		}else {
			this.deviceIdentifier = deviceID;
		}
		Object statusTime = data.get(ThingSpaceConstants.ATTRIBUTE_DATETIME);
		if(statusTime == null || statusTime ==""){
			//this.statusTimeStamp = Utility.convertDateFormat(INCOMING_DATE_FORMAT, CONVERSION_FORMAT, null);
			this.statusTimeStamp = null;
		}else{
			String sTime = statusTime.toString();
			//Skipping the value after '.'.  "2017-10-27T20:22:32.632061281Z" is converted to  "2017-10-27T20:22:32Z"
			if(sTime.contains(".")){
				sTime = sTime.substring(0, sTime.indexOf('.'));
				sTime = sTime +'Z';
			}
			this.statusTimeStamp = Utility.convertDateFormat(INCOMING_DATE_FORMAT, CONVERSION_FORMAT, sTime);
		}
		Object dataFields = data.get(ThingSpaceConstants.ATTRIBUTE_FIELDS);
		if (dataFields == null) {
			// fake it if no attribute info available
			dataFields = new HashMap<String, String>();
		}
		if(dataFields != null){
			HashMap<String, Object> fields = (HashMap<String, Object>) dataFields;
			if(fields.containsKey(ThingSpaceConstants.ATTRIBUTE_LOCATION)){
				HashMap<String, String>  locationFields =(HashMap<String, String>) fields.get(ThingSpaceConstants.ATTRIBUTE_LOCATION);
				this.latitude =(locationFields.get(ThingSpaceConstants.ATTRIBUTE_LATITUDE)== null || locationFields.get(ThingSpaceConstants.ATTRIBUTE_LATITUDE) == "")
						? null : locationFields.get(ThingSpaceConstants.ATTRIBUTE_LATITUDE);
				this.longitude = (locationFields.get(ThingSpaceConstants.ATTRIBUTE_LONGITUDE) == null || locationFields.get(ThingSpaceConstants.ATTRIBUTE_LONGITUDE) == "")
						? null : locationFields.get(ThingSpaceConstants.ATTRIBUTE_LONGITUDE);
			}

			this.temperature =(fields.get(ThingSpaceConstants.ATTRIBUTE_TEMPERATURE) == null || fields.get(ThingSpaceConstants.ATTRIBUTE_TEMPERATURE).equals("") )
					? null : fields.get(ThingSpaceConstants.ATTRIBUTE_TEMPERATURE).toString();
			this.battery = (fields.get(ThingSpaceConstants.ATTRIBUTE_BATTERY) == null || fields.get(ThingSpaceConstants.ATTRIBUTE_BATTERY).equals("") )
					? null : fields.get(ThingSpaceConstants.ATTRIBUTE_BATTERY).toString();
			this.light = (fields.get(ThingSpaceConstants.ATTRIBUTE_LIGHT) == null || fields.get(ThingSpaceConstants.ATTRIBUTE_LIGHT).equals(""))
					? null : fields.get(ThingSpaceConstants.ATTRIBUTE_LIGHT).toString();
			this.tilt =(fields.get(ThingSpaceConstants.ATTRIBUTE_TILT) == null || fields.get(ThingSpaceConstants.ATTRIBUTE_TILT).equals(""))
					? null : fields.get(ThingSpaceConstants.ATTRIBUTE_TILT).toString();
			this.pressure = (fields.get(ThingSpaceConstants.ATTRIBUTE_PRESSURE) == null || fields.get(ThingSpaceConstants.ATTRIBUTE_PRESSURE).equals(""))
					? null : fields.get(ThingSpaceConstants.ATTRIBUTE_PRESSURE).toString();
			this.humidity = (fields.get(ThingSpaceConstants.ATTRIBUTE_HUMIDITY) == null || fields.get(ThingSpaceConstants.ATTRIBUTE_HUMIDITY).equals("")) 
					? null : fields.get(ThingSpaceConstants.ATTRIBUTE_HUMIDITY).toString();
			
			if(fields.containsKey(ThingSpaceConstants.ATTRIBUTE_DEVICEALARM)){
				HashMap<String, Object>  alarmFields =(HashMap<String, Object>) fields.get(ThingSpaceConstants.ATTRIBUTE_DEVICEALARM);
				if(alarmFields.containsKey(ThingSpaceConstants.ATTRIBUTE_TEMPERATURE)){
					HashMap<String, String>  temperatureFields =(HashMap<String, String>) alarmFields.get(ThingSpaceConstants.ATTRIBUTE_TEMPERATURE);
					this.temperature = temperatureFields.get(ThingSpaceConstants.ATTRIBUTE_SENSORREADING);
				} 
				if(alarmFields.containsKey(ThingSpaceConstants.ATTRIBUTE_PRESSURE)){
					HashMap<String, String>  pressureFields =(HashMap<String, String>) alarmFields.get(ThingSpaceConstants.ATTRIBUTE_PRESSURE);
					this.pressure = pressureFields.get(ThingSpaceConstants.ATTRIBUTE_SENSORREADING);
				} 
				if(alarmFields.containsKey(ThingSpaceConstants.ATTRIBUTE_HUMIDITY)){
					HashMap<String, String>  humidityFields =(HashMap<String, String>) alarmFields.get(ThingSpaceConstants.ATTRIBUTE_HUMIDITY);
					this.humidity = humidityFields.get(ThingSpaceConstants.ATTRIBUTE_SENSORREADING);
				} 
				if(alarmFields.containsKey(ThingSpaceConstants.ATTRIBUTE_LIGHT)){
					HashMap<String, String>  lightFields =(HashMap<String, String>) alarmFields.get(ThingSpaceConstants.ATTRIBUTE_LIGHT);
					this.light = lightFields.get(ThingSpaceConstants.ATTRIBUTE_SENSORREADING);
				} 
				if(alarmFields.containsKey(ThingSpaceConstants.ATTRIBUTE_BATTERY)){
					HashMap<String, String>  batteryFields =(HashMap<String, String>) alarmFields.get(ThingSpaceConstants.ATTRIBUTE_BATTERY);
					this.battery = batteryFields.get(ThingSpaceConstants.ATTRIBUTE_SENSORREADING);
				} 
				if(alarmFields.containsKey(ThingSpaceConstants.ATTRIBUTE_TILT)){
					HashMap<String, String>  tiltFields =(HashMap<String, String>) alarmFields.get(ThingSpaceConstants.ATTRIBUTE_TILT);
					this.tilt = tiltFields.get(ThingSpaceConstants.ATTRIBUTE_SENSORREADING);
				}
				
			}

			if(fields.containsKey(ThingSpaceConstants.ATTRIBUTE_ACCELERATION)){
				Double presetShock = configCache.getSystemConfig("shock.preset.value")== null ? 0.5 : new Double(configCache.getSystemConfig("shock.preset.value"));
				HashMap<String, String>  accFields =(HashMap<String, String>) fields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION);
				Double accX =(accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_X)== null || accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_X) == "")
						? null : new Double(accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_X));
				Double accY = (accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_Y) == null || accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_Y) == "")
						? null : new Double(accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_Y));
				Double accZ = (accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_Z) == null || accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_Z) == "")
						? null : new Double(accFields.get(ThingSpaceConstants.ATTRIBUTE_ACCELERATION_Z));

				if((accX != null && (Math.abs(accX) >= presetShock)) || (accY != null && (Math.abs(accY) >= presetShock)) || (accZ != null && (Math.abs(accZ+ 1) >= presetShock))){
					this.shock = "100";
				}else{
					this.shock ="0";
				}
			}
		}
	}


	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}

	/**
	 * @param json
	 * @return
	 */
	public static ThingSpaceSensor fromJson(String json) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			ThingSpaceSensor sensorData = mapper.readValue(json, ThingSpaceSensor.class);
			return(sensorData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null; 
	}
}
