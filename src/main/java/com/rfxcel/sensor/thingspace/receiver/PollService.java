package com.rfxcel.sensor.thingspace.receiver;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Map;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.org.dao.OrganizationDAO;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.control.RESTClient;
import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.dao.DeviceTypeDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.service.SensorProcessJSON;
import com.rfxcel.sensor.thingspace.message.ThingSpaceSensor;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;

/**
 * @author Tejshree Kachare
 * @since 11 Aug 2017
 *
 */
public class PollService {/*implements Runnable {

	private static Logger logger = Logger.getLogger(PollService.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private static PollService instance;
	private Thread thread;
	private String url;
	private RESTClient http = null;
	private static ConfigCache cache = ConfigCache.getInstance();
	private int retryCount = 0;
	*//**
	 *
	 *//*
	private PollService(){
	
		url = cache.getSystemConfig("thingspace.integration.url");
		if(url == null || url.trim().length() == 0){
			logger.error("PollService :: ThingSpace integration url is not configured. Please check system configuration");
		}
		else{
			retryCount = cache.getSystemConfig("thingspace.integration.retry.count")== null ? 1 : Integer.valueOf(cache.getSystemConfig("thingspace.integration.retry.count"));
			http = new RESTClient(null, null);
			thread = new Thread(this);
			thread.start();
		}
	}

	*//**
	 * @return
	 *//*
	public static PollService getInstance(){
		synchronized(PollService.class){
			if(instance == null){
				instance =  new PollService();
			}
		}
		return instance;
	}

	@Override
	public void run() {
		try{
			logger.info("PollService :: Starting Thing space polling service thread.");
			Boolean runPollService = false;
			String authToken;
			String getUrl;
			for(;;){
				runPollService = cache.getSystemConfig("thingspace.poll.service.start") != null ? Boolean.valueOf(cache.getSystemConfig("thingspace.poll.service.start")) : false;
				if(runPollService){
					try{
						//get list of all orgs
						ArrayList<Integer> orgIds = OrganizationDAO.getInstance().getActiveOrgIds();
						for(Integer orgId :orgIds){
							logger.info("PollService :: Processing data for orgainzation with id "+orgId);
							// get authorization token for each organization
							authToken = cache.getOrgConfig("thingspace.auth.token", orgId);
							//check if organization has devices of manufacturer Verizon
							ArrayList<Integer> deviceTypeIds = DeviceTypeDAO.getInstance().getDeviceTypeIdsByOrgId(orgId, SensorConstant.SENSOR_VERIZON);	//-- TODO check if manufacturer name will e
							if(deviceTypeIds !=  null){
								for(Integer deviceTypeId :deviceTypeIds){
									logger.info("PollService :: processing data for deviceType Id "+ deviceTypeId);
									ArrayList<String> deviceIds = AssociationDAO.getInstance().getAssociatedDevicesWithDeviceTypeId(deviceTypeId);
									if(deviceIds != null){
										for(String deviceId : deviceIds){
											Device d = SendumDAO.getInstance().getDeviceById(deviceId, orgId);
											logger.info("PollService :: Making get request for device ID "+ deviceId + " key " + d.getDeviceKey());
											getUrl = url +"/" + d.getDeviceKey();
											getMessage(getUrl, authToken, deviceId);
										}
									}
								}
							}
						}
						

					}catch(Exception e){
						logger.error("PollService :: Error in thingspace polling service.." +e.getMessage());
					}
				}
				try {
					int waitTime = cache.getSystemConfig("thingspace.integration.get.interval")!= null ? Integer.valueOf(cache.getSystemConfig("thingspace.integration.get.interval")) : 720 ;
					logger.info("PollService :: Thread sleeping for "+ waitTime +" mins");
					Thread.sleep(waitTime * 60L * 1000);	
				} catch (Exception ex) {}
			}
		}finally{
			logger.info("PollService :: Exiting thingspace polling service thread.");
		}
	}
	
	
	*//**
	 * 
	 * @param url
	 * @param authToken
	 * @param deviceId 
	 *//*
	private void getMessage(String url, String authToken, String deviceId){
		try{
			//incase of failure make request for retry Count configured
			for(int i=1; i <=retryCount; i++){
				try{
					//make http get call
					String json = http.doGet(url, "Authorization", authToken);
					processMessage(json, deviceId);
					break;
				}catch(Exception e){
					e.printStackTrace();
					logger.error("PollService ::Error encountered while making get request. Attemp count: "+i +". Error : "+ e.getMessage());
				}
			}
		}catch(Exception e){
			logger.error("PollService :: Error while gettting message " + url + " "  +e.getMessage());
		}
	}

	*//**
	 * 
	 * @param json
	 * @param deviceId 
	 *//*
	private void processMessage(String json, String deviceId) {
		try{
			//convert String message to Map
			Map<String, Object> dataMap = Utility.convertNestedJsonToMap(json);
			if(dataMap.containsKey("error")){
				logger.error("PollService :: Error while getting data from thingspace "+ url +" response received is "+ json);
			}else{
				//Code to convert message to sensor format
				ThingSpaceSensor sensor  =  new ThingSpaceSensor(dataMap, deviceId);
				String sensorJson = sensor.toJson();
				logger.info("Received message " + sensorJson);
				try {
					String fileName = createFile(deviceId);
					PrintWriter pw;
					pw = new PrintWriter(new FileWriter(fileName, true));
					pw.println(timeFormat.format(new Date()) + " " + sensorJson);
					pw.flush();
					pw.close();
				} catch (IOException e) {
					logger.error("Error logging thingspace message " +e.getMessage());
					logger.error("Sensor data "+sensorJson);
					e.printStackTrace();
				}
				//call sensor process json
				SensorProcessJSON.getInstance().doQueue(sensorJson);
			}
		}catch(IllegalArgumentException iae){
			logger.error("PollService :: Error while processing data "+ json);
		}
	}
	
	
	public static String createFile(String deviceid)
	{
		String directory = cache.getSystemConfig("thingspace.logfolder");
		if(directory == null){
			return "c:\\logs\\thingspace";
		}
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				logger.info("Directory " + directory + " does not exist!");
				return null;
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return null;
		}
		File subDir = new File(directory, deviceid);
		if (!subDir.exists()) {
			if (!subDir.mkdirs()) {
				logger.info("Directory " + subDir.getAbsolutePath() + " does not exist!");
				return null;
			}
		}
		if (!subDir.isDirectory()) {
			logger.info("Directory " + subDir.getAbsolutePath() + " not a directory!");
			return null;
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(subDir, deviceid + "-" + suffix + ".log");
		return(file.getAbsolutePath());
	}
*/}