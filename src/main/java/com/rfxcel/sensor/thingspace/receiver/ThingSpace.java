package com.rfxcel.sensor.thingspace.receiver;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.service.SensorProcessJSON;
import com.rfxcel.sensor.thingspace.message.ThingSpaceSensor;
import com.rfxcel.sensor.thingspace.util.ThingSpaceConstants;
import com.rfxcel.sensor.thingspace.util.ThingSpaceUtils;
import com.rfxcel.sensor.util.RfxcelException;
import com.rfxcel.sensor.util.Utility;

@WebServlet("/thingspace")
public class ThingSpace extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(ThingSpace.class);
	
	private static ConfigCache configCache = ConfigCache.getInstance();

	public ThingSpace() {}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("text/html");
		OutputStreamWriter ow = new OutputStreamWriter(resp.getOutputStream());
		PrintWriter pw = new PrintWriter(ow);
		pw.println("<html><head><meta http-equiv=\"refresh\" content=\"10\"></head><body>");
		pw.println("<br/>ThingSpace Listener is up and Running!<br/>");
		pw.println("******************************<br/>");
		pw.println("</body></html>");
		pw.flush();
		pw.close();
	}

	@Override
	/**
	 * this is the servlet that accepts the https post request from the calling service.
	 * performs the basic authentication
	 * accepts the json and calls the callProcessMessage method for further processing.
	 */
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		logger.info("ThingSpace message received");
		response.setContentType("text/html");
		final String authorization = request.getHeader("Authorization");
		String requestType = request.getMethod();
		if (authorization != null && authorization.startsWith("Basic") && "POST".equals(requestType)) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			String credentials[] = Utility.decrypytUserNamePwd(base64Credentials);
			String user = configCache.getSystemConfig("kirsen.user") ;
			String password = configCache.getSystemConfig("kirsen.password");
			if (true) {
				InputStream inputStream = request.getInputStream();
				StringBuilder stringBuilder = new StringBuilder();
				BufferedReader bufferedReader = null;
				String payload = null;
				try {
					if (inputStream != null) {
						bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
						char[] charBuffer = new char[128];
						int bytesRead = -1;
						while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
							stringBuilder.append(charBuffer, 0, bytesRead);
						}
					} else {
						stringBuilder.append("");
					}
				} catch (IOException ex) {
					logger.error(ex.getMessage());
				} finally {
					if (bufferedReader != null) {
						try {
							bufferedReader.close();
						} catch (IOException ex) {
							logger.error(ex.getMessage());
						}
					}
				}
				payload = stringBuilder.toString();
				if (!(payload.equals(""))) {
					ThingSpaceUtils.logMessage(payload);
					processPayload(payload);
				} else {
					logger.error("HTTP Payload is empty !!");
				}
			}
		}
		else{
			logger.error("Authentication Failed !!");
			response.setContentType("application/json");
			response.setStatus(401);
			String responseMsg =bindErrorMessage(401,"Unauthorized user");
			response.getWriter().write(responseMsg);
		}
	}
	
	private void processPayload(String payload) 
	{
		try {
			Map<String, Object> dataMap = Utility.convertNestedJsonToMap(payload);
			if (dataMap.get("data") != null) {
				dataMap = (Map<String, Object>) dataMap.get("data");
			}
			if (dataMap.containsKey("deviceid")) {
				String deviceKey = dataMap.get("deviceid").toString();
				Device d = SendumDAO.getInstance().getDeviceByKey(deviceKey);
				if (d == null) {
					logger.error("*** unable to get device by devicekey " + deviceKey + " JSON = " + payload);
					return;
				}
				dataMap.put(ThingSpaceConstants.ATTRIBUTE_ID, d.getDeviceId());
			}
			String id = dataMap.get(ThingSpaceConstants.ATTRIBUTE_ID).toString();
			ThingSpaceSensor sensor = new ThingSpaceSensor(dataMap, id);
			// Following check is explicitly added to skip data point where lat and lang values are sent as "0.0"
			if (("0.0").equals(sensor.getLatitude()) && ("0.0").equals(sensor.getLongitude())){
				logger.info("Skipping data point processing as lat and lang value is 0.0");
				return;
			}
			String sensorJson = sensor.toJson();
			SensorProcessJSON.getInstance().doQueue(sensorJson);
		
		} catch (Exception e) {
			logger.error("Error processing input payload" + payload);
			e.printStackTrace();
		}
	}

	public String bindErrorMessage(int errorCode, String message) {
		ObjectMapper mapper = new ObjectMapper();
		RfxcelException rfxcelException = new RfxcelException();
		rfxcelException.setStatusCode(errorCode);
		rfxcelException.setMessage(message);
		
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(rfxcelException);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return jsonString;
	}


	/**
	 * 
	 * @param deviceid
	 * @return
	 */

}
