package com.rfxcel.sensor.vo;

import java.util.ArrayList;
import java.util.HashMap;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.beans.LocationData;
import com.rfxcel.sensor.beans.MonitorAlerts;
import com.rfxcel.sensor.beans.ProductDetail;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.control.EventController;
import com.rfxcel.sensor.control.EventController.ItemData;
import com.rfxcel.sensor.vo.Response;
/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SensorResponse extends Response
{

	private String packageId;	
	private String productId;
	private String deviceId;
	/**Wrapper for response object*/
	private Sensor sensor;
	private LocationData locationData;
	private ProductDetail productDetail;
	private DeviceDetail deviceDetail;
	private ArrayList<DeviceLog> deviceLogList;
	private HashMap<Object, Object> deviceKeyMap;
	private MonitorAlerts monitorAlerts;
	
	private ItemData itemData;
	
	private String traceId;
	
	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}

	ArrayList<EventController.EventsData> listData = new ArrayList<EventController.EventsData>();
	ArrayList<com.rfxcel.sensor.util.Sensor> sensorDataList = new ArrayList<com.rfxcel.sensor.util.Sensor>();
	
	public ArrayList<EventController.EventsData> getListData() {
		return listData;
	}

	public void setListData(ArrayList<EventController.EventsData> listData) {
		this.listData = listData;
	}

	public ArrayList<com.rfxcel.sensor.util.Sensor> getSensorDataList() {
		return sensorDataList;
	}

	public void setSensorDataList(ArrayList<com.rfxcel.sensor.util.Sensor> sensorDataList) {
		this.sensorDataList = sensorDataList;
	}
	
	/**
	 * default constructor
	 */
	public SensorResponse() {

	}

	public SensorResponse(String dateTime) {
		super.setDateTime( dateTime);
	}

	/**
	 * @return the packageId
	 */
	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return the productId
	 */
	@JsonProperty("productId")
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the deviceId
	 */
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the sensor a wrapper for response object
	 */
	@JsonProperty("sensor")
	public Sensor getSensor() {
		return sensor;
	}
	/**
	 * @param sensor the sensor to set
	 */
	public void setSensor(Sensor sensor) {
		this.sensor = sensor;
	}
	@JsonProperty("locationData")
	public LocationData getLocationData () {
		return locationData;
	}

	public void setLocationData (LocationData locationData)
	{
		this.locationData = locationData;
	}
	
	@JsonProperty("itemData")
	public ItemData getItemData() {
		return itemData;
	}

	public void setItemData(ItemData itemData) {
		this.itemData = itemData;
	}

	/**
	 * @return the productDetail
	 */
	@JsonProperty("productDetail")
	public ProductDetail getProductDetail() {
		return productDetail;
	}

	/**
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(ProductDetail productDetail) {
		this.productDetail = productDetail;
	}

	/**
	 * @return the deviceDetail
	 */
	@JsonProperty("deviceDetail")
	public DeviceDetail getDeviceDetail() {
		return deviceDetail;
	}

	/**
	 * @param deviceDetail the deviceDetail to set
	 */
	public void setDeviceDetail(DeviceDetail deviceDetail) {
		this.deviceDetail = deviceDetail;
	}


	public SensorResponse setResponseStatus(String responseStatus) {
		super.setResponseStatus(responseStatus);
		return this;
	}

	/**
	 * @param responseMessage the responseMessage to set
	 * @return 
	 */
	public Response setResponseMessage(String responseMessage) {
		super.setResponseMessage(responseMessage);
		return this;
	}

	/***
	 * 
	 * @return
	 */
	public ArrayList<DeviceLog> getDeviceLogList() {
		return deviceLogList;
	}

	/**
	 * 
	 * @param deviceLogList
	 */
	public void setDeviceLogList(ArrayList<DeviceLog> deviceLogList) {
		this.deviceLogList = deviceLogList;
	}

	/**
	 * @return the deviceKeyMap
	 */
	public HashMap<Object, Object> getDeviceKeyMap() {
		return deviceKeyMap;
	}

	/**
	 * @param deviceKeyMap the deviceKeyMap to set
	 */
	public void setDeviceKeyMap(HashMap<Object, Object> deviceKeyMap) {
		this.deviceKeyMap = deviceKeyMap;
	}


	@JsonProperty("monitorAlerts")
	public MonitorAlerts getMonitorAlerts() {
		return monitorAlerts;
	}

	public void setMonitorAlerts(MonitorAlerts monitorAlerts) {
		this.monitorAlerts = monitorAlerts;
	}

	@Override
	public String toString() {
		return "SensorResponse ["
			+ (packageId != null ? "packageId=" + packageId + ", " : "")
			+ (productId != null ? "productId=" + productId + ", " : "")
			+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
			+ (sensor != null ? "sensor=" + sensor + ", " : "")
			+ (locationData != null ? "locationData=" + locationData + ", "	: "")
			+ (productDetail != null ? "productDetail=" + productDetail	+ ", " : "")
			+ (deviceDetail != null ? "deviceDetail=" + deviceDetail + ", "	: "")
			+ (getPackageId() != null ? "getPackageId()=" + getPackageId()	+ ", " : "")
			+ (getProductId() != null ? "getProductId()=" + getProductId()+ ", " : "")
			+ (getDeviceId() != null ? "getDeviceId()=" + getDeviceId()	+ ", " : "")
			+ (getSensor() != null ? "getSensor()=" + getSensor() + ", ": "")
			+ (getLocationData() != null ? "getLocationData()="	+ getLocationData() + ", " : "")
			+ (getProductDetail() != null ? "getProductDetail()="+ getProductDetail() + ", " : "")
			+ (getDeviceDetail() != null ? "getDeviceDetail()="	+ getDeviceDetail() : "") 
			+ (deviceLogList != null ? "deviceLogList="+ deviceLogList + ", " : "]"); 
	}

}
