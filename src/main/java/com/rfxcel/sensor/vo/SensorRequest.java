package com.rfxcel.sensor.vo;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;

import com.rfxcel.sensor.beans.ConfigData;
import com.rfxcel.sensor.beans.Device;
import com.rfxcel.sensor.beans.DeviceData;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.DeviceLog;
import com.rfxcel.sensor.beans.LocationRequest;
import com.rfxcel.sensor.beans.ProductDetail;
import com.rfxcel.sensor.beans.Threshold;

/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 * 
 */
public class SensorRequest extends Request {
	
	
	private String dateTime;
	private String authToken;
	private String searchType;
	private String searchString;
	private String user;
	private String action;
	private boolean showMap;
	private DeviceData deviceData;
	private Device device;
	private LocationRequest locationRequest;
	private ConfigData configData;
	private List<DeviceDetail> itemListDetails;
	private ArrayList<Threshold> listThreshHold;
	private ArrayList<String> deviceList;
	private ProductDetail productDetail;
	private DeviceDetail deviceDetail;
	private DeviceLog deviceLog;
	private Boolean rtsDataFetch;
	
	public Boolean getRtsDataFetch() {
		return rtsDataFetch;
	}

	public void setRtsDataFetch(Boolean rtsDataFetch) {
		this.rtsDataFetch = rtsDataFetch;
	}

	public SensorRequest() {
		
	}
	
	/**
	 * @return createDate
	 */
	@JsonProperty("dateTime")
	public String getDateTime() {
		return dateTime;
	}
	/**
	 * @param createDate the createDate to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return the authToken
	 */
	@JsonProperty("authToken")
	public String getAuthToken() {
		return authToken;
	}

	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	/**
	 * @return the searchType
	 */
	@JsonProperty("searchType")
	public String getSearchType() {
		return searchType;
	}
	/**
	 * @param searchType the searchType to set
	 */
	public void setSearchType(String searchType) {
		this.searchType = searchType;
	}
	
	/**
	 * @return the searchString
	 */
	@JsonProperty("searchString")
	public String getSearchString() {
		return searchString;
	}

	/**
	 * @param searchString the searchString to set
	 */
	public void setSearchString(String searchString) {
		this.searchString = searchString;
	}

	/**
	 * @return the showMap
	 */
	@JsonProperty("showMap")
	public boolean isShowMap() {
		return showMap;
	}
	/**
	 * @param showMap the showMap to set
	 */
	public void setShowMap(boolean showMap) {
		this.showMap = showMap;
	}

	/**
	 * @return the deviceData
	 */
	@JsonProperty("deviceData")
	public DeviceData getDeviceData() {
		return deviceData;
	}

	/**
	 * @param deviceData the deviceData to set
	 */
	public void setDeviceData(DeviceData deviceData) {
		this.deviceData = deviceData;
	}
	
	/**
	 * @return the device
	 */
	@JsonProperty("device")
	public Device getDevice() {
		return device;
	}

	/**
	 * @param device the device to set
	 */
	public void setDevice(Device device) {
		this.device = device;
	}
	
	/**
	 * 
	 * @return
	 */
	@JsonProperty("locationData")
	public LocationRequest getLocationRequest() {
		return locationRequest;
	}

	/**
	 * 
	 * @param locationRequest
	 */
	public void setLocationRequest(LocationRequest locationRequest) {
		this.locationRequest = locationRequest;
	}

	/**
	 * @return the itemListDetails
	 */
	@JsonProperty("itemListDetails")
	public List<DeviceDetail> getItemListDetails() {
		return itemListDetails;
	}

	/**
	 * @param itemListDetails the itemListDetails to set
	 */
	public void setItemListDetails(List<DeviceDetail> itemListDetails) {
		this.itemListDetails = itemListDetails;
	}

	public ArrayList<String> getDeviceList() {
		return deviceList;
	}

	public void setDeviceList(ArrayList<String> deviceList) {
		this.deviceList = deviceList;
	}

	public ArrayList<Threshold> getListThreshHold() {
		return listThreshHold;
	}

	public void setListThreshHold(ArrayList<Threshold> listThreshHold) {
		this.listThreshHold = listThreshHold;
	}

	/**
	 * @return the productDetail
	 */
	@JsonProperty("productDetail")
	public ProductDetail getProductDetail() {
		return productDetail;
	}

	/**
	 * @param productDetail the productDetail to set
	 */
	public void setProductDetail(ProductDetail productDetail) {
		this.productDetail = productDetail;
	}
	/**
	 * 
	 * @return config data parameter names and its values
	 */
	@JsonProperty("configData")
	public ConfigData getConfigData() {
		return configData;
	}
	/**
	 * 
	 * @param configData sets list of config param names and its values
	 */
	public void setConfigData(ConfigData configData) {
		this.configData = configData;
	}

	/**
	 * @return the user name of the user who made this request
	 */
	@JsonProperty("user")
	public String getUser() {
		return user;
	}

	/**
	 * 
	 * @param user sets user name of the user
	 */
	public void setUser(String user) {
		this.user = user;
	}

	/**
	 * @return the productDetail
	 */
	@JsonProperty("action")
	public String getAction() {
		return action;
	}

	/**
	 * 
	 * @param action sets the action performed at present by the user
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the deviceDetail
	 */
	public DeviceDetail getDeviceDetail() {
		return deviceDetail;
	}

	/**
	 * @param deviceDetail the deviceDetail to set
	 */
	public void setDeviceDetail(DeviceDetail deviceDetail) {
		this.deviceDetail = deviceDetail;
	}

	/**
	 * 
	 * @return
	 */
	public DeviceLog getDeviceLog() {
		return deviceLog;
	}

	/**
	 * 
	 * @param deviceLog
	 */
	public void setDeviceLog(DeviceLog deviceLog) {
		this.deviceLog = deviceLog;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SensorRequest ["
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (authToken != null ? "authToken=" + authToken + ", " : "")
				+ (searchType != null ? "searchType=" + searchType + ", " : "")
				+ (user != null ? "user=" + user + ", " : "")
				+ (action != null ? "action=" + action + ", " : "")
				+ "showMap="+ showMap+ ", "
				+ (deviceData != null ? "deviceData=" + deviceData + ", " : "")
				+ (locationRequest != null ? "locationRequest="	+ locationRequest + ", " : "")
				+ (configData != null ? "configData=" + configData + ", " : "")
				+ (itemListDetails != null ? "itemListDetails="	+ itemListDetails + ", " : "")
				+ (listThreshHold != null ? "listThreshHold=" + listThreshHold+ ", " : "")
				+ (deviceList != null ? "deviceList=" + deviceList + ", " : "")
				+ (productDetail != null ? "productDetail=" + productDetail	: "") 
				+ (deviceDetail != null ? "deviceDetail=" + deviceDetail	: "")
				+ (groupId != null ? "groupId=" + groupId + ", " : "")
				+ (orgId != null ? "orgId=" + orgId + ", " : "")
				+ (deviceLog != null ? "deviceLog=" + deviceLog + ", " : "")
				+ "]";
	}

}
