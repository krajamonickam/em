package com.rfxcel.sensor.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.sensor.util.SensorConstant;


/**
 * 
 * @author Tejshree Kachare
 *
 */
@JsonSerialize(include=Inclusion.NON_NULL)
public class Response {
	
	public Response() {
	}

	private String dateTime;
	private String responseStatus;
	private String responseMessage;
	private Integer responseCode;
	
	/**
	 * @return the dateTime
	 */
	public String getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}

	/**
	 * @return the responseStatus
	 */
	public String getResponseStatus() {
		return responseStatus;
	}

	/**
	 * @param responseStatus the responseStatus to set
	 * @return 
	 */
	public Response setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
		return this;
	}

	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage the responseMessage to set
	 * @return 
	 */
	public Response setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
		return this;
	}

	/**
	 * @return the responseCode
	 */
	public Integer getResponseCode() {
		return responseCode;
	}

	/**
	 * @param responseCode the responseCode to set
	 */
	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	/**
	 * 
	 * @return
	 */
	public static Response createAuthFailureResponse(){
		SensorResponse response = new SensorResponse();
		response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
		response.setResponseMessage("Token authentication failed. Either token has expired or is invalid") ;
		response.setResponseCode(SensorConstant.RESPONSECODE_AUTHENTICATION_FAILED);
		return response;
	}
	
	@Override
	public String toString() {
		return "Response ["
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (responseStatus != null ? "responseStatus=" + responseStatus+ ", " : "")
				+ (responseMessage != null ? "responseMessage="+ responseMessage + ", " : "")
				+ (responseCode != null ? "responseCode=" + responseCode : "")
				+ "]";
	}
}
