package com.rfxcel.sensor.vo;

import java.util.HashMap;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.sensor.beans.DeviceInfo;

/**
 * 
 * @author Tejshree Kachare
 *
 */
@JsonSerialize(include=Inclusion.NON_NULL)
public class BactGrowthResponse {

	private String responseStatus;
	private String responseMessage;
	private Integer responseCode;
	private DeviceInfo deviceInfo;
	private HashMap<String, String> dataMap;
	
	public String getResponseStatus() {
		return responseStatus;
	}

	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}

	public String getResponseMessage() {
		return responseMessage;
	}

	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	public Integer getResponseCode() {
		return responseCode;
	}

	public void setResponseCode(Integer responseCode) {
		this.responseCode = responseCode;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}

	public HashMap<String, String> getDataMap() {
		return dataMap;
	}

	public void setDataMap(HashMap<String, String> dataMap) {
		this.dataMap = dataMap;
	}

	@Override
	public String toString() {
		return "Response [responseStatus=" + responseStatus + ", responseMessage=" + responseMessage + ", responseCode="
				+ responseCode + ", deviceInfo=" + deviceInfo + ", dataMap=" + dataMap + "]";
	}
}
