package com.rfxcel.sensor.vo;

import java.util.Map;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.util.Attribute;

/**
 *                
 * @author Tejshree Kachare
 *
 */
@JsonSerialize(include=Inclusion.NON_NULL)
public class BactGrowthRequest {

	private Map<String, String> dataMap;
	private Attribute attribute;
	private DeviceInfo deviceInfo;

	public Map<String, String> getDataMap() {
		return dataMap;
	}

	public void setDataMap(Map<String, String> datamap) {
		this.dataMap = datamap;
	}

	public Attribute getAttribute() {
		return attribute;
	}

	public void setAttribute(Attribute attribute) {
		this.attribute = attribute;
	}

	public DeviceInfo getDeviceInfo() {
		return deviceInfo;
	}

	public void setDeviceInfo(DeviceInfo deviceInfo) {
		this.deviceInfo = deviceInfo;
	}
	
	
}
