package com.rfxcel.sensor.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.sensor.beans.Profile;
/**
 * 
 * @author Vijay kumar
 * @since 2017-Apr-12
 * 
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ProfileResponse extends Response
{

	private List<Profile> profileList;
	private Profile profile;
	/**
	 * default constructor
	 */
	public ProfileResponse() {

	}

	public ProfileResponse(String dateTime) {
		super.setDateTime( dateTime);
	}
	
	/**
	 * @return the profileList
	 */
	public List<Profile> getProfileList() {
		return profileList;
	}

	/**
	 * @param profileList the profileList to set
	 */
	public void setProfileList(List<Profile> profileList) {
		this.profileList = profileList;
	}

	public Profile getProfile() {
		return profile;
	}

	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	
	
	
}
