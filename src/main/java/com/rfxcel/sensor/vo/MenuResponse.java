package com.rfxcel.sensor.vo;

import java.util.ArrayList;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.sensor.beans.Menu;

/**
 * 
 * @author sachin_kohale
 * @since May 15, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class MenuResponse extends Response {

	private ArrayList<Menu> menuList;
	
	/**
	 * Default constructor
	 */
	public MenuResponse(){
		
	} 
	
	public MenuResponse(String dateTime){
		super.setDateTime(dateTime);
	}

	/**
	 * 
	 * @return
	 */
	public ArrayList<Menu> getMenuList() {
		return menuList;
	}

	/**
	 * 
	 * @param menuList
	 */
	public void setMenuList(ArrayList<Menu> menuList) {
		this.menuList = menuList;
	}
	
}
