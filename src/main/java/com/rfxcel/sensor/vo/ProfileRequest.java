package com.rfxcel.sensor.vo;

import com.rfxcel.sensor.beans.Profile;

/**
 * 
 * @author Vijay kumar
 * @since 2017-Apr-12
 * 
 */
public class ProfileRequest extends Request {
	
	private Profile profile;
	private String user;
	private String deviceTypeId;
	private String searchBy
	
	;
	public Profile getProfile() {
		return profile;
	}
	public void setProfile(Profile profile) {
		this.profile = profile;
	}
	public String getUser() {
		return user;
	}
	public void setUser(String user) {
		this.user = user;
	}
	public String getDeviceTypeId() {
		return deviceTypeId;
	}
	public void setDeviceTypeId(String deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	public String getSearchBy() {
		return searchBy;
	}
	public void setSearchBy(String searchBy) {
		this.searchBy = searchBy;
	}
	
}
