package com.rfxcel.sensor.kirsen.message;

import java.io.IOException;
import java.util.Map;

import org.codehaus.jackson.JsonGenerationException;
import org.codehaus.jackson.map.JsonMappingException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.sensor.kirsen.util.KirsenConstants;
import com.rfxcel.sensor.util.Sensor;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;

public class KirsenSensor extends Sensor {

	public KirsenSensor(){
		
	}
	/**
	 * since each sensor data has different attribute names, mapping those  in constructor to a standard value
	 * @param kirsenData
	 */
	public KirsenSensor(Map<String, String> kirsenData) {
		this.deviceType = SensorConstant.SENSOR_KIRSEN;
		this.deviceIdentifier = kirsenData.get(KirsenConstants.ATTRIBUTE_IMEI);
		this.latitude = kirsenData.get(KirsenConstants.ATTRIBUTE_LATITUDE) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_LATITUDE);
		this.longitude = kirsenData.get(KirsenConstants.ATTRIBUTE_LONGITUDE) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_LONGITUDE);
		this.hepe = kirsenData.get(KirsenConstants.ATTRIBUTE_COORD_ACCURACY) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_COORD_ACCURACY);
		this.fixType =  kirsenData.get(KirsenConstants.ATTRIBUTE_COORD) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_COORD);
		this.statusTimeStamp = Utility.convertEPOCToDate(kirsenData.get(KirsenConstants.ATTRIBUTE_DATETIME));
		this.temperature =( kirsenData.get(KirsenConstants.ATTRIBUTE_TEMPERATURE) == "") ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_TEMPERATURE);
		this.battery = kirsenData.get(KirsenConstants.ATTRIBUTE_BATTERY) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_BATTERY);
		this.tilt = kirsenData.get(KirsenConstants.ATTRIBUTE_TILT) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_TILT);
		this.shock = kirsenData.get(KirsenConstants.ATTRIBUTE_SHOCK) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_SHOCK);
		this.vibration = kirsenData.get(KirsenConstants.ATTRIBUTE_VIBRATION) == "" ? null : kirsenData.get(KirsenConstants.ATTRIBUTE_VIBRATION);
		//TODO these values are not assigned as these would be present in json conversion
		/*this.id = kirsenData.get(KirsenConstants.ATTRIBUTE_ID);
		this.type = kirsenData.get(KirsenConstants.ATTRIBUTE_TYPE);
		this.description = kirsenData.get(KirsenConstants.ATTRIBUTE_DESCRIPTION);*/
	}

	private String type;
	private String id;
	private String description;
	
	/**
	 * @return the type
	 */
	public String getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(String type) {
		this.type = type;
	}
	
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}
	
	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	
	
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		} catch (JsonGenerationException e) {
			e.printStackTrace();
		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} 
		return jsonInString;

	}
	
	/**
	 * 
	 * @param json
	 * @return
	 */
	public static KirsenSensor fromJson(String json) {
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			KirsenSensor sensorData = mapper.readValue(json, KirsenSensor.class);
			return(sensorData);
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null; 
	}
}
