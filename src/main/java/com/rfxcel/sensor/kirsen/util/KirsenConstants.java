package com.rfxcel.sensor.kirsen.util;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class KirsenConstants {

	public static final String ATTRIBUTE_IMEI = "imei" ;	
	public static final String ATTRIBUTE_LATITUDE = "latitude";	
	public static final String ATTRIBUTE_LONGITUDE = "longitude" ;
	public static final String ATTRIBUTE_COORD_ACCURACY = "coordinateaccuracy";
	public static final String ATTRIBUTE_COORD = "coord" ;	
	public static final String ATTRIBUTE_DATETIME =  "datetime";	
	public static final String ATTRIBUTE_TEMPERATURE = "temperature" ;	
	public static final String ATTRIBUTE_BATTERY ="battery" ;	
	public static final String ATTRIBUTE_TILT ="tilt" ;
	public static final String ATTRIBUTE_SHOCK = "shock";
	public static final String ATTRIBUTE_VIBRATION = "vibration";
	public static final String ATTRIBUTE_TYPE = "type" ;	
	public static final String ATTRIBUTE_ID ="id" ;	
	public static final String ATTRIBUTE_DESCRIPTION = "description";	
	
	public static final String LOG_FILE_LOC ="kirsen.logFolder";
}
