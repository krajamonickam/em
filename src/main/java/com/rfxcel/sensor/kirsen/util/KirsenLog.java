package com.rfxcel.sensor.kirsen.util;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

import com.rfxcel.cache.ConfigCache;

public class KirsenLog {

	private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static ConfigCache cache = ConfigCache.getInstance();
	private static final int maxsize = cache.getSystemConfig("kirsen.msg.log.size") == null? 100: Integer.valueOf(cache.getSystemConfig("kirsen.msg.log.size"));
	public static KirsenLog instance = null;
	private HashMap<String,LinkedList<String>> deviceMsgLog = new HashMap<String,LinkedList<String>>();

	
	public static KirsenLog getInstance(){
		synchronized (KirsenLog.class) {
			if (instance == null) {
				instance = new KirsenLog();
			}
		}
		return(instance);
	}

	/**
	 * 
	 * @param deviceId
	 * @param payload
	 */
	public synchronized void add(String deviceId, String payload){
		String time = sdf.format(new Date());
		if(deviceMsgLog.containsKey(deviceId)){
			LinkedList<String> values = deviceMsgLog.get(deviceId);
			values.add(0, time + " " + payload);
			while (values.size() > maxsize) {
				values.remove();
			}
		}else{
			LinkedList<String> values = new LinkedList<String>();
			values.add(0, time + " " + payload);
			deviceMsgLog.put(deviceId, values);
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param count
	 * @return
	 */
	public synchronized List<String> getString(String deviceId, Integer count){
		List<String> messages;
		if(deviceMsgLog.containsKey(deviceId)){
			LinkedList<String> values = deviceMsgLog.get(deviceId);
			if(count != null && count <= values.size()){
				messages = values.subList(0, count);
			}else{
				messages = values;
			}
		}else{
			messages =  null;
		}
		return messages;
	}
	
	/**
	 * 
	 */
	public synchronized void clear() {
		deviceMsgLog.clear();
	}

}

