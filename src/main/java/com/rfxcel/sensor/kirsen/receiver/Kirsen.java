package com.rfxcel.sensor.kirsen.receiver;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.codehaus.jackson.type.TypeReference;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.JsonParser;

import com.rfxcel.sensor.kirsen.message.KirsenSensor;
import com.rfxcel.sensor.kirsen.util.KirsenConstants;
import com.rfxcel.sensor.kirsen.util.KirsenLog;
import com.rfxcel.sensor.service.SensorProcessJSON;
import com.rfxcel.sensor.util.RfxcelException;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.cache.ConfigCache;

@WebServlet("/kirsen")
public class Kirsen extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = Logger.getLogger(Kirsen.class);
	private static SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
	private SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
	private KirsenLog kirsenLog = KirsenLog.getInstance();
	private static ConfigCache configCache = ConfigCache.getInstance();

	public Kirsen() {}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp)throws ServletException, IOException {
		resp.setContentType("text/html");
		OutputStreamWriter ow = new OutputStreamWriter(resp.getOutputStream());
		PrintWriter pw = new PrintWriter(ow);
		Object refresh = req.getParameter("refresh");
		Integer refreshFreq = refresh== null ? 10 : Integer.valueOf(refresh.toString());
		pw.println("<html><head><meta http-equiv=\"refresh\" content=\""+refreshFreq+"\"></head><body>");
		pw.println("<br/>Kirsen Receiver is up and Running!<br/>");
		if(configCache.getSystemConfig("kirsen.msg.log.size") != null){
			Object deviceId = req.getParameter("deviceId");
			if (deviceId == null) {
				deviceId = "NULL";
			}
			pw.println("<br/>deviceId=" + req.getParameter("deviceId") + "<br/>");
		}
		Object reset = req.getParameter("reset");
		if(reset != null){
			if ("true".equalsIgnoreCase(reset.toString())) {//If reset is true clear the log
				kirsenLog.clear();
			}
		}
		Object deviceId = req.getParameter("deviceId");
		Object count = req.getParameter("count");
		if(deviceId != null){
			Integer msgCount = count != null ? Integer.valueOf(count.toString()) : null;
			List<String> lines = kirsenLog.getString(deviceId.toString(),msgCount);
			if (lines != null) {
				pw.println("<br/>Message(s) received are :<br/> ");
				for (String line : lines) {
					pw.println(line);
					pw.println("<br/>");
				}
			}
		}
		pw.println("******************************<br/>");
		pw.println("</body></html>");
		pw.flush();
		pw.close();
	}

	@Override
	/**
	 * this is the servlet that accepts the https post request from the calling service.
	 * performs the basic authentication
	 * accepts the json and calls the callProcessMessage method for further processing.
	 */
	
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException {
		logger.debug("Kirsen Sensor message received");
		response.setContentType("text/html");
		final String authorization = request.getHeader("Authorization");
		String requestType = request.getMethod();
		if (authorization != null && authorization.startsWith("Basic") && "POST".equals(requestType)) {
			String base64Credentials = authorization.substring("Basic".length()).trim();
			String credentials[] = Utility.decrypytUserNamePwd(base64Credentials);
			InputStream inputStream = request.getInputStream();
			StringBuilder stringBuilder = new StringBuilder();
			BufferedReader bufferedReader = null;
			String payload = null;
			try {
				if (inputStream != null) {
					bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
					char[] charBuffer = new char[128];
					int bytesRead = -1;
					while ((bytesRead = bufferedReader.read(charBuffer)) > 0) {
						stringBuilder.append(charBuffer, 0, bytesRead);
					}
				} else {
					stringBuilder.append("");
				}
			} catch (IOException ex) {
				logger.error(ex.getMessage());
			} finally {
				if (bufferedReader != null) {
					try {
						bufferedReader.close();
					} catch (IOException ex) {
						logger.error(ex.getMessage());
					}
				}
			}
			payload = stringBuilder.toString();
			if (!(payload.equals(""))) {
				ArrayList <KirsenSensor> kirsenRecords =  processPayload(payload);
				for( KirsenSensor sensor : kirsenRecords){
					String kirsenData = sensor.toJson();
					SensorProcessJSON.getInstance().doQueue(kirsenData, false, credentials[0].trim(), credentials[1].trim());
				}
				if(kirsenRecords.size()>0){
					if(configCache.getSystemConfig("kirsen.msg.log.size") != null){
						kirsenLog.add(kirsenRecords.get(0).getDeviceIdentifier(),payload);
					}
					//Print the details
					try{
						String fileName = createFile(kirsenRecords.get(0).getDeviceIdentifier());
						PrintWriter pw = new PrintWriter(new FileWriter(fileName, true));
						pw.println(timeFormat.format(new Date()) + " " + payload.replace('\n', ' ').replace('\r', ' ').replace('\t', ' '));
						pw.flush();
						pw.close();
					}catch (Exception e){							
						logger.error("Error logging data to log file "+ e.getMessage());
						logger.error("Message is : "+ payload);
					}
				}
			} else {
				logger.error("Kirsen Payload is empty !!");
			}
		}else{
			logger.error("Kirsen authentication missing, ignoring message");
			response.setContentType("application/json");
			response.setStatus(401);
			String responseMsg =bindErrorMessage(401,"Unauthorized user");
			response.getWriter().write(responseMsg);
		}
	}
	/**
	 * 
	 * @param payload
	 * @return
	 */
	private ArrayList<KirsenSensor> processPayload(String payload) {
		JsonNode rootNode;
		ArrayList<KirsenSensor> sensorData = new ArrayList<KirsenSensor>();
		try {
			// To make message processing consistent for all the types of messages coming in  1) history 2) temperature history 3)coordinate history
			
			payload = payload.replaceAll("\"lat\"", "\"latitude\"");
			payload = payload.replaceAll("\"lon\"", "\"longitude\"");
			payload = payload.replaceAll("n/a", "");	//Setting values with "n/a" to empty
			ObjectMapper m = new ObjectMapper();
			m.configure(JsonParser.Feature.ALLOW_UNQUOTED_FIELD_NAMES, true);
			rootNode = m.readTree(payload);
			KirsenSensor sensor;
			if(rootNode.has("data")){		//If data node is not null then, message contains array of datapoints
				JsonNode imeiNode = rootNode.path("imei");
				String deviceId = imeiNode.getTextValue();
				JsonNode dataNode = rootNode.path("data");
				if (dataNode.isArray()) {
					ObjectMapper mapper = new ObjectMapper();
					mapper.setSerializationInclusion(Inclusion.NON_EMPTY);
					//mapper.configure(DeserializationConfig.Feature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT, true);
					for (JsonNode item : dataNode) {
						Map<String, String> kirsenDataMap = mapper.convertValue(item, new TypeReference<Map<String, String>>() {});
						kirsenDataMap.put("imei", deviceId);
						sensor = new KirsenSensor(kirsenDataMap);
						sensorData.add(sensor);
					}
				}
			}else{//If data node is null, then it contains single message
				Map<String, String> kirsenDataMap = Utility.convertJsonToMap(payload);
				sensor = new KirsenSensor(kirsenDataMap);
				sensorData.add(sensor);
			}
		} catch (IOException e) {
			logger.error("Error processing input payload" +payload);
			e.printStackTrace();
		}
		return sensorData;
	}

	public String bindErrorMessage(int errorCode, String message) {
		ObjectMapper mapper = new ObjectMapper();
		RfxcelException rfxcelException = new RfxcelException();
		rfxcelException.setStatusCode(errorCode);
		rfxcelException.setMessage(message);
		
		String jsonString;
		try {
			jsonString = mapper.writeValueAsString(rfxcelException);
		} catch (Exception e) {
			e.printStackTrace();
			return null;
		}
		return jsonString;
	}


	/**
	 * 
	 * @param deviceid
	 * @return
	 */
	public static String createFile(String deviceid)
	{
//		String directory = getGeofenceprop().get(KirsenConstants.LOG_FILE_LOC) != null ? getGeofenceprop().get(KirsenConstants.LOG_FILE_LOC).toString() : null ;
		String directory = configCache.getSystemConfig(KirsenConstants.LOG_FILE_LOC);
		if(directory == null){
			return null;
		}
		File dir = new File(directory);
		if (!dir.exists()) {
			if (!dir.mkdirs()) {
				logger.info("Directory " + directory + " does not exist!");
				return null;
			}
		}
		if (!dir.isDirectory()) {
			logger.info("Directory " + directory + " not a directory!");
			return null;
		}
		File subDir = new File(directory, deviceid);
		if (!subDir.exists()) {
			if (!subDir.mkdirs()) {
				logger.info("Directory " + subDir.getAbsolutePath() + " does not exist!");
				return null;
			}
		}
		if (!subDir.isDirectory()) {
			logger.info("Directory " + subDir.getAbsolutePath() + " not a directory!");
			return null;
		}
		String suffix = dateFormat.format(new Date());
		File file = new File(subDir, deviceid + "-" + suffix + ".log");
		return(file.getAbsolutePath());
	}

}
