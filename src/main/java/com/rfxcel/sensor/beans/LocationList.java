package com.rfxcel.sensor.beans;
public class LocationList
{
    private String locationId;

    private String locationName;

    public String getLocationId ()
    {
        return locationId;
    }

    public void setLocationId (String locationId)
    {
        this.locationId = locationId;
    }

    public String getLocationName ()
    {
        return locationName;
    }

    public void setLocationName (String locationName)
    {
        this.locationName = locationName;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [locationId = "+locationId+", locationName = "+locationName+"]";
    }
}