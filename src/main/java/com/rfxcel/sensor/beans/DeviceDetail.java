package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DeviceDetail {
	
	private String deviceId;
	private String deviceKey;
	private Integer deviceTypeId;
	private String deviceType;
	private String deviceName;
	private String parentItemId;
	private ArrayList<Attribute> attributes;
	private String manufacturer;
	private Boolean active;
	private Integer orgId;
	private Integer gpsComFreqLimit;
	private Integer receiveComFreqLimit;
	private Integer sendComFreqLimit;
	private String mdn;	 //mdn - mobile device number
	private Integer activationState;
	
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	@JsonProperty("deviceTypeId")
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}
	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	@JsonProperty("deviceType")
	public String getDeviceType() {
		return deviceType;
	}
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	@JsonProperty("deviceName")
	public String getDeviceName() {
		return deviceName;
	}
	public void setDeviceName(String deviceName) {
		this.deviceName = deviceName;
	}
	@JsonProperty("parentItemId")
	public String getParentItemId() {
		return parentItemId;
	}
	public void setParentItemId(String parentItemId) {
		this.parentItemId = parentItemId;
	}
	@JsonProperty("deviceKey")
	public String getDeviceKey() {
		return deviceKey;
	}
	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
	/**
	 * @return the attributes
	 */
	public ArrayList<Attribute> getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<Attribute> attributes) {
		this.attributes = attributes;
	}
	@JsonProperty("manufacturer")
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	/**
	 * 
	 * @return
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * 
	 * @param active
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getOrgId() {
		return orgId;
	}
	
	/**
	 * 
	 * @param orgId
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	/**
	 * 
	 * @return
	 */
	public Integer getGpsComFreqLimit() {
		return gpsComFreqLimit;
	}
	/**
	 * 
	 * @param gpsComFreqLimit
	 */
	public void setGpsComFreqLimit(Integer gpsComFreqLimit) {
		this.gpsComFreqLimit = gpsComFreqLimit;
	}
	/**
	 * 
	 * @return
	 */
	public Integer getReceiveComFreqLimit() {
		return receiveComFreqLimit;
	}
	/**
	 * 
	 * @param receiveComFreqLimit
	 */
	public void setReceiveComFreqLimit(Integer receiveComFreqLimit) {
		this.receiveComFreqLimit = receiveComFreqLimit;
	}
	/**
	 * 
	 * @return
	 */
	public Integer getSendComFreqLimit() {
		return sendComFreqLimit;
	}
	/**
	 * 
	 * @param sendComFreqLimit
	 */
	public void setSendComFreqLimit(Integer sendComFreqLimit) {
		this.sendComFreqLimit = sendComFreqLimit;
	}
	/**
	 * 
	 * @return the mdn
	 */
	public String getMdn() {
		return mdn;
	}
	/**
	 * 
	 * @param mdn the mdn to set
	 */
	public void setMdn(String mdn) {
		this.mdn = mdn;
	}
	
	public Integer getActivationState() {
		return activationState;
	}
	public void setActivationState(Integer activationState) {
		this.activationState = activationState;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeviceDetail [" + (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (deviceKey != null ? "deviceKey=" + deviceKey + ", " : "")
				+ (deviceTypeId != null ? "deviceTypeId=" + deviceTypeId + ", " : "")
				+ (deviceType != null ? "deviceType=" + deviceType + ", " : "")
				+ (deviceName != null ? "deviceName=" + deviceName + ", " : "")
				+ (parentItemId != null ? "parentItemId=" + parentItemId + ", " : "")
				+ (attributes != null ? "attributes=" + attributes + ", " : "")
				+ (manufacturer != null ? "manufacturer=" + manufacturer + ", " : "")
				+ (active != null ? "active=" + active + ", " : "") + (orgId != null ? "orgId=" + orgId + ", " : "")
				+ (gpsComFreqLimit != null ? "gpsComFreqLimit=" + gpsComFreqLimit + ", " : "")
				+ (receiveComFreqLimit != null ? "receiveComFreqLimit=" + receiveComFreqLimit + ", " : "")
				+ (sendComFreqLimit != null ? "sendComFreqLimit=" + sendComFreqLimit + ", " : "")
				+ (mdn != null ? "mdn=" + mdn + ", " : "")
				+ (activationState != null ? "activationState=" + activationState : "") + "]";
	}
	
	/**
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
	
	
}
