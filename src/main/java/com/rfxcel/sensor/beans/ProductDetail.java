package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


/**
 * @author Tejshree Kachare
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ProductDetail {
	
	private String packageId;
	private String productId;
	private String deviceType;
	private String deviceId;
	private Integer profileId;
	private ShipmentLocation shipFrom;
	private ShipmentLocation shipTo;
	private ArrayList<Product> productList;
	private Boolean packageIsActive;
	private Boolean deviceIsActive;
	private String rtsItemId;
	private String rtsSerialNumber;
	/**
	 * @return the packageId
	 */
	public String getPackageId() {
		return packageId;
	}

	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}

	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}

	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	/**
	 * @return the profileId
	 */
	public Integer getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	/**
	 * @return the shipFrom
	 */
	public ShipmentLocation getShipFrom() {
		return shipFrom;
	}
	
	/**
	 * @param shipFrom the shipFrom to set
	 */
	public void setShipFrom(ShipmentLocation shipFrom) {
		this.shipFrom = shipFrom;
	}
	
	/**
	 * @return the shipTo
	 */
	public ShipmentLocation getShipTo() {
		return shipTo;
	}
	
	/**
	 * @param shipTo the shipTo to set
	 */
	public void setShipTo(ShipmentLocation shipTo) {
		this.shipTo = shipTo;
	}

	/**
	 * @return the productList
	 */
	public ArrayList<Product> getProductList() {
		return productList;
	}

	/**
	 * @param productList the productList to set
	 */
	public void setProductList(ArrayList<Product> productList) {
		this.productList = productList;
	}

	/**
	 * @return the packageIsActive
	 */
	public Boolean getPackageIsActive() {
		return packageIsActive;
	}

	/**
	 * @param packageIsActive the packageIsActive to set
	 */
	public void setPackageIsActive(Boolean packageIsActive) {
		this.packageIsActive = packageIsActive;
	}

	/**
	 * 
	 * @return the deviceIsActive
	 */
	public Boolean getDeviceIsActive() {
		return deviceIsActive;
	}

	/**
	 * 
	 * @param deviceIsActive the deviceIsActive to set
	 */
	public void setDeviceIsActive(Boolean deviceIsActive) {
		this.deviceIsActive = deviceIsActive;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ProductDetail ["
				+ (packageId != null ? "packageId=" + packageId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (deviceType != null ? "deviceType=" + deviceType + ", " : "")
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (profileId != null ? "profileId=" + profileId + ", " : "")
				+ (shipFrom != null ? "shipFrom=" + shipFrom + ", " : "")
				+ (shipTo != null ? "shipTo=" + shipTo + ", " : "")
				+ (productList != null ? "productList=" + productList + ", ": "")
				+ (packageIsActive != null ? "packageIsActive=" + packageIsActive + ", ": "")
				+ (deviceIsActive != null ? "deviceIsActive="	+ deviceIsActive : "") + "]";
	}

	/**
	 * 
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;

	}

	public String getRtsItemId() {
		return rtsItemId;
	}

	public void setRtsItemId(String rtsItemId) {
		this.rtsItemId = rtsItemId;
	}

	public String getRtsSerialNumber() {
		return rtsSerialNumber;
	}

	public void setRtsSerialNumber(String rtsSerialNumber) {
		this.rtsSerialNumber = rtsSerialNumber;
	}
	
}
