package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;

public class Threshold
{
	private String ndc;
	private String attrName;
	private double min;
	private double max;
	private String sensor_type;
	
	@JsonProperty("attrName")
	public String getAttrName() {
		return attrName;
	}
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	@JsonProperty("min")
	public double getMin() {
		return min;
	}
	public void setMin(double min) {
		this.min = min;
	}
	@JsonProperty("max")
	public double getMax() {
		return max;
	}
	public void setMax(double max) {
		this.max = max;
	}
	@JsonProperty("deviceType")
	public String getNdc() {
		return ndc;
	}
	public void setNdc(String ndc) {
		this.ndc = ndc;
	}
	@JsonProperty("sensor_type")
	public String getSensor_type() {
		return sensor_type;
	}
	public void setSensor_type(String sensor_type) {
		this.sensor_type = sensor_type;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ThreshHoldBean [" + (ndc != null ? "ndc=" + ndc + ", " : "")
				+ (attrName != null ? "attrName=" + attrName + ", " : "")
				+ "min=" + min + ", max=" + max + ", "
				+ (sensor_type != null ? "sensor_type=" + sensor_type : "")
				+ "]";
	}
	
	
}
