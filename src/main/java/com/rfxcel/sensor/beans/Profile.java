package com.rfxcel.sensor.beans;

import java.util.ArrayList;
import java.util.List;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

import com.rfxcel.sensor.util.Attribute;


/**
 *@author Vijay kumar
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Profile {
	
	private Integer profileId;
	private String name;
	private String description;
	private Integer deviceTypeId;
	private Integer orgId;
	private Long groupId;
	private List<Attribute> attrType = null;
	private List<Geopoint> geopoints = null;
	private Integer commFrequency;
	private Integer commFrequencyUnit;
	private Integer gpsFrequency;
	private Integer gpsFrequencyUnit;
	private Integer attbFrequency;
	private Integer attbFrequencyUnit;
	private Boolean active;
	private Geopoint geopoint = null;
	private ArrayList<String> attributes;
	private ArrayList<Attribute> additionalAlerts;
	
	/**
	 * 
	 * @return profileId
	 */
	public Integer getProfileId() {
		return profileId;
	}
	/**
	 * 
	 * @param profileId the profileId to set
	 */
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}
	/**
	 * 
	 * @return name
	 */
	public String getName() {
		return name;
	}
	/**
	 * 
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}
	/**
	 * 
	 * @return description
	 */
	public String getDescription() {
		return description;
	}
	/**
	 * 
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}	
	/**
	 * 
	 * @return deviceTypeId
	 */
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}
	/**
	 * 
	 * @param deviceTypeId the deviceTypeId to set
	 */
	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * 
	 * @return orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}
	/**
	 * 
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	/**
	 * 
	 * @return groupId
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * 
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	/**
	 * 
	 * @return attrType
	 */
	public List<Attribute> getAttrType() {
		return attrType;
	}
	/**
	 * 
	 * @param attrType the attrType to set
	 */
	public void setAttrType(List<Attribute> attrType) {
		this.attrType = attrType;
	}
	/**
	 * 
	 * @return geopoints
	 */
	public List<Geopoint> getGeopoints() {
		return geopoints;
	}
	/**
	 * 
	 * @param geopoints the geopoints to set
	 */
	public void setGeopoints(List<Geopoint> geopoints) {
		this.geopoints = geopoints;
	}
	/**
	 * 
	 * @return commFrequency
	 */
	public Integer getCommFrequency() {
		return commFrequency;
	}
	/**
	 * 
	 * @param commFrequency the commFrequency to set
	 */
	public void setCommFrequency(Integer commFrequency) {
		this.commFrequency = commFrequency;
	}
	/**
	 * 
	 * @return commFrequencyUnit
	 */
	public Integer getCommFrequencyUnit() {
		return commFrequencyUnit;
	}
	/**
	 * 
	 * @param commFrequencyUnit the commFrequencyUnit to set
	 */
	public void setCommFrequencyUnit(Integer commFrequencyUnit) {
		this.commFrequencyUnit = commFrequencyUnit;
	}
	/**
	 * 
	 * @return gpsFrequency
	 */
	public Integer getGpsFrequency() {
		return gpsFrequency;
	}
	/**
	 * 
	 * @param gpsFrequency the gpsFrequency to set
	 */
	public void setGpsFrequency(Integer gpsFrequency) {
		this.gpsFrequency = gpsFrequency;
	}
	/**
	 * 
	 * @return gpsFrequencyUnit
	 */
	public Integer getGpsFrequencyUnit() {
		return gpsFrequencyUnit;
	}
	/**
	 * 
	 * @param gpsFrequencyUnit the gpsFrequencyUnit to set
	 */
	public void setGpsFrequencyUnit(Integer gpsFrequencyUnit) {
		this.gpsFrequencyUnit = gpsFrequencyUnit;
	}
	/**
	 * 
	 * @return attbFrequency
	 */
	public Integer getAttbFrequency() {
		return attbFrequency;
	}
	/**
	 * 
	 * @param attbFrequency the attbFrequency to set
	 */
	public void setAttbFrequency(Integer attbFrequency) {
		this.attbFrequency = attbFrequency;
	}
	/**
	 * 
	 * @return attbFrequencyUnit
	 */
	public Integer getAttbFrequencyUnit() {
		return attbFrequencyUnit;
	}
	/**
	 * 
	 * @param attbFrequencyUnit the attbFrequencyUnit to set
	 */
	public void setAttbFrequencyUnit(Integer attbFrequencyUnit) {
		this.attbFrequencyUnit = attbFrequencyUnit;
	}
	/**
	 * 
	 * @return active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * 
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	/**
	 * 
	 * @return geopoint
	 */
	public Geopoint getGeopoint() {
		return geopoint;
	}
	/**
	 * 
	 * @param geopoint the geopoint to set
	 */
	public void setGeopoint(Geopoint geopoint) {
		this.geopoint = geopoint;
	}
	/**
	 * @return the attributes
	 */
	public ArrayList<String> getAttributes() {
		return attributes;
	}
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<String> attributes) {
		this.attributes = attributes;
	}
	/**
	 * @return the geopointAlerts
	 */
	public ArrayList<Attribute> getAdditionalAlerts() {
		return additionalAlerts;
	}
	/**
	 * @param geopointAlerts the geopointAlerts to set
	 */
	public void setAdditionalAlerts(ArrayList<Attribute> additionalAlerts) {
		this.additionalAlerts = additionalAlerts;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Profile [" + (profileId != null ? "profileId=" + profileId + ", " : "")
				+ (name != null ? "name=" + name + ", " : "")
				+ (description != null ? "description=" + description + ", " : "")
				+ (deviceTypeId != null ? "deviceTypeId=" + deviceTypeId + ", " : "")
				+ (orgId != null ? "orgId=" + orgId + ", " : "") + (groupId != null ? "groupId=" + groupId + ", " : "")
				+ (attrType != null ? "attrType=" + attrType + ", " : "")
				+ (geopoints != null ? "geopoints=" + geopoints + ", " : "")
				+ (commFrequency != null ? "commFrequency=" + commFrequency + ", " : "")
				+ (commFrequencyUnit != null ? "commFrequencyUnit=" + commFrequencyUnit + ", " : "")
				+ (gpsFrequency != null ? "gpsFrequency=" + gpsFrequency + ", " : "")
				+ (gpsFrequencyUnit != null ? "gpsFrequencyUnit=" + gpsFrequencyUnit + ", " : "")
				+ (attbFrequency != null ? "attbFrequency=" + attbFrequency + ", " : "")
				+ (attbFrequencyUnit != null ? "attbFrequencyUnit=" + attbFrequencyUnit + ", " : "")
				+ (active != null ? "active=" + active + ", " : "") + (geopoint != null ? "geopoint=" + geopoint : "")
				+ "]";
	}
	
	/**
	 * 
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
}
