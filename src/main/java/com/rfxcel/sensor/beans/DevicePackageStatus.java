package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author Bosco
 * @since 2016-12-11
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DevicePackageStatus {
	private String deviceId;
	private String packageId;
	private String productId;
	private int statusId;

	private boolean nofityExcursion;
	private boolean derivedExcursion;
	private long dataRecordId;
	private ArrayList<String> attributes;
	/**
	 * 
	 * @return deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * 
	 * @param sets deviceId
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * 
	 * @return packageId
	 */
	public String getPackageId() {
		return packageId;
	}

	/**
	 * 
	 * @param sets packageId
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * 
	 * @return
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * 
	 * @param productId
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 
	 * @return lastStatusId
	 */
	public int getStatusId() {
		return statusId;
	}

	/**
	 * 
	 * @param lastStatusId
	 */
	public void setStatusId(int lastStatusId) {
		this.statusId = lastStatusId;
	}

	/**
	 * 
	 * @return nofityExcursion
	 */
	public boolean isNofityExcursion() {
		return nofityExcursion;
	}

	/**
	 * 
	 * @param nofityExcursion
	 */
	public void setNofityExcursion(boolean nofityExcursion) {
		this.nofityExcursion = nofityExcursion;
	}

	/**
	 * 
	 * @return derivedExcursion
	 */
	public boolean isDerivedExcursion() {
		return derivedExcursion;
	}

	/**
	 * 
	 * @param derivedExcursion
	 */
	public void setDerivedExcursion(boolean derivedExcursion) {
		this.derivedExcursion = derivedExcursion;
	}

	/**
	 * @return the dataRecordId
	 */
	public long getDataRecordId() {
		return dataRecordId;
	}

	/**
	 * @param dataRecordId the dataRecordId to set
	 */
	public void setDataRecordId(long dataRecordId) {
		this.dataRecordId = dataRecordId;
	}

	/**
	 * @return the attributes
	 */
	public ArrayList<String> getAttributes() {
		return attributes;
	}

	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<String> attributes) {
		this.attributes = attributes;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DevicePackageStatus ["
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (packageId != null ? "packageId=" + packageId + ", " : "")
				+ "lastStatusId=" + statusId + ", nofityExcursion="
				+ nofityExcursion + ", derivedExcursion=" + derivedExcursion
				+ ", dataRecordId=" + dataRecordId + ", "
				+ (attributes != null ? "attributes=" + attributes : "") + "]";
	}

	

}
