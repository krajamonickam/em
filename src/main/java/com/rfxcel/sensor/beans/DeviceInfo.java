package com.rfxcel.sensor.beans;

import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;
import org.joda.time.DateTime;
@JsonSerialize(include=Inclusion.NON_NULL)
public class DeviceInfo {

	private Long id;
	private String attributeName;
	private String attributeValue;
	private DateTime startTime;
	private DateTime endTime;
	private String deviceId;
	private int deviceLogType = 0;
	private int notifyCount = 0;
	private long start;
	private long end;

	public DeviceInfo() {
	}

	public DeviceInfo(Long id, String attributeName, String attributeValue,DateTime startTime, DateTime endTime, String deviceId,int notifyCount) {
		super();
		this.id = id;
		this.attributeName = attributeName;
		this.attributeValue = attributeValue;
		this.startTime = startTime;
		this.endTime = endTime;
		this.deviceId = deviceId;
		this.notifyCount = notifyCount;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getAttributeName() {
		return attributeName;
	}

	public void setAttributeName(String attributeName) {
		this.attributeName = attributeName;
	}

	public String getAttributeValue() {
		return attributeValue;
	}

	public void setAttributeValue(String attributeValue) {
		this.attributeValue = attributeValue;
	}

	public DateTime getStartTime() {
		return startTime;
	}

	public void setStartTime(DateTime startTime) {
		this.startTime = startTime;
	}

	public DateTime getEndTime() {
		return endTime;
	}

	public void setEndTime(DateTime endTime) {
		this.endTime = endTime;
	}

	public String getDeviceId() {
		return deviceId;
	}

	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	public int getDeviceLogType() {
		return deviceLogType;
	}

	public void setDeviceLogType(int deviceLogType) {
		this.deviceLogType = deviceLogType;
	}

	public int getNotifyCount() {
		return notifyCount;
	}

	public void setNotifyCount(int notifyCount) {
		this.notifyCount = notifyCount;
	}

	public long getStart() {
		return start;
	}

	public void setStart(long start) {
		this.start = start;
	}

	public long getEnd() {
		return end;
	}

	public void setEnd(long end) {
		this.end = end;
	}

	
}
