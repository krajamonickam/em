package com.rfxcel.sensor.beans;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class ShipmentData {

	private Double lat;
	private Double lang;
	private String deviceCode;
	private String deviceProfile;
	private String productId;
	private ShipDetails shipTo;
	private ShipDetails shipFrom;
	private String profileName;
	


	/**
	 * 
	 * @return deviceCode
	 */
	public String getDeviceCode() {
		return deviceCode;
	}
	/**
	 * 
	 * @param set deviceCode
	 */
	public void setDeviceCode(String deviceCode) {
		this.deviceCode = deviceCode;
	}
	/**
	 * 
	 * @return latitude value
	 */
	public Double getLat() {
		return lat;
	}
	/**
	 * 
	 * @param sets latitue value
	 */
	public void setLat(Double lat) {
		this.lat = lat;
	}
	/**
	 * 
	 * @return lang value
	 */
	public Double getLang() {
		return lang;
	}
	/**
	 * 
	 * @param the lang value to set 
	 */
	public void setLang(Double lang) {
		this.lang = lang;
	}
	/**
	 * @return the shipTo
	 */
	public ShipDetails getShipTo() {
		return shipTo;
	}
	/**
	 * @param shipTo the shipTo to set
	 */
	public void setShipTo(ShipDetails shipTo) {
		this.shipTo = shipTo;
	}
	/**
	 * @return the shipFrom
	 */
	public ShipDetails getShipFrom() {
		return shipFrom;
	}
	/**
	 * @param shipFrom the shipFrom to set
	 */
	public void setShipFrom(ShipDetails shipFrom) {
		this.shipFrom = shipFrom;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}
	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}
	/**
	 * @return the deviceProfile
	 */
	public String getDeviceProfile() {
		return deviceProfile;
	}
	/**
	 * @param deviceProfile the deviceProfile to set
	 */
	public void setDeviceProfile(String deviceProfile) {
		this.deviceProfile = deviceProfile;
	}
	/**
	 * 
	 * @return the profileName
	 */
	public String getProfileName() {
		return profileName;
	}
	/**
	 * 
	 * @param profileName the profileName to set
	 */
	public void setProfileName(String profileName) {
		this.profileName = profileName;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShipmentData ["
				+ (lat != null ? "lat=" + lat + ", " : "")
				+ (lang != null ? "lang=" + lang + ", " : "")
				+ (deviceCode != null ? "deviceCode=" + deviceCode + ", " : "")
				+ (deviceProfile != null ? "deviceProfile=" + deviceProfile	+ ", " : "")
				+ (profileName != null ? "profileName=" + profileName	+ ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (shipTo != null ? "shipTo=" + shipTo + ", " : "")
				+ (shipFrom != null ? "shipFrom=" + shipFrom : "") + "]";
	}
	
}
