package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author ashish_kumar1
 * @since 2016-11-17
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DeviceFields {
	private Boolean isAssociated;
	private Boolean isCommissioned;
	private String batteryStatus;
	private String onlineStatus;
	private String latitude;
	private String longitude;
	private Double temperature;
	/**
	 * @return the isAssociated
	 */
	@JsonProperty("isAssociated")
	public Boolean isAssociated() {
		return isAssociated;
	}
	/**
	 * @param isAssociated the isAssociated to set
	 */
	public void setAssociated(Boolean isAssociated) {
		this.isAssociated = isAssociated;
	}
	/**
	 * @return the batteryStatus
	 */
	@JsonProperty("batteryStatus")
	public String getBatteryStatus() {
		return batteryStatus;
	}
	/**
	 * @param batteryStatus the batteryStatus to set
	 */
	public void setBatteryStatus(String batteryStatus) {
		this.batteryStatus = batteryStatus;
	}
	/**
	 * @return the onlineStatus
	 */
	@JsonProperty("onlineStatus")
	public String isOnlineStatus() {
		return onlineStatus;
	}
	/**
	 * @param onlineStatus the onlineStatus to set
	 */
	public void setOnlineStatus(String onlineStatus) {
		this.onlineStatus = onlineStatus;
	}
	/**
	 * @return the latitude
	 */
	@JsonProperty("latitude")
	public String getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	@JsonProperty("longitude")
	public String getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the temperature
	 */
	@JsonProperty("temperature")
	public Double getTemperature() {
		return temperature;
	}
	/**
	 * @param temperature the temperature to set
	 */
	public void setTemperature(Double temperature) {
		this.temperature = temperature;
	}
	/**
	 * 
	 * @return the commission status
	 */
	public Boolean getIsCommissioned() {
		return isCommissioned;
	}
	/**
	 * 
	 * @param isCommissioned
	 */
	public void setIsCommissioned(Boolean isCommissioned) {
		this.isCommissioned = isCommissioned;
	}
	@Override
	public String toString() {
		return "DeviceFields ["
				+ (isAssociated != null ? "isAssociated=" + isAssociated + ", "	: "")
				+ (isCommissioned != null ? "isCommissioned=" + isCommissioned+ ", " : "")
				+ (batteryStatus != null ? "batteryStatus=" + batteryStatus	+ ", " : "")
				+ (onlineStatus != null ? "onlineStatus=" + onlineStatus + ", ": "")
				+ (latitude != null ? "latitude=" + latitude + ", " : "")
				+ (longitude != null ? "longitude=" + longitude + ", " : "")
				+ (temperature != null ? "temperature=" + temperature : "")
				+ "]";
	}
	
}
