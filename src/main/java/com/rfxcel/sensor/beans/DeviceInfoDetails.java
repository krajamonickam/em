package com.rfxcel.sensor.beans;

import org.codehaus.jackson.map.annotate.JsonSerialize;



/**
 * 
 * @author Bosco
 * @since 2016-12-11
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class DeviceInfoDetails
{
	private String deviceId;
	private String packageId;
	private String productId;
	private int pairStatus;
	private int isfavorite;
	private String createdOn;
	private Long updatedOn;
	private String latitude;
	
	private String traceId;
	private String rtsItemId;
	private String rtsSerialNumber;
	

	public String getTraceId() {
		return traceId;
	}

	public void setTraceId(String traceId) {
		this.traceId = traceId;
	}
	
	public String getLatitude() {
		return latitude;
	}

	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}

	public String getLongitude() {
		return longitude;
	}

	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}

	private String longitude;

	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param pairStatus
	 * @param isfavorite
	 */
	public DeviceInfoDetails(String deviceId, String packageId, int pairStatus,int isfavorite) {
		super();
		this.deviceId = deviceId;
		this.packageId = packageId;
		this.pairStatus = pairStatus;
		this.isfavorite = isfavorite;
	}

	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @param pairStatus
	 * @param isfavorite
	 */
	public DeviceInfoDetails(String deviceId, String packageId,String productId, int pairStatus,int isfavorite) {
		super();
		this.deviceId = deviceId;
		this.packageId = packageId;
		this.productId = productId;
		this.pairStatus = pairStatus;
		this.isfavorite = isfavorite;
	}
	/**
	 * 
	 */
	public DeviceInfoDetails(){
	}
	/**
	 * 
	 * @return deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * 
	 * @param sets deviceId
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * 
	 * @return packageId
	 */
	public String getPackageId() {
		return packageId;
	}
	/**
	 * 
	 * @param sets packageId
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return the productId
	 */
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 
	 * @return status of this pair
	 */
	public int getPairStatus() {
		return pairStatus;
	}
	/**
	 * 
	 * @param status
	 */
	public void setPairStatus(int status) {
		this.pairStatus = status;
	}
	/**
	 * 
	 * @return isfavorite flag which indicates the favorite shipment
	 */
	public int getIsfavorite() {
		return isfavorite;
	}
	/**
	 * 
	 * @param isfavorite flag
	 */
	public void setIsfavorite(int isfavorite) {
		this.isfavorite = isfavorite;
	}

	public String getCreatedOn() {
		return createdOn;
	}

	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

	public Long getUpdatedOn() {
		return updatedOn;
	}

	public void setUpdatedOn(Long updatedOn) {
		this.updatedOn = updatedOn;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeviceInfoDetails ["
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (packageId != null ? "packageId=" + packageId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ "pairStatus=" + pairStatus + ", isfavorite=" + isfavorite
				+ "]";
	}

	public String getRtsItemId() {
		return rtsItemId;
	}

	public void setRtsItemId(String rtsItemId) {
		this.rtsItemId = rtsItemId;
	}

	public String getRtsSerialNumber() {
		return rtsSerialNumber;
	}

	public void setRtsSerialNumber(String rtsSerialNumber) {
		this.rtsSerialNumber = rtsSerialNumber;
	}



}

