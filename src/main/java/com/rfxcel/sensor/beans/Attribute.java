package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author ashish_kumar1
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Attribute
{
	private Integer attrId;
	private String attrName;
	private Double attrValue;
	private String unit;
	private String legend;
	private Float min;
	private Float max;
	private Integer roundOffTo = null;
	private Long startDateTime;
	private Boolean excursion = false;
	private Integer excursionDuration;
	private Integer notifyLimit;
	private Boolean showGraph;
	private Boolean isConfigurable;
	private Long alertCode;
	private Integer scope;
	private Double factor;
	private Double multiplyFactor;
	private Integer deviceTypeId;
	
	private Double rangeMin;
	private Double rangeMax;
	private Boolean showMap;
	private Boolean isSensorAttribute;
	private Boolean alert;
	
	private Integer alertOnReset;
	
	
	/**
	 * @return the attrValue
	 */
	@JsonProperty("attrValue")
	public Double getAttrValue() {
		return attrValue;
	}
	/**
	 * @param attrValue the attrValue to set
	 */
	public void setAttrValue(Double attrValue) {
		this.attrValue = attrValue;
	}
	/**
	 * @return the unit
	 */
	@JsonProperty("unit")
	public String getUnit() {
		return unit;
	}
	/**
	 * @param unit the unit to set
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}
	/**
	 * @return the legend
	 */
	@JsonProperty("legend")
	public String getLegend() {
		return legend;
	}
	/**
	 * @param legend the legend to set
	 */
	public void setLegend(String legend) {
		this.legend = legend;
	}
	/**
	 * @return attrId
	 */
	@JsonProperty("attrId")
	public Integer getAttrId() {
		return attrId;
	}
	/**
	 * @param attrId the attrId to set
	 */
	public void setAttrId(Integer attrId) {
		this.attrId = attrId;
	}
	/**
	 * @return attrName
	 */
	@JsonProperty("attrName")
	public String getAttrName() {
		return attrName;
	}
	/**
	 * @param attrName the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	/**
	 * @return the startDateTime
	 */
	@JsonProperty("startDateTime")
	public Long getStartDateTime() {
		return startDateTime;
	}
	/**
	 * @param startDateTime the startDateTime to set
	 */
	public void setStartDateTime(Long startDateTime) {
		this.startDateTime = startDateTime;
	}
	/**
	 * 
	 * @return get min value of the attribute
	 */
	@JsonProperty("min")
	public Float getMin() {
		return min;
	}
	/**
	 * 
	 * @param min set min value of the attibute
	 */
	public void setMin(Float min) {
		this.min = min;
	}
	/**
	 * 
	 * @return max value of the attribute
	 */
	@JsonProperty("max")
	public Float getMax() {
		return max;
	}
	/**
	 * 
	 * @param max set max value of the attribute
	 */
	public void setMax(Float max) {
		this.max = max;
	}
	/**
	 * @return the roundOffTo
	 */
	@JsonProperty("roundOffTo")
	public Integer getRoundOffTo() {
		return roundOffTo;
	}
	/**
	 * @param roundOffTo the roundOffTo to set
	 */
	public void setRoundOffTo(Integer roundOffTo) {
		this.roundOffTo = roundOffTo;
	}
	/**
	 * @return the excursion
	 */
	public Boolean getExcursion() {
		return excursion;
	}
	/**
	 * @param excursion the excursion to set
	 */
	public void setExcursion(Boolean excursion) {
		this.excursion = excursion;
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getExcursionDuration() {
		return excursionDuration;
	}
	/**
	 * 
	 * @param excursionDuration
	 */
	public void setExcursionDuration(Integer excursionDuration) {
		this.excursionDuration = excursionDuration;
	}
	/**
	 * 
	 * @return
	 */
	@JsonProperty("notifyLimit")
	public Integer getNotifyLimit() {
		return notifyLimit;
	}
	/**
	 * 
	 * @param notifyLimit
	 */
	public void setNotifyLimit(Integer notifyLimit) {
		this.notifyLimit = notifyLimit;
	}
	/**
	 * 
	 * @return
	 */
	@JsonProperty("showGraph")
	public Boolean getShowGraph() {
		return showGraph;
	}
	/**
	 * 
	 * @param showGraph
	 */
	public void setShowGraph(Boolean showGraph) {
		this.showGraph = showGraph;
	}
	
	/**
	 * 
	 * @return
	 */
	public Boolean getIsConfigurable() {
		return isConfigurable;
	}
	/**
	 * 
	 * @param isConfigurable
	 */
	public void setIsConfigurable(Boolean isConfigurable) {
		this.isConfigurable = isConfigurable;
	}
	/**
	 * 
	 * @return
	 */
	public Long getAlertCode() {
		return alertCode;
	}
	/**
	 * 
	 * @param alertCode
	 */
	public void setAlertCode(Long alertCode) {
		this.alertCode = alertCode;
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getScope() {
		return scope;
	}
	/**
	 * 
	 * @param scope
	 */
	public void setScope(Integer scope) {
		this.scope = scope;
	}
	/**
	 * 
	 * @return
	 */
	public Double getFactor() {
		return factor;
	}
	/**
	 * 
	 * @param factor
	 */
	public void setFactor(Double factor) {
		this.factor = factor;
	}
	
	/**
	 * 
	 * @return
	 */
	public Double getMultiplyFactor() {
		return multiplyFactor;
	}
	/**
	 * 
	 * @param multiplyFactor
	 */
	public void setMultiplyFactor(Double multiplyFactor) {
		this.multiplyFactor = multiplyFactor;
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}
	/**
	 * 
	 * @param deviceTypeId
	 */
	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * 
	 * @return min range of an attribute
	 */
	@JsonProperty("rangeMin")
	public Double getRangeMin() {
		return rangeMin;
	}
	
	/**
	 * 
	 * @param rangeMin
	 */
	public void setRangeMin(Double rangeMin) {
		this.rangeMin = rangeMin;
	}
	
	/**
	 * 
	 * @return max range of an attribute
	 */
	@JsonProperty("rangeMax")
	public Double getRangeMax() {
		return rangeMax;
	}
	/**
	 * 
	 * @param rangeMax
	 */
	public void setRangeMax(Double rangeMax) {
		this.rangeMax = rangeMax;
	}
	/**
	 * 
	 * @return the showMap
	 */
	@JsonProperty("showMap")
	public Boolean getShowMap() {
		return showMap;
	}
	/**
	 * 
	 * @param showMap the showMap to set
	 */
	public void setShowMap(Boolean showMap) {
		this.showMap = showMap;
	}
	/**
	 * @return the isSensorAttribute
	 */
	@JsonProperty("isSensorAttribute")
	public Boolean getIsSensorAttribute() {
		return isSensorAttribute;
	}
	/**
	 * @param isSensorAttribute the isSensorAttribute to set
	 */
	public void setIsSensorAttribute(Boolean isSensorAttribute) {
		this.isSensorAttribute = isSensorAttribute;
	}
	/**
	 * @return the alert
	 */
	public Boolean getAlert() {
		return alert;
	}
	/**
	 * @param alert the alert to set
	 */
	public void setAlert(Boolean alert) {
		this.alert = alert;
	}
	
	/**
	 * 
	 * @return the alert on reset flag
	 */
	@JsonProperty("alertOnReset")
	public Integer getAlertOnReset() {
		return alertOnReset;
	}
	/**
	 * 
	 * @param alertOnReset flag to set
	 */
	public void setAlertOnReset(Integer alertOnReset) {
		this.alertOnReset = alertOnReset;
	}
	
	@Override
	public String toString() {
		return "Attribute ["
				+ (attrId != null ? "attrId=" + attrId + ", " : "")
				+ (attrName != null ? "attrName=" + attrName + ", " : "")
				+ (attrValue != null ? "attrValue=" + attrValue + ", " : "")
				+ (unit != null ? "unit=" + unit + ", " : "")
				+ (legend != null ? "legend=" + legend + ", " : "")
				+ (min != null ? "min=" + min + ", " : "")
				+ (max != null ? "max=" + max + ", " : "")
				+ (roundOffTo != null ? "roundOffTo=" + roundOffTo + ", " : "")
				+ (startDateTime != null ? "startDateTime=" + startDateTime	+ ", " : "")
				+ (excursion != null ? "excursion=" + excursion + ", " : "")
				+ (excursionDuration != null ? "excursionDuration="+ excursionDuration + ", " : "")
				+ (notifyLimit != null ? "notifyLimit=" + notifyLimit + ", ": "")
				+ (showGraph != null ? "showGraph=" + showGraph + ", " : "")
				+ (isConfigurable != null ? "isConfigurable=" + isConfigurable	+ ", " : "")
				+ (alertCode != null ? "alertCode=" + alertCode + ", " : "")
				+ (scope != null ? "scope=" + scope + ", " : "")
				+ (factor != null ? "factor=" + factor + ", " : "")
				+ (multiplyFactor != null ? "multiplyFactor=" + multiplyFactor	+ ", " : "")
				+ (deviceTypeId != null ? "deviceTypeId=" + deviceTypeId + ", " : "")
				+ (alertOnReset != null ? "alertOnReset=" + alertOnReset + ", " : "")
				+ (showMap != null ? "showMap=" + showMap : "")
				+ "]";
	}
	
}
