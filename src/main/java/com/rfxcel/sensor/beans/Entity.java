package com.rfxcel.sensor.beans;

public class Entity {
	protected String id = null;

	public Entity(String id) {
		this.id = id;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}
	
}
