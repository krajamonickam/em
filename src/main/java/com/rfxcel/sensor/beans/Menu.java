package com.rfxcel.sensor.beans;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author sachin_kohale
 * @since May 15, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Menu {
	private String state;
	private String menuName;
	private Integer order;
	private Long orgId;
	private Integer menuCode;
	
	/**
	 * 
	 * @return
	 */
	public String getState() {
		return state;
	}
	/**
	 * 
	 * @param state
	 */
	public void setState(String state) {
		this.state = state;
	}
	/**
	 * 
	 * @return
	 */
	public String getMenuName() {
		return menuName;
	}
	/**
	 * 
	 * @param menuName
	 */
	public void setMenuName(String menuName) {
		this.menuName = menuName;
	}
	/**
	 * 
	 * @return
	 */
	public Integer getOrder() {
		return order;
	}
	/**
	 * 
	 * @param order
	 */
	public void setOrder(Integer order) {
		this.order = order;
	}
	/**
	 * 
	 * @return
	 */
	public Long getOrgId() {
		return orgId;
	}
	/**
	 * 
	 * @param orgId
	 */
	public void setOrgId(Long orgId) {
		this.orgId = orgId;
	}	
	/**
	 * 
	 * @return
	 */
	public Integer getMenuCode() {
		return menuCode;
	}
	/**
	 * 
	 * @param menuCode
	 */
	public void setMenuCode(Integer menuCode) {
		this.menuCode = menuCode;
	}
}
