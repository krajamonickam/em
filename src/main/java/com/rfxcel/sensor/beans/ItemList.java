package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonRootName;

/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonRootName(value = "itemList")
public class ItemList
{
	/**Array list with itemId to fetch*/
	private ArrayList<String> itemId;

	/**
	 * @return itemId
	 */
	@JsonProperty("itemId")
	public ArrayList<String> getItemId() {
		return itemId;
	}

	/**
	 * @param itemId the itemId to set
	 */
	public void setItemId(ArrayList<String> itemId) {
		this.itemId = itemId;
	}

	@Override
	public String toString() {
		return "ItemList [" + (itemId != null ? "itemId=" + itemId : "") + "]";
	}

	

}
