package com.rfxcel.sensor.beans;

public class ShipDetails {
	private String location;
    private String add1;
    private String add2;
    private String city;
    private String state;
    private String country;
    private String zip;
    private Long dateTime;
    private Long locationId;
	
	public ShipDetails() {
	}

	/**
	 * @param location
	 * @param add1
	 * @param add2
	 * @param city
	 * @param state
	 * @param country
	 * @param zip
	 * @param dateTime
	 */
	public ShipDetails(Long locationId, String location, String add1, String add2, String city,
			String state, String country, String zip, Long dateTime) {
		super();
		this.locationId = locationId;
		this.location = location;
		this.add1 = add1;
		this.add2 = add2;
		this.city = city;
		this.state = state;
		this.country = country;
		this.zip = zip;
		this.dateTime = dateTime;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * @param location the location to set
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the add1
	 */
	public String getAdd1() {
		return add1;
	}

	/**
	 * @param add1 the add1 to set
	 */
	public void setAdd1(String add1) {
		this.add1 = add1;
	}

	/**
	 * @return the add2
	 */
	public String getAdd2() {
		return add2;
	}

	/**
	 * @param add2 the add2 to set
	 */
	public void setAdd2(String add2) {
		this.add2 = add2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the state
	 */
	public String getState() {
		return state;
	}

	/**
	 * @param state the state to set
	 */
	public void setState(String state) {
		this.state = state;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the zip
	 */
	public String getZip() {
		return zip;
	}

	/**
	 * @param zip the zip to set
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}

	/**
	 * @return the dateTime
	 */
	public Long getDateTime() {
		return dateTime;
	}

	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(Long dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * 
	 * @return locationId
	 */
	public Long getLocationId() {
		return locationId;
	}

	/**
	 * 
	 * @param locationId the locationId to set
	 */
	public void setLocationId(Long locationId) {
		this.locationId = locationId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ShipDetails ["
				+ (locationId != null ? "locationId=" + locationId + ", " : "")
				+ (location != null ? "location=" + location + ", " : "")
				+ (add1 != null ? "add1=" + add1 + ", " : "")
				+ (add2 != null ? "add2=" + add2 + ", " : "")
				+ (city != null ? "city=" + city + ", " : "")
				+ (state != null ? "state=" + state + ", " : "")
				+ (country != null ? "country=" + country + ", " : "")
				+ (zip != null ? "zip=" + zip + ", " : "")
				+ (dateTime != null ? "dateTime=" + dateTime : "") + "]";
	}
	
}
