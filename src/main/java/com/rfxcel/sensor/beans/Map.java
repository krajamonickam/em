package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Map
{
	

	private Long lastTimeStamp;
	private Integer zoomFactor;
	private String centerLat;
	private String centerLong;
	private ArrayList<Location> locations;

	public Map() {
		locations = new ArrayList<Location>();
	}
	
	/**
	 * @return the lastTimeStamp
	 */
	@JsonProperty("lastTimeStamp")
	public Long getLastTimeStamp() {
		return lastTimeStamp;
	}

	/**
	 * @param lastTimeStamp the lastTimeStamp to set
	 */
	public void setLastTimeStamp(Long lastTimeStamp) {
		this.lastTimeStamp = lastTimeStamp;
	}
	/**
	 * @return the zoomFactor
	 */
	@JsonProperty("zoomFactor")
	public Integer getZoomFactor() {
		return zoomFactor;
	}

	/**
	 * @param zoomFactor the zoomFactor to set
	 */
	public void setZoomFactor(Integer zoomFactor) {
		this.zoomFactor = zoomFactor;
	}
	
	

	public String getCenterLat() {
		return centerLat;
	}

	public String getCenterLong() {
		return centerLong;
	}

	public void setCenterLatLong(String centerLat, String centerLong) {
		this.centerLat = centerLat;
		this.centerLong = centerLong;
	}

	/**
	 * @return locations
	 */
	@JsonProperty("locations")
	public ArrayList<Location> getLocations() {
		return locations;
	}

	/**
	 * @param locations the locations to set
	 */
	public void setLocations(ArrayList<Location> locations) {
		this.locations = locations;
	}

	@Override
	public String toString() {
		return "Map ["
				+ (lastTimeStamp != null ? "lastTimeStamp=" + lastTimeStamp+ ", " : "")
				+ (zoomFactor != null ? "zoomFactor=" + zoomFactor + ", " : "")
				+ ((centerLat != null) ? "center=" + centerLat + "," + centerLong + ", " : "")
				+ (locations != null ? "locations=" + locations : "") + "]";
	}

}