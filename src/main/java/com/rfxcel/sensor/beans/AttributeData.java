package com.rfxcel.sensor.beans;

import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AttributeData
{
	/**Name of the attribute*/
	private String legend;
	private String attrName;
	private Long lastDateTime;
	private Double minValue;
	private Double maxValue;
	private String unit;
	private Double rangeMin;
	private Double rangeMax;
	private Boolean alert;
	

	/**List of dataPoints for particular legend*/
	private List<DataPoint> dataPoints;
	
	
	/**
	 * Default constructor
	 */
	public AttributeData() {
	}
	
	/**
	 * 
	 * @param legend
	 * @param attrName
	 * @param datapoints
	 * @param minValue
	 * @param maxValue
	 */

	public AttributeData(String legend,String attrName, List<DataPoint> datapoints, Double minValue, Double maxValue, String unit, boolean alert) {
	 this.legend = legend;
	 this.attrName = attrName;
	 this.dataPoints = datapoints;
	 this.setMinValue(minValue);
	 this.setMaxValue(maxValue);
	 this.unit = unit;
	 this.alert = alert;

	}

	/**
	 * @return the legend
	 */
	@JsonProperty("legend")
	public String getLegend() {
		return legend;
	}

	/**
	 * @param legend the legend to set
	 */
	public void setLegend(String legend) {
		this.legend = legend;
	}

	/**
	 * @return the attrName
	 */
	@JsonProperty("attrName")
	public String getAttrName() {
		return attrName;
	}

	/**
	 * @param attrName the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}

	/**
	 * @return the lastDateTime
	 */
	@JsonProperty("lastDateTime")
	public Long getLastDateTime() {
		return lastDateTime;
	}

	/**
	 * @param lastDateTime the lastDateTime to set
	 */
	public void setLastDateTime(Long lastDateTime) {
		this.lastDateTime = lastDateTime;
	}

	/**
	 * @return the dataPoints
	 */
	@JsonProperty("dataPoints")
	public List<DataPoint> getDataPoints() {
		return dataPoints;
	}

	/**
	 * @param dataPoints the dataPoints to set
	 */
	public void setDataPoints(List<DataPoint> dataPoints) {
		this.dataPoints = dataPoints;
	}

	/**
	 * @return the minValue
	 */
	@JsonProperty("minValue")
	public Double getMinValue() {
		return minValue;
	}

	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}

	/**
	 * @return the maxValue
	 */
	@JsonProperty("maxValue")
	public Double getMaxValue() {
		return maxValue;
	}

	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * 
	 * @return
	 */
	public String getUnit() {
		return unit;
	}

	/**
	 * 
	 * @param unit
	 */
	public void setUnit(String unit) {
		this.unit = unit;
	}

	/**
	 * @return the rangeMin
	 */
	public Double getRangeMin() {
		return rangeMin;
	}

	/**
	 * @param rangeMin the rangeMin to set
	 */
	public void setRangeMin(Double rangeMin) {
		this.rangeMin = rangeMin;
	}

	/**
	 * @return the rangeMax
	 */
	public Double getRangeMax() {
		return rangeMax;
	}

	/**
	 * @param rangeMax the rangeMax to set
	 */
	public void setRangeMax(Double rangeMax) {
		this.rangeMax = rangeMax;
	}
	
	@JsonProperty("alert")
	public Boolean getAlert() {
		return alert;
	}

	public void setAlert(Boolean alert) {
		this.alert = alert;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AttributeData ["
				+ (legend != null ? "legend=" + legend + ", " : "")
				+ (attrName != null ? "attrName=" + attrName + ", " : "")
				+ (lastDateTime != null ? "lastDateTime=" + lastDateTime + ", "
						: "")
				+ (minValue != null ? "minValue=" + minValue + ", " : "")
				+ (maxValue != null ? "maxValue=" + maxValue + ", " : "")
				+ (dataPoints != null ? "dataPoints=" + dataPoints + ", " : "") 
				+ (unit != null ? "unit=" + unit : "") +"]";
	}
	
}
