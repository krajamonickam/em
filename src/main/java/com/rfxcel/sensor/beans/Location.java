package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;
/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Location
{
	private Long id;
	private Long dateTime;
	private String messageId;
	private String lat;
	private String lang;
	private String packageId;
	private String deviceId;
	private String productId;
	private String status;
	private String address;
	private Integer pairStatus;
	
	/**
	 * @param deviceId
	 * @param packageId
	 */
	public Location(String deviceId, String packageId, String productId) {
		this.deviceId = deviceId;
		this.packageId = packageId;
		this.setProductId(productId);
	}
	
	/**
	 * 
	 * @param id
	 * @param dateTime
	 * @param lat
	 * @param lang
	 * @param pairStatus
	 */
	public Location(Long id, Long dateTime, String lat, String lang, Integer pairStatus) {
		this.id = id;
		this.dateTime = dateTime;
		this.lat = lat;
		this.lang = lang;
		this.pairStatus = pairStatus;
	}
	
	
	/**
	 * @param dateTime
	 * @param lat
	 * @param lang
	 * @param packageId
	 * @param deviceId
	 * @param pairStatus
	 */
	public Location(Long dateTime,String deviceId,String packageId, String lat, String lang, Integer pairStatus) {
		super();
		this.dateTime = dateTime;
		this.packageId = packageId;
		this.deviceId = deviceId;
		this.lat = lat;
		this.lang = lang;
		this.pairStatus = pairStatus;
	}


	public Location() {
	}
	
	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	
	
	/**
	 * @return the dateTime
	 */
	@JsonProperty("dateTime")
	public Long getDateTime() {
		return dateTime;
	}
	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(Long dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return messageId
	 */
	@JsonProperty("messageId")
	public String getMessageId() {
		return messageId;
	}
	/**
	 * @param messageId the messageId to set
	 */
	public void setMessageId(String messageId) {
		this.messageId = messageId;
	}
	/**
	 * @return  lat
	 */
	@JsonProperty("lat")
	public String getLat() {
		return lat;
	}
	/**
	 * @param lat the lat to set
	 */
	public void setLat(String lat) {
		this.lat = lat;
	}
	/**
	 * @return  lang
	 */
	@JsonProperty("lang")
	public String getLang() {
		return lang;
	}
	/**
	 * @param lang the lang to set
	 */
	public void setLang(String lang) {
		this.lang = lang;
	}
	
	/**
	 * @return the packageId
	 */
	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return the deviceId
	 */
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	
	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return status
	 */
	@JsonProperty("status")
	public String getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}
	/**
	 * 
	 * @return address corresponds to latitude and longitude values
	 */
	@JsonProperty("address")
	public String getAddress() {
		return address;
	}
	/**
	 * 
	 * @param address street address of latitude and longitude 
	 */
	public void setAddress(String address) {
		this.address = address;
	}

	/**
	 * @return the pairStatus
	 */
	public Integer getPairStatus() {
		return pairStatus;
	}

	/**
	 * @param pairStatus the pairStatus to set
	 */
	public void setPairStatus(Integer pairStatus) {
		this.pairStatus = pairStatus;
	}


	@Override
	public String toString() {
		return "Location [" + (id != null ? "id=" + id + ", " : "")
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (messageId != null ? "messageId=" + messageId + ", " : "")
				+ (lat != null ? "lat=" + lat + ", " : "")
				+ (lang != null ? "lang=" + lang + ", " : "")
				+ (packageId != null ? "packageId=" + packageId + ", " : "")
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (status != null ? "status=" + status + ", " : "")
				+ (address != null ? "address=" + address + ", " : "")
				+ (pairStatus != null ? "pairStatus=" + pairStatus : "") + "]";
	}
	
	

}