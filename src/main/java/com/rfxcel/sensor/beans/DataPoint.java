package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;


/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
public class DataPoint
{
	private Long dateTime;
	private Double dataPoint;
	/**
	 * @return dateTime
	 */
	@JsonProperty("dateTime")
	public Long getDateTime() {
		return dateTime;
	}
	/**
	 * @param dateTime to set
	 */
	public void setDateTime(Long dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return dataPoint
	 */
	@JsonProperty("dataPoint")
	public Double getDataPoint() {
		return dataPoint;
	}
	/**
	 * @param dataPoint the dataPoint to set
	 */
	public void setDataPoint(Double dataPoint) {
		this.dataPoint = dataPoint;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DataPoint ["
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (dataPoint != null ? "dataPoint=" + dataPoint : "") + "]";
	}
}

