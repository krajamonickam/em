package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class MonitorAlerts {
	
	public ArrayList<String> alertsMsg = new ArrayList<String>();
	public Integer alertsIndex = 0; 
	public Boolean alertsFinished = true;
	
	@JsonProperty("alertsMsg")
	public ArrayList<String> getMonitorAlerts() {
		return alertsMsg;
	}
	public void setMonitorAlerts(ArrayList<String> monitorAlerts) {
		this.alertsMsg = monitorAlerts;
	}
	
	public Integer getAlertsIndex() {
		return alertsIndex;
	}
	public void setAlertsIndex(Integer alertsIndex) {
		this.alertsIndex = alertsIndex;
	}
	public Boolean getAlertsFinished() {
		return alertsFinished;
	}
	public void setAlertsFinished(Boolean alertsFinished) {
		this.alertsFinished = alertsFinished;
	}
	
	/**
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
	
}
