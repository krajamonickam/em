package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonRootName;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;

/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonRootName(value = "deviceData")
public class DeviceData
{
	/**deviceId for which data is required*/
	private String deviceId;
	/**packageId attached to deviceId for which data is required*/
	private String packageId;
	/**attributes data of a device for graph used for  getAttrData service*/
	private ArrayList<Attribute> attrType;
	/**Fields to be fetched in, field names should be constant. Used for getDeviceStatus service*/
	private ArrayList<String> fields;
	
	private String dateTime;
	
	/**Specifies range with startDate to filter dataPoints*/
	private Long startDate;
	/**Specifies range endDate*/
	private Long endDate;
	/**size of record set*/
	private Integer dpCount;
	
	private String shipmentStatus;
	
	private Long id;
	
	/** Time Duration for getting graph data  **/
	private Float duration;
	
	private Boolean showAll;
	
	private Integer start;
	
	private Integer pageSize;
	
	private Integer profileId;
	
	private String productId;
	
	private Boolean showMapAttribute;
	
	/*to check shipment list type currentShipmenList(relationStatus=1), completedShipment(relationStatus=0)*/
	private Integer relationStatus;
	/*to check shipment list type pendingShipmentList(SensorDataExist = 0)*/
	//private Integer SensorDataExist;
	private String rtsItemId;
	private String rtsSerialNumber;
	/**
	 * @param deviceId
	 * @param packageId
	 */
	public DeviceData(String deviceId, String packageId) {
		this.deviceId = deviceId;
		this.packageId = packageId;
	}
	
	public DeviceData() {
	}
	/**
	 * @return  deviceId
	 */
	@JsonProperty("deviceId")
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the packageId
	 */
	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}
	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}
	/**
	 * @return the fields
	 */
	@JsonProperty("fields")
	public ArrayList<String> getFields() {
		return fields;
	}
	/**
	 * @param fields the fields to set
	 */
	public void setFields(ArrayList<String> fields) {
		this.fields = fields;
	}
	
	/**
	 * @return  startDate
	 */
	@JsonProperty("startDate")
	public Long getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	/**
	 * @return  endDate
	 */
	@JsonProperty("endDate")
	public Long getEndDate() {
		return endDate;
	}
	/**
	 * @return the dpCount
	 */
	@JsonProperty("dpCount")
	public Integer getDpCount() {
		return dpCount;
	}
	/**
	 * @param dpCount the dpCount to set
	 */
	public void setDpCount(Integer dpCount) {
		this.dpCount = dpCount;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}
	/**
	 * @return attrType
	 *//*
	@JsonProperty("attrType")
	public ArrayList<Attribute> getAttributeType() {
		return attrType;
	}
	*//**
	 * @param attrType the attrType to set
	 *//*
	public void setAttributeType(ArrayList<Attribute> attrType) {
		this.attrType = attrType;
	}*/
	
	@JsonProperty("attrType")
	public ArrayList<Attribute> getAttrType() {
		return attrType;
	}

	public void setAttrType(ArrayList<Attribute> attrType) {
		this.attrType = attrType;
	}

	
	
	@JsonProperty("dateTime")
	public String getDateTime() {
		return dateTime;
	}
	
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * 
	 * @return shipmentStatus
	 */
	@JsonProperty("shipmentStatus")
	public String getShipmentStatus() {
		return shipmentStatus;
	}
	/**
	 * 
	 * @param shipmentStatus
	 */
	public void setShipmentStatus(String shipmentStatus) {
		this.shipmentStatus = shipmentStatus;
	}

	/**
	 * @return the id
	 */
	@JsonProperty("id")
	public Long getId() {
		return id;
	}
	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}
	/**
	 * @return the duration
	 */
	@JsonProperty("duration")
	public Float getDuration() {
		return duration;
	}
	/**
	 * @param duration the duration to set
	 */
	public void setDuration(Float duration) {
		this.duration = duration;
	}
	
	/**
	 * @return the showAll
	 */
	public Boolean getShowAll() {
		return showAll;
	}
	/**
	 * @param showAll the showAll to set
	 */
	public void setShowAll(Boolean showAll) {
		this.showAll = showAll;
	}

	/**
	 * @return the start
	 */
	@JsonProperty("start")
	public Integer getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(Integer start) {
		this.start = start;
	}

	/**
	 * @return the pageSize
	 */
	@JsonProperty("pageSize")
	public Integer getPageSize() {
		return pageSize;
	}

	/**
	 * @param pageSize the pageSize to set
	 */
	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}


	/**
	 * @return the profileId
	 */
	public Integer getProfileId() {
		return profileId;
	}

	/**
	 * @param profileId the profileId to set
	 */
	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * 
	 * @return the showMapAttribute
	 */
	public Boolean getShowMapAttribute() {
		return showMapAttribute;
	}

	/**
	 * 
	 * @param showMapAttribute the showMapAttribute to set
	 */
	public void setShowMapAttribute(Boolean showMapAttribute) {
		this.showMapAttribute = showMapAttribute;
	}

	/**
	 * 
	 * @return the relationStatus
	 */
	public Integer getRelationStatus() {
		return relationStatus;
	}

	/**
	 * 
	 * @param relationStatus the relationStatus to set
	 */
	public void setRelationStatus(Integer relationStatus) {
		this.relationStatus = relationStatus;
	}

	/**
	 * 
	 * @return the SensorDataExist
	 */
	/*public Integer getSensorDataExist() {
		return SensorDataExist;
	}

	*//**
	 * 
	 * @param sensorDataExist the SensorDataExist to set
	 *//*
	public void setSensorDataExist(Integer sensorDataExist) {
		SensorDataExist = sensorDataExist;
	}
*/
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "DeviceData ["
				+ (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (packageId != null ? "packageId=" + packageId + ", " : "")
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (attrType != null ? "attrType=" + attrType + ", " : "")
				+ (fields != null ? "fields=" + fields + ", " : "")
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (startDate != null ? "startDate=" + startDate + ", " : "")
				+ (endDate != null ? "endDate=" + endDate + ", " : "")
				+ (dpCount != null ? "dpCount=" + dpCount + ", " : "")
				+ (shipmentStatus != null ? "shipmentStatus=" + shipmentStatus+ ", " : "")
				+ (id != null ? "id=" + id + ", " : "")
				+ (duration != null ? "duration=" + duration + ", " : "")
				+ (showAll != null ? "showAll=" + showAll + ", " : "")
				+ (showMapAttribute != null ? "showMapAttribute=" + showMapAttribute + ", " : "")
				+ (start != null ? "start=" + start + ", " : "")
				+ (pageSize != null ? "pageSize=" + pageSize + ", " : "") + "]";
	}

	/**
	 * 
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;

	}

	public String getRtsItemId() {
		return rtsItemId;
	}

	public void setRtsItemId(String rtsItemId) {
		this.rtsItemId = rtsItemId;
	}

	public String getRtsSerialNumber() {
		return rtsSerialNumber;
	}

	public void setRtsSerialNumber(String rtsSerialNumber) {
		this.rtsSerialNumber = rtsSerialNumber;
	}
}
