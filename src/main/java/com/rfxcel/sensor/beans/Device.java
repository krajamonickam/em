package com.rfxcel.sensor.beans;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author albeena
 * @since 2016-10-10
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Device
{
	private String deviceId;
	private Integer deviceTypeId;
	private Boolean active;
	private int status;
	private Integer orgId;
	private Long groupId;
	private Long batteryValue;
	private Integer demoFlag;
	private String deviceType;
	private String deviceKey;
	private String mdn; //mdn - mobile device number
	/**
	 * 0 - pending , 1 completed
	 */
	private Integer activationStatus;
	/**
	 * 
	 */
	public Device() {
		// TODO Auto-generated constructor stub
	}
	/**
	 * @param deviceId
	 * @param deviceTypeId
	 * @param active
	 * @param status
	 * @param orgId
	 * @param groupId
	 * @param batteryValue
	 * @param demoFlag
	 * @param deviceType
	 * @param deviceKey
	 */
	public Device(String deviceId, Integer deviceTypeId, Boolean active,
			int status, Integer orgId, Long groupId, Long batteryValue,
			Integer demoFlag, String deviceType, String deviceKey, String mdn) {
		this.deviceId = deviceId;
		this.deviceTypeId = deviceTypeId;
		this.active = active;
		this.status = status;
		this.orgId = orgId;
		this.groupId = groupId;
		this.batteryValue = batteryValue;
		this.demoFlag = demoFlag;
		this.deviceType = deviceType;
		this.deviceKey = deviceKey;
		this.mdn = mdn;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}
	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}
	/**
	 * @return the deviceTypeId
	 */
	public Integer getDeviceTypeId() {
		return deviceTypeId;
	}
	/**
	 * @param deviceTypeId the deviceTypeId to set
	 */
	public void setDeviceTypeId(Integer deviceTypeId) {
		this.deviceTypeId = deviceTypeId;
	}
	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}
	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}
	/**
	 * @return the status
	 */
	public int getStatus() {
		return status;
	}
	/**
	 * @param status the status to set
	 */
	public void setStatus(int status) {
		this.status = status;
	}
	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}
	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}
	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}
	/**
	 * @return the batteryValue
	 */
	public Long getBatteryValue() {
		return batteryValue;
	}
	/**
	 * @param batteryValue the batteryValue to set
	 */
	public void setBatteryValue(Long batteryValue) {
		this.batteryValue = batteryValue;
	}
	/**
	 * @return the demoFlag
	 */
	public Integer getDemoFlag() {
		return demoFlag;
	}
	/**
	 * @param demoFlag the demoFlag to set
	 */
	public void setDemoFlag(Integer demoFlag) {
		this.demoFlag = demoFlag;
	}
	/**
	 * @return the deviceType
	 */
	public String getDeviceType() {
		return deviceType;
	}
	/**
	 * @param deviceType the deviceType to set
	 */
	public void setDeviceType(String deviceType) {
		this.deviceType = deviceType;
	}
	/**
	 * @return the deviceKey
	 */
	public String getDeviceKey() {
		return deviceKey;
	}
	/**
	 * @param deviceKey the deviceKey to set
	 */
	public void setDeviceKey(String deviceKey) {
		this.deviceKey = deviceKey;
	}
	/**
	 * 
	 * @return mdn
	 */
	public String getMdn() {
		return mdn;
	}
	/**
	 * 
	 * @param mdn the mdn to set
	 */
	public void setMdn(String mdn) {
		this.mdn = mdn;
	}
	public Integer getActivationStatus() {
		return activationStatus;
	}
	public void setActivationStatus(Integer activationStatus) {
		this.activationStatus = activationStatus;
	}
	
	@Override
	public String toString() {
		return "Device [" + (deviceId != null ? "deviceId=" + deviceId + ", " : "")
				+ (deviceTypeId != null ? "deviceTypeId=" + deviceTypeId + ", " : "")
				+ (active != null ? "active=" + active + ", " : "") 
				+ "status=" + status + ", "
				+ (orgId != null ? "orgId=" + orgId + ", " : "") 
				+ (groupId != null ? "groupId=" + groupId + ", " : "")
				+ (batteryValue != null ? "batteryValue=" + batteryValue + ", " : "")
				+ (demoFlag != null ? "demoFlag=" + demoFlag + ", " : "")
				+ (deviceType != null ? "deviceType=" + deviceType + ", " : "")
				+ (deviceKey != null ? "deviceKey=" + deviceKey + ", " : "")
				+ (mdn != null ? "mdn=" + mdn + ", " : "")
				+ (activationStatus != null ? "activationStatus=" + activationStatus : "") + "]";
	}
	
}
