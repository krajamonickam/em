package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;

/**
 * 
 * @author bosco
 * @since 2016-12-06
 */
public class LocationRequest {
	private String id;
	private String name;
	
	@JsonProperty("id")
	public String getId() {
		return id;
	}
	public void setId(String idv) {
		id = idv;
	}
	@JsonProperty("name")
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LocationRequest [" + (id != null ? "id=" + id + ", " : "")
				+ (name != null ? "name=" + name : "") + "]";
	}

}
