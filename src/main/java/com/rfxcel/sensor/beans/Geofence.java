/**
 * @ COMPANY              : RFXCEL	
 * @ PROJECT              : Sensor Receiver
 * @ AUTHOR               : JEYASEELAN   
 * @ DESCRIPTION          : 
 *
 * @ MODIFICATION HISTORY:
 **********************************************************************************
 * DATE			*		NAME                *       			CHANGES           *
 **********************************************************************************
 * Jun 1, 2016	*		JEYASEELAN A		* CREATED                             *
 * 				*							*                                     *
 *				*							*									  *
 *				*							*									  *
 **********************************************************************************
 */
package com.rfxcel.sensor.beans;


public class Geofence {
	
	private Long id;
	private String geofenceNumber;
	private String pointName;
	private Double latitude;
	private Double longitude;
	private Integer radius;
	private int isHome=0;
	
	/**
	 * -1: value is not set
	 * 1: device has entered geopoint radius
	 * 0: device exited from geopoint radius
	 */
	private int state = -1;	
	
	public Geofence(){
	}
	
	/**
	 * 
	 * @param id 
	 * @param geofenceNumber
	 * @param pointName
	 * @param latitude
	 * @param longitude
	 * @param radius
	 */
	public Geofence(Long id, String geofenceNumber, String pointName, Double latitude,Double longitude, Integer radius,int isHome) {
		this.id = id;
		this.geofenceNumber = geofenceNumber;
		this.pointName = pointName;
		this.latitude = latitude;
		this.longitude = longitude;
		this.radius = radius;
		this.isHome = isHome;
	}
	
	
	/**
	 * @return the geofenceNumber
	 */
	public String getGeofenceNumber() {
		return geofenceNumber;
	}
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @param geofenceNumber the geofenceNumber to set
	 */
	public void setGeofenceNumber(String geofenceNumber) {
		this.geofenceNumber = geofenceNumber;
	}
	/**
	 * @return the pointName
	 */
	public String getPointName() {
		return pointName;
	}
	/**
	 * @param pointName the pointName to set
	 */
	public void setPointName(String pointName) {
		this.pointName = pointName;
	}
	/**
	 * @return the latitude
	 */
	public Double getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public Double getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the radius
	 */
	public Integer getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	/**
	 * @return the state
	 * -1:- value is not set
	 * 1:- device has entered geopoint radius
	 * 0:- device exited from geopoint radius
	 */
	public int getState() {
		return state;
	}
	/**
	 * @param state the state to set
	 */
	public void setState(int state) {
		this.state = state;
	}
	/**
	 * 0- if not home location
	 * 1- if home location
	 * @return
	 */
	public int getIsHome() {
		return isHome;
	}
	/**
	 * 0- if not home location
	 * 1- if home location
	 * @param isHome
	 */
	public void setIsHome(int isHome) {
		this.isHome = isHome;
	}
}
