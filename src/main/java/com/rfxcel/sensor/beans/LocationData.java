package com.rfxcel.sensor.beans;

import java.util.ArrayList;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class LocationData
{
	ArrayList<ShipmentLocation> locations ;

	public LocationData() {
		locations = new ArrayList<ShipmentLocation>();
	}
	
	@JsonProperty("locationList")
	public ArrayList<ShipmentLocation> getLocations() {
		return locations;
	}

	public void setLocations(ArrayList<ShipmentLocation> locations) {
		this.locations = locations;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "LocationData ["
				+ (locations != null ? "locations=" + locations : "") + "]";
	}

    

   
}