package com.rfxcel.sensor.beans;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Geopoint {

	private String address;
	private String latitude;
	private String longitude;
	private Integer radius;
	private String pointName;
	private String pointNumber;
	private Integer locationType;
	
	
	private String addr1;
	private String addr2;
	private String city;
	private String state;
	private String zip;
	private String country;
	
	/**
	 * @return the address
	 */
	public String getAddress() {
		return address;
	}
	/**
	 * @param address the address to set
	 */
	public void setAddress(String address) {
		this.address = address;
	}
	/**
	 * @return the latitude
	 */
	public String getLatitude() {
		return latitude;
	}
	/**
	 * @param latitude the latitude to set
	 */
	public void setLatitude(String latitude) {
		this.latitude = latitude;
	}
	/**
	 * @return the longitude
	 */
	public String getLongitude() {
		return longitude;
	}
	/**
	 * @param longitude the longitude to set
	 */
	public void setLongitude(String longitude) {
		this.longitude = longitude;
	}
	/**
	 * @return the radius
	 */
	public Integer getRadius() {
		return radius;
	}
	/**
	 * @param radius the radius to set
	 */
	public void setRadius(Integer radius) {
		this.radius = radius;
	}
	/**
	 * @return the pointName
	 */
	public String getPointName() {
		return pointName;
	}
	/**
	 * @param pointName the pointName to set
	 */
	public void setPointName(String pointName) {
		this.pointName = pointName;
	}
	/**
	 * 
	 * @return pointNumber
	 */
	public String getPointNumber() {
		return pointNumber;
	}
	
	/**
	 * 
	 * @param pointNumber
	 */
	public void setPointNumber(String pointNumber) {
		this.pointNumber = pointNumber;
	}
	
	/**
	 * @return the locationType
	 */
	public Integer getLocationType() {
		return locationType;
	}
	/**
	 * @param locationType the locationType to set
	 */
	public void setLocationType(Integer locationType) {
		this.locationType = locationType;
	}
	
	/**
	 * 
	 * @return Address1 
	 */
	public String getAddr1() {
		return addr1;
	}
	
	/**
	 * 
	 * @param set addr1
	 */
	public void setAddr1(String addr1) {
		this.addr1 = addr1;
	}
	
	/**
	 * 
	 * @return addr2
	 */
	public String getAddr2() {
		return addr2;
	}
	
	/**
	 * 
	 * @param set addr2
	 */
	public void setAddr2(String addr2) {
		this.addr2 = addr2;
	}
	/**
	 * 
	 * @return city
	 */
	public String getCity() {
		return city;
	}
	
	/**
	 * 
	 * @param set city
	 */
	public void setCity(String city) {
		this.city = city;
	}
	
	/**
	 * 
	 * @return state code
	 */
	public String getState() {
		return state;
	}
	
	/**
	 * 
	 * @param set state code
	 */
	public void setState(String state) {
		this.state = state;
	}
	
	/**
	 * 
	 * @return zip code
	 * 
	 */
	public String getZip() {
		return zip;
	}
	/**
	 * 
	 * @param set zip code
	 */
	public void setZip(String zip) {
		this.zip = zip;
	}
	
	/**
	 * 
	 * @return get country code
	 */
	public String getCountry() {
		return country;
	}
	/**
	 * 
	 * @param set country
	 */
	public void setCountry(String country) {
		this.country = country;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Geopoint [" + (address != null ? "address=" + address + ", " : "")
				+ (latitude != null ? "latitude=" + latitude + ", " : "")
				+ (longitude != null ? "longitude=" + longitude + ", " : "")
				+ (radius != null ? "radius=" + radius + ", " : "")
				+ (pointName != null ? "pointName=" + pointName + ", " : "")
				+ (pointNumber != null ? "pointNumber=" + pointNumber + ", " : "")
				+ (locationType != null ? "locationType=" + locationType : "") 
				+ (addr1 != null ? "addr1=" + addr1 : "")
				+ (addr2 != null ? "addr2=" + addr2 : "")
				+ (city != null ? "city=" + city : "")
				+ (state != null ? "state=" + state : "")
				+ (zip != null ? "zip=" + zip : "")
				+ (country != null ? "country=" + country : "")
				+ "]";
	}
}
