package com.rfxcel.sensor.beans;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;


/**
 * 
 * @author ashish_kumar1
 * @since 2016-10-10
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class Sensor
{
	/***
	 * Returns all the shipments info. Used in getActiveShipmentList service
	 */
	private ArrayList<DeviceInfoDetails> shipmentList;
	/**
	 * A list of objects with legend and dataPoints
	 * Returned only when request is made for getAttrData,Omitted otherwise. Used in getAttrData service
	 * */
	private ArrayList<AttributeData> attributes;
	/**
	 * It will provide latest snap shot of device parameters. Used in getDeviceStatus service.
	 */
	private DeviceFields fields;
	/**
	 * A list of locations for a device
	 * Returned only when request is made for getMapData,Omitted otherwise
	 */
	private Map map;
	
	/**
	 * A list of itemId's starting with input itemId
	 * Returned only when request is made for getItemList,Omitted otherwise
	 */
	private ArrayList<String> itemList;
	/**
	 * A list of attributes for a device
	 * Returned only when request is made for getAttrList,Omitted otherwise.Used in getLatestAttrInfo service
	 */
	private ArrayList<Attribute> attributeList;	
	
	private List<DeviceDetail> deviceList;
	private List<Device> devices;
	private ShipmentData shipmentData;
	
	HashMap<String, String> configMap;
	private ArrayList<Object> rows;
	
	private String homeGeopointName;
	private Long statusTime;
	
	/**Specifies range with startDate to filter dataPoints*/
	private Long startDate;
	/**Specifies range endDate*/
	private Long endDate;
	/**
	 * 
	 * @return the shipmentList
	 */
	@JsonProperty("shipmentList")
	public ArrayList<DeviceInfoDetails> getShipmentList() {
		return shipmentList;
	}
	/**
	 * 
	 * @param shipmentList the shipmentList to set
	 */
	public void setShipmentList(ArrayList<DeviceInfoDetails> shipmentList) {
		this.shipmentList = shipmentList;
	}
	
	
	/**
	 * @return the attributes
	 */
	@JsonProperty("attributes")
	public ArrayList<AttributeData> getAttributes() {
		return attributes;
	}
	
	/**
	 * @param attributes the attributes to set
	 */
	public void setAttributes(ArrayList<AttributeData> attributes) {
		this.attributes = attributes;
	}
	/**
	 * @return the fields
	 */
	@JsonProperty("fields")
	public DeviceFields getFields() {
		return fields;
	}
	/**
	 * @param fields the fields to set
	 */
	public void setFields(DeviceFields fields) {
		this.fields = fields;
	}
	/**
	 * @return the map
	 */
	@JsonProperty("map")
	public Map getMap() {
		return map;
	}
	/**
	 * @param map the map to set
	 */
	public void setMap(Map map) {
		this.map = map;
	}
	/**
	 * @return the itemList
	 */
	@JsonProperty("itemList")
	public ArrayList<String> getItemList() {
		return itemList;
	}
	/**
	 * @param itemList the itemList to set
	 */
	public void setItemList(ArrayList<String> itemList) {
		this.itemList = itemList;
	}
	/**
	 * @return the attributeList
	 */
	@JsonProperty("attributeList")
	public ArrayList<Attribute> getAttributeList() {
		return attributeList;
	}
	/**
	 * @param attributeList the attributeList to set
	 */
	public void setAttributeList(ArrayList<Attribute> attributeList) {
		this.attributeList = attributeList;
	}
	public List<DeviceDetail> getDeviceList() {
		return deviceList;
	}
	public void setDeviceList(List<DeviceDetail> deviceList) {
		this.deviceList = deviceList;
	}
	
	public List<Device> getDevices() {
		return devices;
	}
	public void setDevices(List<Device> devices) {
		this.devices = devices;
	}
	/**
	 * @return the shipmentData
	 */
	@JsonProperty("shipmentData")
	public ShipmentData getShipmentData() {
		return shipmentData;
	}
	/**
	 * @param shipmentData the shipmentData to set
	 */
	public void setShipmentData(ShipmentData shipmentData) {
		this.shipmentData = shipmentData;
	}
	/**
	 * 
	 * @return list of config params and its values
	 */
	public HashMap<String, String> getConfigMap() {
		return configMap;
	}
	/**
	 * 
	 * @param sets the config map having list of params of sensor devices
	 */
	public void setConfigMap(HashMap<String, String> configMap) {
		this.configMap = configMap;
	}

	/**
	 * @return the rows
	 */
	@JsonProperty("rows")
	public ArrayList<Object> getRows() {
		return rows;
	}
	
	/**
	 * @param rows the rows to set
	 */
	public void setRows(ArrayList<Object> rows) {
		this.rows = rows;
	}
	/**
	 * @return the geopointName
	 */
	public String getHomeGeopointName() {
		return homeGeopointName;
	}
	/**
	 * @param geopointName the geopointName to set
	 */
	public void setHomeGeopointName(String homeGeopointName) {
		this.homeGeopointName = homeGeopointName;
	}
	/**
	 * 
	 * @return the statusTime
	 */
	public Long getStatusTime() {
		return statusTime;
	}
	/**
	 * 
	 * @param statusTime the statusTime to set
	 */
	public void setStatusTime(Long statusTime) {
		this.statusTime = statusTime;
	}
	/**
	 * @return the startDate
	 */
	public Long getStartDate() {
		return startDate;
	}
	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(Long startDate) {
		this.startDate = startDate;
	}
	
	/**
	 * @return the endDate
	 */
	public Long getEndDate() {
		return endDate;
	}
	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(Long endDate) {
		this.endDate = endDate;
	}
	
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Sensor ["
				+ (shipmentList != null ? "shipmentList=" + shipmentList + ", ": "")
				+ (attributes != null ? "attributes=" + attributes + ", " : "")
				+ (fields != null ? "fields=" + fields + ", " : "")
				+ (map != null ? "map=" + map + ", " : "")
				+ (itemList != null ? "itemList=" + itemList + ", " : "")
				+ (attributeList != null ? "attributeList=" + attributeList+", " : "")
				+ (deviceList != null ? "deviceList=" + deviceList + ", " : "")
				+ (devices != null ? "devices=" + devices + ", ": "")
				+ (shipmentData != null ? "shipmentData=" + shipmentData + ", ": "")
				+ (configMap != null ? "configMap=" + configMap + ", " : "")
				+ (rows != null ? "rows=" + rows + ", " : "")
				+ (homeGeopointName != null ? "homeGeopointName="+ homeGeopointName : "") 
				+ (statusTime != null ? "statusTime="+ statusTime : "")
				+ (startDate != null ? "startDate="+ startDate : "")
				+ (endDate != null ? "endDate="+ endDate : "")
				+ "]";
	}

	
}
