package com.rfxcel.sensor.beans;

/**
 * 
 * @author Tejshree Kachare
 * @since 26 July 2017
 */


public class ExcursionLog {

	private String deviceId; 
	private String packageId;
	private Integer orgId;
	private String attribute;

	public ExcursionLog(){
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param orgId
	 */
	public ExcursionLog(String deviceId, String packageId, Integer orgId) {
		this.deviceId = deviceId;
		this.packageId = packageId;
		this.orgId = orgId;
	}

	public ExcursionLog(String deviceId, String packageId, String attribute, Integer orgId) {
		this.deviceId = deviceId;
		this.packageId = packageId;
		this.attribute = attribute;
		this.orgId = orgId;
	}
	/**
	 * @return the deviceId
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * @param deviceId the deviceId to set
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * @return the packageId
	 */
	public String getPackageId() {
		return packageId;
	}

	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the attribute
	 */
	public String getAttribute() {
		return attribute;
	}

	/**
	 * @param attribute the attribute to set
	 */
	public void setAttribute(String attribute) {
		this.attribute = attribute;
	}
}