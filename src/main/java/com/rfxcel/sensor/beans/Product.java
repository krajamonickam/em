package com.rfxcel.sensor.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author Tejshree Kachare
 * @since Dec 11 2016
 *
 */
@JsonSerialize(include= JsonSerialize.Inclusion.NON_NULL)
public class Product {

	private String productId;
	private String productType;
	private String packageId;
	
	public Product() {
	}

	/**
	 * @return the productId
	 */
	@JsonProperty("productId")
	public String getProductId() {
		return productId;
	}

	/**
	 * @param productId the productId to set
	 */
	public void setProductId(String productId) {
		this.productId = productId;
	}

	/**
	 * @return the productType
	 */
	public String getProductType() {
		return productType;
	}

	/**
	 * @param productType the productType to set
	 */
	public void setProductType(String productType) {
		this.productType = productType;
	}

	/**
	 * @return the packageId
	 */
	@JsonProperty("packageId")
	public String getPackageId() {
		return packageId;
	}

	/**
	 * @param packageId the packageId to set
	 */
	public void setPackageId(String packageId) {
		this.packageId = packageId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Product ["
				+ (productId != null ? "productId=" + productId + ", " : "")
				+ (productType != null ? "productType=" + productType + ", ": "")
				+ (packageId != null ? "packageId=" + packageId : "") + "]";
	}

}
