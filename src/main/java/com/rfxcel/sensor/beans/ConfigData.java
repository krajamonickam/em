package com.rfxcel.sensor.beans;

import java.util.ArrayList;

/**
 * 
 * @author Bosco
 * @since 2016-12-07
 */
public class ConfigData
{

	private ArrayList<String> params;

	/**
	 * 
	 * @return config param names 
	 */
	public ArrayList<String> getParams() {
		return params;
	}

	/**
	 * 
	 * @param params -  sets the parameter names of device
	 */
	public void setParams(ArrayList<String> params) {
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "ConfigData [" + (params != null ? "params=" + params : "")
				+ "]";
	}
	
	
	
		
}
