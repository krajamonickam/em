package com.rfxcel.sensor.beans;

import java.sql.Timestamp;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * 
 * @author sachin_kohale
 * @since May 05, 2017
 */
@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class DeviceLog {
	private Long id;
	private String deviceId;
	private Long userId;
	private String userName;
	private String logMsg;
	private Timestamp createdTime;
	private Integer orgId;
	
	public DeviceLog(){
		
	}
	public DeviceLog(String deviceId, String userName, String logMsg,
			Timestamp createdTime) {
		super();		
		this.deviceId = deviceId;
		this.userName = userName;
		this.logMsg = logMsg;
		this.createdTime = createdTime;
	}

	/**
	 * 
	 * @return
	 */
	public Long getId() {
		return id;
	}

	/**
	 * 
	 * @param id
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * 
	 * @return
	 */
	public String getDeviceId() {
		return deviceId;
	}

	/**
	 * 
	 * @param deviceId
	 */
	public void setDeviceId(String deviceId) {
		this.deviceId = deviceId;
	}

	/**
	 * 
	 * @return
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * 
	 * @param userId
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * 
	 * @return
	 */
	public String getLogMsg() {
		return logMsg;
	}

	/**
	 * 
	 * @param logMsg
	 */
	public void setLogMsg(String logMsg) {
		this.logMsg = logMsg;
	}

	/**
	 * 
	 * @return
	 */
	public Timestamp getCreatedTime() {
		return createdTime;
	}

	/**
	 * 
	 * @param createdTime
	 */
	public void setCreatedTime(Timestamp createdTime) {
		this.createdTime = createdTime;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}
	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	/**
	 * 
	 * @return
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * 
	 * @param userName
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

}
