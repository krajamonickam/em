package com.rfxcel.migrate;

import org.flywaydb.core.Flyway;
import org.flywaydb.core.api.MigrationInfo;
import org.flywaydb.core.api.MigrationInfoService;
import org.flywaydb.core.api.MigrationState;

public class CheckVersion {

	public static void main(String[] args) {
		if ((args.length != 3) && (args.length != 4) && (args.length != 5)) {
			System.out.println("Syntax: CheckVersion <dburl> <dbport> <dbname> <dbuserid> <dbpassword>");
			System.out.println("Example: CheckVersion rfxverizonitt root root localhost 3306 ");
			System.exit(1);
		}
		String dbName = args[0];
		String userid = args[1];
		String password = args[2];
		String dbURL = null;
		if (args.length >= 4) {
			dbURL = args[3];
		}
		String port = null;
		if (args.length >= 5) {
			port = args[4];
		}
		String url = "jdbc:mariadb://" + dbURL + ":" + port + "/" + dbName;
		System.out.println("Connecting to " + url + " with userid " + userid);
		Flyway flyway = new Flyway();
		flyway.setDataSource(url, userid, password);
		MigrationInfoService i = flyway.info();
		System.out.println("\n\n*** COMPLETED MIGRATIONS ****");
		for (MigrationInfo x : i.all()) {
			if (isCompleted(x.getState())) {
				System.out.println(x.getVersion() + " " + x.getDescription() + " - " + x.getInstalledOn());
			}
		}	
		System.out.println("\n\n*** PENDING MIGRATIONS ****");
		for (MigrationInfo x : i.all()) {
			if (!isCompleted(x.getState())) {
				System.out.println(x.getVersion() + " " + x.getDescription() + "  - " + x.getState());
			}
		}	
	}
	
	public static boolean isCompleted(MigrationState m)
	{
		if (m.equals(MigrationState.SUCCESS)) {
			return(true);
		}
		return(false);
	}

}
