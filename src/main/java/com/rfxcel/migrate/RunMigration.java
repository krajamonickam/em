package com.rfxcel.migrate;

import java.net.URL;

import org.flywaydb.core.Flyway;

public class RunMigration {

	public static void main(String[] args) {
		/*
		if ((args.length != 3) && (args.length != 4) && (args.length != 5)) {
			System.out.println("Syntax: RunMigration <dburl> <dbport> <dbname> <dbuserid> <dbpassword>");
			System.out.println("Example: RunMigration rfxverizonitt root root localhost 3306 ");
			System.exit(1);
		}
		String dbName = args[0];
		String userid = args[1];
		String password = args[2];
		String dbURL = null;
		if (args.length >= 4) {
			dbURL = args[3];
		}
		String port = null;
		if (args.length >= 5) {
			port = args[4];
		}
		String url = "jdbc:mariadb://" + dbURL + ":" + port + "/" + dbName;
		System.out.println("Connecting to " + url + " with userid " + userid);
		Flyway flyway = new Flyway();
		flyway.setDataSource(url, userid, password);
		flyway.setBaselineOnMigrate(true);
		flyway.migrate();
		*/
		String url = "jdbc:mariadb://localhost:3308/rfxem";
		
		Flyway flyway = new Flyway();
		flyway.setDataSource(url, "root", "root");
		flyway.setBaselineOnMigrate(true);
		flyway.migrate(); 
		
	}

}
