package com.rfxcel.messaging;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.AuthConstants;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.federation.FederatedGetShard;
import com.rfxcel.sensor.util.Timer;
import com.twilio.Twilio;
import com.twilio.rest.api.v2010.account.Call;
import com.twilio.rest.api.v2010.account.Message;
import com.twilio.type.PhoneNumber;

@Path("sensor")
public class VoiceService {
	private static final Logger logger = Logger.getLogger(VoiceService.class);
    public static final String ACCOUNT_SID = "AC0a2b0cda65d50d2bed474fcb955333bb";
	public static final String AUTH_TOKEN = "c8b8e3969cef7cf1f0086098f022f852";
	public static final String fromPhone = "+18318880826";
	private static VoiceService instance = null;
	private static HashMap<String, String> tokenMap = new HashMap<String, String>();
	private ConfigCache configCache = ConfigCache.getInstance();
	private static int tokenNum = 1000;
	
	public static void main(String[] args) {
	    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	    String toPhone = "+14088925501";
	    Call call;
		try {
			call = Call.creator(new PhoneNumber(toPhone), new PhoneNumber(fromPhone),
			        new URI("http://35.163.27.198/alert.xml")).create();
		    System.out.println(call.getSid());
		} catch (URISyntaxException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
	

	public static VoiceService getInstance()
	{
		if (instance == null) {
			instance = new VoiceService();
		}
		return(instance);
	}
	
	public VoiceService() {
	    Twilio.init(ACCOUNT_SID, AUTH_TOKEN);
	    tokenMap.put("0", "Voice token service is successful");
	}
	
	@GET
	@POST
	@Path("voice/{token}")
	@Produces("text/plain")
	public Response getToken(@PathParam("token")String token){
		String text = tokenMap.get(token);
		if (text == null) text = "no message found";
		StringBuilder sb = new StringBuilder();
		sb.append("<Response><Say voice=\"alice\">" + text + "</Say></Response>");
		logger.info("getToken for " + token + " message " + sb.toString());
		return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(sb.toString()).build();
	}
	
	@GET
	@Path("voicetest/{phone}")
	@Produces("text/plain")
	public Response testSendVoice(@PathParam("phone")String phone, @QueryParam("msg") String msg){
		send(phone, msg);
		return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity("OK").build();
	}

	public void send(String phoneNumber, String message) {
	    Call call;
		try {
			String myURL = configCache.getSystemConfig("application.url");
			if (myURL == null) {
				myURL = "http://127.0.0.1/#/";
			}
			String key = new Integer(tokenNum++).toString();
			tokenMap.put(key, message);
			myURL = myURL.replace("#/", "rest/sensor/voice/") + key;
			call = Call.creator(new PhoneNumber(phoneNumber), new PhoneNumber(fromPhone), new URI(myURL)).create();
		    logger.info("VoiceService - sent voice message to " + phoneNumber + " SID: " + call.getSid() + " URL: " + myURL + " message: " + message);
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
}
