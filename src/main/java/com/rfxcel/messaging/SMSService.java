package com.rfxcel.messaging;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;

import org.apache.log4j.Logger;

import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.AmazonSNSClientBuilder;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import com.amazonaws.services.sns.model.PublishRequest;
import com.amazonaws.services.sns.model.PublishResult;
import com.rfxcel.cache.ConfigCache;

public class SMSService  implements Runnable 
{
	private static final Logger logger = Logger.getLogger(SMSService.class);
	private static SMSService instance = null;
	private ConfigCache configCache = ConfigCache.getInstance();
	private AmazonSNSClient snsClient = null;
	private Map<String, MessageAttributeValue> smsAttributes = null;
	private LinkedBlockingQueue<SMSRequest> queue = new LinkedBlockingQueue<SMSRequest>();
	private Thread thread = null;
	
	public class SMSRequest
	{
		private String phoneNumber;
		private String message;
		public SMSRequest(String phoneNumber, String message) {
			super();
			this.phoneNumber = phoneNumber;
			this.message = message;
		}
		public String getPhoneNumber() {
			return phoneNumber;
		}
		public void setPhoneNumber(String phoneNumber) {
			this.phoneNumber = phoneNumber;
		}
		public String getMessage() {
			return message;
		}
		public void setMessage(String message) {
			this.message = message;
		}
		public String toString()
		{
			return("{ phoneNumber : '" + this.phoneNumber  + "', message : '" + this.message + "' }");
		}
	}

	public static SMSService getInstance()
	{
		if (instance == null) {
			instance = new SMSService();
		}
		return(instance);
	}

	public SMSService() {
		String awsAccessKey = configCache.getSystemConfig("aws.accesskey");
		String awsSecretKey = configCache.getSystemConfig("aws.secretkey");
		String awsRegion = configCache.getSystemConfig("aws.region");
		String smsType = configCache.getSystemConfig("aws.smstype");
		BasicAWSCredentials awsCreds = new BasicAWSCredentials(awsAccessKey, awsSecretKey);
		snsClient = (AmazonSNSClient) AmazonSNSClientBuilder.standard()
                .withCredentials(new AWSStaticCredentialsProvider(awsCreds))
                .withRegion(Regions.fromName(awsRegion))
                .build();
		smsAttributes = new HashMap<String, MessageAttributeValue>();
		smsAttributes.put("AWS.SNS.SMS.SMSType", new MessageAttributeValue()
				.withStringValue(smsType) 
				.withDataType("String"));
		logger.info("SMSService - initialized with keys = " + awsAccessKey + "," + awsSecretKey + " region = " + awsRegion + " smsType = " + smsType);
	}
	
	public void send(String phoneNumber, String message)
	{
		synchronized(this) {
			if (thread == null) {
				logger.info("SMSService - starting new thread....");
				thread = new Thread(this);
				thread.start();
			}
		}
		SMSRequest req = new SMSRequest(phoneNumber, message);
		this.queue.add(req);
		logger.info("SMSService - queuing request " + req.toString());
	}
	
	public boolean doSend(String phoneNumber, String message)
	{
		try {
			PublishRequest request = new PublishRequest()
										.withMessage(message).withPhoneNumber(phoneNumber)
										.withMessageAttributes(smsAttributes);
			logger.info("SMSService - processing request " + request.toString());
			PublishResult result = snsClient.publish(request);
			logger.info("SMSService - processed response = " + result.toString());
			return(true);
		} catch (Exception e) {
			logger.info("SMSService - exception = " + e.getMessage());
			return(false);
		}
	}

	@Override
	public void run() {
		try
		{
			SMSRequest req = null;
			while ((req = queue.take()) != null)
			{
				logger.info("SMSService - dequeuing request " + req.toString());
				this.doSend(req.getPhoneNumber(), req.getMessage());
			}
		}
		catch(Exception e)
		{
			logger.info("SMSService thread - exception = " + e.getMessage());
		}
		synchronized(this) { this.thread = null; }
	}
}
