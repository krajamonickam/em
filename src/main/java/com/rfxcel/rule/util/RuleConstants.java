package com.rfxcel.rule.util;

import java.util.HashMap;

import com.rfxcel.notification.util.NotificationConstant;


/**
 * 
 * @author Tejshree Kachare
 *
 */
public class RuleConstants {

	public static HashMap <String, Long> ATTRIBUTE_ALERT_MAP = new HashMap<String, Long>();
	
	static
	{
		populateAttributeAlertMap();
	}
	
	public static final String SENSOR_ATTR_TEMPERATURE = "temperature" ;
	public static final String SENSOR_ATTR_LIGHT = "light" ;
	public static final String SENSOR_ATTR_TILT = "tilt" ;
	public static final String SENSOR_ATTR_LOW_BATTERY = "lowbattery" ;
	public static final String SENSOR_ATTR_LOW_BATTERY_VOLTAGE = "lowbatteryvolt" ;
	public static final String SENSOR_ATTR_PRESSURE = "pressure" ;
	public static final String SENSOR_ATTR_HUMIDITY = "humidity" ;
	public static final String SENSOR_ATTR_CUMULATIVE_LOG = "cumulativeLog" ;
	public static final String SENSOR_ATTR_GROWTH_LOG = "growthLog" ;
	public static final String SENSOR_ATTR_BATTERY = "battery" ;
	public static final String SENSOR_ATTR_DURATION = "duration" ;
	public static final String SENSOR_ATTR_SHOCK = "shock" ;
	public static final String SENSOR_ATTR_AMBIENTTEMP = "ambientTemperature" ;
	public static final String SENSOR_ATTR_RESET = "reset" ;
	public static final String SENSOR_ATTR_DEVICE_IDENTIFIER = "deviceIdentifier";
	public static final String SENSOR_ATTR_GEOFENCE = "geofence";
	public static final String SENSOR_ATTR_TIME = "timeLong";
	public static final String SENSOR_ATTR_VIBRATION = "vibration";
	
	public static final String SENSOR_ATTR_GEOFENCE_ROUTE_ARRIVED = "geofenceEntry" ;
	public static final String SENSOR_ATTR_GEOFENCE_ROUTE_DEVIATED = "geofenceExit" ;
	public static final String SENSOR_ATTR_GEOFENCE_ROUTE_RESUMED = "geofenceOnRoute";
	
	public static final String SENSOR_ATTR_GEOFENCE_POINT_ARRIVED = "geofenceArrival" ;
	public static final String SENSOR_ATTR_GEOFENCE_POINT_DEPARTED = "geofenceDeparture";
	public static final String SENSOR_ATTR_GEOFENCE_WITHOUT_MOTION = "samelocation";
	
	
	/**
	 * 
	 */
	private static void populateAttributeAlertMap() {
		
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_TEMPERATURE, NotificationConstant.ALERT_CODE_TEMPERATURE_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_LIGHT, NotificationConstant.ALERT_CODE_LIGHT_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_TILT, NotificationConstant.ALERT_CODE_TILT_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_PRESSURE, NotificationConstant.ALERT_CODE_PRESSURE_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_HUMIDITY, NotificationConstant.ALERT_CODE_HUMIDY_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_CUMULATIVE_LOG, NotificationConstant.ALERT_CODE_CUMULATICVE_LOG_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_SHOCK, NotificationConstant.ALERT_CODE_SHOCK_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_BATTERY, NotificationConstant.ALERT_CODE_BATTERY_EXCURSION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_AMBIENTTEMP, NotificationConstant.ALERT_CODE_AMBIENT_TEMP_EXCURSION);

	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_RESET, NotificationConstant.ALERT_CODE_ATTR_RESET);
	
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GEOFENCE_ROUTE_ARRIVED, NotificationConstant.ALERT_CODE_SHIPMENT_ON_ROUTE);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GEOFENCE_ROUTE_DEVIATED, NotificationConstant.ALERT_CODE_ROUTE_DEVIATION);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GEOFENCE_ROUTE_RESUMED, NotificationConstant.ALERT_CODE_ROUTE_RESUMED);
	
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GEOFENCE_POINT_ARRIVED, NotificationConstant.ALERT_CODE_GEO_POINT_ENTRY);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GEOFENCE_POINT_DEPARTED, NotificationConstant.ALERT_CODE_GEO_POINT_EXIT);
	
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GEOFENCE_WITHOUT_MOTION, NotificationConstant.ALERT_CODE_WITHOUT_MOTION);
	
	
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_DURATION, 3050L);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_GROWTH_LOG, NotificationConstant.ALERT_CODE_GROWTH_LOG);
	ATTRIBUTE_ALERT_MAP.put(SENSOR_ATTR_VIBRATION, NotificationConstant.ALERT_CODE_VIBRATION);
	
	/*ATTRIBUTE_ALERT_MAP.put(key, 3701);
	ATTRIBUTE_ALERT_MAP.put(key, 3702);
	ATTRIBUTE_ALERT_MAP.put(GEO_FENCE_ALARM_MSG_ENTRY, 3703);
	ATTRIBUTE_ALERT_MAP.put(GEO_FENCE_ALARM_MSG_EXIT, 3704);
	ATTRIBUTE_ALERT_MAP.put(key, 3705);
	*/
		
	}
}
