package com.rfxcel.rule.vo;


/**
 * 
 * @author Tejshree Kachare
 *@since Nov 18 2016
 */
public class RuleVO {
	public static final int ALERT_OFF = 0;
	public static final int ALERT_ON = 1;
	public static final int ALERT_NOTIFICATION = 2;

	private Long id;
	private String attrName;
	private Double minValue;
	private Double maxValue;
	private Integer sensorType;
	private Integer alert;
	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the attrName
	 */
	public String getAttrName() {
		return attrName;
	}
	
	/**
	 * @param attrName the attrName to set
	 */
	public void setAttrName(String attrName) {
		this.attrName = attrName;
	}
	
	/**
	 * @return the minValue
	 */
	public Double getMinValue() {
		return minValue;
	}
	
	/**
	 * @param minValue the minValue to set
	 */
	public void setMinValue(Double minValue) {
		this.minValue = minValue;
	}
	
	/**
	 * @return the maxValue
	 */
	public Double getMaxValue() {
		return maxValue;
	}
	
	/**
	 * @param maxValue the maxValue to set
	 */
	public void setMaxValue(Double maxValue) {
		this.maxValue = maxValue;
	}

	/**
	 * @return the sensorType
	 */
	public Integer getSensorType() {
		return sensorType;
	}

	/**
	 * @param sensorType the sensorType to set
	 */
	public void setSensorType(Integer sensorType) {
		this.sensorType = sensorType;
	}

	/**
	 * @return the alert
	 */
	public Integer getAlert() {
		return alert;
	}

	/**
	 * @param alert the alert to set
	 */
	public void setAlert(Integer alert) {
		this.alert = alert;
	}


	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "RuleVO ["
				+ (attrName != null ? "attrName=" + attrName + ", " : "")
				+ (minValue != null ? "minValue=" + minValue + ", " : "")
				+ (maxValue != null ? "maxValue=" + maxValue  + "," : "")
				+ (alert != null ? "alert=" + alert : "")
				+ "]";
	}
	
}
