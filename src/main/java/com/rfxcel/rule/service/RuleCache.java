package com.rfxcel.rule.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.rfxcel.cache.AssociationCache;
import com.rfxcel.rule.dao.RuleDAO;
import com.rfxcel.rule.vo.RuleVO;
import com.rfxcel.sensor.dao.AssociationDAO;

/**
 * @author Tejshree Kachare
 * @since Nov 21 2016
 */
public class RuleCache {

	private static Logger logger = Logger.getLogger(RuleCache.class);
	private static RuleCache ruleCache;
	private static HashMap<Integer, HashMap<String, RuleVO>> cache = new HashMap<Integer, HashMap<String, RuleVO>>();

	private RuleCache() {
	}

	public static RuleCache getInstance() {
		if (ruleCache == null) {
			synchronized (RuleCache.class) {
				if (ruleCache == null) {
					ruleCache = new RuleCache();
					ruleCache.populate();
				}
			}
		}
		return ruleCache;
	}

	// Populate cache
	public void populate() {
		//Synchronize on cache
		synchronized (cache) {
			// Clear cache
			clear();
			cache.putAll(RuleDAO.getRules());
		}
	}

	/**
	 * clear all the data from ruleCache
	 */
	private static void clear() {
		cache.clear();
	}

	/**
	 * 
	 */
	public void reset(){
		populate();
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @return
	 */
	public Collection<RuleVO> getRule(String deviceId, String packageId) {
		Collection<RuleVO> rules = null;
		try {
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, packageId);
			if(profileId != null){
				HashMap<String, RuleVO> map = cache.get(profileId);
				if(map != null){
					rules = map.values();
				}
			}
		} catch (Exception e) {
			logger.error(" Exception while getting rule : " + e.getMessage());
		}
		return rules;
	}
	
	
	/**
	 * @param deviceId
	 * @param packageId
	 * @param attrName
	 * @return
	 */
	public RuleVO getRule(String deviceId, String packageId, String attrName) {
		RuleVO rule = null;
		try {
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, packageId);
			if(profileId != null){
				HashMap<String, RuleVO> map = cache.get(profileId);
				if(map != null){
					rule = map.get(attrName);
				}
			}
		} catch (Exception e) {
			logger.error(" Exception while getting rule : " + e.getMessage());
		}
		return rule;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @param attrName
	 * @return
	 */
	public RuleVO getRule(String deviceId, String packageId, String productId, String attrName) {
		RuleVO rule = null;
		try {
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, packageId);
			if(profileId == null){
				profileId = AssociationDAO.getInstance().getProfileIdForShipment (deviceId, packageId, productId);
			}
			if(profileId != null){
				HashMap<String, RuleVO> map = cache.get(profileId);
				if(map != null){
					rule = map.get(attrName);
				}
			}
		} catch (Exception e) {
			logger.error(" Exception while getting rule : " + e.getMessage());
		}
		return rule;
	}
	
	/**
	 * <p> Get threshold rules for all the attributes of device specified by deviceId
	 * @param deviceId and packageId
	 * @return
	 */
	public HashMap<String, RuleVO> getAttributeThreshold(String deviceId, String packageId, String productId){
		HashMap<String, RuleVO> rules = null;
		try {
			Integer profileId = AssociationCache.getInstance().getActiveShipmentProfileId(deviceId, packageId);
			if(profileId == null){
				profileId = AssociationDAO.getInstance().getProfileIdForShipment(deviceId, packageId, productId);
			}
			rules = cache.get(profileId);
		} catch (Exception e) {
			logger.error(" Exception while getting attribute thresholds : " + e.getMessage());
		}
		return rules;
	}

	
	/**
	 * <p> Remove rule corresponding to profileIds </p>
	 * @param profileIds
	 */
	public void removeRules(ArrayList<Integer> profileIds) {
		synchronized (cache) {
			for(Integer profileId : profileIds){
				cache.remove(profileId);
			}
		}
	}
	
	/**
	 * <p> Remove rule corresponding to profileId </p>
	 * @param profileId
	 */
	public void removeRules(Integer profileId) {
		synchronized (cache) {
			if(cache.get(profileId) != null){
				cache.remove(profileId);
			}			
		}
	}
	
}
