package com.rfxcel.rule.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.apache.log4j.Logger;
import org.joda.time.DateTime;

import com.rfxcel.notification.service.NotificationService;
import com.rfxcel.notification.util.NotificationConstant;
import com.rfxcel.notification.vo.AlertDetailsVO;
import com.rfxcel.notification.vo.NotificationVO;
import com.rfxcel.rule.util.RuleConstants;
import com.rfxcel.rule.vo.RuleVO;
import com.rfxcel.sensor.beans.DeviceInfo;
import com.rfxcel.sensor.beans.ExcursionLog;
import com.rfxcel.sensor.dao.ExcursionLogDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.Attribute;
import com.rfxcel.sensor.util.ExcursionTime;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.cache.AssociationCache;
import com.rfxcel.cache.DeviceAttributeLogCache;
import com.rfxcel.cache.DeviceCache;

/**
 * 
 * @author Tejshree Kachare
 * @since Nov 18 2016
 *
 */
public class RuleEvaluationService {

	private static Logger logger = Logger.getLogger(RuleEvaluationService.class);
	private static SendumDAO sendumDAO = SendumDAO.getInstance();
	private static DeviceAttributeLogCache deviceAttrLogCache = DeviceAttributeLogCache.getInstance();
	private static AssociationCache assoCache = AssociationCache.getInstance();
	/**
	 * 
	 * @param attributeValueMap, Incoming sensor data. In addition to map data it also contains containerId and pk of sensor_data table 
	 * @return list of attributes that caused excursion
	 */
	public static ArrayList<String> evaluateRules(Map<String, String> attributeValueMap){
		ArrayList<String> attributeList = new ArrayList<String>();
		String attrName = null;
		String deviceId = null;
		String packageId;
		try{
			//Get device identifier
			deviceId = attributeValueMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
			packageId = attributeValueMap.get("containerId");
			//Get Rules for the given device type id from cache
			Collection<RuleVO> rulesList = RuleCache.getInstance().getRule(deviceId, packageId);
			//check if rules are violated.
			//Each violated rule , raise notification
			Double attrValueDouble;
			String attrValue;
			Double minValue;
			Double maxValue;
			Boolean alert;
			Boolean notification;
			//Iterate over rules and get the attribute for which the rule is defined, check if its value is within limit 
			if(rulesList != null){
				for(RuleVO ruleVO : rulesList){
					attrName = ruleVO.getAttrName();				//Get Attribute Name
						alert = notification = false;
					if(ruleVO.getAlert() == RuleVO.ALERT_ON){
						alert = true;
						notification =true;
					}else if(ruleVO.getAlert() == RuleVO.ALERT_NOTIFICATION){
						alert = false;
						notification =true;
					}
					attrValue = attributeValueMap.get(attrName);	//Get value of attribute from sensor data map
					//Evaluate rule only if attribute value is not null 			
					if(attrValue != null && attrValue != "" ){
						attrValueDouble = Double.parseDouble(attrValue);
						minValue = ruleVO.getMinValue();
						maxValue = ruleVO.getMaxValue();
						if (minValue < maxValue)
						{
							Attribute attribute = DeviceCache.getInstance().getAttribute(deviceId, attrName);
							
							if(attribute != null){
								if( (attrValueDouble < minValue || attrValueDouble > maxValue)){
									logger.info("Device " + deviceId + " package " + packageId + " attribute " +attrName +" attribute value " +attrValueDouble + " out of range (" + minValue + "," + maxValue + ")");
									if ((attrName.equals(RuleConstants.SENSOR_ATTR_BATTERY) || attrName.equals(RuleConstants.SENSOR_ATTR_HUMIDITY))
											&& attrValueDouble > 100.0) {
										logger.info("No need to raise alert/notification for battery or humidity if the value exceeds more than 100%");
									} else {
										if(alert){
											attributeList.add(attrName);
										}
										if(notification){
											processExcursion(attributeValueMap, attribute, attrValueDouble, ruleVO, alert);
											processExcursionTime(deviceId, attrValueDouble, attribute, attributeValueMap, ruleVO, alert);
										}
									}
									
								}
								else{
									//clear attribute log, if any, maintained for notification count limit along with attribute reset notification
									cleanupAfterExcursion(deviceId,attribute,attrValueDouble, attributeValueMap, notification, ruleVO, alert);		
									cleanupExcursionTimeLog(deviceId, attrName, attributeValueMap, alert);
								}
							}
						}
					}
				}
				if(attributeList.size() >0){
					assoCache.addShipmentExcursionAttr(deviceId, packageId, attributeList);
				}
			}
		}
		catch (NumberFormatException nfe){
			logger.error("Exception while parsing attribute value for "+attrName+ " " +nfe.getMessage());
		}
		catch(Exception e){
			logger.error("Exception occured while evaluating rules for device "+deviceId+ " " +e.getMessage());
		}
		return attributeList;
	}


	/**
	 * 
	 * @param attributeValueMap
	 * @param attribute
	 * @param attrValue
	 * @param ruleVO
	 * @throws NumberFormatException
	 */
	private static void processExcursion( Map<String, String> attributeValueMap, Attribute attribute, Double attrValue, RuleVO ruleVO, boolean alert)throws NumberFormatException{
		String deviceId = attributeValueMap.get(RuleConstants.SENSOR_ATTR_DEVICE_IDENTIFIER);
		String sensorRecordId = attributeValueMap.get("sensorRecordId");
		String containerId = attributeValueMap.get("containerId");
		String productId = attributeValueMap.get("productId");
		Integer orgId = attributeValueMap.get("orgId") == null ? null : Integer.valueOf(attributeValueMap.get("orgId"));
		AlertDetailsVO alertDetailsVO;
		NotificationVO notVO;
		HashMap<String, String> attrValueMap;
		//Stop creating notification for an attribute if it reaches its notification limit
		Boolean raiseNotification = processNotificationLimit(deviceId,attribute,attrValue);
		if(raiseNotification){
			if(attribute != null && (attribute.getRoundOffTo() != null)){
				attrValue = Utility.round(attrValue, attribute.getRoundOffTo());
			}
			attrValueMap = new HashMap<String, String>();
			attrValueMap.put("<<ATTR>>",attrValue.toString());
			attrValueMap.put("<<MIN>>", ruleVO.getMinValue().toString());
			attrValueMap.put("<<MAX>>", ruleVO.getMaxValue().toString());
			Long alertCode = RuleConstants.ATTRIBUTE_ALERT_MAP.get(attribute.getName());
			if(alertCode!= null){
				alertDetailsVO = new AlertDetailsVO(alertCode, attrValueMap, orgId);
				alertDetailsVO.setAttributeName(attribute.getName());
				notVO = new NotificationVO(deviceId, containerId,productId, ruleVO.getId(), Long.valueOf(sensorRecordId) , alertDetailsVO);
				if (alert) {
					notVO.setState(NotificationVO.STATE_RED);
				}
				else {
					notVO.setState(NotificationVO.STATE_GREEN);
				}
				NotificationService.createNotification(notVO);
			}else{
				logger.error("Alert code is not configured for attribute :"+attribute.getName()+" in organization with id "+orgId);
			}

		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrValue
	 * @param attribute
	 * @param attributeValueMap
	 * @param ruleVO 
	 * @throws NumberFormatException
	 */
	private static void processExcursionTime(String deviceId, Double attrValue, Attribute attribute, Map<String, String> attributeValueMap, RuleVO ruleVO, boolean alert) throws NumberFormatException{
		String containerId = attributeValueMap.get("containerId");
		String sensorRecordId = attributeValueMap.get("sensorRecordId");
		String productId = attributeValueMap.get("productId");
		String attrName = attribute.getName();
		Integer orgId = attributeValueMap.get("orgId") == null ? null : Integer.valueOf(attributeValueMap.get("orgId"));
		DeviceInfo deviceInfo = deviceAttrLogCache.getExcursionTimeLog(deviceId, attrName);
		if(attribute!= null && attribute.getExcursionDuration() > 0 ){

			if(deviceInfo == null ) {
				deviceInfo = new DeviceInfo();
				deviceInfo.setDeviceId(deviceId);
				deviceInfo.setAttributeName(attrName);
				deviceInfo.setAttributeValue(String.valueOf(attrValue));
				deviceInfo.setStartTime(new DateTime());
				deviceInfo.setDeviceLogType(0);
				deviceInfo.setNotifyCount(0);// notification is not raised until excursion duration is not crossed
				deviceInfo.setEndTime(new DateTime());
				sendumDAO.addDeviceLog(deviceInfo);
				deviceAttrLogCache.putExcursionTimeLog(deviceId, attrName, deviceInfo);

			}else{
				deviceInfo.setEndTime(new DateTime());// set the end time to current time
				//calculate the excursion time
				ExcursionTime excursionTime = Utility.calculateExcursion(deviceInfo);
				if(excursionTime!= null && attribute.getExcursionDuration() > 0 && excursionTime.getMinutes() >= attribute.getExcursionDuration()){
					deviceInfo.setNotifyCount(deviceInfo.getNotifyCount() + 1);// since excursion duration is crossed notification count is increased
					if(deviceInfo.getNotifyCount() <= attribute.getNotifyLimit() ){// to limit the number of notification raised as per the notify limit
						logger.info("Device " + deviceId + " attribute " + attrName + " value " + attrValue + 
								" excursionDuration " + excursionTime.getMinutes() + " > " + attribute.getExcursionDuration() +
								" notifyCount " + deviceInfo.getNotifyCount() + " <= " + attribute.getNotifyLimit() + " - created notification");
						long alertCode = NotificationConstant.ALERT_CODE_3050;
						HashMap<String, String> attrValueMap = new HashMap<String, String>();
						attrValueMap.put("<<ATTR>>",attribute.getLegend());
						attrValueMap.put("<<ATTR1>>",excursionTime.getElapsedTime());
						attrValueMap.put("<<MIN>>", ruleVO.getMinValue().toString());
						attrValueMap.put("<<MAX>>", ruleVO.getMaxValue().toString());
						
						AlertDetailsVO alertDetailsVO = new AlertDetailsVO(alertCode, attrValueMap, orgId);
						alertDetailsVO.setAttributeName(attrName);
						NotificationVO notVO = new NotificationVO(deviceId, containerId,productId, null, Long.valueOf(sensorRecordId), alertDetailsVO);
						if (alert) {
							notVO.setState(NotificationVO.STATE_RED);
						}
						else {
							notVO.setState(NotificationVO.STATE_GREEN);
						}
						NotificationService.createNotification(notVO);
					}
					else {
						logger.info("Device " + deviceId + " attribute " + attrName + " value " + attrValue + 
								" excursionDuration " + excursionTime.getMinutes() + " > " + attribute.getExcursionDuration() +
								" notifyCount " + deviceInfo.getNotifyCount() + " > " + attribute.getNotifyLimit() + " - skipping notification");
					}
				}
				sendumDAO.updateDeviceLog(deviceInfo);
			}
		}
	}

	/**
	 * 
	 * @param deviceId
	 * @param attribute
	 * @param attrValue
	 * @return
	 */
	private static Boolean processNotificationLimit(String deviceId,Attribute attribute, Double attrValue){
		Boolean raiseNotification = true; // -- If notify limit is 0 , we need to raise notification so setting default to true
		String attrName = attribute.getName();
		DeviceInfo deviceInfo = deviceAttrLogCache.getNotificationLimitLog(deviceId, attrName);
		if(attribute!= null && attribute.getNotifyLimit() > 0 ){

			if(deviceInfo == null ) {
				deviceInfo = new DeviceInfo();
				deviceInfo.setDeviceId(deviceId);
				deviceInfo.setAttributeName(attrName);
				deviceInfo.setAttributeValue(String.valueOf(attrValue));
				deviceInfo.setStartTime(new DateTime());
				deviceInfo.setEndTime(new DateTime());
				deviceInfo.setDeviceLogType(1);
				deviceInfo.setNotifyCount(1);			// -- setting the notificationCount to 1
				sendumDAO.addDeviceLog(deviceInfo);
				deviceAttrLogCache.putNotificationLimitLog(deviceId, attrName, deviceInfo);
				}else{
				deviceInfo.setEndTime(new DateTime());
				deviceInfo.setNotifyCount(deviceInfo.getNotifyCount() + 1);
				deviceAttrLogCache.putNotificationLimitLog(deviceId, attrName, deviceInfo);	// updating cache with new notify count
				sendumDAO.updateDeviceLog(deviceInfo);
			}
			//If notification count reaches the max number configured for an attribute ,skip creating notification
			if(deviceInfo.getNotifyCount() > attribute.getNotifyLimit() ){
				raiseNotification = false;
			}else{
				raiseNotification = true;
			}
		}
		return raiseNotification;
	}

	/**
	 * 
	 * @param deviceId
	 * @param attribute
	 * @param attrValue
	 * @param attributeValueMap
	 * @param notification 
	 * @param ruleVO 
	 * @throws NumberFormatException
	 */
	private static void cleanupAfterExcursion(String deviceId, Attribute attribute, Double attrValue, Map<String, String> attributeValueMap, Boolean notification, RuleVO ruleVO, boolean alert) throws NumberFormatException{
		String attrName = attribute.getName();
		String containerId = attributeValueMap.get("containerId");
		String sensorRecordId = attributeValueMap.get("sensorRecordId");
		String productId = attributeValueMap.get("productId");
		Integer orgId = attributeValueMap.get("orgId") == null ? null : Integer.valueOf(attributeValueMap.get("orgId"));
		DeviceInfo deviceInfo = deviceAttrLogCache.getNotificationLimitLog(deviceId, attrName);
		if(deviceInfo != null){
			deviceInfo = new DeviceInfo();
			deviceInfo.setDeviceId(deviceId);
			deviceInfo.setDeviceLogType(1);
			deviceAttrLogCache.removeNotificationLimitLog(deviceId, attrName);
			sendumDAO.removeDeviceLog(deviceInfo, attrName);

			//Raise Attribute value reset alert to indicate attribute value has come within the normal range
			// do not crease reset alert for attribute if the flag(column : alert_on_reset) is turned off in sensor_attributes table. 0 - do not create reset alert , 1 - create reset alert 
			if (notification && (attribute!= null && attribute.getAlertOnReset()!= null && attribute.getAlertOnReset()== 1)){
				AlertDetailsVO alertDetailsVO;
				NotificationVO notVO;
				HashMap<String, String> attrValueMap;
				if(attribute != null && (attribute.getRoundOffTo() != null)){
					attrValue = Utility.round(attrValue, attribute.getRoundOffTo());
				}

				attrValueMap = new HashMap<String, String>();
				attrValueMap.put("<<ATTR>>",attribute.getLegend());
				attrValueMap.put("<<ATTR1>>",attrValue.toString());
				attrValueMap.put("<<MIN>>", ruleVO.getMinValue().toString());
				attrValueMap.put("<<MAX>>", ruleVO.getMaxValue().toString());
				alertDetailsVO = new AlertDetailsVO(NotificationConstant.ALERT_CODE_ATTR_RESET, attrValueMap, orgId);
				alertDetailsVO.setAttributeName(attrName);
				notVO = new NotificationVO(deviceId, containerId, productId, null, Long.valueOf(sensorRecordId) , alertDetailsVO);
				if (alert) {
					notVO.setState(NotificationVO.STATE_YELLOW);
				}
				else {
					notVO.setState(NotificationVO.STATE_GREEN);
				}
				NotificationService.createNotification(notVO);
			}
		}
		// clear entry related to that attribute from sensor_attribute_log table
		ExcursionLog excursionLog = new ExcursionLog(deviceId, containerId, attrName, orgId);
		ExcursionLogDAO.getInstance().removeExcursionLog(excursionLog);
		assoCache.removeShipmentExcursionAttr(deviceId, containerId, attrName);
	}

	/**
	 * 
	 * @param deviceId
	 * @param attrName
	 * @param attributeValueMap
	 */
	private static void cleanupExcursionTimeLog(String deviceId, String attrName, Map<String, String> attributeValueMap, boolean alert){
		DeviceInfo deviceInfo = deviceAttrLogCache.getExcursionTimeLog(deviceId, attrName);
		if(deviceInfo != null){
			deviceInfo = new DeviceInfo();
			deviceInfo.setDeviceId(deviceId);
			deviceInfo.setDeviceLogType(0);
			deviceAttrLogCache.removeExcursionTimeLog(deviceId, attrName);
			sendumDAO.removeDeviceLog(deviceInfo, attrName);
		}
	}
}
