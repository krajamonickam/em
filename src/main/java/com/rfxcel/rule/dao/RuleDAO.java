package com.rfxcel.rule.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.rule.vo.RuleVO;

/**
 * 
 * @author Tejshree Kachare
 * @since  Nov 21 2016
 *
 */
public class RuleDAO {

	private static Logger logger = Logger.getLogger(RuleDAO.class);
	
	/**
	 * <p> get Threshold Rules for a device Type</p>
	 * @param deviceTypeId
	 * @return
	 */
	public static List<RuleVO> getRulesForADeviceType(int deviceTypeId){
		Connection connection = null;
		List<RuleVO> rules = new ArrayList<RuleVO>();
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT  attr_name, min_value, max_value from sensor_threshold where sensor_type = ?";
			PreparedStatement prepStmt = connection.prepareStatement(query);
			prepStmt.setInt(1 , deviceTypeId);
			ResultSet rs = prepStmt.executeQuery();
			RuleVO ruleVO;
			while(rs.next()){
				ruleVO = new RuleVO();
				ruleVO.setAttrName( rs.getString("attr_name"));
				ruleVO.setMinValue(rs.getDouble("min_value"));
				ruleVO.setMaxValue(rs.getDouble("max_value"));
				rules.add(ruleVO);
			}
			rs.close();
			prepStmt.close();
	
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(" Error while getting rules for a device Type: " + e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(connection);
		}
		return rules;
	}


	/**
	 * <p> Get device Type Id using device Id </p>
	 * @param deviceId
	 * @return
	 */
	public static int getDeviceType(String deviceId){
		Connection connection = null;
		int deviceTypeId = 0;
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT device_type_id from sensor_devices where device_id = ?";
			PreparedStatement prepStmt = connection.prepareStatement(query);
			prepStmt.setString(1 , deviceId);
			ResultSet rs = prepStmt.executeQuery();
			while(rs.next()){
				deviceTypeId = rs.getInt("device_type_id");
			}
			rs.close();
			prepStmt.close();
		} catch (SQLException e) {
			e.printStackTrace();
			logger.error(" Error while getting device Type: " + e.getMessage());
		}finally{
			DAOUtility.getInstance().closeConnection(connection);
		}
		return deviceTypeId;
	}


	/**
	 * <p>
	 * This method is used to get all the rules based  on device Type
	 * </p>
	 * @return
	 */

	public static HashMap<Integer, HashMap<String, RuleVO>> getRules(){
		Connection connection = null;
		HashMap<Integer, HashMap<String, RuleVO>> rules = new HashMap<Integer, HashMap<String, RuleVO>>();
		try {
			connection = DAOUtility.getInstance().getConnection();
			String query = "SELECT id, attr_name, min_value, max_value, device_type_id, profile_id, alert from sensor_threshold ";
			PreparedStatement prepStmt = connection.prepareStatement(query);
			ResultSet rs = prepStmt.executeQuery();
			RuleVO ruleVO;
			Integer profileId;
			String attrName;
			while(rs.next()){
				ruleVO = new RuleVO();
				ruleVO.setId(rs.getLong("id"));
				attrName = rs.getString("attr_name");//.toLowerCase();
				ruleVO.setAttrName(attrName);
				ruleVO.setMinValue(rs.getDouble("min_value"));
				ruleVO.setMaxValue(rs.getDouble("max_value"));
				ruleVO.setAlert(rs.getInt("alert"));
				profileId = rs.getInt("profile_id");
				if(rules.get(profileId)!= null){
					rules.get(profileId).put(attrName,ruleVO);
				}else{
					HashMap<String, RuleVO> temp = new HashMap<String, RuleVO>();
					temp.put(attrName, ruleVO);
					rules.put(profileId, temp);
				}
			}
			rs.close();
			prepStmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting all the rules : "+ e.getMessage());
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(connection);
		}
		return rules;
	}
}
