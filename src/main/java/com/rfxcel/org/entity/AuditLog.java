package com.rfxcel.org.entity;

import java.sql.Timestamp;

/**
 * 
 * @author Tejshree Kachare
 * @since v 2.3 (6 Oct 2017)
 *  
 *
 */
public class AuditLog {

	private Long id;
	private Long userId;
	private String userName;
	private Long entityId;
	private String entityType;
	private String action;
	private String oldValue;
	private String newValue;
	private String description;
	private Integer orgId;
	private String createdOn;

	/**
	 * @param userId
	 * @param userName
	 * @param entityId
	 * @param entityType
	 * @param action
	 * @param oldValue
	 * @param newValue
	 * @param description
	 * @param createdOn
	 */
	public AuditLog(Long userId, String userName, Long entityId, String entityType, String action, String oldValue,	String newValue, String description, Integer orgId) {
		this.userId = userId;
		this.userName = userName;
		this.entityId = entityId;
		this.entityType = entityType;
		this.action = action;
		this.oldValue = oldValue;
		this.newValue = newValue;
		this.description = description;
		this.orgId = orgId;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the userName
	 */
	public String getUserName() {
		return userName;
	}

	/**
	 * @param userName the userName to set
	 */
	public void setUserName(String userName) {
		this.userName = userName;
	}

	/**
	 * @return the entityId
	 */
	public Long getEntityId() {
		return entityId;
	}

	/**
	 * @param entityId the entityId to set
	 */
	public void setEntityId(Long entityId) {
		this.entityId = entityId;
	}

	/**
	 * @return the entityType
	 */
	public String getEntityType() {
		return entityType;
	}

	/**
	 * @param entityType the entityType to set
	 */
	public void setEntityType(String entityType) {
		this.entityType = entityType;
	}

	/**
	 * @return the action
	 */
	public String getAction() {
		return action;
	}

	/**
	 * @param action the action to set
	 */
	public void setAction(String action) {
		this.action = action;
	}

	/**
	 * @return the oldValue
	 */
	public String getOldValue() {
		return oldValue;
	}

	/**
	 * @param oldValue the oldValue to set
	 */
	public void setOldValue(String oldValue) {
		this.oldValue = oldValue;
	}

	/**
	 * @return the newValue
	 */
	public String getNewValue() {
		return newValue;
	}

	/**
	 * @param newValue the newValue to set
	 */
	public void setNewValue(String newValue) {
		this.newValue = newValue;
	}

	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(String description) {
		this.description = description;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the createdTime
	 */
	public String getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdTime the createdTime to set
	 */
	public void setCreatedOn(String createdOn) {
		this.createdOn = createdOn;
	}

}
