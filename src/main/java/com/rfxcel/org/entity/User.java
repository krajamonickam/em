package com.rfxcel.org.entity;

import java.sql.Timestamp;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;



/**
 * 
 * @author Tejshree Kachare
 * @Since Apr 11, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class User {
	public static final int USERTYPE_SYSTEM = 0;
	public static final int USERTYPE_ADMIN = 1;
	public static final int USERTYPE_STANDARD = 2;
	public static final int USERTYPE_SUPPORT = 3;
	public static final String[] USERTYPETOSTRING = {"System", "Admin", "Standard", "Support" };

	private Long userId;
	private String userLogin;
	private String name;
	private String password;
	private String email;
	private String phone;
	private Timestamp lastLogin;
	private Boolean active;
	private Integer userType;
	private Integer orgId;
	private Long groupId;
	private Integer shard;
	private String userTz;
	private String token;
	private Timestamp lastPasswordChanged;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Integer tzId;
	private Integer privilege;
	
	public User() {
	}

	/**
	 * @param userId
	 * @param userLogin
	 * @param name
	 * @param password
	 * @param email
	 * @param phone
	 * @param lastLogin
	 * @param active
	 * @param userType
	 * @param orgId
	 * @param groupId
	 * @param userTz
	 * @param token
	 * @param lastPasswordChanged
	 * @param createdOn
	 * @param updatedOn
	 * @param tzId
	 */
	public User(Long userId, String email, String name, String phone, String userLogin,
			Timestamp lastLogin, Boolean active, Integer userType, int orgId,
			Long groupId, Integer shard, String userTz, Timestamp lastPasswordChanged, 
			Timestamp createdOn, Timestamp updatedOn,Integer tzId, Integer privilege) {		
		this.userId = userId;	
		this.email = email;	
		this.name = name;
		this.phone = phone;
		this.userLogin = userLogin;
		this.lastLogin = lastLogin;
		this.active = active;
		this.userType = userType;
		this.orgId = orgId;
		this.groupId = groupId;
		this.shard = shard;
		this.userTz = userTz;
		this.lastPasswordChanged = lastPasswordChanged;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;
		this.tzId = tzId;
		this.privilege = privilege;
	}

	/**
	 * @return the userId
	 */
	public Long getUserId() {
		return userId;
	}

	/**
	 * @param userId the userId to set
	 */
	public void setUserId(Long userId) {
		this.userId = userId;
	}

	/**
	 * @return the userLogin
	 */
	public String getUserLogin() {
		return userLogin;
	}

	/**
	 * @param userLogin the userLogin to set
	 */
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the password
	 */
	public String getPassword() {
		return password;
	}

	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the lastLogin
	 */
	public Timestamp getLastLogin() {
		return lastLogin;
	}

	/**
	 * @param lastLogin the lastLogin to set
	 */
	public void setLastLogin(Timestamp lastLogin) {
		this.lastLogin = lastLogin;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the userType
	 */
	public Integer getUserType() {
		return userType;
	}

	/**
	 * @param userType the userType to set
	 */
	public void setUserType(Integer userType) {
		this.userType = userType;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	public Integer getShard() {
		return shard;
	}

	public void setShard(Integer shard) {
		this.shard = shard;
	}

	/**
	 * @return the userTz
	 */
	public String getUserTz() {
		return userTz;
	}

	/**
	 * @param userTz the userTz to set
	 */
	public void setUserTz(String userTz) {
		this.userTz = userTz;
	}

	public Integer getPrivilege() {
		return privilege;
	}

	public void setPrivilege(Integer privilege) {
		this.privilege = privilege;
	}

	/**
	 * @return the token
	 */
	public String getToken() {
		return token;
	}

	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}

	/**
	 * @return the lastPasswordChanged
	 */
	public Timestamp getLastPasswordChanged() {
		return lastPasswordChanged;
	}

	/**
	 * @param lastPasswordChanged the lastPasswordChanged to set
	 */
	public void setLastPasswordChanged(Timestamp lastPasswordChanged) {
		this.lastPasswordChanged = lastPasswordChanged;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the tzId
	 */
	public Integer getTzId() {
		return tzId;
	}

	/**
	 * @param tzId the tzId to set
	 */
	public void setTzId(Integer tzId) {
		this.tzId = tzId;
	}

	public String userTypeString() {
		return(USERTYPETOSTRING[userType]);
	}

	public static String userTypeString(int userType) {
		return(USERTYPETOSTRING[userType]);
	}
	
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "User [" + (userId != null ? "userId=" + userId + ", " : "")
				+ (userLogin != null ? "userLogin=" + userLogin + ", " : "")
				+ (name != null ? "name=" + name + ", " : "") + (password != null ? "password=" + password + ", " : "")
				+ (email != null ? "email=" + email + ", " : "") + (phone != null ? "phone=" + phone + ", " : "")
				+ (lastLogin != null ? "lastLogin=" + lastLogin + ", " : "")
				+ (active != null ? "active=" + active + ", " : "")
				+ (userType != null ? "userType=" + userType + ", " : "") + "orgId=" + orgId + ", "
				+ (groupId != null ? "groupId=" + groupId + ", " : "")
				+ (userTz != null ? "userTz=" + userTz + ", " : "") + (token != null ? "token=" + token + ", " : "")
				+ (lastPasswordChanged != null ? "lastPasswordChanged=" + lastPasswordChanged + ", " : "")
				+ (createdOn != null ? "createdOn=" + createdOn + ", " : "")
				+ (updatedOn != null ? "updatedOn=" + updatedOn + ", " : "") + (tzId != null ? "tzId=" + tzId : "")
				+ "]";
	}
	
	
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
}
