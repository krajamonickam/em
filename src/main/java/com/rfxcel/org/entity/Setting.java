package com.rfxcel.org.entity;

import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_DEFAULT)
public class Setting {
	private String paramName;
	private String paramValue;
	private String displayName;
	private int active;
	private Integer orgId;

	/**
	 * 
	 */
	public Setting(){		
	}
		
	/**
	 * 
	 * @param paramName
	 * @param paramValue
	 * @param displayName
	 */
	public Setting(String paramName, String paramValue, String displayName) {
		super();
		this.paramName = paramName;
		this.paramValue = paramValue;
		this.displayName = displayName;
	}
	/**
	 * 
	 * @param paramValue
	 * @param displayName
	 */
	public Setting(String paramValue, String displayName) {
		super();
		this.paramValue = paramValue;
		this.displayName = displayName;
	}

	/**
	 * 
	 * @return
	 */
	public String getParamName() {
		return paramName;
	}
	/**
	 * 
	 * @param paramName
	 */
	public void setParamName(String paramName) {
		this.paramName = paramName;
	}
	/**
	 * 
	 * @return
	 */
	public String getParamValue() {
		return paramValue;
	}
	
	/**
	 * 
	 * @param paramValue
	 */
	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}
	
	/**
	 * 
	 * @return
	 */
	public String getDisplayName() {
		return displayName;
	}
	
	/**
	 * 
	 * @param displayName
	 */
	public void setDisplayName(String displayName) {
		this.displayName = displayName;
	}
	
	/**
	 * 
	 * @return
	 */
	public int getActive() {
		return active;
	}
	
	/**
	 * 
	 * @param active
	 */
	public void setActive(int active) {
		this.active = active;
	}
	
	/**
	 * 
	 * @return
	 */
	public Integer getOrgId() {
		return orgId;
	}
	
	/**
	 * 
	 * @param orgId
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}	
	

}
