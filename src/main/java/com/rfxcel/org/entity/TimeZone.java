package com.rfxcel.org.entity;

import org.codehaus.jackson.map.annotate.JsonSerialize;

/**
 * @author Albeena May 26, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class TimeZone {

	private Integer Id;
	private String timeZoneName;
	private String timeZoneCode;
	private String timeZoneOffset;
	

	/**
	 * @param id
	 * @param timeZoneName
	 * @param timeZoneCode
	 * @param timeZoneOffset
	 */
	public TimeZone(Integer id, String timeZoneName, String timeZoneCode, String timeZoneOffset) {
		this.Id = id;
		this.timeZoneName = timeZoneName;
		this.timeZoneCode = timeZoneCode;
		this.timeZoneOffset = timeZoneOffset;
	}

	/**
	 * @return the id
	 */
	public Integer getId() {
		return Id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Integer id) {
		Id = id;
	}

	/**
	 * @return the timeZoneName
	 */
	public String getTimeZoneName() {
		return timeZoneName;
	}

	/**
	 * @param timeZoneName the timeZoneName to set
	 */
	public void setTimeZoneName(String timeZoneName) {
		this.timeZoneName = timeZoneName;
	}

	/**
	 * @return the timeZoneCode
	 */
	public String getTimeZoneCode() {
		return timeZoneCode;
	}

	/**
	 * @param timeZoneCode the timeZoneCode to set
	 */
	public void setTimeZoneCode(String timeZoneCode) {
		this.timeZoneCode = timeZoneCode;
	}

	/**
	 * @return the timeZoneOffset
	 */
	public String getTimeZoneOffset() {
		return timeZoneOffset;
	}

	/**
	 * @param timeZoneOffset the timeZoneOffset to set
	 */
	public void setTimeZoneOffset(String timeZoneOffset) {
		this.timeZoneOffset = timeZoneOffset;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "USTimeZone [" + (Id != null ? "Id=" + Id + ", " : "")
				+ (timeZoneName != null ? "timeZoneName=" + timeZoneName + ", " : "")
				+ (timeZoneCode != null ? "timeZoneCode=" + timeZoneCode + ", " : "")
				+ (timeZoneOffset != null ? "timeZoneOffset=" + timeZoneOffset : "") + "]";
	}
	

}
