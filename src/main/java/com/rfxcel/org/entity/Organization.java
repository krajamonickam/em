package com.rfxcel.org.entity;

import java.sql.Timestamp;

import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize.Inclusion;


/**(
 * 
 * @author Tejshree Kachare
 * @since April 10, 2017
 */
public class Organization {

	
	
	
	/**
	 * @param extId
	 * @param name
	 * @param shortName
	 * @param address1
	 * @param address2
	 * @param city
	 * @param stateCode
	 * @param stateName
	 * @param postalCode
	 * @param country
	 * @param phone
	 * @param email
	 * @param active
	 * @param createdOn
	 * @param updatedOn
	 * @param orgId
	 * @param orgTz
	 * @param tzId
	 */
	public Organization(Integer orgId,String name, String shortName, String address1,	String address2, String city,
			String stateCode, String stateName,	String postalCode, String country, String phone,
			String email, String extId, Boolean active, Timestamp createdOn, Timestamp updatedOn,String orgTz,Integer tzId) {
		this.orgId=orgId;
		this.name = name;
		this.shortName = shortName;
		this.address1 = address1;
		this.address2 = address2;
		this.city = city;
		this.stateCode = stateCode;
		this.stateName = stateName;
		this.postalCode = postalCode;
		this.country = country;
		this.phone = phone;
		this.email = email;
		this.extId = extId;
		this.active = active;
		this.createdOn = createdOn;
		this.updatedOn = updatedOn;		
		this.orgTz = orgTz;
		this.tzId = tzId;
	}

	public Organization(String name){
		this.name = name;
	}

	private String extId;
	private String  name;
	private String shortName;
	private String address1;
	private String address2;
	private String city;
	private String stateCode;
	private String stateName;
	private String postalCode;
	private String country;
	private String phone;
	private String email;
	private Boolean active;
	private Timestamp createdOn;
	private Timestamp updatedOn;
	private Integer orgId;
	private String orgTz;
	private Integer tzId;

	public Organization(){
	}

	/**
	 * @return the extId
	 */
	public String getExtId() {
		return extId;
	}

	/**
	 * @param extId the extId to set
	 */
	public void setExtId(String extId) {
		this.extId = extId;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the shortName
	 */
	public String getShortName() {
		return shortName;
	}

	/**
	 * @param shortName the shortName to set
	 */
	public void setShortName(String shortName) {
		this.shortName = shortName;
	}

	/**
	 * @return the address1
	 */
	public String getAddress1() {
		return address1;
	}

	/**
	 * @param address1 the address1 to set
	 */
	public void setAddress1(String address1) {
		this.address1 = address1;
	}

	/**
	 * @return the address2
	 */
	public String getAddress2() {
		return address2;
	}

	/**
	 * @param address2 the address2 to set
	 */
	public void setAddress2(String address2) {
		this.address2 = address2;
	}

	/**
	 * @return the city
	 */
	public String getCity() {
		return city;
	}

	/**
	 * @param city the city to set
	 */
	public void setCity(String city) {
		this.city = city;
	}

	/**
	 * @return the stateCode
	 */
	public String getStateCode() {
		return stateCode;
	}

	/**
	 * @param stateCode the stateCode to set
	 */
	public void setStateCode(String stateCode) {
		this.stateCode = stateCode;
	}

	/**
	 * @return the stateName
	 */
	public String getStateName() {
		return stateName;
	}

	/**
	 * @param stateName the stateName to set
	 */
	public void setStateName(String stateName) {
		this.stateName = stateName;
	}

	/**
	 * @return the postalCode
	 */
	public String getPostalCode() {
		return postalCode;
	}

	/**
	 * @param postalCode the postalCode to set
	 */
	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	/**
	 * @return the country
	 */
	public String getCountry() {
		return country;
	}

	/**
	 * @param country the country to set
	 */
	public void setCountry(String country) {
		this.country = country;
	}

	/**
	 * @return the phone
	 */
	public String getPhone() {
		return phone;
	}

	/**
	 * @param phone the phone to set
	 */
	public void setPhone(String phone) {
		this.phone = phone;
	}

	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}

	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}

	/**
	 * @return the active
	 */
	public Boolean getActive() {
		return active;
	}

	/**
	 * @param active the active to set
	 */
	public void setActive(Boolean active) {
		this.active = active;
	}

	/**
	 * @return the createdOn
	 */
	public Timestamp getCreatedOn() {
		return createdOn;
	}

	/**
	 * @param createdOn the createdOn to set
	 */
	public void setCreatedOn(Timestamp createdOn) {
		this.createdOn = createdOn;
	}

	/**
	 * @return the updatedOn
	 */
	public Timestamp getUpdatedOn() {
		return updatedOn;
	}

	/**
	 * @param updatedOn the updatedOn to set
	 */
	public void setUpdatedOn(Timestamp updatedOn) {
		this.updatedOn = updatedOn;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the orgTz
	 */
	public String getOrgTz() {
		return orgTz;
	}

	/**
	 * @param orgTz the orgTz to set
	 */
	public void setOrgTz(String orgTz) {
		this.orgTz = orgTz;
	}

	/**
	 * @return the tzId
	 */
	public Integer getTzId() {
		return tzId;
	}

	/**
	 * @param tzId the tzId to set
	 */
	public void setTzId(Integer tzId) {
		this.tzId = tzId;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "Organization [" + (extId != null ? "extId=" + extId + ", " : "")
				+ (name != null ? "name=" + name + ", " : "")
				+ (shortName != null ? "shortName=" + shortName + ", " : "")
				+ (address1 != null ? "address1=" + address1 + ", " : "")
				+ (address2 != null ? "address2=" + address2 + ", " : "") + (city != null ? "city=" + city + ", " : "")
				+ (stateCode != null ? "stateCode=" + stateCode + ", " : "")
				+ (stateName != null ? "stateName=" + stateName + ", " : "")
				+ (postalCode != null ? "postalCode=" + postalCode + ", " : "")
				+ (country != null ? "country=" + country + ", " : "") + (phone != null ? "phone=" + phone + ", " : "")
				+ (email != null ? "email=" + email + ", " : "") + (active != null ? "active=" + active + ", " : "")
				+ (createdOn != null ? "createdOn=" + createdOn + ", " : "")
				+ (updatedOn != null ? "updatedOn=" + updatedOn + ", " : "")
				+ (orgId != null ? "orgId=" + orgId + ", " : "") + (orgTz != null ? "orgTz=" + orgTz + ", " : "")
				+ (tzId != null ? "tzId=" + tzId : "") + "]";
	}

	/**
	 * @return
	 */
	public String toJson() {
		String jsonInString = null;
		try {
			ObjectMapper mapper = new ObjectMapper();
			mapper.setSerializationInclusion(Inclusion.NON_NULL);
			jsonInString = mapper.writeValueAsString(this);
		}catch (Exception e) {
			e.printStackTrace();
		} 
		return jsonInString;
	}
	
}
