package com.rfxcel.org.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.notification.dao.AlertDAO;
import com.rfxcel.notification.dao.AlertGroupDAO;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.notification.entity.Alert;
import com.rfxcel.org.entity.Organization;
import com.rfxcel.org.entity.TimeZone;
import com.rfxcel.sensor.dao.AssociationDAO;
import com.rfxcel.sensor.dao.DeviceLogDAO;
import com.rfxcel.sensor.dao.DeviceTypeDAO;
import com.rfxcel.sensor.dao.ExcursionLogDAO;
import com.rfxcel.sensor.dao.ProfileDAO;
import com.rfxcel.sensor.dao.SendumDAO;



/**
 * 
 * @author Tejshree Kachare
 * @since April 11, 2017
 *
 */
public class OrganizationDAO {

	private static final Logger logger = Logger.getLogger(OrganizationDAO.class);
	SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static OrganizationDAO singleton;
	
	public static OrganizationDAO getInstance(){
		if(singleton == null){
			synchronized (OrganizationDAO.class) {
				if(singleton == null){
					singleton = new OrganizationDAO();
				}
			}
		}
		return singleton;
	}
	
	
	/**
	 * 
	 * @param org
	 * @throws SQLException
	 */
	public Integer addOrganization(Organization org) throws SQLException{
		 Integer orgId = null ;
		try{
			Object i = DAOUtility.runInTransaction(conn->{
				Integer id = addOrganization(conn, org);
				//adding defaultAlerts
				addDefaultAlerts(conn,id);
				//adding default deviceTypes

				DeviceTypeDAO.getInstance().addDefaultDeviceType(conn, id);				
				//adding default orgSetting
				ConfigDAO.getInstance().addDefaultOrgSetting(conn,id);
							
				return id;
			});
			
			orgId = i!= null ? Integer.valueOf(i.toString()):null;
			 return orgId;
		}finally{
		}
	}
				
	/**
	 * 
	 * @param conn
	 * @param org
	 * @return
	 * @throws SQLException
	 */
	public Integer addOrganization(Connection conn , Organization org) throws SQLException{
		Integer id = null;
		try{
			String sqlQury ="INSERT INTO rfx_org(org_name, addr_1, addr_2, city, state_code,state_name, postal_code, phone, email, org_extid, short_name, country,org_tz,tz_id) values (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQury, Statement.RETURN_GENERATED_KEYS);
			stmt.setString(1,org.getName());
			
			if(org.getAddress1() != null){
				stmt.setString(2, org.getAddress1());
			}else{
				stmt.setNull(2, Types.NULL);
			}
			
			if(org.getAddress2() != null){
				stmt.setString(3, org.getAddress2());
			}else{
				stmt.setNull(3, Types.NULL);
			}
			
			if(org.getCity() != null){
				stmt.setString(4, org.getCity());
			}else{
				stmt.setNull(4, Types.NULL);
			}
			
			if(org.getStateCode() != null){
				stmt.setString(5, org.getStateCode());
			}else{
				stmt.setNull(5, Types.NULL);
			}
			
			if(org.getStateName()!= null){
				stmt.setString(6, org.getStateName());
			}else{
				stmt.setNull(6, Types.NULL);
			}
			
			if(org.getPostalCode() != null){
				stmt.setString(7, org.getPostalCode());
			}else{
				stmt.setNull(7, Types.NULL);
			}
			
			if(org.getPhone() != null){
				stmt.setString(8, org.getPhone());
			}else{
				stmt.setNull(8, Types.NULL);
			}
			
			if(org.getEmail() != null){
				stmt.setString(9, org.getEmail());
			}else{
				stmt.setNull(9, Types.NULL);
			}
			
			if(org.getExtId() != null){
				stmt.setString(10, org.getExtId());
			}else{
				stmt.setNull(10, Types.NULL);
			}
			
			if(org.getShortName() != null){
				stmt.setString(11, org.getShortName());
			}else{
				stmt.setNull(11, Types.NULL);
			}
		
			if(org.getCountry() != null){
				stmt.setString(12, org.getCountry());
			}else{
				stmt.setNull(12, Types.NULL);
			}
			
			if(org.getTzId() != null && !"".equals(org.getTzId())){			
				TimeZone ut = getTimeZoneById(org.getTzId());
				stmt.setString(13,ut.getTimeZoneOffset());
				stmt.setInt(14,org.getTzId());
			}else{
				stmt.setNull(13, Types.NULL);
				stmt.setNull(14, Types.NULL);
			}
			
//			if(org.getTzId() != null){
//				stmt.setInt(14,org.getTzId());
//			}else{
//				stmt.setNull(14, Types.NULL);
//			}
			
			stmt.executeUpdate();
			ResultSet rs=stmt.getGeneratedKeys();
			if (rs.next()) {
				id = rs.getInt(1);
			}
			rs.close();
			stmt.close();
		}finally{

		}

		return id;
	}

	public List<Organization> getAllOrganizations() throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			ArrayList<Organization> attrList = new ArrayList<Organization>();
			String searchQuery;
			Statement stmt = conn.createStatement();
			searchQuery = "SELECT org_id, org_name,short_name, addr_1, addr_2, city, state_code, state_name, postal_code, country, phone, email, org_extid, active, created_time, updated_time,org_tz,tz_id FROM rfx_org";
			ResultSet  rs = stmt.executeQuery(searchQuery);
			Organization org;
			while (rs.next()) {
				org = new Organization(rs.getInt("org_id"), rs.getString("org_name"), rs.getString("short_name"), rs.getString("addr_1"), rs.getString("addr_2"), rs.getString("city"),
						rs.getString("state_code"), rs.getString("state_name"), rs.getString("postal_code"), rs.getString("country"), rs.getString("phone"),
						rs.getString("email"),rs.getString("org_extid"), rs.getBoolean("active"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getString("org_tz"),rs.getInt("tz_id"));
				attrList.add(org);
			}
			rs.close();
			stmt.close();
			return attrList;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}


	/**
	 * 
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public Organization getOrganization(Long groupId) throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			Integer  orgId = getOrgForGroupId(groupId);
			Organization org = null;
			if(orgId !=null){
				String searchQuery;
				searchQuery = "SELECT org_id, org_name,short_name, addr_1, addr_2, city, state_code, state_name, postal_code, country, phone, email, org_extid, active, created_time, updated_time,org_tz,tz_id FROM rfx_org where org_id = ?";
				PreparedStatement stmt = conn.prepareStatement(searchQuery);
				stmt.setLong(1, orgId);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					org = new Organization(rs.getInt("org_id"), rs.getString("org_name"), rs.getString("short_name"), rs.getString("addr_1"), rs.getString("addr_2"), rs.getString("city"),
							rs.getString("state_code"), rs.getString("state_name"), rs.getString("postal_code"), rs.getString("country"), rs.getString("phone"),
							rs.getString("email"),rs.getString("org_extid"), rs.getBoolean("active"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getString("org_tz"),rs.getInt("tz_id"));
				}
				rs.close();
				stmt.close();
			}
			return org;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public Organization getOrganizationById(Integer orgId) throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			Organization org = null;
			if(orgId !=null){
				String searchQuery;
				searchQuery = "SELECT org_id, org_name,short_name, addr_1, addr_2, city, state_code, state_name, postal_code, country, phone, email, org_extid, active, created_time, updated_time,org_tz,tz_id FROM rfx_org where org_id = ?";
				PreparedStatement stmt = conn.prepareStatement(searchQuery);
				stmt.setLong(1, orgId);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					org = new Organization(rs.getInt("org_id"), rs.getString("org_name"), rs.getString("short_name"), rs.getString("addr_1"), rs.getString("addr_2"), rs.getString("city"),
							rs.getString("state_code"), rs.getString("state_name"), rs.getString("postal_code"), rs.getString("country"), rs.getString("phone"),
							rs.getString("email"),rs.getString("org_extid"), rs.getBoolean("active"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getString("org_tz"),rs.getInt("tz_id"));
				}
				rs.close();
				stmt.close();
			}
			return org;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	
	
	/**
	 * 
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public Integer getOrgForGroupId(long groupId) throws SQLException{
		Connection conn = null;
		Integer orgId = null ;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String searchQuery;
			searchQuery = "SELECT org_id FROM rfx_users where group_id = ? Limit 1";
			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setLong(1, groupId);
			ResultSet  rs = stmt.executeQuery();
			while (rs.next()) {
				orgId = rs.getInt("org_id");
			}
			rs.close();
			stmt.close();
			return orgId;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param org
	 * @throws SQLException
	 */
	public void updateOrganization(Organization org) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQury ="UPDATE rfx_org SET org_name = ?, short_name = ?, addr_1 = ?, addr_2 = ?, city = ?, state_code = ?, state_name = ?, postal_code = ?, phone = ?, email = ?, country = ?, updated_time=NOW(),org_tz = ?,tz_id = ? where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(sqlQury);
			stmt.setString(1,org.getName());
			
			if(org.getShortName() != null){
				stmt.setString(2, org.getShortName());
			}else{
				stmt.setNull(2, Types.NULL);
			}
			
			if(org.getAddress1() != null){
				stmt.setString(3, org.getAddress1());
			}else{
				stmt.setNull(3, Types.NULL);
			}
			
			if(org.getAddress2() != null){
				stmt.setString(4, org.getAddress2());
			}else{
				stmt.setNull(4, Types.NULL);
			}
			
			if(org.getCity() != null){
				stmt.setString(5, org.getCity());
			}else{
				stmt.setNull(5, Types.NULL);
			}
			
			if(org.getStateCode() != null){
				stmt.setString(6, org.getStateCode());
			}else{
				stmt.setNull(6, Types.NULL);
			}
			
			if(org.getStateName()!= null){
				stmt.setString(7, org.getStateName());
			}else{
				stmt.setNull(7, Types.NULL);
			}
			
			if(org.getPostalCode() != null){
				stmt.setString(8, org.getPostalCode());
			}else{
				stmt.setNull(8, Types.NULL);
			}
			
			if(org.getPhone() != null){
				stmt.setString(9, org.getPhone());
			}else{
				stmt.setNull(9, Types.NULL);
			}
			
			if(org.getEmail() != null){
				stmt.setString(10, org.getEmail());
			}else{
				stmt.setNull(10, Types.NULL);
			}
			
			if(org.getCountry() != null){
				stmt.setString(11, org.getCountry());
			}else{
				stmt.setNull(11, Types.NULL);
			}	
			if(org.getTzId() != null && !"".equals(org.getTzId())){
				TimeZone ut = getTimeZoneById(org.getTzId());
				stmt.setString(12, ut.getTimeZoneOffset());
				stmt.setInt(13, org.getTzId());
			}else{
				stmt.setNull(12, Types.NULL);
				stmt.setNull(13, Types.NULL);
			}
//			if(org.getTzId() != null){
//				stmt.setInt(13, org.getTzId());
//			}else{
//				stmt.setNull(13, Types.NULL);
//			}
			stmt.setInt(14, org.getOrgId());			
			
			stmt.executeUpdate();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteOrganization(int orgId) throws SQLException{
		try{
			DAOUtility.runInTransaction(conn->{
				String sqlQury ="UPDATE rfx_org SET active = 0 where org_id = ?";
				PreparedStatement stmt = conn.prepareStatement(sqlQury);
				stmt.setInt(1, orgId);
				stmt.executeUpdate();
				stmt.close();
				 
				// Deactivate users and cleanup user cache
				UserDAO.getInstance().deactivateOrgUsers(conn,orgId);
				// Deactivate DeviceTypes
				DeviceTypeDAO.getInstance().deactivateOrgDeviceTypes(conn,orgId);
				// Deactivate profiles for org
				ProfileDAO.getInstance().deactivateProfilesForOrg(conn,orgId);
				// Deactivate devices
				SendumDAO.getInstance().deactivateDevicesForOrg(conn,orgId);
				// delete package for an organization
				SendumDAO.getInstance().deletePackagesForOrg(conn,orgId);
				// update associate table
				AssociationDAO.getInstance().DisassociateShipmentsForOrg(conn,orgId);
				//Deactivate org related configuration
				ConfigDAO.getInstance().deactivateOrgConfigForOrg(conn, orgId);
				// remove org related configuration from cache
				ConfigCache.getInstance().removeOrgConfig(orgId);
				
				AlertGroupDAO.getInstance().deactivateAlertGroupsForOrg(conn, orgId);
				AlertDAO.getInstance().deleteAlertsByOrg(conn,orgId);
				AlertDAO.getInstance().deleteProfileAlerts(conn,orgId);
				
				ExcursionLogDAO.getInstance().deleteExcursionLogForOrg(conn,orgId);
				DeviceLogDAO.getInstance().deleteDeviceLogForOrg(conn, orgId);
				
				return null;
			});
		}finally{
		}
	}
	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Organization getOrganizationByOrgId(Integer orgId) throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();			
			Organization org = null;			
			String query = "SELECT org_id,org_name,short_name, addr_1, addr_2, city, state_code, state_name, postal_code, country, phone, email, org_extid, active, created_time, updated_time,org_tz,tz_id FROM rfx_org where org_id = ?";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setInt(1, orgId);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					org = new Organization(orgId, rs.getString("org_name"), rs.getString("short_name"), rs.getString("addr_1"), rs.getString("addr_2"), rs.getString("city"),
							rs.getString("state_code"), rs.getString("state_name"), rs.getString("postal_code"), rs.getString("country"), rs.getString("phone"),
							rs.getString("email"),rs.getString("org_extid"), rs.getBoolean("active"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getString("org_tz"),rs.getInt("tz_id"));
				}
				rs.close();
				stmt.close();			
			return org;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	/**
	 * 
	 * @param orgName
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfOrganizationIsPresent(String orgExtId) throws SQLException{
		Connection conn= null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_org WHERE org_extid = ? and active=1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, orgExtId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * 
	 * @param orgName
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfOrganizationIsPresentByOrgName(String orgName) throws SQLException{
		Connection conn= null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_org WHERE org_name = ? and active=1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, orgName);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * <p>It checks if orgnization is present with same org name except current org id</p>
	 * @param orgName
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfOrganizationIsPresentByOrgName(String orgName, Integer orgId) throws SQLException{
		Connection conn= null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_org WHERE org_name = ? and org_id <> ? and active=1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, orgName);
			stmt.setInt(2, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public Organization getActiveOrganizationByOrgId(Integer orgId) throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();			
			Organization org = null;			
			String query = "SELECT org_name FROM rfx_org where org_id = ? and active=1";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setInt(1, orgId);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					org = new Organization(rs.getString("org_name"));
				}
				rs.close();
				stmt.close();			
			return org;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param orgName
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfOrganizationIsPresent(int orgId) throws SQLException{
		Connection conn= null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_org WHERE org_id = ? and active=1)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}
	
	
	public Boolean checkIfOrganizationIsActive(int orgId) throws SQLException{
		Connection conn= null;
		Boolean isActive= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT active from rfx_org WHERE org_id = ? ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				isActive = rs.getBoolean("active");
			}
			rs.close();
			stmt.close();
			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isActive;
	}
	
	
	/**
	 * <p>This is used to add default alert e.g for geofence and same location alert. Initially, it is added without any alertGroupId.</p>
	 * @param alertList
	 * @return
	 * @throws SQLException 
	 */
	public void addDefaultAlerts(Connection conn, Integer orgId) throws SQLException{
			ArrayList<Alert> alertList = getDefaultAlertList(conn);					
			AlertDAO.getInstance().addAlerts(conn, alertList, orgId);			
	}
	
	/**
	 * <p>get default alert list</p>
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<Alert> getDefaultAlertList(Connection conn) throws SQLException{
		ArrayList<Alert> alertList = new ArrayList<Alert>();
			String sqlQuery = "SELECT alert_code, alert_msg, alert_detail_msg FROM sensor_default_alerts WHERE is_default=1";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				Alert alert = new Alert();
				alert.setAlertCode(rs.getLong("alert_code"));
				alert.setAlertMsg(rs.getString("alert_msg"));
				alert.setAlertDetailMsg(rs.getString("alert_detail_msg"));
				alertList.add(alert);
			}
			rs.close();
			stmt.close();
		return alertList;
	}
	
	
	public List<TimeZone> getTimeZones() throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			ArrayList<TimeZone> zoneList = new ArrayList<TimeZone>();
			Statement stmt = conn.createStatement();
			String query = "SELECT id,timezone_name, timezone_code,timezone_offset FROM rfx_timezone ORDER BY timezone_code;";
			ResultSet  rs = stmt.executeQuery(query);
			TimeZone utz;
			while (rs.next()) {
				utz = new TimeZone(rs.getInt("id"),rs.getString("timezone_name"), rs.getString("timezone_code"), rs.getString("timezone_offset"));
				zoneList.add(utz);
			}
			rs.close();
			stmt.close();
			return zoneList;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	/**
	 * 
	 * @param timeZoneCode
	 * @return
	 * @throws SQLException
	 */
	public TimeZone getTimeZoneById(Integer id) throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();			
			TimeZone utz = null;			
			String query = "SELECT id,timezone_name, timezone_code,timezone_offset FROM rfx_timezone WHERE id=?";
				PreparedStatement stmt = conn.prepareStatement(query);
				stmt.setInt(1, id);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					utz = new TimeZone(rs.getInt("id"),rs.getString("timezone_name"), rs.getString("timezone_code"), rs.getString("timezone_offset"));
				}
				rs.close();
				stmt.close();			
			return utz;
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * <p>Return list of active orgIds </p>
	 * @return
	 */
	public ArrayList<Integer> getActiveOrgIds() {
		Connection conn = null;
		ArrayList<Integer> orgIds = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			orgIds = new ArrayList<Integer>();
			String query = "SELECT org_id from rfx_org WHERE active=1";
				PreparedStatement stmt = conn.prepareStatement(query);
				ResultSet  rs = stmt.executeQuery();
				while (rs.next()) {
					orgIds.add(rs.getInt("org_id"));
				}
				rs.close();
				stmt.close();	
		} catch (SQLException e) {
			logger.error("Error while gettting active orgIds "+e.getMessage());
			e.printStackTrace();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return orgIds;
	}
}
