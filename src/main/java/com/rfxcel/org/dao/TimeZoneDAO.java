package com.rfxcel.org.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.notification.dao.DAOUtility;

public class TimeZoneDAO {

	
	private static final Logger logger = Logger.getLogger(TimeZoneDAO.class);
	private static TimeZoneDAO timeZoneDAO;
	private static ConfigCache propcache = ConfigCache.getInstance();
	
	public static TimeZoneDAO getInstance(){
		if(timeZoneDAO == null){
			timeZoneDAO = new TimeZoneDAO();
		}
		return timeZoneDAO;
	}
	
	
	/**
	 * 
	 * @param id
	 * @return
	 */
	public String getTimeZoneCode(Integer id){
		Connection conn= null;
		String timeZoneCode = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT timezone_code from rfx_timezone where id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, id);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				timeZoneCode = rs.getString("timezone_code");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting timeZone Code for TimeZone id "+ id);
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return timeZoneCode;
	
		
		
	}
}
