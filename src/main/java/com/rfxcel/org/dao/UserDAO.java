package com.rfxcel.org.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

import com.google.common.collect.ArrayListMultimap;
import com.google.common.collect.ListMultimap;
import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.org.entity.Organization;
import com.rfxcel.org.entity.TimeZone;
import com.rfxcel.org.entity.User;


/**
 * 
 * @author Tejshree Kachare
 * @since Apr 11, 2017
 *
 */
public class UserDAO {
	private static final Logger logger = Logger.getLogger(UserDAO.class);	
	private static UserDAO singleton;
	
	public static UserDAO getInstance(){
		if(singleton == null){
			synchronized (UserDAO.class) {
				if(singleton == null){
					singleton = new UserDAO();
				}
			}
		}
		return singleton;
	}


	/**
	 * 
	 * @param userList
	 * @return 
	 * @throws SQLException
	 */
	public User addUser(User user) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query ="INSERT INTO rfx_users(user_id,user_login, user_name, user_email, user_type, phone, org_id, group_id, shard, user_tz,tz_id) values (?,?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			Long userId = System.currentTimeMillis();

			stmt.setLong(1,userId);
			stmt.setString(2,user.getUserLogin());
			String name = user.getName();
			if(name != null){
				stmt.setString(3, name);
			}else{
				stmt.setNull(3, Types.NULL);
			}
			String email = user.getEmail();		//TODO remove null check once email is made mandatory from UI
			if(email != null){
				stmt.setString(4, email);
			}else{
				stmt.setNull(4, Types.NULL);
			}
			Integer userType = user.getUserType();
			if(userType != null){
				stmt.setInt(5, userType);
			}else{
				stmt.setInt(5,2);		// COMMENT '0-System, 1-Admin, 2-Normal',
			}
			stmt.setString(6, user.getPhone());
			stmt.setInt(7, user.getOrgId());
			//AR - stmt.setLong(8,(userType != null && userType == 1) ? userId : user.getGroupId());
			//AR - Please do NOT change the following code without discussing with me
			stmt.setLong(8, user.getOrgId());
			//AR - end change
			stmt.setInt(9, user.getShard());
			
			if(user.getTzId() != null){
				TimeZone timeZone = OrganizationDAO.getInstance().getTimeZoneById(user.getTzId());
				if(timeZone != null){
					 stmt.setString(10,timeZone.getTimeZoneOffset());
					 stmt.setInt(11,user.getTzId());
					 user.setUserTz(timeZone.getTimeZoneOffset());
				}
			}else{
				Organization org = OrganizationDAO.getInstance().getOrganizationByOrgId(user.getOrgId());
				if(org != null){
					user.setUserTz(org.getOrgTz());
					stmt.setString(10,user.getUserTz());
					user.setTzId(org.getTzId());
					stmt.setInt(11,user.getTzId());
				}
			}
			
			stmt.executeUpdate();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return user;
	}


	/**
	 * 
	 * @param userLogin
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfUserLoginIsPresent(String userLogin) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_users WHERE user_login = ? AND active = 1 )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userLogin);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();

		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}


	/**
	 * 
	 * @return userList of only active users
	 * @throws SQLException 
	 */
	public List<User> getUserList(Integer orgId) throws SQLException {
		Connection conn = null;
		List<User> userList = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_id, user_email, user_name, phone, user_login, last_login, active, user_type, org_id, group_id, shard, user_tz, last_passwd_changed, created_time, updated_time, tz_id, privilege FROM rfx_users "
						+ "WHERE org_id = ? AND active = 1 AND user_type != 0";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			userList =  new ArrayList<User>();
			User user = null;
			while(rs.next()){
				user = new User(rs.getLong("user_id"), rs.getString("user_email"), rs.getString("user_name"), rs.getString("phone"), rs.getString("user_login"), rs.getTimestamp("last_login"), rs.getBoolean("active"), rs.getInt("user_type"), rs.getInt("org_id"),  rs.getLong("group_id"), 
						rs.getInt("shard"), rs.getString("user_tz"), rs.getTimestamp("last_passwd_changed"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getInt("tz_id"),
						rs.getInt("privilege"));
				userList.add(user);
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return userList;
	}

	/**
	 * 
	 * @return usergroupList of only active users
	 * @throws SQLException 
	 */
	public List<User> getUserGroupList(Integer orgId) throws SQLException {
		Connection conn = null;
		List<User> usergroupList = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
		/*	String query = "SELECT * FROM rfx_users u WHERE NOT EXISTS (SELECT * FROM alert_group_user_mapping a WHERE u.user_id = a.user_id ) "
					+ "AND u.user_type != 0 AND u.org_id =? AND u.group_id =? AND u.active = 1";*/
			String query = "SELECT * FROM rfx_users u WHERE NOT EXISTS (SELECT * FROM alert_group_user_mapping a WHERE u.user_id = a.user_id ) "
					+ "AND u.user_type != 0 AND u.org_id =? AND u.active = 1";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			/*stmt.setLong(2, groupId);*/
			ResultSet rs = stmt.executeQuery();
			usergroupList =  new ArrayList<User>();
			User user = null;
			while(rs.next()){
				user = new User(rs.getLong("user_id"), rs.getString("user_email"), rs.getString("user_name"), rs.getString("phone"), rs.getString("user_login"), rs.getTimestamp("last_login"), rs.getBoolean("active"), rs.getInt("user_type"), rs.getInt("org_id"), rs.getLong("group_id"), 
						rs.getInt("shard"), rs.getString("user_tz"), rs.getTimestamp("last_passwd_changed"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getInt("tz_id"),
						rs.getInt("privilege"));
				usergroupList.add(user);
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return usergroupList;
	}
	
	
	/**
	 * 
	 * @return usergroupList of only active users for specific alertGroup
	 * @throws SQLException 
	 */
	public List<User> getUsers(Long orgId, Long alertGroupId) throws SQLException {
		Connection conn = null;
		List<User> usergroupList = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			/*String query = "SELECT * FROM rfx_users u WHERE EXISTS (SELECT * FROM alert_group_user_mapping a WHERE u.user_id = a.user_id ) "
					+ "AND u.user_type != 0 AND u.org_id =? AND u.active = 1";*/
			String query = "SELECT * FROM rfx_users u INNER JOIN alert_group_user_mapping a ON a.user_id = u.user_id WHERE a.alert_group_id = ?"
					+ " AND u.org_id = ? AND u.user_type != 0 AND u.active = 1";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, alertGroupId);
			stmt.setLong(2, orgId);
			ResultSet rs = stmt.executeQuery();
			usergroupList =  new ArrayList<User>();
			User user = null;
			while(rs.next()){
				user = new User(rs.getLong("user_id"), rs.getString("user_email"), rs.getString("user_name"), rs.getString("phone"), rs.getString("user_login"), rs.getTimestamp("last_login"), rs.getBoolean("active"), rs.getInt("user_type"), rs.getInt("org_id"), rs.getLong("group_id"), 
						rs.getInt("shard"), rs.getString("user_tz"), rs.getTimestamp("last_passwd_changed"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getInt("tz_id"),
						rs.getInt("privilege"));
				usergroupList.add(user);
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return usergroupList;
	}

	/**
	 * 
	 * @param user
	 * @throws SQLException
	 */
	public void updateUser(User user) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE rfx_users SET user_name = ?, user_email = ?, phone=?, user_tz = ?, updated_time = NOW(),tz_id=? WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			String userName, email, phone, userTz;
			Integer userType;

			userName = user.getName();
			email = user.getEmail();
			phone = user.getPhone();
			userType = user.getUserType();
			userTz = user.getUserTz();

			if(userName != null){
				stmt.setString(1, userName);
			}else{
				stmt.setNull(1, Types.NULL);
			}

			if(email != null){
				stmt.setString(2, email);
			}else{
				stmt.setNull(2, Types.NULL);
			}

			if(phone != null){
				stmt.setString(3, phone);
			}else{
				stmt.setNull(3, Types.NULL);
			}


			/*if(userType != null){
				stmt.setInt(4, userType);
			}else{
				stmt.setInt(4,0);		// TO indicate normal user
			}*/

			if(user.getTzId() != null){
				TimeZone timeZone = OrganizationDAO.getInstance().getTimeZoneById(user.getTzId());
				stmt.setString(4, timeZone.getTimeZoneOffset());
				stmt.setInt(5, user.getTzId());
				 user.setUserTz(timeZone.getTimeZoneOffset());
			}else{
				Organization org = OrganizationDAO.getInstance().getOrganizationByOrgId(user.getOrgId());
				if(org != null){
					user.setUserTz(org.getOrgTz());
					user.setTzId(org.getTzId());
					stmt.setString(4,user.getUserTz());
					stmt.setInt(5, user.getTzId());
				}
			}

			stmt.setLong(6, user.getUserId());
			stmt.executeUpdate();
			stmt.close();			
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}


	/**
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException 
	 */
	public Boolean checkIfActiveUserIsPresent(Long userId) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_users WHERE user_id = ? and active=1 )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, userId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();

		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException 
	 */
	public Boolean checkIfActiveUserIsPresent(String userLogin) throws SQLException{
		Connection conn = null;
		Boolean isPresent= false;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT EXISTS(SELECT 1 from rfx_users WHERE user_login = ? and active=1 )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userLogin);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while(rs.next()){
				count = rs.getInt(1);
			}
			if(count == 1){
				isPresent = true;
			}
			rs.close();
			stmt.close();

		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}

	/**
	 * @throws SQLException 
	 * 
	 */
	public void deleteUser(Long userId) throws SQLException {
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "Update rfx_users set active=0 where user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, userId);
			stmt.executeUpdate();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param orgId
	 * @param groupId
	 * @return User Object
	 * @throws SQLException
	 */
	public User getUserByloginName(String userLogin) throws SQLException {
		Connection conn = null;
		User user = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_id, user_email, user_name, phone, user_login, last_login, active, user_type, org_id, group_id, shard, user_tz, last_passwd_changed, created_time, updated_time, tz_id, privilege FROM rfx_users "
					+ "WHERE user_login = ? ";

			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userLogin);

			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				user = new User(rs.getLong("user_id"), rs.getString("user_email"), rs.getString("user_name"), rs.getString("phone"), rs.getString("user_login"), rs.getTimestamp("last_login"), rs.getBoolean("active"), rs.getInt("user_type"), rs.getInt("org_id"), rs.getLong("group_id"), 
						rs.getInt("shard"), rs.getString("user_tz"), rs.getTimestamp("last_passwd_changed"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getInt("tz_id"),
						rs.getInt("privilege"));
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return user;
	}

	/**
	 *<p> Deactivate All users of an organization </p> 
	 * @param orgId
	 * @throws SQLException 
	 */
	public void deactivateOrgUsers(Connection conn,Integer orgId) throws SQLException {
			String query = "Update rfx_users set active=0 where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			stmt.executeUpdate();
			stmt.close();
			ArrayList<String> userLogins = getUserLoginsForOrg(orgId);
			UserTimeZoneCache.getInstance().removeUserTimeZoneOffset(userLogins);
			UserTimeZoneCache.getInstance().removeUserTimeZone(userLogins);
	}

	/**
	 * 
	 * @param userId
	 * @return
	 * @throws SQLException
	 */
	public User getUserById(Long userId)throws SQLException {
		Connection conn = null;
		User user = null;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT user_id, user_email, user_name, phone, user_login, last_login, active, user_type, org_id, group_id, shard, user_tz,"
					+ "last_passwd_changed, created_time, updated_time, tz_id, privilege FROM rfx_users WHERE user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, userId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				user = new User(rs.getLong("user_id"), rs.getString("user_email"), rs.getString("user_name"), rs.getString("phone"), rs.getString("user_login"), rs.getTimestamp("last_login"), rs.getBoolean("active"), rs.getInt("user_type"), rs.getInt("org_id"), rs.getLong("group_id"), 
						rs.getInt("shard"), rs.getString("user_tz"), rs.getTimestamp("last_passwd_changed"), rs.getTimestamp("created_time"), rs.getTimestamp("updated_time"),rs.getInt("tz_id"),
						rs.getInt("privilege"));
			}
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return user;
	}
	
	
	/**
	 * <p> Get list of userLogins for an organization </p>
	 * @param orgId
	 * @return
	 */
	public ArrayList<String> getUserLoginsForOrg(Integer orgId) throws SQLException {
		Connection conn = null;
		ArrayList<String> userLogins = new ArrayList<String>();
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT user_login from rfx_users where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setLong(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				userLogins.add(rs.getString("user_login"));
			}
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return userLogins;
	}
	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ListMultimap<String, String> getAdminUserDetails(int orgId) throws SQLException {
		Connection conn = null;
		ListMultimap<String, String> multimap = ArrayListMultimap.create();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT * FROM rfx_users WHERE org_id=? AND user_type=1";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				multimap.put(rs.getString("user_name"),rs.getString("user_email"));
			}
			
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return multimap;
	}
	/**
	 * 
	 * @param userLogin
	 * @return
	 * @throws SQLException
	 */
	public Integer getUsersOrgId(String userLogin) throws SQLException{
		Connection conn = null;
		Integer orgId = null;
		try{
			conn = DAOUtility.getInstance().getConnection();	
			String query = "SELECT org_id from rfx_users where user_login = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userLogin);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				orgId = rs.getInt("org_id");
			}
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return orgId;
	}

}
