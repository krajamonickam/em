package com.rfxcel.org.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.org.entity.Setting;

/**
 * 
 * @author sachin_kohale
 * @since Apr 13, 2017
 */
public class ConfigDAO {
	private static final Logger logger = Logger.getLogger(ConfigDAO.class);
	private static ConfigDAO singleton;

	public static ConfigDAO getInstance() {
		if (singleton == null) {
			synchronized (ConfigDAO.class) {
				if (singleton == null) {
					singleton = new ConfigDAO();
				}
			}
		}
		return singleton;
	}

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<Setting> getSystemSettingList() throws SQLException {
		List<Setting> settingList = null;
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT param_name, param_value, display_name FROM system_config WHERE active=1";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			settingList = new ArrayList<Setting>();
			Setting setting = null;
			while (rs.next()) {
				setting = new Setting(rs.getString("param_name"),
						rs.getString("param_value"),
						rs.getString("display_name"));
				settingList.add(setting);
			}
			rs.close();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return settingList;
	}

	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public List<Setting> getOrgSettingList(Integer orgId) throws SQLException {
		List<Setting> settingList = null;
		Connection conn = null;
		String query = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			if(!checkIfOrgIdIsPresent(orgId)){
				query = "SELECT param_name, param_value, display_name FROM org_config WHERE org_id=? AND active=1";
				orgId = 0;
			}else{
				query = "SELECT param_name, param_value, display_name FROM org_config WHERE org_id =? AND active=1";
			}
			
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			settingList = new ArrayList<Setting>();
			Setting setting = null;
			while (rs.next()) {
				setting = new Setting(rs.getString("param_name"),
						rs.getString("param_value"),
						rs.getString("display_name"));
				settingList.add(setting);
			}
			rs.close();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return settingList;
	}
	
	/**
	 * 
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public HashMap<String, Setting> getOrgSettingMap(Integer orgId) throws SQLException {
		HashMap<String, Setting> settingMap = null;
		Connection conn = null;
		String query = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			if(!checkIfOrgIdIsPresent(orgId)){
				query = "SELECT param_name, param_value, display_name FROM org_config WHERE org_id=? AND active=1";
				orgId = 0;
			}else{
				query = "SELECT param_name, param_value, display_name FROM org_config WHERE org_id =? AND active=1";
			}
			
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			settingMap = new HashMap<String, Setting>();
			Setting setting = null;
			while (rs.next()) {
				setting = new Setting(rs.getString("param_value"), rs.getString("display_name"));
				settingMap.put(rs.getString("param_name"), setting);
			}
			rs.close();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return settingMap;
	}

	/**
	 * 
	 * @param settingList
	 * @throws SQLException
	 */
	public void updateSystemSetting(List<Setting> settingList)
			throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE system_config SET param_value = ?, display_name = ? WHERE param_name = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			String paramName, paramValue, displayName;

			for (Setting setting : settingList) {
				paramName = setting.getParamName();
				paramValue = setting.getParamValue();
				displayName = setting.getDisplayName();
				stmt.setString(1, paramValue);
				stmt.setString(2, displayName);
				stmt.setString(3, paramName);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}

	}

	/**
	 * 
	 * @param settingList
	 * @throws SQLException
	 */
	public void updateOrgSetting(Connection conn, List<Setting> settingList, Integer orgId)
			throws SQLException {
		if (!checkIfOrgIdIsPresent(orgId)) {
			addOrgSetting(conn, settingList, orgId);
		} else {
			deleteOrgConfig(conn, orgId);
			addOrgSetting(conn, settingList, orgId);
		}
	}
	
	/**
	 * 
	 * @param settingList
	 * @param orgId
	 * @throws SQLException
	 */
	public void updateOrgSetting(List<Setting> settingList, Integer orgId)
			throws SQLException {
		if (!checkIfOrgIdIsPresent(orgId)) {
			addOrgSetting(settingList, orgId);
		} else {
			deleteOrgConfig(orgId);
			addOrgSetting(settingList, orgId);
		}
	}

	/**
	 * 
	 * @param OrgId
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfOrgIdIsPresent(Integer orgId) throws SQLException {
		Connection conn = null;
		Boolean isPresent = false;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT EXISTS(SELECT 1 from org_config WHERE org_id = ? )";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			int count = 0;
			while (rs.next()) {
				count = rs.getInt(1);
			}
			if (count == 1) {
				isPresent = true;
			}
			rs.close();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isPresent;
	}

	public void addOrgSetting(Connection conn, List<Setting> settingList, Integer orgId)throws SQLException {
		try {
			String query = "INSERT INTO org_config (param_name,param_value,display_name,org_id) VALUES (?,?,?,?) ";
			PreparedStatement stmt = conn.prepareStatement(query);
			for (Setting setting : settingList) {
				stmt.setString(1, setting.getParamName());
				stmt.setString(2, setting.getParamValue());
				stmt.setString(3, setting.getDisplayName());
				stmt.setInt(4, orgId);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		} finally {
		}
	}
	/**
	 * 
	 * @param settingList
	 * @throws SQLException
	 */
	public void addOrgSetting(List<Setting> settingList, Integer orgId)throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "INSERT INTO org_config (param_name,param_value,display_name,org_id) VALUES (?,?,?,?) ";
			PreparedStatement stmt = conn.prepareStatement(query);
			for (Setting setting : settingList) {
				stmt.setString(1, setting.getParamName());
				stmt.setString(2, setting.getParamValue());
				stmt.setString(3, setting.getDisplayName());
				stmt.setInt(4, orgId);
				stmt.addBatch();
			}
			stmt.executeBatch();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * @param conn2 
	 * @throws SQLException
	 * 
	 */
	public void deleteOrgConfig(Connection conn, Integer orgId) throws SQLException {
		try {
			String query = "DELETE FROM org_config where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
		}
	}
	
	/**
	 * 
	 * @param orgId
	 * @throws SQLException
	 */
	public void deleteOrgConfig(Integer orgId) throws SQLException {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "DELETE FROM org_config where org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			stmt.executeUpdate();
			stmt.close();
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * <p>
	 * method to load all system config
	 * </p>
	 * 
	 * @return
	 */
	public HashMap<String, String> getSystemConfig() {
		HashMap<String, String> configParams = new HashMap<String, String>();
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT param_name, param_value FROM system_config where active = 1";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			while (rs.next()) {
				String paramName = rs.getString("param_name");
				String paramvalue = rs.getString("param_value");
				configParams.put(paramName, paramvalue);
			}
		} catch (Exception e) {
			logger.error("Error while loading config params " + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return configParams;
	}

	/**
	 * @return
	 */
	public HashMap<Integer, HashMap<String, String>> getOrgConfig() {
		HashMap<Integer, HashMap<String, String>> orgConfig = new HashMap<Integer, HashMap<String, String>>();
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT param_name, param_value, org_id FROM org_config where active = 1";
			Statement stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			String paramName, paramValue;
			Integer orgId;
			while (rs.next()) {
				paramName = rs.getString("param_name");
				paramValue = rs.getString("param_value");
				orgId = rs.getInt("org_id");
				if (orgConfig.get(orgId) == null) {
					HashMap<String, String> configMap = new HashMap<String, String>();
					configMap.put(paramName, paramValue);
					orgConfig.put(orgId, configMap);
				} else {
					orgConfig.get(orgId).put(paramName, paramValue);
				}
			}
		} catch (Exception e) {
			logger.error("Error while loading config params " + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return orgConfig;
	}
	
	
	/**
	 * @return
	 */
	public  HashMap<String, String> getOrgConfig(int orgId) {
		HashMap<String, String> orgConfig = new HashMap<String, String>();
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT param_name, param_value FROM org_config where active = 1 and org_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			String paramName, paramValue;
			while (rs.next()) {
				paramName = rs.getString("param_name");
				paramValue = rs.getString("param_value");
				orgConfig.put(paramName, paramValue);
			}
		} catch (Exception e) {
			logger.error("Error while loading config params for org "+orgId+" "+ e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return orgConfig;
	}
	
	/**
	 * @return
	 */
	public  String getOrgConfig(String paramName, int orgId) {
		Connection conn = null;
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT param_value FROM org_config where active = 1 and org_id = ? and param_name = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setInt(1, orgId);
			stmt.setString(2, paramName.trim());
			ResultSet rs = stmt.executeQuery();
			String paramValue = null;
			if (rs.next()) 
			{
				paramValue = rs.getString("param_value");
			}
			return paramValue;
		} catch (Exception e) {
			logger.error("Error while loading config params for org "+orgId+" "+ e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return null;
	}
	/**
	 * <p>get default orgsettinglist</p>
	 * @return
	 * @throws SQLException
	 */
	public List<Setting> getDefaultOrgSettingList() throws SQLException {
		List<Setting> settingList = null;
		Connection conn = null;
		String sqlQuery = null;
		try {
			conn = DAOUtility.getInstance().getConnection();			
			sqlQuery = "SELECT param_name, param_value, display_name FROM org_config WHERE org_id =0 AND active=1";				
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			
			ResultSet rs = stmt.executeQuery();
			settingList = new ArrayList<Setting>();
			Setting setting = null;
			while (rs.next()) {
				setting = new Setting(rs.getString("param_name"),
						rs.getString("param_value"),
						rs.getString("display_name"));
				settingList.add(setting);
			}
			rs.close();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return settingList;
	}
	
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public HashMap<String, Setting> getDefaultOrgSettingMap() throws SQLException {
		HashMap<String, Setting> settingMap = null;
		Connection conn = null;
		String sqlQuery = null;
		try {
			conn = DAOUtility.getInstance().getConnection();			
			sqlQuery = "SELECT param_name, param_value, display_name FROM org_config WHERE org_id =0 AND active=1";				
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			
			ResultSet rs = stmt.executeQuery();
			settingMap = new HashMap<String, Setting>();
			Setting setting = null;
			while (rs.next()) {
				setting = new Setting(rs.getString("param_value"), rs.getString("display_name"));
				settingMap.put(rs.getString("param_name"), setting);
			}
			rs.close();
			stmt.close();

		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return settingMap;
	}
	

	/**
	 * <p>method to add default orgSetting</p>
	 * @param conn 
	 * @param orgId
	 * @throws SQLException
	 */
	public void addDefaultOrgSetting(Connection conn, Integer orgId) throws SQLException{
		List<Setting> settingList = getDefaultOrgSettingList();
		updateOrgSetting(conn, settingList, orgId);
	}
	
	/**
	 * 
	 * @param conn
	 * @param orgId
	 * @throws SQLException
	 */
	public void deactivateOrgConfigForOrg(Connection conn, Integer orgId) throws SQLException {
		String query = "UPDATE org_config SET active=0 WHERE org_id=?";
		PreparedStatement stmt = conn.prepareStatement(query);
		stmt.setInt(1, orgId);
		stmt.executeUpdate();
		stmt.close();
	}
}
