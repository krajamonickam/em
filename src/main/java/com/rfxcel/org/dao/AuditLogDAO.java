package com.rfxcel.org.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Types;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.apache.log4j.Logger;

import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.SensorRequest;

/**
 * 
 * @author Tejshree Kachare
 *
 */
public class AuditLogDAO {

	private static final Logger logger = Logger.getLogger(AuditLogDAO.class);	
	private static AuditLogDAO singleton;
	private static UserTimeZoneCache userTimeZoneCache = UserTimeZoneCache.getInstance();

	public static AuditLogDAO getInstance(){
		if(singleton == null){
			synchronized (AuditLogDAO.class) {
				if(singleton == null){
					singleton = new AuditLogDAO();
				}
			}
		}
		return singleton;
	}

	/**
	 * @param auditLog
	 * @throws SQLException
	 */
	public void addAuditLog(AuditLog auditLog) throws SQLException{
		Connection conn = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "INSERT INTO audit_log(user_id, user_name, entity_type,entity_id, action, old_value, new_value, description, org_id, create_time) VALUES(?,?,?,?,?,?,?,?,?,?)";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			if(auditLog.getUserId() == null){
				stmt.setNull(1, Types.NULL);
			}else{
				stmt.setLong(1, auditLog.getUserId());
			}
			stmt.setString(2, auditLog.getUserName());
			stmt.setString(3, auditLog.getEntityType());
			if( auditLog.getEntityId() == null){
				stmt.setNull(4, Types.NULL);
			}else{
				stmt.setLong(4, auditLog.getEntityId());
			}
			stmt.setString(5, auditLog.getAction());
			stmt.setString(6, auditLog.getOldValue());
			stmt.setString(7, auditLog.getNewValue());
			stmt.setString(8, auditLog.getDescription());
			stmt.setInt(9, auditLog.getOrgId());
			stmt.setString(10, Utility.getDateFormat().format(new Date()));
			stmt.executeUpdate();
			stmt.close();
		}catch(Exception e){			
			e.printStackTrace();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}
	
	
	/**
	 * @param orgId
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<AuditLog> getAuditLogs(SensorRequest request) throws SQLException{
		Connection conn = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ArrayList<AuditLog> auditLogs = new ArrayList<AuditLog>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			Integer orgId = request.getOrgId();
			String timeZoneOffset = userTimeZoneCache.getTimeZoneOffset(request.getUser());
			if(timeZoneOffset == null){
				timeZoneOffset = "+00:00";
			}
			String sqlQuery = "SELECT user_id, user_name, entity_type,entity_id, action, old_value, new_value, description, CONVERT_TZ(create_time,'"+SensorConstant.TIME_ZONE_OFFSET+"','"+timeZoneOffset +"') as create_time FROM audit_log WHERE org_id = ? order by create_time desc";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				AuditLog auditLog = new AuditLog(rs.getLong("user_id"), rs.getString("user_name"),rs.getLong("entity_id"), rs.getString("entity_type"),  
						rs.getString("action"), rs.getString("old_value"), rs.getString("new_value"), rs.getString("description"), orgId);
				auditLog.setCreatedOn(sdf.format(rs.getTimestamp("create_time")));
				auditLogs.add(auditLog);
			}
			rs.close();
			stmt.close();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return auditLogs;
	}
	
	/**
	 * 
	 * @param orgId
	 * @param searchString
	 * @return
	 * @throws SQLException
	 */
	public ArrayList<AuditLog> getAuditLogs(Integer orgId, String searchString) throws SQLException{
		Connection conn = null;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		ArrayList<AuditLog> auditLogs = new ArrayList<AuditLog>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String sqlQuery = "SELECT user_id, user_name, entity_type, entity_id, ACTION, old_value, new_value, description, create_time"
					+ " FROM audit_log WHERE org_id = ? AND (user_name LIKE '%"+searchString+"%' OR entity_type LIKE '%"+searchString+"%' OR description LIKE '%"+searchString+"%')"
					+ " ORDER BY create_time DESC";
			PreparedStatement stmt = conn.prepareStatement(sqlQuery);
			stmt.setInt(1, orgId);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				AuditLog auditLog = new AuditLog(rs.getLong("user_id"), rs.getString("user_name"),rs.getLong("entity_id"), rs.getString("entity_type"),  
						rs.getString("action"), rs.getString("old_value"), rs.getString("new_value"), rs.getString("description"), orgId);
				auditLog.setCreatedOn(sdf.format(rs.getTimestamp("create_time")));
				auditLogs.add(auditLog);
			}
			rs.close();
			stmt.close();
		}
		finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return auditLogs;
	}
}