package com.rfxcel.org.vo;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.Organization;
import com.rfxcel.sensor.vo.Request;


/**
 * 
 * @author Tejshree Kachare
 * @since April 10, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OrganizationRequest extends Request{
	
	private Organization organization;
	
	/**
	 * 
	 */
	public OrganizationRequest() {
	}

	/**
	 * @return the organization
	 */
	public Organization getOrganization() {
		return organization;
	}

	/**
	 * @param organization the organization to set
	 */
	public void setOrganization(Organization organization) {
		this.organization = organization;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "OrganizationRequest ["
				+ (organization != null ? "organization=" + organization + ", "	: "")
				+ (userId != null ? "userId=" + userId + ", " : "")
				+ (orgId != null ? "orgId=" + orgId + ", " : "")
				+ (groupId != null ? "groupId=" + groupId : "") + "]";
	}

	
}
