package com.rfxcel.org.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.User;
import com.rfxcel.sensor.vo.Response;


/**
 * 
 * @author Tejshree Kachare
 * @since Apr 11, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class UserResponse extends Response {
	private List<User> userList;

	/**
	 * 
	 */
	public UserResponse(){
	}
	
	/**
	 * 
	 * @param dateTime
	 */
	public UserResponse(String dateTime) {
		super.setDateTime(dateTime);
	}
	/**
	 * 
	 * @return userList
	 */
	public List<User> getUserList() {
		return userList;
	}
	/**
	 * 
	 * @param userList set to userList
	 */
	public void setUserList(List<User> userList) {
		this.userList = userList;
	}

	
}
