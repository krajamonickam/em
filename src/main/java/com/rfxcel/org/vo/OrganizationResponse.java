package com.rfxcel.org.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.Organization;
import com.rfxcel.sensor.vo.Response;


/**
 * 
 * @author Tejshree Kachare
 * @since April 10, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class OrganizationResponse extends Response {

	private List<Organization> organizationList;
	public OrganizationResponse(String dateTime) {
		super.setDateTime(dateTime);
	}
	
	/**
	 * @return the organizationList
	 */
	public List<Organization> getOrganizationList() {
		return organizationList;
	}
	
	/**
	 * @param organizationList the organizationList to set
	 */
	public void setOrganizationList(List<Organization> organizationList) {
		this.organizationList = organizationList;
	}
}
