package com.rfxcel.org.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.TimeZone;
import com.rfxcel.sensor.vo.Response;


/**
 * @author Albeena May 26, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class TimeZoneResponse extends Response {
	private List<TimeZone> timeZoneList;

	/**
	 * 
	 */
	public TimeZoneResponse(){
	}

	/**
	 * @return the timeZoneList
	 */
	public List<TimeZone> getTimeZoneList() {
		return timeZoneList;
	}

	/**
	 * @param timeZoneList the timeZoneList to set
	 */
	public void setTimeZoneList(List<TimeZone> timeZoneList) {
		this.timeZoneList = timeZoneList;
	}
	
	

	
}
