package com.rfxcel.org.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.Setting;
import com.rfxcel.sensor.vo.Request;

/**
 * 
 * @author sachin_kohale
 * @since Apr 13, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SettingRequest extends Request {

	private Setting setting;
	private Long groupId;
	private Integer orgId;
	private List<Setting> settingList;

	/**
	 * 
	 * @return
	 */
	public Setting getSetting() {
		return setting;
	}

	/**
	 * 
	 * @param setting
	 */
	public void setSetting(Setting setting) {
		this.setting = setting;
	}

	/**
	 * 
	 * @return
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * 
	 * @param groupId
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * 
	 * @return
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * 
	 * @param orgId
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * 
	 * @return
	 */
	public List<Setting> getSettingList() {
		return settingList;
	}

	/**
	 * 
	 * @param settingList
	 */
	public void setSettingList(List<Setting> settingList) {
		this.settingList = settingList;
	}
	
	
	
}
