package com.rfxcel.org.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.Setting;
import com.rfxcel.sensor.vo.Response;

/**
 * 
 * @author sachin_kohale
 * @since Apr 13, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SettingResponse extends Response {
	
	private List<Setting> settingList;
	/**
	 * 
	 */
	public SettingResponse(){		
	}
	
	/**
	 * 
	 * @param dateTime
	 */
	public SettingResponse(String dateTime) {
		super.setDateTime(dateTime);
	}

	/**
	 * 
	 * @return settingList
	 */
	public List<Setting> getSettingList() {
		return settingList;
	}

	/**
	 * 
	 * @param settingList to settingList
	 */
	public void setSettingList(List<Setting> settingList) {
		this.settingList = settingList;
	}
	
}
