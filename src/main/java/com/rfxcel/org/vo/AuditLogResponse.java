package com.rfxcel.org.vo;

import java.util.List;

import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.sensor.vo.Response;

/**
 * 
 * @author Tejshree Kachare
 * @since V2.3 Oct 9, 2017
 * 
 */

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AuditLogResponse extends Response{

	private List<AuditLog> auditLogs;
	
	public AuditLogResponse(){		
	}
	
	public AuditLogResponse(String dateTime){
		super.setDateTime(dateTime);
	}

	/**
	 * @return the auditLogs
	 */
	public List<AuditLog> getAuditLogs() {
		return auditLogs;
	}

	/**
	 * @param auditLogs the auditLogs to set
	 */
	public void setAuditLogs(List<AuditLog> auditLogs) {
		this.auditLogs = auditLogs;
	}

}
