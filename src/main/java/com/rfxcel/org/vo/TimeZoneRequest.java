package com.rfxcel.org.vo;


import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.TimeZone;
import com.rfxcel.sensor.vo.Request;

/**
 * @author Albeena May 26, 2017
 *
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class TimeZoneRequest extends Request {
	
	private TimeZone timeZone;
	
	/**
	 * 
	 */
	public TimeZoneRequest() {
	}

	/**
	 * @return the timeZone
	 */
	public TimeZone getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(TimeZone timeZone) {
		this.timeZone = timeZone;
	}

}
