package com.rfxcel.org.vo;


import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.User;
import com.rfxcel.sensor.vo.Request;


/**
 * 
 * @author Tejshree Kachare
 * @since Apr 11, 2017
 */
@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class UserRequest extends Request {
	
	private User user;
	private String url;
	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

}
