package com.rfxcel.org.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.org.dao.ConfigDAO;
import com.rfxcel.org.entity.Setting;
import com.rfxcel.org.vo.SettingRequest;
import com.rfxcel.org.vo.SettingResponse;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;

/**
 * 
 * @author sachin_kohale
 * @since Apr 13, 2017
 */
@Path("setting")
public class SettingService {
	
	private static Logger logger = Logger.getLogger(SettingService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static ConfigDAO settingDao = ConfigDAO.getInstance();

	@Path("getSettingList")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response getSettingList(@Context HttpServletRequest httpRequest, SettingRequest request){
		SettingResponse response = new SettingResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getSetting() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Setting information cannot be null.")).build();
			}
			Setting setting = request.getSetting();
			Integer orgId = setting.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			//
			//	ITT-666	-APP-HIGH-002: Authorization checks are implemented by in-browser filtering
			//  The following code prevents access to the settings menu for anyone other than system. Note that this completely prevents access to
			//  org settings, since now admin/support/standard users can no longer view or modify the org settings. For now, that is OK, we are
			//  closing the security issue in ITT-666. Please do not remove this code without discussing with Arun Rao
			//
			Integer loginUserType = TokenManager.getInstance().getUserType(httpRequest);
			if(loginUserType != 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to access settings")).build();
			}					
			// end ITT-666
			List<Setting> settingList = new ArrayList<Setting>();
			HashMap<String, Setting> defaultSettingMap = ConfigDAO.getInstance().getDefaultOrgSettingMap();
			if(orgId == 0){
				settingList = settingDao.getSystemSettingList();
			}else{
				HashMap<String, Setting> settingMap = settingDao.getOrgSettingMap(orgId);
				for(Entry<String, Setting> e : defaultSettingMap.entrySet()){
					if(!settingMap.containsKey(e.getKey())){
						settingMap.put(e.getKey(), e.getValue());
					}
				}
				for(Entry<String, Setting> settingMapEntry : settingMap.entrySet()){
					String paramName = settingMapEntry.getKey();
					String paramValue = settingMapEntry.getValue().getParamValue();
					String displayName = settingMapEntry.getValue().getDisplayName();
					Setting settingObj = new Setting(paramName, paramValue, displayName);
					settingList.add(settingObj);
				}
				
				
			}
			response.setSettingList(settingList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch(Exception e){
			logger.error("Exception in SettingService.getSettingList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SettingService.getSettingList Request :: "+request +" Response :: "+response );
		}
	}
	
	@Path("addSettings")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response addSettings(@Context HttpServletRequest httpRequest, SettingRequest request){
		SettingResponse response = new SettingResponse(simpleDateFormat.format(new Date()));
		try{
			
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			//Check if request is not empty
			if(request == null || request.getSettingList() == null || request.getSettingList().size() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Setting List cannot be null or empty.")).build();
			}
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			
			//
			//	ITT-666	-APP-HIGH-002: Authorization checks are implemented by in-browser filtering
			//  The following code prevents access to the settings menu for anyone other than system. Note that this completely prevents access to
			//  org settings, since now admin/support/standard users can no longer view or modify the org settings. For now, that is OK, we are
			//  closing the security issue in ITT-666. Please do not remove this code without discussing with Arun Rao
			//
			Integer loginUserType = TokenManager.getInstance().getUserType(httpRequest);
			if(loginUserType != 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to access settings")).build();
			}					
			// end ITT-666
			
			if(orgId == 0){
				settingDao.updateSystemSetting(request.getSettingList());
				ConfigCache.getInstance().populateSystemConfig();
			}else{				
				settingDao.updateOrgSetting(request.getSettingList(), orgId);
				ConfigCache.getInstance().updateOrgSettings(orgId);
			}
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("System/Organization Settings Saved Successfully")).build();
			
		}catch(Exception e){
			logger.error("Exception in SettingService.addSettings "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: SettingService.addSettings Request :: "+request +" Response :: "+response );
		}
	}
}
