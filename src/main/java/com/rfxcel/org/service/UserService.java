package com.rfxcel.org.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.AuthService;
import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.federation.FederatedGetShard;
import com.rfxcel.notification.dao.AlertGroupDAO;
import com.rfxcel.notification.entity.AlertGroup;
import com.rfxcel.notification.vo.AlertGroupRequest;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.dao.TimeZoneDAO;
import com.rfxcel.org.dao.UserDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.entity.User;
import com.rfxcel.org.vo.UserRequest;
import com.rfxcel.org.vo.UserResponse;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Timer;


/**
 * 
 * @author Tejshree Kachare
 * @Since Apr 11, 2017
 */
@Path("user")
public class UserService {

	private static Logger logger = Logger.getLogger(UserService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static UserDAO userDao = UserDAO.getInstance();
	private static AlertGroupDAO alertGroupDao = AlertGroupDAO.getInstance();
	private static UserTimeZoneCache userTimeZoneCache = UserTimeZoneCache.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	
	
	@Path("addUser")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response addUser(@Context HttpServletRequest httpRequest,UserRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try {
			//Check if request is not empty
			if(request == null || request.getUser() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User information cannot be null.")).build();
			}
			User user = request.getUser();
			Integer orgId = user.getOrgId();			
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			Timer timer = new Timer();
			String exists = FederatedGetShard.getInstance().getShardForUser(user.getUserLogin(), true);
			logger.info("addUser.getUserShard for " + user + " retrieved in " + timer.getSeconds() + "s - shard = " + exists);
			if ((exists != null) && (exists.length() > 0)) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("UserID " + user.getUserLogin() + " already exists")).build();
			}

			String myToken = TokenManager.getInstance().getToken(httpRequest);
			if(myToken == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			int myUserType = TokenManager.getInstance().getUserType(httpRequest);
			if (myUserType == User.USERTYPE_SYSTEM) {
				// allow system to create support or admin or standard users
			}
			else if (myUserType == User.USERTYPE_ADMIN) {
				// allow admin to create standard users in their org
				if (user.getUserType() != User.USERTYPE_STANDARD) {
					Long userId = tokenManager.getUserId(httpRequest);
					String userName = tokenManager.getUserForToken(httpRequest);
					String logMessage = "User "+ userName +" tried to created userName " + user.getName() + " of type " + user.userTypeString() + 
							", forcing down to type " + User.userTypeString(User.USERTYPE_STANDARD);
					AuditLog auditLog = new AuditLog(userId, userName, userId, "User", "Create", null, user.toJson(),logMessage,orgId);
					auditLogDao.addAuditLog(auditLog);
					logger.error(logMessage);
					user.setUserType(User.USERTYPE_STANDARD);
				}
			}
			else if (myUserType == User.USERTYPE_SUPPORT) {
				// don't allow support users to add any users
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to add user")).build();
			}
			else {
				// don't allow standard users to add any users
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to add user")).build();
			}
			
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			String userLogin = user.getUserLogin();
			if(userLogin == null || userLogin.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("UserLogin cannot be null or empty.")).build();
			}
			
			String name = user.getName();
			if( name == null || name.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User Name cannot be null or empty.")).build();
			}
			
			String email = user.getEmail();
			if( email == null || email.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User emailId cannot be null or empty.")).build();
			}
			
			Boolean present = userDao.checkIfUserLoginIsPresent(userLogin);
			if(present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("UserLogin "+userLogin +" is already present in system.")).build();
			}
			String shardString = ConfigCache.getInstance().getSystemConfig("application.shard");
			Integer shard = Integer.parseInt(shardString);
			user.setShard(shard);

			user = userDao.addUser(user);
			// Add timeZone offsetMApping to cache
			userTimeZoneCache.addUserTimeZoneOffset(user.getUserLogin(), user.getUserTz());
			String tzCode = TimeZoneDAO.getInstance().getTimeZoneCode(user.getTzId());
			userTimeZoneCache.addUserTimeZone(user.getUserLogin(), tzCode);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has added new user " + user.getName();
			AuditLog auditLog = new AuditLog(userId, userName, userId, "User", "Create", null, user.toJson(),logMessage,orgId);
			auditLogDao.addAuditLog(auditLog);
			
			// send set password email
			//AuthService.setPassword(userLogin, request.getUrl());
			String userTypeString = user.userTypeString();
			String successMessage = userTypeString + " user " + userName + " created successfully";
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage(successMessage)).build();
		}catch (Exception e) {
			logger.error("Exception in UserService.addUsers "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.addUsers Request :: "+request +" Response :: "+response );
		}
	}
	
	
	
	@Path("deleteUser")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteUser(@Context HttpServletRequest httpRequest,UserRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try {
			if(request == null || request.getUser() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User Id cannot be null.")).build();
			}

			User user = request.getUser();
			Integer orgId = user.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			Long userId = user.getUserId();
			Boolean present = userDao.checkIfActiveUserIsPresent(userId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User is not configured or is already deleted.")).build();
			}
			
			int myOrgId = orgId.intValue();
			Long myUserId = TokenManager.getInstance().getUserId(httpRequest);
			int myUserType = TokenManager.getInstance().getUserType(httpRequest);
			User oldUser = userDao.getUserById(userId);
			int oldUserType = oldUser.getUserType().intValue();
			
			if (myUserType == User.USERTYPE_SYSTEM) {
				// allow system to delete any user
			}
			else if (myUserType == User.USERTYPE_ADMIN) {
				if (((oldUserType == User.USERTYPE_ADMIN) || (oldUserType == User.USERTYPE_STANDARD)) && (oldUser.getOrgId().intValue() == myOrgId)) {
					// allow admin to delete non-system users in their org
				}
				else {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
							response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to delete users")).build();
				}
			}
			else if (myUserType == User.USERTYPE_SUPPORT) {
				// don't allow support users to delete any users
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to delete users")).build();
			}
			else {
				// don't allow standard users to delete users other than themselves
				if (myUserId.longValue() != userId) {
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
							response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Insufficient authority to delete users")).build();
				}
			}
			
			userDao.deleteUser(userId);
			userTimeZoneCache.removeUserTimeZoneOffset(oldUser.getUserLogin());
			
			userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has deleted user "+oldUser.getName();
			AuditLog auditLog = new AuditLog(userId, userName, userId, "User", "Delete", user.toJson(), null,logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("User deleted successfully")).build();

		}catch (Exception e) {
			logger.error("Exception in UserService.deleteUser "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.deleteUser Request :: "+request +" Response :: "+response );
		}
	}

	
	@POST
	@Path("getUserList")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getUserList(@Context HttpServletRequest httpRequest, UserRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getUser() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User information cannot be null.")).build();
			}
			User user = request.getUser();
			Integer orgId = user.getOrgId();
			/*Long groupId = user.getGroupId();*/
			
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			List<User> userList = null;
			userList = userDao.getUserList(orgId);
			response.setUserList(userList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
			
		}catch(Exception e){
			logger.error("Exception in UserService.getUserList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.getUserList Request :: "+request +" Response :: "+response );
		}		
	}
	
	@POST
	@Path("getAlertGroupList")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAlertGroupList(@Context HttpServletRequest httpRequest, UserRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getUser() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User information cannot be null.")).build();
			}
			User user = request.getUser();
			Integer orgId = user.getOrgId();
			/*Long groupId = user.getGroupId();*/
			
			if(orgId == null && orgId != 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			
			List<User> userGroupList = null;
			List<User> userList = null;
			userGroupList = userDao.getUserGroupList(orgId);
			if(userGroupList.isEmpty()){
				int groupUsersCount = alertGroupDao.alertGroupUsersCount(orgId);
				userList = userDao.getUserList(orgId);
				if(userList.size() != groupUsersCount){
					userGroupList = userList;
				}     
			}
			response.setUserList(userGroupList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
			
		}catch(Exception e){
			logger.error("Exception in UserService.getAlertGroupList "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.getAlertGroupList Request :: "+request +" Response :: "+response );
		}		
	}
	
	@POST
	@Path("getUsers")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getUsers(@Context HttpServletRequest httpRequest, AlertGroupRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getAlertGroup() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group information cannot be null.")).build();
			}
			AlertGroup alertGroup = request.getAlertGroup();
			Long orgId = alertGroup.getOrgId();
			Long alertGroupId = alertGroup.getId();
			
			//check if org id is not null
			if(orgId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest,orgId.intValue());
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			//check if alertGroupId is not null
			if(alertGroupId == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Alert Group id cannot be null.")).build();
			}
			List<User> userGroupList = null;
			userGroupList = userDao.getUsers(orgId, alertGroupId);
			if(!userGroupList.isEmpty()){
				response.setUserList(userGroupList);  
			}
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
			
		}catch(Exception e){
			logger.error("Exception in UserService.getUsers "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.getUsers Request :: "+request +" Response :: "+response );
		}		
	}
	
	@POST
	@Path("updateUser")
	@Consumes("application/json")
	@Produces("application/json")
	public Response updateUser(@Context HttpServletRequest httpRequest, UserRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getUser() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User information cannot be null.")).build();
			}
			User user = request.getUser();	
			Integer orgId = user.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			//check if user is present or not
			Long userId = user.getUserId();
			Boolean present = userDao.checkIfActiveUserIsPresent(userId);
			if(!present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User is not configured or is already deleted.")).build();
			}
			
			String name = user.getName();
			if( name == null || name.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User Name cannot be null or empty.")).build();
			}
			
			String email = user.getEmail();
			if( email == null || email.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User emailId cannot be null or empty.")).build();
			}
			User oldUser = userDao.getUserById(userId);
			userDao.updateUser(user);
			
			userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has updated user "+oldUser.getName();
			AuditLog auditLog = new AuditLog(userId, userName, userId, "User", "Update", oldUser.toJson(), user.toJson(),logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			userTimeZoneCache.addUserTimeZoneOffset(user.getUserLogin(), user.getUserTz());
			String tzCode = TimeZoneDAO.getInstance().getTimeZoneCode(user.getTzId());
			userTimeZoneCache.addUserTimeZone(user.getUserLogin(), tzCode);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("User updated successfully")).build();
		}catch(Exception e){
			logger.error("Exception in UserService.updateUser "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.updateUser Request :: "+request +" Response :: "+response );
		}
	}
	
	
	
	/**
	 * <p> Deactivate all users of an organization
	 *//*
	public static void deactivateOrgUsers(Integer orgId){
		try{
			userDao.deactivateOrgUsers(orgId);
			
		}catch(Exception e){
			logger.error("Exception in UserService.deactivateOrganizationUsers "+ e.getMessage());
			e.printStackTrace();
		}finally{
			diagnosticService.write("API :: UserService.deactivateOrganizationUsers " );
		}
	}*/
	
	/**
	 * 
	 * @param groupId
	 * @return
	 * @throws SQLException
	 */
	public static  com.rfxcel.sensor.vo.Response validateGroup(Long groupId) throws SQLException{
		com.rfxcel.sensor.vo.Response  resp = new com.rfxcel.sensor.vo.Response();
		if(groupId == null || groupId < 0){
			resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("groupId cannot be null or negative");
		}
		return resp;
	}
	
	
	@Path("setPassword")
	@POST
	@Consumes("application/json")
	@Produces("application/json")
	public Response setPassword(@Context HttpServletRequest httpRequest,UserRequest request){
		UserResponse response = new UserResponse(simpleDateFormat.format(new Date()));
		try {
			//Check if request is not empty
			if(request == null || request.getUser() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User information cannot be null.")).build();
			}
			User user = request.getUser();
			Integer orgId = user.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			String userLogin = user.getUserLogin();
			if(userLogin == null || userLogin.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("UserLogin cannot be null or empty.")).build();
			}
			
			String name = user.getName();
			if( name == null || name.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User Name cannot be null or empty.")).build();
			}
			
			String email = user.getEmail();
			if( email == null || email.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("User emailId cannot be null or empty.")).build();
			}
			
			Boolean present = userDao.checkIfUserLoginIsPresent(userLogin);
			user = userDao.getUserByloginName(userLogin);
			if(present && user.getActive()){
				// send set password email
				AuthService.setPassword(userLogin, request.getUrl());
			}
			
			Long userId = user.getUserId();
			AuditLog auditLog = new AuditLog(userId, name, userId, "SetPassword", "", null, null, "An email has been sent to user "+name +" to set password.", orgId);
			auditLogDao.addAuditLog(auditLog);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("An email has been sent to "+ user.getEmail()+" to create a password")).build();
		}catch (Exception e) {
			logger.error("Exception in UserService.setPassword "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: UserService.setPassword Request :: "+request +" Response :: "+response );
		}
	}
}
