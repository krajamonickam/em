package com.rfxcel.org.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.vo.AuditLogResponse;
import com.rfxcel.sensor.dao.SearchDAO;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorRequest;


/**
 * @author Tejshree Kachare
 * @since V2.3 (Oct 9, 2017)
 */

@Path("audit")
public class AuditService {
	private static Logger logger = Logger.getLogger(AuditService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	
	@POST
	@Path("getAuditLogs")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAuditLogs(@Context HttpServletRequest httpRequest, SensorRequest request){

		AuditLogResponse response = new AuditLogResponse(simpleDateFormat.format(new Date()));	
		try{
			//check if request is not null
			if(request == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Request cannot be null.")).build();
			}
			Integer orgId = request.getOrgId();
			//check if org id is not null
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS)
						.entity(response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative")).build();			
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			String user = TokenManager.getInstance().getUserForToken(httpRequest);
			request.setUser(user);
			
			//check the Elastic search fetch is true or false
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			String alertDevicelogString = ConfigCache.getInstance().getSystemConfig("alert.devicelog.search.enable");
			Boolean alertDeviceLogSearch = false;
			if(alertDevicelogString != null){
				alertDeviceLogSearch = Boolean.valueOf(alertDevicelogString);
			}
			
			String responseMsg = "";
			if(fetchESData || alertDeviceLogSearch){
				response = SearchDAO.getInstance().getAuditLogs(request);
				if(SensorConstant.RESP_STAT_ERROR.equals(response.getResponseStatus())){
					responseMsg = response.getResponseMessage();
					response.setResponseMessage(responseMsg);
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);	
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response).build();
				}
			}else{
				ArrayList<AuditLog> auditLogs = auditLogDao.getAuditLogs(request);
				response.setAuditLogs(auditLogs);
			}
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
						
		}catch(Exception e){
			logger.error("Exception in AuditService.getAuditLog "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: AlertService.getAlertList  Request :: "+request +" Response :: "+response );
		}
	
	}
}
