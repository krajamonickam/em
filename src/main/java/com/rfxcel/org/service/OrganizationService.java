package com.rfxcel.org.service;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.google.common.collect.ListMultimap;
import com.rfxcel.auth2.TokenManager;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.cache.DeviceCache;
import com.rfxcel.notification.service.EmailAlertService;
import com.rfxcel.notification.vo.EmailDetailsVO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.entity.Organization;
import com.rfxcel.org.entity.User;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.dao.OrganizationDAO;
import com.rfxcel.org.dao.UserDAO;
import com.rfxcel.sensor.dao.DeviceTypeDAO;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.org.vo.OrganizationRequest;
import com.rfxcel.org.vo.OrganizationResponse;
import com.rfxcel.org.vo.TimeZoneRequest;
import com.rfxcel.org.vo.TimeZoneResponse;


/**
 * 
 * @author Tejshree Kachare
 * @since April 10, 2017
 */

@Path("organization")
public class OrganizationService {

	private static Logger logger = Logger.getLogger(OrganizationService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static OrganizationDAO orgDAO = OrganizationDAO.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	private static UserDAO  userDAO = UserDAO.getInstance();
	
	@POST
	@Path("addOrganization")
	@Consumes("application/json")
	@Produces("application/json")
	public Response addOrganization(@Context HttpServletRequest httpRequest, OrganizationRequest request){
		OrganizationResponse response = new OrganizationResponse(simpleDateFormat.format(new Date()));
		try {
			
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			//Check if request is not empty
			if(request == null || request.getOrganization() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization details cannot be null")).build();
			}
			//check if loginedUserType is other than system user			
			Integer loginedUserType = TokenManager.getInstance().getUserType(httpRequest);
			if(loginedUserType != 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Other than System user cannot add organization.")).build();
			}					
			
			Organization org = request.getOrganization();
			String orgName = org.getName();
			String extId = org.getExtId();
			//Check mandatory fields
			if(orgName == null || orgName.trim().length() ==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization name cannot be null or empty")).build();
			}
		
			//check if organization with given orgExtId exist  
			if(extId== null || extId.trim().length() ==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization's Ext Id cannot be null or empty")).build();
			}
			//check if organization timezone is not null
			Integer tzId = org.getTzId();
			if(tzId == null || tzId <= 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization timezone cannot be null or empty")).build();
			}
			
			Boolean present = orgDAO.checkIfOrganizationIsPresent(extId);
			if(present){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization with org ExtId "+extId + " is already configured")).build();
			}
			
			Boolean isPresent = orgDAO.checkIfOrganizationIsPresentByOrgName(orgName);
			if(isPresent){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization with org name "+orgName+" is already configured")).build();
			}
			
			Integer organizationId = orgDAO.addOrganization(org);
			ArrayList<Integer> deviceTypeIds = DeviceTypeDAO.getInstance().getDeviceTypeIdsByOrgId(organizationId);
			//adding into deviceTypeAttribute cache
			for(Integer deviceTypeId : deviceTypeIds){
				DeviceCache.getInstance().addDeviceTypeAttrbute(deviceTypeId); 
			}	
			
			ConfigCache.getInstance().updateOrgSettings(organizationId);
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has create organization "+orgName;
			AuditLog auditLog = new AuditLog(userId, userName, new Long(organizationId), "Organization", "Create", null, org.toJson(),logMessage, organizationId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Organization "+org.getName() +" created successfully")).build();
			
		}catch (Exception e) {
			logger.error("Exception in OrganizationService.addOrganization "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: OrganizationService.addOrganization  Request :: "+request +" Response :: "+response );
		}
	}
	
	
	@POST
	@Path("getOrganizations")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getOrganizations(@Context HttpServletRequest httpRequest, OrganizationRequest request){
		OrganizationResponse response = new OrganizationResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			//Check if request is groupId is not empty
			if(request == null || request.getOrgId() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization ID cannot be null")).build();
			}
			List<Organization> orgList = null;
			if(orgId == 0)	//orgId for system user is 0
			{
				orgList = orgDAO.getAllOrganizations();
			}else{
				Organization org = orgDAO.getOrganizationByOrgId(orgId);
				if(org != null){
					orgList = new ArrayList<Organization>();
					orgList.add(org);
				}else{
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization with ID "+ orgId+ " is not configured")).build();
				}
			}
			response.setOrganizationList(orgList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in OrganizationService.getOrganizations "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: OrganizationService.getOrganizations  Request :: "+request +" Response :: "+response );
		}
	}
	
	
	@POST
	@Path("modifyOrganization")
	@Consumes("application/json")
	@Produces("application/json")
	public Response modifyOrganization(@Context HttpServletRequest httpRequest, OrganizationRequest request){
		OrganizationResponse response = new OrganizationResponse(simpleDateFormat.format(new Date()));
		try{

			//Check if request is not empty
			if(request == null || request.getOrganization() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization details cannot be null")).build();
			}
			
			Organization org = request.getOrganization();
			Integer orgId = org.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			String orgName = org.getName();
			//Check mandatory fields
			if(orgName == null || orgName.trim().length() == 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization name cannot be null or empty")).build();
			}
			//check if organization timezone is not null
			Integer tzId = org.getTzId();
			if(tzId == null || tzId <= 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization timezone cannot be null or empty")).build();
			}
			
			//Check mandatory fields
			com.rfxcel.sensor.vo.Response resp = validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			//check if same org name is present other than current org id
			Boolean isPresent = orgDAO.checkIfOrganizationIsPresentByOrgName(orgName, orgId);
			if(isPresent){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization with org name "+orgName+" is already configured")).build();
			}
			Organization oldOrg = orgDAO.getOrganizationByOrgId(orgId);
			orgDAO.updateOrganization(org);
			
			Long userId = tokenManager.getUserId(httpRequest);
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has updated organization "+oldOrg.getName();
			AuditLog auditLog = new AuditLog(userId, userName, new Long(orgId), "Organization", "Update", oldOrg.toJson(), org.toJson(),logMessage,orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Organization "+orgName +" modified successfully")).build();
			
		}catch(Exception e){
			logger.error("Exception in OrganizationService.modifyOrganization "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: OrganizationService.modifyOrganization  Request :: "+request +" Response :: "+response );
		}			
		
	}
	
	@POST
	@Path("deleteOrganization")
	@Consumes("application/json")
	@Produces("application/json")
	public Response deleteOrganization(@Context HttpServletRequest httpRequest, OrganizationRequest request){
		OrganizationResponse response = new OrganizationResponse(simpleDateFormat.format(new Date()));
		try{
			//Check if request is not empty
			if(request == null || request.getOrganization() == null){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization details cannot be null")).build();
			}
			Organization org = request.getOrganization();
			
			Integer orgId = org.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			com.rfxcel.sensor.vo.Response resp = validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			org=orgDAO.getActiveOrganizationByOrgId(orgId);
			orgDAO.deleteOrganization(orgId);
			
			//cleanupAfterOrgDelete(orgId);	
			Long userId = tokenManager.getUserId(httpRequest);
			User loginUser = userDAO.getUserById(userId);
			
			ListMultimap<String, String> multimap =  UserDAO.getInstance().getAdminUserDetails(orgId);
			for (Map.Entry<String, String> entry : multimap.entries()) {
				 	String salutation = "Hi "+entry.getKey()+",";
	    			String message = "Organization "+org.getName()+" has been deleted by user "+loginUser.getName()+" at time "+Utility.getDateFormat().format(new Date())+" UTC.";
	    			EmailDetailsVO emailDetails= new EmailDetailsVO(entry.getValue(), "Organization "+org.getName()+" has been deleted", message, "",salutation);
	    			new EmailAlertService().triggerEmail(emailDetails);
			}			
			
			
			String userName = tokenManager.getUserForToken(httpRequest);
			String logMessage = "User "+ userName +" has deleted organization "+org.getName();
			AuditLog auditLog = new AuditLog(userId, userName, new Long(orgId), "Organization", "Delete", org.toJson(), null,logMessage, orgId);
			auditLogDao.addAuditLog(auditLog);
			
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
					response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Organization deleted successfully")).build();
		}catch(Exception e){
			logger.error("Exception in OrganizationService.deleteOrganization "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: OrganizationService.deleteOrganization  Request :: "+request +" Response :: "+response );
		}			
	}
	
	
	/**
	 * <p> DeActivate records in Db tables and clear cache for org
	 * @param orgId
	 *//*
	private void cleanupAfterOrgDelete(Integer orgId) {
		// Deactivate users and cleanup user cache
		userDAO.deactivateOrgUsers(orgId);
		// Deactivate DeviceTypes
		DeviceTypeService.deleteDeviceTypeForOrg(orgId);
		// Deactivate profiles for org
		ProfileService.deactivateProfilesForOrg(orgId);
		// Deactivate devices
		SensorService.deactivateDevicesForOrg(orgId);
		//Remove package product mapping for an organization
		AssociationCache.getInstance().removePackageProductMappingForOrg(orgId);
		// remove org related configuration from cache
		ConfigCache.getInstance().removeOrgConfig(orgId);
		
		AlertGroupService.deactivateAlertGroupsForOrg(orgId);
		
		AlertService.deleteAlertsForOrg(orgId);
		
	}*/


	/**
	 * Validate if organization specified by id exist or not
	 * @param orgId
	 * @return 
	 * @return
	 * @throws SQLException 
	 */
	public static  com.rfxcel.sensor.vo.Response validateOrganization(Integer orgId) throws SQLException{
		com.rfxcel.sensor.vo.Response  resp = new com.rfxcel.sensor.vo.Response();
		if(orgId == null || orgId < 0){
			return resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative");
		}
		// check if organization is present
		Boolean present = orgDAO.checkIfOrganizationIsPresent(orgId);
		if(!present){
			return	resp.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Organization is not active or not configured");
		}
		return resp;
	}
	
	@POST
	@Path("getTimeZones")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getTimeZones(@Context HttpServletRequest httpRequest, TimeZoneRequest request){
		TimeZoneResponse response = new TimeZoneResponse();
		try {
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null or negative.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			List<com.rfxcel.org.entity.TimeZone> timeZoneList = orgDAO.getTimeZones();
			response.setTimeZoneList(timeZoneList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in OrganizationService.getTimeZones "+ e.getMessage());
			e.printStackTrace();
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: OrganizationService.getTimeZones  Request :: "+request +" Response :: "+response );
		}
	}
}

	
	

