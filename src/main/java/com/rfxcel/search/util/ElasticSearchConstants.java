package com.rfxcel.search.util;

import java.text.SimpleDateFormat;

/**
 * 
 * @author Tejshree Kachare
 * @Since 3 Aug 2017
 *
 */
public interface ElasticSearchConstants {

	/**
	 * Elastic Search Document type for sensor notifications
	 */
	String ES_DOCTYPE_SENSOR_NOTIFICATION = "sensorNotifications";
	/**
	 * Elastic search Document type for device logs
	 */
	String ES_DOCTYPE_DEVICE_LOGS = "deviceLogs";
	/**
	 * Elastic search Document type for audit logs
	 */
	String ES_DOCTYPE_AUDIT_LOGS = "auditLogs";
	/**
	 * Elastic Search Document type for sensor Associations
	 */
	String ES_DOCTYPE_SENSOR_ASSOCIATION = "sensorAssociate";
	/**
	 * Elastic Search Document type for sensor data
	 */
	String ES_DOCTYPE_SENSOR_DATA = "sensorData";

	/**
	 * Elastic Search Document type for trans history
	 */
	String ES_DOCTYPE_TRANS_HISTORY = "transHistory";

	/**
	 * Elastic search default date format
	 */
	SimpleDateFormat esDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSX");
}
