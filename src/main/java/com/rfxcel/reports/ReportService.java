package com.rfxcel.reports;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.org.service.OrganizationService;
import com.rfxcel.cache.RefreshCache;
import com.rfxcel.cache.UserTimeZoneCache;
import com.rfxcel.sensor.beans.DeviceData;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Utility;
import com.rfxcel.sensor.vo.Request;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.auth2.TokenManager;

/**
 * @author Tejshree Kachare
 */
@Path("/sensor")
public class ReportService {
	
	private static Logger logger = Logger.getLogger(ReportService.class);
	private static final String COMMA_DELIMITER = ",";
	private static final String NEW_LINE_SEPARATOR = "\n";
	SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd");
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static UserTimeZoneCache userTimeZoneOffsetCache = UserTimeZoneCache.getInstance();
	
	
	@POST
	@Path("/getShipmentReport")
	@Consumes("application/json")
	@Produces("application/octet-stream")
	public Response	getShipmentReport(@Context HttpServletRequest httpRequest, SensorRequest request){  
		FileWriter fileWriter = null;
		String respMsg = null;
		try{
			
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"OrgId cannot be null or negative.\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				respMsg = "{\"responseStatus\": \"error\",\"responseCode\": 403,\"responseMessage\": \"Token authentication failed. Either token has expired or is invalid\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			String timeZoneOffset = userTimeZoneOffsetCache.getTimeZoneOffset(request.getUser());
			if (timeZoneOffset == null) {
				timeZoneOffset = "+00:00";
			}
			com.rfxcel.sensor.vo.Response resp = OrganizationService.validateOrganization(orgId);
			if(SensorConstant.RESP_STAT_ERROR.equalsIgnoreCase(resp.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(resp).build();
			}
			
			DeviceData deviceData = request.getDeviceData();
			if (request.getDeviceData() == null) {
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity("{\"responseStatus\": \"error\",\"responseMessage\": \"Device data is null\" }").build();
			}	
			String deviceId = deviceData.getDeviceId();
			
			//check if device is present in request
			if ( deviceId == null || deviceId.trim().length()==0) {
				respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"Device Id is null\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}	
			//Check if the device Id is configured in system
			Boolean isPresent = SendumDAO.getInstance().checkIfDeviceIsPresent(deviceData.getDeviceId(), orgId);
			if(!isPresent){
				respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"Device Id is not configured in system\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			//check if package is present
			//String packageId = deviceData.getPackageId();
			//Check if package is commissioned
			if (deviceData.getPackageId() == null || deviceData.getPackageId().trim().length()==0) {
				respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"Package id is null\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}	
			//Check if the packageId is present in system
			isPresent = SendumDAO.getInstance().checkIfPackageIsPresent(deviceData.getPackageId(), deviceData.getProductId(), orgId);
			if(!isPresent){
				respMsg ="{\"responseStatus\": \"error\",\"responseMessage\": \"Package id is not configured in system\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity( respMsg).build();
			}
			StringBuilder sb = new StringBuilder();
			String report = getShipmentData(deviceData.getDeviceId(), deviceData.getPackageId(), timeZoneOffset);
			sb.append(report);
			
			String fileName = deviceData.getDeviceId()+"_"+simpleDateFormat.format(new Date())+".csv";
			ResponseBuilder responseBuilder = Response.ok((Object) sb.toString());
			responseBuilder.header("Content-Disposition", "attachment; filename="+fileName);
			return responseBuilder.build();
		}catch (SQLException e) {
			logger.error("Exception in getShipmentReport "+ e.getMessage());
			e.printStackTrace();
			respMsg ="{\"responseStatus\": \"error\",\"responseMessage\":\""+e.getMessage() +"\"} ";
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(respMsg).build();
		}catch (Exception e) {
			logger.error("Exception in getShipmentReport "+ e.getMessage());
			e.printStackTrace();
			respMsg = "{\"responseStatus\": \"error\",\"responseMessage\":\""+e.getMessage() +"\"} ";
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(respMsg).build();
		}
		finally{
			diagnosticService.write("API :: getShipmentReport.  Request :: "+request +" Response :: "+respMsg );
			if(fileWriter != null){
				try {
					fileWriter.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
		}
	}
	/**
	 * 
	 * @param deviceId
	 * @param packageId
	 * @throws SQLException 
	 * @throws ParseException 
	 */
	public static String getShipmentData(String deviceId, String packageId, String offset) throws SQLException, ParseException{
		Connection conn = null ;
		StringBuilder sb = new StringBuilder() ;
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "Select CONVERT_TZ(sd.status_time,'"+ SensorConstant.TIME_ZONE_OFFSET+ "','"	+ offset + "') as status_time1, sd.* from sensor_data sd where device_id=? and container_id=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, packageId);
			ResultSet rs = stmt.executeQuery();
			sb.append(getHeaderRow());
			/**
			 * id,	device_id,	container_id,	temperature,	pressure,	humidity,	light,	tilt,	battery_rem
			growth_log,	cumulative_log,latitude,	longitude,	position_error,	fix_type,	fix_valid,	temp_probe,	status_time
			capacity_rem,	voltage,	cycle_count,	signal_strength,	cell_sys_id,	ambient_temp,	 */
			while(rs.next()){
				Long statusTime = 0l;	
				String statusTimeStr = "";
				if (rs.getString("status_time1") != null) {					
					statusTime = sdf.parse(rs.getString("status_time1")).getTime();		
					statusTimeStr = sdf.format(new Date(statusTime));
				}
				sb.append(writeRecord(rs.getLong("id"), rs.getString("device_id"), rs.getString("container_id"), rs.getString("temperature"), rs.getString("pressure"),
						rs.getString("humidity"), rs.getString("light"), rs.getString("tilt"), rs.getString("battery_rem"), rs.getString("growth_log"), rs.getString("cumulative_log"),
						rs.getString("latitude"), rs.getString("longitude"), rs.getString("position_error"), rs.getString("fix_type"), rs.getString("fix_valid"),
						rs.getString("temp_probe"), statusTimeStr, rs.getLong("capacity_rem"), rs.getLong("voltage"), rs.getString("cycle_count"), rs.getString("signal_strength"),
						rs.getString("cell_sys_id"), rs.getString("ambient_temp")));
			}
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return sb.toString();
	}
	/**
	 */
	private static String writeRecord(long id, String deviceId, String containerId,String temperature, String pressure, 
			String humidity, String light,	String tilt, String batteryRem, String growthLog, String cumLog,
			String latitude, String longitude, String positionError, String fixType,String fixValid,
			String tempProbe, String statusTime, Long capacityRem,Long voltage, String cycleCount, String signalStrength,
			String cellSysId, String ambientTemp) {

		StringBuilder sb = new StringBuilder();
		sb.append(id).append(COMMA_DELIMITER);
		deviceId = (deviceId ==null) ? "": deviceId;
		sb.append(deviceId).append(COMMA_DELIMITER);
		containerId = (containerId ==null) ? "": containerId;
		sb.append(containerId).append(COMMA_DELIMITER);
		temperature = (temperature ==null) ? "": temperature;
		sb.append(temperature).append(COMMA_DELIMITER);
		pressure = (pressure ==null) ? "": pressure;
		sb.append(pressure).append(COMMA_DELIMITER);
		humidity = (humidity ==null) ? "": humidity;
		sb.append(humidity).append(COMMA_DELIMITER);
		light = (light ==null) ? "": light;
		sb.append(light).append(COMMA_DELIMITER);
		tilt = (tilt ==null) ? "": tilt;
		sb.append(tilt).append(COMMA_DELIMITER);
		if(batteryRem == null){
			sb.append("").append(COMMA_DELIMITER);
		}else{
			sb.append(batteryRem).append(COMMA_DELIMITER);
		}
		growthLog = (growthLog ==null) ? "": growthLog;
		sb.append(growthLog).append(COMMA_DELIMITER);
		cumLog = (cumLog ==null) ? "": cumLog;
		sb.append(cumLog).append(COMMA_DELIMITER);
		latitude = (latitude ==null) ? "": latitude;
		sb.append(latitude).append(COMMA_DELIMITER);
		longitude = (longitude ==null) ? "": longitude;
		sb.append(longitude).append(COMMA_DELIMITER);
		positionError = (positionError ==null) ? "": positionError;
		sb.append(positionError).append(COMMA_DELIMITER);
		fixType = (fixType ==null) ? "": fixType;
		sb.append(fixType).append(COMMA_DELIMITER);
		fixValid = (fixValid ==null) ? "": fixValid;
		sb.append(fixValid).append(COMMA_DELIMITER);
		tempProbe = (tempProbe ==null) ? "": tempProbe;
		sb.append(tempProbe).append(COMMA_DELIMITER);
		statusTime = (statusTime ==null) ? "": statusTime;
		sb.append(statusTime).append(COMMA_DELIMITER);
		if(capacityRem == null){
			sb.append("").append(COMMA_DELIMITER);
		}else{
			sb.append(capacityRem).append(COMMA_DELIMITER);
		}
		if(voltage == null){
			sb.append("").append(COMMA_DELIMITER);
		}else{
			sb.append(voltage).append(COMMA_DELIMITER);
		}
		cycleCount = (cycleCount ==null) ? "": cycleCount;
		sb.append(cycleCount).append(COMMA_DELIMITER);
		signalStrength = (signalStrength ==null) ? "": signalStrength;
		sb.append(signalStrength).append(COMMA_DELIMITER);
		cellSysId = (cellSysId ==null) ? "": cellSysId;
		sb.append(cellSysId).append(COMMA_DELIMITER);
		ambientTemp = (ambientTemp ==null) ? "": ambientTemp;
		sb.append(ambientTemp);
		sb.append(NEW_LINE_SEPARATOR);

		return sb.toString();
	}
	
	/**
	 * 
	 * @return
	 */
	private static String getHeaderRow(){
		StringBuilder sb = new StringBuilder();
		sb.append("id").append(COMMA_DELIMITER).
		append("DeviceId").append(COMMA_DELIMITER)
		.append("packageId").append(COMMA_DELIMITER)
		.append("Temperature").append(COMMA_DELIMITER)
		.append("Pressure").append(COMMA_DELIMITER)
		.append("Humidity").append(COMMA_DELIMITER)
		.append("Light").append(COMMA_DELIMITER)
		.append("Tilt").append(COMMA_DELIMITER)
		.append("Battery Remaining").append(COMMA_DELIMITER)
		.append("Growth Log").append(COMMA_DELIMITER)
		.append("Cumulative Log").append(COMMA_DELIMITER)
		.append("Latitude").append(COMMA_DELIMITER)
		.append("Longitude").append(COMMA_DELIMITER)
		.append("Position Error").append(COMMA_DELIMITER)
		.append("Fix Type").append(COMMA_DELIMITER)
		.append("Fix Valid").append(COMMA_DELIMITER)
		.append("Temperature PRobe").append(COMMA_DELIMITER)
		.append("Status Time").append(COMMA_DELIMITER)
		.append("Capacity Remaining").append(COMMA_DELIMITER)
		.append("Voltage").append(COMMA_DELIMITER)
		.append("Cycle Count").append(COMMA_DELIMITER)
		.append("Signal Strength").append(COMMA_DELIMITER)
		.append("Cell system Id").append(COMMA_DELIMITER)
		.append("Ambient Temperature")
		.append(NEW_LINE_SEPARATOR);
		return sb.toString();
		
	}
	@POST
	@Path("/resetData")
	@Consumes("application/json")
	@Produces ("application/json")
	public Response	resetData(@Context HttpServletRequest httpRequest, Request request){  
		try{
			SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
			Integer orgId = request.getOrgId();
			if(orgId == null ){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
			String respMsg = "{\"responseStatus\": \"error\",\"responseCode\": 403,\"responseMessage\": \"Token authentication failed. Either token has expired or is invalid\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			SendumDAO.getInstance().resetData();
			RefreshCache.doReset();
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Sensor data have been reset successfully")).build();
		}catch (Exception e) {
			logger.error("Exception in resetData "+ e.getMessage());
			e.printStackTrace();
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity("{\"responseStatus\": \"error\",\"responseMessage\":\""+e.getMessage() +"\"} ").build();
		}
		
	}
	
	@POST
	@Path("/startSimulator")
	@Consumes("application/json")
	@Produces ("application/json")
	public Response	startSimulator(@Context HttpServletRequest httpRequest, Request request){  
		try{
			SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
			Integer orgId = request.getOrgId();
			if(orgId == null ){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				String respMsg = "{\"responseStatus\": \"error\",\"responseCode\": 403,\"responseMessage\": \"Token authentication failed. Either token has expired or is invalid\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			Utility.startDataSimulator();
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Sensor data simulator have been run successfully")).build();
		}catch (Exception e) {
			logger.error("Exception in resetData "+ e.getMessage());
			e.printStackTrace();
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity("{\"responseStatus\": \"error\",\"responseMessage\":\""+e.getMessage() +"\"} ").build();
		}
	}
	
	
	@POST
	@Path("/resetOrg")
	@Consumes("application/json")
	@Produces ("application/json")
	public Response	resetOrg(@Context HttpServletRequest httpRequest, Request request){  
		try{
			logger.info("*** resetOrg(" + request.getOrgId() + ") started *** ");
			SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
			Integer orgId = request.getOrgId();
			if(orgId == null ){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("OrgId cannot be null.")).build();
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
			String respMsg = "{\"responseStatus\": \"error\",\"responseCode\": 403,\"responseMessage\": \"Token authentication failed. Either token has expired or is invalid\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			SendumDAO.getInstance().resetOrg(orgId);
			logger.info("*** resetOrg(" + request.getOrgId() + ") completed *** ");
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Reset Organization has started, please allow a few minutes for completion")).build();
		}catch (Exception e) {
			logger.error("Exception in resetOrg "+ e.getMessage());
			e.printStackTrace();
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity("{\"responseStatus\": \"error\",\"responseMessage\":\""+e.getMessage() +"\"} ").build();
		}
		
	}
}


