package com.rfxcel.integration.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;

import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.sensor.util.Config;

public class SensorDAO {

	private static final Logger logger = Logger.getLogger(SensorDAO.class);
	private static SensorDAO sensorDAO;

	public static SensorDAO getInstance(){
		if(sensorDAO == null){
			sensorDAO = new SensorDAO();
		}
		Config.getInstance().getProp();
		return sensorDAO;
	}

	SensorDAO(){
	}

	
	/**
	 * 
	 * @return
	 * @throws SQLException
	 */
	public HashMap<String, String> getAllActiveShipments() throws SQLException{
		Connection conn = null;
		HashMap<String, String> result = new HashMap<String, String>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String searchQuery = "SELECT child_id, parent_id FROM sensor_associate WHERE relation_status = 1 order by update_datetime desc";
			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			ResultSet  rs = stmt.executeQuery();
			while (rs.next()) {
				result.put(rs.getString("child_id"), rs.getString("parent_id"));
			}
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return result;
	}

	/**
	 * 
	 * @param deviceId
	 * @throws SQLException
	 */
	public String getActiveAssociatedPackage(String deviceId) throws SQLException{
		Connection conn = null;
		String packageId = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String searchQuery = "SELECT parent_id FROM sensor_associate WHERE child_id = ? AND relation_status = 1";
			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setString(1, deviceId);
			ResultSet  rs = stmt.executeQuery();
			while (rs.next()) {
				packageId = rs.getString("parent_id");
			}
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return packageId;
	}
	
	/**
	 * 
	 * @param deviceId
	 * @return
	 * @throws SQLException
	 */
	public String getActiveAssociatedProduct(String deviceId) throws SQLException{
		Connection conn = null;
		String productId = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String searchQuery = "SELECT product_id FROM sensor_associate WHERE child_id = ? AND relation_status = 1";
			PreparedStatement stmt = conn.prepareStatement(searchQuery);
			stmt.setString(1, deviceId);
			ResultSet  rs = stmt.executeQuery();
			while (rs.next()) {
				productId = rs.getString("product_id");
			}
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return productId;
	}

	/**
	 * 
	 * @param searchString
	 * @return
	 */
	/*public ArrayList<String> searchShipmentHomeGeopoints(String searchString) {
		return AssociationDAO.getInstance().searchHomeGeopoints(searchString);
	}*/
	
	/**
	 * <p>It is used to fetch sensor data for EMS requirement</p>
	 * @param deviceId
	 * @param packageId
	 * @param productId
	 * @return
	 */
	public ArrayList<com.rfxcel.sensor.util.Sensor> getSensorData(String deviceId, String packageId, String productId) {
		Connection conn = null;
		ArrayList<com.rfxcel.sensor.util.Sensor> sensorDataList = new ArrayList<com.rfxcel.sensor.util.Sensor>();		
		try {
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT device_id, temperature, pressure, humidity, light, tilt, battery_rem AS battery, growth_log, cumulative_log, latitude, longitude,"
					+ " status_time, voltage, excursion_status, excursion_attributes, vibration, shock FROM sensor_data"
					+ " WHERE device_id=? AND container_id=? AND product_id=? AND latitude IS NOT NULL AND longitude IS NOT NULL";
    		PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, deviceId);
			stmt.setString(2, packageId);
			stmt.setString(3, productId);
			ResultSet rs = stmt.executeQuery();
			com.rfxcel.sensor.util.Sensor sensor = null;
			while (rs.next()) {
				sensor = new com.rfxcel.sensor.util.Sensor();
				sensor.setDeviceIdentifier(rs.getString("device_id"));
				sensor.setTemperature(rs.getString("temperature"));
				sensor.setPressure(rs.getString("pressure"));
				sensor.setHumidity(rs.getString("humidity"));
				sensor.setLight(rs.getString("light"));
				sensor.setTilt(rs.getString("tilt"));
				sensor.setBattery(rs.getString("battery"));
				sensor.setGrowthLog(rs.getString("growth_log"));
				sensor.setCumulativeLog(rs.getString("cumulative_log"));
				sensor.setLatitude(rs.getString("latitude"));
				sensor.setLongitude(rs.getString("longitude"));
				sensor.setStatusTimeStamp(rs.getString("status_time"));
				sensor.setVoltage(rs.getString("voltage"));
				sensor.setExcursionStatus(rs.getString("excursion_status"));
				sensor.setExcursionAttributes(rs.getString("excursion_attributes"));
				sensor.setVibration(rs.getString("vibration"));
				sensor.setShock(rs.getString("shock"));
				sensorDataList.add(sensor);
			}			
			rs.close();
			stmt.close();
		} catch (Exception e) {
			logger.error("Error while fetching sensor data for deviceId : "	+ deviceId + ", packageId : "+packageId+", productId : "+productId+" and error details is" + e.getMessage());
		} finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return sensorDataList;

	}
}
