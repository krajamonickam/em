package com.rfxcel.integration.service;

import java.util.HashMap;
import java.util.Map;


public class DeviceAssociateSaveWrapperRequest  {
	
	
	private DeviceAssociateSaveRequest requestObject;

	public DeviceAssociateSaveRequest getRequestObject() {
		return requestObject;
	}

	public void setRequestObject(DeviceAssociateSaveRequest requestObject) {
		this.requestObject = requestObject;
	}
	
}
