package com.rfxcel.integration.service;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.ResponseBuilder;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.integration.dao.SensorDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.reports.ReportService;
import com.rfxcel.sensor.service.DiagnosticService;


@Path("/integrateSensor")
public class IntegrateSensorReport {

	private static Logger logger = Logger.getLogger(IntegratedSensor.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final SimpleDateFormat reportDateFormat  = new SimpleDateFormat("yyyyMMddHHmmss");
	private static final SensorDAO sensorDAO = SensorDAO.getInstance();
	private static final SendumDAO sendumDAO = SendumDAO.getInstance();
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();


	@POST
	@Path("/getReport")
	@Consumes("application/json")
	@Produces("application/zip")
	public Response getReport(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		String respMsg;
		try {
			List<DeviceDetail> deviceDetails = request.getItemListDetails();
			HashMap<String, String> shipment = new HashMap<String, String>();
			/**
			 * get device ID
			 * If deviceId is null, get all active shipments and fetch data for it
			 * If deviceId is not null, then fetch shipment corresponding to it
			 * create csv file for each shipment, name of file should indicate shipment
			 * zip all the files and send it in response
			 */
			if(deviceDetails == null || deviceDetails.size() == 0){
				//Get all active shipments
				shipment = sensorDAO.getAllActiveShipments();
			}else{
				String deviceId = deviceDetails.get(0).getDeviceId();
				if(deviceId == null || deviceId.trim().length()==0){
					shipment = sensorDAO.getAllActiveShipments();
				}else{
					String packageId = sensorDAO.getActiveAssociatedPackage(deviceId);
					if(packageId == null){
						respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"No active association present for device Id "+deviceId+"\" }";
						return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
					}
					shipment.put(deviceId, packageId);
				}
			}
			//Get report directory path property
			String timestamp = reportDateFormat.format(new Date());
			String reportDir = ConfigCache.getInstance().getSystemConfig("sensor.report.directory");
			if(reportDir == null){
				respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"Report directory path is not configured.\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			String timeStampDir = reportDir+timestamp;
			boolean dirExist = createReportDir(timeStampDir);
			if(!dirExist){
				logger.error("Unable to create report directory, please check configuration ");
				respMsg = "{\"responseStatus\": \"error\",\"responseMessage\": \"Report directory cannot be created please check the configuration\" }";
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(respMsg).build();
			}
			//Iterate over all the active shipment
			String sb;
			for(String key : shipment.keySet()){
				String value = shipment.get(key);
//				sb = ReportService.getShipmentData(key, value);         //This is not used in this release so commented
				//Create csv report file 
				String fileName = key+"-"+System.currentTimeMillis()+".csv";
//				writeReportToFile(timeStampDir, fileName, sb);          //This is not used in this release so commented
			}
			File file = new File(timeStampDir);
			String zipFileName = timeStampDir+".zip";
			zipDirectory(file, zipFileName);
			deleteDir(file);

			File zippedFile = new File(zipFileName);
			ResponseBuilder responseBuilder = Response.ok((Object) zippedFile);
			responseBuilder.header("Content-Disposition", "attachment; filename="+zipFileName);
			return responseBuilder.build();
		}catch (Exception e) {
			logger.error("Exception in getReport "+ e.getMessage());
			e.printStackTrace();
			respMsg = "{\"responseStatus\": \"error\",\"responseMessage\":\""+e.getMessage() +"\"} ";
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(respMsg).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.getReport Request :: "+ request +" Response :: "+ res);
		}
	}


	/**
	 * 
	 * @param reportDir
	 * @param fileName
	 * @param sb
	 */
	private void writeReportToFile(String reportDir, String fileName, String sb) {
		FileWriter fw = null ;
		try {
			File file = new File(reportDir,fileName);
			fw = new FileWriter(file);
			fw.write(sb);
			fw.flush();
		} catch (IOException e) {
			logger.error("Exception while writing report to file");
			e.printStackTrace();
		}finally{
			if(fw!=null){
				try {
					fw.flush();
					fw.close();
				} catch (IOException e) {
				}
			}
		}
	}

	/**
	 * @param reportDir
	 */
	private boolean createReportDir(String reportDir) {
		boolean dirExist = true;
		if(reportDir == null){
			dirExist = false;
		}
		else{
			File dir = new File(reportDir);
			if (!dir.exists()) {
				if (!dir.mkdirs()) {
					dirExist = false;
					logger.info("Error while creating report Directory " + reportDir );
				}
			}
		}
		return dirExist;
	}

	/**
	 * This method zips the directory
	 * @param dir
	 * @param zipDirName
	 */
	private void zipDirectory(File dir, String zipDirName) {
		FileOutputStream fos = null ;
		ZipOutputStream zos = null ;
		try {
			ArrayList<String> filesListInDir = new ArrayList<String>();
			populateFilesList(filesListInDir, dir);
			//now zip files one by one
			//create ZipOutputStream to write to the zip file
			fos = new FileOutputStream(zipDirName);
			zos = new ZipOutputStream(fos);
			for(String filePath : filesListInDir){
				//for ZipEntry we need to keep only relative file path, so we used substring on absolute path
				ZipEntry ze = new ZipEntry(filePath.substring(dir.getAbsolutePath().length()+1, filePath.length()));
				zos.putNextEntry(ze);
				//read the file and write to ZipOutputStream
				FileInputStream fis = new FileInputStream(filePath);
				byte[] buffer = new byte[1024];
				int len;
				while ((len = fis.read(buffer)) > 0) {
					zos.write(buffer, 0, len);
				}
				zos.closeEntry();
				fis.close();
			}
			zos.close();
			fos.close();
		} catch (IOException e) {
			e.printStackTrace();
		}finally{
			try {
				if (fos != null){
					fos.close();
				}if(zos != null){
					zos.close();
				}
			} catch (IOException e) {
			}
		}
	}

	/**
	 * This method populates all the files in a directory to a List
	 * @param dir
	 * @throws IOException
	 */
	private void populateFilesList(ArrayList<String> filesListInDir, File dir) throws IOException {
		File[] files = dir.listFiles();
		for(File file : files){
			if(file.isFile()) {
				filesListInDir.add(file.getAbsolutePath());
			}
			else {
				populateFilesList(filesListInDir, file);
			}
		}
	}

	/**
	 * @param file
	 */
	private void deleteDir(File file) {
		try{
			File[] contents = file.listFiles();
			if (contents != null) {
				for (File f : contents) {
					deleteDir(f);
				}
			}
			file.delete();
		} catch(Exception e){
		}
	}
	
	
	@GET
    @Path("/testGetFile")
    @Produces("application/zip")
    public Response getFile() {
        File file = new File("D:\\Verizon ITT\\sensor\\report\\20170215142607.zip");
        ResponseBuilder response = Response.ok((Object) file);
        response.header("Content-Disposition",
            "attachment; filename=\"20170215142607.zip\"");
        return response.build();
    }
}
