package com.rfxcel.integration.service;

import java.util.HashMap;
import java.util.Map;


public class TraceItemRequest  {
	
	
	private String itemIdValue;
	private String itemIdCondition;
	private boolean serialPrefix;
	private Map<String, Object> aiCode = new HashMap<>(); 


	public String getItemIdValue() {
		return itemIdValue;
	}

	public void setItemIdValue(String itemIdValue) {
		this.itemIdValue = itemIdValue;
	}

	public String getItemIdCondition() {
		return itemIdCondition;
	}

	public void setItemIdCondition(String itemIdCondition) {
		this.itemIdCondition = itemIdCondition;
	}

	public boolean isSerialPrefix() {
		return serialPrefix;
	}

	public void setSerialPrefix(boolean serialPrefix) {
		this.serialPrefix = serialPrefix;
	}

	public Map<String, Object> getAiCode() {
		return aiCode;
	}

	public void setAiCode(Map<String, Object> aiCodes) {
		this.aiCode = aiCodes;
	}
	
}
