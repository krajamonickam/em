package com.rfxcel.integration.service;

import java.util.HashMap;
import java.util.Map;


public class ReceiveSaveRequest  {
	
	
	private TraceLocation shipFrom;
	private TraceLocation billFrom;
	private TraceLocation receiveAt;
	public TraceLocation getReceiveAt() {
		return receiveAt;
	}


	public void setReceiveAt(TraceLocation receiveAt) {
		this.receiveAt = receiveAt;
	}


	public TraceLocation getBillAt() {
		return billAt;
	}


	public void setBillAt(TraceLocation billAt) {
		this.billAt = billAt;
	}


	private TraceLocation billAt;
	
	private String dispositionId;
	private String dispositionValue;
	
	private String comment;
	
	
	private Long carrierPartyId;
	private String carrierPartyOrgName;
	
	private String shipDate;
	private String transactionDate;
	private String transNumber;
	private String transBizOrgName;
	private Long transBizOrgGLN;
	private String transType;
	private Long transBizVersionId;
	
	
	private TraceItem[] shippingItemList;
	private boolean fromEdit;
	private String eventId;
	private String pageFrom;


	public TraceLocation getShipFrom() {
		return shipFrom;
	}


	public void setShipFrom(TraceLocation shipFrom) {
		this.shipFrom = shipFrom;
	}


	public TraceLocation getBillFrom() {
		return billFrom;
	}


	public void setBillFrom(TraceLocation billFrom) {
		this.billFrom = billFrom;
	}


	

	public String getDispositionId() {
		return dispositionId;
	}


	public void setDispositionId(String dispositionId) {
		this.dispositionId = dispositionId;
	}


	public String getDispositionValue() {
		return dispositionValue;
	}


	public void setDispositionValue(String dispositionValue) {
		this.dispositionValue = dispositionValue;
	}


	public String getComment() {
		return comment;
	}


	public void setComment(String comment) {
		this.comment = comment;
	}

	public Long getCarrierPartyId() {
		return carrierPartyId;
	}


	public void setCarrierPartyId(Long carrierPartyId) {
		this.carrierPartyId = carrierPartyId;
	}


	public String getCarrierPartyOrgName() {
		return carrierPartyOrgName;
	}


	public void setCarrierPartyOrgName(String carrierPartyOrgName) {
		this.carrierPartyOrgName = carrierPartyOrgName;
	}


	public String getShipDate() {
		return shipDate;
	}


	public void setShipDate(String shipDate) {
		this.shipDate = shipDate;
	}


	public String getTransactionDate() {
		return transactionDate;
	}


	public void setTransactionDate(String transactionDate) {
		this.transactionDate = transactionDate;
	}


	public String getTransNumber() {
		return transNumber;
	}


	public void setTransNumber(String transNumber) {
		this.transNumber = transNumber;
	}


	public String getTransBizOrgName() {
		return transBizOrgName;
	}


	public void setTransBizOrgName(String transBizOrgName) {
		this.transBizOrgName = transBizOrgName;
	}


	public Long getTransBizOrgGLN() {
		return transBizOrgGLN;
	}


	public void setTransBizOrgGLN(Long transBizOrgGLN) {
		this.transBizOrgGLN = transBizOrgGLN;
	}


	public String getTransType() {
		return transType;
	}


	public void setTransType(String transType) {
		this.transType = transType;
	}


	public Long getTransBizVersionId() {
		return transBizVersionId;
	}


	public void setTransBizVersionId(Long transBizVersionId) {
		this.transBizVersionId = transBizVersionId;
	}


	public TraceItem[] getShippingItemList() {
		return shippingItemList;
	}


	public void setShippingItemList(TraceItem[] shippingItemList) {
		this.shippingItemList = shippingItemList;
	}
	
	public String getEventId() {
		return eventId;
	}


	public void setEventId(String eventId) {
		this.eventId = eventId;
	}


	public boolean getFromEdit() {
		return fromEdit;
	}


	public void setFromEdit(boolean fromEdit) {
		this.fromEdit = fromEdit;
	}
	
	public String getPageFrom() {
		return pageFrom;
	}


	public void setPageFrom(String pageFrom) {
		this.pageFrom = pageFrom;
	}

	
}
