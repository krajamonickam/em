
package com.rfxcel.integration.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;


@JsonInclude(JsonInclude.Include.NON_NULL)
@JsonPropertyOrder({
    "dateTime",
    "responseStatus",
    "responseMessage",
    "listData"
})
public class TraceItemResponse {
	
	
	@JsonProperty("dateTime")
    private String dateTime;
    @JsonProperty("responseStatus")
    private String responseStatus;
    @JsonProperty("responseMessage")
    private String responseMessage;
    @JsonProperty("listData")

    private TraceItem[] listData = null;
	
	 public String getDateTime() {
		return dateTime;
	}
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	public String getResponseStatus() {
		return responseStatus;
	}
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	public String getResponseMessage() {
		return responseMessage;
	}
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}
	public TraceItem[] getListData() {
		return listData;
	}
	public void setListData(TraceItem[] listData) {
		this.listData = listData;
	}
	
	
	
	
}
