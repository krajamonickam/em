package com.rfxcel.integration.service;

import java.util.HashMap;
import java.util.Map;


public class DeviceDisassociateSaveWrapperRequest  {
	
	
	private DeviceDisassociateSaveRequest requestObject;

	public DeviceDisassociateSaveRequest getRequestObject() {
		return requestObject;
	}

	public void setRequestObject(DeviceDisassociateSaveRequest requestObject) {
		this.requestObject = requestObject;
	}
	
}
