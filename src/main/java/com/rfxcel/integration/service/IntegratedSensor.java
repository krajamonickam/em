package com.rfxcel.integration.service;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.rfxcel.integration.dao.SensorDAO;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorRequest;
import com.rfxcel.sensor.vo.SensorResponse;
import com.rfxcel.sensor.beans.Attribute;
import com.rfxcel.sensor.beans.DeviceData;
import com.rfxcel.sensor.beans.DeviceDetail;
import com.rfxcel.sensor.beans.ProductDetail;
import com.rfxcel.sensor.beans.Sensor;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.service.SensorService;
import com.rfxcel.cache.AssociationCache;


@Path("/integrateSensor")
public class IntegratedSensor {/*

	private static Logger logger = Logger.getLogger(IntegratedSensor.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final String VALIDATION_TYPE_DEVICE_PACKAGE = "device-package";
	private static final String VALIDATION_TYPE_DEVICE = "device";
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static final SensorDAO sensorDAO = SensorDAO.getInstance();
	private static final SendumDAO sendumDAO = SendumDAO.getInstance();
	private static final AssociationCache associationCache =  AssociationCache.getInstance();
	@POST
	@Path("/getShipmentData")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getShipmentData(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			DeviceData deviceData = request.getDeviceData();
			res = SensorService.validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE_PACKAGE);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			Integer start = deviceData.getStart();
			Integer pageSize = deviceData.getPageSize();
			String deviceId = deviceData.getDeviceId();
			String packageId = deviceData.getPackageId();
			if(start == null || pageSize == null){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Pagination parameters start or pageSize cannot be null");
				return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
			}
			ArrayList<Attribute> attributes = sensorDAO.getAttributes(deviceData.getDeviceId());
			ArrayList<Object> result = sensorDAO.getShipmentDataForAttributes(attributes,deviceId, packageId , start, pageSize);
			res.setDeviceId(deviceId);
			res.setPackageId(packageId);
			Sensor sensor = new Sensor();
			sensor.setAttributeList(attributes);
			sensor.setRows(result);
			sensor.setHomeGeopointName(associationCache.getShipmentHomeGeopoint(deviceId, packageId));
			res.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getShipmentData "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.getShipmentData.  Request :: "+request +" Response :: "+res );
		}
	}

	@POST
	@Path("/getItems")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getItems(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			String searchType = request.getSearchType();
			String searchString  = request.getSearchString();
			ArrayList<String> itemList = null;
			if(searchType == null || searchType.trim().length() ==0 ||(!searchType.equals("1") && !searchType.equals("2") && !searchType.equals("3"))){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Please provide proper searchType for selecting Device(1) or TraceId(2) or Device Home(3)")).build();
			}
			if(searchString == null || searchString.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Please provide proper searchString")).build();
			}
			if(searchType.equals("1")){
				itemList = sensorDAO.searchDevices(searchString);
			}
			else if(searchType.equals("2")){
				itemList = sensorDAO.searchPackages(searchString);
			}
			else if(searchType.equals("3")){
				itemList = sensorDAO.searchShipmentHomeGeopoints(searchString);
			}
			Sensor sensor = new Sensor();
			sensor.setItemList(itemList);
			res.setSensor(sensor);

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getPackageIdsForDevice "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.getItems.  Request :: "+request +" Response :: "+res );
		}
	}
	
	
	@POST
	@Path("/getAssociatedItems")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getAssociatedItems(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			String searchType = request.getSearchType();
			String searchString  = request.getSearchString();
			ArrayList<String> itemList = null ;
			if(searchType == null || searchType.trim().length() ==0 ||(!searchType.equals("1") && !searchType.equals("2"))){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Please provide proper searchType for selecting Device(1) or TraceId(2)")).build();
			}
			if(searchString == null || searchString.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Please provide proper searchString")).build();
			}
			if(searchType.equals("1")){
				//check if device is present
				boolean present = SendumDAO.getInstance().checkIfDeviceIsPresent(searchString);
				if(!present){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Device Id "+ searchString+" is not configured in system")).build();
				}
				itemList = sensorDAO.getAssociatedPackages(searchString);
			}
			if(searchType.equals("2")){
				//Check if package is present
				boolean present = SendumDAO.getInstance().checkIfPackageIsPresent(searchString);
				if(!present){
					return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS).setResponseMessage("Package Id "+ searchString+" is not configured in system")).build();
				}
				itemList = sensorDAO.getAssociatedDevices(searchString);
			}
			Sensor sensor = new Sensor();
			sensor.setItemList(itemList);
			res.setSensor(sensor);

			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getAssociatedItems "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.getAssociatedItems.  Request :: "+request +" Response :: "+res );
		}
	}
	
	
	@POST
	@Path("/getDeviceAttributes")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getDeviceAttributes(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			DeviceData deviceData = request.getDeviceData();
			res = SensorService.validateDeviceData(deviceData, VALIDATION_TYPE_DEVICE);
			if(SensorConstant.RESP_STAT_ERROR.equals(res.getResponseStatus())){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res).build();
			}
			ArrayList<Attribute> attrList = sendumDAO.getAttributeDefinition(deviceData.getDeviceId(), false);// false to fetch attributes with show_graph =1
			Sensor sensor = new Sensor();
			sensor.setAttributeList(attrList);
			res.setSensor(sensor);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getDeviceAttributes "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.getDeviceAttributes.  Request :: "+request +" Response :: "+res );
		}
	}
	
	@POST
	@Path("/getThresholds")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getThresholds(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			DeviceDetail deviceDetail = request.getDeviceDetail();
			if(deviceDetail == null){ 
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Detail cannot be null")).build();
			}
			String deviceType = deviceDetail.getDeviceType();
			if(deviceType == null || deviceType.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type cannot be null")).build();
			}
			Boolean isPresent = SendumDAO.getInstance().checkIfDeviceTypeIsPresent(deviceType);
			if(!isPresent){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Type "+deviceType +" is not configured in system")).build();
			}
			
			ArrayList <Attribute> attributes = sensorDAO.getAttributesForDeviceType(deviceType);
			deviceDetail.setAttributes(attributes);
			res.setDeviceDetail(deviceDetail);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in getThresholds "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.getThresholds.  Request :: "+request +" Response :: "+res );
		}
	}
	
	
	
	@POST
	@Path("/checkIfPackageIsActive")
	@Consumes("application/json")
	@Produces("application/json")
	public Response checkIfPackageIsActive(SensorRequest request) {
		SensorResponse res = new SensorResponse(simpleDateFormat.format(new Date()));
		try {
			 ProductDetail productDetail = request.getProductDetail();
			if(productDetail == null){ 
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Device Data cannot be null")).build();
			}
			String packageId = productDetail.getPackageId();
			if(packageId == null || packageId.trim().length()==0){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage("Package Id cannot be null")).build();
			}
			int state = sendumDAO.getPackageState(packageId);
			boolean packageIsActive = true;
			if(state ==-1 || state == SensorConstant.DEVICE_STATE_DECOMMISSIONED){
				 packageIsActive = false;
			}
			productDetail.setPackageIsActive(packageIsActive);
			
			res.setProductDetail(productDetail);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		}catch (Exception e) {
			logger.error("Exception in checkIfPackageIsActive "+ e.getMessage());
			e.printStackTrace();
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR).setResponseMessage(e.getLocalizedMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: IntegratedSensor.checkIfPackageIsActive.  Request :: "+request +" Response :: "+res );
		}
	}
*/}
