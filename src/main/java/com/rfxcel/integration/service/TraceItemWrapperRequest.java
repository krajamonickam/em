package com.rfxcel.integration.service;

import java.util.HashMap;
import java.util.Map;


public class TraceItemWrapperRequest  {
	
	
	private TraceItemRequest requestObject;

	public TraceItemRequest getRequestObject() {
		return requestObject;
	}

	public void setRequestObject(TraceItemRequest requestObject) {
		this.requestObject = requestObject;
	}
	
}
