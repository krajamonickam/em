package com.rfxcel.integration.service;

import java.util.Map;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.control.RESTClient;
import com.rfxcel.sensor.util.SensorConstant;

/**
 * @author tejshree_kachare
 */
@Path("event")
public class EventService {
	private static final Logger logger = Logger.getLogger(EventService.class);
	private static RESTClient http = new RESTClient("", "");
	private ObjectMapper mapper = new ObjectMapper();

	@Path("/getEventItemSummary")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getEventItemSummary(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"event/getEventItemSummary";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getEventItemSummary " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	@Path("/getEventItemDetails")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getEventItemDetails(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String endpointUrl = rtsAppUrl+"event/getEventItemDetails";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getEventItemDetails " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	@Path("/selectEventDetails")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response selectEventDetails(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String endpointUrl = rtsAppUrl+"event/selectEventDetails";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: selectEventDetails " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@Path("/getRelatedEventsByEventId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRelatedEventsByEventId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String endpointUrl = rtsAppUrl+"event/getRelatedEventsByEventId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getRelatedEventsByEventId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@Path("/getEventSubItemDetails")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getEventSubItemDetails(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String endpointUrl = rtsAppUrl+"event/getEventSubItemDetails";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getEventSubItemDetails " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}

	@Path("/getCompilanceReportFlags")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getCompilanceReportFlags(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String endpointUrl = rtsAppUrl+"download/getCompilanceReportFlags";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getCompilanceReportFlags " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@Path("/getRelatedNotificationByEventId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRelatedNotificationByEventId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String endpointUrl = rtsAppUrl+"notification/getRelatedNotificationByEventId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getRelatedNotificationByEventId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@Path("/createDeviceAssociation")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response createDeviceAssociation(@HeaderParam("orgId") String orgId, Map<String, String> requestObject)
	{
		try {
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			
			String traceId = requestObject.get("traceId");
			String itemId = requestObject.get("itemId");
			String deviceId = requestObject.get("deviceId");
			String[] gtin = itemId.split("-");
			//String itemSerialNumber = ConfigCache.getInstance().getOrgConfig("rts.application.sgtin", Integer.parseInt(orgId)) + itemId;
			String itemSerialNumber = ConfigCache.getInstance().getSystemConfig("rts.application.sscc") + gtin[0] + "." + traceId;
			
			//Get Site
			
			SiteListResponse siteListResponse = null;
			String response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"common/getChildSites", "Authorization", authToken, "{}");	 
			
			} catch (Exception e) {
				logger.error("exception in getSites: " + e.getMessage());
				
			}
			siteListResponse = mapper.readValue(response, SiteListResponse.class);
			SiteList site = siteListResponse.getListData()[0];
			
			//Get Trace Item for Pallet
			
			TraceItemWrapperRequest traceItemWrapperRequest = new TraceItemWrapperRequest();
			TraceItemRequest traceItemRequest = new TraceItemRequest();
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(itemSerialNumber);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			TraceItemResponse traceItemResponse = null;
			
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem parentEntity = traceItemResponse.getListData()[0];
			
			//Get Trace Item for Device
			
			traceItemRequest.setItemIdCondition("3");
			traceItemRequest.setItemIdValue(deviceId);
			traceItemRequest.setSerialPrefix(true);
			traceItemWrapperRequest.setRequestObject(traceItemRequest );
			traceItemResponse = null;
			response = null;
			try {
				response = http.doRTSPost(rtsAppUrl+"scan/getTraceItem", "Authorization", authToken, mapper.writeValueAsString(traceItemWrapperRequest));	 
			
			} catch (Exception e) {
				logger.error("exception in getTraceItem: " + e.getMessage());
				
			}
			traceItemResponse = mapper.readValue(response, TraceItemResponse.class);
			TraceItem[] childEntityList = traceItemResponse.getListData();
			
			//Create Device Association
			DeviceAssociateSaveWrapperRequest deviceAssociateSaveWrapperRequest = new DeviceAssociateSaveWrapperRequest();
			DeviceAssociateSaveRequest deviceAssociateSaveRequest = new DeviceAssociateSaveRequest();
			deviceAssociateSaveRequest.setSiteId(site.getSiteId());
			
			deviceAssociateSaveRequest.setChildEntityList(childEntityList);
			deviceAssociateSaveRequest.setParentEntity(parentEntity);
			deviceAssociateSaveRequest.setEventType("41");
			
			deviceAssociateSaveWrapperRequest.setRequestObject(deviceAssociateSaveRequest);
			String endpointUrl = rtsAppUrl+"scan/saveDeviceAssociation";
			
			String jsonText = http.doRTSPost(endpointUrl, "Authorization", authToken, mapper.writeValueAsString(deviceAssociateSaveWrapperRequest));
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in EventService :: getRelatedNotificationByEventId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
}
