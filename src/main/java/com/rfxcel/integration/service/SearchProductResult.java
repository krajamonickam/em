package com.rfxcel.integration.service;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class SearchProductResult {

	private int offset;
	private int limit;
	private String orderBy;
	private String sortColumn;
	private int totalFilteredRecords;
	private int noOfRecords;
	private SearchProduct[] pageItems;
	public int getOffset() {
		return offset;
	}
	public void setOffset(int offset) {
		this.offset = offset;
	}
	public int getLimit() {
		return limit;
	}
	public void setLimit(int limit) {
		this.limit = limit;
	}
	public String getOrderBy() {
		return orderBy;
	}
	public void setOrderBy(String orderBy) {
		this.orderBy = orderBy;
	}
	public String getSortColumn() {
		return sortColumn;
	}
	public void setSortColumn(String sortColumn) {
		this.sortColumn = sortColumn;
	}
	public int getTotalFilteredRecords() {
		return totalFilteredRecords;
	}
	public void setTotalFilteredRecords(int totalFilteredRecords) {
		this.totalFilteredRecords = totalFilteredRecords;
	}
	public int getNoOfRecords() {
		return noOfRecords;
	}
	public void setNoOfRecords(int noOfRecords) {
		this.noOfRecords = noOfRecords;
	}
	public SearchProduct[] getPageItems() {
		return pageItems;
	}
	public void setPageItems(SearchProduct[] pageItems) {
		this.pageItems = pageItems;
	}
	
	
}
