package com.rfxcel.integration.service;


import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

import javax.ws.rs.Consumes;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONArray;
import org.json.JSONObject;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.sensor.control.EventController;
import com.rfxcel.sensor.control.RESTClient;
import com.rfxcel.sensor.dao.SendumDAO;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.vo.SensorResponse;

@Path("item")
public class ItemService {
	private static final Logger logger = Logger.getLogger(ItemService.class);
	private static final SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static RESTClient http = new RESTClient("", "");

	@Path("/getItemSummaryByTraceEntityId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getItemSummaryByTraceEntityId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"item/getItemSummaryByTraceEntityId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getItemSummaryByTraceEntityId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	@Path("/getItemDetailsByTraceEntityId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getItemDetailsByTraceEntityId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"item/getItemDetailsByTraceEntityId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getItemDetailsByTraceEntityId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	@Path("/getParentTraceItemByTraceEntityId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getParentTraceItemByTraceEntityId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"item/getParentTraceItemByTraceEntityId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getParentTraceItemByTraceEntityId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	
	
	@Path("/getRelatedNotificationByItemId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getRelatedNotificationByItemId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"item/getRelatedNotificationByItemId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getRelatedNotificationByItemId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	
	@Path("/getSubItemsByTraceEntityId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getSubItemsByTraceEntityId(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"item/getSubItemsByTraceEntityId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getSubItemsByTraceEntityId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
	@Path("/getEventsByTraceEntityId")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getEventsByTraceEntityId(@HeaderParam("orgId") String orgId, String requestObject)	
	{
		SensorResponse response = new SensorResponse(simpleDateFormat.format(new Date()));
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getEventItemSummaryURL = rtsAppUrl+"item/getEventsByTraceEntityId";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getEventItemSummaryURL, "Authorization", authToken, requestObject);

			ObjectMapper mapper = new ObjectMapper();	
			JSONObject jsonObject = new JSONObject(jsonText);
			ArrayList<EventController.EventsData> eventDataList = new ArrayList<EventController.EventsData>();
			if(jsonObject.has("listData")){					
				JSONArray eventList = jsonObject.getJSONArray("listData");
				Object[] events = mapper.readValue(eventList.toString(), Object[].class);
				for(Object event : events){
					HashMap<String, Object> eventMap =  (HashMap<String, Object>) event;
					EventController.EventsData eventsData = (EventController.EventsData) mapper.convertValue(eventMap, EventController.EventsData.class);
					eventDataList.add(eventsData);
				}
			}
			
			JSONObject requestJson = new JSONObject(requestObject);
			String traceEntityId = requestJson.get("traceEntityId").toString();
			
			// get sensor data list from ITT
			/*
			String deviceId = EventController.getInstance().getDeviceByTraceEntId(traceEntityId);
			String packageId = requestJson.get("traceId").toString();
			String productId = requestJson.get("itemId").toString();
			*/
			
			/*
			String deviceId = "99000512107823";
			String packageId = "5000033500";
			String productId = "0364896-5";
			*/
			
			
			String deviceId = requestJson.get("deviceId").toString();
			String packageId = requestJson.get("packageId").toString();
			String productId = requestJson.get("productId").toString();
			
			logger.info("retrieve sensor data: " + deviceId + ", " + packageId + ", " + productId);
			
			ArrayList<com.rfxcel.sensor.util.Sensor> sensorDataList = SendumDAO.getInstance().getSensorData(deviceId, packageId, productId);
			response.setListData(eventDataList);
			response.setSensorDataList(sensorDataList);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(response.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS)).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getEventsByTraceEntityId " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}	
	
	@Path("/getClusterMapItemLocations")
	@POST
	@Produces(MediaType.APPLICATION_JSON)
	@Consumes(MediaType.APPLICATION_JSON)
	public Response getClusterMapItemLocations(@HeaderParam("orgId") String orgId, String requestObject)
	{
		try {			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", Integer.parseInt(orgId));
			if(rtsAppUrl == null){
				rtsAppUrl = "http://localhost:9000/rts/rest/";
			}
			String getMapItemLocationsURL = rtsAppUrl+"item/getClusterMapItemLocations";
			String authToken = ConfigCache.getInstance().getOrgConfig("rts.application.auth", Integer.parseInt(orgId));
			String jsonText = http.doRTSPost(getMapItemLocationsURL, "Authorization", authToken, requestObject);
			return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(jsonText).build();
		} catch (Exception e) {
			e.printStackTrace();
			logger.error("Error in ItemService :: getClusterMapItemLocations " + e.getMessage());
			return Response.status(SensorConstant.RESPONSECODE_ERROR).entity(e.getMessage()).build();
		}
	}
	
}
