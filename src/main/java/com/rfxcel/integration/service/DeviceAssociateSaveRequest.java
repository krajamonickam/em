package com.rfxcel.integration.service;

public class DeviceAssociateSaveRequest  {
	

	private String eventType;
	private String packagingDate;
	private String productionDate;
	private String siteId;
	
	private TraceItem parentEntity;
	private TraceItem[] childEntityList;
	public String getEventType() {
		return eventType;
	}
	public void setEventType(String eventType) {
		this.eventType = eventType;
	}
	public String getPackagingDate() {
		return packagingDate;
	}
	public void setPackagingDate(String packagingDate) {
		this.packagingDate = packagingDate;
	}
	public String getProductionDate() {
		return productionDate;
	}
	public void setProductionDate(String productionDate) {
		this.productionDate = productionDate;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public TraceItem getParentEntity() {
		return parentEntity;
	}
	public void setParentEntity(TraceItem parentEntity) {
		this.parentEntity = parentEntity;
	}
	public TraceItem[] getChildEntityList() {
		return childEntityList;
	}
	public void setChildEntityList(TraceItem[] childEntityList) {
		this.childEntityList = childEntityList;
	}

	
}
