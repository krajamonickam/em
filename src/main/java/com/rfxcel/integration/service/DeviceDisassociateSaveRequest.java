package com.rfxcel.integration.service;

public class DeviceDisassociateSaveRequest  {
	

	private boolean isChildren;
	private boolean isParent;
	private String siteId;
	private TraceItem[] entityList;
	public boolean getIsChildren() {
		return isChildren;
	}
	public void setIsChildren(boolean isChildren) {
		this.isChildren = isChildren;
	}
	public boolean getIsParent() {
		return isParent;
	}
	public void setIsParent(boolean isParent) {
		this.isParent = isParent;
	}
	public String getSiteId() {
		return siteId;
	}
	public void setSiteId(String siteId) {
		this.siteId = siteId;
	}
	public TraceItem[] getEntityList() {
		return entityList;
	}
	public void setEntityList(TraceItem[] entityList) {
		this.entityList = entityList;
	}
	
	
}
