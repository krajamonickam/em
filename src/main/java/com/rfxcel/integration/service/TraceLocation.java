package com.rfxcel.integration.service;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceLocation {

	@JsonProperty("versionId")
	private long versionId;
	@JsonProperty("mfrSiteExtId")
	private String mfrSiteExtId;
	@JsonProperty("mfrId")
	private long mfrId;
	@JsonProperty("versionNumber")
	private String versionNumber;
	@JsonProperty("mfrLocationName")
	private String mfrLocationName;
	@JsonProperty("addressLine1")
	private String addressLine1;
	@JsonProperty("addressLine2")
	private String addressLine2;
	@JsonProperty("city")
	private String city;
	@JsonProperty("state")
	private String state;
	@JsonProperty("zipCode")
	private String zipCode;
	
	public long getVersionId() {
		return versionId;
	}
	public void setVersionId(long versionId) {
		this.versionId = versionId;
	}
	public String getMfrSiteExtId() {
		return mfrSiteExtId;
	}
	public void setMfrSiteExtId(String mfrSiteExtId) {
		this.mfrSiteExtId = mfrSiteExtId;
	}
	public long getMfrId() {
		return mfrId;
	}
	public void setMfrId(long mfrId) {
		this.mfrId = mfrId;
	}
	public String getVersionNumber() {
		return versionNumber;
	}
	public void setVersionNumber(String versionNumber) {
		this.versionNumber = versionNumber;
	}
	public String getMfrLocationName() {
		return mfrLocationName;
	}
	public void setMfrLocationName(String mfrLocationName) {
		this.mfrLocationName = mfrLocationName;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	
	
}
