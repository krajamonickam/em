package com.rfxcel.integration.service;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonInclude(JsonInclude.Include.NON_NULL)
public class TraceItem {

	private long productId;
	private String productName;
	private int quantity;
	private String productType;
	private String unit;
	
	//added by nishanth to get Product Information 
	private String dosageForm;
	private String packageForm;
	private String productCode;
	private String enterpriseName;
	private String entExtId;
	private long manufacturerId;
	private String drugId;
	private String prodCodeType;
	private String uomType;
	private String itemType;
	private String expiryDate;
	private String lotNumber;
	private String serialNumber;
	private long entityId;
	private long parentEntityId;
	private String quantityName;
	private long rootEntId;
	private int dispositionId;
	private String dispositionValue;
	private String mfrLocExtId;
	private String disposition;
	private String lotId;
	private String itemTypeValue;
	private String rootEntExtId;
	private String rootItemType;
	private String rootItemTypeValue;
	private String containerSize;
	private String userId;
	private String userName;
	private String shipEventId;
	
	public TraceItem(){}
	
	public TraceItem(long productId, String productName, int quantity,
			String productType, String unit) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.quantity = quantity;
		this.productType = productType;
		this.unit = unit;
	}

	
	public TraceItem(long productId, String productName, String productCode, String drugId,String uomType,String itemType,long manufacturerId,String entExtId) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCode = productCode;
		this.drugId = drugId;
		this.uomType=uomType;	
		this.itemType=itemType;
		this.manufacturerId=manufacturerId;
		this.entExtId=entExtId;
	}

	
	public TraceItem(long productId, String productName, String productCode, String drugId,String uomType,String itemType,long manufacturerId,String entExtId,long entId,String prodCodeType) {
		super();
		this.productId = productId;
		this.productName = productName;
		this.productCode = productCode;
		this.drugId = drugId;
		this.uomType=uomType;	
		this.itemType=itemType;
		this.manufacturerId=manufacturerId;
		this.entExtId=entExtId;
		this.entityId=entId;
		this.prodCodeType=prodCodeType;
	}
	public long getProductId() {
		return productId;
	}

	public void setProductId(long productId) {
		this.productId = productId;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public int getQuantity() {
		return quantity;
	}

	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}

	public String getProductType() {
		return productType;
	}

	public void setProductType(String productType) {
		this.productType = productType;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getDosageForm() {
		return dosageForm;
	}

	public void setDosageForm(String dosageForm) {
		this.dosageForm = dosageForm;
	}

	public String getPackageForm() {
		return packageForm;
	}

	public void setPackageForm(String packageForm) {
		this.packageForm = packageForm;
	}

	public String getProductCode() {
		return productCode;
	}

	public void setProductCode(String productCode) {
		this.productCode = productCode;
	}

	public String getEnterpriseName() {
		return enterpriseName;
	}

	public void setEnterpriseName(String enterpriseName) {
		this.enterpriseName = enterpriseName;
	}

	public long getManufacturerId() {
		return manufacturerId;
	}

	public void setManufacturerId(long manufacturerId) {
		this.manufacturerId = manufacturerId;
	}

	public String getDrugId() {
		return drugId;
	}

	public void setDrugId(String drugId) {
		this.drugId = drugId;
	}

	public String getProdCodeType() {
		return prodCodeType;
	}

	public void setProdCodeType(String prodCodeType) {
		this.prodCodeType = prodCodeType;
	}

	public String getUomType() {
		return uomType;
	}

	public void setUomType(String uomType) {
		this.uomType = uomType;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getExpiryDate() {
		return expiryDate;
	}

	public void setExpiryDate(String expiryDate) {
		this.expiryDate = expiryDate;
	}

	public String getLotNumber() {
		return lotNumber;
	}

	public void setLotNumber(String lotNumber) {
		this.lotNumber = lotNumber;
	}

	public String getSerialNumber() {
		return serialNumber;
	}

	public void setSerialNumber(String serialNumber) {
		this.serialNumber = serialNumber;
	}

	public String getEntExtId() {
		return entExtId;
	}

	public void setEntExtId(String entExtId) {
		this.entExtId = entExtId;
	}

	public long getEntityId() {
		return entityId;
	}

	public void setEntityId(long entityid) {
		this.entityId = entityid;
	}

	public long getParentEntityId() {
		return parentEntityId;
	}

	public void setParentEntityId(long parentEntityId) {
		this.parentEntityId = parentEntityId;
	}

	public String getQuantityName() {
		return quantityName;
	}

	public void setQuantityName(String quantityName) {
		this.quantityName = quantityName;
	}

	public long getRootEntId() {
		return rootEntId;
	}

	public void setRootEntId(long rootEntId) {
		this.rootEntId = rootEntId;
	}

	public String getDispositionValue() {
		return dispositionValue;
	}

	public void setDispositionValue(String dispositionValue) {
		this.dispositionValue = dispositionValue;
	}

	public String getMfrLocExtId() {
		return mfrLocExtId;
	}

	public void setMfrLocExtId(String mfrLocExtId) {
		this.mfrLocExtId = mfrLocExtId;
	}

	public String getDisposition() {
		return disposition;
	}

	public void setDisposition(String disposition) {
		this.disposition = disposition;
	}

	public int getDispositionId() {
		return dispositionId;
	}

	public void setDispositionId(int dispositionId) {
		this.dispositionId = dispositionId;
	}

	public String getLotId() {
		return lotId;
	}

	public void setLotId(String lotId) {
		this.lotId = lotId;
	}

	public String getItemTypeValue() {
		return itemTypeValue;
	}

	public void setItemTypeValue(String itemTypeValue) {
		this.itemTypeValue = itemTypeValue;
	}

	public String getRootEntExtId() {
		return rootEntExtId;
	}

	public void setRootEntExtId(String rootEntExtId) {
		this.rootEntExtId = rootEntExtId;
	}

	public String getRootItemType() {
		return rootItemType;
	}

	public void setRootItemType(String rootItemType) {
		this.rootItemType = rootItemType;
	}

	public String getRootItemTypeValue() {
		return rootItemTypeValue;
	}

	public void setRootItemTypeValue(String rootItemTypeValue) {
		this.rootItemTypeValue = rootItemTypeValue;
	}

	public String getContainerSize() {
		return containerSize;
	}

	public void setContainerSize(String containerSize) {
		this.containerSize = containerSize;
	}

	public String getUserId() {
		return userId;
	}

	public void setUserId(String userId) {
		this.userId = userId;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getShipEventId() {
		return shipEventId;
	}

	public void setShipEventId(String shipEventId) {
		this.shipEventId = shipEventId;
	}

	
}
