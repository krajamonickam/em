package com.rfxcel.auth2;

import java.util.Date;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;

import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.cache.ConfigCache;

public class TokenManager{
	private static Logger logger = Logger.getLogger(TokenManager.class);
    private ConcurrentHashMap<String,ConcurrentHashMap<String,Object>> tokens = new ConcurrentHashMap<String,ConcurrentHashMap<String,Object>>();	//TODO check the concurrent access and set the iniCapacity and concurrency factor
    private static TokenManager instance;
    private static boolean authenticate  = false;

    public static TokenManager getInstance(){
    	if (instance == null) {
    		instance = new TokenManager();
    		authenticate = getAuthenticate();
    	}
    	return instance;
    }

    /**
     * 
     * @param token
     * @param user
     * @param expiresIn
     * @param userId 
     * @param groupId 
     */
    public void addToken(String token,String user,Date expiresIn, int userType, int orgId, Long userId, Long groupId) {
    	logger.info("** addToken " + token + " user " + user + " expires " + expiresIn.toString());
    	ConcurrentHashMap<String, Object> map = new ConcurrentHashMap<String, Object>(4,0.75f,2);
    	 map.put(AuthConstants.USER,user);
    	 map.put(AuthConstants.LAST_ACCESS_TIME, new Date());			//setting the last access time as current time
    	 map.put(AuthConstants.USER_TYPE, new Integer(userType));
    	 map.put(AuthConstants.ORGANIZATION_ID, orgId);
    	 map.put(AuthConstants.USER_ID, userId);
    	 map.put(AuthConstants.GROUP_ID, groupId);
         tokens.put(token,map);
    }
    
    /**
     * 
     * @param token
     * @return
     */
    public boolean removeToken(String token) {
    	logger.info("** removeToken " + token);
        if (tokens.containsKey(token)) {
        	tokens.remove(token);
        	return true;
		}  
        return false;
   }
    
    /**
     * 
     * @param token
     * @return
     */
    public boolean isValidToken(String token, Integer orgId) {
    	if (tokens.size()>0) {
        	Map<String, Object> tokenMap = tokens.get(token);
        	/**
        	 * If token is not null, 
        	 * Check if orgId in request is same at that of in token map
        	 * check the lastAccessTime.
        	 *  If the time interval between last access time and now is more than token invalidate time.. remove token and return false
        	 */
        	if (tokenMap != null) {
        		Integer organizationId = (Integer) tokenMap.get(AuthConstants.ORGANIZATION_ID);
        		if(orgId != null && organizationId != null && organizationId !=0 && organizationId.intValue() != orgId.intValue()){
        	    	logger.info("** isValidToken " + token + " token orgId(" + organizationId + ") mismatch orgId(" + orgId + ")");
        			return false;
        		}
        		Date curr = new Date();
        		Date lastAccessTime = (Date)tokenMap.get(AuthConstants.LAST_ACCESS_TIME);
            	long diff =  curr.getTime() - lastAccessTime.getTime(); //in milliseconds
            	String expiryTime = ConfigCache.getInstance().getSystemConfig("token_expires_in");
        		Long expTime = AuthConstants.TOKENEXPIRESIN;//Assigning default value just in case property is not configured in DB
				if(expiryTime!= null)
        			expTime = Long.valueOf(expiryTime);
            	if (diff > expTime) {	//if diff is greater than token Expiry time, token is invalid
        			removeToken(token);
        			return false;
        		}else{
        			//Set the last access time for this token
        			tokenMap.put(AuthConstants.LAST_ACCESS_TIME, curr);
        		}
             return true;
			}
		}
        return false;
    }
    /**
     * 
     * @param token
     * @return user
     */
    public String getUser(String token) {
    	if (tokens.size()>0) {
        	Map<String, Object> tokenMap = tokens.get(token);
        	if (tokenMap != null) {
        		return((String)tokenMap.get(AuthConstants.USER));
			}
		}
        return(null);
    }
    /**
     * 
     * @param token
     * @return user
     */
    public Integer getUserType(String token) {
    	if (tokens.size()>0) {
        	Map<String, Object> tokenMap = tokens.get(token);
        	if (tokenMap != null) {
        		return((Integer)tokenMap.get(AuthConstants.USER_TYPE));
			}
		}
        return(null);
    }
	/**
	 * 
	 * @return
	 */
    public static boolean getAuthenticate(){
    	boolean authenticate = false;
    	String authenticateProp;
    	authenticateProp = ConfigCache.getInstance().getSystemConfig(SensorConstant.AUTHENTICATE_REQUEST);
    	if (authenticateProp == null){		// if property is not configured, setting authenticate to false
    		authenticate = false;
    	}else{
    		authenticate = Boolean.valueOf(authenticateProp);
    	}
    	if (!authenticate) {
        	logger.info("** Authentication is " + authenticate + " **");
    	}
    	return authenticate;
    }
    
    /**
     * @param request
     * @return
     */
	public boolean validateToken(HttpServletRequest request, Integer orgId){
		if (!authenticate) {
			return(true);
		}
		boolean valid = false;		
		String token = request.getHeader("Authorization");
		if (token == null ){
			valid = false;
		}else if(orgId == null){
			valid = false;
		}else{
			valid = isValidToken(token, orgId);
		}
		return valid;
	}
    /**
     * @param request
     * @return
     */
	public String getToken(HttpServletRequest request){
		String token = request.getHeader("Authorization");
		return(token);
	}
	
	
    
    /**
     * 
     * @param request
     * @return
     */
    public String getUserForToken(HttpServletRequest request){
    	String token = request.getHeader("Authorization");
    	if (token == null) return(null);
    	ConcurrentHashMap<String, Object> map = tokens.get(token);
    	String userName;
    	if(map != null){
    		userName = map.get(AuthConstants.USER)!= null? (String)map.get(AuthConstants.USER): null ;
    	}else{
    		userName = null;
    	}
    	return userName;
    }
    
    
   /**
    * @param httpRequest
    * @return
    */
	public Long getUserId(HttpServletRequest httpRequest) {
		Long userId;
		String token = httpRequest.getHeader("Authorization");
		if (token == null ){
			return null;
		}
		ConcurrentHashMap<String, Object> map = tokens.get(token);
		if(map != null){
    		userId = map.get(AuthConstants.USER_ID)!= null? (Long)map.get(AuthConstants.USER_ID): null ;
    	}else{
    		userId = null;
    	}
		return userId;
	}
	
	
	
	/**
	    * @param httpRequest
	    * @return
	    */
		public Long getGroupId(HttpServletRequest httpRequest) {
			Long groupId;
			String token = httpRequest.getHeader("Authorization");
			if (token == null ){
				return null;
			}
			ConcurrentHashMap<String, Object> map = tokens.get(token);
			if(map != null){
				groupId = map.get(AuthConstants.GROUP_ID)!= null? (Long)map.get(AuthConstants.GROUP_ID): null ;
	    	}else{
	    		groupId = null;
	    	}
			return groupId;
		}
		
		/**
		 * 
		 * @param httpRequest
		 * @return
		 */
		public Integer getUserType(HttpServletRequest httpRequest){
			Integer userType = null;
			String token = httpRequest.getHeader("Authorization");
			if(token == null){
				return null;
			}
			ConcurrentHashMap<String, Object> map = tokens.get(token);
			if(map != null){
				userType = map.get(AuthConstants.USER_TYPE) != null ? (Integer)map.get(AuthConstants.USER_TYPE) : null;
			}else{
				userType = null;
			}
			return userType;
			
		}
}