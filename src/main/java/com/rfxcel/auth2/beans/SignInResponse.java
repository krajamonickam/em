package com.rfxcel.auth2.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.rfxcel.org.entity.User;


@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class SignInResponse  {
	private String responseStatus;
	private String responseMessage;
	private String dateTime;
	private String authToken;
	private Long expiresIn;
	private String timeZone;
	private Integer autoRefreshFreq; 
	private Boolean autoRefresh;
	private Integer orgId;
	private Long groupId;
	private User currentUser;
	private Double centerLat;
	private Double centerLong;
	private Integer mapZoom;
	private String mapType;
	private String helpdeskContactMessage;
	private Boolean systemDemo;
	private Boolean clickToChat;
	private Boolean fetchESData;
	private Boolean newProfileScreen;
	private Boolean alertDeviceLogSearch;
	private String shard;
	private String rtsURL;
	public String getRtsURL() {
		return rtsURL;
	}

	public void setRtsURL(String rtsURL) {
		this.rtsURL = rtsURL;
	}

	/**
	 * 
	 * @param dateTime
	 */
	public SignInResponse(String dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * 
	 */
	public SignInResponse() {
	}
	/**
	 * @return the expiresIn
	 */
	@JsonProperty("expiresIn")
	public Long getExpiresIn() {
		return expiresIn;
	}
	/**
	 * @param expiresIn the expiresIn to set
	 */
	public void setExpiresIn(Long expiresIn) {
		this.expiresIn = expiresIn;
	}
	/**
	 * @return the responseStatus
	 */
	@JsonProperty("responseStatus")
	public String getResponseStatus() {
		return responseStatus;
	}
	/**
	 * @param responseStatus the responseStatus to set
	 */
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	
	
	/**
	 * @return the responseMessage
	 */
	public String getResponseMessage() {
		return responseMessage;
	}

	/**
	 * @param responseMessage the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/**
	 * @return the timestamp
	 */
	@JsonProperty("dateTime")
	public String getTimeStamp() {
		return dateTime;
	}
	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimeStamp(String dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return the authToke
	 */
	@JsonProperty("authToken")
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToke the authToke to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}

	/**
	 * @return the timeZone
	 */
	@JsonProperty("timeZone")
	public String getTimeZone() {
		return timeZone;
	}

	/**
	 * @param timeZone the timeZone to set
	 */
	public void setTimeZone(String timeZone) {
		this.timeZone = timeZone;
	}

	/**
	 * @return the autoRefreshFreq
	 */
	@JsonProperty("autoRefreshFreq")
	public Integer getAutoRefreshFreq() {
		return autoRefreshFreq;
	}

	/**
	 * @param autoRefreshFreq the autoRefreshFreq to set
	 */
	public void setAutoRefreshFreq(Integer autoRefreshFreq) {
		this.autoRefreshFreq = autoRefreshFreq;
	}
	
	

	/**
	 * @return the autoRefresh
	 */
	@JsonProperty("autoRefresh")
	public Boolean getAutoRefresh() {
		return autoRefresh;
	}

	/**
	 * @param autoRefresh the autoRefresh to set
	 */
	public void setAutoRefresh(Boolean autoRefresh) {
		this.autoRefresh = autoRefresh;
	}

	@JsonProperty("centerLat")
	public Double getCenterLat() {
		return centerLat;
	}

	public void setCenterLat(Double centerLat) {
		this.centerLat = centerLat;
	}

	@JsonProperty("centerLong")
	public Double getCenterLong() {
		return centerLong;
	}

	public void setCenterLong(Double centerLong) {
		this.centerLong = centerLong;
	}

	@JsonProperty("mapZoom")
	public Integer getMapZoom() {
		return mapZoom;
	}

	public void setMapZoom(Integer mapZoom) {
		this.mapZoom = mapZoom;
	}

	@JsonProperty("mapType")
	public String getMapType() {
		return mapType;
	}

	public void setMapType(String mapType) {
		this.mapType = mapType;
	}

	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}

	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}

	/**
	 * @return the groupId
	 */
	public Long getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(Long groupId) {
		this.groupId = groupId;
	}

	/**
	 * 
	 * @return the helpdeskContactEmail
	 */
	public String getHelpdeskContactMessage() {
		return helpdeskContactMessage;
	}

	/**
	 * 
	 * @param helpdeskContactEmail the helpdeskContactEmail to set
	 */
	public void setHelpdeskContactMessage(String helpdeskContactMessage) {
		this.helpdeskContactMessage = helpdeskContactMessage;
	}


	/**
	 * 
	 * @return the systemDemo
	 */
	public Boolean getSystemDemo() {
		return systemDemo;
	}

	/**
	 * 
	 * @param systemDemo the systemDemo to set
	 */
	public void setSystemDemo(Boolean systemDemo) {
		this.systemDemo = systemDemo;
	}
	
	
	
	@JsonProperty("clickToChat")
	public Boolean getClickToChat() {
		return clickToChat;
	}

	public void setClickToChat(Boolean clickToChat) {
		this.clickToChat = clickToChat;
	}

	/**
	 * 
	 * @return the fetchESData
	 */
	public Boolean getFetchESData() {
		return fetchESData;
	}

	/**
	 * 
	 * @param fetchESData the fetchESData to set
	 */
	public void setFetchESData(Boolean fetchESData) {
		this.fetchESData = fetchESData;
	}
	
	/**
	 * 
	 * @return the NewProfileScreen
	 */

	public Boolean getNewProfileScreen() {
		return newProfileScreen;
	}
	/**
	 * 
	 * @param newProfileScreen TO SET
	 */
	public void setNewProfileScreen(Boolean newProfileScreen) {
		this.newProfileScreen = newProfileScreen;
	}

	/**
	 * @return the alertDeviceLogSearch
	 */
	public Boolean getAlertDeviceLogSearch() {
		return alertDeviceLogSearch;
	}

	/**
	 * @param alertDeviceLogSearch the alertDeviceLogSearch to set
	 */
	public void setAlertDeviceLogSearch(Boolean alertDeviceLogSearch) {
		this.alertDeviceLogSearch = alertDeviceLogSearch;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "SignInResponse ["
				+ (responseStatus != null ? "responseStatus=" + responseStatus	+ ", " : "")
				+ (responseMessage != null ? "responseMessage="	+ responseMessage + ", " : "")
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (authToken != null ? "authToken=" + authToken + ", " : "")
				+ (expiresIn != null ? "expiresIn=" + expiresIn + ", " : "")
				+ (timeZone != null ? "timeZone=" + timeZone + ", " : "")
				+ (autoRefreshFreq != null ? "autoRefreshFreq="	+ autoRefreshFreq + ", " : "")
				+ (autoRefresh != null ? "autoRefresh=" + autoRefresh + ", ": "") + (orgId != null ? "orgId=" + orgId + ", " : "")
				+ (groupId != null ? "groupId=" + groupId + ", " : "")
				+ (helpdeskContactMessage != null ? "helpdeskContactEmail=" + helpdeskContactMessage + ", " : "")
				+ (systemDemo != null ? "systemDemo=" + systemDemo + ", " : "")
				+ (clickToChat != null ? "clickToChat=" + clickToChat + ", " : "")
				+ (newProfileScreen != null ? "newProfileScreen=" + newProfileScreen + ", " : "")
				+ (fetchESData != null ? "fetchESData=" + fetchESData : "") +"]";
	}

	/**
	 * @return the currentUser
	 */
	public User getCurrentUser() {
		return currentUser;
	}

	/**
	 * @param currentUser the currentUser to set
	 */
	public void setCurrentUser(User currentUser) {
		this.currentUser = currentUser;
	}

	public String getShard() {
		return shard;
	}

	public void setShard(String shard) {
		this.shard = shard;
	}

	
}
