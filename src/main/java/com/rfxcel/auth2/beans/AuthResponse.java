package com.rfxcel.auth2.beans;

import org.codehaus.jackson.annotate.JsonProperty;
import org.codehaus.jackson.map.annotate.JsonSerialize;

@JsonSerialize(include=JsonSerialize.Inclusion.NON_NULL)
public class AuthResponse {

	private String dateTime;
	private String responseStatus;
	private String responseMessage;
	
	public AuthResponse(){
	}
	
	public AuthResponse(String dateTime) {
		this.dateTime = dateTime;
	}
	
	/**
	 * @return the dateTime
	 */
	@JsonProperty("dateTime")
	public String getDateTime() {
		return dateTime;
	}
	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return the responseStatus
	 */
	@JsonProperty("responseStatus")
	public String getResponseStatus() {
		return responseStatus;
	}
	/**
	 * @param responseStatus the responseStatus to set
	 */
	public void setResponseStatus(String responseStatus) {
		this.responseStatus = responseStatus;
	}
	/**
	 * @return the responseMessage
	 */
	@JsonProperty("responseMessage")
	public String getResponseMessage() {
		return responseMessage;
	}
	/**
	 * @param responseMessage the responseMessage to set
	 */
	public void setResponseMessage(String responseMessage) {
		this.responseMessage = responseMessage;
	}

	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuthResponse ["
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (responseStatus != null ? "responseStatus=" + responseStatus+ ", " : "")
				+ (responseMessage != null ? "responseMessage="	+ responseMessage : "") + "]";
	}
}
