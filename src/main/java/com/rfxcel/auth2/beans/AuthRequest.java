package com.rfxcel.auth2.beans;

import org.codehaus.jackson.annotate.JsonProperty;

public class AuthRequest {
	
	private String userLogin;
	private String password;
	private String clientId;
	private String secret;
	private String dateTime;
	private String token;
	private String url;
	private String authToken;
	private String passwordAction;
	private Integer orgId;
	
	/**
	 * @return the userLogin
	 */
	public String getUserLogin() {
		return userLogin;
	}
	/**
	 * @param userLogin the userLogin to set
	 */
	public void setUserLogin(String userLogin) {
		this.userLogin = userLogin;
	}
	
	/**
	 * @return the clientId
	 */
	@JsonProperty("clientId")
	public String getClientId() {
		return clientId;
	}
	/**
	 * @param clientId the clientId to set
	 */
	public void setClientId(String clientId) {
		this.clientId = clientId;
	}
	/**
	 * @return the secret
	 */
	@JsonProperty("secret")
	public String getSecret() {
		return secret;
	}
	/**
	 * @param secret the secret to set
	 */
	public void setSecret(String secret) {
		this.secret = secret;
	}

	
	/**
	 * @return the password
	 */
	@JsonProperty("password")
	public String getPassword() {
		return password;
	}
	/**
	 * @param password the password to set
	 */
	public void setPassword(String password) {
		this.password = password;
	}
	/**
	 * @return the dateTime
	 */
	@JsonProperty("dateTime")
	public String getDateTime() {
		return dateTime;
	}
	/**
	 * @param dateTime the dateTime to set
	 */
	public void setDateTime(String dateTime) {
		this.dateTime = dateTime;
	}
	/**
	 * @return the token
	 */
	@JsonProperty("token")
	public String getToken() {
		return token;
	}
	/**
	 * @param token the token to set
	 */
	public void setToken(String token) {
		this.token = token;
	}
	/**
	 * @return the url
	 */
	@JsonProperty("url")
	public String getUrl() {
		return url;
	}
	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}
	/**
	 * @return the authToken
	 */
	@JsonProperty("authToken")
	public String getAuthToken() {
		return authToken;
	}
	/**
	 * @param authToken the authToken to set
	 */
	public void setAuthToken(String authToken) {
		this.authToken = authToken;
	}
	
	/**
	 * @return the passwordAction
	 */
	public String getPasswordAction() {
		return passwordAction;
	}
	/**
	 * @param passwordAction the passwordAction to set
	 */
	public void setPasswordAction(String passwordAction) {
		this.passwordAction = passwordAction;
	}
	/**
	 * @return the orgId
	 */
	public Integer getOrgId() {
		return orgId;
	}
	/**
	 * @param orgId the orgId to set
	 */
	public void setOrgId(Integer orgId) {
		this.orgId = orgId;
	}
	/* (non-Javadoc)
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "AuthRequest ["
				+ (userLogin != null ? "userName=" + userLogin + ", " : "")
				+ (password != null ? "password=" + password + ", " : "")
				+ (clientId != null ? "clientId=" + clientId + ", " : "")
				+ (secret != null ? "secret=" + secret + ", " : "")
				+ (dateTime != null ? "dateTime=" + dateTime + ", " : "")
				+ (token != null ? "token=" + token + ", " : "")
				+ (url != null ? "url=" + url + ", " : "")
				+ (authToken != null ? "authToken=" + authToken : "") 
				+ (orgId != null ? "orgId=" + orgId : "") + "]";
	
	}
	
}
