package com.rfxcel.auth2;

import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.oltu.oauth2.as.issuer.MD5Generator;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuer;
import org.apache.oltu.oauth2.as.issuer.OAuthIssuerImpl;

import com.rfxcel.auth2.beans.AuthRequest;
import com.rfxcel.auth2.beans.SignInResponse;
import com.rfxcel.auth2.dao.AuthDao;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.org.dao.UserDAO;
import com.rfxcel.org.entity.User;

public class TokenUtility {	
	
	private  static  TokenManager tokenMngr = TokenManager.getInstance();
	private static SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	public  static SignInResponse generateToken(AuthRequest request) throws Exception{
		SignInResponse response = new SignInResponse();
		OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

		// do checking for different grant types
		if (!checkUserPass(request.getUserLogin(), request.getPassword())) {
			return buildInvalidUserPassResponse();
		}

		final String accessToken = oauthIssuerImpl.accessToken();
		
		String expiryTime = ConfigCache.getInstance().getSystemConfig("token_expires_in");
		Long expirTimeInMillis= AuthConstants.TOKENEXPIRESIN;//Assigning default value just in case property is not configured in DB
		if(expiryTime!= null)
			expirTimeInMillis = Long.valueOf(expiryTime);
		
		final Date expireIn = new Date(new Date().getTime()+expirTimeInMillis);
		User user = UserDAO.getInstance().getUserByloginName(request.getUserLogin());
		tokenMngr.addToken(accessToken,request.getUserLogin(),expireIn, user.getUserType(), user.getOrgId(), user.getUserId(), user.getGroupId());
		response.setAuthToken(accessToken);
		response.setExpiresIn(expirTimeInMillis);
		response.setShard(user.getShard().toString());
		response.setResponseStatus("success");
		response.setTimeStamp(simpleDateFormat.format(new Date()));
		response.setCurrentUser(user);
		return response;
	}
	
	/**
	 * <p>Generate token for support user, validate user by userLogin.</p>
	 * @param request
	 * @return
	 * @throws Exception
	 */
	public static SignInResponse generateTokenForSupportUser(AuthRequest request) throws Exception{
		SignInResponse response = new SignInResponse();
		OAuthIssuer oauthIssuerImpl = new OAuthIssuerImpl(new MD5Generator());

		final String accessToken = oauthIssuerImpl.accessToken();
		
		String expiryTime = ConfigCache.getInstance().getSystemConfig("token_expires_in");
		Long expirTimeInMillis= AuthConstants.TOKENEXPIRESIN;//Assigning default value just in case property is not configured in DB
		if(expiryTime!= null)
			expirTimeInMillis = Long.valueOf(expiryTime);
		
		final Date expireIn = new Date(new Date().getTime()+expirTimeInMillis);
		User user = UserDAO.getInstance().getUserByloginName(request.getUserLogin());
		if(user == null){
			return buildInvalidUserResponse();
		}
		tokenMngr.addToken(accessToken,request.getUserLogin(),expireIn, user.getUserType(), user.getOrgId(), user.getUserId(), user.getGroupId());

		response.setAuthToken(accessToken);
		response.setExpiresIn(expirTimeInMillis);
		response.setResponseStatus("success");
		response.setTimeStamp(simpleDateFormat.format(new Date()));
		return response;
	}

	public  static SignInResponse destroyToken(AuthRequest request){
		SignInResponse response = new SignInResponse();
		// do checking for different grant types
		if (!checkUserToken(request.getAuthToken())) {
			return buildInvalidTokenResponse();
		}
		response.setResponseStatus("success");
		response.setTimeStamp(simpleDateFormat.format(new Date()));
		return response;
	}
	
	private static SignInResponse buildInvalidTokenResponse() {
		SignInResponse response = new SignInResponse();
		response.setResponseStatus("error");
		response.setResponseMessage("Invalid Token");
		response.setTimeStamp(simpleDateFormat.format(new Date()));
		return response;
	}

	private static boolean checkUserToken(String authToken) {	
		return tokenMngr.removeToken(authToken);
	}

	private static SignInResponse buildInvalidUserPassResponse(){
		SignInResponse response = new SignInResponse();
		response.setResponseStatus("error");
		response.setResponseMessage("Invalid username or password");
		response.setTimeStamp(simpleDateFormat.format(new Date()));
		return response;
	}


	private static boolean checkUserPass(String user, String pass) throws Exception {
		return AuthDao.getInstance().checkUserPass(user, pass);
	}
	
	/**
	 * <p>
	 * To be used for generating token fro reset password functionality
	 * </p>
	 * @param userId
	 * @return
	 */
	public static String getRestPasswordToken(String userId){
		String accessToken = null;
		// for now use md5 of userId + current time as token
		String access_token_data =  userId+System.nanoTime();
		accessToken = DigestUtils.md5Hex(access_token_data);
		return accessToken;
	}
	
	private static SignInResponse buildInvalidUserResponse(){
		SignInResponse response = new SignInResponse();
		response.setResponseStatus("error");
		response.setResponseMessage("Invalid userlogin");
		response.setTimeStamp(simpleDateFormat.format(new Date()));
		return response;
	}
}
