package com.rfxcel.auth2;
public class AuthConstants {
    public static final String USER = "User";
    public static final String USER_TYPE = "userType";
    public static final String USER_TIMEZONE = "timeZone";
    public static final String LAST_ACCESS_TIME = "lastAccessTime";
    public static final String HEADER_AUTHORIZATION = "Authorization";
	public static final String RESOURCE_SERVER_NAME = "resource";
	public static final long TOKENEXPIRESIN = 1800000;
	public static final int RESPONSECODE_SUCCESS = 200;
	public static final int RESPONSECODE_ERROR = 500;
	public static final String PASSWORD_ACTION_RESET = "reset";
	public static final String PASSWORD_ACTION_SET = "set";
	public static final String ORGANIZATION_ID	=	"orgId";
	public static final String USER_ID = "userId";
	public static final String GROUP_ID = "groupId";
}
