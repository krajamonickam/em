
package com.rfxcel.auth2.dao;

import java.nio.charset.Charset;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashMap;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.log4j.Logger;

import com.rfxcel.notification.dao.DAOUtility;
import com.rfxcel.auth2.beans.AuthRequest;
import com.rfxcel.cache.ConfigCache;

public class AuthDao {
	private static final Logger logger = Logger.getLogger(AuthDao.class);
	private static AuthDao authDAO;
	private static ConfigCache propcache = ConfigCache.getInstance();
	
	public static AuthDao getInstance(){
		if(authDAO == null){
			authDAO = new AuthDao();
		}
		return authDAO;
	}

	/**
	 * <p> Check if username and password entry is present in db </p>
	 * @param username
	 * @param pass
	 * @return
	 * @throws SQLException 
	 */
	public boolean checkUserPass(String username,String pass) throws SQLException{
		boolean isValid = false;
		boolean update512 = false;
		Connection conn= null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_passwd, user_passwd512 FROM rfx_users WHERE user_login=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, username);
			ResultSet  rs = stmt.executeQuery();
			String password = DigestUtils.sha1Hex(pass.getBytes(Charset.forName("UTF-8")));
			String password512 = DigestUtils.sha512Hex(pass.getBytes(Charset.forName("UTF-8")));
			while (rs.next()) {
				String passwd512 = rs.getString("user_passwd512");
				String passwd = rs.getString("user_passwd");
				if (passwd512 == null) {
					if ((passwd != null) && (passwd.equals(password))){
						update512 = true;
						isValid = true;
					}
				}
				else {
					if ((passwd512 != null) && (passwd512.equals(password512))){
						isValid = true;
					}
				}
			}
			rs.close();
			stmt.close();
			if (isValid && update512) {
				query = "UPDATE rfx_users SET user_passwd512=?, user_passwd = NULL WHERE user_login=?";
				stmt = conn.prepareStatement(query);
				stmt.setString(1, password512);
				stmt.setString(2, username);
				stmt.executeUpdate();
				stmt.close();
			}
		}
		finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return isValid;
	}
	
	/**
	 * 
	 * @param request
	 * @return
	 * @throws SQLException
	 */
	public int checkIfResetPassTokenIsValid(AuthRequest request) throws SQLException {
		Connection conn= null;
		int state = -1;		//0- token not valid, 1- token not matching
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT token FROM rfx_users WHERE user_login=? and token_expiry_time > now()";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, request.getUserLogin());
			ResultSet  rs = stmt.executeQuery();
			String token = null ;
			while (rs.next()) {
				token = rs.getString("token");
			}
			if(token == null){
				state = 0;
			}else if(!token.equals(request.getToken())) {
				state = 1;
			}
			rs.close();
			stmt.close();
		}
		finally {
			DAOUtility.getInstance().closeConnection(conn);
		}
		return state;
	}
	
	
	
	/**
	 * 
	 * @param request
	 * @return 0- token is not present ie password is already set , 1- token not matching
	 * @throws SQLException
	 */
	public int checkIfSetPassTokenIsValid(AuthRequest request) throws SQLException {
		Connection conn= null;
		int state = -1;		//0- token is not present, password is already set, 1- token not matching
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT token FROM rfx_users WHERE user_login=?)";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, request.getUserLogin());
			ResultSet  rs = stmt.executeQuery();
			String token = null ;
			while (rs.next()) {
				token = rs.getString("token");
			}
			if(token == null){
				state = 0;
			}else if(!token.equals(request.getToken())) {
				state = 1;
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return state;
	}

	/**
	 * 
	 * @param request
	 * @throws SQLException
	 */
	public void resetPassword(AuthRequest request) throws SQLException {
		Connection conn= null;
		try{
			// convert the password to encoded value
			String encodedPassword = DigestUtils.sha512Hex(request.getPassword().getBytes(Charset.forName("UTF-8")));
			conn = DAOUtility.getInstance().getConnection();
			String query = "UPDATE rfx_users SET user_passwd512=?, last_passwd_changed=now(), token = NULL, token_expiry_time = NULL WHERE user_login=?  ";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, encodedPassword);
			stmt.setString(2, request.getUserLogin());
			stmt.executeUpdate();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	
	/**
	 * 
	 * @param userName
	 * @param token
	 * @param reset
	 * @throws SQLException
	 */
	public void updateTokenInfoForChangePassword(String userName, String token, Boolean reset) throws SQLException {
		Connection conn= null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			int duration;
			String configProp;
			if(reset){
				configProp =  propcache.getSystemConfig("reset.pass.token.expiry");
				duration = configProp ==  null ? 1 : Integer.valueOf(configProp);
			}else{
				configProp =  propcache.getSystemConfig("set.pass.token.expiry");
				duration = configProp ==  null ? 1 : Integer.valueOf(configProp);
			}
			String query = "UPDATE rfx_users SET token=?, token_expiry_time=DATE_ADD(now(), INTERVAL ? Day) WHERE user_login=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, token);
			stmt.setInt(2, duration);
			stmt.setString(3, userName);
			stmt.executeUpdate();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
	}

	/**
	 * 
	 * @param userName
	 * @return
	 * @throws SQLException
	 */
	public HashMap<String, String> getUserDetails(String userName) throws SQLException {
		Connection conn= null;
		HashMap<String, String> values = new HashMap<String, String>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_name, user_email FROM rfx_users WHERE user_login=?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userName);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				values.put("userName", rs.getString("user_name"));
				values.put("emailAddress", rs.getString("user_email"));
			}
			rs.close();
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return values;
	}
	
	/**
	 * 
	 * @param userName
	 * @return
	 * @throws SQLException
	 */
	public Boolean checkIfUserIsPresent(String userName) throws SQLException {
		Connection conn= null;
		long id = -1;
		Boolean present = false;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_id FROM rfx_users WHERE user_login = ? AND active=1 LIMIT 1";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userName);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				id = rs.getLong("user_id");
			}
			if(id != -1){
				present = true;
			}
			
			stmt.close();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return present;
	}
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	public HashMap<String, String> getUsersTimeZoneOffset(){
		Connection conn= null;
		HashMap<String, String> userTzMapping  = new HashMap<String, String>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_login, user_tz FROM rfx_users";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				userTzMapping.put(rs.getString("user_login"), rs.getString("user_tz"));
				
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting timeZone for users: ");
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return userTzMapping;
	}
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	public String getUserTimeZoneOffset(String userName){
		Connection conn= null;
		String timeZoneOffset = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT user_login, user_tz FROM rfx_users where user_login = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userName);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				timeZoneOffset = rs.getString("user_tz");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting timeZone offset for user "+ userName);
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return timeZoneOffset;
	}
	
	/**
	 * 
	 * @return
	 */
	public HashMap<String, String> getUsersTimeZone(){
		Connection conn= null;
		HashMap<String, String> userTzMapping  = new HashMap<String, String>();
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT  user_login, timezone_code  FROM rfx_users inner join rfx_timezone on rfx_users.tz_id = rfx_timezone.id";
			PreparedStatement stmt = conn.prepareStatement(query);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				userTzMapping.put(rs.getString("user_login"), rs.getString("timezone_code"));
				
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error in getUsersTimeZone : ");
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return userTzMapping;
	}
	
	/**
	 * 
	 * @param userName
	 * @return
	 */
	public String getUserTimeZone(String userLogin){
		Connection conn= null;
		String timeZoneCode = null;
		try{
			conn = DAOUtility.getInstance().getConnection();
			String query = "SELECT  user_login, timezone_code FROM rfx_users inner join rfx_timezone on rfx_users.tz_id = rfx_timezone.id where user_id = ?";
			PreparedStatement stmt = conn.prepareStatement(query);
			stmt.setString(1, userLogin);
			ResultSet rs = stmt.executeQuery();
			while(rs.next()){
				timeZoneCode = rs.getString("timezone_code");
			}
			rs.close();
			stmt.close();
		} catch (SQLException e) {
			logger.error("Error while getting timeZone offset for user with id "+ userLogin);
			e.printStackTrace();
		}finally{
			DAOUtility.getInstance().closeConnection(conn);
		}
		return timeZoneCode;
	}
	
}
