package com.rfxcel.auth2;

import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;

import org.apache.commons.codec.binary.StringUtils;
import org.apache.log4j.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;

import com.rfxcel.auth2.beans.AuthRequest;
import com.rfxcel.auth2.beans.AuthResponse;
import com.rfxcel.auth2.beans.SignInResponse;
import com.rfxcel.auth2.dao.AuthDao;
import com.rfxcel.cache.ConfigCache;
import com.rfxcel.federation.RemoteGetShard;
import com.rfxcel.federation.FederatedGetShard;
import com.rfxcel.notification.service.EmailAlertService;
import com.rfxcel.notification.util.EmailTemplateUtil;
import com.rfxcel.notification.vo.EmailDetailsVO;
import com.rfxcel.org.dao.AuditLogDAO;
import com.rfxcel.org.dao.OrganizationDAO;
import com.rfxcel.org.dao.UserDAO;
import com.rfxcel.org.entity.AuditLog;
import com.rfxcel.org.entity.Organization;
import com.rfxcel.org.entity.User;
import com.rfxcel.sensor.control.RESTClient;
import com.rfxcel.sensor.service.DiagnosticService;
import com.rfxcel.sensor.util.SensorConstant;
import com.rfxcel.sensor.util.Timer;


@Path("sensor")
public class AuthService { 
	SimpleDateFormat simpleDateFormat  = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private static final Logger logger = Logger.getLogger(AuthService.class);
	private static DiagnosticService diagnosticService = DiagnosticService.getInstance();
	private static final ConfigCache propCache = ConfigCache.getInstance();
	private static final UserDAO userDao = UserDAO.getInstance();
	private static AuditLogDAO auditLogDao = AuditLogDAO.getInstance();
	private static TokenManager tokenManager = TokenManager.getInstance();
	private RESTClient http = null;
	
	public AuthService() {
		http = new RESTClient("", "");
	//	simpleDateFormat.setTimeZone(TimeZone.getTimeZone("UTC")); TODO check if we need to set dateTime in response in UTC
	}
	
	@GET
	@Path("user/shard/{user}")
	@Produces("application/json")
	public Response getShard(@PathParam("user")String user, @QueryParam("federated") Boolean federated){
		Timer timer = new Timer();
		if (federated == null) federated = new Boolean(false);
		String shard = FederatedGetShard.getInstance().getShardForUser(user, federated);
		logger.info("getUserShard for " + user + " retrieved in " + timer.getSeconds() + "s - " + shard);
		return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(shard).build();
	}
	
	@POST
	@Path("user/login")
	@Consumes("application/json")
	@Produces("application/json")
	public Response signIn(AuthRequest request){
		SignInResponse res = new SignInResponse(simpleDateFormat.format(new Date()));
		try {
			String userLogin = request.getUserLogin(); 
			String password = request.getPassword();
			if (userLogin == null || userLogin.trim().length() == 0) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("User Login is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			if (password == null || password.trim().length() == 0) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Password is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			User user = userDao.getUserByloginName(userLogin);
			
			if(user != null ) {
				Integer orgId = user.getOrgId();
				if(user.getUserType()!=0)	//Skipping org active check for System user
				{
					Boolean active = OrganizationDAO.getInstance().checkIfOrganizationIsActive(orgId);
					if(!active){
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
						res.setResponseMessage("Cannot login since organization is inactive");
						return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
					}
				}
				if(!user.getActive()){
					res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
					res.setResponseMessage("Inactive User");
					return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
				}else{
					res.setCurrentUser(user);
				}
		
				res = TokenUtility.generateToken(request);
				if (orgId != 0) {
					loginRTS(orgId);
				}
	
				if (res.getResponseStatus().equals(SensorConstant.RESP_STAT_SUCCESS)) {
					Long userId = user.getUserId();
					String userName = user.getName();
					AuditLog auditLog = new AuditLog(userId, userName, new Long(userId), "SignIn", "", null, null, "User "+userName +" has Logged In.", orgId);
					auditLogDao.addAuditLog(auditLog);
	
				}
			} else{
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Invalid User!, The user name does not match , please check your user name!");
			}
			return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			
		} catch (Exception e) {
			
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage(e.getLocalizedMessage());
			logger.error("Exception in login "+ e.getMessage());
			
			return Response.status(AuthConstants.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: login.  Request :: "+request +" Response :: "+res );
		}
	}

	

	@POST
	@Path("user/logout")
	@Consumes("application/json")
	@Produces("application/json")
	public Response signOut(AuthRequest request){
		SignInResponse res = new SignInResponse(simpleDateFormat.format(new Date()));
		try {
			Integer orgId = request.getOrgId();
			String userLogin = request.getUserLogin();
			//check if orgId is valid
			if(orgId == null || orgId < 0){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("OrgId cannot be null or negative.");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();								
			}
			if (userLogin == null || userLogin.equals("")) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("UserLogin is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}

			if (request.getAuthToken()==null || request.getAuthToken().equals("")) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Auth token is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			User user = userDao.getUserByloginName(userLogin);
			res = TokenUtility.destroyToken(request);
			if(res.getResponseStatus().equals("success")){
				if(user != null){
				//Audit Log related code
				Long userId = user.getUserId();
				String userName = user.getName();
				AuditLog auditLog = new AuditLog(userId, userName, new Long(userId), "SignOut", "", null, null, "User "+userName +" has Logged Out.", orgId);
				auditLogDao.addAuditLog(auditLog);
				}
			}
			return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();

		} catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage(e.getLocalizedMessage());
			logger.error("Exception in signOut "+ e.getMessage());
			e.printStackTrace();
			return Response.status(AuthConstants.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: signOut.  Request :: "+request +" Response :: "+res );
		}
	}
	
	@POST
	@Path("user/forgetPassword")
	@Consumes("application/json")
	@Produces("application/json")
	public Response forgetPassword(AuthRequest request){
		AuthResponse res = new AuthResponse(simpleDateFormat.format(new Date()));
		try {
			String userLogin =  request.getUserLogin();
			String url = request.getUrl();
			if (userLogin == null || userLogin.equals("")) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Username is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			//Check if user is present
			Boolean userPresent = AuthDao.getInstance().checkIfUserIsPresent(userLogin);
			if(!userPresent){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Username is not configured in system");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			if(url == null || url.equals("")){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("URL is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			
			String token = TokenUtility.getRestPasswordToken(userLogin);
			Integer orgId = UserDAO.getInstance().getUsersOrgId(userLogin);
			url =url+"?token="+token+"&userName="+userLogin+"&action=reset"+"&orgId="+orgId;
	
			
			AuthDao.getInstance().updateTokenInfoForChangePassword(userLogin, token, true);
			//Trigger mail and return response to UI
			HashMap< String, String> values = AuthDao.getInstance().getUserDetails(userLogin);
			String toAddress = values.get("emailAddress");
			String name = values.get("userName");
			String salutation = "Hi "+name;
			String subject = propCache.getSystemConfig("reset.password.subject");
			String title=propCache.getSystemConfig("reset.password.title");
			String bodyTextPart = propCache.getSystemConfig("reset.password.text.body");
			String bodyTextPart1 = "";
			String bodyTextPart2 = "";
			String bodyTextPart3="";
			if(bodyTextPart!=null){
				String[] textPart = bodyTextPart.split("#");
				int textpartSize = textPart.length;
				if(textpartSize == 1){
					bodyTextPart1 =textPart[0]; 
				}else if(textpartSize == 2){
					bodyTextPart1 =textPart[0]; 
					bodyTextPart2 = textPart[1];
				}else if(textpartSize == 3){
					bodyTextPart1 =textPart[0]; 
					bodyTextPart2 = textPart[1];
					bodyTextPart3 = textPart[2];
				}
			}else{
				logger.info("Create password text body is not configured.");
			}
			String message = EmailTemplateUtil.getWelcomeEmailTemplate(url, "btnReset", title, bodyTextPart1, bodyTextPart2, bodyTextPart3);
			ArrayList<String> imageList = new ArrayList<String>();
			imageList.add("btnReset");
			EmailDetailsVO emailDetails= new EmailDetailsVO(toAddress, subject, message, "",salutation);
			emailDetails.setImageMapping(imageList);
			new EmailAlertService().triggerEmail(emailDetails);
			
			// Audit Log related code
			User user = userDao.getUserByloginName(userLogin);
			Long userId = user.getUserId();
			String userName = user.getName();
			AuditLog auditLog = new AuditLog(userId, userName, new Long(userId), "ForgetPassword", "", null, null, "User "+userName +" has made API call for forget password.",orgId);
			auditLogDao.addAuditLog(auditLog);
			
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			res.setResponseMessage("An email has been sent to you with a link to reset your password");
			return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();

		} catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage(e.getLocalizedMessage());
			logger.error("Exception in forgetPassword "+ e.getMessage());
			e.printStackTrace();
			return Response.status(AuthConstants.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: forgetPassword.  Request :: "+request +" Response :: "+res );
		}
	}
	
	/**
	 * <p> Reset password if the token is valid </p>
	 * @param request
	 * @return
	 */
	@POST
	@Path("user/setPassword")
	@Consumes("application/json")
	@Produces("application/json")
	public Response resetPassword(@Context HttpServletRequest httpRequest, AuthRequest request){
		AuthResponse res = new AuthResponse(simpleDateFormat.format(new Date()));
		try {
			//check if userName is present
			String userLogin = request.getUserLogin();
			String password = request.getPassword();
			String action = request.getPasswordAction();
			Integer orgId = request.getOrgId();
			
			//check if orgId is valid
			if(orgId == null || orgId < 0){
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("OrgId cannot be null or negative.");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();								
			}
			if (userLogin == null || userLogin.equals("")) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Username is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			//check if password is present
			if (password ==null || password.equals("")) {
				res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				res.setResponseMessage("Password is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
			}
			boolean systemUser = false;
			String userFromToken = TokenManager.getInstance().getUserForToken(httpRequest);
			if (userFromToken != null) {
				User user = userDao.getUserByloginName(userFromToken);
				if ((user != null) && (user.getUserType() == 0)) {
					systemUser = true;
				}
			}
			User user = userDao.getUserByloginName(userLogin);
			Long userId = user.getUserId();
			String userName = user.getName();
			if( action== null || AuthConstants.PASSWORD_ACTION_RESET.equalsIgnoreCase(action)){
				if (!systemUser) {
					//Check if the token is valid //0- token not valid, 1- token not matching
					int state = AuthDao.getInstance().checkIfResetPassTokenIsValid(request);
					if(state == 0){
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
						res.setResponseMessage("Reset password link has expired");
						return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
					}
					if(state == 1){
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
						res.setResponseMessage("Reset password link is not valid");
						return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
					}
				}
				// Audit Log related code
				AuditLog auditLog = new AuditLog(userId, userName, new Long(userId), "ResetPassword", "", null, null, "User "+userName +" has reset password.", orgId);
				auditLogDao.addAuditLog(auditLog);
				res.setResponseMessage("Password Reset Successful");
			}else if(AuthConstants.PASSWORD_ACTION_SET.equalsIgnoreCase(action)){
				if (!systemUser) {
					int state = AuthDao.getInstance().checkIfSetPassTokenIsValid(request);
					if(state == 0){
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
						res.setResponseMessage("Password is already set");
						return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
					}
					if(state == 1){
						res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
						res.setResponseMessage("Set password link is not valid");
						return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
					}
				}
				// Audit Log related code
				AuditLog auditLog = new AuditLog(userId, userName, userId, "SetPassword", "", null, null, "User "+userName +" has set password.", orgId);
				auditLogDao.addAuditLog(auditLog);
				res.setResponseMessage("Password set successfully");
			}
			AuthDao.getInstance().resetPassword(request);
			
			res.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(res).build();
		} catch (Exception e) {
			res.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			res.setResponseMessage(e.getLocalizedMessage());
			logger.error("Exception in resetPassword "+ e.getMessage());
			e.printStackTrace();
			return Response.status(AuthConstants.RESPONSECODE_ERROR).entity(res).build();
		}finally{
			diagnosticService.write("API :: resetPassword.  Request :: "+request +" Response :: "+res );
		}
	}
	
	/**
	 * 
	 * @param userLogin
	 * @param url
	 * @throws SQLException
	 */
	public static void setPassword(String userLogin, String url) throws SQLException{
		if(url != null && url.trim().length() != 0){
			String token = TokenUtility.getRestPasswordToken(userLogin);
			
			Integer orgId = UserDAO.getInstance().getUsersOrgId(userLogin);
			url =url+"?token="+token+"&userName="+userLogin+"&action=set"+"&orgId="+orgId;
			AuthDao.getInstance().updateTokenInfoForChangePassword(userLogin, token, false);
			//Trigger mail and return response to UI
			HashMap< String, String> values = AuthDao.getInstance().getUserDetails(userLogin);
			String toAddress = values.get("emailAddress");
			String name = values.get("userName");
			String salutation = "Hi "+name;
			String subject = propCache.getSystemConfig("create.password.subject");
			String title=propCache.getSystemConfig("create.password.title");
			String bodyTextPart = propCache.getSystemConfig("create.password.text.body");
			String bodyTextPart1 = "";
			String bodyTextPart2 = "";
			String bodyTextPart3="";
			if(bodyTextPart!=null){
				String[] textPart = bodyTextPart.split("#");
				int textpartSize = textPart.length;
				if(textpartSize == 1){
					bodyTextPart1 =textPart[0]; 
				}else if(textpartSize == 2){
					bodyTextPart1 =textPart[0]; 
					bodyTextPart2 = textPart[1];
				}else if(textpartSize == 3){
					bodyTextPart1 =textPart[0]; 
					bodyTextPart2 = textPart[1];
					bodyTextPart3 = textPart[2];
				}
			}else{
				logger.info("Create password text body is not configured.");
			}
			String message = EmailTemplateUtil.getWelcomeEmailTemplate(url, "btnConfirm", title, bodyTextPart1, bodyTextPart2, bodyTextPart3);
			ArrayList<String> imageList = new ArrayList<String>();
			imageList.add("btnConfirm");
			EmailDetailsVO emailDetails= new EmailDetailsVO(toAddress, subject, message, "",salutation);
			emailDetails.setImageMapping(imageList);
			new EmailAlertService().triggerEmail(emailDetails);
		}else{
			logger.error("URl value was not set while calling create User API. So set password mail is not triggered...");
		}
	}
	
	/**
	 * <p>This API is used to login admin/standard user as support user. This functionality is available under system user only.</p>
	 * @param request
	 * @return
	 */
	@POST
	@Path("user/loginAsSupportUser")
	@Consumes("application/json")
	@Produces("application/json")
	public Response loginAsSupportUser(@Context HttpServletRequest httpRequest, AuthRequest request){
		SignInResponse signInResponse = new SignInResponse(simpleDateFormat.format(new Date()));		
		try{			
			
			String authToken = request.getAuthToken();
			if (authToken == null || authToken.equals("")) {
				signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				signInResponse.setResponseMessage("Auth token is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
			}
			String userLogin = request.getUserLogin(); 			
			if (userLogin == null || userLogin.trim().length() == 0) {
				signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				signInResponse.setResponseMessage("User Login is null or empty");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
			}
			String userName = tokenManager.getUserForToken(httpRequest);
			Long userId = tokenManager.getUserId(httpRequest);

			//Logout the system user
			signInResponse = TokenUtility.destroyToken(request);			
			if(SensorConstant.RESP_STAT_ERROR.equals(signInResponse.getResponseStatus())){
				signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				signInResponse.setResponseMessage("Unable to signout the current user due to "+signInResponse.getResponseMessage());
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
			}
			
			//login admin/standard user ad support user			
			signInResponse = TokenUtility.generateTokenForSupportUser(request);
			if(SensorConstant.RESP_STAT_ERROR.equals(signInResponse.getResponseStatus())){
				signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				signInResponse.setResponseMessage(signInResponse.getResponseMessage());
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
			}

			User user = userDao.getUserByloginName(userLogin);
			Integer orgId = user.getOrgId();
			if(user.getUserType()!=0)	//Skipping org active check for System user
			{
				Boolean active = OrganizationDAO.getInstance().checkIfOrganizationIsActive(orgId);
				if(!active){
					signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
					signInResponse.setResponseMessage("Cannot login since organization is inactive");
					return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
				}
			}
			if(!user.getActive()){
				signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				signInResponse.setResponseMessage("Inactive User");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
			}else{
				signInResponse.setCurrentUser(user);
				loginRTS(orgId);
			}


			AuditLog auditLog = new AuditLog(userId, userName, new Long(userId), "User", "Switch", null, null, " Support user "+userName +" has Logged in as user "+user.getName(),orgId );
			auditLogDao.addAuditLog(auditLog);
			signInResponse.setResponseStatus(SensorConstant.RESP_STAT_SUCCESS);
			return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(signInResponse).build();
		}catch (Exception e) {
			signInResponse.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			signInResponse.setResponseMessage(e.getLocalizedMessage());
			logger.error("Exception in loginAsSupportUser "+ e.getMessage());
			e.printStackTrace();
			return Response.status(AuthConstants.RESPONSECODE_ERROR).entity(signInResponse).build();
		}finally{
			diagnosticService.write("API :: loginAsSupportUser.  Request :: "+request +" Response :: "+signInResponse );
		}
	}
	
	/**
	 * <p>This API is used to load config data just after login API. </p>
	 * @param httpRequest
	 * @param request
	 * @return
	 */
	@POST
	@Path("user/getConfigData")
	@Consumes("application/json")
	@Produces("application/json")
	public Response getConfigData(@Context HttpServletRequest httpRequest, AuthRequest request){
		SignInResponse response = new SignInResponse(simpleDateFormat.format(new Date()));
		try{
			//check if request is null
			if(request == null){
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				response.setResponseMessage("Request cannot be null.");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(response).build();	
			}
			Integer orgId = request.getOrgId();
			//check if org id is not null or negative
			if(orgId == null || orgId < 0){
				response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
				response.setResponseMessage("OrgId cannot be null or negative.");
				return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(response).build();				
			}
			boolean validToken = TokenManager.getInstance().validateToken(httpRequest, orgId);
			if(!validToken){
				return Response.status(SensorConstant.RESPONSECODE_SUCCESS).entity(com.rfxcel.sensor.vo.Response.createAuthFailureResponse()).build();
			}
			
			String autoRefreshFreqString = propCache.getOrgConfig("sensor.graph.autorefresh.freq", orgId);
			int autoRefreshFreq = 0;
			if(autoRefreshFreqString != null){
				autoRefreshFreq = Integer.valueOf(autoRefreshFreqString);
			}
			String autoRefreshString = propCache.getOrgConfig("sensor.graph.autorefresh", orgId);
			Boolean autoRefresh = false;
			if(autoRefreshString != null){
				autoRefresh =  Boolean.valueOf(autoRefreshString);
			}
			
			String centerLatString = propCache.getOrgConfig("dashboard.map.center.latitude", orgId);
			Double centerLat = null;
			if (centerLatString == null) {
				centerLat = 39d;
			}
			else {
				centerLat = Double.valueOf(centerLatString);
			}
			
			String centerLongString = propCache.getOrgConfig("dashboard.map.center.longitude", orgId);
			Double centerLong = null;
			if (centerLongString == null) {
				centerLong = -99d;
			}
			else {
				centerLong = Double.valueOf(centerLongString);
			}
			
			String mapZoomString = propCache.getOrgConfig("dashboard.map.zoom", orgId);
			Integer mapZoom = null;
			if (mapZoomString == null) {
				mapZoom = 5;
			}
			else {
				mapZoom = Integer.valueOf(mapZoomString);
			}
			
			String mapType = propCache.getOrgConfig("dashboard.map.type", orgId);
			if (mapType == null) {
				mapType = "google.maps.MapTypeId.HYBRID";
			}
			
			String helpdeskContactMessage = propCache.getSystemConfig("helpdesk.contact.message");
			if(helpdeskContactMessage == null){
				helpdeskContactMessage = "";
			}
						
			String systemDemoString = propCache.getOrgConfig("system.demo", orgId);
			Boolean systemDemo = false;
			if(systemDemoString != null){
				systemDemo = Boolean.valueOf(systemDemoString);
			}
			
			String clickToChatString = propCache.getOrgConfig("system.clicktochat", orgId);
			Boolean clickToChat = false;
			if(clickToChatString != null){
				clickToChat = Boolean.valueOf(clickToChatString);
			}
			
			String fetchESDataString = ConfigCache.getInstance().getSystemConfig("elasticsearch.data.fetch");
			Boolean fetchESData = false;
			if(fetchESDataString != null){
				fetchESData = Boolean.valueOf(fetchESDataString);
			}
			
			String alertDevicelogString = ConfigCache.getInstance().getSystemConfig("alert.devicelog.search.enable");
			Boolean alertDeviceLogSearch = false;
			if(alertDevicelogString != null){
				alertDeviceLogSearch = Boolean.valueOf(alertDevicelogString);
			}
			
			
			//String rtsURL = ConfigCache.getInstance().getSystemConfig("rts.application.url");
			String rtsURL = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			response.setRtsURL(rtsURL);
			response.setAuthToken(ConfigCache.getInstance().getOrgConfig("rts.application.auth", orgId));
			response.setAutoRefresh(autoRefresh);
			response.setAutoRefreshFreq(autoRefreshFreq);
			response.setCenterLat(centerLat);
			response.setCenterLong(centerLong);
			response.setMapType(mapType);
			response.setMapZoom(mapZoom);
			response.setHelpdeskContactMessage(helpdeskContactMessage);
			response.setSystemDemo(systemDemo);
			response.setClickToChat(clickToChat);
			response.setFetchESData(fetchESData);
			response.setAlertDeviceLogSearch(alertDeviceLogSearch);
			response.setNewProfileScreen(propCache.getOrgConfig("new.profile.screen",orgId)==null ? false : Boolean.valueOf(propCache.getOrgConfig("new.profile.screen",orgId)));
			
			return Response.status(AuthConstants.RESPONSECODE_SUCCESS).entity(response).build();
		}catch(Exception e){
			response.setResponseStatus(SensorConstant.RESP_STAT_ERROR);
			response.setResponseMessage(e.getLocalizedMessage());
			logger.error("Exception in getConfigData "+ e.getMessage());			
			return Response.status(AuthConstants.RESPONSECODE_ERROR).entity(response).build();
		}finally{
			diagnosticService.write("API :: getConfigData.  Request :: "+request +" Response :: "+response );
		}
	}
	
	private void loginRTS(int orgId) {
		// Logging into rts application
		String userLogin = ConfigCache.getInstance().getOrgConfig("rts.application.username", orgId);
		String password = ConfigCache.getInstance().getOrgConfig("rts.application.password", orgId);
		
		if (userLogin == null || password == null) {
			logger.info("username or password is empty for organization " + orgId);
			return;
		}
		String bodyText="{\r\n\t\t\"userName\": \""+ userLogin.trim() +"\",\r\n\t\t\"password\": \""
						+password.trim()+"\"\r\n\t}";
		
		String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
		rtsAppUrl = rtsAppUrl + "user/login";
		String jsonText = http.doPost(rtsAppUrl, "Authorization","",bodyText);
		JSONObject jsonObject = new JSONObject(jsonText);
	
		if(jsonObject.has("authToken")){
			String authToken=jsonObject.getString("authToken");
			ConfigCache.getInstance().setOrgConfig(orgId, SensorConstant.AUTH_TOKEN_KEY,authToken);
			logger.info("rTS token has been reset for organization in AuthService" + orgId);
		}
	}
}
