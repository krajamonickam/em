package com.rfxcel.federation;

import java.sql.SQLException;
import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.org.dao.UserDAO;

public class LocalGetShard implements Callable<String>
{
	private static final Logger logger = Logger.getLogger(LocalGetShard.class);
	private String user = null;
	
	public LocalGetShard(String user) {
		this.user = user;
	}

	@Override
	public String call() {
		try {
			if (user != null) {
				if (UserDAO.getInstance().checkIfActiveUserIsPresent(user)) {
					String shard = ConfigCache.getInstance().getSystemConfig("application.shard");
					String result = "{\"user\":\"" + user + "\",\"shard\":" + shard + "}";
					logger.info("getLocalUser retrieved " + result);
					return(result);
				}
			}
		} catch (SQLException e) {
			e.printStackTrace();
		}
		return(null);
	}
}
