package com.rfxcel.federation;

import java.util.Date;
import java.util.List;
import java.util.concurrent.CompletionService;
import java.util.concurrent.ExecutorCompletionService;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import org.apache.log4j.Logger;
import org.json.JSONObject;

import com.rfxcel.cache.ConfigCache;
import com.rfxcel.org.dao.OrganizationDAO;
import com.rfxcel.org.entity.Organization;
import com.rfxcel.sensor.control.RESTClient;
import com.rfxcel.sensor.util.SensorConstant;

public class FederatedGetShard {
	private static final Logger logger = Logger.getLogger(FederatedGetShard.class);
	private static FederatedGetShard instance = null;
	private static String[] urlList = null;
	private RESTClient http = null;
	public static FederatedGetShard getInstance() {
		if (instance == null) {
			instance = new FederatedGetShard();
		}
		return(instance);
	}

	public FederatedGetShard() {
		String urls = ConfigCache.getInstance().getSystemConfig("application.urllist");
		if (urls != null) {
			urlList = urls.split(",");
			for (String url : urlList) {
				logger.info("*** URL " + url + " added to federated list ***");
			}
		}
		http = new RESTClient("", "");
		Thread rtsTokenMonitorThread = new Thread() {
			public void run() {
				while (true) {
					logger.info("rTS token monitor thread invoked: " + new Date());
					try {
						List<Organization> orgs = OrganizationDAO.getInstance().getAllOrganizations();
						for (Organization org : orgs) {
							
							loginRTS(org.getOrgId());
						}
					} catch (Exception e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					try {
						Thread.sleep(900000);
					} catch (InterruptedException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}
			}
			
		};
		rtsTokenMonitorThread.start();
		logger.info("rTS token monitor thread started!!!");
	}
	
	private void loginRTS(int orgId) {
		// Logging into rts application
		try {
			String userLogin = ConfigCache.getInstance().getOrgConfig("rts.application.username", orgId);
			String password = ConfigCache.getInstance().getOrgConfig("rts.application.password", orgId);
			
			if (userLogin == null || password == null) {
				logger.info("username or password is empty for organization " + orgId);
				return;
			}
			String bodyText="{\r\n\t\t\"userName\": \""+ userLogin.trim() +"\",\r\n\t\t\"password\": \""
							+password.trim()+"\"\r\n\t}";
			
			String rtsAppUrl = ConfigCache.getInstance().getOrgConfig("rts.application.url", orgId);
			rtsAppUrl = rtsAppUrl + "user/login";
			String jsonText = http.doPost(rtsAppUrl, "Authorization","",bodyText);
			JSONObject jsonObject = new JSONObject(jsonText);
		
			if(jsonObject.has("authToken")){
				String authToken=jsonObject.getString("authToken");
				ConfigCache.getInstance().setOrgConfig(orgId, SensorConstant.AUTH_TOKEN_KEY,authToken);
				logger.info("rTS token has been reset for organization " + orgId);
			}
		} catch(Exception e) { 
			logger.error("exception in loginRTS: " + e.getMessage() + ", " + orgId);	
		}
	}
	
	public String getShardForUser(String user, boolean federated) {
		if (user == null) {
			return(null);
		}
		if (!federated || (urlList == null)) {
			LocalGetShard req = new LocalGetShard(user);
			return(req.call());
		}
		else {
			ExecutorService executor = Executors.newFixedThreadPool(urlList.length);
			CompletionService<String> completionService = new ExecutorCompletionService<String>(executor);
			int requested = 1;
			completionService.submit(new LocalGetShard(user));
			for (int i = 0; i < urlList.length; i++) {
				completionService.submit(new RemoteGetShard(urlList[i], user));
				requested++;
			}
			int received  = 0;
			while (received < requested) {
				try {
					Future<String> f = completionService.take();
					String result = f.get();
					received++;
					if (result == null) continue;
					if (result.trim().length() > 0) {
						return(result);
					}
				} catch (Exception e) {
					return(null);
				}
			}
			return(null);		
		}
	}
}