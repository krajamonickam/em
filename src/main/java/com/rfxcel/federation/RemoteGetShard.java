package com.rfxcel.federation;

import java.util.concurrent.Callable;

import org.apache.log4j.Logger;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RemoteGetShard implements Callable<String>
{
	private static final Logger logger = Logger.getLogger(RemoteGetShard.class);
	private String url = null;
	private String user = null;
	
	public RemoteGetShard(String url, String user) {
		this.url = url;
		this.user = user;
	}

	@Override
	public String call() {
		String url = this.url + "/" + this.user;
	    Request request = new Request.Builder().url(url).build();
	    OkHttpClient client = new OkHttpClient();
	    try (Response response = client.newCall(request).execute()) {
	      if (!response.isSuccessful()) {
	    	  logger.error("HTTP error " + response.code() + " from " + url);
		      return(null);
	      };
	      String shard = response.body().string(); 
   		  logger.info("getRemoteUser retrieved " + shard);
	      return(shard);
	    }
	    catch(Exception ex) {
    	  logger.error("HTTP exception " + ex.getMessage() + " from " + url);
	      return(null);
	    }
	}
}
